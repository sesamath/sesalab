const fs = require('fs')
const path = require('path')
const fetch = require('node-fetch')
const { agent } = require('supertest')

const promisify = require('sesalab-commun/promisify')

const settings = require('../config')
const loadFixtures = require('./fixtures/load')

const isDebug = Boolean(process.env.DEBUG)

if (process.env.NODE_ENV !== 'test') throw new Error('Ce fichier ne doit être chargé qu’avec NODE_ENV=test (il vide la db utilisée)')

const usualConf = require('../_private/config')
// on vérifie que c'est pas la même base, sinon on va tout écraser (on check pas host & port par flemme et sécurité,
// faudrait comparer l'absence avec localhost, et comme on peut avoir 2 hostname ≠ qui pointent sur la même cible
// il faudrait lancer une résolution dns…)
if (settings.$entities.database.name === usualConf.$entities.database.name) throw Error('Impossible de lancer les tests sur la même base que celle par défaut (ça va tout effacer)')

const logDir = path.resolve(__dirname, '../log/testBrowser')
if (!fs.existsSync(logDir)) fs.mkdirSync(logDir, { recursive: true })

// Global lassi
require('lassi')({
  root: path.join(__dirname, '/..'),
  // Les settings de tests sont construits à partir de config/test.js et non
  // à partir de _private/config.js
  settings,
  test: true
})

const isPlayWright = /playwright/.test(process.argv.join(' '))
const isTestBrowser = isPlayWright || /wdio/.test(process.argv.join(' '))

if (isTestBrowser) global.app = lassi.component('sesalab', ['sesalab-sso'])
else global.app = lassi.component('sesalab')

global.app.settings = settings
// on charge l'api
require('sesalab-api/source')
if (isTestBrowser) {
  // mais aussi tout ça si on est lancé par wdio (cf source/index)
  require('../sesalab-formateur/source/server')
  require('../sesalab-eleve/source/server')
  // require('../sesalab-gestion/source/server')
  require('../sesalab-home/source/server')
  // et le sso vers les sesatheques
  require('sesalab-sso')
}

// On utilise une unique promise pour être sûr que lassi n'est bootstrap
// qu'une seule fois
let lassiBootstrapPromise
// idem pour le shutdown
let shutdownPromise
// un timer pour shutdown après le dernier test (mais pas tout de suite
// au cas où un autre démarre juste après)
let lassiShutdownTimer

const getSupertestAgent = () => agent(lassi.service('$rail').get())

/**
 * Boot sesalab (sans flush ni load des fixtures)
 * @return {Promise}
 */
const boot = () => {
  if (!lassiBootstrapPromise) {
    lassiBootstrapPromise = new Promise((resolve, reject) => {
      lassi.bootstrap(global.app, (err) => {
        if (err) return reject(err)
        resolve()
      })
    })
  }
  return lassiBootstrapPromise
}

/**
 * Vérifie que les sésathèques de la config répondent
 * @return {Promise}
 */
const checkBiblis = () => {
  const checkBibli = async (url) => {
    try {
      const response = await fetch(url)
      if (response.ok) return
    } catch (error) {
      // on affiche l'erreur que si c'est pas du ECONNREFUSED
      if (error.code !== 'ECONNREFUSED') console.error(error)
    }
    throw Error(`La sésathèque ne répond pas sur ${url} (pour test:browser il faut lancer un pnpm start:testBoth par ailleurs dans le dossier sesatheque)`)
  }
  const urls = settings.application.sesatheques.map(s => s.baseUrl)
  return Promise.all(urls.map(checkBibli))
}

/**
 * Vide la base
 * @return {Promise}
 */
const flush = () => {
  if (isDebug) console.log('flush DB')
  return Promise.all(Object.values(lassi.service('$entities').definitions())
    // On evite de vider la table d'update
    .filter(entityDef => entityDef.name !== 'LassiUpdate')
    // On ne fait pas de flush() pour préserver les index
    .map(entityDef => entityDef.getCollection().deleteMany({}))
  )
}

/**
 * Reset la base avec les fixtures
 * @return {Promise<undefined>}
 */
const resetDb = () => flush().then(() => promisify(loadFixtures))

/**
 * Shutdown le sesalab de test
 * @return {Promise}
 */
const shutdown = () => {
  if (!shutdownPromise) {
    shutdownPromise = new Promise((resolve) => {
      if (!lassiBootstrapPromise) return resolve()
      lassiBootstrapPromise = undefined
      lassi.on('shutdown', resolve)
      lassi.shutdown()
    })
  }
  return shutdownPromise
}

/**
 * Fonction à appeler juste après un describe pour ajouter boot en before et shutdown en after,
 * avec également resetDb en beforeEach (sauf options contraire)
 * et stub des mails si on passe un array en options.mails
 * @param {Object} options
 * @param {boolean} [options.doNotResetDb=false] passer true pour ne pas faire de resetDb en beforeEach
 * @param {boolean} [options.doCheckBiblis=false] passer true pour vérifier que les biblis répondent
 * @param {Object[]} [options.mails] Si fourni avec un array, le vide en beforeEach et le complète avec les mails qui seraient envoyés (send & sendMail)
 */
const addHooks = ({ doCheckBiblis, doNotResetDb, mails } = {}) => {
  /*
   * Enregistre les hooks mocha
   */

  // Empêche les tests de démarrer avant la fin du bootstrap
  before(async function () {
    // 10s suffisent pas toujours à CircleCI
    this.timeout(10000)
    if (lassiShutdownTimer) clearTimeout(lassiShutdownTimer)
    await boot()
    if (!doNotResetDb) {
      await resetDb()
    }
    if (doCheckBiblis) {
      await checkBiblis()
    }
  })

  // on coupe dans 100ms, sauf si un autre test bootstrap avant
  after(() => {
    lassiShutdownTimer = setTimeout(shutdown, 100)
  })

  // @todo: (perf) ajouter un évenement sur le .store() de lassi pour ne reset la BDD que sur les tests modifiant les données
  // Pour l'instant, vu le nombre de tests on peut encore reset à chaque test

  // On utilise des promises et non des callbacks beforeEach((done) => loadFixtures(done)) car beforeEach/afterEach
  // ne passe pas de callback quand lancé par Webdriver.io:
  // https://github.com/webdriverio/webdriverio/issues/1245

  // pour playwright on veut pas de flush à chaque fois, c'est lui qui le gère
  if (!doNotResetDb) {
    // plutôt que de flush en after et load en before,
    // beforeEach(() => promisify(loadFixtures))
    // afterEach(() => promisify(flush))
    // on fait flush+load en before, ça permet de pouvoir aller voir en base après un échec
    beforeEach(resetDb)
  }

  if (Array.isArray(mails)) {
    // Stub emails
    beforeEach(() => {
      // on vide se tableau à chaque test, mais sans réaffecter la ref
      while (mails.length) mails.pop()
      const $mail = lassi.service('$mail')
      // pour la méthode send
      sinon.stub($mail, 'send').callsFake((to, subject, template, variables, cb) => {
        mails.push({ to, subject, template, variables })
        if (cb) cb()
      })
      // et la méthode sendMail
      sinon.stub($mail, 'sendMail').callsFake((opts, cb) => {
        mails.push(opts)
        if (cb) cb()
      })
    })

    afterEach(() => {
      const $mail = lassi.service('$mail')
      $mail.send.restore()
      $mail.sendMail.restore()
    })
  }
}

module.exports = {
  addHooks,
  boot,
  checkBiblis,
  flush,
  getSupertestAgent,
  logDir,
  resetDb,
  shutdown
}
