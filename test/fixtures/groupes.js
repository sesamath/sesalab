module.exports = [
  {
    utilisateurs: [],
    structure: '1',
    niveau: '2',
    nom: 'ma classe',
    isClass: true,
    oid: '1'
  },
  {
    utilisateurs: [],
    structure: '2',
    niveau: '2',
    nom: 'ma classe dans mon autre école',
    isClass: true,
    oid: '2'
  },
  {
    utilisateurs: [],
    structure: '1',
    niveau: '2',
    nom: 'classe supprimée',
    isClass: true,
    oid: 'classe-supprime-oid',
    __deletedAt: new Date()
  },
  {
    utilisateurs: ['eleve-valide-oid'],
    owner: 'prof-valide-oid',
    niveau: '2',
    nom: 'groupe supprimé',
    isClass: false,
    oid: 'groupe-supprime-oid',
    __deletedAt: new Date()
  }
]
