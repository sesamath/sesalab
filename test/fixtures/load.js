const flow = require('an-flow')

const sequences = require('./sequences')
const structures = require('./structures')
const groupes = require('./groupes')
const utilisateurs = require('./utilisateurs')
const utilisateurAcces = require('./utilisateurAcces')

module.exports = (cb) => {
  const Structure = lassi.service('Structure')
  const Groupe = lassi.service('Groupe')
  const Utilisateur = lassi.service('Utilisateur')
  const UtilisateurAcces = lassi.service('UtilisateurAcces')
  const Sequence = lassi.service('Sequence')

  const imports = [
    {
      data: structures,
      Entity: Structure
    },
    {
      data: groupes,
      Entity: Groupe
    },
    {
      data: utilisateurs,
      Entity: Utilisateur
    },
    {
      data: utilisateurAcces,
      Entity: UtilisateurAcces
    },
    {
      data: sequences,
      Entity: Sequence
    }
  ]

  flow(imports)
    .seqEach(function ({ data, Entity }) {
      flow(data)
        .seqEach(function (entityData) {
          Entity.create(entityData).store(this)
        })
        .done(this)
    })
    .done(cb)
}
