const moment = require('moment')
const defaultTimezone = require('sesalab-commun/constants').DEFAULT_TIMEZONE
// on vérifie ça au moins dans les tests, ça évite de le faire au runtime ailleurs dans le code
if (!defaultTimezone) throw new Error('DEFAULT_TIMEZONE manquant')

module.exports = [
  {
    ajoutElevesAuthorises: true,
    ajoutFormateursInterdit: false,
    datePurge: moment().add(1, 'year').toDate(),
    pays: 'Fr',
    ville: 'Nantes',
    nom: 'Mon école',
    code: 'AAAAA',
    oid: '1',
    timezone: defaultTimezone
  },
  {
    ajoutElevesAuthorises: true,
    ajoutFormateursInterdit: false,
    datePurge: moment().add(1, 'year').toDate(),
    pays: 'Fr',
    ville: 'Nantes',
    nom: 'Mon autre école',
    code: 'AAAAB',
    oid: '2',
    timezone: defaultTimezone
  },
  {
    ajoutElevesAuthorises: false,
    ajoutFormateursInterdit: true,
    datePurge: moment().add(1, 'year').toDate(),
    pays: 'Fr',
    ville: 'Nantes',
    nom: 'Mon école privée',
    code: 'AAAAC',
    oid: '3',
    timezone: defaultTimezone
  }
]
