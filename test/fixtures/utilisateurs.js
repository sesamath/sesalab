// Hash for password 'azerty44' and salt '1234'
const hashedDefaultPassword = 'd48801eba138ae6fe607ce2e5e8a1773dfd905bd526f05b4a5e22f2bc29b3550'
let hashedPassword
try {
  const $crypto = lassi.service('$crypto')
  hashedPassword = $crypto.hash('azerty44')
} catch (error) {
  hashedPassword = hashedDefaultPassword
}

module.exports = [
  {
    profil: {
      name: 'ecole',
      values: []
    },
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        validatedBy: 'FIXTURE',
        accepted: true
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        accepted: true
      }
    },
    nom: 'Prof',
    prenom: 'Valide',
    mail: 'prof-valide@sesamath.net',
    login: 'prof-valide',
    password: hashedPassword,
    type: 1,
    created: new Date(),
    structures: ['1'],
    oid: 'prof-valide-oid',
    cguAccepted: true,
    bilanWithSymbols: false,
    fontSize: 'default'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: null
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: null
      }
    },
    nom: 'Prof',
    prenom: 'Non-Valide-Email',
    mail: 'prof-non-valide-email@sesamath.net',
    login: 'prof-non-valide-email',
    password: 'aaaaaaa',
    type: 1,
    created: new Date(),
    structures: ['1'],
    oid: 'prof-non-valide-email-oid',
    cguAccepted: false,
    bilanWithSymbols: false,
    fontSize: 'default'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        validatedBy: 'FIXTURE',
        accepted: false
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        accepted: true
      }
    },
    nom: 'Prof',
    prenom: 'Non-Valide-Structure', // non accepté dans la structure
    mail: 'prof-non-valide-structure@sesamath.net',
    login: 'prof-non-valide-structure',
    password: 'aaaaaaa',
    type: 1,
    created: new Date(),
    structures: ['1'],
    oid: 'prof-non-valide-structure-oid',
    cguAccepted: false,
    bilanWithSymbols: false,
    fontSize: 'default'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        validatedBy: 'FIXTURE',
        accepted: true
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        accepted: true
      }
    },
    classe: '1',
    nom: 'Eleve',
    prenom: 'Valide',
    mail: 'eleve-valide@sesamath.net',
    login: 'eleve-valide',
    password: hashedPassword,
    type: 0,
    structures: ['1'],
    created: new Date(),
    oid: 'eleve-valide-oid'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: null
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: null
      }
    },
    classe: '1',
    nom: 'Eleve',
    prenom: 'Non-Valide-Email',
    mail: 'eleve-non-valide-email@sesamath.net',
    login: 'eleve-non-valide-email',
    password: 'aaaaaaa',
    type: 0,
    structures: ['1'],
    created: new Date(),
    oid: 'eleve-non-valide-email-oid'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: null
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        accepted: true
      }
    },
    classe: '1',
    nom: 'Eleve',
    prenom: 'Non-Valide-Structure',
    mail: 'eleve-non-valide-structure@sesamath.net',
    login: 'eleve-non-valide-structure',
    password: 'aaaaaaa',
    type: 0,
    structures: ['1'],
    created: new Date(),
    oid: 'eleve-non-valide-structure-oid'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        validatedBy: 'FIXTURE',
        accepted: true
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        accepted: true
      }
    },
    classe: '1',
    nom: 'Eleve',
    prenom: 'External',
    mail: 'eleve-external@sesamath.net',
    login: 'eleve-external',
    externalMech: 'sso',
    externalId: '1234',
    password: 'aaaaaaa',
    type: 0,
    structures: ['1'],
    created: new Date(),
    oid: 'eleve-external-oid'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        validatedBy: 'FIXTURE',
        accepted: true
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        accepted: true
      }
    },
    classe: '1',
    nom: 'Eleve',
    prenom: 'NationalId',
    mail: 'eleve-nationalid@sesamath.net',
    login: 'nationalid.eleve',
    password: 'aaaaaaa',
    nationalId: '42',
    type: 0,
    structures: ['1'],
    created: new Date(),
    oid: 'eleve-nationalid-oid'
  },
  {
    validators: {
      account: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        validatedBy: 'FIXTURE',
        accepted: true
      },
      mail: {
        token: 'abc',
        createdAt: new Date(),
        responseDate: new Date(),
        accepted: true
      }
    },
    classe: '1',
    nom: 'Eleve',
    prenom: 'Supprime',
    mail: 'eleve-supprime@sesamath.net',
    login: 'supprime.eleve',
    password: 'aaaaaaa',
    type: 0,
    structures: ['1'],
    created: new Date(),
    oid: 'eleve-supprime-oid',
    __deletedAt: new Date()
  }
]
