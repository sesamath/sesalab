const { hasProp } = require('sesajstools')

/*
 *
 * ATTENTION :
 *
 * Ce fichier est utilisé par loadRessources docker/sesatheque/cli/loadRessources.js et est donc injecté dans le container
 * de la sésathèque tel quel. (cf. docker-compose.yml)
 * Il ne faut donc pas qu'il ait de dépendances avec d'autres fichiers issus de sesalab
 * ou alors il faudra revoir ce fonctionnement.
 */
/**
 * Retourne une liste de ressources à injecter dans la sésathèque principale
 * @param bibliBaseId
 */
module.exports = function getRessources (bibliBaseId) {
  function add (exo) {
    // on complète ce qu'on peut déduire
    exo.rid = `${bibliBaseId}/${exo.oid}`
    exo.restriction = 0
    exo.publie = true
    exo.langue = 'fra'
    if (!hasProp(exo, 'resume')) exo.resume = ''
    if (!hasProp(exo, 'description')) exo.description = ''
    if (!hasProp(exo, 'commentaires')) exo.commentaires = ''
    // on est pas obligé de préciser origine/idOrigine quand y'a pas d'oid, mais avec (update supposé)
    // il en faut un
    exo.origine = bibliBaseId
    exo.idOrigine = exo.oid
    if (exo.type === 'arbre') exo.categories = [8]
    else if (!exo.categories) exo.categories = [9] // à déterminer, mais faut l'indiquer
    // on stocke
    ressources[exo.oid] = exo

    // et on cast en Ref (pour usage comme enfant par les arbres suivants)
    aliases[exo.oid] = {
      aliasOf: exo.rid,
      type: exo.type,
      titre: exo.titre,
      resume: exo.resume,
      description: exo.description,
      publie: true,
      categories: exo.categories
    }
  }

  if (!bibliBaseId) throw new Error('Il faut fournir un baseId pour la sésathèque à utiliser')

  // les ressources…
  const ressources = {}

  // … transformées en Ref (pour les mettre dans les arbres suivants)
  const aliases = {}

  // exo mathenpoche rapide à faire
  add({
    oid: 'em207',
    origine: 'em',
    idOrigine: '208',
    type: 'em',
    titre: 'Entiers et espaces',
    resume: 'Ajouter les espaces pour écrire convenablement un nombre entier.<br />Exemple : "65432 = 65 432"',
    commentaires: '10 questions.<br>Tirage aléatoire des nombres. Mais ils sont de plus en plus grands au fil des questions',
    parametres: {
      mep_langue_id: 'fr',
      swf_id: '208',
      mep_modele: 'mep1',
      old: '6N1s1ex1',
      id_originel: '208',
      aide_id: '189'
    },
    niveaux: [],
    categories: [7],
    typePedagogiques: [9, 2],
    typeDocumentaires: [9],
    relations: [
      [13, 'sesabibli/189'],
      [16, 'sesabibli/208']
    ],
    dateCreation: '2017-06-30T07:55:24.960Z',
    dateMiseAJour: '2017-08-02T14:22:29.518Z'
  })

  // exo j3p
  add({
    oid: 'j3p32476',
    type: 'j3p',
    titre: 'Nombres sympathiques',
    commentaires: 'Somme ou produit (paramétrable) de termes se regroupant astucieusement.',
    parametres: {
      g: [[
        1,
        'cm2exN2_12',
        [{
          pe: '>=0',
          nn: 'fin',
          conclusion: 'FIN'
        }, {
          nbrepetitions: 5,
          indication: '',
          limite: '',
          nbchances: 2,
          operation: '+',
          nbtermes: 3,
          supersympathiques: true,
          max_du_dernier: 9,
          chamboule: true,
          aide_seconde_chance: true,
          sommes: [20, 30, 40, 50, 60, 70, 80, 90, 100]
        }]
      ]]
    },
    niveaux: [],
    categories: [7],
    typePedagogiques: [9, 2],
    typeDocumentaires: [9],
    dateCreation: '2015-06-11T14:59:40.010Z',
    dateMiseAJour: '2017-08-02T14:23:43.494Z'
  })

  // exo calculatice
  add({
    oid: 'ecjs50115',
    origine: 'calculatice',
    idOrigine: '0',
    type: 'ecjs',
    titre: 'Quadricalc niveau 1',
    resume: '',
    description: '',
    commentaires: '',
    parametres: {
      options: {
        choixVitesse: 1400,
        totalQuestion: 1,
        temps_question: 0,
        choixOperateur: [
          1
        ],
        totalTentative: '15',
        plageA: '2-9',
        nom: 'quadricalc-as3',
        plageB: '2-4',
        totalEssai: 1
      },
      fichierjs: 'quadricalc'
    },
    niveaux: [
      '11'
    ],
    categories: [
      7
    ],
    typePedagogiques: [
      9
    ],
    typeDocumentaires: [
      9
    ],
    relations: [],
    auteurs: [],
    auteursParents: [],
    contributeurs: [],
    groupes: [],
    groupesAuteurs: [],
    langue: 'fra',
    publie: true,
    restriction: 0,
    dateCreation: '2015-10-15T16:01:18.611Z',
    dateMiseAJour: '2017-08-18T21:20:47.433Z',
    version: 1,
    inc: 0,
    indexable: true,
    archiveOid: '0'
  })

  // pour ajouter d'autres exos, faire une recherche sur la sesathèque, description puis json,
  // et ajouter des add ici

  // on ajoute nos arbres, en partant de ceux qui seront inclus par d'autres (et doivent donc exister avant)
  add({
    oid: 'sesalabBar',
    titre: 'Ressources à boire',
    type: 'arbre',
    resume: 'Ressources spécifiques bar',
    enfants: [aliases.em207, aliases.j3p32476]
  })
  add({
    oid: 'sesalabFoo',
    titre: 'Folles ressources',
    type: 'arbre',
    resume: 'Ressources spécifiques foo',
    enfants: [{
      titre: 'Un dossier fou',
      type: 'arbre',
      enfants: [{
        titre: 'sous-dossier',
        type: 'arbre',
        enfants: [aliases.em207, aliases.ecjs50115]
      }, {
        titre: 'sous-dossier vide',
        type: 'arbre',
        enfants: []
      }]
    }]
  })
  add({
    oid: 'sesalabExosVrac',
    titre: 'Ressources pas très bien rangées',
    type: 'arbre',
    resume: 'Des ressources en vrac',
    enfants: [{
      titre: 'Un dossier à nommer',
      type: 'arbre',
      enfants: [aliases.em207]
    }]
  })
  add({
    oid: 'sesalabExosInteractifs',
    titre: 'Exercices interactifs',
    type: 'arbre',
    resume: 'Un peu pauvre pour le moment',
    enfants: [{
      titre: 'Avec flash',
      type: 'arbre',
      enfants: [aliases.em207]
    }, {
      titre: 'Sans flash',
      type: 'arbre',
      enfants: [aliases.j3p32476, aliases.ecjs50115]
    }]
  })
  add({
    oid: 'sesalabAutonomie',
    titre: 'Exercices en autonomie',
    type: 'arbre',
    resume: 'L’arbre des exos dispos en autonomie',
    enfants: [
      aliases.sesalabExosInteractifs,
      aliases.sesalabExosVrac,
      aliases.sesalabFoo
    ]
  })
  add({
    oid: 'sesalabExosInteractifsEcole',
    titre: 'Exercices interactifs',
    type: 'arbre',
    resume: 'Des exos interactifs pour l’école',
    enfants: [aliases.em207, aliases.j3p32476]
  })
  add({
    oid: 'sesalabCollege',
    titre: 'Ressources pour le collège',
    type: 'arbre',
    resume: 'La liste des arbres sélectionnés par défaut pour le profil collège',
    enfants: [
      aliases.sesalabExosInteractifs,
      aliases.sesalabFoo
    ]
  })
  add({
    oid: 'sesalabEcole',
    titre: 'Ressources pour l’école',
    type: 'arbre',
    resume: 'La liste des arbres sélectionnés par défaut pour le profil école',
    enfants: [
      aliases.sesalabExosInteractifs,
      aliases.sesalabExosVrac
    ]
  })
  add({
    oid: 'sesalabAll',
    titre: 'Arbres affichables dans sesalab',
    type: 'arbre',
    resume: 'La liste de tous les arbres affichables dans sesalab',
    enfants: [
      aliases.sesalabEcole,
      aliases.sesalabCollege,
      aliases.sesalabExosInteractifs,
      aliases.sesalabAutonomie,
      aliases.sesalabExosVrac,
      aliases.sesalabFoo,
      aliases.sesalabBar
    ]
  })
  return ressources
}
