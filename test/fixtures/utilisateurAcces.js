const now = Date.now()
const dayMs = 24 * 3_600_000
let i = 1

module.exports = [
  // structure 1
  {
    utilisateur: 'eleve-external-oid',
    type: 0,
    structure: '1',
    // y'a 3j
    timestamp: now - 3 * dayMs,
    origine: 'sso',
    departement: 44,
    pays: 'France',
    academie: 1,
    oid: `ua${i++}`
  },
  {
    utilisateur: 'eleve-external-oid',
    type: 0,
    structure: '1',
    // y'a 2j
    timestamp: now - 2 * dayMs,
    origine: 'sso',
    departement: 44,
    pays: 'France',
    academie: 1,
    oid: `ua${i++}`
  },
  {
    utilisateur: 'eleve-external-oid',
    type: 0,
    structure: '1',
    // hier
    timestamp: now - dayMs,
    origine: 'sso',
    departement: 44,
    pays: 'France',
    academie: 1,
    oid: `ua${i++}`
  },
  // structure foo
  {
    utilisateur: 'eleve2',
    type: 0,
    structure: 'foo',
    // hier
    timestamp: now - dayMs,
    origine: 'sso',
    departement: 12,
    pays: 'France',
    academie: 12,
    oid: `ua${i++}`
  },
  {
    utilisateur: 'prof1',
    type: 1,
    structure: 'foo',
    // y'a 3j
    timestamp: now - 3 * dayMs,
    origine: 'sso',
    departement: 12,
    pays: 'France',
    academie: 12,
    oid: `ua${i++}`
  },
  // structure bar
  {
    utilisateur: 'eleve3',
    type: 0,
    structure: 'bar',
    // y'a 3j
    timestamp: now - 3 * dayMs,
    oid: `ua${i++}`
  }
]
