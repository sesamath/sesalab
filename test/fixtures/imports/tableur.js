const eleves = [
  {
    login: 'import.eleve1',
    mail: '',
    nom: 'IMPORT',
    password: 'abcdef1',
    prenom: 'Eleve 1',
    classeNom: '3E A',
    structures: ['1'],
    type: 0
  },
  {
    login: 'import.eleve2',
    mail: '',
    nom: 'IMPORT',
    password: 'abcdef2',
    prenom: 'Eleve 2',
    classeNom: '3E D',
    structures: ['1'],
    type: 0
  }
]

module.exports = {
  eleves
}
