module.exports = [
  {
    sousSequences: [
      {
        nom: 'sous-séquence A',
        eleves: [],
        serie: []
      }
    ],
    publicAmis: false,
    public: false,
    groupes: [],
    structure: '1',
    owner: 'prof-valide-oid',
    nom: 'Additions',
    oid: 'sequence-additions'
  }
]
