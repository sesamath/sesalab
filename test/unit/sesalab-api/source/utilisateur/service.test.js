'use strict'
const { addHooks } = require('test/app')

let $utilisateur

describe('$utilisateur', () => {
  addHooks()
  before(() => {
    $utilisateur = lassi.service('$utilisateur')
  })

  const login = 'login'
  describe('getPasswordError', () => {
    it('Retourne une erreur en cas de mot de passe incorrect', () => {
      expect(!$utilisateur.getPasswordError('', login)).to.equal(false)
      expect(!$utilisateur.getPasswordError('login', login)).to.equal(false)
      expect(!$utilisateur.getPasswordError('short', login)).to.equal(false)
      expect(!$utilisateur.getPasswordError('password', login)).to.equal(false)
      expect(!$utilisateur.getPasswordError('123456', login)).to.equal(false)
      expect(!$utilisateur.getPasswordError('password123456', login)).to.equal(true)
    })
  })
  describe('generateUnPassword', () => {
    it('Génère un mot de passe aléatoire au bon format', () => {
      expect(!$utilisateur.getPasswordError($utilisateur.generateUnPassword(), login)).to.equal(true)
    })
  })
})
