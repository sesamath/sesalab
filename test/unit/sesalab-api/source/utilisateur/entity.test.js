'use strict'
const flow = require('an-flow')
const { addHooks } = require('test/app')
const { createEleve, createProf } = require('test/helpers/utilisateurs')

let Utilisateur

describe('Utilisateur', () => {
  addHooks()

  before(() => {
    if (!Utilisateur) Utilisateur = lassi.service('Utilisateur')
  })
  describe('toJSON', () => {
    it('masque le password lorsqu’on jsonifie un utilisateur', (done) => {
      // On teste sur un Utilisateur avant et après store en DB
      const utilisateur = Utilisateur.create({ type: 0, login: 'test', password: 'secret' })
      expect(JSON.parse(JSON.stringify(utilisateur)).password).to.equal(undefined)

      Utilisateur.match('login').equals('eleve-valide').grabOne((err, dbUtilisateur) => {
        if (err) return done(err)
        expect(JSON.parse(JSON.stringify(dbUtilisateur)).password).to.equal(undefined)
        done()
      })
    })
  })

  describe('beforeStore', () => {
    describe('doublon de login prof', () => {
      // pas terrible de stuber la console, mais tant que ça écrit dedans dans les contrôleurs / services
      // faut ça pour pas pourrir la sortie des tests
      let consoleErrorStub
      beforeEach(() => {
        consoleErrorStub = sinon.stub(console, 'error')
      })
      afterEach(() => {
        consoleErrorStub.restore()
      })
      it('autorise un un login différent dans la même structure', (done) => {
        createProf(Utilisateur, { login: 'nouveau' }, (err) => {
          expect(err).to.equal(null)
          // pourquoi ce test provoque un timeout si qqun écrit dans appLog ????
          expect(consoleErrorStub).to.not.have.been.called
          done()
        })
      })
      it('throw une erreur si login déjà existant, même sur une autre structure', (done) => {
        // (fixtures) prof-valide existe déjà sur la structure 1
        createProf(Utilisateur, { login: 'prof-valide', structures: ['2'] }, (err) => {
          expect(err.message).to.equal('Impossible d’enregistrer pour cause de doublon (valeur "prof-valide" en doublon pour Utilisateur.globalUniqueLogin)')
          done()
        })
      })
      it('throw une erreur si login déjà existant, même si non validé', (done) => {
        // (fixtures) prof-non-valide-structure existe déjà sur la structure 1
        createProf(Utilisateur, { login: 'prof-non-valide-structure', structures: ['2'] }, (err) => {
          expect(err.message).to.equal('Impossible d’enregistrer pour cause de doublon (valeur "prof-non-valide-structure" en doublon pour Utilisateur.globalUniqueLogin)')
          done()
        })
      })
    })

    describe('doublon de login eleve', () => {
      describe('dans une même structure', () => {
        let consoleErrorStub
        beforeEach(() => {
          consoleErrorStub = sinon.stub(console, 'error')
        })
        afterEach(() => {
          consoleErrorStub.restore()
        })
        it('autorise un élève avec un login différent', (done) => {
          createEleve(Utilisateur, { login: 'nouveau' }, (err) => {
            expect(err).to.equal(null)
            expect(consoleErrorStub).to.not.have.been.called
            done()
          })
        })
        it('throw une erreur si il existe un élève valide avec le même login', (done) => {
          // (fixtures) eleve-valide existe déjà sur la structure 1
          createEleve(Utilisateur, { login: 'eleve-valide' }, (err) => {
            expect(err.message).to.equal('Impossible d’enregistrer pour cause de doublon (valeur "1-eleve-valide" en doublon pour Utilisateur.structureUniqueLogin)')
            done()
          })
        })
        it('throw une erreur si il existe un élève valide avec le même externalMech/externalId', (done) => {
          // (fixtures) sso/1234 existe déjà sur la structure 1
          createEleve(Utilisateur, { login: 'nouveau', externalMech: 'sso', externalId: '1234' }, (err) => {
            try {
              expect(err.message).to.match(/sso\/1234 existe déjà \(eleve-external-oid\)/)
              expect(consoleErrorStub).to.have.been.calledOnce
              done()
            } catch (error) {
              done(error)
            }
          })
        })
        it('throw une erreur si login existant dans la structure, même si non-validé', (done) => {
          // (fixtures) eleve-non-valide-structure existe déjà sur la structure 1
          createEleve(Utilisateur, { login: 'eleve-non-valide-structure' }, (err) => {
            expect(err.message).to.equal('Impossible d’enregistrer pour cause de doublon (valeur "1-eleve-non-valide-structure" en doublon pour Utilisateur.structureUniqueLogin)')
            done()
          })
        })

        describe('avec un eleve supprimé', () => {
          const eleveSupprimeOid = 'eleve-supprime-oid'

          it('autorise de créer un élève avec le même login', (done) => {
            createEleve(Utilisateur, { login: 'supprime.eleve' }, (err) => {
              expect(err).to.equal(null)
              expect(consoleErrorStub).to.not.have.been.called
              done()
            })
          })

          it('autorise d’assigner son login à un élève existant', (done) => {
            flow()
              .seq(function () {
                Utilisateur.match('login').equals('eleve-valide').grabOne(this)
              })
              .seq(function (eleve) {
                eleve.login = 'supprime.eleve'
                eleve.store((err) => {
                  expect(err).to.equal(null)
                  done()
                })
              })
              .catch(done)
          })

          it('empêche la restauration de l’élève supprimé si il existe un élève avec son login', (done) => {
            flow()
              .seq(function () {
                createEleve(Utilisateur, { login: 'supprime.eleve' }, this)
              })
              .seq(function (eleve) {
                Utilisateur.match('oid').equals(eleveSupprimeOid).includeDeleted().grabOne(this)
              })
              .seq(function (eleveSupprime) {
                eleveSupprime.markToRestore()
                eleveSupprime.store((err) => {
                  expect(err.message).to.equal('Impossible d’enregistrer pour cause de doublon (valeur "1-supprime.eleve" en doublon pour Utilisateur.structureUniqueLogin)')
                  done()
                })
              })
              .catch(done)
          })
        })
      })

      describe('dans une autre structure', () => {
        it('autorise un élève avec un login existant dans une autre structure', (done) => {
          // (fixtures) eleve-valide existe déjà sur la structure 1
          createEleve(Utilisateur, { login: 'eleve-valide', structures: ['2'], classe: '2' }, (err) => {
            expect(err).to.equal(null)
            done()
          })
        })
      })
    })
  })

  describe('Schema validation', () => {
    it('valide un prof', (done) => {
      flow().seq(function () {
        createProf(Utilisateur, {}, this)
      }).seq(function (prof) {
        prof.isValid((err) => {
          try {
            expect(err).to.not.exist
            this(null, prof)
          } catch (error) {
            done(error)
          }
        })
      }).seq(function (prof) {
        prof.mail = 'mauvais mail'
        prof.isValid((err) => {
          try {
            expect(err.message).to.match(/Utilisateur\/mail Impossible de sauvegarder un utilisateur avec un mail invalide/)
            done()
          } catch (error) {
            done(error)
          }
        })
      }).catch(done)
    })
  })
})
