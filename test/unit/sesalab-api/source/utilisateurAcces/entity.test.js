'use strict'
const flow = require('an-flow')
const { addHooks } = require('test/app')

let UtilisateurAcces

describe('Utilisateur', () => {
  addHooks()

  before(() => {
    if (!UtilisateurAcces) UtilisateurAcces = lassi.service('UtilisateurAcces')
  })

  describe('Schema validation', () => {
    it('valide un accès', (done) => {
      flow().seq(function () {
        UtilisateurAcces.create({
          utilisateur: 'foo',
          type: 0,
          structure: '1',
          timestamp: Date.now()
        }).store(this)
      }).seq(function (ua) {
        expect(ua.utilisateur).equals('foo')
        ua.departement = 'bar'
        ua.store((err) => {
          expect(err?.message).to.match(/UtilisateurAcces\/departement doit être de type integer/)
          done()
        })
      }).catch(done)
    })
  })
})
