const flow = require('an-flow')

const { addHooks } = require('test/app')

let $utilisateurAcces

const now = Date.now()
const dayMs = 24 * 3_600_000
const checks = [
  // pour chaque test on met l'objet filters et le nb attendu
  [{ after: now - 1.5 * dayMs, departement: 44 }, 1],
  [{ after: now - 5 * dayMs, structure: '1' }, 3],
  [{ after: now - 5 * dayMs, academie: 1 }, 3],
  [{ after: 1, structure: 'foo' }, 2],
  [{ after: 1, structure: 'foo', type: 1 }, 1]
]

describe('$utilisateurAcces', () => {
  addHooks()

  before(() => {
    $utilisateurAcces = lassi.service('$utilisateurAcces')
  })

  describe('count', () => {
    it('Remonte le bon nb d’accès avec différents filtres', (done) => {
      flow(checks)
        .seqEach(function ([filters, nb], index) {
          const nextCheck = this
          $utilisateurAcces.count(filters, (err, n) => {
            if (err) return nextCheck(err)
            try {
              expect(n).to.equals(nb, `pb avec le test d’index ${index}`)
              nextCheck()
            } catch (error) {
              nextCheck(error)
            }
          })
        })
        .done(done)
    })
    it('plante si on ne passe ni structure ni critère géo', (done) => {
      $utilisateurAcces.count({ after: now - 3 * dayMs, type: 1 }, (err, n) => {
        try {
          expect(err.message).to.match(/filtre géographique ou utilisateur manquant/)
          done()
        } catch (error) {
          done(error)
        }
      })
    })
    it('plante si on ne passe pas after', (done) => {
      const filter = checks[0][0]
      delete filter.after
      $utilisateurAcces.count(filter, (err, n) => {
        try {
          expect(err.message).to.match(/after manquant/)
          done()
        } catch (error) {
          done(error)
        }
      })
    })
  })
})
