'use strict'

const { addHooks } = require('test/app')

let $crypto
describe('$crypto', () => {
  addHooks()

  before(() => {
    $crypto = lassi.service('$crypto')
  })

  describe('hash', () => {
    it('Permet de hasher un password', () => {
      const password = 'foo ! # bar $baz'
      const passHashed = $crypto.hash(password)
      expect(typeof passHashed).to.equal('string')
      expect(passHashed.length).to.equal(64)
      expect(passHashed).to.equal($crypto.hash(password))
    })
  })

  describe('token', () => {
    it('Renvoie un token', () => {
      const token1 = $crypto.token()
      const token2 = $crypto.token()
      expect(typeof token1).to.equal('string')
      expect(token2.length).to.equal(128)
      expect(token1).not.to.equal(token2)
    })
  })

  describe('fromIntToCode', () => {
    it('Transforme un entier', () => {
      expect($crypto.fromIntToCode(0)).equal('AAAAA')
      expect($crypto.fromIntToCode(4)).to.equal('AAAAE')
      expect($crypto.fromIntToCode(26)).equal('AAABA')
    })
    it('Gère les types et les bornes', () => {
      const notNumber = () => $crypto.fromIntToCode('string')
      expect(notNumber).to.throw(TypeError) // Cas d'un paramètre qui n'est pas un nombre
      const decimal = () => $crypto.fromIntToCode(3.2)
      expect(decimal).to.throw(TypeError) // Cas d'un décimal
      const negativeNumber = () => $crypto.fromIntToCode(-1)
      expect(negativeNumber).to.throw(RangeError) // Cas d'un nombre négatif
      const bigNumber = () => $crypto.fromIntToCode(Math.pow(26, 5))
      expect(bigNumber).to.throw(RangeError) // Cas d'un nombre supérieur au macimum autorisé
    })
  })

  describe('fromCodeToInt', () => {
    it('Transforme un code en entier', () => {
      expect($crypto.fromCodeToInt('AAAAA')).to.equal(0)
      expect($crypto.fromCodeToInt('aAC')).to.equal(2)
      expect($crypto.fromCodeToInt('AAAAE')).to.equal(4)
      expect($crypto.fromCodeToInt('AAABA')).to.equal(26)
    })
    it('Gère les types et les bornes', () => {
      const notString = () => $crypto.fromCodeToInt(42)
      expect(notString).to.throw(TypeError) // Cas d'un paramètre qui n'est pas une chaîne de caractère
      const longString = () => $crypto.fromCodeToInt('AAAAAA')
      expect(longString).to.throw(Error) // Cas d'une chaîne de caractère trop longue
    })
  })

  describe('fromCodeToInt et fromIntToCode', () => {
    it('Restent cohérents sur des grands nombres', () => {
      let k = 1
      while (k < 10000) {
        expect($crypto.fromCodeToInt($crypto.fromIntToCode(k))).to.equal(k)
        k += 11 // On avance de 11 en 11 :)
      }
    })
  })

  describe('incrementCode', () => {
    it('Incrémente', () => {
      expect($crypto.incrementCode('AAAAA')).to.equal('AAAAB')
      expect($crypto.incrementCode('AAAAZ')).to.equal('AAABA')
      expect($crypto.incrementCode('TuBaS')).to.equal('TUBAT')
    })
    it('Gère la borne limite', () => {
      const borneLimite = () => $crypto.incrementCode('ZZZZZ')
      expect(borneLimite).to.throw(RangeError) // Cas limite de création des structures
    })
  })
})
