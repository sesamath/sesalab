'use strict'

const flow = require('an-flow')
const { addHooks } = require('test/app')

let Resultat
let $resultat

const resultats = [
  {
    participants: ['1234', '456'],
    sequence: 'seq1',
    score: 0,
    ressource: { rid: 'sesabibli/42', type: 'j3p', titre: 'fake j3p' }
  },
  {
    participants: ['1234', '456'],
    sequence: 'seq1',
    score: 0.5,
    ressource: { rid: 'sesabibli/42', type: 'j3p', titre: 'fake j3p' }
  },
  {
    participants: ['456', '1234'],
    sequence: 'seq1',
    score: 1,
    ressource: { rid: 'sesabibli/42', type: 'j3p', titre: 'fake j3p' }
  },
  {
    participants: ['1234', '456'],
    sequence: 'seq2',
    score: 0,
    ressource: { rid: 'sesabibli/43', type: 'j3p', titre: 'fake j3p bis' }
  },
  {
    participants: ['456', '1234'],
    sequence: 'seq2',
    score: 1,
    ressource: { rid: 'sesabibli/44', type: 'mathgraph', titre: 'fake mathgraph' }
  },
  {
    participants: ['1234'],
    sequence: 'seq3',
    score: 1,
    ressource: { rid: 'sesabibli/44', type: 'mathgraph', titre: 'fake mathgraph' }
  },
  {
    participants: ['456'],
    sequence: 'seq4',
    score: 0,
    ressource: { rid: 'sesabibli/42', type: 'j3p', titre: 'fake j3p' }
  }
]

describe('$resultat', () => {
  addHooks()

  before(() => {
    Resultat = lassi.service('Resultat')
    $resultat = lassi.service('$resultat')
  })
  // faut recharger à chaque test car addHooks fait un reset DB avant chaque test
  beforeEach((done) => {
    const Sequence = lassi.service('Sequence')
    flow(resultats).seqEach(function (resultat) {
      Resultat.create(resultat).store(this)
    }).set([1, 2, 3, 4]).seqEach(function (i) {
      // et on crée 4 séquences bidon
      Sequence.create({ oid: `seq${i}`, nom: `Séquence${i}`, owner: `owner${i}` }).store(this)
    }).done(done)
  })

  describe('getSequences', () => {
    it('Remonte toutes les séquences si on en exclue aucune', (done) => {
      flow().seq(function () {
        $resultat.getSequences(['1234', '456'], [], this)
      }).seq(function (sequences) {
        expect(sequences).to.have.length(2)
        const sequencesOids = sequences.map(s => s.oid)
        expect(sequencesOids).to.contain('seq1')
        expect(sequencesOids).to.contain('seq2')

        // on recommence pour chaque élève, 1234…
        $resultat.getSequences(['1234'], [], this)
      }).seq(function (sequences) {
        expect(sequences).to.have.length(3)
        const sequencesOids = sequences.map(s => s.oid)
        expect(sequencesOids).to.contain('seq1')
        expect(sequencesOids).to.contain('seq2')
        expect(sequencesOids).to.contain('seq3')

        // …puis 456
        $resultat.getSequences(['456'], [], this)
      }).seq(function (sequences) {
        expect(sequences).to.have.length(3)
        const sequencesOids = sequences.map(s => s.oid)
        expect(sequencesOids).to.contain('seq1')
        expect(sequencesOids).to.contain('seq2')
        expect(sequencesOids).to.contain('seq4')
        done()
      }).catch(done)
    })

    it('Filtre correctement', (done) => {
      flow().seq(function () {
        $resultat.getSequences(['1234', '456'], ['seq1'], this)
      }).seq(function (sequences) {
        expect(sequences).to.have.length(1)
        expect(sequences[0].oid).to.equals('seq2')

        $resultat.getSequences(['1234', '456'], ['seq1', 'seq2'], this)
      }).seq(function (sequences) {
        expect(sequences).to.have.length(0)

        $resultat.getSequences(['1234'], ['seq2'], this)
      }).seq(function (sequences) {
        expect(sequences).to.have.length(2)
        const sequencesOids = sequences.map(s => s.oid)
        expect(sequencesOids).to.contain('seq1')
        expect(sequencesOids).to.contain('seq3')

        $resultat.getSequences(['456'], ['seq1', 'seq4'], this)
      }).seq(function (sequences) {
        expect(sequences).to.have.length(1)
        expect(sequences[0].oid).to.equals('seq2')
        done()
      }).catch(done)
    })
  })
})
