'use strict'
const flow = require('an-flow')
const { addHooks } = require('test/app')
const { createGroupe } = require('test/helpers/groupes')

let Groupe
let $settings

describe('Groupe', () => {
  addHooks()

  before(() => {
    Groupe = lassi.service('Groupe')
    $settings = lassi.service('$settings')
  })

  describe('Validation', () => {
    it('lève une erreur si le champ nom est vide', (done) => {
      flow(['', ' ', '\t']).seqEach(function () {
        createGroupe({ nom: '' }, (error) => {
          // faut un try/catch dans les cb sinon ça donne du UnhandledPromiseRejectionWarning quand expect throw
          try {
            expect(error.message).to.match(/Groupe\/nom doit avoir un champ "nom"/)
            done()
          } catch (error) {
            done(error)
          }
        })
      }).catch(done)
    })

    it('empêche les doublons, de noms même si la casse ou les accents diffère', (done) => {
      // Groupe "ma classe" dans les fixtures
      createGroupe({ nom: 'Ma Classe' }, (error, groupe) => {
        // que ce soit avec done ou Promise, faut des try/catch dans les cb internes
        try {
          expect(groupe).to.be.undefined
          expect(error.message).to.match(/Impossible d’enregistrer pour cause de doublon \(valeur "1---ma_classe" en doublon pour Groupe.uniqueNom\)/)
          createGroupe({ nom: ' ma claSSe ' }, (error, groupe) => {
            try {
              expect(groupe).to.be.undefined
              expect(error.message).to.match(/Impossible d’enregistrer pour cause de doublon \(valeur "1---ma_classe" en doublon pour Groupe.uniqueNom\)/)
              done()
            } catch (error) {
              done(error)
            }
          })
        } catch (error) {
          done(error)
        }
      })
    })

    describe('si le nombre maximum est atteint pour la structure', () => {
      let maxClasses

      // faut un beforeEach car y'a un afterEach(() => promisify(flushEntities))
      // en haut de la hiérarchie des tests
      beforeEach((done) => {
        // avant on comptait le nb de classes puis stub $settings.get('application.maxClasses')
        // pour renvoyer ce nombre, mais depuis 2018-12 cette valeur est lue au boot
        maxClasses = $settings.get('application.maxClasses')
        // on crée le nb max de classes dans une structure 42 (y'en a pas dans les fixtures)
        let i = 0
        const noms = []
        while (i < maxClasses) noms.push(`classe n°${i++}`)
        flow(noms).seqEach(function (nom) {
          createGroupe({ nom, structure: '42' }, this)
        }).seq(function () {
          done()
        }).catch(done)
      })

      after((done) => {
        Groupe.match('structure').equals('42').purge(done)
      })

      it('empêche de créer une classe', (done) => {
        createGroupe({ nom: 'Nouvelle Classe', structure: '42' }, (error, groupe) => {
          try {
            expect(error).to.be.a('error')
            const re = new RegExp(`Vous avez atteint le nombre maximum de classe par structure : ${maxClasses}`)
            expect(error.message).to.match(re)
            expect(groupe).to.be.undefined
            done()
          } catch (error) {
            done(error)
          }
        })
      })

      it('empêche de restaurer une classe', (done) => {
        flow().seq(function () {
          Groupe.match('structure').equals('42').count(this)
        }).seq(function (nb) {
          Groupe.match('nom').equals('classe supprimée').includeDeleted().grabOne(this)
        }).seq(function (groupeSupprime) {
          groupeSupprime.structure = '42'
          groupeSupprime.markToRestore()
          groupeSupprime.store((error, groupe) => {
            try {
              expect(error).to.be.a('error')
              const re = new RegExp(`Vous avez atteint le nombre maximum de classe par structure : ${maxClasses}`)
              expect(error.message).to.match(re)
              expect(groupe).to.be.undefined
              done()
            } catch (error) {
              done(error)
            }
          })
        }).catch(done)
      })
    })

    describe('onDuplicate', () => {
      // en fixture on a {niveau : '2', structure: '1', isClass: true}
      const classeData = { nom: 'classe supprimée ', niveau: '3' }
      const groupeData = { nom: 'groupe supprimé', niveau: '3' }
      const recupProps = ['isClass', 'structure', 'owner', 'utilisateurs']

      const checkGroupe = (data, groupe) => {
        Object.entries(data).forEach(([prop, value]) => {
          if (prop === 'nom') expect(groupe[prop]).to.equals(value.trim())
          else if (prop === 'utilisateurs') expect(groupe[prop]).to.deep.equals(value)
          else expect(groupe[prop]).to.equals(value)
        })
      }

      it('Récupère l’oid du groupe|classe supprimé si on en crée un du même nom', (done) => {
        let deletedClasse
        let deletedGroupe
        flow().seq(function () {
          Groupe.match('oid').equals('classe-supprime-oid').onlyDeleted().grabOne(this)
        }).seq(function (groupe) {
          deletedClasse = groupe
          recupProps.forEach(p => {
            if (deletedClasse[p]) classeData[p] = deletedClasse[p]
          })
          Groupe.create(classeData).store(this)
        }).seq(function (classe) {
          // c'est un classe valide
          expect(classe.isDeleted()).to.be.false
          // on a récupéré l'oid du groupe deleted mais conservé tout le reste
          expect(classe.oid).to.equals(deletedClasse.oid)
          checkGroupe(classeData, classe)
          // la classe deleted n'existe plus
          Groupe.match('oid').equals('classe-supprime-oid').onlyDeleted().grabOne(this)
        }).seq(function (classe) {
          expect(classe).to.be.undefined
          // la classe précédente est bien en bdd
          Groupe.match('oid').equals('classe-supprime-oid').grabOne(this)
        }).seq(function (classe) {
          expect(classe.isDeleted()).to.be.false
          expect(classe.oid).to.equals(deletedClasse.oid)
          checkGroupe(classeData, classe)

          // on recommence pour le groupe
          Groupe.match('oid').equals('groupe-supprime-oid').onlyDeleted().grabOne(this)
        }).seq(function (groupe) {
          deletedGroupe = groupe
          recupProps.forEach(p => {
            if (deletedGroupe[p]) groupeData[p] = deletedGroupe[p]
          })
          Groupe.create(groupeData).store(this)
        }).seq(function (groupe) {
          // c'est un groupe valide
          expect(groupe.isDeleted()).to.be.false
          // on a récupéré l'oid du groupe deleted mais conservé tout le reste
          expect(groupe.oid).to.equals(deletedGroupe.oid)
          checkGroupe(groupeData, groupe)
          // le groupe deleted n'existe plus
          Groupe.match('oid').equals('groupe-supprime-oid').onlyDeleted().grabOne(this)
        }).seq(function (groupe) {
          expect(groupe).to.be.undefined
          // le groupe précédente est bien en bdd
          Groupe.match('oid').equals('groupe-supprime-oid').grabOne(this)
        }).seq(function (groupe) {
          expect(groupe.isDeleted()).to.be.false
          expect(groupe.oid).to.equals(deletedGroupe.oid)
          checkGroupe(groupeData, groupe)
          done()
        }).catch(done)
      })

      it('Signale un doublon si on veut enregistrer un groupe avec oid du même nom qu’un deleted', (done) => {
        // le groupe qu'on va chercher
        const nom = 'classe supprimée'
        flow().seq(function () {
          Groupe.match('nom').equals('ma_classe').grabOne(this)
        }).seq(function (groupe) {
          const nextSeq = this
          groupe.nom = nom
          groupe.store((error, groupe) => {
            expect(error).not.to.be.null
            expect(groupe).to.be.undefined
            expect(error.message).to.match(/^Impossible d’enregistrer pour cause de doublon/)
            nextSeq()
          })
        }).seq(function () {
          // on vérifie que le groupe supprimé est intact
          Groupe.match('nom').equals(nom).includeDeleted().grab(this)
        }).seq(function (groupes) {
          expect(groupes).to.have.length(1)
          const groupe = groupes[0]
          expect(groupe.isDeleted()).to.be.true
          expect(groupe.oid).to.equals('classe-supprime-oid')
          done()
        }).catch(done)
      })

      it('empêche de restaurer le groupe si la place est prise', (done) => {
        flow().seq(function () {
          Groupe.match('nom').equals('classe supprimée').onlyDeleted().grabOne(this)
        }).seq(function (groupe) {
          groupe.nom = 'ma classe'
          // pas de restore() qui ne touche qu'à la propriété __deletedAt (sans persister le reste en bdd)
          groupe.markToRestore()
          groupe.store((error, groupe) => {
            expect(error).not.to.be.null
            expect(groupe).to.be.undefined
            expect(error.message).to.match(/^Impossible d’enregistrer pour cause de doublon/)
            done()
          })
        }).catch(done)
      })
    })
  })
})
