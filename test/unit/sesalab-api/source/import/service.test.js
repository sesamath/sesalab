'use strict'
const flow = require('an-flow')
const constants = require('sesalab-commun/constants.js')
const { addHooks } = require('test/app')

let $import
let Utilisateur
let Groupe
// let $groupe

const structureOid = '1'

const shouldClasseExist = (nom, cb) => {
  Groupe
    .match('structure').equals(structureOid)
    .match('nom').equals(nom)
    .grabOne((err, groupe) => {
      if (err) return cb(err)

      expect(groupe).to.be.an('object', `le groupe ${nom} n'existe pas`)
      expect(groupe.nom).to.equal(nom)
      cb(null, groupe)
    })
}

const shouldElevesExist = (users, cb) => {
  flow(users).seqEach(function (user) {
    flow().seq(function () {
      Utilisateur
        .match('structures').equals(structureOid)
        .match('type').equals(constants.TYPE_ELEVE)
        .match('login').equals(user.login)
        .grabOne(this)
    }).seq(function (dbUser) {
      expect(dbUser).to.be.an('object', `login ${user.login} n'existe pas`)
      expect(dbUser).to.include(user)
      this()
    }).done(this)
  }).done(cb)
}

// @todo tester aussi les 3 imports (tableur / onde / siecle) en envoyant le fichier source entier
// - ça fait ce qu'on attend
// - ça plante si on met 500 classes
// - etc.
describe('$import', () => {
  addHooks()

  before(() => {
    Utilisateur = lassi.service('Utilisateur')
    Groupe = lassi.service('Groupe')
    $import = lassi.service('$import')
  })

  describe('importElevesInClasse', () => {
    describe('import de classe', () => {
      /**
       * Ajoute une classe (nomClasse) en y mettant un nouvel élève
       * @private
       * @param nomClasse
       * @param cb
       */
      const importClasse = (nomClasse, cb) => {
        $import.importClassesAndEleves(
          structureOid,
          null,
          [{ nom: 'Eleve', prenom: 'Nouvel', classeNom: nomClasse }],
          'prenom.nom',
          cb
        )
      }

      it('crée un élève dans une nouvelle classe', (done) => {
        flow()
          .seq(function () {
            importClasse('Nouvelle Classe', this)
          })
          .seq(function (response) {
            expect(response.nbGroupsAdded).to.equal(1)
            expect(response.eleves[0].classeNom).equals('Nouvelle Classe')
            shouldClasseExist('Nouvelle Classe', this)
          })
          .seq(function (classe) {
            shouldElevesExist([{ login: 'nouvel.eleve', classe: classe.oid }], this)
          })
          .done(done)
      })

      it('crée un élève dans une classe existante', (done) => {
        flow()
          .seq(function () {
            importClasse('ma classe', this) // existe déjà (fixtures)
          })
          .seq(function (response) {
            expect(response.nbGroupsAdded).to.equal(0)
            expect(response.eleves[0].classeNom).equals('ma classe')
            shouldClasseExist('ma classe', this)
          })
          .seq(function (classe) {
            expect(classe.oid).to.equal('1')
            shouldElevesExist([{ login: 'nouvel.eleve', classe: classe.oid }], this)
          })
          .done(done)
      })
    })
    describe('imports des élèves', () => {
      const importEleves = (eleves, cb) => {
        $import.importClassesAndEleves(
          structureOid,
          null,
          eleves,
          'prenom.nom',
          cb
        )
      }
      describe('création', () => {
        it('importe un élève avec login et password généré', (done) => {
          flow()
            .seq(function () {
              importEleves([{ nom: 'Eleve', prenom: 'Nouvel', classeNom: 'ma classe' }], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(1)
              expect(response.errors).to.have.length(0)
              expect(response.eleves).to.have.length(1)
              const eleve = response.eleves[0]
              expect(eleve.message).to.equal('L’élève Nouvel Eleve (ma classe) a été importé.')
              expect(eleve.isEleveCreated).equals(true)
              expect(eleve.classeNom).equals('ma classe')
              expect(eleve.nom).equals('Eleve')
              expect(eleve.prenom).equals('Nouvel')
              expect(eleve.login).equals('nouvel.eleve')
              expect(eleve.passwordComment).equals('Généré automatiquement')
              expect(eleve.nouveauPassword).to.be.a('string')
              expect(eleve.nouveauPassword).to.have.lengthOf.above(5)

              shouldElevesExist([{ nom: 'Eleve', prenom: 'Nouvel', login: 'nouvel.eleve', classe: '1' }], this)
            })
            .done(done)
        })

        it('crée un élève avec uniquement un prénom', (done) => {
          flow()
            .seq(function () {
              importEleves([{ prenom: 'Nouvel', login: 'nouvel.eleve', classeNom: 'ma classe' }], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(1)
              expect(response.errors).to.have.length(0)
              shouldElevesExist([{ prenom: 'Nouvel', login: 'nouvel.eleve' }], this)
            })
            .done(done)
        })

        it('crée un élève avec uniquement un nom', (done) => {
          flow()
            .seq(function () {
              importEleves([{ nom: 'Nouvel', login: 'nouvel.eleve', classeNom: 'ma classe' }], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(1)
              expect(response.errors).to.have.length(0)
              shouldElevesExist([{ nom: 'Nouvel', login: 'nouvel.eleve' }], this)
            })
            .done(done)
        })

        it('rejette un élève sans nom ni prénom', (done) => {
          flow()
            .seq(function () {
              importEleves([
                { nom: 'Eleve', prenom: 'Nouvel', password: 'top1Secret', login: 'nouvel.eleve', classeNom: 'ma classe' },
                { password: 'top1Secret', login: 'nouvel.eleve2', classeNom: 'ma classe' },
                { nom: '', password: 'top1Secret', login: 'nouvel.eleve3', classeNom: 'ma classe' },
                { nom: ' ', password: 'top1Secret', login: 'nouvel.eleve4', classeNom: 'ma classe' },
                { nom: ' ', prenom: ' ', password: 'top1Secret', login: 'nouvel.eleve5', classeNom: 'ma classe' }
              ], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(1)
              expect(response.eleves).to.have.length(5)
              expect(response.eleves[0].message).to.equal('L’élève Nouvel Eleve (ma classe) a été importé.')

              response.eleves.forEach((eleve, index) => {
                if (index === 0) return
                expect(eleve.error).to.equal(`L’élève nouvel.eleve${index + 1} n’a pas été importé car celui-ci n’a ni nom ni prénom.`)
              })
              shouldElevesExist([{ nom: 'Eleve', prenom: 'Nouvel', login: 'nouvel.eleve', classe: '1' }], this)
            })
            .done(done)
        })

        it('crée un élève avec login et password spécifiés', (done) => {
          flow()
            .seq(function () {
              importEleves([{ nom: 'Eleve', prenom: 'Nouvel', password: 'top1Secret', login: 'mon.login', classeNom: 'ma classe' }], this)
            })
            .seq(function (response) {
              const eleve = response.eleves[0]
              expect(eleve.login).equals('mon.login')
              expect(eleve.passwordComment).equals('Affecté comme demandé')
              expect(eleve.nouveauPassword).to.equal('top1Secret')

              shouldElevesExist([{ nom: 'Eleve', prenom: 'Nouvel', login: 'mon.login', classe: '1' }], this)
            })
            .done(done)
        })

        it('crée un élève avec un password simpliste', (done) => {
          flow()
            .seq(function () {
              importEleves([{ nom: 'Élève', prenom: 'Nouvel', password: 'tropsimple', classeNom: 'ma classe' }], this)
            })
            .seq(function (response) {
              const eleve = response.eleves[0]
              expect(eleve.login).equals('nouvel.eleve')
              expect(eleve.passwordComment).equals('Affecté comme demandé')
              expect(eleve.nouveauPassword).to.equal('tropsimple')

              shouldElevesExist([{ nom: 'Élève', prenom: 'Nouvel', login: 'nouvel.eleve', classe: '1' }], this)
            })
            .done(done)
        })

        it('crée des homonymes si on le demande', (done) => {
          flow()
            .seq(function () {
              importEleves([
                { nom: 'Eleve', prenom: 'Nouvel', classeNom: 'ma classe' },
                { nom: 'Eleve', prenom: 'Nouvel', classeNom: 'ma classe' },
                { nom: 'Eleve', prenom: 'Nouvel', classeNom: 'ma classe' }
              ], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(3)
              shouldElevesExist([
                { nom: 'Eleve', prenom: 'Nouvel', login: 'nouvel.eleve' },
                { nom: 'Eleve', prenom: 'Nouvel', login: 'nouvel.eleve2' },
                { nom: 'Eleve', prenom: 'Nouvel', login: 'nouvel.eleve3' }
              ], this)
            })
            .done(done)
        })
      })

      describe('mise à jour', () => {
        it('maj un élève si son login est ciblé', (done) => {
          flow()
            .seq(function () {
              importEleves([
                { nom: 'Eleve', prenom: 'NouveauPrenom', login: 'eleve-valide', classeNom: 'ma classe' }
              ], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(0)
              expect(response.nbUsersUpdated).to.equal(1)
              const eleve = response.eleves[0]
              expect(eleve.login).equals('eleve-valide')
              expect(eleve.isEleveCreated).to.equal(false)
              expect(eleve.isEleveUpdated).to.equal(true)
              expect(eleve.aChangeDeClasse).to.equal(false)
              expect(eleve.isEleveRestored).to.equal(false)
              expect(eleve.passwordComment).to.equal('Ancien conservé')
              expect(eleve.message).to.equal('L’élève NouveauPrenom Eleve (ma classe) a été mis à jour.')

              expect(response.errors).to.have.length(0)

              shouldElevesExist([{ oid: 'eleve-valide-oid', prenom: 'NouveauPrenom', login: 'eleve-valide', classe: '1' }], this)
            })
            .done(done)
        })
        it('peut changer un élève de classe si on précise son login', (done) => {
          flow()
            .seq(function () {
              $import.importClassesAndEleves(
                structureOid,
                null,
                [{ nom: 'Eleve', prenom: 'NouveauPrenom', login: 'eleve-valide', classeNom: 'Nouvelle Classe' }],
                'prenom.nom',
                this
              )
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(0)
              expect(response.nbUsersUpdated).to.equal(1)
              const eleve = response.eleves[0]
              expect(eleve.login).equals('eleve-valide')
              expect(eleve.classeNom).equals('Nouvelle Classe')
              expect(eleve.isEleveCreated).to.equal(false)
              expect(eleve.isEleveUpdated).to.equal(true)
              expect(eleve.aChangeDeClasse).to.equal(true)
              expect(eleve.isEleveRestored).to.equal(false)
              expect(eleve.passwordComment).to.equal('Ancien conservé')
              expect(eleve.message).to.equal('L’élève NouveauPrenom Eleve (Nouvelle Classe) a été mis à jour (il a changé de classe).')

              expect(response.errors).to.have.length(0)
              shouldClasseExist('Nouvelle Classe', this)
            })
            .seq(function (nouvelleClasse) {
              shouldElevesExist([{ nom: 'Eleve', prenom: 'NouveauPrenom', login: 'eleve-valide', classe: nouvelleClasse.oid }], this)
            })
            .done(done)
        })
        it('restaure et maj un élève en corbeille si son login est ciblé', (done) => {
          flow()
            .seq(function () {
              importEleves([
                { nom: 'Eleve', prenom: 'NouveauPrenom', login: 'supprime.eleve', classeNom: 'ma classe' }
              ], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(0)
              expect(response.nbUsersUpdated).to.equal(1)
              const eleve = response.eleves[0]
              expect(eleve.login).equals('supprime.eleve')
              expect(eleve.isEleveCreated).to.equal(false)
              expect(eleve.isEleveUpdated).to.equal(true)
              expect(eleve.isEleveRestored).to.equal(true)
              // Sur une restauration sans préciser de password on re-génère un password
              expect(eleve.passwordComment).to.equal('Généré automatiquement')
              expect(eleve.nouveauPassword).to.be.a('string')
              expect(eleve.nouveauPassword).to.have.lengthOf.above(5)
              expect(eleve.message).to.equal('L’élève NouveauPrenom Eleve (ma classe) était dans la corbeille et a été mis à jour.')

              expect(response.errors).to.have.length(0)

              // On vérifie que l'élève supprimé a bien été restauré (sinon shouldElevesExist ne le trouverait pas)
              // et que son prenom a bien été mis à jour
              shouldElevesExist([{ oid: 'eleve-supprime-oid', prenom: 'NouveauPrenom', login: 'supprime.eleve', classe: '1' }], this)
            })
            .done(done)
        })

        it('restaure et maj un élève en corbeille si son nationalId est ciblé', (done) => {
          flow()
            .seq(function () {
              Utilisateur.match('oid').equals('eleve-nationalid-oid').grabOne(this)
            })
            .seq(function (eleveExistant) {
              eleveExistant.softDelete(this)
            })
            .seq(function () {
              importEleves([{ nom: 'Eleve', prenom: 'NouveauPrenom', nationalId: '42', classeNom: 'ma classe' }], this)
            })
            .seq(function (response) {
              expect(response.nbUsersAdded).to.equal(0)
              expect(response.nbUsersUpdated).to.equal(1)
              const eleve = response.eleves[0]
              expect(eleve.login).equals('nationalid.eleve')
              expect(eleve.isEleveCreated).to.equal(false)
              expect(eleve.isEleveUpdated).to.equal(true)
              expect(eleve.isEleveRestored).to.equal(true)
              // Sur une restauration on re-génère un password
              expect(eleve.passwordComment).to.equal('Généré automatiquement')
              expect(eleve.nouveauPassword).to.be.a('string')
              expect(eleve.nouveauPassword).to.have.lengthOf.above(5)
              expect(eleve.message).to.equal('L’élève NouveauPrenom Eleve (ma classe) était dans la corbeille et a été mis à jour.')

              expect(response.errors).to.have.length(0)

              // On vérifie que l'élève supprimé a bien été restauré (sinon shouldElevesExist ne le trouverait pas)
              // et que son prenom a bien été mis à jour
              shouldElevesExist([{ oid: 'eleve-nationalid-oid', prenom: 'NouveauPrenom', login: 'nationalid.eleve', classe: '1' }], this)
            })
            .done(done)
        })
      })

      describe('gestion des homonymes', () => {
        it('empêche de créer un élève ayant le même nom qu’un élève existant', (done) => {
          flow()
            .seq(function () {
              importEleves([
                // Homonymes existant en fixtures
                { nom: 'Eleve', prenom: 'Valide', nationalId: '12345', classeNom: 'ma classe' }, // cas limit élève importé a un national id mais pas l'existant
                { nom: 'Eleve', prenom: 'NationalId', classeNom: 'ma classe' } // cas limite l'élève existant a un national id mais pas l'élève importé
              ], this)
            })
            .seq(function (response) {
              expect(response.eleves).to.have.length(2)
              expect(response.nbUsersAdded).to.equal(0)
              expect(response.eleves[0].error).to.equal('Le compte élève Valide Eleve n’a pas été importé car il existe déjà un élève ayant ce nom avec l’identifiant eleve-valide. Vous devez ajouter ou mettre à jour cet élève manuellement (ou refaire un import en précisant son identifiant).')
              expect(response.eleves[1].error).to.equal('Le compte élève NationalId Eleve n’a pas été importé car il existe déjà un élève ayant ce nom avec l’identifiant nationalid.eleve. Vous devez ajouter ou mettre à jour cet élève manuellement (ou refaire un import en précisant son identifiant).')
              this()
            })
            .done(done)
        })
        it('permet de créer un élève ayant le même nom qu’un élève existant si un login est donné', (done) => {
          flow()
            .seq(function () {
              importEleves([
                // Homonyme existant en fixtures
                { nom: 'Eleve', prenom: 'Valide', login: 'nouveau-valide', classeNom: 'ma classe' }
              ], this)
            })
            .seq(function (response) {
              expect(response.errors).to.have.length(0)
              expect(response.nbUsersAdded).to.equal(1)
              shouldElevesExist([{ login: 'nouveau-valide', classe: '1' }], this)
            })
            .done(done)
        })
        it('permet de créer un élève ayant le même nom qu’un élève existant si ils ont un nationalId différent', (done) => {
          flow()
            .seq(function () {
              importEleves([
                // Homonyme existant en fixtures
                { nom: 'Eleve', prenom: 'NationalId', nationalId: '43', classeNom: 'ma classe' }
              ], this)
            })
            .seq(function (response) {
              expect(response.errors).to.have.length(0)
              expect(response.nbUsersAdded).to.equal(1)
              // test l'incrémentation correcte d'un login existant sur un élève d'avant l'import
              expect(response.eleves[0].login).equals('nationalid.eleve2')
              shouldElevesExist([{ login: 'nationalid.eleve2', classe: '1' }], this)
            })
            .done(done)
        })
        it('permet de créer un élève ayant le même nom qu’un élève existant en corbeille', (done) => {
          flow()
            .seq(function () {
              importEleves([
                // Homonyme existant en fixtures
                { nom: 'Eleve', prenom: 'Supprime', classeNom: 'ma classe' }
              ], this)
            })
            .seq(function (response) {
              expect(response.errors).to.have.length(0)
              expect(response.nbUsersAdded).to.equal(1)
              // test l'incrémentation correcte d'un login existant sur un élève d'avant l'import
              expect(response.eleves[0].login).equals('supprime.eleve2')
              shouldElevesExist([{ login: 'supprime.eleve2', classe: '1' }], this)
            })
            .done(done)
        })
      })
    })
  })
})
