const isEmail = require('sesalab-commun/isEmail')

describe('isEmail', () => {
  it('Valide une adresse email', () => {
    // tout ce qu'on doit refuser
    ;[
      '',
      undefined,
      null,
      {},
      [],
      ['email@example.com'],
      new Date(),
      0,
      42,
      -1,
      true,
      false,
      'email.com',
      '@email.com',
      'email.com@',
      'email@foo@example.com',
      ' email@example.com',
      'email@example.com ',
      'email@examplecom',
      'email prof@example.com',
      'email @example.com',
      'email@ example.com',
      'email;prof@example.com',
      'email,prof@example.com',
      'email:prof@example.com',
      'email\tprof@example.com',
      'email\nprof@example.com',
      'email\rprof@example.com',
      'email@exa;mple.com',
      'email@exa:mple.com',
      'email@exa,mple.com',
      'email@exa mple.com',
      'email@exa;mple.com',
      'email@example.c;om',
      'email@example.c4m',
      'email@example.com\'',
      'email@example.c',
      // ces cas devraient fonctionner d'après les RFC
      // mais sont des erreurs de frappes dans 100% des cas rencontrés.
      // À traiter séparément quand on aura ajouté un flag au validate avec 2 regex,
      // une intolérante qui suggère une faute de frappe et une plus proche des RFC
      '_email@example.com',
      'em\'ail@example.com',
      'em"ail@example.com',
      'em#ail@example.com',
      'em$ail@example.com',
      'em~ail@example.com',
      // accents
      'émail@example.com',
      'emâil@example.com',
      'email@exâmple.com',
      'email@example.côm',
      // domaine ou prefixe très courts
      'email@a.bb',
      'e@a.bb'
    ].forEach(email => expect(isEmail.validate(email)).to.equal(false, email))

    // et ce qui doit passer
    ;[
      'email@example.com',
      'e-mail@example.com',
      'e_mail@example.com',
      'e+mail@example.com',
      'e42mail@example.com',
      '4email@example.com',
      'email@example.longlongtld',
      'em@aa.bb'
    ].forEach(email => expect(isEmail.validate(email)).to.equal(true, email))
  })
})
