const { formatCsvCell, formatStringForFilename } = require('sesalab-commun/formatString.js')

describe('formatString', () => {
  describe('formatCsvCell', () => {
    it('Formatte une string pour un csv (quoted, sans quote dedans)', () => {
      expect(formatCsvCell()).to.equal('""')
      expect(formatCsvCell('string ')).to.equal('"string"')
      expect(formatCsvCell(' str ing ')).to.equal('"str ing"')
      expect(formatCsvCell(42)).to.equal('42')
      expect(formatCsvCell(4242.42)).to.equal('4242,42')
      expect(formatCsvCell(' une "st\fring" \ndeux " \rtro\vis; 4 ')).to.equal('"une ""st - ring""  - deux ""  - tro - is; 4"')
      expect(formatCsvCell(' une "st\fring" \ndeux " \rtro\vis; 4 ', true)).to.equal('"une ""st\fring"" \ndeux "" \rtro\vis; 4"')
      // @todo stub console.error pour tester du boolean Object & co
    })
  })

  describe('formatStringForFilename', () => {
    it('Formatte un nom de fichier', () => {
      expect(formatStringForFilename('fichier_test.csv')).to.equal('fichier_test.csv')
      expect(formatStringForFilename(' fï@ch+^/`ié.te-"\nst.c\'sv')).to.equal('fi_ch_ie.te-_st.c_sv')
    })
  })
})
