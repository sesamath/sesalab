const resultChecksum = require('sesalab-commun/resultChecksum')

describe('resultChecksum', () => {
  it('Crée un hash qui varie en fonction du score et de la ressource', () => {
    expect(resultChecksum('sesabibli/123', ['ae7762de12c34', '5e7764e1223a'], 0.3)).to.equal('43bf67')
    // L'ordre des élèves ne devrait pas influencer le checksum
    expect(resultChecksum('sesabibli/123', ['5e7764e1223a', 'ae7762de12c34'], 0.3)).to.equal('43bf67')

    // Un score en text ne devrait pas changer le checksum
    // mais ça râle quand on passe une string, on stub console.error
    const consoleError = console.error
    console.error = function fakeConsoleError (error) {
      if (error && error.message === 'notify appelé sans window disponible') return
      consoleError.apply(console, arguments)
    }
    expect(resultChecksum('sesabibli/123', ['5e7764e1223a', 'ae7762de12c34'], '0.3')).to.equal('43bf67')
    // on restaure
    console.error = consoleError

    // Sans score ça fonctionne aussi (en tout cas on ne veut pas de NaN)
    expect(resultChecksum('sesabibli/123', ['5e7764e1223a', 'ae7762de12c34'], undefined)).to.equal('4d87a3')
    expect(resultChecksum('sesabibli/123', ['5e7764e1223a', 'ae7762de12c34'], null)).to.equal('4d87a3')

    // On change le score
    expect(resultChecksum('sesabibli/123', ['ae7762de12c34', '5e7764e1223a'], 0.4)).to.equal('44d077')
    expect(resultChecksum('sesabibli/123', ['ae7762de12c34', '5e7764e1223a'], 0.5)).to.equal('45e187')

    // On change la resource
    expect(resultChecksum('sesabibli/124', ['ae7762de12c34', '5e7764e1223a'], 0.3)).to.equal('43c06a')
    expect(resultChecksum('sesabibli/125', ['ae7762de12c34', '5e7764e1223a'], 0.3)).to.equal('43c16d')
  })
})
