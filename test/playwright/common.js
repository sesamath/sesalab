/**
 * Fonctions utilitaires
 * @module common
 */

const store = require('./store')

const dateFormat = new Intl.DateTimeFormat('fr-FR', { year: 'numeric', month: '2-digit', day: '2-digit' })
const timeFormat = new Intl.DateTimeFormat('fr-FR', { hour: '2-digit', minute: '2-digit', second: '2-digit' })

/**
 * Retourne true si tous les éléments de la liste sont true (pas truthy, et récursivement), false sinon
 * @param {Array} list
 * @return {boolean}
 */
function allTrue (list) {
  if (!Array.isArray(list)) throw TypeError(`allTrue ne gère que des Array (ici ${typeof list} : ${JSON.stringify(list)})`)
  return list.every(item => {
    if (Array.isArray(item)) return allTrue(item)
    return item === true
  })
}

/**
 * Valide un test
 * @param {Page} page
 * @param {ElementCheck} check
 * @return {Promise<boolean>}
 */
async function checkOne (page, { selector, validation }) {
  if (validation instanceof RegExp) {
    const content = await page.innerText(selector)
    if (!content) throw Error(`Aucun élément trouvé avec le sélecteur '${selector} sur la page ${await page.url()}'`)
    if (!validation.test(content)) throw Error(`La validation de l'élément '${selector}' avec l’expression ${validation.toString()} a échoué (page ${await page.url()})`)
    return true
  } else if (typeof validation === 'function') {
    const elt = await page.$(selector)
    return validation(elt)
  } else {
    throw Error('check invalide, il doit contenir deux propriétés : { selector: string, validation: RegExp|Funtion }')
  }
}

/**
 * Retourne la date au format YYYY-MM-DD:hh:mm:ss.sss
 * @return {string}
 */
function getDateFormatted () {
  const d = new Date()
  const ms = (String(d.getMilliseconds())).padStart(3, '0')
  return `${dateFormat.format(d)} ${timeFormat.format(d)}.${ms}`
}

/**
 * Envoie les arguments à console.log en préfixant avec la date courante
 * @param {any} ...arg
 */
function log (arg, ...others) {
  const datePrefix = `[${getDateFormatted()}] `
  if (typeof arg === 'string') console.log(datePrefix + arg, ...others)
  else console.log(datePrefix, arg, ...others)
}
/**
 * Envoie les arguments à console.log en préfixant avec la date courante
 * @param {any} ...arg
 * @param others
 */
function logError (arg, ...others) {
  const datePrefix = `[${getDateFormatted()}] `
  if (typeof arg === 'string') console.error(datePrefix + arg, ...others)
  else console.error(datePrefix, arg, ...others)
}

/**
 * idem log si store.verbose, sinon ne fait rien
 * @param {any} ...args
 */
function logIfVerbose (...args) {
  if (store.verbose) log(...args)
}

/**
 * Retournent les contenus des éléments qui matchent un pattern
 * @param {Page} page
 * @param {string} selector
 * @param {RegExp} pattern
 * @return {Promise<JSHandle[]>} La liste des éléments dont le contenu match le pattern
 */
async function patternFilter (page, selector, pattern) {
  if (!(pattern instanceof RegExp)) return Promise.reject(Error('patternFilter veut un pattern en RexExp'))
  const elts = await page.$$(selector)
  // elt.innerText() retourne une promesse, on peut pas faire de async/await dans un map, on passe par Promise.all
  const contents = await Promise.all(elts.map((elt) => elt.innerText()))
  return contents.map((content, i) => pattern.test(content) ? elts[i] : null).filter(Boolean)
}

/**
 * Lance une série de fonctions async une par une et retourne le tableau des résultats
 * @param {function[]} asyncFunctions une liste de fcts qui doivent chacune retourner une promesse
 * @param {boolean} [continueOnError=false] Passer true pour continuer la séquence en cas d'erreur (elle est alors mise dans le tableau de résultats)
 * @return {any[]} La liste des valeurs de résolution de chaque asyncFunction, dans le même ordre
 */
async function runAllInSeq (asyncFunctions, continueOnError = false) {
  if (!Array.isArray(asyncFunctions) || !asyncFunctions.every(f => typeof f === 'function')) throw Error('arguments invalides')
  const results = []
  while (asyncFunctions.length) {
    const fn = asyncFunctions.shift()
    try {
      const result = await fn()
      results.push(result)
    } catch (error) {
      if (!continueOnError) throw error
      results.push(error)
    }
  }
  return results
}

/**
 * Si arg est un ElementHandle, retourne son contenu (html ou texte suivant asText), si c'est un JSHandle retourne son json, sinon retourne arg
 * @param {Protocol.ElementHandle|Protocol.ElementHandle|any} arg
 * @param {boolean} asText retourne le innerText plutôt que le innerHTML (si c'était un ElementHandle)
 * @return {Promise<string|any>}
 */
function serializeHandle (arg, asText = false) {
  if (typeof arg === 'object' && typeof arg.asElement === 'function') {
    const elt = arg.asElement()
    if (elt) return asText ? elt.innerText() : elt.innerHTML()
    // c'était un JSHandle qui n'était pas un ElementHandle
    if (typeof arg.jsonValue === 'function') return arg.jsonValue()
    // sinon un objet avec une méthode asElement qui n'était pas un JSHandle ? On le retournera ci-dessous tel quel
  }
  return Promise.resolve(arg)
}

/**
 * Patiente un certain temps
 * @param {number} delay délai d'attente en ms
 */
async function waitMs (delay) {
  await new Promise(resolve => setTimeout(() => resolve(), delay))
}

module.exports = {
  allTrue,
  checkOne,
  getDateFormatted,
  log,
  logError,
  logIfVerbose,
  patternFilter,
  runAllInSeq,
  serializeHandle,
  waitMs
}
