Tests playwright
================

Il faut d'abord avoir lancé les deux Sésathèques en mode test avec `pnpm run start:testBoth` (dans le dossier racine du dépôt local sesatheque),
puis lancer les tests sesalab avec `pnpm run test:browser`


Problèmes connus
----------------

Cf https://github.com/microsoft/playwright/blob/master/docs/troubleshooting.md

### No usable sandbox!
Sous linux, pour les tests avec chrome il fallait ajouter à /etc/sysctl.conf la ligne
```
kernel.unprivileged_userns_clone = 1
```
puis le charger avec `sysctl -p` (en root)

Pour vérifier que ça règle le pb sans modifier /etc/sysctl.conf c'est (en root) `sysctl -w kernel.unprivileged_userns_clone=1`
(qui ne reste valable que jusqu'au prochain reboot)

Ça ne semble plus nécessaire depuis ~2020
