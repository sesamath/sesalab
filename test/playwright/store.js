/**
 * Gère notre singleton store (défini getters/setters pour contrôler les modifications de ce store)
 * @module store
 */
/**
 * Un singleton pour conserver les préférences globales (un store maison)
 * @typedef Store
 * @property {Protocol.Browser} browserInstance l'objet Browser courant, null s'il n'y en a pas d'instancié
 * @property {string} browserName Le nom du browser courant
 * @property {string[]} browsers La liste des browsers qu'on va tester
 * @property {boolean} [continue] pour continuer en cas d'erreur, par défaut a la même valeur que headless
 * @property {boolean} [debug=false]
 * @property {boolean} [headless=false]
 * @property {boolean} [logRequest=false]
 * @property {boolean} [logFailedRequest=true]
 * @property {boolean} [quiet=false]
 * @property {boolean} [timeoutLocked=false] Vaut true si le timeout a été imposé par le lancement (option ou environnement),
 *                        dans ce cas un éventuel timeout précisé dans un scenario sera ignoré
 * @property {boolean} [verbose=false]
 * @property {number} timeout
 */

/**
 * Nos préférences initialisées par le lancement (options et environnement)
 * Contient aussi l'instance courante de Browser (pour éviter de déclarer un 2e singleton uniquement pour ça)
 * @type {Store}
 */
const store = {
  // props sans getter/setter
  /** @type {Protocol.Browser} */
  browserInstance: null,
  browserName: ''
}
// un objet pour stocker les booléens (pour utiliser une propriété dynamique)
const booleans = {
  continue: false,
  debug: false,
  headless: false,
  logRequest: false,
  logFailedRequest: true,
  quiet: false,
  timeoutLocked: false,
  verbose: false
}
let timeout = 30000
let slow

const boolSetter = (prop, value) => {
  if (typeof value !== 'boolean') throw Error(`${prop} est un boolean`)
  booleans[prop] = value
}

// les booléens
Object.keys(booleans).forEach(prop => {
  Object.defineProperty(store, prop, {
    get: () => booleans[prop],
    set: boolSetter.bind(null, prop)
  })
})
// timeout
Object.defineProperty(store, 'timeout', {
  get: () => timeout,
  set: (value) => {
    if (isNaN(value) || value < 100) throw Error(`timeout invalide (doit être un nombre ≥ 100, ce sont des ms) : ${value}`)
    timeout = value
  }
})
// propriétés facultatives, qui restent à undefined tant qu'on les affecte pas
Object.defineProperty(store, 'slow', {
  get: () => slow,
  set: (value) => {
    if (!value || typeof value !== 'number' || value < 0) throw Error('scénario invalide (doit être un nombre positif)')
    slow = value
    console.log('slow set', slow)
  }
})

module.exports = store
