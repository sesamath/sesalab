// process.env.CHROME_BIN = '/usr/bin/chromium'
const playwright = require('playwright')

// pour le rerouting des appels lié au window.sesathequeBaseUrl mis en dur dans le index.html au build
const config = require('config')
const usualConfig = require('../../_private/config')
const store = require('./store')
const { log, logError, logIfVerbose, serializeHandle } = require('./common')

// le check de la case rgpd marche pas avec chromium, pas trouvé pourquoi, cf https://github.com/microsoft/playwright/issues/2360
const defaultBrowserName = 'chromium'

// pour le rerouting des appels lié au window.sesathequeBaseUrl mis en dur dans le index.html au build
const sesathequeBaseUrl = config.application.sesatheques[0].baseUrl
const usualSesathequeBaseUrl = usualConfig.application.sesatheques[0].baseUrl

/**
 * Vérifie que l'objet page.messages ne contient pas d'erreurs (affiche warning & error si y'en a)
 * Reset la liste des warnings/errors
 * @param {Protocol.Page} page
 * @param {boolean} [throwOnError=false] Passer true pour lancer une erreur s'il y en a (sinon on le signale en console)
 * @throws {Error} Si y'a une erreur avec throwOnError
 */
function checkMessages (page, throwOnError = false) {
  const { messages } = page
  if (!store.quiet && Array.isArray(messages.warning) && messages.warning.length) {
    console.log('warnings dans la console du navigateur :\n', messages.warning.join('\n'))
    messages.warning = []
  }
  if (Array.isArray(messages.error) && messages.error.length) {
    console.error('Erreurs dans la console du navigateur :\n', messages.error.join('\n'))
    if (throwOnError) throw Error(`Il y a eu des erreurs dans la console du navigateur sur ${page.url()}`)
    messages.error = []
  }
}

/**
 * Affiche le html de la page courante en console (de mocha)
 * @param {Page} page
 * @return {Promise<void>}
 */
async function dump (page) {
  const html = await page.$eval('body', (body) => body.parentNode.innerHTML)
  console.log('La page contient : \n', html)
}

/**
 * Initialise le store et l'instance courante du browser d'après l'environnement
 * @param {string} browserName
 * @return {Promise<Protocol.Browser>}
 */
async function initCurrentBrowser (browserName) {
  logIfVerbose(`init browser ${browserName}`)
  const browserOptions = {}
  if (store.browserInstance) await store.browserInstance.close()
  store.browserInstance = null
  store.browserName = ''
  const pwBrowser = playwright[browserName]
  browserOptions.headless = store.headless
  // on ajoute les devtools pour chromium, sauf si headless
  if (!store.headless && browserName === 'chromium') browserOptions.devtools = true
  // chromium plante en headless chez moi, on tente ce workaround https://github.com/microsoft/playwright/issues/4761
  if (store.headless) browserOptions.args = ['--disable-gpu']
  if (store.slow) {
    console.log('slow mis en option du browser', store.slow)
    browserOptions.slowMo = store.slow
  }
  const browser = await pwBrowser.launch(browserOptions)
  // on lui crée son contexte
  const context = await browser.newContext()
  // on ajoute un route handler pour changer les appels vers urlBibli/ping.html => urlBibliTest/ping.html
  // (c'est le seul cas où l'url de la bibli "normale" en dur dans index.html est utilisé, avant l'appel des settings)
  const usualPingUrl = usualSesathequeBaseUrl + 'ping.html'
  await context.route(usualPingUrl, (route, request) => {
    // on redirige
    route.fulfill({
      status: 301,
      headers: {
        location: sesathequeBaseUrl + 'ping.html'
      }
    })
  })
  store.browserInstance = browser
  store.browserName = browserName
  logIfVerbose(`browser ${browserName} instancié (version ${browser.version()})`)
  return browser
}

/**
 * Init le store (avec son browser) d'après l'environnement
 * @return {Promise<void>}
 */
async function init () {
  if (process.env.HEADLESS) {
    store.headless = true
  }
  if (process.env.DEBUG) {
    store.debug = true
    store.logRequest = true
    store.logFailedRequest = true
    store.quiet = false
    store.verbose = true
  } else if (process.env.VERBOSE) {
    store.debug = false
    store.logRequest = false
    store.logFailedRequest = true
    store.quiet = false
    store.verbose = true
  } else if (process.env.QUIET) {
    store.debug = false
    store.logRequest = false
    store.logFailedRequest = false
    store.quiet = true
    store.verbose = false
  }
  const browserName = process.env.DEFAULT_BROWSER || defaultBrowserName
  await initCurrentBrowser(browserName)
}

/**
 * Retourne une page
 * @param {pageOptions} [options]
 * @return {Promise<Page>}
 */
async function getPage ({ addMessages, url } = {}) {
  const browser = store.browserInstance
  const context = browser.contexts()[0]
  const page = await context.newPage()
  // on ajoute des listeners sur les messages si on veut les récupérer
  if (addMessages) {
    page.messages = { error: [], warning: [] }
    // https://github.com/microsoft/playwright/blob/master/docs/api.md#event-console
    page.on('console', async (message) => {
      // https://github.com/microsoft/playwright/blob/master/docs/api.md#class-consolemessage
      const type = message.type()
      if (Array.isArray(page.messages[type])) {
        // https://playwright.dev/docs/api/class-consolemessage#consolemessageargs retourne des JSHandle
        const args = await Promise.all(message.args().forEach(serializeHandle))
        // à voir si on utilise pas plutôt message.text() qui retourne une string en sync
        page.messages[type].concat(args)
      }
    })
    page.on('close', () => checkMessages(page, true))
  }
  if (store.logFailedRequest) {
    // https://playwright.dev/docs/api/class-request#requestfailure
    page.on('requestfailed', (request) => console.log(`request failure on ${request.method()} ${request.url()} ${request.failure().errorText}`))
  }
  if (store.logRequest) page.on('response', logResponse)
  if (url) await page.goto(url)
  return page
}

/**
 * À passer en listener avec page.on('response', logResponse) ou page.removeListener('response', logResponse)
 * @param {Protocol.Response} res La réponse {@link https://playwright.dev/docs/api/class-response}
 */
function logResponse (res) {
  const req = res.request()
  log(`${req.method()} ${req.url()} ${res.status()} ${res.statusText()} => ${res.ok() ? 'ok' : 'KO'}`)
  if (store.debug) {
    // on affiche aussi le contenu de la réponse json
    const headers = res.headers()
    if (/json/.test(headers['content-type'])) {
      // pas de await ici car on est pas async, on log en tâche de fond
      res.json().then(resJson => log(`réponse json de ${req.url()} :\n`, resJson)).catch(logError)
    }
  }
}

module.exports = {
  checkMessages,
  dump,
  getPage,
  init,
  logResponse
}

/**
 * Options possibles pour getPage
 * @typedef {Object} pageOptions
 * @property {boolean} [options.addMmessage] Ajoute une propriété messages sur l'objet page avec un listener pour les remplir (d'après la console du navigateur)
 * @property {string} [options.url] Pour charger cette url avant de retourner la page
 */
/**
 * Options possibles pour getPage
 * @typedef {Object} pageMessages
 * @property {Array<*>} [error] Pour récupérer les appels à console.error dans le navigateur
 * @property {Array<*>} [warning] item pour console.warning, etc.
 */

/**
 * @callback pageTest
 * @param {Page} page
 * @return {Promise}
 */
