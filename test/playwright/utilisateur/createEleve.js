const fs = require('fs')
const path = require('path')
const { expect } = require('chai')

const { addHooks, logDir } = require('../../app')
const { getPage } = require('../browser')
const { patternFilter, waitMs } = require('../common')
const store = require('../store')

const config = require('../../../config')

// on fait le test avec un login qui n'est pas dans les fixtures qui init la base
const { eleve, structure } = require('./fixturesSup')

const pageOptions = {
  addMessages: true,
  url: config.application.baseUrl
}

module.exports = function () {
  describe('Crée un utilisateur élève et tente de se logguer', async () => {
    // pour récupérer les mails qui auraient été envoyés
    const cumulMails = []
    const mails = []
    addHooks({ doNotResetDb: true, mails })

    let validationUrl

    it('créé un compte élève', async function creeEleve () {
      this.timeout(60000)
      const page = await getPage(pageOptions)
      try {
        const linksFiltered = await patternFilter(page, 'a', /Créer un compte élève/)
        expect(linksFiltered).to.have.length(1)
        await linksFiltered[0].click()
        await page.waitForSelector('#rgpd', { state: 'attached', timeout: 2000 })
        expect(page.url()).to.equal(config.application.baseUrl + 'eleve/inscription/')
        // coche rgpd
        if (store.browserName === 'chromium' && !store.headless) {
          // on a un bug avec chromium : Clicking the checkbox did not change its state
          // (il trouve l'input, clique mais ça change pas sa propriété checked)
          // ça ne le fait pas en headless… ni avec firefox
          // en fait faut attendre un peu (apparemment la popup n'est pas encore rendue et ça déconne)
          await waitMs(300)
        }
        await page.check('#rgpd')

        // Selection de la structure
        const { code, nom } = structure
        const inputStructure = await page.waitForSelector('input[name="structure"]')
        await inputStructure.type(code)
        const li = await page.waitForSelector('ul.structure-drop-down li')
        await li.click()
        await page.waitForSelector('body div.content form label') // veuillez choisir votre classe
        const fullName = await page.$eval('input[name="structure"]', (el) => el.value)
        expect(fullName).to.be.a('string')
        expect(fullName).to.contain(nom)
        for (const [k, v] of Object.entries(eleve)) {
          await page.fill(`input[name="${k}"]`, v)
          // await waitMs(2000)
        }
        const buttons = await patternFilter(page, 'a.button', /[Cc][Rr][Éé][Ee][Rr]/)
        expect(buttons).to.have.length(1, 'Pas trouvé le bouton pour valider la création de compte élève')
        await buttons[0].click()
        // https://github.com/microsoft/playwright/blob/master/docs/api.md#pagewaitforfunctionpagefunction-arg-options
        await page.waitForFunction(() => /Compte crée avec succès/.test(document.querySelector('div.title').innerText))
        mails.forEach(mail => {
          cumulMails.push(mail)
        })
        await page.close()
      } catch (error) {
        await page.screenshot({ path: path.resolve(logDir, 'chrome.inscription.eleve.HS.png') })
        const content = await page.content()
        fs.writeFileSync(path.resolve(logDir, 'chrome.inscription.eleve.HS.html'), content)
        if (!store.headless) await page.pause()
        throw error
      }
    })

    it('as reçu un mail avec les infos pour le valider', async () => {
      expect(cumulMails).to.have.length(1)
      const { subject, text, to } = cumulMails[0]
      expect(subject).to.match(/^Bienvenue sur/)
      expect(to).to.equal(eleve.mail)
      const chunks = /Cliquez sur ce lien pour valider votre adresse e-mail : (http[:/.a-z0-9-]+)/.exec(text)
      expect(Array.isArray(chunks)).to.be.true
      validationUrl = chunks[1]
    })

    it('prend une erreur de mail non validé au login (insensible à la casse)', function () {
      async function checkLogin (login) {
        const page = await getPage(pageOptions)
        await page.fill('#login', login)
        await page.fill('#password', password)
        await page.click('.actions button[type="submit"]')
        const errorSelector = '#main div.user div.form-error'
        await page.waitForSelector(errorSelector)
        const errorMessage = await page.innerText(errorSelector)
        expect(errorMessage).to.equal('Votre courriel n’est pas encore activé')
        await page.close()
      }

      this.timeout(60000)
      const { login, password } = eleve
      return Promise.all([
        login,
        ` ${login} `,
        login.toLowerCase(),
        login.toUpperCase(),
        login.substr(0, 3).toUpperCase() + login.substr(3).toLowerCase()
      ].map(checkLogin))
    })

    it('valide l’adresse mail avec le lien donné dans le mail', async function () {
      this.timeout(60000)
      const page = await getPage({ ...pageOptions, url: validationUrl })
      const title = await page.innerText('#main h1')
      expect(title).to.equal('Valider mon compte')
      const bodyText = await page.innerText('#main')
      const { mail } = eleve
      const mailRe = new RegExp(mail)
      expect(bodyText).to.match(mailRe)
      await page.click('#main button')
      await page.waitForSelector('#main h2')
      const reponse = await page.innerText('#main')
      expect(reponse).to.match(/Votre adresse.*a été validée/)
      expect(reponse).to.match(mailRe)
      expect(reponse).to.match(/Les formateurs de votre structure sont avertis/)
      await page.close()
    })

    it('prend ensuite une erreur de compte non activé (par un formateur) au login', async function () {
      this.timeout(60000)
      // on refait pas tous les cas
      const { login, password } = eleve
      const page = await getPage(pageOptions)
      await page.fill('#login', login)
      await page.fill('#password', password)
      await page.click('.actions button[type="submit"]')
      const errorSelector = '#main div.user div.form-error'
      await page.waitForSelector(errorSelector)
      const errorMessage = await page.innerText(errorSelector)
      expect(errorMessage).to.equal('Votre compte n’est pas encore activé par un formateur de la structure')
      await page.close()
    })
  })
}
