/*
Pour les tests fonctionnels, ils sont chaînés (création d'un utilisateur élève, prof, validation par le prof, etc.
On gère l'enchaînement ici
 */
const { boot, checkBiblis, resetDb, shutdown } = require('../../app')
const { init } = require('../browser')

const checkHome = require('./checkHome')
// const createStructure = require('./createStructure')
const createEleve = require('./createEleve')
// const createFormateur = require('./createFormateur')
// const validateEleve = require('./validateEleve')

describe('Flux de création d’utilisateurs', async function scenarioUtilisateur () {
  before(async () => {
    await boot()
    await checkBiblis()
    // nos tests ne font pas de reset, on le fait une fois ici
    await resetDb()
    await init()
  })

  after(shutdown)

  checkHome()
  // createStructure()
  // createFormateur()
  createEleve()
  // validateEleve()
})
