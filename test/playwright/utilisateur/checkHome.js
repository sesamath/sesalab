const { expect } = require('chai')

const { addHooks } = require('../../app')
const { getPage } = require('../browser')

const config = require('config')

module.exports = function run () {
  describe('Boot', async () => {
    addHooks({ doNotResetDb: true })

    it('affiche la home sans erreur en console', async function home () {
      this.timeout(60000)
      const page = await getPage({
        addMessages: true,
        url: config.application.baseUrl
      })
      // https://github.com/microsoft/playwright/blob/master/docs/api.md#elementhandleselector
      const title = await page.innerText('h1')
      expect(title).to.equal(config.application.homeTitle)
      return page.close()
    })
  })
}
