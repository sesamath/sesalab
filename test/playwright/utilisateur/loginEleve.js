const { expect } = require('chai')

const { addHooks } = require('test/app')
const { getPage } = require('../browser')

const config = require('config')

// on fait le test avec un login qui n'est pas dans les fixtures qui init la base
const { eleve } = require('./fixturesSup')

module.exports = function () {
  describe('Se loggue avec le user élève', async () => {
    addHooks({ doNotResetDb: true })

    // pour vérifier ce qui sort en console du navigateur
    const messages = {
      error: [],
      warning: []
    }

    it('avec plusieurs casses du login', async function () {
      this.timeout(60000)
      const { login, password, nom, prenom } = eleve
      const logins = [
        login,
        login.toLowerCase(),
        login.toUpperCase(),
        login.substr(0, 3).toUpperCase() + login.substr(3).toLowerCase()
      ]
      for (const loginChecked of logins) {
        const page = await getPage({
          addMessages: true,
          url: config.application.baseUrl
        })
        await page.fill('#login', loginChecked)
        await page.fill('#password', password)
        await page.click('.actions button[type="submit"]')
        await page.waitForSelector('.sl-header nav ul li')
        expect(page.url()).to.equal(config.application.baseUrl + 'eleve/')
        const displayedName = await page.innerText('.sl-header nav ul li')
        expect(displayedName).to.be.a('string')
        expect(displayedName).to.equal(prenom + ' ' + nom)
        await page.close()
      }
    })

    it('sans message d’erreur', () => {
      if (messages.warning.length) console.log('warnings :\n', messages.warning.join('\n'))
      if (messages.error.length) {
        console.log('errors :\n', messages.error.join('\n'))
        throw Error('Il y a eu du console.error dans le navigateur')
      }
    })
  })
}
