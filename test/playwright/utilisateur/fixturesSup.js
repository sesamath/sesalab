const structures = require('../../fixtures/structures')
// on fait le test avec un login qui n'est pas dans les fixtures qui init la base

module.exports = {
  eleve: {
    nom: 'Élève',
    prenom: 'Création',
    mail: 'eleve-creation@sesamath.net',
    login: 'eleve-Création',
    password: 'açer!t y44',
    passwordBis: 'açer!t y44'
  },
  formateur: {
    nom: 'Formateur',
    prenom: 'Création',
    mail: 'formateur-creation@sesamath.net',
    login: 'formateur-Création',
    password: 'çà c\'est "Dù {pa$$} !',
    passwordBis: 'çà c\'est "Dù {pa$$} !'
  },
  structure: structures.find(s => s.ajoutElevesAuthorises && !s.ajoutFormateursInterdit)
}
