const flow = require('an-flow')
const promisify = require('sesalab-commun/promisify')
const { addHooks } = require('test/app')
const structures = require('../fixtures/structures')
const registerStudentPage = require('./pageobjects/registerStudentPage')
const { waitUntil } = require('./helpers')

describe('browser test register student', () => {
  addHooks()

  beforeEach(() => {
    registerStudentPage.open()
  })

  it('permet la création d’un compte élève', () => {
    const titleElement = registerStudentPage.title
    titleElement.waitForExist(1000)
    expect(titleElement.getText()).to.contain('Créer un nouveau compte')

    // Selection de la structure
    registerStudentPage.rgpd.click()
    registerStudentPage.structure.click()
    registerStudentPage.structure.setValue(structures[0].nom)
    waitUntil(() => registerStudentPage.structureListItems.length > 0)
    registerStudentPage.findStructureInList(structures[0].code).click()
    waitUntil(() => registerStudentPage.classLabel)

    // Formulaire élève
    registerStudentPage.name.setValue('Eleve')
    registerStudentPage.surname.setValue('Creation')
    registerStudentPage.mail.setValue('eleve-creation@sesamath.net')
    registerStudentPage.login.setValue('eleve-creation')
    registerStudentPage.password.setValue('azerty44')
    registerStudentPage.passwordBis.setValue('azerty44')
    registerStudentPage.submit()

    waitUntil(() => registerStudentPage.accountCreatedNotification)
    // browser.pause(10) // bizarre, parfois il faut attendre un peu pour que le check suivant fonctionne…

    // Vérifie la présence de l'utilisateur en BDD
    browser.call(() => promisify((callback) => {
      const Utilisateur = lassi.service('Utilisateur')
      flow().seq(function () {
        Utilisateur
          .match('login')
          .equals('eleve-creation')
          .grabOne(this)
      }).seq(function (utilisateur) {
        expect(utilisateur).not.to.equal(undefined, 'le login eleve-creation n’existe pas en base')
        callback()
      }).catch(callback)
    }))
  })
})
