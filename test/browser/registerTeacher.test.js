const flow = require('an-flow')
const promisify = require('sesalab-commun/promisify')
const { addHooks } = require('test/app')
const structures = require('../fixtures/structures')
const registerTeacherPage = require('./pageobjects/registerTeacherPage.js')
const { waitUntil } = require('./helpers')

describe('browser test register teacher', () => {
  addHooks()

  beforeEach(() => {
    registerTeacherPage.open()
  })

  it('permet la création d’un compte formateur', () => {
    registerTeacherPage.rgpdOk()
    registerTeacherPage.createStructureButton.click()

    // Formulaire structure
    const structure = structures[0]
    waitUntil(() => registerTeacherPage.country)
    registerTeacherPage.country.setValue(structure.pays)
    registerTeacherPage.city.setValue(structure.ville)
    registerTeacherPage.schoolName.setValue(structure.nom)
    registerTeacherPage.structureButton.click()
    waitUntil(() => registerTeacherPage.accountLabel)

    // Formulaire enseignant
    registerTeacherPage.name.setValue('Prof')
    registerTeacherPage.surname.setValue('Creation')
    const userMail = 'prof-creation@sesamath.net'
    registerTeacherPage.mail.setValue(userMail)
    const userLogin = 'prof-creation'
    registerTeacherPage.login.setValue(userLogin)
    registerTeacherPage.password.setValue('azerty44')
    registerTeacherPage.passwordBis.setValue('azerty44')
    registerTeacherPage.submit()

    waitUntil(() => registerTeacherPage.accountValidationNotification)
    // bizarre, mais ce test passe pas toujours, avec 100ms d'attente ça passe à tous les coups
    // comme si y'avait du lag sur mongoDb…
    browser.pause(100)

    let oid, token
    // Vérifie la présence de l'utilisateur en BDD
    browser.call(() => promisify((callback) => {
      const Utilisateur = lassi.service('Utilisateur')
      flow().seq(function () {
        Utilisateur
          .match('login')
          .equals(userLogin)
          .grabOne(this)
      }).seq(function (utilisateur) {
        expect(utilisateur).not.to.equal(undefined)
        oid = utilisateur.oid
        token = utilisateur.validators.mail.token
        callback()
      }).catch(callback)
    }))

    // valide le token sur l'api
    browser.url(`/valider-email/${oid}/${token}`)
    waitUntil(() => $(`div*=Votre adresse de courriel ${userMail} a été validée.`))
    // @todo à implémenter
  })
})
