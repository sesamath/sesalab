const { addHooks } = require('test/app')
const homePage = require('./pageobjects/homePage.js')
const initBibli = require('./initBibli')
const { waitUntil } = require('./helpers')

describe('browser test login teacher', () => {
  addHooks()
  initBibli()

  it('empêche le login d’un utilisateur inconnu', () => {
    homePage.signIn('login-random', 'random')

    waitUntil(() => homePage.errorNotifications.length > 0, 'Timeout => login HS')
    const foundError = homePage.errorNotifications.some(elt => /identifiant inconnu ou mot de passe incorrect/.test(elt.getText()))
    expect(foundError).not.to.be.false
  })

  it('permet la connexion au compte d’un formateur', () => {
    homePage.signIn('prof-valide', 'azerty44')

    waitUntil(() => $('#user-account').getText(), 'login KO')
    expect($('#user-account').getText()).to.contain('Valide Prof')
  })
})
