const flow = require('an-flow')

const promisify = require('sesalab-commun/promisify')
const { DEFAULT_FOLDER_NAME } = require('sesalab-commun/constants')
const { addHooks } = require('test/app')
const { waitUntil } = require('./helpers')
const classesPanelPage = require('./pageobjects/panels/classesPanelPage.js')
const dialogInputPage = require('./pageobjects/dialogInputPage.js')
const homePage = require('./pageobjects/homePage.js')
const ressourcesPanelPage = require('./pageobjects/panels/ressourcesPanelPage.js')
const SequencePage = require('./pageobjects/SequencePage.js')
const sequencesPanelPage = require('./pageobjects/panels/sequencesPanelPage.js')
const teacherPage = require('./pageobjects/teacherPage.js')
const sequences = require('../fixtures/sequences')

const getSequenceCount = () => promisify((callback) => lassi.service('Sequence').match().count(callback))

describe('browser test sequence', () => {
  addHooks()

  it('création d’une séquence', () => {
    const initialCount = browser.call(getSequenceCount)

    homePage.signIn('prof-valide', 'azerty44')

    waitUntil(() => teacherPage.username)
    sequencesPanelPage.open()
    sequencesPanelPage.createSequenceButton.click()
    dialogInputPage.input.setValue('Soustractions')
    dialogInputPage.submit()

    expect(browser.call(getSequenceCount)).to.equal(initialCount + 1)
  })

  it('édition d’une séquence', () => {
    const sequence = sequences[0]
    homePage.signIn('prof-valide', 'azerty44')

    // Ouverture d'une séquence
    waitUntil(() => sequencesPanelPage.panel)
    sequencesPanelPage.open()
    sequencesPanelPage.tree().findNode(DEFAULT_FOLDER_NAME).click()
    sequencesPanelPage.tree().findNode(sequence.nom).doubleClick() // Séquence "Additions"

    // On regarde si un onglet existe
    const sequenceTab = $('.qx-tabview-page-button-top > div').getHTML(`*=${sequence.nom}`)
    waitUntil(() => sequenceTab != null)

    // Tag les zones de drop d'une sous-séquence
    const sequencePage = new SequencePage(sequence.oid)

    // Ouvre les classes
    classesPanelPage.open()
    classesPanelPage.tree().findNode('seconde').click()
    classesPanelPage.tree().findNode('ma classe').click()

    // Drag & drop d'un élève sur l'éditeur de séquences
    const studentName = 'Eleve Valide'
    const studentTag = classesPanelPage.tree().tagNode(studentName)
    const studentsTag = sequencePage.tree().tagNode('Élèves')
    studentTag.dragAndDrop(studentsTag)
    const studentNodeInSequence = sequencePage.tree().findNode(studentName)
    expect(studentNodeInSequence.value).not.to.be.null

    // Ouvre l'arbre des ressources
    ressourcesPanelPage.open()
    ressourcesPanelPage.tree().findNode('Exercices interactifs').click()
    ressourcesPanelPage.tree().findNode('Sans flash').click()

    // Drag & drop d'un exercice sur l'éditeur de séquences
    const serieName = 'Quadricalc niveau 1'
    const serieTag = ressourcesPanelPage.tree().tagNode(serieName)
    const seriesTag = sequencePage.tree().tagNode('Série')
    serieTag.dragAndDrop(seriesTag)
    const serieNodeInSequence = sequencePage.tree().findNode(serieName)
    expect(serieNodeInSequence.value).not.to.be.null

    // Sauvegarde
    sequencePage.submit()

    // Vérifie la présence des données en BDD
    browser.call(() => promisify((callback) => {
      const Sequence = lassi.service('Sequence')
      flow()
        .seq(function () {
          Sequence
            .match('oid')
            .equals(sequence.oid)
            .grabOne(this)
        })
        .seq(function (_sequence) {
          expect(_sequence).not.to.equal(undefined)
          expect(_sequence.sousSequences[0].eleves).to.have.lengthOf(1)
          expect(_sequence.sousSequences[0].eleves[0].nom).to.equal('Eleve')
          expect(_sequence.sousSequences[0].serie).to.have.lengthOf(1)
          expect(_sequence.sousSequences[0].serie[0].titre).to.equal(serieName)
          callback()
        })
        .catch(callback)
    }))
  })
})
