const config = require('../../../config')

class HomePage {
  get errorNotifications () { return $$('div.form-error') }
  get username () { return browser.$('#login') }
  get password () { return browser.$('#password') }
  get submitButton () { return $$('form div.actions button').find(button => /connecter/.test(button.getText())) }

  open () {
    browser.url(config.application.baseUrl)
  }

  submit () {
    this.submitButton.click()
  }

  signIn (login, password) {
    this.open()
    this.username.setValue(login)
    this.password.setValue(password)
    this.submit()
  }
}

module.exports = new HomePage()
