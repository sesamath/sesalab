const { waitUntil } = require('../helpers')

class TreePage {
  get rawContent () { return `#${this.parentName} .tree-content div > div` }
  get content () { return $(this.rawContent) }

  constructor (parentName) {
    this.parentName = parentName
  }

  findNode (name) {
    waitUntil(() => this.content.$(`div*=${name}`))
    return this.content.$(`div*=${name}`)
  }

  /**
   * Trouve un élément de l'arbre à l'aide du context navigateur et lui ajoute un identifiant aléatoire
   * @param {string} nodeText Texte du noeud recherché
   */
  tagNode (nodeText) {
    // Génère un identifiant unique
    // ----------------------------
    // ATTENTION :
    // WebdriverIO semble rencontrer quelques problèmes avec certains caractères ("-" et chiffres)
    // Les sélécteurs (pour le dragAndDrop) fonctionnent 20% du temps avec les caractères ci-dessus
    // ----------------------------
    TreePage.NextIdentifier = String.fromCharCode(TreePage.NextIdentifier.charCodeAt(0) + 1)
    const identifier = `wdio_tree_node_${TreePage.NextIdentifier}`

    function _tag (parent, name, id) {
      let node = null
      const children = document.querySelectorAll(parent)
      for (let i = 0; i < children.length; i++) {
        if (children[i].textContent === name) {
          node = children[i]
          node.id = id
          break
        }
      }

      return node
    }

    // On recherche l'élément puis on le tag
    const taggedNode = browser.execute(_tag, this.rawContent, nodeText, identifier)
    global.assert.isNotNull(taggedNode)

    // On s'assure que l'élément existe bien dans le DOM
    waitUntil(() => $(`#${identifier}`))

    return $(`#${identifier}`)
  }
}

TreePage.NextIdentifier = 'a'

module.exports = TreePage
