class DialogInputPage {
  get input () { return $('.input-dialog input') }

  submit () {
    $$('.input-dialog .qx-button-box')[0].click()
  }
}

module.exports = new DialogInputPage()
