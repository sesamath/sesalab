const PanelPage = require('./PanelPage')

class ClassesPanelPage extends PanelPage {
  name () {
    return 'panel-classes'
  }
}

module.exports = new ClassesPanelPage()
