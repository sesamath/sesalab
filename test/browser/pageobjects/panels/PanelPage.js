const TreePage = require('../TreePage')

class PanelPage {
  get panel () { return $(`#${this.name()}`) }

  tree () {
    if (!this.treePage) {
      this.treePage = new TreePage(this.name())
    }

    return this.treePage
  }

  open () {
    this.panel.click()
  }

  name () {
    return ''
  }
}

module.exports = PanelPage
