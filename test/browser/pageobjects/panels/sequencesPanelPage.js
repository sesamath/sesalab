const PanelPage = require('./PanelPage')

class SequencesPanelPage extends PanelPage {
  get createSequenceButton () { return $('#sequence-creation') }
  name () {
    return 'panel-mes-sequences'
  }
}

module.exports = new SequencesPanelPage()
