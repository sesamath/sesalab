const PanelPage = require('./PanelPage')

class RessourcesPanelPage extends PanelPage {
  name () {
    return 'panel-ressources'
  }
}

module.exports = new RessourcesPanelPage()
