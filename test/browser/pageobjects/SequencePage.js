const TreePage = require('./TreePage')

class SequencePage {
  get title () { return $$('div.form-error') }
  get name () { return `sequence-editor-${this.oid}` }
  get editor () { return $(`#${this.name}`) }

  constructor (oid) {
    this.oid = oid
    this.treePage = new TreePage(this.name)
  }

  submit () {
    return $('#sequence-save').click()
  }

  tree () {
    return this.treePage
  }
}

module.exports = SequencePage
