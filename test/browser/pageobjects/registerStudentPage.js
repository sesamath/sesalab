const _ = require('lodash')
const config = require('../../../config')

class RegisterStudentPage {
  get title () { return browser.$('div.title') }
  get name () { return browser.$('input[name="nom"]') }
  get surname () { return browser.$('input[name="prenom"]') }
  get mail () { return browser.$('input[name="mail"]') }
  get login () { return browser.$('input[name="login"]') }
  get password () { return browser.$('input[name="password"]') }
  get passwordBis () { return browser.$('input[name="passwordBis"]') }
  get rgpd () { return browser.$('#rgpd') }
  get structure () { return browser.$('input[name="structure"]') }
  get structureList () { return browser.$('ul.structure-drop-down') }
  get structureListItems () { return $$('ul.structure-drop-down li') } // browser.elements ne fonctionne pas ici, bug de webdriverio ?
  get classLabel () { return browser.$('body div.content form label') }
  get accountCreatedNotification () { return browser.$('div.title*=Compte crée avec succès') }

  open () {
    browser.url(config.application.baseUrl + 'eleve/inscription')
  }

  submit () {
    $$('a.button*=Créer')[0].click()
  }

  findStructureInList (structureCode) {
    const listItems = $$('ul.structure-drop-down li span.code')
    return _.find(listItems, (item) => {
      return item.getText() === structureCode
    })
  }
}

module.exports = new RegisterStudentPage()
