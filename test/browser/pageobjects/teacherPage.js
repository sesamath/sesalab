const config = require('../../../config')

class TeacherPage {
  get username () { return browser.$('#user-account') }

  open () {
    browser.url(config.application.baseUrl + 'formateur')
  }
}

module.exports = new TeacherPage()
