const config = require('../../../config')

class RegisterTeacherPage {
  get createStructureButton () { return $$('div.qx-radiobutton')[1] }
  get country () { return $$('input.qx-abstract-field')[1] }
  get city () { return $$('input.qx-abstract-field')[2] }
  get schoolName () { return $$('input.qx-abstract-field')[3] }
  get structureButton () { return $$('div.qx-button-box')[1] }
  get accountLabel () { return browser.$('div.qx-button-box*=Créer le compte') }

  get name () { return $$('input.qx-abstract-field')[0] }
  get surname () { return $$('input.qx-abstract-field')[1] }
  get mail () { return $$('input.qx-abstract-field')[2] }
  get login () { return $$('input.qx-abstract-field')[3] }
  get password () { return $$('input.qx-abstract-field')[4] }
  get passwordBis () { return $$('input.qx-abstract-field')[5] }

  get accountValidationNotification () { return browser.$('div*=Un mail a été envoyé à') }

  open () {
    browser.url(config.application.baseUrl + 'formateur/inscription/')
  }

  rgpdOk () {
    browser.waitUntil(() => $$('div.qx-checkbox')[0])
    const cbx = $$('div.qx-checkbox')
    if (cbx && cbx.length) cbx[0].click()
    else console.error(Error('Pas trouvé de checkbox'))
  }

  submit () {
    $('div.qx-button-box*=Créer le compte').click()
  }
}

module.exports = new RegisterTeacherPage()
