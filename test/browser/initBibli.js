// script qui doit être appelé dans un describe, pour y ajouter un before, une fois que lassi est chargé
const client = require('sesatheque-client/dist/server/nodeClient')
const st = require('sesatheque-client/dist/server/sesatheques')

const getBibliRessources = require('../fixtures/getBibliRessources')

let initBibliPromise

/**
 * Ajoute l'init de la bibli en before (à mettre après un describe)
 */
module.exports = () => {
  /**
   * Enregistre les ressources sur la bibli de test
   */
  before(function () {
    // on met 10s pour être tranquille
    this.timeout(10000)
    if (!initBibliPromise) {
      const $settings = lassi.service('$settings')
      const sesatheques = $settings.get('application.sesatheques')
      const bibli = sesatheques[0]
      const { apiToken, baseId, baseUrl } = bibli
      if (!apiToken) return Promise.reject(new Error('Pour lancer les test browser, il faut un apiToken pour la 1re sesathèque en configuration'))
      // faut ajouter cette sésathèque au cas où il la connait pas
      st.addSesatheque(baseId, baseUrl)
      // on veut un Array et pas un object avec des rid en propriété
      const ressources = Object.values(getBibliRessources(baseId))
      initBibliPromise = Promise.all(ressources.map(ressource => client.saveRessource(baseId, ressource, apiToken)))
    }
    return initBibliPromise
  })
}
