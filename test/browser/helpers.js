module.exports = {
  /**
   * Wrapper de browser.waitUntil avec des options par défaut ({@link https://webdriver.io/docs/api/browser/waitUntil.html})
   * @param {function} condition
   * @param {Object|number|string} options Si string sera pris pour timeoutMessage, si number pour timeout, si object on ajoutera éventuellement nos timeout & timeoutMessage, le reste sera transmis tel quel
   * @param {number} [options.timeout]
   * @param {string} [options.timeoutMessage]
   */
  waitUntil: function waitUntil (condition, options) {
    if (typeof options === 'number') options = { timeout: options }
    else if (typeof options === 'string') options = { timeoutMessage: options }
    else if (!options) options = {}
    if (!options.timeout) options.timeout = 10000
    if (!options.timeoutMessage) options.timeoutMessage = 'Timeout :-/'
    browser.waitUntil(condition, options)
  }
}
