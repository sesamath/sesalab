const { addHooks } = require('../app')
const homePage = require('./pageobjects/homePage.js')
const { waitUntil } = require('./helpers')

const selector = '#home > div.sl-header > nav > ul > li.ng-binding.ng-scope'

describe('browser test login student', () => {
  addHooks()

  it('permet la connexion au compte d’un élève', () => {
    const getUsername = () => $(selector).getText()

    homePage.signIn('eleve-valide', 'azerty44')

    waitUntil(getUsername, `timeout, pas trouvé le sélecteur ${selector}`)
    expect(getUsername()).to.contain('Valide Eleve')
  })
})
