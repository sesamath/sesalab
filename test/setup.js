// Env variables must be defined before loading app, because they are
// used to configure the app
process.env.NODE_ENV = 'test'
const path = require('path')
const rootPathSetup = require('../source/rootPathSetup')

// pour faire des require relatifs à la racine, utilisé un peu partout (notamment pour sesalab-common/…)
const rootDir = path.join(__dirname, '..')
rootPathSetup(rootDir)

const appLog = require('sesalab-api/source/tools/appLog')
appLog.setLogLevel('warning')

// require('@babel/register')({
//   presets: ['env']
// })
// Load test libraries
const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
chai.use(sinonChai)

// pour pouvoir utiliser la syntaxe chai .to.be.true ou expect(mySpy).to.have.been.calledOnce
// on a mis no-unused-expressions à off pour les fichier *.test.js
// en global dans la conf eslint (package.json)

// Global variables
global.expect = chai.expect
global.chai = chai
global.sinon = sinon
global.assert = chai.assert
global.should = chai.should()
