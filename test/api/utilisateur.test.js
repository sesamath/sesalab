const { addHooks } = require('../app')
const {
  createEleve,
  createEleveByFormateur,
  createFormateur,
  validateMailUtilisateur,
  validateAccountUtilisateur,
  expectIndexedOnStructure,
  saveResultat
} = require('./helpers')

// services Lassi chargés dans le before
let Backup, Journal, Utilisateur, $validators

describe('API', () => {
  const mails = []
  addHooks({ mails })

  before(() => {
    Backup = lassi.service('Backup')
    Journal = lassi.service('Journal')
    Utilisateur = lassi.service('Utilisateur')
    $validators = lassi.service('$validators')
  })

  let response
  beforeEach(() => {
    response = null // pour être sûr que `response` est bien assigné dans le test
  })

  const errorMessage = (expected, got) => `Expected ${JSON.stringify(expected)}, got ${JSON.stringify(got)}`

  const itIsSuccessfull = () => {
    it('renvoie une 200', () => {
      expect(response.status).to.equal(200)
      expect(response.body.success).to.equal(true, errorMessage({ success: true }, response.body))
    })
  }

  const itFails = (expectedErrorMessage) => {
    it(`renvoie une 200 avec une erreur "${expectedErrorMessage}"`, () => {
      expect(response.status).to.equal(200)
      expect(response.body.success).to.equal(false, errorMessage({ success: false }, response.body))
      expect(response.body.message).to.equal(expectedErrorMessage)
    })
  }

  const itNeedsAuth = (expectedErrorMessage, expectedUrlRedirect) => {
    it(`renvoie une 401 avec une erreur "${expectedErrorMessage}"`, () => {
      expect(response.status).to.equal(401)
      expect(response.body.success).to.equal(false, errorMessage({ success: false }, response.body))
      expect(response.body.message).to.equal(expectedErrorMessage)
      expect(response.body.urlRedirect).to.equal(expectedUrlRedirect)
    })
  }

  describe('POST api/formateur', () => {
    context('with correct data', () => {
      let utilisateur
      beforeEach((done) => {
        createFormateur('AAAAA', (err, res) => {
          response = res
          done(err)
        })
      })
      beforeEach((done) => {
        Utilisateur.match('login').equals('nouveau-formateur').grabOne((e, u) => {
          utilisateur = u
          done(e)
        })
      })

      itIsSuccessfull()

      it('cree un formateur avec un mail à valider', () => {
        expect(utilisateur.nom).to.equal('Formateur')
        expect(utilisateur.validators.mail.token).to.exist
        // null a été viré par lassi au beforeStore, on récupère donc une entité sans la propriété
        expect(utilisateur.validators.mail).not.to.have.property('responseDate')
        expect(mails.length).to.equal(1)
        const { to, text } = mails[0]
        expect(to).to.equal('prof-nouveau@sesamath.net')
        expect(text).to.contain('identifiant nouveau-formateur')
        expect(text).to.contain(utilisateur.validators.mail.token)
      })
    })

    context('with a student login existing from the same structure', () => {
      beforeEach((done) => {
        createFormateur('AAAAA' /* structure 1 */, { login: 'eleve-valide' }, (err, res) => {
          response = res
          done(err)
        })
      })

      itFails('Désolé, cet identifiant est déjà pris')
    })

    context('with a student login existing from another structure', () => {
      beforeEach((done) => {
        createFormateur('AAAAB' /* structure 2 */, { login: 'eleve-valide' }, (err, res) => {
          response = res
          done(err)
        })
      })

      itIsSuccessfull()
    })

    context('with a teacher login from another structure', () => {
      beforeEach((done) => {
        createFormateur('AAAAB' /* structure 2 */, { login: 'prof-valide' }, (err, res) => {
          response = res
          done(err)
        })
      })

      itFails('Désolé, cet identifiant est déjà pris')
    })

    context('with incorrect data', () => {
      beforeEach((done) => {
        createFormateur('AAAAC', (err, res) => {
          response = res
          done(err)
        })
      })

      it('refuse la creation d’un formateur sur une structure "privée"', () => {
        expect(response.status).to.equal(200)
        expect(response.body.success).to.equal(false)
        expect(response.body.message).to.equal('La structure interdit la création de compte formateur')
      })
    })
  })

  describe('POST api/eleve', () => {
    context('with correct data', () => {
      let utilisateur

      beforeEach((done) => {
        createEleve('AAAAA', '1', (err, res) => {
          response = res
          done(err)
        })
      })
      beforeEach((done) => {
        Utilisateur.match('login').equals('nouvel-eleve').grabOne((e, u) => {
          utilisateur = u
          done(e)
        })
      })

      itIsSuccessfull()

      it('cree un eleve avec un mail à valider', () => {
        expect(utilisateur.nom).to.equal('Eleve')
        expect(utilisateur.validators.mail.token).to.exist
        expect(utilisateur.validators.mail).not.to.have.property('responseDate')
        expect(mails.length).to.equal(1)
        expect(mails[0].to).to.equal('eleve-nouveau@sesamath.net')
        expect(mails[0].text).to.contain('identifiant nouvel-eleve')
        expect(mails[0].text).to.contain(utilisateur.validators.mail.token)
      })
    })

    context('as a teacher', () => {
      let eleve

      beforeEach((done) => createEleveByFormateur('1', (err, res) => {
        response = res
        done(err)
      }))
      beforeEach((done) => {
        Utilisateur.match('login').equals('nouvel-eleve-formateur').grabOne((e, u) => {
          eleve = u
          done(e)
        })
      })

      itIsSuccessfull()

      it('cree un eleve depuis un compte formateur', () => {
        expect(eleve).to.exist
        expect(eleve.validators).to.exist
        expect(eleve.validators.account).to.exist
        expect(eleve.validators.mail).to.not.exist
      })

      it('indexe le compte sur la structure', (done) => {
        expectIndexedOnStructure(eleve, true, done)
      })

      it('vérifie que l’utilisateur n’est pas en attente de validation', () => {
        expect($validators.isValid(eleve)).to.be.true
      })
    })
  })

  describe('GET api/utilisateur/validateMail/:oid/:token', () => {
    context('sur un formateur', () => {
      context('pour le premier compte d’une structure', () => {
        let utilisateur
        beforeEach((done) => createFormateur('AAAAB', done)) // <-- structure sans aucun formateur valide
        beforeEach((done) => validateMailUtilisateur('nouveau-formateur', (err, res) => {
          response = res
          done(err)
        }))
        beforeEach((done) => {
          Utilisateur.match('login').equals('nouveau-formateur').grabOne((e, u) => {
            utilisateur = u
            done(e)
          })
        })

        itIsSuccessfull()

        it('valide le mail et le compte', () => {
          expect(utilisateur.validators.mail.responseDate).to.be.a('date')
          expect(utilisateur.validators.account.responseDate).to.be.a('date')
          expect(utilisateur.validators.account.accepted).to.equal(true)
          // Un seul mail envoyé, celui de la validation de mail
          expect(mails.length).to.equal(1)
        })

        it('indexe le compte sur la structure', (done) => {
          expectIndexedOnStructure(utilisateur, true, done)
        })

        it('vérifie que l’utilisateur n’est pas en attente de validation', () => {
          expect($validators.isValid(utilisateur)).to.be.true
        })
      })

      context('pour une structure ayant déjà un formateur valide', () => {
        let utilisateur

        beforeEach((done) => createFormateur('AAAAA', done)) // <-- structure ayant au moins un formateur valide
        beforeEach((done) => validateMailUtilisateur('nouveau-formateur', (err, res) => {
          response = res
          done(err)
        }))
        beforeEach((done) => {
          Utilisateur.match('login').equals('nouveau-formateur').grabOne((e, u) => {
            utilisateur = u
            done(e)
          })
        })

        itIsSuccessfull()

        it('valide le mail mais pas le compte', () => {
          expect(utilisateur.validators.mail.responseDate).to.be.a('date')
          expect(utilisateur.validators.account).not.to.have.property('responseDate')

          // Contient le mail de validation de mail (testé plus haut) et un mail de validation
          // de compte envoyé au formateur valide
          expect(mails.length).to.equal(2)
          // mail pour valider le mail du nouveau prof
          expect(mails[0].to).to.equal(utilisateur.mail)
          expect(mails[0].text).to.contain(utilisateur.validators.mail.token)
          expect(mails[0].text).to.match(/Bienvenue.*\n\nVous avez créé un compte d’identifiant nouveau-formateur/m)
          // mail de demande de validation au prof valide
          expect(mails[1].to).to.equal('prof-valide@sesamath.net')
          // même en multiligne "." ne match pas \n,
          // faut utiliser [^] pour matcher tout caractère \n compris
          expect(mails[1].text).to.match(/Bonjour[^]*prof-nouveau@sesamath.net.*a créé[^]*Merci de valider ou refuser son accès[^]*Son compte sera automatiquement supprimé/m)
        })

        it('indexe le compte sur la structure', (done) => {
          expectIndexedOnStructure(utilisateur, true, done)
        })

        it('vérifie que l’utilisateur est en attente de validation', () => {
          expect($validators.isValid(utilisateur)).to.be.null
        })
      })
    })

    context('sur un élève', () => {
      // Note: on ne peut pas créer de compte élève sur une structure sans formateur car il n'y a pas de classe
      //       où s'assigner
      context('sur une structure ayant déjà un formateur valide', () => {
        let utilisateur

        beforeEach((done) => createEleve('AAAAA', '1', done))
        beforeEach((done) => validateMailUtilisateur('nouvel-eleve', (err, res) => {
          response = res
          done(err)
        }))
        beforeEach((done) => {
          Utilisateur.match('login').equals('nouvel-eleve').grabOne((e, u) => {
            utilisateur = u
            done(e)
          })
        })

        itIsSuccessfull()

        it('valide le mail mais pas le compte', () => {
          expect(utilisateur.validators.mail.responseDate).to.be.a('date')
          expect(utilisateur.validators.account).not.to.have.property('responseDate')

          // Contient le mail de validation de mail (testé plus haut) et un mail de validation
          // de compte envoyé au formateur valide
          expect(mails.length).to.equal(2)
          expect(mails[1].to).to.equal('prof-valide@sesamath.net')
          expect(mails[1].text).to.match(/Bonjour Valide Prof.*\n\nNouvel Eleve.*a créé[^]*Merci de valider ou refuser/m)
        })

        it('indexe le compte sur la structure', (done) => {
          expectIndexedOnStructure(utilisateur, true, done)
        })

        it('vérifie que l’utilisateur est en attente de validation', () => {
          expect($validators.isValid(utilisateur)).to.be.null
        })
      })
    })
  })

  describe('GET api/utilisateur/validate/:oid/:action', () => {
    let decision
    let utilisateur
    let userCreated

    const expectAccountAccepted = () => {
      itIsSuccessfull()

      it('valide le compte', () => {
        expect(utilisateur.validators.account.responseDate).to.be.a('date')
        expect(utilisateur.validators.account.accepted).to.equal(true)
      })

      it('indexe le compte sur la structure', (done) => {
        expectIndexedOnStructure(utilisateur, true, done)
      })
    }

    const expectAccountRefused = () => {
      itIsSuccessfull()
      it('ne valide pas le compte', (done) => {
        expect(utilisateur).to.equal(undefined)
        Backup.match('id').equals(`Utilisateur-${userCreated.oid}`).grab((error, backups) => {
          if (error) return done(error)
          expect(backups).to.have.length(1)
          const user = backups[0].entity
          expect(user.oid).to.equals(userCreated.oid)
          // et on vérifie l'entrée de Journal
          Journal.match('id').equals(`Utilisateur-${userCreated.oid}`).grab((error, logs) => {
            if (error) return done(error)
            expect(logs).to.have.length(1)
            const { message } = logs[0]
            expect(message).to.equals('Refus de la création du compte par un formateur de la structure')
            done()
          })
        })
      })
    }

    afterEach(() => Utilisateur.match().purge()
      .then(() => Backup.match().purge())
    )

    context('sur un compte formateur', () => {
      beforeEach((done) => {
        utilisateur = null
        userCreated = null
        createFormateur('AAAAA', (error, res) => {
          if (error) return done(error)
          userCreated = res._body.utilisateur
          done()
        })
      })
      beforeEach((done) => validateMailUtilisateur('nouveau-formateur', done))
      beforeEach((done) => validateAccountUtilisateur('nouveau-formateur', decision, (err, res) => {
        response = res
        done(err)
      }))
      beforeEach((done) => {
        Utilisateur.match('login').equals('nouveau-formateur').grabOne((e, u) => {
          utilisateur = u
          done(e)
        })
      })

      context('avec un accept', () => {
        // ici on s'appuie sur le fait que les before sont joués avant les beforeEach...
        before(() => {
          decision = 'accept'
        })

        expectAccountAccepted()
      })

      context('avec un refus', () => {
        // ici on s'appuie sur le fait que les before sont joués avant les beforeEach...
        before(() => {
          decision = 'decline'
        })
        expectAccountRefused()
      })
    })

    context('sur un compte eleve', () => {
      beforeEach((done) => createEleve('AAAAA', '1', (error, res) => {
        if (error) return done(error)
        userCreated = res._body.utilisateur
        done()
      }))
      beforeEach((done) => validateMailUtilisateur('nouvel-eleve', done))
      beforeEach((done) => validateAccountUtilisateur('nouvel-eleve', decision, (err, res) => {
        response = res
        done(err)
      }))
      beforeEach((done) => {
        Utilisateur.match('login').equals('nouvel-eleve').grabOne((e, u) => {
          utilisateur = u
          done(e)
        })
      })

      context('avec un accept', () => {
        before(() => { // ici on s'appuie sur le fait que les before sont joués avant les beforeEach...
          decision = 'accept'
        })

        expectAccountAccepted()
      })

      context('avec un refus', () => {
        before(() => { // ici on s'appuie sur le fait que les before sont joués avant les beforeEach...
          decision = 'decline'
        })
        expectAccountRefused()
      })
    })
  })

  describe('POST /api/resultat/sauvegarde', () => {
    context('nécessite une session valide', () => {
      beforeEach((done) => {
        saveResultat((err, res) => {
          response = res
          done(err)
        })
      })

      itNeedsAuth('Session invalide ou expirée', '/lost-session')
    })
  })
})
