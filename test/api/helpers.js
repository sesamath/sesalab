const flow = require('an-flow')
const path = require('path')
const { TYPE_FORMATEUR, TYPE_ELEVE } = require('sesalab-commun/constants')
const { getSupertestAgent } = require('../app')

const Utilisateur = () => lassi.service('Utilisateur')

/**
 * Log un utilisateur et le retourne à la fonction de callback.
 * @param {string} login
 * @param {string} password
 * @param {function} callback
 */
function logInUtilisateur (agent, login, password, callback) {
  flow()
    .seq(function () {
      agent
        .post('/api/login')
        .type('application/json')
        .send({
          users: [{ login, password }]
        })
        .end(this)
    })
    .seq(function () {
      Utilisateur().match('login').equals(login).grabOne(this)
    })
    .done(callback)
}
exports.logInUtilisateur = logInUtilisateur

/**
 * Crée un formateur via un appel API
 * @param {string} structureCode
 * @param {Object} [overrideData]
 * @param {function} done
 */
exports.createFormateur = (structureCode, overrideData, done) => {
  if (typeof overrideData === 'function') {
    done = overrideData
    overrideData = {}
  }

  getSupertestAgent()
    .post('/api/formateur')
    .send({
      type: TYPE_FORMATEUR,
      login: 'nouveau-formateur',
      mail: 'prof-nouveau@sesamath.net',
      nom: 'Formateur',
      prenom: 'Nouveau',
      password: 'top-secret1',
      passwordBis: 'top-secret1',
      structureCode,
      ...overrideData
    })
    .then((res) => done(null, res))
    .catch(done)
}

/**
 * Crée un élève via un appel API
 * @param {string} structureCode
 * @param {string} classeOid
 * @param {function} done
 */
exports.createEleve = (structureCode, classeOid, done) => {
  getSupertestAgent()
    .post('/api/eleve')
    .send({
      type: TYPE_ELEVE,
      classe: classeOid,
      login: 'nouvel-eleve',
      nom: 'Eleve',
      prenom: 'Nouvel',
      mail: 'eleve-nouveau@sesamath.net',
      password: 'top-secret1',
      passwordBis: 'top-secret1',
      structureCode
    })
    .then((res) => done(null, res))
    .catch(done)
}

/**
 * En tant que Prof Valide (fixtures), crée le compte d'un nouvel élève
 * @param {string} classeOid
 * @param {function} done
 */
exports.createEleveByFormateur = (classeOid, done) => {
  const agent = getSupertestAgent()
  flow()
    .seq(function () {
      logInUtilisateur(agent, 'prof-valide', 'azerty44', this)
    })
    .seq(function (_formateur) {
      agent
        .post('/api/eleve')
        .send({
          type: TYPE_ELEVE,
          classe: classeOid,
          login: 'nouvel-eleve-formateur',
          nom: 'Eleve formateur',
          prenom: 'Nouvel',
          password: 'top-secret1',
          structures: [_formateur.structures[0]]
        })
        .then((res) => done(null, res))
        .catch(this)
    })
    .catch(done)
}

/**
 * Valide le mail d'un nouvel utilisateur
 * @param {string} login login de l'utilisateur à valider
 * @param {function} done
 */
exports.validateMailUtilisateur = (login, done) => {
  flow()
    .seq(function () {
      Utilisateur().match('login').equals(login).grabOne(this)
    })
    .seq(function (utilisateur) {
      const token = utilisateur.validators.mail.token
      const oid = utilisateur.oid
      getSupertestAgent()
        .get(`/api/utilisateur/validateMail/${oid}/${token}`)
        .then((res) => this(null, res))
        .catch(this)
    })
    .done(done)
}

/**
 * En tant que Prof Valide (fixtures), valide le compte d'un nouvel utilisateur
 * @param {string} login login de l'utilisateur a valider
 * @param {string} action "accept" ou "decline"
 * @param {function} done
 */
exports.validateAccountUtilisateur = (login, action, done) => {
  // L'agent permet de garder les cookies
  const agent = getSupertestAgent()

  flow()
    .seq(function () {
      // On se connecte en tant que premier compte de la structure afin de valider le compte en attente de validation
      logInUtilisateur(agent, 'prof-valide', 'azerty44', this)
    })
    .seq(function () {
      Utilisateur().match('login').equals(login).grabOne(this)
    })
    .seq(function (utilisateur) {
      agent
        .get(`/api/utilisateur/validate/${utilisateur.oid}/${action}`)
        .then((res) => this(null, res))
        .catch(this)
    })
    .done(done)
}

/**
 * Vérifie si l'utilisateur est indexé ou non sur sa structure
 * @param {object} utilisateur
 * @param {boolean} bool true pour vérifier qu'il est indexé, false pour vérifier qu'il ne l'est pas
 * @param {function} done
 */
exports.expectIndexedOnStructure = (utilisateur, bool, done) => {
  const { login, structures: [structure] } = utilisateur
  Utilisateur()
    .match('structures').equals(structure)
    .match('login').equals(login)
    .grabOne((err, utilisateur) => {
      if (err) return done(err)
      if (bool && !utilisateur) return done(new Error(`l’utilisateur ${login} n’est pas indexé sur la structure ${structure} alors qu’il devrait l’être`))
      if (!bool && utilisateur) return done(new Error(`l’utilisateur ${login} est indexé sur la structure ${structure} alors qu’il ne devrait pas l’être`))
      done()
    })
}

/**
 * Sauvegarde un résultat via un appel API
 * @param {function} done
 */
exports.saveResultat = (done) => {
  getSupertestAgent()
    .post('/api/resultat/sauvegarde')
    .send({})
    .then((res) => done(null, res))
    .catch(done)
}

/**
 * Permet l'import de classes et d'élèves via un appel API
 * @param {string} fileName Nom du fichier à importer depuis le dossier "files"
 * @param {string} type Chaine avec la valeur "onde" ou "siecle"
 * @param {string} structureOid Identifiant de la structure sur laquelle importer les données
 * @param {string} loginFormat Format des logins à respecter
 */
exports.importClassesAndStudentsFromFile = (fileName, type, structureOid, loginFormat, next) => {
  const agent = getSupertestAgent()

  flow().seq(function () {
    logInUtilisateur(agent, 'prof-valide', 'azerty44', this)
  }).seq(function () {
    agent
      .post(`/api/import-${type}`)
      .field('structureOid', structureOid)
      .field('loginFormat', loginFormat)
      .attach('file', path.join(__dirname, `../fixtures/imports/${fileName}`))
      .then(res => this(null, res))
      .catch(this)
  }).done(next)
}
