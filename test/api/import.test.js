'use strict'

const flow = require('an-flow')
const { addHooks, getSupertestAgent } = require('../app')
const { eleves } = require('../fixtures/imports/tableur')
const { logInUtilisateur, importClassesAndStudentsFromFile } = require('./helpers')

let $cache

describe('Import', () => {
  addHooks()

  beforeEach((done) => {
    if (!$cache) $cache = lassi.service('$cache')
    // tous nos tests sont sur la structureOid 1
    $cache.delete('processing_import_1', () => {
      done()
    })
  })

  describe('import tableur', () => {
    it('ajoute des classes et des élèves', (done) => {
      const agent = getSupertestAgent()
      flow()
        .seq(function () {
          logInUtilisateur(agent, 'prof-valide', 'azerty44', this)
        })
        .seq(function () {
          agent
            .put('/api/import-tableur')
            .send({
              structureOid: '1',
              loginFormat: 'nom.prenom',
              eleves
            })
            .then(res => this(null, res))
            .catch(this)
        })
        .seq(function (response) {
          if ((response.status !== 200 || !response.body.success) && response.body.message) return done(Error(`import HS : ${response.body.message}`))
          expect(response.status).to.equal(200)
          const { stats } = response.body
          expect(stats.eleves).to.have.length(2)
          const [eleve1, eleve2] = stats.eleves
          expect(eleve1.nom).to.equal('IMPORT')
          expect(eleve1.prenom).to.equal('Eleve 1')
          expect(eleve1.login).to.equal('import.eleve1')
          expect(eleve2.nom).to.equal('IMPORT')
          expect(eleve2.prenom).to.equal('Eleve 2')
          expect(eleve2.login).to.equal('import.eleve2')

          expect(stats.nbUsersAdded).to.equal(2)
          expect(stats.nbGroupsAdded).to.equal(2)
          expect(stats.errors).to.have.length(0)

          done()
        })
        .catch(done)
    })
  })

  describe('import siecle', () => {
    it('ajoute des classes et des élèves', (done) => {
      importClassesAndStudentsFromFile('siecle.xml', 'siecle', '1', 'nom.prenom', (error, response) => {
        if (error) return done(error)
        expect(response.status).to.equal(200)
        if (!response.body.success && response.body.message) return done(Error(response.body.message))
        const { stats: { errors, eleves, nbUsersAdded, nbGroupsAdded } } = response.body
        expect(errors).to.have.length(0)
        expect(eleves).to.have.length(2)
        const [eleve1, eleve2] = eleves
        expect(eleve1.nom).to.equal('IMPORT')
        expect(eleve1.prenom).to.equal('Eleve 1')
        expect(eleve1.login).to.equal('import.eleve1')
        expect(eleve2.nom).to.equal('IMPORT')
        expect(eleve2.prenom).to.equal('Eleve 2')
        expect(eleve2.login).to.equal('import.eleve2')

        expect(nbUsersAdded).to.equal(2)
        expect(nbGroupsAdded).to.equal(2)

        done()
      })
    })

    it('ignore un fichier dans un format incorrect', (done) => {
      const consoleErrorStub = sinon.stub(console, 'error')
      const end = (error) => {
        consoleErrorStub.restore()
        done(error)
      }
      importClassesAndStudentsFromFile('onde.csv', 'siecle', '1', 'nom.prenom', (error, response) => {
        if (error) return end(error)
        expect(response.status).to.equal(200)
        expect(response.body.success).to.equal(false)
        expect(response.body.message).to.match(/Extension de fichier non gérée : csv/)
        end()
      })
    })
  })

  describe('import onde', () => {
    it('ajoute des classes et des élèves', (done) => {
      importClassesAndStudentsFromFile('onde.csv', 'onde', '1', 'nom.prenom', (error, response) => {
        if (error) return done(error)
        expect(response.status).to.equal(200)
        const { stats } = response.body
        const [eleve1, eleve2] = stats.eleves
        expect(stats.eleves.length).to.equal(4)
        expect(eleve1.nom).to.equal('IMPORT')
        expect(eleve1.prenom).to.equal('Eleve 1')
        expect(eleve1.login).to.equal('import.eleve1')
        expect(eleve2.nom).to.equal('IMPORT')
        expect(eleve2.prenom).to.equal('Eleve 2')
        expect(eleve2.login).to.equal('import.eleve2')

        expect(stats.nbUsersAdded).to.equal(4)
        expect(stats.nbGroupsAdded).to.equal(3)
        expect(stats.errors).to.have.length(0)

        done()
      })
    })

    it('ignore un fichier dans un format incorrect', (done) => {
      const consoleErrorStub = sinon.stub(console, 'error')
      const end = (error) => {
        consoleErrorStub.restore()
        done(error)
      }
      importClassesAndStudentsFromFile('siecle.xml', 'onde', '1', 'nom.prenom', (error, response) => {
        if (error) return end(error)
        expect(response.status).to.equal(200)
        expect(response.body.success).to.equal(false)
        expect(response.body.message).to.match(/Extension de fichier non gérée : xml/)
        end()
      })
    })
  })
})
