let Groupe

// crée un groupe en bdd avec ces valeurs par défaut, surchargées par groupeData
exports.createGroupe = (groupeData, cb) => {
  if (!Groupe) Groupe = lassi.service('Groupe')
  Groupe.create(Object.assign(
    {
      utilisateurs: [],
      structure: '1',
      niveau: '2',
      nom: 'autre groupe',
      isClass: true
    },
    groupeData
  )).store(cb)
}
