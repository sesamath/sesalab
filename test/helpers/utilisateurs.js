// Permet de créer un élève avec un jeu de données partiel.
// ex: createEleve(Utilisateur, {login: 'test'}, cb)
exports.createEleve = (Utilisateur, eleveData, cb) => {
  Utilisateur.create(Object.assign(
    {
      classe: '1',
      nom: 'Nouvel',
      prenom: 'Eleve',
      mail: 'eleve-nouveau@sesamath.net',
      login: 'eleve-nouveau',
      password: 'aaaaaaa',
      type: 0,
      structures: ['1']
    },
    eleveData
  )).store(cb)
}

exports.createProf = (Utilisateur, profData, cb) => {
  Utilisateur.create(Object.assign(
    {
      nom: 'Prof',
      prenom: 'Valide',
      mail: 'prof-nouveau@sesamath.net',
      login: 'prof-nouveau',
      password: 'aaaaaaa',
      type: 1,
      created: new Date(),
      structures: ['1'],
      cguAccepted: true,
      bilanWithSymbols: false,
      fontSize: 'default'
    },
    profData
  )).store(cb)
}
