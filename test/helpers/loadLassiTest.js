const flow = require('an-flow')
const { addHooks } = require('../app')
const { createEleve } = require('./utilisateurs')
const userFixtures = require('../fixtures/utilisateurs')

/**
 * Test de lassi commun à test/unit et test/browser
 */
module.exports = () => {
  describe('boot & resetDb', () => {
    addHooks()

    it('initialize lassi and loads fixtures', () => new Promise((resolve, reject) => {
      const Utilisateur = lassi.service('Utilisateur')
      Utilisateur
        .match('login')
        .equals('prof-valide')
        .grabOne((error, prof) => {
          if (error) return reject(error)
          expect(prof.nom).to.equal('Prof')
          resolve()
        })
    }))

    it('reset database before each test - step 1', () => new Promise((resolve, reject) => {
      const Utilisateur = lassi.service('Utilisateur')
      flow()
        .seq(function () {
          Utilisateur
            .match('login')
            .equals('nouveau')
            .grabOne(this)
        })
        .seq(function (user) {
          expect(user).to.equal(undefined)
          createEleve(Utilisateur, { login: 'nouveau' }, this)
        })
        .seq(function () {
          Utilisateur
            .match('login')
            .equals('nouveau')
            .grabOne(this)
        })
        .seq(function (eleve) {
          expect(eleve).to.be.an('object')
          expect(eleve.login).to.equal('nouveau')
          resolve()
        })
        .catch(reject)
    }))

    it('reset database before each test - step 2', () => new Promise((resolve, reject) => {
      const Utilisateur = lassi.service('Utilisateur')
      flow()
        .seq(function (prof) {
          // On vérifie que l'utilisateur créé dans le test précédent
          // n'existe plus
          Utilisateur
            .match('login')
            .equals('nouveau')
            .grabOne(this)
        })
        .seq(function (eleve) {
          expect(eleve).to.equal(undefined)
          resolve()
        })
        .catch(reject)
    }))

    it('Chiffrement du mot de passe de test attendu', () => {
      const $crypto = lassi.service('$crypto')
      const hashedPass = $crypto.hash('azerty44')
      expect(hashedPass).to.equal(userFixtures[0].password)
    })
  })
}
