/**
 * La configuration spécifique pour mocha
 *
 * Il faut avoir créé la base de test dans mongo, avec accès pour un user mocha/mocha
 * ```mongo
 * use sesalabTest
 * db.createUser({user: 'mocha', pwd: 'mocha', roles: ["readWrite"]})
 * ```
 */
module.exports = {
  // @see https://github.com/NodeRedis/node_redis#rediscreateclient
  application: {
    // doit être déclaré dans les config des sesatheques de test
    baseId: 'sesalabTest',
    baseUrl: 'http://sesalab.local:3010',
    sesatheques: [
      { baseId: 'biblitest3011', baseUrl: 'http://bibliotheque.local:3011/' },
      { baseId: 'communtest3012', baseUrl: 'http://commun.local:3012/' }
    ]
  },

  appLog: {
    // logLevel: 'debug', // décommenter pour avoir plus de trace sur ce qui se passe coté serveur quand un test plante
    addBaseIdSuffix: true // on veut du log/app.test.log pour les traces du serveur lancé pour les tests
  },

  $cache: {
    redis: {
      prefix: 'sesalab_test'
    }
  },

  $entities: {
    database: {
      // avec cet exemple il faut avoir créé le user dans mongo avec (commande lancée par un user ayant ces droits)
      // use sesalabTest
      // db.createUser({user: 'sesalabTest', pwd: 'gp32!9SgAr,SH', roles: ["readWrite"]})
      user: 'sesalabTest',
      password: 'gp32!9sTFgSr,SH',
      name: 'sesalabTest'
    }
  }
}
