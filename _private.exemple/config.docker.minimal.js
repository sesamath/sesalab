/**
 * Nos params spécifiques à cette instance de sesalab
 *
 * Les surcharges se font par complétion, on part de _private/config.js, ajoute ce qui est dans config/index.js et n'aurait pas été défini avant puis _private.exemple/config.js
 *
 * Ce fichier contient les entrées minimales à mettre dans _private/config.js pour que cela fonctionne
 * (ici pour docker, mais c'est aussi valable sans)
 */

if (typeof window !== 'undefined') {
  // Ce fichier contient des infos sensible qu'on ne veut pas dans le code client
  // Utiliser plutôt un GET /api/settings pour obtenir les informations de config
  throw new Error('_private/config.js should never be included in browser source code!')
}

/**
 * Attention à commenter dans ce fichier d'exemple toutes les clés facultatives,
 * (ou alors les indiquer aussi dans config/index.js pour que ça ne plante pas au boot si elles manquent)
 * et à lister toutes les clés obligatoires
 * (pour les strings qui doivent impérativement être non vide préciser ici xxx)
 */
module.exports = {
  application: {
    /* admin destinataire des notification d'anomalie de l'appli */
    mail: 'dockerSesalab@example.com',
    /* from de tous les mails envoyés */
    fromMail: 'dockerSesalab@example.com',
    /* destinataire des signalements utilisateur */
    contactMail: 'dockerSesalab@example.com'
  },

  // il faut obligatoirement mettre un salt,
  // si vous voulez utiliser le login des fixtures (prof-valide/azerty44) il faut mettre 1234
  // mais c'est conseillé d'en mettre un plus sérieux et de se créer un compte pour le dev
  $crypto: {
    salt: '1234'
  },

  $rail: {
    cookie: {
      key: 'nbé32!ps2#OXei8Htd'
    },
    session: {
      secret: '2#OXei8Htdnbé32!p'
    }
  }
}
