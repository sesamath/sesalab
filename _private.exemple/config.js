const { join } = require('node:path')

/**
 * Nos params spécifiques à cette instance de sesalab
 * surchargera la conf de config/index.js
 */

if (typeof window !== 'undefined') {
  // Ce fichier contient des infos sensible qu'on ne veut pas dans le code client
  // Utiliser plutôt un GET /api/settings pour obtenir les informations de config
  throw new Error('_private/config.js should never be included in browser source code!')
}

/**
 * Attention à commenter dans ce fichier d'exemple toutes les clés facultatives,
 * (ou alors les indiquer aussi dans config/index.js pour que ça ne plante pas au boot si elles manquent)
 * et à lister toutes les clés obligatoires
 * (pour les strings qui doivent impérativement être non vide préciser ici xxx)
 */
module.exports = {
  application: {
    /* pour le header */
    name: 'Sesalab',
    /* pour le title de la page d'accueil */
    homeTitle: 'Bienvenue sur Sésalab',
    /* admin destinataire des notification d'anomalie de l'appli */
    mail: 'xxx',
    /* from de tous les mails envoyés */
    fromMail: 'xxx',
    /* destinataire des signalements utilisateur */
    contactMail: 'xxx',

    /* @see https://github.com/Sesamath/lassi/blob/mongodb/source/services/rail.js */
    maintenance: {
      // active: false, // mettre à true pour forcer la maintenance dès le démarrage
      /* ou bien mettre un motif dans ce fichier */
      lockFile: join(__dirname, '/../_private/maintenance.lock'),
      message: 'Site en maintenance, veuillez réessayer plus tard'
      // htmlPage: path.join(__dirname, '/../_private/content/maintenance.inc.html'),
      // staticDir: path.join(__dirname, '/../_private/content/maintenance')
    },

    // utilisé par sesasso-labomep pour aller sur le sso de dev ou de prod
    // mais aussi pour le cache du statique (désactivé en dev) et les logs +/- bavards
    staging: 'dev', // dev|preprod|prod
    // isProd vaut `staging === 'prod'` lorsqu'il est absent, le mettre à false si on veut
    // tourner en staging prod mais sans envoyer de mails aux formateurs par ex.
    // isProd: false,

    // Niveau de log par défaut
    logLevel: 'info', // error|warning|info|debug

    /* mettre ici un éventuel pass chiffré pour usurper tout compte (à des fins de débug / assistance) */
    // superPassword: '',

    // baseId sera fourni par la 1re Sésathèque et initialisé lors du preBoot
    baseUrl: 'http://sesalab.local:3000/',

    /**
     * Attention, certaines baseId sont déjà déclarées dans sesatheque-client/src/sesatheques
     * si vous les utilisez il faut avoir le même baseUrl, sinon prendre un autre baseId
     * ATTENTION également à l'ordre,
     * - la première sera la sésathèque globale (pour les ressources de gauche)
     * - la seconde sera la sésathèque commune (pour les ressources crées par les formateurs, partagées ou pas)
     * On impose de préciser baseId ET baseUrl pour éviter une erreur
     * qui pourrait avoir des conséquences importantes
     */
    sesatheques: [
      // pour un usage sans docker, prendre plutôt ça qui évite d'avoir à modifier son /etc/hosts
      // {baseId: 'localhost3001', baseUrl: 'http://localhost:3001/'},
      // {baseId: 'localhost3003', baseUrl: 'http://localhost:3003/'}
      // pour docker il faut ça (et si on veut aussi l'utiliser hors docker ajouter dans /etc/hosts
      // les hosts concernés sur la ligne localhost
      // 127.0.0.1 bibliotheque.local commun.local sesalab.local localhost
      { baseId: 'biblilocal3001', baseUrl: 'http://bibliotheque.local:3001/' },
      { baseId: 'communlocal3002', baseUrl: 'http://commun.local:3002/' }
    ],

    /**
     * Les profils présentés dans les préférences prof, pour les ressources affichées à gauche
     * On ne précise que l'oid et pas le rid car baseId sera imposé avec celui de la 1re Sésathèque.
     */
    profils: {
      /**
       * Ces 3 premiers sont obligatoires mais ne sont pas affichés
       */
      all: {
        label: 'Tout',
        oid: 'sesalabAll'
        // public: false // true par défaut, préciser false si un arbre de ressources était privé
      },
      default: {
        label: 'Liste par défaut',
        oid: 'sesalabAll'
      },
      autonomie: {
        label: 'Exercices utilisables en autonomie',
        oid: 'sesalabAutonomie'
      },
      /**
       * Ceux-là sont facultatifs mais il en faudrait au moins deux
       */
      ecole: {
        label: 'École',
        oid: 'sesalabEcole'
      },
      college: {
        label: 'Collège',
        oid: 'sesalabCollege'
      } /*,
      lycee: {
        label: 'Lycée',
        oid: '50039'
      } */
      /* vous pouvez en ajouter autant que nécessaire…
         (cela apparaîtra sur le choix du profil, à la 1re connexion ou dans les préférences) */
    },

    /* les ≠ urls de l'aide contextuelle */
    aide: {
      homepage: 'https://aide.labomep.sesamath.net/?connexion',
      bilans: 'https://aide.labomep.sesamath.net/doku.php?id=bilans:start',
      charte: 'https://aide.labomep.sesamath.net/doku.php?id=charte',
      comptes: 'https://aide.labomep.sesamath.net/doku.php?id=comptes:start',
      corbeille: 'https://aide.labomep.sesamath.net/doku.php?id=menu_principal:corbeille',
      eleves: 'https://aide.labomep.sesamath.net/doku.php?id=eleves:start',
      gestion: 'https://aide.labomep.sesamath.net/doku.php?id=menu_principal:responsable_geographique',
      panneau: {
        classes: 'https://aide.labomep.sesamath.net/doku.php?id=panneaux:classes',
        groupes: 'https://aide.labomep.sesamath.net/doku.php?id=panneaux:mes_groupes',
        mesRessources: 'https://aide.labomep.sesamath.net/doku.php?id=panneaux:mes_ressources',
        ressources: 'https://aide.labomep.sesamath.net/doku.php?id=panneaux:ressources',
        ressourcesPartagees: 'https://aide.labomep.sesamath.net/doku.php?id=panneaux:ressources_partagees',
        sequences: 'https://aide.labomep.sesamath.net/doku.php?id=panneaux:mes_sequences',
        sequencesCollegues: 'https://aide.labomep.sesamath.net/doku.php?id=panneaux:sequences_collegues'
      },
      profil: {
        etablissement: 'https://aide.labomep.sesamath.net/doku.php?id=menu_principal:configuration_etablissement',
        preferences: 'https://aide.labomep.sesamath.net/doku.php?id=menu_principal:preferences'
      },
      rgpd: 'https://aide.labomep.sesamath.net/doku.php?id=rgpd',
      sequences: 'https://aide.labomep.sesamath.net/doku.php?id=sequences:start'
    },
    /* affiché dans about, plus obligatoire depuis l'entrée en vigueur du RGPD */
    // numeroCNIL: 'xxx'

    // domaines à ajouter pour le Content Security Policy, si l'aide est en aide.labomep.sesamath.net il faut ajouter le SSO
    otherCspDomains: ['ssl.sesamath.net']
  }, // application

  /* conf appLog par défaut à surcharger si besoin * /
  appLog: {
    dir: join(__dirname, '..', 'log'),
    logLevel: 'warning',
    addBaseIdSuffix: false // le surcharger à true dans _private/test.js par ex
  },
  /* */

  // bugsnag: {
  //   apiKey: undefined /* remplacer par une clé api bugsnag si besoin */
  // },

  /**
   * Exemple de configuration pour le module sesalab-gestion
   * Un utilisateur est identifié par son login et son nom (par sécurité).
   * structuresFilters permet de définir le périmètre de structures du gestionnaire
   * Les clés possibles sont academieId, departementNumero, ville, oid. Seule la clé la plus large sera utilisée.
   * Sa valeur peut être une string ou une liste
   *
   * Pour le SSO Sésamath, le login est de la forme 'sesasso_xxx' (où xxx est l'id numérique sesasso)
   */
  gestion: [
    // {
    //   login: 'le login de l'utilisateur',
    //   nom: 'son nom',
    //   structuresFilter: {
    //     academieId: ['academieId1', 'academieId2'],
    //     departementNumero: ['numero1', 'numero2', 'numero3'],
    //     oid: ['id1', 'id2', 'id3', 'id4'],
    //     ville: ['ville1', 'ville2'],
    //   }
    // }
  ],

  // ///////////////////////////
  // config des services lassi
  // ///////////////////////////

  // @see https://github.com/NodeRedis/node_redis#rediscreateclient
  $cache: {
    redis: {
      host: 'localhost',
      port: 6379,
      prefix: 'sesalab_' // attention à en préciser un différent pour chaque appli utilisant le même redis
    }
  },

  $crypto: {
    salt: 'xxx' // à completer
  },

  $entities: {
    database: {
      host: 'localhost',
      port: 27017,
      // avec cet exemple il faut avoir créé le user dans mongo avec (commande lancée par un user ayant ces droits)
      // use sesalab
      // db.createUser({user: 'xxx', pwd: 'xxx', roles: ["readWrite"]})
      // user: 'xxx',
      // password: 'xxx',
      name: 'sesalab'
    }
  },

  $rail: {
    accessLog: {
      logFile: 'log/access.log'
    },
    cookie: {
      key: 'xxx' // à compléter
    },
    session: {
      secret: 'xxx' // à compléter
    }
  },
  $server: {
    port: 3000,
    maxTimeout: 301000 // ms
  }

  // Autres modules node
  // passé à nodemailer, préciser éventuellement smtp ou sendmail (cf https://nodemailer.com/smtp/)
  // par défaut ce sera sendmail avec /usr/sbin/sendmail
  /*
  smtp : {
    host: 'mail.xxx',
    port: 25,
  },
  sendmail: '/usr/sbin/sendmail',
  /* */
}
