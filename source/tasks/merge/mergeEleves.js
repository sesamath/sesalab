/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesalab (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de Sesalab, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

if (typeof app === 'undefined') throw new Error('Ce module ne doit être requis qu’après instanciation de lassi avec cli')

const flow = require('an-flow')
const _ = require('lodash')
const minimist = require('minimist')
const constants = require('sesalab-commun/constants.js')

/**
 * Permet de merger deux élèves
 *
 * @param {Object} [options] Si non fourni on regarde argv (ajouter alors -- devant chaque option)
 * @param {boolean} [options.dryRun]
 * @param {boolean} [options.force]
 * @param {boolean} [options.keepLastPassword]
 * @param {string} [options.toKeep]
 * @param {string} [options.toDelete]
 * @param {simpleCallback} done
 */
function mergeEleves (options, done) {
  /**
   * Renvoie un message d'erreur si les deux élèves comparés ne sont pas des doublons
   *
   * @param {Object} eleveToKeep élève à conserver
   * @param {Object} eleveToDelete élève à fusionner dans le précédent
   *
   * @return {string} Le message d'erreur éventuel
   */
  function getDuplicatedElevesError (eleveToKeep, eleveToDelete) {
    // On vérifie que les deux utilisateurs sont bien des élèves
    // force ne peut pas bypasser ce test
    if (eleveToKeep.type !== constants.TYPE_ELEVE) throw new Error(`L’utilisateur ${eleveToKeep.oid} n’est pas un élève`)
    if (eleveToDelete.type !== constants.TYPE_ELEVE) throw new Error(`L’utilisateur ${eleveToDelete.oid} n’est pas un élève`)

    const start = `Les deux élèves (${eleveToKeep.oid} et ${eleveToDelete.oid})`
    const errors = []

    // On vérifie que les deux élèves ont bien un identifiant unique en commun
    // (les deux doivent être uniques, mais c'est pas parce qu'on a un doublon sur l'un
    // qu'il est aussi en doublon sur l'autre, en cas de doublon sur l'un on merge
    // comme demandé sans regarder l'autre)
    const hasSameLogin = eleveToKeep.login === eleveToDelete.login
    const hasSameExternal = eleveToKeep.externalMech && eleveToDelete.externalMech &&
      eleveToKeep.externalId && eleveToDelete.externalId &&
      eleveToKeep.externalMech === eleveToDelete.externalMech &&
      eleveToKeep.externalId === eleveToDelete.externalId
    if (!hasSameLogin && !hasSameExternal) {
      const message = `${start} n’ont ni le même login (${eleveToKeep.login} et ${eleveToDelete.login}) ` +
      `ni les mêmes identifiants externes (${eleveToKeep.externalMech}/${eleveToKeep.externalId} et ` +
      `${eleveToDelete.externalMech}/${eleveToDelete.externalId})`
      // ça c'est un truc que force peut passer mais pas tryFixError
      if (force) errors.push(message)
      else throw new Error(message)
    }

    // On vérifie que les deux élèves appartiennent à la même structure s'ils en ont tous les deux une
    // si toDelete n'en a pas on s'en moque et on continue
    // si toKeep n'en a pas on signale l'erreur avec la situation du 2e
    const toKeepStructure = eleveToKeep.structures && eleveToKeep.structures.length && eleveToKeep.structures[0]
    const toDeleteStructure = eleveToDelete.structures && eleveToDelete.structures.length && eleveToDelete.structures[0]
    if (toKeepStructure) {
      if (toDeleteStructure && toDeleteStructure !== toKeepStructure) {
        errors.push(`${start} n'appartiennent pas à la même structure (${toKeepStructure} et ${toDeleteStructure})`)
      }
    } else {
      // le 1er n'a pas de structure
      let message = `L’élève à conserver ${eleveToKeep.oid} ne possède pas de structure`
      if (toDeleteStructure) message += ` mais l’élève à fusionner (${eleveToDelete.oid}) en a une (${toDeleteStructure})`
      errors.push(message)
    }

    // On vérifie que les deux élèves ont les mêmes nom et prénom
    const normalizedEleveToKeep = $utilisateur.normalizeFullname(eleveToKeep)
    const normalizedEleveToDelete = $utilisateur.normalizeFullname(eleveToDelete)
    if (normalizedEleveToKeep !== normalizedEleveToDelete) {
      errors.push(`${start} se nomment différemment (${eleveToKeep.nom} ${eleveToKeep.prenom} et ${eleveToDelete.nom} ${eleveToDelete.prenom})`)
    }

    // la cible devrait pas être softDeleted si la source ne l'est pas
    if (eleveToKeep.isDeleted() && !eleveToDelete.isDeleted()) {
      errors.push(`Fusion d’un élève valide ${eleveToDelete.oid} dans un softDeleted (${eleveToKeep.oid})`)
    }

    return errors.join('\n')
  } // getDuplicatedElevesError

  /**
   * Console.log si --verbose (rien sinon)
   */
  function logVerbose () {
    verbose && console.log.apply(console, arguments)
  };

  /**
   * Essaie de régler un pb de fusion en éliminant l'un des deux (jamais connecté, sans résultat ni référencement)
   * C'est délicat, donc on fait rien pour le moment (sinon aiguiller vers la suite ou la sortie en fct de force)…
   * @param error
   * @param force
   * @param next
   */
  function tryFixError (error, force, next) {
    // @todo si l'un des deux ne s'est jamais connecté on signale le pb et on l'efface sans modifier l'autre
    // -> à discuter car un élève peut ne pas s'être connecté mais le prof l'a quand même mis dans des entités
    if (force) {
      console.error(error, '; on continue quand même (--force)')
      next()
    } else {
      next(new Error(error))
    }
  } // tryFixError

  if (typeof options === 'function') {
    done = options
    options = minimist(process.argv.slice(2))
  }

  // variables
  const { dryRun, force, keepLastPassword, toDelete, toKeep, verbose } = options
  if (!toKeep || !toDelete) throw new Error('Vous devez renseigner les oids des élèves à garder et à supprimer')
  if (toKeep === toDelete) throw new Error('Vous avez renseigné deux fois le même id')
  if (dryRun && force) throw new Error('Impossible d’appeler les options --dryRun et --force simultanément')

  const Utilisateur = lassi.service('Utilisateur')
  const UtilisateurAcces = lassi.service('UtilisateurAcces')
  const $groupe = lassi.service('$groupe')
  const $resultat = lassi.service('$resultat')
  const $seance = lassi.service('$seance')
  const $sequence = lassi.service('$sequence')
  const $utilisateur = lassi.service('$utilisateur')

  let eleveToKeep
  let eleveToDelete

  // main
  flow()
  // recup toKeep
    .seq(function () {
    // Permet de séparer les différents merges en console
      logVerbose('################################################################################################')

      Utilisateur
        .match('oid').equals(toKeep)
        .match('type').equals(constants.TYPE_ELEVE)
        .includeDeleted()
        .grabOne(this)
    })

  // récup toDelete
    .seq(function (eleve) {
      eleveToKeep = eleve
      if (!eleveToKeep) throw new Error('Impossible de récupérer l’élève à garder')

      Utilisateur
        .match('oid').equals(toDelete)
        .match('type').equals(constants.TYPE_ELEVE)
        .includeDeleted()
        .grabOne(this)
    })

  // check
    .seq(function (eleve) {
      eleveToDelete = eleve
      if (!eleveToDelete) throw new Error('Impossible de récupérer l’élève à supprimer')

      // On s'assure que les élèves sont bien des doublons dans le cas où l'option force n'est pas activée
      const error = getDuplicatedElevesError(eleveToKeep, eleveToDelete)
      if (error) tryFixError(error, force, this)
      else this()
    })

  // start merge
    .seq(function () {
      let message = verbose ? '' : '\n' // on saute quand même une ligne entre chaque merge si non-verbose
      message += `Merge de ${eleveToDelete.oid} (${eleveToDelete.nom} ${eleveToDelete.prenom} ${eleveToDelete.login} ` +
      `${eleveToDelete.externalMech}/${eleveToDelete.externalId} ` +
      `structure ${eleveToKeep.structures && eleveToKeep.structures[0]})`
      if (eleveToDelete.isDeleted()) message += ' softDeleted'
      message += `\n    dans ${eleveToKeep.oid} (${eleveToKeep.nom} ${eleveToKeep.prenom} ${eleveToKeep.login} ` +
      `${eleveToKeep.externalMech}/${eleveToKeep.externalId})`
      if (eleveToKeep.isDeleted()) message += ' softDeleted'
      if (dryRun) message += ' DRY-RUN'
      if (force) message += ' FORCE'
      console.log(message)

      logVerbose('\n----- MERGE DES GROUPES -----')
      // On récupère tous les groupes de l'élève que l'on souhaite supprimer
      $groupe.withUsers([eleveToDelete.oid], this)
    })

  // Groupes
    .seqEach(function (groupe) {
    // On enlève l'id de l'élève à supprimer (et d'éventuelles valeurs falsy)
      const utilisateurs = groupe.utilisateurs.filter(oid => oid && oid !== eleveToDelete.oid)
      // Si on a affaire à une classe autre que celle de l'élève à garder, alors on ne rajoute pas l'id
      // pour éviter que celui-ci se retrouve dans deux classes différentes
      if (groupe.isClass && eleveToKeep.classe) {
        console.log(`Delete ${eleveToDelete.oid} in groupe ${groupe.oid}`)
      } else {
      // On rajoute l'id de l'élève à garder
        console.log(` Replace ${eleveToDelete.oid} by ${eleveToKeep.oid} in groupe ${groupe.oid}`)
        utilisateurs.push(eleveToKeep.oid)
      }
      if (dryRun) return this()
      // On s'assure d'enlever les doublons
      groupe.utilisateurs = _.uniq(utilisateurs)
      // on veut sauvegarder cette màj même si y'a un doublon
      groupe.$byPassDuplicate = true
      groupe.store(this)
    })

  // Résultats
    .seq(function () {
      logVerbose('\n----- MERGE DES RESULTATS -----')
      // On récupère tous les résultats de l'élève que l'on souhaite supprimer
      $resultat.forUsers([eleveToDelete.oid], { includeDeleted: true }, this)
    })
    .seqEach(function (resultat) {
      console.log(` Replace ${eleveToDelete.oid} by ${eleveToKeep.oid} in resultat ${resultat.oid}`)
      if (dryRun) return this()
      // On enlève l'id de l'élève à supprimer pour rajouter celui de l'élève à garder
      const participants = resultat.participants.filter(oid => oid && oid !== eleveToDelete.oid)
      if (!participants.includes(eleveToKeep.oid)) participants.push(eleveToKeep.oid)
      // On s'assure d'enlever les doublons et les valeurs erronnées
      resultat.participants = _.uniq(participants)
      resultat.store(this)
    })

  // Séances
    .seq(function () {
      logVerbose('\n----- MERGE DES SEANCES -----')
      // On récupère toutes les séances des deux élèves
      $seance.withUsers([eleveToDelete.oid, eleveToKeep.oid], { includeDeleted: true }, this)
    })
    .seq(function (seances) {
      const seancesBySequence = _.groupBy(seances, 'sequence')
      // on veut la liste des séances en array, mais séquence par séquence
      const seancesInArray = _.map(Object.keys(seancesBySequence), key => seancesBySequence[key])
      this(null, seancesInArray)
    })
    .seqEach(function (seances) {
      if (seances.length > 1) {
        const ordered = _.sortBy(seances, 'dernierVisionnage')
        const latest = ordered.pop()
        const moreThanTwoSeances = seances.length > 2
        if (moreThanTwoSeances) console.error(`Attention, plus de 2 séances ont été trouvées pour la séquence ${seances[0].sequence}`)

        flow()
          .seq(function () {
            console.log(`Mise à jour de la dernière séance visionnée ${latest.oid}`)
            if (!moreThanTwoSeances) {
              // On parcourt la séance la plus ancienne pour y fusionner la compilation des résultats
              // si la plus récente ne contient pas déjà des résultats pour une ressource donnée
              _.each(ordered[0].ressources, (ressource) => {
                if (!_.find(latest.ressources, r => r.rid === ressource.rid)) {
                  console.log(`Ajout de la ressource ${ressource.rid} dans la séance ${latest.oid}`)
                  latest.ressources.push(ressource)
                }
              })
            }
            if (dryRun) return this()
            // On garde uniquement la séance la plus récente  en affectant l'id de l'élève à garder
            latest.eleve = eleveToKeep.oid
            latest.store(this)
          })
        // On supprime les autres séances
          .seq(function () {
            const next = this
            flow(ordered)
              .seqEach(function (seance) {
                console.log(`Suppression de la séance ${seance.oid}`)
                if (dryRun) return this()
                seance.delete(this)
              })
              .done(next)
          })
          .done(this)
      } else {
        console.log(` Replace ${eleveToDelete.oid} by ${eleveToKeep.oid} in seance ${seances[0].oid}`)
        if (dryRun) return this()
        // On remplace l'id de l'élève à garder (il est possible que cette séance lui était déjà assignée)
        seances[0].eleve = eleveToKeep.oid
        seances[0].store(this)
      }
    })

  // Séquences
    .seq(function () {
      logVerbose('\n----- MERGE DES SEQUENCES -----')
      // On récupère toutes les séquences de l'élève que l'on souhaite supprimer
      $sequence.withEntities([eleveToDelete.oid], constants.SEQUENCE_TYPE_ELEVE, { includeDeleted: true }, this)
    })
    .seqEach(function (sequence) {
      console.log(` Replace ${eleveToDelete.oid} by ${eleveToKeep.oid} in sequence ${sequence.oid}`)
      if (dryRun) return this()
      // On parcourt chaque sous-séquence et on enlève l'élève à supprimer
      _.each(sequence.sousSequences, (sousSequence) => {
        if (_.find(sousSequence.eleves, eleve => eleve.type === constants.SEQUENCE_TYPE_ELEVE && eleve.oid === eleveToDelete.oid)) {
          _.remove(sousSequence.eleves, eleve => eleve.type === constants.SEQUENCE_TYPE_ELEVE && eleve.oid === eleveToDelete.oid)
          // On rajoute l'élève à garder si besoin
          if (!_.find(sousSequence.eleves, eleve => eleve.type === constants.SEQUENCE_TYPE_ELEVE &&
          eleve.oid === eleveToKeep.oid)) {
            sousSequence.eleves.push({
              oid: eleveToKeep.oid,
              nom: eleveToKeep.nom,
              prenom: eleveToKeep.prenom,
              type: constants.SEQUENCE_TYPE_ELEVE
            })
          }
        }
      })
      sequence.store(this)
    })

  // Choix du mot de passe
    .seq(function () {
      const next = this
      if (!keepLastPassword) return next()

      flow()
        .seq(function () {
          UtilisateurAcces
            .match('utilisateur').in([eleveToKeep.oid, eleveToDelete.oid])
            .sort('timestamp', 'desc')
            .grabOne(this)
        })
        .seq(function (utilisateurAcces) {
          if (!utilisateurAcces) return next()
          if (utilisateurAcces.utilisateur === eleveToKeep.oid) return next()
          // Le dernier élève connecté est l'élève à supprimer donc on garde son mot de passe
          eleveToKeep.password = eleveToDelete.password
          eleveToKeep.store(next)
        })
        .catch(next)
    })

    // Delete de l'élève à supprimer
    .seq(function () {
      console.log(`Suppression de l'élève ${eleveToDelete.oid}`)
      if (dryRun) return done()
      eleveToDelete.delete(this)
    })
    .empty()
    .done(done)
}

mergeEleves.help = function mergeElevesHelp () {
  console.log(`
La commande mergeEleves permet de fusionner deux comptes élèves
Paramètres :
  --toKeep="oidToKeep": renseigne l'oid de l'élève à garder
  --toDelete="oidToDelete": renseigne l'oid de l'élève à supprimer
Options :
  --dryRun : signale tout ce qui devrait être fait sans le faire
  --force : force la fusion sans vérifier les pré-requis (noms, login|external et structure identiques)
  --keepLastPassword : garde le password de celui qui s'est connecté en dernier, même si c'est celui de --toDelete
  --verbose : être plus bavard
  `)
}

module.exports = mergeEleves
