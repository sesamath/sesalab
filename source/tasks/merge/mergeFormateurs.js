/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesalab (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de Sesalab, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

if (typeof app === 'undefined') throw new Error('Ce module ne doit être requis qu’après instanciation de lassi avec cli')

const flow = require('an-flow')
const _ = require('lodash')
const minimist = require('minimist')
const { TYPE_FORMATEUR } = require('sesalab-commun/constants.js')

/**
 * Permet de merger deux formateurs
 *
 * @param {Object} [options] Si non fourni on regarde argv (ajouter alors -- devant chaque option)
 * @param {boolean} [options.dryRun]
 * @param {boolean} [options.force]
 * @param {boolean} [options.keepLastPassword]
 * @param {boolean} [options.verbose]
 * @param {string|Utilisateur} [options.toKeep]
 * @param {string|Utilisateur} [options.toDelete]
 * @param {simpleCallback} done
 */
function mergeFormateurs (options, done) {
  /**
   * Renvoie un message d'erreur si les deux formateurs comparés ne sont pas des doublons
   *
   * @param {Object} formateurToKeep formateur à conserver
   * @param {Object} formateurToDelete formatteur à fusionner dans le précédent
   *
   * @return {string} Le message d'erreur éventuel
   */
  function getDuplicatedFormateursError (formateurToKeep, formateurToDelete) {
    // On vérifie que les deux utilisateurs sont bien des formateurs
    // force ne peut pas bypasser ce test
    if (formateurToKeep.type !== TYPE_FORMATEUR) throw new Error(`L’utilisateur ${formateurToKeep.oid} n’est pas un formateur`)
    if (formateurToDelete.type !== TYPE_FORMATEUR) throw new Error(`L’utilisateur ${formateurToDelete.oid} n’est pas un formateur`)

    const start = `Les deux formateurs (${formateurToKeep.oid} et ${formateurToDelete.oid})`
    const errors = []

    // On vérifie que les deux formateurs ont bien un identifiant unique en commun
    // (les deux doivent être uniques, mais c'est pas parce qu'on a un doublon sur l'un
    // qu'il est aussi en doublon sur l'autre, en cas de doublon sur l'un on merge
    // comme demandé sans regarder l'autre)
    const hasSameLogin = formateurToKeep.login === formateurToDelete.login
    const hasSameExternal = formateurToKeep.externalMech && formateurToDelete.externalMech &&
      formateurToKeep.externalId && formateurToDelete.externalId &&
      formateurToKeep.externalMech === formateurToDelete.externalMech &&
      formateurToKeep.externalId === formateurToDelete.externalId
    const hasSameMail = formateurToKeep.mail && formateurToKeep.mail === formateurToDelete.mail
    if (!hasSameLogin && !hasSameExternal && !hasSameMail) {
      const message = `${start} n’ont ni le même login (${formateurToKeep.login} et ${formateurToDelete.login})` +
        ` ni les mêmes identifiants externes (${formateurToKeep.externalMech}/${formateurToKeep.externalId} et ${formateurToDelete.externalMech}/${formateurToDelete.externalId})` +
        ` ni le même mail (${formateurToKeep.mail} et ${formateurToDelete.mail})`
      // ça c'est un truc que force peut passer mais pas tryFixError
      if (force) errors.push(message)
      else throw new Error(message)
    }

    // On vérifie que les deux formateurs appartiennent à une structure
    // si toDelete n'en a pas on s'en moque et on continue
    // si toKeep n'en a pas on signale l'erreur avec la situation du 2e
    const toKeepStructure = formateurToKeep.structures && formateurToKeep.structures.length && formateurToKeep.structures[0]
    const toDeleteStructure = formateurToDelete.structures && formateurToDelete.structures.length && formateurToDelete.structures[0]
    if (toKeepStructure) {
      if (toDeleteStructure && toDeleteStructure !== toKeepStructure) {
        let message = `${start} n'appartiennent pas à la même structure (${toKeepStructure} et ${toDeleteStructure})`
        if (formateurToKeep.externalId && formateurToKeep.externalMech) {
          errors.push(message)
        // Ici, on souhaite merger deux formateurs de structures différentes sachant que le formateur à conserver
        // ne se connecte pas en SSO -> erreur
        } else {
          message += ` et le formateur à conserver ne possède pas d'identifiant externe (${formateurToKeep.externalMech}/${formateurToKeep.externalId})`
          // ça c'est un truc que force peut passer mais pas tryFixError
          if (force) errors.push(message)
          else throw new Error(message)
        }
      }
    } else {
      let message = `Le formateur à conserver ${formateurToKeep.oid} ne possède pas de structure`
      if (toDeleteStructure) message += ` mais le formateur à fusionner (${formateurToDelete.oid}) en a une (${toDeleteStructure})`
      errors.push(message)
    }

    // On vérifie que les deux formateurs ont les mêmes nom et prénom
    const normalizedFormateurToKeep = $utilisateur.normalizeFullname(formateurToKeep)
    const normalizedFormateurToDelete = $utilisateur.normalizeFullname(formateurToDelete)
    if (normalizedFormateurToKeep !== normalizedFormateurToDelete) {
      errors.push(`${start} se nomment différemment (${formateurToKeep.nom} ${formateurToKeep.prenom} et ${formateurToDelete.nom} ${formateurToDelete.prenom})`)
    }

    return errors.join('\n')
  }

  /**
   * Console.log si --verbose (rien sinon)
   */
  function logVerbose () {
    verbose && console.log.apply(console, arguments)
  }

  /**
   * Essaie de régler un pb de fusion en éliminant l'un des deux (jamais connecté, sans résultat ni référencement)
   * C'est délicat, donc on fait rien pour le moment (sinon aiguiller vers la suite ou la sortie en fct de force)…
   * @param error
   * @param force
   * @param next
   */
  function tryFixError (error, force, next) {
    if (force) {
      console.error(error, '; on continue quand même (--force)')
      next()
    } else {
      next(new Error(error))
    }
  }

  if (typeof options === 'function') {
    done = options
    options = minimist(process.argv.slice(2))
  }

  // variables
  const { dryRun, force, keepLastPassword, verbose } = options
  let { toDelete, toKeep } = options
  if (!toKeep || !toDelete) throw new Error('Vous devez renseigner les oids des formateurs à garder et à supprimer')
  if (toKeep === toDelete) throw new Error('Vous avez renseigné deux fois le même id')
  if (dryRun && force) throw new Error('Impossible d’appeler les options --dryRun et --force simultanément')

  const Utilisateur = lassi.service('Utilisateur')
  const UtilisateurAcces = lassi.service('UtilisateurAcces')
  const $groupe = lassi.service('$groupe')
  const $sequence = lassi.service('$sequence')
  const $utilisateur = lassi.service('$utilisateur')

  let formateurToKeep
  let formateurToDelete

  flow()
    // recup toKeep
    .seq(function () {
    // Permet de séparer les différents merges en console
      logVerbose('##### start mergeFormateur #####')

      // on peut nous passer un formateur complet
      if (typeof toKeep === 'object' && toKeep.type === TYPE_FORMATEUR && toKeep.oid) return this(null, toKeep)
      // minimist transforme les chaînes numériques en number
      if (typeof toKeep === 'number') toKeep = String(toKeep)
      else if (typeof toKeep !== 'string') return this(Error('option toKeep invalide'))
      // faut aller le chercher
      Utilisateur
        .match('oid').equals(toKeep)
        .match('type').equals(TYPE_FORMATEUR)
        .includeDeleted()
        .grabOne(this)
    })

    // récup toDelete
    .seq(function (formateur) {
      formateurToKeep = formateur
      if (!formateurToKeep) throw new Error('Impossible de récupérer le formateur à garder')

      if (typeof toDelete === 'object' && toDelete.type === TYPE_FORMATEUR && toDelete.oid) return this(null, toDelete)
      if (typeof toDelete === 'number') toDelete = String(toDelete)
      else if (typeof toDelete !== 'string') return this(Error('Option toDelete invalide'))
      Utilisateur
        .match('oid').equals(toDelete)
        .match('type').equals(TYPE_FORMATEUR)
        .includeDeleted()
        .grabOne(this)
    })

    // check
    .seq(function (formateur) {
      formateurToDelete = formateur
      if (!formateurToDelete) throw new Error('Impossible de récupérer le formateur à supprimer')

      // On s'assure que les formateurs sont bien des doublons dans le cas où l'option force n'est pas activée
      const error = getDuplicatedFormateursError(formateurToKeep, formateurToDelete)
      if (error) tryFixError(error, force, this)
      else this()
    })

    // start merge
    .seq(function () {
      let message = verbose ? '' : '\n' // on saute quand même une ligne entre chaque merge si non-verbose
      message += `Merge de ${formateurToDelete.oid} (${formateurToDelete.nom} ${formateurToDelete.prenom} ` +
      `${formateurToDelete.login} ${formateurToDelete.externalMech}/${formateurToDelete.externalId} ${formateurToDelete.mail}` +
      `structure ${formateurToKeep.structures && formateurToKeep.structures[0]}) dans ${formateurToKeep.oid} ` +
      `(${formateurToKeep.nom} ${formateurToKeep.prenom} ${formateurToKeep.login} ` +
      `${formateurToKeep.externalMech}/${formateurToKeep.externalId} ${formateurToKeep.mail})`
      if (dryRun) message += ' DRY-RUN'
      if (force) message += ' FORCE'
      console.log(message)

      logVerbose('\n----- MERGE DES GROUPES -----')
      // On récupère tous les groupes du formateur que l'on souhaite supprimer
      $groupe.byOwner([formateurToDelete.oid], { includeDeleted: true }, this)
    })

    // Groupes
    .seqEach(function (groupe) {
      console.log(` Replace ${formateurToDelete.oid} by ${formateurToKeep.oid} in groupe ${groupe.oid}`)
      if (dryRun) return this()
      // On affecte un nouveau owner au groupe avec l'id du formateur à garder
      groupe.owner = formateurToKeep.oid
      // On veut sauvegarder cette màj même si y'a un doublon
      groupe.$byPassDuplicate = true
      groupe.store(this)
    })

    // Séquences
    .seq(function () {
      logVerbose('\n----- MERGE DES SEQUENCES -----')
      // On récupère toutes les séquences du formateur que l'on souhaite supprimer
      $sequence.getSequencesOf(formateurToDelete.oid, { includeDeleted: true }, this)
    })
    .seqEach(function (sequence) {
      console.log(` Replace ${formateurToDelete.oid} by ${formateurToKeep.oid} in sequence ${sequence.oid}`)
      if (dryRun) return this()
      sequence.owner = formateurToKeep.oid
      sequence.store(this)
    })

  // FIXME il manque la màj coté ressources sur les sésathèques

    .seq(function () {
      // si celui à garder est deleted et pas l'autre, on le restaure
      if (formateurToKeep.isDeleted() && !formateurToDelete.isDeleted()) {
        console.log(`L’utilisateur à conserver ${formateurToKeep.oid} était deleted mais pas celui à supprimer (${formateurToDelete.oid}), on restaure celui qu’on garde`)
        formateurToKeep.markToRestore()
      }

      // Choix du mot de passe et mise à jour du formateur à garder
      if (!keepLastPassword) return this()
      UtilisateurAcces
        .match('utilisateur').in([formateurToKeep.oid, formateurToDelete.oid])
        .sort('timestamp', 'desc')
        .grabOne(this)
    })
    .seq(function (utilisateurAcces) {
      if (utilisateurAcces && utilisateurAcces.utilisateur !== formateurToKeep.oid) {
      // Le dernier formateur connecté est le formateur à supprimer donc on garde son mot de passe
        formateurToKeep.password = formateurToDelete.password
      }
      formateurToKeep.structures = _.union(formateurToKeep.structures, formateurToDelete.structures)

      // Delete du formateur à supprimer
      console.log(`Suppression du formateur ${formateurToDelete.oid}`)
      if (dryRun) return done()
      formateurToDelete.delete(this)
    })

    // sauvegarde de celui à conserver (après le delete en cas de doublon
    .seq(function () {
      formateurToKeep.store(this)
    })
    .empty()
    .done(done)
}

mergeFormateurs.help = function mergeFormateursHelp () {
  console.log(`
La commande mergeFormateurs permet de fusionner deux comptes formateurs
Paramètres :
  --toKeep="oidToKeep": renseigne l'oid de l'élève à garder
  --toDelete="oidToDelete": renseigne l'oid de l'élève à supprimer
Options :
  --dryRun : signale tout ce qui devrait être fait sans le faire
  --force : force la fusion sans vérifier les pré-requis (noms, login|external et structure identiques)
  --keepLastPassword : garde le password de celui qui s'est connecté en dernier, même si c'est celui de --toDelete
  --verbose : être plus bavard
  `)
}

module.exports = mergeFormateurs
