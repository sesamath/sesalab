/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesalab (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de Sesalab, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

if (typeof app === 'undefined') throw new Error('Ce module ne doit être requis qu’après instanciation de lassi avec cli')

const flow = require('an-flow')
const constants = require('sesalab-commun/constants.js')
const minimist = require('minimist')
const _ = require('lodash')

/**
 * Permet le merge de deux classes ayant le même nom.
 *
 * @param oidToKeep Identifiant de la classe à garder
 * @param oidToDelete Identifiant de la classe à supprimer
 * @param {errorCallback} done
 */
function main (done) {
  const Groupe = lassi.service('Groupe')
  const Structure = lassi.service('Structure')
  const $sequence = lassi.service('$sequence')
  const $utilisateur = lassi.service('$utilisateur')
  const $settings = lassi.service('$settings')
  const maxResults = $settings.get('application.maxResults', 500)

  /**
   * Nettoie les doublons dans toutes les structures
   * @param {simpleCallback} cb
   */
  function cleanAllStructures (options, cb) {
    function grab (skip) {
      Structure
        .match()
        .grab({ limit: maxResults, skip }, (error, structures) => {
          if (error) return cb(error)
          flow(structures)
            .seqEach(function (structure) {
              mergeDuplicateInStructure(options, structure, this)
            })
            .seq(function () {
              if (structures.length === maxResults) return process.nextTick(grab, skip + maxResults)
              console.log(`${skip + structures.length} structures examinées`)
              cb()
            })
            .catch(cb)
        })
    }

    grab(0)
  }

  /**
   * Merge les classes de options.structure
   *
   * @param {Object} options Options passées à la commande
   * @param {string} options.structure oid de la structure
   * @param {simpleCallback} cb
   */
  function cleanStructure (options, cb) {
    const { structure } = options
    flow()
      .seq(function () {
        Structure
          .match('oid').equals(structure)
          .grabOne(this)
      })
      .seq(function (structure) {
        if (!structure) return cb(new Error(`La structure ${structure} n’existe pas`))
        mergeDuplicateInStructure(options, structure, this)
      })
      .done(cb)
  }

  /**
   * Merge deux classes
   *
   * @param {Object} options Options passées à la commande
   * @param {string} options.toKeep L'oid de la classe à conserver
   * @param {string} options.toDelete L'oid de la classe à supprimer après merge
   * @param cb
   */
  function merge (options, cb) {
    const { dryRun, force, includeDeleted, toDelete, toKeep } = options
    if (!toKeep || !toDelete) throw new Error('Vous devez renseigner les oids des classes à garder et à supprimer')
    if (toKeep === toDelete) throw new Error(`Les arguments "toKeep" et "toDelete" ne peuvent pas être identiques (${toKeep})`)

    let classeToKeep = null
    flow()
      .seq(function () {
        const query = Groupe
          .match('oid').equals(toKeep)
          .match('isClass').equals(true)
        if (includeDeleted) query.includeDeleted()
        query.grabOne(this)
      })
      .seq(function (classe) {
        classeToKeep = classe
        if (!classeToKeep) throw new Error('Aucune classe avec l’identifiant donné à "toKeep"')

        let query = Groupe
          .match('oid').equals(toDelete)
          .match('isClass').equals(true)
        if (includeDeleted) query = query.includeDeleted()
        query.grabOne(this)
      })
      .seq(function (classeToDelete) {
        if (!classeToDelete) throw new Error('Aucune classe avec l’identifiant donné à "toDelete"')
        if (!force && !checkClassesValidity(classeToKeep, classeToDelete)) {
          const message = 'Les classes trouvées ne sont pas identiques'
          if (dryRun) console.log(message)
          else throw new Error(message)
        }

        mergeClasses([classeToKeep, classeToDelete], options, this)
      })
      .done(cb)
  }

  /**
   * Merge toutes les classes homonymes de la structure
   *
   * @param {Object} options Options passées à la commande
   * @param {Structure} structure
   * @param {simpleCallback} cb
   */
  function mergeDuplicateInStructure (options, structure, cb) {
    const { dryRun, includeDeleted, strict } = options
    let nbMerge = 0
    let toDeleteOids = []
    flow()
      .seq(function () {
        let query = Groupe
          .match('structure').equals(structure.oid)
          .match('isClass').equals(true)
        if (includeDeleted) query = query.includeDeleted().sort('__deletedAt')
        query.grab(this)
      })
      .seq(function (_classes) {
        if (!_classes.length) return cb()
        const classesByName = _.groupBy(_classes, (classe) => {
          if (!classe.nom) {
            console.error(`classe ${classe.oid} sans nom`)
            classe.nom = ''
          }
          return strict ? classe.nom : classe.nom.toLowerCase().trim()
        })

        // un tableau avec les listes de classes en doublon
        const duplicatesClasses = _.filter(classesByName, (classes) => {
          return classes.length > 1
        })
        if (!duplicatesClasses.length) return cb()

        // S'il y a un mix softDeleted et active, on supprimer les softDeleted et on merge les autres,
        // s'il n'y a aucune classe valide, on merge les softDeleted entre eux
        let cleanDuplicates = []
        if (includeDeleted) {
          _.each(duplicatesClasses, (classes) => {
            const valides = []
            const toDelTmp = []
            _.each(classes, (classe) => {
              if (classe.isDeleted()) toDelTmp.push(classe.oid)
              else valides.push(classe)
            })
            if (valides.length) {
              toDeleteOids = toDeleteOids.concat(toDelTmp)
              if (valides.length > 1) cleanDuplicates.push(valides)
            } else {
            // Les classes sont toutes soft deleted, il n'est pas prudent de faire un merge dans ce cas :
            // on ne garde que la dernière
              classes.pop()
              toDeleteOids = toDeleteOids.concat(classes.map(c => c.oid))
            }
          })
        } else {
          cleanDuplicates = duplicatesClasses
        }
        this(null, cleanDuplicates)
      })
      .seqEach(function (groupOfClasses) {
        nbMerge++
        mergeClasses(groupOfClasses, options, this)
      })
      .seq(function () {
        console.log(`${nbMerge} fusion${nbMerge > 1 ? 's' : ''} de noms de classe pour la structure ${structure.nom} (${structure.oid})`)
        if (!toDeleteOids.length) return cb()
        if (dryRun) {
          console.log(`${toDeleteOids.length} classes softDeleted avec un doublon valide auraient été supprimées pour la structure ${structure.nom} (${structure.oid})`)
          return cb()
        }
        console.log('On va effacer', toDeleteOids)
        Groupe
          .match('oid').in(toDeleteOids)
          .match('isClass').equals(true)
          .includeDeleted()
          .purge(this)
      })
      .seq(function (nbDeleted) {
        console.log(`${nbDeleted} suppression${nbDeleted > 1 ? 's' : ''} de classe softDeleted avec un doublon valide pour la structure ${structure.nom} (${structure.oid})`)
        cb()
      })
      .catch(cb)
  }

  /**
   * Vérifie que les données classe sont valides et fusionnables
   *
   * @param classeA Instance de la première classe
   * @param classeB Instance de la seconde classe
   * @return true si les deux classes sont fusionnables
   */
  function checkClassesValidity (classeA, classeB) {
    return classeA.oid !== classeB.oid && classeA.isClass && classeB.isClass && classeA.nom === classeB.nom && classeA.structure === classeB.structure
  }

  /**
   * Merge plusieurs classes dans la 1re
   *
   * @param {Groupe[]} classes Tableau de classes
   * @param {Object} options Options passées à la commande
   * @param {simpleCallback} cb Callback appelé en fin de merge
   */
  function mergeClasses (classes, options, cb) {
    if (!classes || classes.length < 2) return cb(new Error('Il faut au moins deux classes pour une fusion'))
    const { dryRun } = options
    // on fusionne toujours dans la 1re
    const classeToKeep = classes[0]
    // le cumul
    let studentsOids = []
    // les nouveaux dans la 1re classe
    let newStudents = []
    // ceux dont il faut changer la classe (les même avec les deleted en plus)
    let studentsToUpdate = []
    const classesOids = _.map(classes, 'oid')
    const classesOidsToDel = classesOids.slice(1)
    console.log(`Fusion des classes "${classeToKeep.nom}" (${classes.length} classes)`)

    flow(classes)
      .seqEach(function (classe) {
      // Il ne faut pas ajouter des élèves d'une classe deleted dans une qui ne le serait pas
        if (classeToKeep.isDeleted() || !classe.isDeleted()) studentsOids = studentsOids.concat(classe.utilisateurs)
        this()
      })
    // Création du tableau d'utilisateurs et récupération des élèves mis à jour
      .seq(function () {
        studentsOids = _.uniq(_.compact(studentsOids))
        newStudents = studentsOids.filter(student => classeToKeep.utilisateurs.indexOf(student) === -1)
        console.log(`Récupération des élèves (${studentsOids.length} élève(s), dont ${newStudents.length} à ajouter dans la classe conservée)`)
        // Il faut mettre à jour les deleted
        $utilisateur.loadUsers(newStudents, true, this)
      })
      .seq(function (_studentsDeleted) {
        studentsToUpdate = _studentsDeleted
        $utilisateur.loadUsers(newStudents, false, this)
      })
      .seq(function (_studentsActives) {
        this(null, studentsToUpdate.concat(_studentsActives))
      })
    // Met à jour la propriété groupes des élèves ayant changés de classe
      .seqEach(function (student) {
        console.log(`L'élève ${student.nom} ${student.prenom} va changer de classe.`)
        if (dryRun) return this()
        student.classe = classeToKeep.oid
        student.store(this)
      })
    // Récupère les séquences ayant un lien avec les anciennes classes
      .seq(function () {
        $sequence.withEntities(classesOidsToDel, constants.SEQUENCE_TYPE_GROUPE, { includeDeleted: true }, this)
      })
      .seqEach(function (sequence) {
        console.log(`La séquence ${sequence.nom} va être mise à jour.`)
        if (dryRun) return this()
        // On parcourt chaque sous-séquence et on enlève les classes à supprimer
        _.each(sequence.sousSequences, (sousSequence) => {
          if (_.find(sousSequence.eleves, eleve => eleve.type === constants.SEQUENCE_TYPE_GROUPE && classesOidsToDel.indexOf(eleve.oid) !== -1)) {
            _.remove(sousSequence.eleves, eleve => eleve.type === constants.SEQUENCE_TYPE_GROUPE && classesOidsToDel.indexOf(eleve.oid) !== -1)
            // On rajoute la classe à garder si besoin
            if (!_.find(sousSequence.eleves, eleve => eleve.type === constants.SEQUENCE_TYPE_GROUPE &&
            eleve.oid === classeToKeep.oid)) {
              sousSequence.eleves.push({
                oid: classeToKeep.oid,
                nom: classeToKeep.nom,
                type: constants.SEQUENCE_TYPE_GROUPE
              })
            }
          }
        })

        sequence.store(this)
      })

    // Suppression des anciennes classes
      .seq(function () {
        if (dryRun) return this()
        Groupe
          .match('oid').in(classesOidsToDel)
          .match('isClass').equals(true)
          .purge(this)
      })
      .seq(function (nbDeleted) {
        console.log(`Les classes suivantes ${dryRun ? 'auraient' : 'ont'} été supprimées : ${classesOidsToDel.join(' ')}`)
        // Mise à jour de la nouvelle classe
        if (dryRun) return this()
        classeToKeep.utilisateurs = studentsOids
        classeToKeep.store(this)
      })
      .seq(function () {
        console.log(`La classe ${classeToKeep.nom} (${classeToKeep.oid}) ${dryRun ? 'aurait' : 'a'} été mise à jour.`)
        cb()
      })
      .catch(cb)
  }

  function init (options) {
    if (!options) options = minimist(process.argv.slice(2))
    const { all, dryRun, force, strict, structure, toKeep, toDelete } = options
    if (dryRun && force) throw new Error('Impossible d’appeler les options dryRun et force simultanément')

    let mode = dryRun ? 'DRY-RUN' : force ? 'FORCE' : 'NORMAL'
    mode += strict ? 'STRICT' : ''
    let message = `Début du merge (mode : ${mode})`
    if (all) message += ' pour toutes les structures'
    else if (structure) message += ` pour la structure ${structure}`
    else if (toKeep && toDelete) message += ` de la classe ${toDelete} dans ${toKeep}`
    console.log(message)

    // Nettoyage de l'ensemble de la base
    if (all) return cleanAllStructures(options, done)
    if (structure) return cleanStructure(options, done)
    if (toKeep && toDelete) return merge(options, done)

    console.error('Il faut donner quelque chose à faire')
    main.help()
  }

  init()
}

main.help = () => {
  console.log(`
La commande mergeClasses demande comme argument les identifiants des classes à merger
Paramètres :
  --toKeep="oidToKeep": renseigne l'oid de la classe à garder
  --toDelete="oidToDelete": renseigne l'oid de la classe à supprimer
  --structure="oid": renseigne l'oid de la structure à traiter
Options :
  --all : Fusionne l'ensemble des classes dupliquées au sein d'une même structure
  --dryRun : signale tout ce qui devrait être fait sans le faire
  --force : permet de forcer le merge même si les classes n'ont pas le même nom
  --includeDeleted : pour merger également les classes softDeleted
  --strict : avec "--all" ne fusionne que les classes ayant exactement le même nom (sinon c'est non sensible à la casse et aux espaces de début et fin)
  `)
}

module.exports = main
