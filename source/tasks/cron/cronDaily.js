// tâche cli pour les nettoyages quotidien
// sur un linux standard, devrait être lancé dans un /etc/cron.d/sesalab avec par ex (ici lancement à 1h42
// 42 1  * *       *       leUser /path/to/sesalab/root/source/cli.js cronDaily --noStdout
// cela devrait simplement envoyer les erreurs en notification
// si l'on veut que les erreurs soient aussi affichées en console (donc catchées par cron
// qui enverra un mail à l'admin du serveur ou celui configuré dans le fichier cron),
// mais seulement elles, c'est plutôt
// 42 1  * *       *       leUser /path/to/sesalab/root/source/cli.js cronDaily -q
// si l'on veut pas de -q pour conserver toutes les infos dans un cli.log paramétré en config,
// on peut aussi faire (noter la redirection de stderr avant celle de stdout pour la conserver
// et pas l'envoyer également sur /dev/null)
// 42 1  * *       *       leUser /path/to/sesalab/root/source/cli.js cronDaily -q 2>&1 1>/dev/null

// @todo ajouter un contrôle des structures vides
// @todo ajouter un afterDelete sur les users (pour virer la structure si y'en a plus)
// @todo ajouter l'envoi du rapport quotidien avec les actions faites et un peu de stats sur l'activité de la veille

// cf logSetup pour la gestion des logs

const fs = require('fs')
const path = require('path')

const flow = require('an-flow')
const appLog = require('sesalab-api/source/tools/appLog')
const minimist = require('minimist')
const { addDays, diffDays, endOfDay, formatDate, formatDateTime } = require('sesajs-date')
// const { stringify } = require('sesajstools')

const { MAX_CONNEXION_DELAY_BEFORE_STRUCTURE_REMOVAL, TYPE_ELEVE } = require('sesalab-commun/constants.js')
const { getMailTextAccountValidationMissing } = require('sesalab-api/source/utilisateur/mailTemplates')

const { forcePurge, quiet, verbose } = minimist(process.argv.slice(2))

// LOGS - IMPORTANT
// Quand on est lancé par cron, la sortie (stdout & stderr) devrait être envoyée par mail
// Ici on utilise log qui va logguer dans cronDaily.log + stdout|stderr
// et par ailleurs sendNotification envoie aussi un mail si on a récolté des erreurs
const cronLog = appLog.getLogger('cronDaily')
cronLog.setLogLevel(quiet ? 'warning' : 'info')
const log = (...args) => {
  cronLog(...args)
  if (verbose) console.log(...args)
}
log.error = (...args) => {
  cronLog.error(...args)
  if (!quiet) console.error(...args)
}

const logIfError = (error) => {
  if (error) log.error(error)
}

/**
 * Retourne singular ou plural suivant nb
 * @param {number} nb
 * @param {string} singular
 * @param {string} [plural] si non fourni ce sera singular avec un "s" ajouté
 * @returns {string}
 */
const plural = (nb, singular, plural = '') => plural
  ? (nb > 1 ? plural : singular)
  : `${singular}${nb > 1 ? 's' : ''}`

const { application: { name: appName } } = require('../../../config')
const { MAX_AGE_BACKUP_MS, MAX_AGE_JOURNAL_MS, MAX_AGE_UTILISATEUR_ACCESS_MS } = require('../../../sesalab-commun/constants')

// le rapport
const report = {}

/**
 * Nombre limite d'entités traitées dans un lot
 * @private
 * @type {Number}
 */
const PURGE_LIMIT = 50
/**
 * Nb de jours de rétention en corbeille par défaut (si on trouve pas
 * settings.application.joursPurgeCorbeille ou qu'il a une valeur < 1)
 * @private
 * @type {number}
 */
const DEFAULT_JOURS_PURGE_CORBEILLE = 30

const lockFile = path.resolve(__dirname, '..', '..', '..', '_private', 'cronDaily.lock')

// les services affectés par init
let Backup, Journal, Structure, Utilisateur, UtilisateurAcces, $entitiesCli, $mail, $settings, $structure, $utilisateur, $utilisateurAcces, $validators

function _init () {
  Backup = lassi.service('Backup')
  Journal = lassi.service('Journal')
  Structure = lassi.service('Structure')
  Utilisateur = lassi.service('Utilisateur')
  UtilisateurAcces = lassi.service('UtilisateurAcces')
  $entitiesCli = lassi.service('$entities-cli')
  $mail = lassi.service('$mail')
  $settings = lassi.service('$settings')
  $structure = lassi.service('$structure')
  $utilisateur = lassi.service('$utilisateur')
  $utilisateurAcces = lassi.service('$utilisateurAcces')
  $validators = lassi.service('$validators')
}

/**
 * Supprime les comptes expirés et envoie un rappel aux comptes en attente d'activation depuis plus de joursAvantExpiration/2 (cf config)
 * (profs ou élèves qui n'ont pas été validés ni rejetés)
 * @private
 * @param done
 */
function cronExpiredAccount (done) {
  log('Recherche des comptes expirés')
  const joursAvantExpiration = app.settings.application.joursAvantExpiration
  const silentDelay = Math.round(joursAvantExpiration / 2)
  const now = Date.now()
  let nbExpiredAccount = 0
  let nbNearExpireAccount = 0

  // appliqué à chaque utilisateur isValid = null (donc ni accepté ni rejeté)
  const onUser = (utilisateur, cb) => {
    const { nom, prenom, oid, type, created, removeDate, validators } = utilisateur
    const isValid = $validators.isValid(utilisateur)
    if (isValid !== null) {
      log.error(`user qui remonte avec du isValid null mais avec $validators.isValid qui dit ${isValid} !`, utilisateur)
      return cb()
    }
    if (removeDate?.getTime() > now) {
      // Compte déjà traité et pas encore expiré
      return cb()
    }
    let createdAt
    if (validators?.account?.createdAt) {
      createdAt = validators.account.createdAt
    } else if (created) {
      if (validators.account) {
        log.error(`utilisateur ${oid} avec validators.account mais sans validators.account.createdAt`, utilisateur.validators)
        createdAt = created
      } else {
        // sinon c'était un import tableur fait avant 2023-08-31, on arrête là
        // @todo ajouter ce validator avec accepted: true ici
        return cb()
      }
    } else {
      // là ça déconne vraiment, en regarde sa dernière connexion, et si y'en a pas on le vire
      // (très vieux compte pas connecté depuis 18 mois qui aurait dû être supprimé)
      UtilisateurAcces
        .match('utilisateur').equals(oid)
        .count((error, nb) => {
          if (error) return cb(error)
          if (nb === 0) {
            log(`Suppression de l’utilisateur ${oid} sans validators.account.createdAt NI created, pas connecté depuis 18 mois`)
            // le delete va déclencher la purge des éléments liés
            utilisateur.delete(cb)
          } else {
            log.error(`utilisateur ${oid} sans validators.account.createdAt NI created, avec ${nb} connexions depuis 18 mois`, utilisateur)
            cb()
          }
        })
      return
    }

    // Inutile de continuer si le compte est en attente de validation depuis peu de temps (délai / 2)
    const daysSinceAccountCreation = diffDays(createdAt, new Date())
    if (daysSinceAccountCreation <= silentDelay) {
      return cb()
    }

    flow().seq(function () {
      utilisateur.removeDate = addDays(createdAt, joursAvantExpiration)
      if (utilisateur.removeDate.getTime() < now) {
        const typeLabel = type === TYPE_ELEVE ? 'élève' : 'prof'
        // avant de le supprimer on vérifie quand même qq trucs…
        if (utilisateur.externalMech) {
          log.error(`On allait supprimer l'utilisateur ${oid} (${typeLabel}) ${utilisateur.externalMech}/${utilisateur.externalId} qui est vu comme expiré, il faut aller corriger les données`)
          return cb()
        }
        nbExpiredAccount++
        log(`suppression de l’utilisateur ${oid}, expiré le ${formatDate({ date: utilisateur.removeDate, fr: true })} (${typeLabel} ${prenom} ${nom})`)
        utilisateur.delete(this)
      } else {
        nbNearExpireAccount++
        utilisateur.store(this)
      }
    }).seq(function (utilisateur) {
      // s'il a un mail on lui envoie une notif pour dire qu'il a pas encore été validé et on fixe une removeDate
      // FIXME mettre removeDate à la création du compte
      // attention, si on a fait un delete à l'étape précédente, utilisateur est undefined
      if (utilisateur?.mail) {
        $mail.sendMail({
          to: utilisateur.mail,
          subject: 'Compte en attente de validation',
          text: getMailTextAccountValidationMissing({ appName, utilisateur })
        }, this)
      } else {
        cb()
      }
    }).done(cb)
  }

  flow().seq(function () {
    $mail.setup(this)
  }).seq(function () {
    Utilisateur
      .match('isValid').isNull()
      .forEachEntity(onUser, this)
  }).seq(function () {
    // conclusion
    if (nbNearExpireAccount) {
      log(`${nbNearExpireAccount} ${plural(nbNearExpireAccount, 'compte')} ${plural(nbNearExpireAccount, 'proche')} de l’expiration (plus de ${silentDelay}j sur les ${joursAvantExpiration}j avant expiration)`)
    } else {
      log('Aucun compte proche de l’expiration')
    }
    if (nbExpiredAccount) {
      report.expiredAccounts = `${nbExpiredAccount} ${plural(nbExpiredAccount, 'compte')} ${plural(nbExpiredAccount, 'expiré')} (et ${plural(nbExpiredAccount, 'supprimé')})`
    } else {
      report.expiredAccounts = 'Aucun compte expiré'
    }
    log(report.expiredAccounts)
    done()
  }).catch(done)
}

/**
 * Purge les éléments en corbeille depuis plus de joursPurgeCorbeille (défini en config)
 * @private
 * @param done Callback
 */
function cronPurgeCorbeille (done) {
  let nbDays = app.settings.application.joursPurgeCorbeille
  if (nbDays < 1) nbDays = DEFAULT_JOURS_PURGE_CORBEILLE
  log('Purge de la corbeille')
  // la commande cli purgeDeleted de lassi cause en console
  const consoleLog = console.log
  console.log = () => undefined
  flow(['Groupe', 'Sequence', 'Utilisateur'])
    .seqEach(function (entityName) {
      $entitiesCli.commands().purgeDeleted(entityName, nbDays, this)
    })
    .seq(function (nbs) {
      // on restaure la console
      console.log = consoleLog
      const [grp, seq, usr] = nbs
      report.purgeCorbeille = `Purge de la corbeille avec ${grp} ${plural(grp, 'groupe')}, ${seq} ${plural(seq, 'sequence')} et ${usr} ${plural(usr, 'utilisateur')}`
      log(report.purgeCorbeille)
      done()
    })
    .catch(done)
}

/**
 * Purge les structures ayant une date de purge aujourd'hui (ou avant)
 * @private
 * @param done Callback
 */
function cronPurgeStructures (done) {
  // on est à priori lancé en début de journée et on veut purger tous ceux qui doivent l'être aujourd'hui
  const endOfToday = endOfDay()
  const oidsToSkip = []

  const getQuery = () => {
    let query = Structure.match('datePurge').before(endOfToday)
    if (oidsToSkip.length) {
      query = query.match('oid').notIn(oidsToSkip)
    }
    return query.sort('datePurge')
  }

  log('Purge des structures ayant dépassé leur date de purge')

  const limit = PURGE_LIMIT
  /** nb total de structures traitées */
  let nbStructuresProcessed = 0
  /** nb total de structures à purger */
  let nbStructuresToPurge = 0
  /** nb total de structures purgées */
  let nbStructuresPurged = 0

  const beforeBatch = () => {
    getQuery().count((error, nb) => {
      if (error) return done(error)
      if (nb) {
        log(`${nb} purges de structure à faire aujourd'hui`)
        nbStructuresToPurge = nb
        batch()
      } else {
        log('Pas de purge de structure à faire aujourd’hui')
        done()
      }
    })
  }

  const batch = () => {
    /** nb de structures traitées dans ce batch */
    let nbProcessed = 0
    /** nb de purges effectuées dans ce batch */
    let nbPurges = 0

    flow().seq(function () {
      // les structures purgées au batch précédent ne remontent plus
      // (purge effectuée ou date décalée ou oid ajouté à oidsToSkip)
      // => pas de skip, ce serait trop risqué d'en rater, on ne sait
      // pas trop si le pb a été traité en décalant la date (ça remonte plus)
      // ou autrement.
      getQuery().grab({ limit }, this)
    }).seqEach(function (structure) {
      nbProcessed++ // local
      nbStructuresProcessed++ // global
      // string de la date de purge, utilisée dans 2 seq ≠
      let purgeWanted

      const nextStructure = this

      flow().seq(function () {
        purgeWanted = formatDate({ date: structure.datePurge, fr: true }) // DD/MM/YYYY
        log(`purge de ${structure.oid} (demandée pour le ${purgeWanted})`)
        // si on veut forcer la purge indépendamment de la date de purge (même si trop vieux)
        const options = forcePurge ? { noCheckDatePurge: true } : {}
        $structure.purgeStructure(structure, options, (error, stats) => {
          // error.message loggé à l'étape suivante s'il existe
          this(null, error || stats)
        })
      }).seq(function (stats) {
        const str = `${structure.nom} (code: ${structure.code}, oid: ${structure.oid})`
        if (stats.message) {
          // c'est une erreur, faut éviter que cette structure ne remonte dans le batch suivant
          oidsToSkip.push(structure.oid)
          log.error(`Pour la structure ${str} on a le message ${stats.message}`)
        } else {
          nbPurges++ // local
          nbStructuresPurged++ // global
          // affichage du résultat
          let msg = `La structure ${structure.nom} (code: ${structure.code}, oid: ${structure.oid}`
          if (structure.uai) msg += `, uai: ${structure.uai}`
          msg += ') a bien été purgée : '
          log(msg)
          log(JSON.stringify(stats, null, 2))
        }
        this()
      }).done(function (error) {
        if (error) log.error(error)
        nextStructure()
      })
    }).seq(function () {
      let message = `${nbPurges} structures purgées dans ce batch`
      log(message)
      // pb éventuel dans ce batch
      if (nbPurges < nbProcessed) {
        message += ` mais ${nbProcessed - nbPurges} structures auraient dues être purgées et ne l'ont pas été`
        log.error(message)
      }
      if (!nbProcessed) {
        batchDone(Error('Il y a probablement un souci car ce batch n’a rien traité'))
      } else if (nbStructuresProcessed < nbStructuresToPurge) {
        process.nextTick(batch)
      } else {
        batchDone()
      }
    }).catch(batchDone)
  }

  const batchDone = (error) => {
    let message = `Fin de la purge des structures ayant dépassé leur date de purge, ${nbStructuresPurged} structures purgées`
    // totaux qui ne correspondent pas
    if (nbStructuresPurged < nbStructuresToPurge) message += ` (mais ${nbStructuresProcessed - nbStructuresPurged} échecs et ${nbStructuresToPurge - nbStructuresProcessed} non traitées)`
    log(message)
    report.purgeStructures = message
    done(error)
  }

  beforeBatch()
}

/**
 * Ajoute une date de suppression à la date du jour pour les structures sans utilisateur
 * @param done
 */
function cronFlagStructuresToRemove (done) {
  const limit = 500
  let skip = 0
  let nbFlagged = 0

  const batch = () => {
    let nbStructures = 0
    flow().seq(function () {
      Structure.match('removeDate').isNull().sort('oid').grab({ limit, skip }, this)
    }).seqEach(function (structure) {
      nbStructures++
      const nextStructure = this
      $structure.countUsers(structure.oid, (error, nbUsers) => {
        if (error) return nextStructure(error)
        if (nbUsers !== 0) return nextStructure()
        structure.removeDate = new Date()
        structure.store(nextStructure)
        nbFlagged++
      })
    }).seq(function () {
      if (nbStructures === limit) {
        skip += limit
        batch()
      } else {
        report.flagStructureToRemove = `${nbFlagged} ${plural(nbFlagged, 'structure')} à supprimer (sans utilisateur => flag)`
        done()
      }
    }).catch(done)
  } // batch

  batch()
}

/**
 * Supprime les structures en attente de suppression.
 * @private
 * @param done Callback
 */
function cronRemoveStructures (done) {
  function onEachStructure (structure, nextStructure) {
    const oid = structure.oid
    let structureString = `${oid} ${structure.type ?? ''} ${structure.nom}`
    if (structure.externalMech) structureString += ` (${structure.externalMech}/${structure.externalId})`

    // on vérifie qu'il n'y a pas de formateur dedans
    flow().seq(function () {
      $utilisateur.countTeacherByStructure(oid, this)
    }).seq(function (teachersCount) {
      if (teachersCount > 0) {
        // y'a eu un formateur ajouté depuis que le dernier avait été viré… bizarre mais ça peut arriver (si un prof sort d'un étab et qu'un autre se connecte dessus ensuite mais le même jour)
        log(`Impossible de supprimer la structure ${oid}, celle-ci possède ${teachersCount} ${plural(teachersCount, 'formateur')} (on supprime son removeDate)`)
        // on retire le removeDate (il sera remis à la suppression du compte formateur)
        delete structure.removeDate
        structure.store(logIfError)
        return nextStructure()
      }

      // pas de formateur, on regarde si des élèves se sont connectés depuis x jours
      const filters = {
        after: addDays(new Date(), -1 * MAX_CONNEXION_DELAY_BEFORE_STRUCTURE_REMOVAL), // count() prend une date ou un timestamp
        structure: oid,
        type: TYPE_ELEVE
      }
      $utilisateurAcces.count(filters, this)
    }).seq(function (accessCount) {
      if (accessCount) {
        log(`Impossible de supprimer la structure ${oid} car il y a des connexions élèves de moins de ${MAX_CONNEXION_DELAY_BEFORE_STRUCTURE_REMOVAL} jours (on décale son removeDate de ${MAX_CONNEXION_DELAY_BEFORE_STRUCTURE_REMOVAL} jours)`)
        // décale le removeDate (sans regarder la dernière connexion, pas bien grave de le garder encore un peu)
        structure.removeDate = new Date(addDays(new Date(), MAX_CONNEXION_DELAY_BEFORE_STRUCTURE_REMOVAL))
        structure.store(logIfError) // en tâche de fond
        return nextStructure()
      }
      // avant la suppression, il faut quand même vérifier qu'il ne reste pas de comptes élève,
      // et les virer si c'est le cas (pas connecté depuis longtemps sinon on ne serait plus là)
      Utilisateur
        .match('structures').equals(oid)
        .match('type').equals(TYPE_ELEVE) // à priori inutile, garde-fou en cas de bug sur le code précédent
        .softPurge(this) // purge soft au cas où…
    }).seq(function (nbElevesSuppr) {
      // c'est tout bon, on peut supprimer la structure
      if (nbElevesSuppr) {
        log(`Il restait ${nbElevesSuppr} dans la structure ${structureString} => softDelete (des élèves et de la structure)`)
        structure.softDelete(this)
      } else {
        structure.delete(this)
      }
    }).seq(function () {
      log(`Structure ${structureString} supprimée`)
      structureRemovedCount++
      nextStructure()
    }).catch(function (error) {
      log.error(`Erreur dans la suppression de la structure ${oid}`, error)
      nextStructure()
    })
  }

  let structureRemovedCount = 0

  // on ne fait pas de boucle skip/limit, on suppose qu'on aura jamais plus de 1000 structures à virer,
  // et si ça arrive les 1000 suivantes seront supprimées le lendemain
  log('Purge puis suppression des structures en attente de suppression')
  flow().seq(function () {
    const today = new Date()
    const endDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 23, 59, 59)
    Structure
      .match('removeDate').before(endDate)
      .forEachEntity(onEachStructure, this, { limit: 1000 })
  }).seq(function () {
    report.removeStructure = structureRemovedCount
      ? `${structureRemovedCount} ${plural(structureRemovedCount, 'structure')} ${plural(structureRemovedCount, 'supprimée')}`
      : 'Pas de suppression de structure aujourd’hui'
    log(report.removeStructure)
    done()
  }).catch(done)
}

/**
 * Supprime tous les UtilisateurAcces de plus de 18 mois (devrait plus y en avoir…)
 */
function cronRemoveOldAccess (done) {
  const oldestTs = Date.now() - MAX_AGE_UTILISATEUR_ACCESS_MS
  const oldestJour = formatDate(oldestTs)
  UtilisateurAcces.match('timestamp').lowerThan(oldestTs).purge((error, nb) => {
    if (error) return done(error)
    const message = `${nb} ${plural(nb, 'entrée')} UtilisateurAcces ${plural(nb, 'supprimée')} (avant ${oldestJour})`
    log(message)
    report.removeOldAccess = message
    done()
  })
}

/**
 * Supprime tous les backups de plus de MAX_AGE_BACKUP_MS
 * @param done
 */
function cronRemoveOldBackup (done) {
  const oldestTs = Date.now() - MAX_AGE_BACKUP_MS
  const oldestJour = formatDate(oldestTs)
  // le .purge() peut planter si y'en a trop, on fait ça par paquet de 1000
  let total = 0
  const limit = 1000
  const batch = () => {
    Backup.match('timestamp').lowerThan(oldestTs).purge({ limit }, (error, nb) => {
      if (error) return done(error)
      total += nb
      if (nb < limit) {
        const message = `${total} ${plural(total, 'entité')} de Backup ${plural(total, 'supprimée')} (avant ${oldestJour})`
        log(message)
        report.removeOldBackup = message
        done()
      } else {
        process.nextTick(batch)
      }
    })
  }
  batch()
}

/**
 * Supprime toutes les entrées de Journal de plus de MAX_AGE_JOURNAL_MS
 * @param done
 */
function cronRemoveOldJournal (done) {
  const oldestTs = Date.now() - MAX_AGE_JOURNAL_MS
  const oldestJour = formatDate(oldestTs)
  const limit = 1000
  let total = 0
  const batch = () => {
    Journal.match('timestamp').lowerThan(oldestTs).purge({ limit }, (error, nb) => {
      if (error) return done(error)
      total += nb
      if (nb < limit) {
        const message = `${total} ${plural(total, 'entrée')} de Journal ${plural(total, 'supprimée')} (avant ${oldestJour})`
        log(message)
        report.removeOldJournal = message
        done()
      } else {
        process.nextTick(batch)
      }
    })
  }
  batch()
}

/**
 * Supprime les comptes prof non validés et les structures qui n'ont pas de compte formateur.
 * @private
 * @param done Callback
 */
function cronRemoveNonValidatedAccounts (done) {
  const dateCreationMax = addDays(new Date(), -1 * app.settings.application.joursAvantExpiration)
  const dateCreationMaxFr = formatDate({ date: dateCreationMax, fr: true })

  // les structures dont on a viré des users
  const structureOids = new Set()

  log('Purge des comptes formateurs non validés et des structures sans compte formateur')
  let message
  flow().seq(function () {
    $utilisateur.purgeTeachersNotValidated(dateCreationMax, this)
  }).seq(function (nbDeleted) {
    message = `${nbDeleted} ${plural(nbDeleted, 'formateur')} ${plural(nbDeleted, 'invalide')} ${plural(nbDeleted, 'supprimé')} (non ${plural(nbDeleted, 'validé')} et ${plural(nbDeleted, 'créé')} avant ${dateCreationMaxFr})`
    log(message)
    // les élèves non validés
    $utilisateur.purgeStudentsNotValidated(dateCreationMax, this)
  }).seq(function (nbDeleted) {
    const msg2 = `${nbDeleted} ${plural(nbDeleted, 'élève')} (non ${plural(nbDeleted, 'validé')} et ${plural(nbDeleted, 'créé')} avant ${dateCreationMaxFr})`
    log(msg2)
    report.removeNonValidatedAccounts = message + ' et ' + msg2
    $utilisateur.checkStructureToRemove(Array.from(structureOids), this)
  }).empty().done(done)
}

/**
 * Si lockfile existe, s'il a moins de 36h ça passe une erreur à next,
 * sinon note le pb et le supprime.
 * @param next
 */
function checkLock (next) {
  const stats = fs.statSync(lockFile, { throwIfNoEntry: false })
  if (!stats) return next()

  // le lock existe (on prend mtime et pas ctime car ctime ne peut pas être modifié par un touch,
  // et il est modifié par un chmod)
  const fileTs = stats.mtime
  if (Date.now() - fileTs > 36 * 3600_000) {
    // il a plus de 36h
    const msg = `${lockFile} a plus de 24h (${formatDateTime({ ts: fileTs })}), on l'efface`
    fs.unlinkSync(lockFile)
    log.error(msg)
    // l'erreur est notée, on continue
    return next()
  }

  // il a moins de 24h, on passera cette erreur à next
  const msg = `Impossible de lancer cronDaily, le précédent tourne encore (il y a un fichier ${lockFile} qui date de ${formatDateTime({ ts: fileTs })})`
  const lockError = new Error(msg)
  const exit = (error) => {
    if (error) {
      // une erreur dans notre code, on l'ajoute aussi à appLog
      appLog.error(error)
      log.error(error)
    }
    // on retourne l'erreur du fichier de lock
    next(lockError)
  }

  // faut notifier que le cron pourra pas se lancer
  $mail.setup((error) => {
    if (error) return exit(error)
    const data = {
      subject: `Impossible de lancer le cron.daily pour ${appName}`,
      text: msg,
      // pour ne pas avoir de throttle, même si ça semble inutile ici
      // car on est le seul appel de ce process
      doNotThrottle: true
    }
    $mail.sendNotification(data, exit)
  })
}

/**
 * La commande cli exportée, lance les tâches quotidiennes
 * @param {simpleCallback} done
 */
function cronDaily (done) {
  // on stub console.error, donc aussi log.error qui l'appelle, pour récupérer d'éventuelles erreurs et les envoyer par mail à la fin
  const errors = []
  const consoleError = console.error
  console.error = (...args) => {
    errors.push(...args)
    consoleError.apply(console, args)
  }

  const beforeExit = (error) => {
    console.error = consoleError
    // toujours dans le log et pas à l'écran
    cronLog('Cron daily END')
    // faut virer le lock, on vérifie quand même que le lock existe,
    // au cas où ce serait le writeFileSync qui aurait planté
    if (fs.existsSync(lockFile)) {
      fs.unlinkSync(lockFile)
    }
    uncaugthExit = false
    done(error)
  }

  // pour choper un ctrl+c ou toute autre interruption non déclenchée par notre code
  let uncaugthExit = true
  const exitListener = (code) => {
    if (uncaugthExit) {
      log.error(Error(`Process interrompu (code ${code}) avec une tâche cronDaily en cours`))
      beforeExit()
    }
    // sinon beforeExit a déjà été appelé
  }
  // on le lance sur ces signaux (pas SIGHUP avec un listener faut appeler ensuite soi-même process.exit())
  for (const signal of ['exit', 'SIGINT', 'SIGTERM']) {
    process.on(signal, exitListener)
  }

  flow().seq(function () {
    // toujours dans le log et pas à l'écran
    cronLog('Cron daily START')
    _init()
    // on vérifie le lockfile, faut gérer la callback ici pour choper l'erreur,
    // (si elle allait dans le catch ça virerait le lockfile)
    const nextStep = this
    checkLock((error) => {
      if (error) {
        return done(error)
      }
      nextStep()
    })
  }).seq(function () {
    const isProd = $settings.get('application.isProd', false)
    const tasks = [
      cronExpiredAccount,
      cronPurgeCorbeille]
    // hors prod on purge pas les structures
    if (isProd) {
      tasks.push(cronPurgeStructures)
    } else {
      const msg = 'Hors prod pas de purge de structure'
      report.purgeStructures = msg
      log(msg)
    }
    // faut ajouter les tâches qui suivent la purge
    tasks.push(cronRemoveNonValidatedAccounts, cronFlagStructuresToRemove, cronRemoveStructures, cronRemoveOldAccess, cronRemoveOldBackup, cronRemoveOldJournal)
    // et on lance les tâches
    this(null, tasks)
  }).seqEach(function (task) {
    const nextTask = this
    // si une tâche plante, on veut quand même exécuter la suivante
    process.nextTick(task, (error) => {
      if (error) log.error(error)
      nextTask()
    })
  }).seq(function () {
    // toutes les tasks sont terminées
    if (quiet && errors.length) {
      // y'a rien eu en console, on écrit quand même qu'il y a eu des erreurs
      console.error(`Il y a eu ${errors.length} ${plural(errors.length, 'erreur')}, détails dans le cronDaily.log`)
    }
    // et on notifie par mail la synthèse
    $mail.setup(this)
  }).seq(function () {
    const data = {
      subject: `cron.daily du ${formatDate()} pour ${appName}`,
      text: 'Synthèse des actions :\n',
      // pour ne pas avoir de throttle, même si ça semble inutile ici
      // car on est le seul appel de ce process
      doNotThrottle: true
    }
    if (errors.length) {
      // y'a des erreurs, on l'ajoute au début du sujet / body
      data.subject = 'ERREUR dans le ' + data.subject
      data.text = errors.map(e => e.stack || e).join('\n') + '\n\n' + data.text
    }
    // et l'ajout de la synthèse, erreurs ou pas
    for (const [key, value] of Object.entries(report)) {
      data.text += `${key}: ${value}\n`
    }
    $mail.sendNotification(data, beforeExit)
  }).catch(beforeExit)
} // cronDaily

cronDaily.help = () => console.log(`La tâche cronDaily ne prend pas d’argument.
Elle fait :
  - validation des demandes de création de compte sans réponse à leur date d’expiration
  - purge les corbeilles des éléments trop vieux
  - purge les structures des comptes élèves, si la date de purge est dépassé de moins de 30j (sinon ça râle sans rien faire)
  - suppression des structures en attente de suppression
  - suppression des comptes formateurs non validés et des structures sans compte formateur

Elle est prévue pour être exécutée par cron mais peut être lancée manuellement. Par défaut les infos du déroulement ne sont écrites que dans le log. Passer -v pour les voir aussi à l'écran (ou -q pour les supprimer aussi du log).

En cas d'erreurs, elles sont notifiées par mail à la fin

Options :
  --forcePurge : purger même si les dates de purges ne correspondent pas (trop vieux)
  -q --quiet : ne pas afficher les messages d'info (ni en console ni dans le log)
  -v --verbose : afficher tous les messages d'info en console
`)

module.exports = cronDaily
