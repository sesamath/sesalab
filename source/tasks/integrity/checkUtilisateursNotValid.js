const { TYPE_ELEVE } = require('sesalab-commun/constants')
const appLog = require('sesalab-api/source/tools/appLog')

/**
 * Vérifie la cohérence de la table des utilisateurs
 * @param {function} done
 */
function checkUtilisateursNotValid (done) {
  const Utilisateur = lassi.service('Utilisateur')
  let total = 0
  let nbValidatedAdded = 0

  const log = (msg) => {
    appLog(msg)
    console.log(msg)
  }
  const logErr = (...args) => {
    appLog.error(...args)
    console.error(...args)
  }

  const onUser = (user, cb) => {
    total++
    if (total % 1000 === 0) console.log('On en est à', { total, nbValidatedAdded })
    if (user.validators?.account) {
      return cb()
    }

    const validAccount = () => {
      if (!user.validators) user.validators = {}
      user.validators.account = {
        accepted: true,
        createdAt: today,
        dateResponse: today,
        validatedBy: 'CLI-checkUtilisateur'
      }
      user.store((error) => {
        if (error) return cb(error)
        nbValidatedAdded++
        log(`utilisateur ${user.oid} validé d'office`)
        cb()
      })
    }
    const today = new Date()

    // avant on ne mettait pas de validators quand y'avait rien à valider (import par ex)
    // dans ce cas on l'ajoute d'office validé, idem pour ceux qui on un externalMech
    if (!user.validators || user.externalMech) return validAccount()

    // comptes locaux, pour les élèves on bloque pas sur le mail,
    // mais y'a eu une époque ou le validator account n'était créé que ensuite, comme pour les formateurs
    if (user.validators.mail) {
      if (user.type === TYPE_ELEVE) {
        // on lui crée son validator.account tout de suite, sans le valider ni le refuser
        // (ça le fera apparaître sur la liste des comptes à valider pour le formateur,
        // même si l'élève n'a pas validé son mail)
        user.validators.account = {
          createdAt: today
        }
        return user.store(cb)
      }
      // pour un formateur on le laisse valider son mail d'abord
    } else {
      logErr(`utilisateur ${user.oid} avec validators mais ni account ni mail`, user.validators)
    }
    cb()
  }

  const afterAll = (error) => {
    if (error) return done(error)
    done(null, { total, nbValidatedAdded })
  }

  Utilisateur
    .match('isValid').equals(null)
    .includeDeleted()
    .forEachEntity(onUser, afterAll)
}
checkUtilisateursNotValid.help = () => console.log('La commande checkUtilisateursNotValid ne prend pas d’arguments.\nElle liste les anomalies trouvées sur les utilisateurs ni validés ni refusés')

module.exports = checkUtilisateursNotValid
