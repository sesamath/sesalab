'use strict'

const flow = require('an-flow')
const minimist = require('minimist')
const util = require('util')
const _ = require('lodash')

function validation (done) {
  const { all, dump, limit, deep, dumpAll } = minimist(process.argv.slice(2))
  let { entities } = minimist(process.argv.slice(2))

  if (entities) {
    entities = entities.split(',')
  } else {
    entities = _.keys(lassi.service('$entities').entities)
  }

  console.log('Running validation on ' + entities.join(', '))

  flow(entities)
    .seqEach(function (entityName) {
      const countByErrors = {}
      let errorCount = 0
      let validCount = 0
      console.log(`=============\n============= Entity ${entityName}\n=============`)

      const onEntity = (entity, cb) => {
        entity.isValid((err) => {
          if (err) {
            const msg = err.message
            if (!countByErrors[msg]) {
              countByErrors[msg] = 0
            }
            if (dumpAll || (countByErrors[msg] === 0 && dump)) {
              // Avec l'option dump, on affiche les détails de la prmière entité en échec
              console.log(`\n${entityName}#${entity.oid} error: ${msg}`)
              if (!deep) console.log(entity)
              if (deep) console.log(util.inspect(entity, false, null))
            }
            countByErrors[msg]++
            errorCount++
            if (validCount % 100 === 0) process.stdout.write('e') // progression
          } else {
            validCount++
            if (validCount % 100 === 0) process.stdout.write('.') // progression
          }
          cb()
        }, {
          onlyChangedAttributes: !all
        })
      }

      lassi.service(entityName).match().includeDeleted().forEachEntity(onEntity, (error, nb) => {
        if (error) return done(error)
        if (!errorCount) console.log(`\nPas d'erreurs : ${nb} ${entityName} valides`)

        this(null, { [entityName]: countByErrors })
      }, { limit })
    })
    .done(done)
}

validation.help = function displayHelp () {
  console.log(
    `
La commande validation vérifie que les entities sont valides
Options :
  --all : on vérifie toutes les règles de validation des entités même les validateOnChange
  --entities=Utilisateur,Groupe : on limite la validation aux entités spécifiées
  --dump : affiche les entités invalides en console en tant qu'exemple, une fois pour chaque message d'erreur
  --deep : dump en profondeur des entités
  --dumpAll : affiche toutes les entités invalides en console
  --limit=100 : pour ne valider que les x premières entités par type d'entité
`
  )
}
module.exports = validation
