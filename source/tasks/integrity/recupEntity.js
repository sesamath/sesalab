const { createReadStream, existsSync } = require('node:fs')
const flow = require('an-flow')
const minimist = require('minimist')

/**
 * Parse un fichier *.export (un objet par ligne en json) pour ajouter à la db ce qui n'y est pas
 * @param {function} _done
 */
function recupEntities (_done) {
  // un wrapper de _done
  const done = (error, results) => {
    if (error) {
      while (error.cause) error = error.cause
      console.error('plantage avec', error, 'on avait', getResults())
      return _done(error)
    }
    _done(null, results)
  }

  const { file } = minimist(process.argv.slice(2))
  if (!file) return done(Error('Il faut fournir l’option "--file xxx"'))
  if (!existsSync(file)) return done(Error(`${file} n’existe pas`))

  let Entity, entityName
  for (const e of ['Groupe', 'Sequence', 'Structure', 'Utilisateur']) {
    if (RegExp(`${e}.export`).test(file)) {
      console.log(`Recup de l’entity ${e}`)
      entityName = e
      Entity = lassi.service(e)
      break
    }
  }
  if (!Entity) return _done(Error(`Pas trouvé d'entity correspondant à ${file} (il doit finir par xxx.export où xxx est le nom d’une entity)`))

  // on part sur des exports lisibles (un json par ligne)
  const stream = createReadStream(file)
  let str = ''
  const entities = {}
  let isStarted = false

  const dateRegExp = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d+)?Z/
  const reviver = (key, value) => {
    if (dateRegExp.test(value)) return new Date(value)
    if (typeof value === 'object' && value.$date) return value.$date
    return value
  }

  // gestion de la sortie provoquée, pour choper un ctrl+c ou toute autre interruption non déclenchée par notre code
  let uncaugthExit = true
  const exitListener = (code) => {
    if (uncaugthExit) {
      console.log(`Process interrompu (code ${code}) avec une tâche en cours, elle en est à`, getResults())
      uncaugthExit = false
    }
  }
  // on le lance sur ces signaux (pas SIGHUP avec un listener faut appeler ensuite soi-même process.exit())
  for (const signal of ['exit', 'SIGINT', 'SIGTERM']) {
    process.on(signal, exitListener)
  }

  stream.on('data', (data) => {
    str += data
    if (str.includes('\n')) {
      const chunks = str.split('\n')
      str = chunks.pop()
      for (const chunk of chunks) {
        nbEntitiesInExport++
        const mongoObj = JSON.parse(chunk, reviver)
        entities[mongoObj._data.oid] = mongoObj._data
        if (!isStarted) {
          isStarted = true
          recup()
        }
      }
    }
  })
  stream.on('end', () => {
    doneWhenFinished()
  })

  let nbEntitiesInExport = 0
  let nbEntitiesOk = 0
  let nbEntitiesChanged = 0
  let nbEntitiesAdded = 0
  let nbAlreadyRecreatedEntities = 0
  const getResults = () => ({ nbEntitiesInExport, nbEntitiesOk, nbEntitiesChanged, nbEntitiesAdded, nbAlreadyRecreatedEntities })

  const isFinished = () => {
    const nbRestant = Object.keys(entities).length
    const total = nbEntitiesOk + nbEntitiesChanged + nbEntitiesAdded + nbAlreadyRecreatedEntities
    return !nbRestant && nbEntitiesInExport === total
  }

  /**
   * Lance la récupération des objets issus de l'export pour les ajouter en db s'ils n'y sont pas déjà
   */
  const recup = () => {
    const reste = Object.keys(entities)
    if (!reste.length && !isFinished()) {
      setTimeout(recup, 50)
      return
    }
    flow(reste)
      .seqEach(function (oid) {
        const next = this
        Entity
          .match('oid').equals(oid)
          .includeDeleted()
          .grabOne((error, e) => {
            if (error) return next(error)
            if (e) {
              // si c'est une séquence, elle peut être avec un owner deleted
              if (e.owner === 'deleted') {
                Object.assign(e, entities[oid])
                return e.store(error => {
                  if (error) return next(error)
                  delete entities[oid]
                  nbEntitiesChanged++
                  next()
                })
              }
              // sinon on touche à rien
              nbEntitiesOk++
              delete entities[oid]
              return next()
            }
            Entity.create(entities[oid]).store(error => {
              if (error) {
                while (error.cause) error = error.cause
                if (error?.message?.match(/(duplicate key|pour cause de doublon)/)) {
                  console.warn(`WARNING : impossible de recréer l'entity ${entityName} ${oid} pour cause de doublon : ${error.message}`)
                  nbAlreadyRecreatedEntities++
                  delete entities[oid]
                  return next()
                }
                // sinon c'est vraiment un pb et on arrête là
                error.entity = entities[oid]
                return next(error)
              }
              nbEntitiesAdded++
              delete entities[oid]
              next()
            })
          })
      })
      .seq(function () {
        console.log('On en est à', getResults())
        if (!isFinished()) setTimeout(recup, 50)
      })
      .catch(done)
  }

  const doneWhenFinished = () => {
    if (isFinished()) {
      uncaugthExit = false
      return done(null, getResults())
    }
    setTimeout(doneWhenFinished, 200)
  }
}
recupEntities.help = () => console.log('La commande recupEntities exige une option --file {fichier d’export}.\nElle ajoute toutes les structures du fichier qui n’existerait pas déjà')

module.exports = recupEntities
