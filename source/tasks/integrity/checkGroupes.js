const minimist = require('minimist')

const { pick } = require('sesalab-commun/utils')
const { TYPE_ELEVE } = require('sesalab-commun/constants')
const appLog = require('sesalab-api/source/tools/appLog')

/**
 * Vérifie la cohérence des groupes/classes
 *
 * @param {Object} [resultats] Stats des erreurs
 * @param {function} done
 */
function checkGroupes (done) {
  const Groupe = lassi.service('Groupe')
  const Utilisateur = lassi.service('Utilisateur')

  const { fix, structure } = minimist(process.argv.slice(2))

  console.log(`============ Intégrité des groupes/classes ${structure ? `de la structure ${structure}` : ''} ============`)
  const resultats = {
    erreursGroupes: 0,
    nbGroupes: 0
  }

  const classeOidsByEleveOid = {} // Pour chaque élève on liste les classes où il apparait
  const classeDetailsByOid = {}
  const entityToSave = {}

  const logAnomalieGroupe = (text, groupe) => {
    resultats.erreursGroupes++
    console.log(`Anomalie groupe#${groupe.oid} '${groupe.nom}' : ${text}`)
    appLog.error(`Anomalie groupe#${groupe.oid} '${groupe.nom}' : ${text}`)
  }

  const onGroupe = (groupe, cb) => {
    resultats.nbGroupes++
    if (groupe.owner && groupe.structure) logAnomalieGroupe('groupe perso ayant une structure', groupe)
    // pour les groupes non classe on ne contrôle rien d'autre
    if (!groupe.isClass) return cb()

    // une classe n'a pas de proprio
    if (groupe.owner) logAnomalieGroupe('classe avec un owner', groupe)
    if (!groupe.structure) logAnomalieGroupe('classe sans structure', groupe)

    // on note oid/nom pour le log ultérieur
    classeDetailsByOid[groupe.oid] = pick(groupe, ['oid', 'nom']) // // suffisamment d'infos pour logAnomalieGroupe

    // Sur une classe on regarde si elle a des élèves appartenant à une autre classe, on peuple classeOidsByEleveOid
    for (const eleveOid of groupe.utilisateurs) {
      if (!classeOidsByEleveOid[eleveOid]) classeOidsByEleveOid[eleveOid] = []
      classeOidsByEleveOid[eleveOid].push(groupe.oid)
    }

    // et on regarde si des élèves sont marqués dans cette classe sans être dans ses utilisateurs
    const query = Utilisateur
      .match('type').equals(TYPE_ELEVE)
      .match('classe').equals(groupe.oid)
    if (groupe.utilisateurs?.length) query.match('oid').notIn(groupe.utilisateurs)
    // on ne restreint pas à la structure pour trouver d'autres anomalies éventuelles
    query.forEachEntity(
      (eleve, nextEleve) => {
        if (!eleve.structures.includes(groupe.structure)) {
          console.log(`Anomalie élève#${eleve.oid} est dans la structure ${eleve.structures.join('|')} et a une classe (${groupe.oid}) qui est dans la structure ${groupe.structure}`)
          return nextEleve()
        }
        // on boucle ici sur les élèves qui déclarent être dans ce groupe sans être dans ses utilisateurs
        logAnomalieGroupe(`L’élève ${eleve.oid} n’était pas dans la classe ${groupe.oid} (alors qu'il annonce y être)`, groupe)
        // faut chercher si une autre classe le connaît aussi (une màj de classe qui n'aurait pas été répercutée sur le user ?)
        Groupe
          .match('isClass').equals('true')
          .match('utilisateurs').equals(eleve.oid)
          .grab((error, classes) => {
            if (error) return cb(error)
            if (!classes.length) {
              // ok, aucune classe ne le connaît, lui dit être dans cette classe on va l'y remettre
              if (fix) {
                groupe.utilisateurs.push(eleve.oid)
                entityToSave[groupe.oid] = groupe
              }
              return nextEleve()
            }
            if (classes.includes(groupe.oid)) {
              return cb(Error(`BUG dans checkGroupe, la classe ${groupe.oid} ne contenait pas l’élève ${eleve.oid} mais elle remonte quand on cherche les classes qui le contienne`))
            }
            if (classes.length === 1) {
              const classe = classes[0]
              if (eleve.structures.includes(classe.structure)) {
                logAnomalieGroupe(`L’utilisateur ${eleve.oid} déclare être dans la classe ${groupe.oid} qui ne le connaît pas, mais la classe ${classes[0].oid} le connaît`, groupe)
                if (fix) {
                  // on rectifie l'élève
                  eleve.classe = classes[0].oid
                  // on néglige la proba d'avoir le même oid pour un groupe et un élève
                  entityToSave[eleve.oid] = eleve
                }
              } else {
                logAnomalieGroupe(`L’utilisateur ${eleve.oid} (structures ${eleve.structures.join(', ')}) déclare être dans la classe ${groupe.oid} (structure ${groupe.structure}) qui ne le connaît pas, mais la classe ${classe.oid} (structure ${classe.structure}) le connaît, tout ça sur des structures différentes !`, groupe)
              }
            } else {
              const msg = `L’utilisateur ${eleve.oid} (structures ${eleve.structures.join(', ')}) déclare être dans la classe ${groupe.oid} (structure ${groupe.structure}) qui ne le connaît pas, et plusieurs classes le revendiquent : ` + classes.map(classe => `${classe.oid} (structure ${classe.structure})`).join(' / ')
              logAnomalieGroupe(msg, groupe)
            }
            nextEleve()
          })
      },
      cb
    )
  }

  const afterAll = (error) => {
    if (error) return done(error)

    const eleveDansPlusieursClassesParClasseOid = {}
    const duplicates = []
    for (const [eleveOid, classeOids] of Object.entries(classeOidsByEleveOid)) {
      // on ne garde que les élèves apparaissant dans plusieurs classes, et pour chacun d'eux
      // on crée un objet inverse: classe oid => [eleves apparaissant dans plusieurs classes]
      if (classeOids.length > 1) {
        for (const classeOid of classeOids) {
          if (!eleveDansPlusieursClassesParClasseOid[classeOid]) eleveDansPlusieursClassesParClasseOid[classeOid] = []
          eleveDansPlusieursClassesParClasseOid[classeOid].push(eleveOid)
        }
        duplicates.push(eleveOid)
      }
    }

    for (const [classeOid, eleveOids] of Object.entries(eleveDansPlusieursClassesParClasseOid)) {
      logAnomalieGroupe(`élèves présent dans d'autres classes : ${eleveOids.join(', ')}`, classeDetailsByOid[classeOid])
    }

    // et on sauvegarde si besoin
    storeAll()
  }

  const storeAll = () => {
    const entities = Object.values(entityToSave)
    if (!entities.length) return done(null, resultats)
    entities.pop().store((error, groupe) => {
      if (error) return done(error, resultats)
      delete entityToSave[groupe.oid]
      storeAll()
    })
  }

  // On ne vérifie pas les entités soft-deleted car les contrôles de cohérences ne se font
  // que sur les entitées non-supprimées (et donc à la restauration des entités supprimées)
  const query = structure ? Groupe.match('structure').equals(structure) : Groupe.match()
  query.forEachEntity(onGroupe, afterAll)
}
checkGroupes.help = () => {
  console.log('La commande checkGroupes ne prend pas d’argument.\nElle liste les anomalies trouvées sur les groupes.\nOptions possible :\n--fix : fait quelques corrections en base (ajout des élèves qui ont une classe mais ne sont pas dedans)\n--structure xxx : se limiter à la structure xxx')
}

module.exports = checkGroupes
