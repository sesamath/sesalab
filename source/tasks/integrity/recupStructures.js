// const { BSON, EJSON } = require('bson')
// const { existsSync, readFileSync } = require('node:fs')
const { createReadStream, existsSync } = require('node:fs')
const flow = require('an-flow')
const minimist = require('minimist')
const { TYPE_ELEVE } = require('../../../sesalab-commun/constants')

/**
 * Parse un fichier Structure.export (une structure par ligne en json) pour ajouter à la db celles qui n'y seraient pas
 * @param {function} _done
 */
function recupStructures (_done) {
  // un wrapper de _done
  const done = (error, results) => {
    if (error) {
      while (error.cause) error = error.cause
      console.error('plantage avec', error, 'on avait', getResults())
      return _done(error)
    }
    _done(null, results)
  }

  const { file } = minimist(process.argv.slice(2))
  if (!file) return done(Error('Il faut fournir l’option "--file xxx"'))
  if (!existsSync(file)) return done(Error(`${file} n’existe pas`))
  // const rawContent = readFileSync(file) // .toString()
  // const content = EJSON.deserialize(rawContent)
  // content.type est Buffer et content.data le contenu du Buffer
  // pas réussi à l'exploiter…

  // on part sur des exports lisibles (un json par ligne)
  const stream = createReadStream(file)
  let str = ''
  const structures = {}
  let isStarted = false

  const dateRegExp = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d+)?Z/
  const reviver = (key, value) => {
    if (dateRegExp.test(value)) return new Date(value)
    if (typeof value === 'object' && value.$date) return value.$date
    return value
  }

  // gestion de la sortie provoquée, pour choper un ctrl+c ou toute autre interruption non déclenchée par notre code
  let uncaugthExit = true
  const exitListener = (code) => {
    if (uncaugthExit) {
      console.log(`Process interrompu (code ${code}) avec une tâche en cours, elle en est à`, getResults())
      uncaugthExit = false
    }
  }
  // on le lance sur ces signaux (pas SIGHUP avec un listener faut appeler ensuite soi-même process.exit())
  for (const signal of ['exit', 'SIGINT', 'SIGTERM']) {
    process.on(signal, exitListener)
  }

  stream.on('data', (data) => {
    str += data
    if (str.includes('\n')) {
      const chunks = str.split('\n')
      str = chunks.pop()
      for (const chunk of chunks) {
        nbStructuresInExport++
        const mongoObj = JSON.parse(chunk, reviver)
        structures[mongoObj._data.oid] = mongoObj._data
        if (!isStarted) {
          isStarted = true
          recup()
        }
      }
    }
  })
  stream.on('end', () => {
    doneWhenFinished()
  })

  let nbStructuresInExport = 0
  let nbStructuresOk = 0
  let nbStructuresAdded = 0
  let nbUsersChanged = 0
  let nbSequencesChanged = 0
  let nbAlreadyRecreatedStructures = 0
  const getResults = () => ({ nbStructuresInExport, nbStructuresOk, nbStructuresAdded, nbUsersChanged, nbSequencesChanged, nbAlreadyRecreatedStructures })

  const Sequence = lassi.service('Sequence')
  const Structure = lassi.service('Structure')
  const Utilisateur = lassi.service('Utilisateur')

  const isFinished = () => {
    const nbRestant = Object.keys(structures).length
    const total = nbStructuresOk + nbStructuresAdded + nbAlreadyRecreatedStructures
    return !nbRestant && nbStructuresInExport === total
  }

  /**
   * Récupère les users de la structure oldOid qui n'existe plus pour les mettre dans celle qui a cet uai
   * @param {string} oldOid id de la structure disparue (et impossible à ajouter car y'en a déjà une avec son uai)
   * @param {string} uai uai de la structure vers laquelle migrer les utilisateurs
   * @param next
   */
  const recupUsers = (oldOid, uai, next) => {
    let newOid
    let nbUsers = 0

    // change l'oid de la structure pour les séquences de ce user qui étaient dans l'ancienne structure
    const changeSequences = (user, next) => {
      // change l'oid de la structure pour une séquence
      const changeSequence = (sequence, nextSequence) => {
        sequence.structure = newOid
        nbSequencesChanged++
        sequence.store(nextSequence)
      }
      Sequence
        .match('structure').equals(oldOid)
        .match('owner').equals(user.oid)
        .forEachEntity(changeSequence, next)
    }

    const changeUser = (user, next) => {
      if (user.structures.includes(newOid)) {
        // il y est déjà, faut juste virer l'ancienne
        user.structures = user.structures.filter(oid => oid !== oldOid)
      } else {
        // faut remplacer l'ancienne par la nouvelle
        user.structures = user.structures.map(oid => oid === oldOid ? newOid : oid)
      }
      nbUsers++
      user.store(error => {
        if (error) {
          if (/dup key.*structureUniqueLogin/.test(error.message)) {
            // un utilisateur qui a déjà été réimporté dans la nouvelle structure, on laisse tomber
            console.warn(`WARNING : le user ${user.oid} qui était dans la structure ${oldOid} devenue ${newOid} (${uai}) a un login ${user.login} qui existe déjà dans la nouvelle structure (on le laisse orphelin dans la structure disparue)`)
            return next()
          }
        }
        nbUsersChanged++
        // avant de passer à _next faut mettre à jour la structure dans les séquences de l'utilisateur
        // pour les élèves y'a pas de màj de séquence à faire
        if (user.type === TYPE_ELEVE) return next()
        // pour les prof faut màj la propriété structure des séquences qui étaient dans l'ancienne
        changeSequences(user, next)
      })
    }

    flow().seq(function () {
      Structure
        .match('uai').equals(uai)
        .includeDeleted()
        .grabOne(this)
    }).seq(function (s) {
      if (!s) return next(Error(`On a eu un doublon sur l’uai ${uai} mais on ne retrouve pas cette structure`))
      // faut changer les users de oldOid vers s.oid
      newOid = s.oid
      Utilisateur
        .match('structures').equals(oldOid)
        .includeDeleted()
        .forEachEntity(changeUser, this)
    }).seq(function () {
      console.log(`${nbUsers} utilisateurs déplacés de ${oldOid} vers ${newOid} (${uai})`)
      next()
    }).catch(next)
  }

  /**
   * Lance la récupération des objets issus de l'export pour les ajouter en db s'ils n'y sont pas déjà
   */
  const recup = () => {
    const reste = Object.keys(structures)
    if (!reste.length && !isFinished()) {
      setTimeout(recup, 50)
      return
    }
    flow(reste)
      .seqEach(function (oid) {
        const next = this
        Structure
          .match('oid').equals(oid)
          .includeDeleted()
          .grabOne((error, s) => {
            if (error) return next(error)
            if (s) {
              nbStructuresOk++
              delete structures[oid]
              return next()
            }
            Structure.create(structures[oid]).store(error => {
              if (error) {
                while (error.cause) error = error.cause
                if (error?.message?.match(/(duplicate key|pour cause de doublon)/)) {
                  // à priori c'est un uai, faut récupérer la nouvelle structure et changer les utilisateurs qui étaient dans l'ancienne
                  const chunks = /uai: "(\d+\w)"/.exec(error.message)
                  console.warn(`WARNING : la structure disparue ${oid} a déjà été recréée avec un autre oid et le même uai) : ${error.message}`)
                  nbAlreadyRecreatedStructures++
                  delete structures[oid]
                  if (chunks) return recupUsers(oid, chunks[1], next)
                  return next()
                }
                // sinon c'est vraiment un pb et on arrête là
                error.structure = structures[oid]
                return next(error)
              }
              nbStructuresAdded++
              delete structures[oid]
              next()
            })
          })
      })
      .seq(function () {
        console.log('On en est à', getResults())
        if (!isFinished()) setTimeout(recup, 50)
      })
      .catch(done)
  }

  const doneWhenFinished = () => {
    if (isFinished()) {
      uncaugthExit = false
      return done(null, getResults())
    }
    setTimeout(doneWhenFinished, 200)
  }
}
recupStructures.help = () => console.log('La commande recupStructures exige une option --file {fichier d’export}.\nElle ajoute toutes les structures du fichier qui n’existerait pas déjà')

module.exports = recupStructures
