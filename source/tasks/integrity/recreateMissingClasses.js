const minimist = require('minimist')
const flow = require('an-flow')
const { TYPE_ELEVE } = require('sesalab-commun/constants')

/**
 * Pour chaque structure, cherche les classes, récupère tous les élèves dans aucune d'elles et crée les classes qui manquent.
 * Si un élève a une classe, que cette classe n'existe pas, et qu'aucune classe ne contient l'élève, alors on crée une classe pour y mettre cet élève
 * @param {function} done
 */
function recreateMissingClasses (done) {
  const Groupe = lassi.service('Groupe')
  const Structure = lassi.service('Structure')
  const Utilisateur = lassi.service('Utilisateur')

  const result = {
    nbStructures: 0,
    nbClassesCrees: 0,
    nbElevesDeplaces: 0
  }

  const { structure } = minimist(process.argv.slice(2))

  const onStructure = (structure, cb) => {
    console.log(`Analyse structure ${structure.oid}`)
    result.nbStructures++
    // on cherche les classes de la structure, puis les utilisateurs dans aucune classe
    Groupe
      .match('structure').equals(structure.oid)
      .match('isClass').equals(true)
      .grab((error, groupes) => {
        if (error) return cb(error)
        // les élèves orphelins (faudra les changer de classe)
        const orphanUserByOrphanClassOid = {}
        // les classes qu'on va créer (elles ont pas encore d'oid)
        const newClassesByOrphanClassOid = {}

        const onOrphanEleve = (user, nextEleve) => {
          if (!user.classe) {
            console.error(`user ${user.oid} sans classe`)
            return nextEleve()
          }
          if (newClassesByOrphanClassOid[user.classe]) {
            // on a déjà créé sa classe, on l'ajoute dedans
            newClassesByOrphanClassOid[user.classe].utilisateurs.push(user.oid)
            // et on garde une ref sur ce user pour le mettre à jour ensuite
            orphanUserByOrphanClassOid[user.classe].push(user)
            return nextEleve()
          }
          // il faut créer la classe
          console.log(`La classe ${user.classe} n’existe pas dans la structure ${structure.oid}`)
          const num = Object.keys(newClassesByOrphanClassOid).length + 1
          newClassesByOrphanClassOid[user.classe] = Groupe.create({
            nom: `classe${num}`,
            isClass: true,
            structure: structure.oid,
            utilisateurs: [user.oid]
          })
          orphanUserByOrphanClassOid[user.classe] = [user]
          nextEleve()
        }

        // à la fin faut sauvergarder les classes créées et les élèves qui ont changé de classe
        const storeChanges = () => {
          const orphanClassOids = Object.keys(newClassesByOrphanClassOid)
          if (!orphanClassOids.length) return cb()
          /* pour debug * /
          console.log(`Dans la structure ${structure.oid} on a ${orphanClassOids.length} classes disparues`)
          for (const oid of orphanClassOids) {
            console.log(`classe ${oid} disparue qui contient ${orphanUserByOrphanClassOid[oid].length} élèves : ${orphanUserByOrphanClassOid[oid].map(u => u.oid).join(' ')}`)
          }
          return cb()
          /* */
          const oidToDelete = orphanClassOids[0]
          const classe = newClassesByOrphanClassOid[oidToDelete]
          classe.store((error, newClasse) => {
            if (error) return cb(error)
            result.nbClassesCrees++
            // nouvelle classe créée, reste à mettre à jour tous ces orphelins
            flow(orphanUserByOrphanClassOid[oidToDelete])
              .seqEach(function (eleve) {
                eleve.classe = newClasse.oid
                result.nbElevesDeplaces++
                eleve.store(this)
              })
              .seq(function () {
                console.log(`Structure ${structure.oid}, classe ${newClasse.oid} (${newClasse.nom}) créée en remplacement de ${oidToDelete} qui n’existait pas, on a déplacé dedans ${orphanUserByOrphanClassOid[oidToDelete].length} élèves : ${orphanUserByOrphanClassOid[oidToDelete].map(u => u.oid).join(' ')}`)
                // classe et users ok, on les vire du pending
                delete newClassesByOrphanClassOid[oidToDelete]
                delete orphanUserByOrphanClassOid[oidToDelete]
                // et on passe au suivant
                storeChanges()
              })
              .catch(cb)
          })
        }

        // les classes existantes
        const existingClassesOids = groupes.map(g => g.oid)
        // on cherche les utilisateurs de cette structure dans aucune classe existante
        const query = Utilisateur
          .match('type').equals(TYPE_ELEVE)
          .match('structures').equals(structure.oid)
        if (existingClassesOids.length) {
          query.match('classe').notIn(existingClassesOids)
        }
        query.forEachEntity(onOrphanEleve, (error) => {
          // on a passé en revue tous les orphelins
          if (error) return cb(error)
          storeChanges()
        })
      })
  }

  const afterAll = (error) => {
    if (error) return done(error)
    done(null, result)
  }

  const query = structure
    ? Structure.match('oid').equals(structure)
    : Structure.match()
  query.forEachEntity(onStructure, afterAll)
}
recreateMissingClasses.help = () => console.log('La commande recreateMissingClasses recrée des classes manquantes pour les élèves orphelins.\nElle peut prendre une option --structure')

module.exports = recreateMissingClasses
