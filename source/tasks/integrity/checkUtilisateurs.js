const { pick } = require('sesalab-commun/utils')

const { TYPE_ELEVE, TYPE_FORMATEUR } = require('sesalab-commun/constants.js')
const appLog = require('sesalab-api/source/tools/appLog')

/**
 * Vérifie la cohérence de la table des utilisateurs
 * - doublons de login
 * - doublons externalMech
 * @param {function} done
 */
function checkUtilisateurs (done) {
  const resultats = {
    erreursUtilisateurs: 0,
    total: 0
  }

  const duplicateHash = {}

  const addUserToDuplicateHash = (duplicateKey, user) => {
    if (!duplicateHash[duplicateKey]) duplicateHash[duplicateKey] = []
    duplicateHash[duplicateKey].push(pick(user, ['oid', 'type']))
  }

  const onUser = (user, cb) => {
    if (user.externalMech) {
      // external unique
      addUserToDuplicateHash(`externalMech:${user.externalMech}--externalId:${user.externalId}`, user)
    }
    if (user.type === TYPE_ELEVE) {
      // login unique par structure
      addUserToDuplicateHash(`login-eleve:${user.login}--structure:${user.structures[0]}`, user)
      if (!user.classe) {
        appLog.error(`checkUtilisateur a détecté une anomalie, élève ${user.oid} sans classe`)
      }
    }
    if (user.type === TYPE_FORMATEUR) {
      // login unique globalement
      addUserToDuplicateHash(`login-prof:${user.login}`, user) // unique globalement
      if (user.classe) {
        appLog.error(`checkUtilisateur a détecté une anomalie, formateur ${user.oid} avec classe ${user.classe}`)
      }
    }

    cb()
  }

  const typeToText = (type) => type === TYPE_ELEVE ? 'élève' : 'formateur'
  const logAnomalieDoublons = (text, users) => {
    resultats.erreursUtilisateurs++
    resultats.total++
    console.log(text + ' - ' + users.map(u => `${typeToText(u.type)}#${u.oid}`).join(', '))
  }

  const afterAll = (error) => {
    if (error) return done(error)
    console.log('============ Doublons utilisateurs ============')
    const duplicates = {}
    for (const [key, users] of Object.entries(duplicateHash)) {
      if (users.length > 1) duplicates[key] = users
    }
    // on veut trier par clé
    for (const key of Object.keys(duplicates).sort()) {
      logAnomalieDoublons(`Doublons ${key}`, duplicates[key])
    }

    done(null, resultats)
  }
  // On ne vérifie pas les entités soft-deleted car les contrôles de cohérences ne se font
  // que sur les entitées non-supprimées (et donc à la restauration des entités supprimées)
  lassi.service('Utilisateur').match().forEachEntity(onUser, afterAll)
}
checkUtilisateurs.help = () => console.log('La commande checkUtilisateurs ne prend pas d’arguments.\nElle liste les anomalies trouvées sur les utilisateurs')

module.exports = checkUtilisateurs
