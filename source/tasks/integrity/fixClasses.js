'use strict'

const _ = require('lodash')
const flow = require('an-flow')
const minimist = require('minimist')
const constants = require('sesalab-commun/constants.js')

function fixClasses (done) {
  const { dryRun, verbose } = minimist(process.argv.slice(2))

  const log = (msg) => {
    if (verbose) console.log(msg)
  }
  const warning = (msg) => console.log(`WARNING - ${msg}`)

  const updateStructure = (structure, cb) => {
    const structureOid = structure.oid
    // pas un warning mais on le veut en console
    log(`Mise à jour des élèves/classes de la struture ${structureOid}`, true)
    // classe oid => classe
    const classesByOid = {}
    const existingElevesByOid = {}

    // Pour chaque élève on enregistre son unique classe.
    // eleve oid => classe
    // Règle de priorité pour assigner la classe à un élève:
    //   1. eleve.classe et la classe est non supprimée
    //   2. première classe non supprimée qui contient l'élève
    //   3. eleve.classe et la classe est supprimée
    //   4. première classe supprimée qui contient l'élève
    //   5. sinon, pas de classe
    //
    // 2 et 4 sont implémentées par addElevesFromClasse() quand on parcourt les classes
    // 1, 3 et 5 par getEleveClasse() quand on analyse un élève
    const classesByEleveOid = {}
    const addElevesFromClasse = (classe) => {
      _.forEach(classe.utilisateurs, (oid) => {
        if (!classesByEleveOid[oid]) {
          classesByEleveOid[oid] = classe
          return // pas de return false car on veut continuer la boucle pour d'éventuels doublons
        }
        // warning qu'on veut seulement dans le console.log
        warning(`l'élève ${oid} est référencé dans deux classes (on garde de préférence la classe non-supprimée, ou à défaut la première créée) :
- classe ${classesByEleveOid[oid].oid}, deleted: ${classesByEleveOid[oid].isDeleted()}
- classe ${classe.oid}, deleted: ${classe.isDeleted()}`)
        if (classesByEleveOid[oid].isDeleted() && !classe.isDeleted()) {
          classesByEleveOid[oid] = classe
        }
      })
    }

    const getEleveClasse = (eleve) => {
      const classe = eleve.classe && classesByOid[eleve.classe]

      if (!classe) {
        log(`l'élève ${eleve.oid} n'a pas de classe ou référence une classe qui n'existe plus.`)
        if (classesByEleveOid[eleve.oid]) {
          warning(`élève ${eleve.oid} sans classe mais référencé dans classe ${classesByEleveOid[eleve.oid].oid} => on lui assigne`)
        }
        return classesByEleveOid[eleve.oid]
      }

      if (!classesByEleveOid[eleve.oid] || classe.oid === classesByEleveOid[eleve.oid].oid) {
        // on n'avait pas encore trouvé la classe de l'élève, ou la classe trouvée correspond à celle de l'élève.
        return classe
      }

      warning(`
l'élève ${eleve.oid} référence une classe différente de celle dans laquelle il est référencé :
- classe de l'élève ${classe.oid} deleted: ${classe.isDeleted()}
- classe où il est référencé:  ${classesByEleveOid[eleve.oid].oid} deleted: ${classesByEleveOid[eleve.oid].isDeleted()}
On prend de préférence la classe de l'élève (sauf si elle est supprimée)
      `)

      // Si la classe de l'élève n'est pas supprimée, on la prend en priorité
      if (!classe.isDeleted()) return classe
      // Si les 2 sont supprimées on prend celle de l'élève en priorité
      if (classe.isDeleted() && classesByEleveOid[eleve.oid].isDeleted()) return classe

      // Sinon on prend la classe qu'on avait trouvé
      return classesByEleveOid[eleve.oid]
    }

    flow()
      // traitement des classes de l'établissement pour remplir classesByOid et classesByEleveOid
      .seq(function () {
        const onClasse = (classe, cb) => {
          classesByOid[classe.oid] = classe
          addElevesFromClasse(classe)
          cb()
        }

        lassi.service('Groupe')
          .match('structure').equals(structureOid)
          .match('isClass').equals(true)
          .sort('oid', 'asc')
          .includeDeleted().forEachEntity(onClasse, this)
      })

      // traitement des élèves de l'établissement
      .seq(function () {
        const onEleve = (eleve, cb) => {
          existingElevesByOid[eleve.oid] = true

          const classe = getEleveClasse(eleve)
          // On enregistre la classe dans laquelle on va finalement mettre l'élève
          classesByEleveOid[eleve.oid] = classe
          const classeOid = classe && classe.oid

          if (eleve.classe !== classeOid) {
            eleve.classe = classeOid
            // Hack - on évite de déclencher le beforeStore checkChangeClasse
            // TODO: idéalement on devrait pouvoir faire .store({skipBeforeStore: true}) mais
            //       ce ne sera pas implémenté dans cette PR
            eleve.$before.classe = eleve.classe
            // fin hack
            log(`--- updating élève Utilisateur ${eleve.oid} classe ${eleve.classe}`)
            if (dryRun) return this(null, eleve)
            eleve.store(cb)
          } else {
            cb()
          }
        }

        lassi.service('Utilisateur')
          .match('structures').equals(structureOid)
          .match('type').equals(constants.TYPE_ELEVE)
          .includeDeleted().forEachEntity(onEleve, this)
      })

      // Maintenant on met à jour les utilisateurs des classes à partir de classesByEleveOid
      // que l'on "inverse"
      .seq(function () {
        const eleveOidsByClasseOid = {}
        _.forEach(classesByEleveOid, (classe, eleveOid) => {
          if (!classe) return // eleve sans classe
          if (!eleveOidsByClasseOid[classe.oid]) { eleveOidsByClasseOid[classe.oid] = [] }
          if (existingElevesByOid[eleveOid]) {
            eleveOidsByClasseOid[classe.oid].push(eleveOid)
          } else {
            warning(`la classe "${classe.nom}" ${classe.oid} référence un élève inexistant dans cette structure : ${eleveOid}`)
          }
        })

        flow(_.keys(classesByOid))
          .seqEach(function (classeOid) {
            const classe = classesByOid[classeOid]
            const classeUtilisateurs = eleveOidsByClasseOid[classeOid] || []

            // On ne met à jour la classe que si son attribut 'utilisateurs' a changé
            if (_.xor(classeUtilisateurs, classe.utilisateurs).length) {
              classe.utilisateurs = classeUtilisateurs
              log(`--- updating Groupe ${classeOid} with utilisateurs [${classe.utilisateurs.join(', ')}]`)
              if (dryRun) return this(null, classe)
              classe.store(this)
            } else {
              this()
            }
          })
          .done(cb)
      })
      .catch(cb)
  }
  lassi.service('Structure').match().forEachEntity(updateStructure, done)
}

fixClasses.help = function displayHelp () {
  console.log(
    `
La commande fixClasses fait en sorte que chaque élève n'appartienne qu'à une unique classe
Options :
  --dryRun : signale tout ce qui devrait être fait sans le faire
  --verbose : détaille toutes les modifications appliquées. Sans verbose on n'affiche que les anomalies corrigées.
`
  )
}
module.exports = fixClasses
