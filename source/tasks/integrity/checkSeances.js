'use strict'

const flow = require('an-flow')
const minimist = require('minimist')

const logIfError = (error) => {
  if (error) console.error(error)
}
/**
 * Vérifie la cohérence des séances par rapport aux résultat
 *
 * @param {function} done
 */
function checkSeances (done) {
  const { dryRun, maxDuplicate, maxMissing, maxWrong, maxSeq, sequence, verbose } = minimist(process.argv.slice(2))
  console.info('============ Intégrité des séances (vs résultats) ============')
  if (sequence) console.info(`limité à la séquence ${sequence}`)
  const Seance = lassi.service('Seance')
  const Sequence = lassi.service('Sequence')
  const Resultat = lassi.service('Resultat')
  const $resultat = lassi.service('$resultat')

  const stats = {
    nbWrongSeances: 0,
    nbMissingSeances: 0,
    nbDuplicateSeances: 0,
    totSeances: 0,
    totSequences: 0
  }

  const getEmptyState = (rid) => ({
    rid,
    nbEssais: 0,
    pourcentageSucces: 0,
    fin: false
  })

  // sauvegarde une seance en tâche de fond
  const createSeance = (sequence, eleve, ressources) => {
    stats.nbMissingSeances++
    if (maxMissing > 0 && stats.nbMissingSeances > maxMissing) {
      console.info(`max de séance manquante ${maxMissing} atteint, ABANDON`)
      return done(null, stats)
    }
    Seance.create({ sequence, eleve, ressources }).store(logIfError)
  }

  const checkSequence = (sequenceOid, next) => {
    // On parse les Séances de cette séquence (max une par élève à priori)
    let lastEleveOid, lastSeanceOid
    const elevesHavingSeance = []

    flow().seq(function () {
      Seance
        .match('sequence').equals(sequenceOid)
        .sort('eleve')
        .grab(this)
    }).seqEach(function (seance) {
      const nextSeance = this
      const { eleve: eleveOid, oid: seanceOid } = seance
      const logPrefix = `séquence ${sequenceOid} séance ${seanceOid} élève ${eleveOid}`

      if (eleveOid === lastEleveOid) {
        console.warn(`${logPrefix} séance en doublon de ${lastSeanceOid}`)
        stats.nbDuplicateSeances++
        if (maxDuplicate > 0 && stats.nbDuplicateSeances > maxDuplicate) {
          console.info(`max de séance en double ${maxDuplicate} atteint, ABANDON`)
          return done(null, stats)
        }
        if (dryRun) return this()
        // on peut la virer sans appeler $seance.duplicate car on a déjà reconstruit proprement la précédente
        return seance.delete(this)
      }
      // pas un doublon, on continue
      elevesHavingSeance.push(eleveOid)
      lastEleveOid = eleveOid
      lastSeanceOid = seanceOid

      flow().seq(function () {
        $resultat.buildSeanceIndivData(sequenceOid, eleveOid, this)
      }).seq(function (seanceData) {
        // on a fini pour les résultats de cette séquence et cet élève,
        // on compare seance.ressources à ce qu'on a récupéré
        /** Les messages d'erreurs */
        const msgs = []
        // on parcours les ressources présentes
        for (const r of seance.ressources) {
          const state = seanceData.ressources.find(state => state.rid === r.rid)
          if (state) {
            if (state.nbEssais !== r.nbEssais) {
              msgs.push(`${r.rid} : nbEssais ${r.nbEssais} => ${state.nbEssais}`)
              r.nbEssais = state.nbEssais
            }
            if (state.pourcentageSucces !== r.pourcentageSucces) {
              msgs.push(`${r.rid} : pourcentageSucces ${r.pourcentageSucces} => ${state.pourcentageSucces}`)
              r.pourcentageSucces = state.pourcentageSucces
            }
            if (state.fin !== r.fin) {
              msgs.push(`${r.rid} : fin ${r.fin} => ${state.fin}`)
              r.fin = state.fin
            }
          } else {
            // pas de résultat sur cet exo, si on a qq chose dans la séance
            // on vérifie que c'est les valeurs par défaut
            if (r.nbEssais !== 0) {
              msgs.push(`${r.rid} : nbEssais ${r.nbEssais} => 0`)
              r.nbEssais = 0
            }
            if (r.pourcentageSucces) {
              msgs.push(`${r.rid} : pourcentageSucces ${r.pourcentageSucces} => 0`)
              r.pourcentageSucces = 0
            }
            if (r.fin) {
              msgs.push(`${r.rid} : fin ${r.fin} => false`)
              r.fin = false
            }
          }
        }

        // et on vérifie qu'on a pas de resultat sur des ressources absentes de la séance
        for (const buildRessData of seanceData.ressources) {
          const { rid } = buildRessData
          if (!seance.ressources.some(r => r.rid === rid)) {
            // ça manque
            msgs.push(`ress ${rid} absente de la séance : ${JSON.stringify(buildRessData)}`)
            seance.ressources.push(buildRessData)
          }
        }

        stats.totSeances++
        const hasPb = msgs.length > 0
        if (hasPb) {
          stats.nbWrongSeances++
          if (maxWrong > 0 && stats.nbWrongSeances > maxWrong) {
            console.info(`max d’anomalies ${maxWrong} atteint, ABANDON`)
            return done(null, stats)
          }
          let msg = logPrefix
          if (verbose) msg += ` :\n  ${seanceOid}  ${msgs.join(`\n  ${seanceOid}  `)}`
          else msg += ` ${msgs.length} erreur${msgs.length > 1 ? 's' : ''}`
          console.info(msg)
        }
        if (dryRun) return nextSeance()
        // pour pas garder en ram toutes les séances on évite seance.store(nextSeance)
        seance.store((error) => nextSeance(error))
      }).catch(nextSeance)

    // toutes les séances existantes pour cette séquence ont été parsées, on regarde s'il en manque
    // (des résultats pour des élèves qui n'ont pas de séance)
    }).seq(function () {
      stats.totSequences++
      lastEleveOid = ''
      let statesByRid = {}
      // on cherche tous les résultats des élèves qui n'ont pas de séance

      const onResultat = ({ participants, ressource, score, fin }, next) => {
        // on ne traite que les résultats fait seul
        if (participants.length > 1) return this()
        const eleveOid = participants[0]
        const { rid } = ressource
        if (lastEleveOid && lastEleveOid !== eleveOid) {
          // on sauvegarde la seance de l'élève précédent en tâche de fond
          createSeance(sequenceOid, eleveOid, Object.values(statesByRid))
          // et on reset pour cet élève
          lastEleveOid = eleveOid
          statesByRid = {}
        }
        // on ajoute ce résultat à statesByRid
        if (!statesByRid[rid]) statesByRid[rid] = getEmptyState(rid)
        const s = statesByRid[rid]
        s.nbEssais++
        if (fin) {
          s.fin = true
          if (score * 100 > s.pourcentageSucces) s.pourcentageSucces = Math.round(score * 100)
        }
        // et on passe au résultat suivant
        next()
      }

      const query = Resultat.match('sequence').equals(sequenceOid)
      if (elevesHavingSeance.length) query.match('eleve').notIn(elevesHavingSeance)
      query.sort('eleve').forEachEntity(onResultat, (error) => {
        if (error) return next(error)
        // faut sauvegarder le dernier
        if (lastEleveOid) createSeance(sequenceOid, lastEleveOid, Object.values(statesByRid))
        // c'est tout pour cette séquence
        next()
      })

    // fini
    }).catch(next)
  } // checkSequence

  if (verbose) console.info('## CheckSeances, sur chaque ligne\n## sequence seance eleve\n##    puis\n##  seance rid pb')
  // si on veut juste pour une séquence
  if (sequence) return checkSequence(sequence, (error) => done(error, stats))
  // sinon on les fait toutes
  const limit = 100
  const parseSequences = (skip = 0) => {
    let nb = 0
    flow().seq(function () {
      Sequence.match().grab({ limit, skip }, this)
    }).seqEach(function ({ oid }) {
      nb++
      if (maxSeq > 0 && nb > maxSeq) {
        console.info(`max de séquences ${maxSeq} atteint, ABANDON`)
        return done(null, stats)
      }
      checkSequence(oid, this)
    }).seq(function () {
      if (nb < limit) return done(null, stats)
      process.nextTick(parseSequences, skip + limit)
    }).catch(done)
  }
  parseSequences()
}

checkSeances.help = () => {
  console.info(
    `
La commande checkSeances vérifie que les séances sont cohérentes par rapport aux résultats sauvegardés
Options :
  --dryRun : signale les incohérences sans rien corriger
  --maxDuplicate : s'arrêter après ce nb de séances en doublon
  --maxMissing : s'arrêter après ce nb de séances manquantes
  --maxSeq : s'arrêter après ce nb de séquences analysées
  --maxWrong : s'arrêter après ce nb d'anomalie
  --sequence=xxx : pour ne traiter que cette séquence
  --verbose : détaille les incohérences (sinon on a juste la liste des séances).
`
  )
}

module.exports = checkSeances
