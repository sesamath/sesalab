/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesalab (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de Sesalab, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

const flow = require('an-flow')
const checkGroupes = require('./checkGroupes')
const checkResultats = require('./checkResultats')
const checkSeances = require('./checkSeances')
const checkUtilisateurs = require('./checkUtilisateurs')
const checkUtilisateursNotValid = require('./checkUtilisateursNotValid')

/**
 * Liste les problèmes d'intégrité de données :
 *
 * Utilisateurs:
 *  - Liste les élèves en doublons de login au sein d'une même structure
 *  - Liste les pfos en doublons de login (global)
 *  - Liste les élèves / profs ayant le même login (peut poser des problèmes au login)
 *
 * Groupes:
 *  - Liste les groupes incohérent (classe sans structure, classe avec owner, groupe perso avec une structure)
 *  - Liste les classes ayant des élèves en communs avec une autre classe
 *
 * Note: certaines des vérifications sont redondantes avec les scripts que l'on trouve dans
 *       findInBd/duplicate/utils, mais la méthode ici est différente (ce qui permettra éventuellement
 *       de trouver des bugs dans l'un ou l'autre)
 *
 * @param {string}        password
 * @param {errorCallback} done
 */
function integrityCheck (done) {
  flow().seq(function () {
    checkUtilisateurs(this)
  }).seq(function (resultats) {
    console.log('checkUtilisateurs retourne', resultats)
    checkUtilisateursNotValid(this)
  }).seq(function (resultats) {
    console.log('checkUtilisateursNotValid retourne', resultats)
    checkGroupes(this)
  }).seq(function (resultats) {
    console.log('checkGroupes retourne', resultats)
    checkSeances(this)
  }).seq(function (resultats) {
    console.log('checkSeances retourne', resultats)
    checkResultats(this)
  }).seq(function (resultats) {
    console.log('checkResultats retourne', resultats)
    done()
  }).catch(done)
}

integrityCheck.help = function displayHelp () {
  console.log(
    'La commande integrityCheck liste les problèmes d’intégrité de données'
  )
}

module.exports = integrityCheck
