'use strict'

const flow = require('an-flow')
const minimist = require('minimist')
const { createIndexFromEleves } = require('sesalab-api/source/resultat/helpers.js')

let Resultat
let Utilisateur
let $resultat

let dryRun
let slowCoef = 2
let verbose

const stats = {
  nbElevesDisparus: 0,
  nbParticipantsOk: 0,
  nbResultatsOrphelins: 0,
  total: 0
}

/**
 * Init dryRun & verbose + services lassi
 * @private
 * @return {{dryRun: boolean, verbose: boolean}}
 */
function init (options) {
  // on pourrait mettre un flag pour empêcher deux appels cli en //
  // mais si l'admin est trop bête pour le faire tant pis pour lui (ça plantera pas mais mongo va ramer)
  Resultat = lassi.service('Resultat')
  $resultat = lassi.service('$resultat')
  Utilisateur = lassi.service('Utilisateur')

  if (!options) options = minimist(process.argv.slice(2))
  dryRun = Boolean(options.dryRun)
  if (options.slow) slowCoef = 20
  verbose = Boolean(options.verbose)
  if (verbose) console.log(`Lancement de checkResultats avec les options\ndryRun ${dryRun}\nslow ${options.slow ? 'oui' : 'non'}\nverbose ${verbose}`)
}

/**
 * @callback getElevesListsCallback
 * @param {Error} [error]
 * @param {string[]} La liste d'index (chacun étant un eleveList avec séparateur virgule)
 */
/**
 * Récupère l'index "eleves" (participants concaténés) qui suit lastElevesList
 * (version avec aggregate dans le commit 610c65cd, trop lourd pour mongo avec des millions de résultats)
 * @private
 * @param {string} lastElevesList
 * @param {getElevesListsCallback} cb
 */
function fetchNextElevesList (lastElevesList, cb) {
  const query = Resultat.match('eleves')
  if (lastElevesList) query.greaterThan(lastElevesList)
  query.grabOne((error, resultat) => {
    if (error) return cb(error)
    if (resultat) return cb(null, createIndexFromEleves(resultat.participants))
    cb()
  })
}

/**
 * Check une propriété "eleves" (en déduit la liste des participants,
 * regarde s'ils existent toujours et supprime les résultats concernés si besoin)
 * @private
 * @param {string} eleveList
 * @param cb
 * @return {*}
 */
function checkElevesList (eleveList, cb) {
  // check string non vide
  if (typeof eleveList !== 'string') return cb(Error(`Propriété eleves invalide (type ${typeof eleveList})`))
  if (!eleveList) return cb(Error('Propriété eleves vide'))

  stats.total++
  const elevesOids = eleveList.split(',')
  const logMessages = []
  flow().seq(function () {
    // on récupère les élèves
    Utilisateur.match('oid').in(elevesOids).grab(this)
  }).seq(function (eleves) {
    // Ce seq purge les résultats à plusieurs, le suivant les résultats individuels

    // On regarde s'il manque des élèves
    if (eleves.length === elevesOids.length) {
      // tous là
      stats.nbParticipantsOk++
      return this(null, [])
    }

    if (eleves.length) {
      // un ou des élèves ont disparu, mais pas tous
      const existing = eleves.map(e => e.oid)
      const missing = elevesOids.filter(oid => !existing.includes(oid))
      return this(null, missing)
    }

    // tous les élèves ont disparus, mais si c'est uniquement des résultats individuels c'est géré à l'étape suivante
    if (elevesOids.length === 1) return this(null, elevesOids)

    // faut purger les résultats à plusieurs de ces élèves
    const nextStep = this
    const method = dryRun ? 'count' : 'purge'
    Resultat.match('eleves').equals(eleveList)[method]((error, nbDeleted) => {
      if (error) return cb(error)
      stats.nbResultatsOrphelins += nbDeleted
      if (verbose) console.log(`${nbDeleted} résultats effacés pour les participants ${eleveList}`)
      // avec effacement des résultats individuels au seq suivant
      nextStep(null, elevesOids)
    })

    // on peut passer aux résultats individuels
  }).seqEach(function (eleveOid) {
    if (verbose) logMessages.push(`Pour l’élève ${eleveOid} qui n’est plus en base on supprime`)
    if (dryRun) Resultat.match('eleves').equals(eleveOid).count(this)
    else $resultat.purgeEleve(eleveOid, this)
  }).seq(function (purgeResults) {
    if (purgeResults && purgeResults.length) {
      purgeResults.forEach((nbDeleted, index) => {
        if (!nbDeleted) return // élève encore présent dans des résultats à plusieurs mais qui n'en a plus en individuel, normal que ça arrive
        stats.nbElevesDisparus++
        stats.nbResultatsOrphelins += nbDeleted
        if (verbose) console.log(logMessages[index] + ` ${nbDeleted} résultats`)
      })
    }
    cb(null, eleveList)
  }).catch(cb)
}

function nextBatch (lastElevesList, cb) {
  const nbDeleted = stats.nbResultatsOrphelins
  flow().seq(function () {
    fetchNextElevesList(lastElevesList, this)
  }).seq(function (elevesList) {
    if (!elevesList) return cb() // y'avait plus de suivant
    checkElevesList(elevesList, this)
  }).seq(function (elevesList) {
    if (verbose && stats.total % 1000 === 0) console.log(`${stats.total} participants traités`)
    // on laisse un peu de temps à mongo pour refroidir (flush des journaux,
    // traitement de toutes les autres tâche en attente, etc.),
    // 2 ou 20ms par résultat effacé pendant ce batch (10 ou 200 si aucun)
    const waitMs = (10 + stats.nbResultatsOrphelins - nbDeleted) * slowCoef
    setTimeout(() => nextBatch(elevesList, cb), waitMs)
  }).catch(cb)
}

/**
 * Vérifie la cohérence des résultats (vire tous ceux dont les élèves ont disparu)
 * @param {Object} [options] Si non fourni on regarde argv (ajouter alors -- devant chaque option)
 * @param {boolean} [options.dryRun]
 * @param {boolean} [options.verbose]
 * @param {function} done
 */
function checkResultats (options, done) {
  if (Resultat) return done(Error('checkResultats must be called only once'))
  if (typeof options === 'function') {
    done = options
    init()
  } else {
    init(options)
  }

  nextBatch('', (error) => {
    if (error) done(error)
    done(null, stats)
  })
}

checkResultats.help = () => console.log(`La commande checkResultats liste les anomalies trouvées sur les résultats
et supprime tous les orphelins (dont l’élève ou les élèves n’existent plus)
Options :
  --dryRun : signale toutes les suppressions sans les faire
  --slow : ajouter une pause avant de passer au groupe de participants suivant
  --verbose : être plus bavard (signaler chaque oid élève orphelin)
`)

module.exports = checkResultats
