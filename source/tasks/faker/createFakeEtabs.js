/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesalab (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de Sesalab, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

if (typeof app === 'undefined') throw new Error('Ce module ne doit être requis qu’après instanciation de lassi avec cli')

const flow = require('an-flow')
const moment = require('moment')
const constants = require('sesalab-commun/constants.js')

/**
 * Crée des établissements
 *
 * @param {integer}  nbStructures
 * @param {string}   mailFormateur
 * @param {Date} jourPurge
 * @param {Callback} done
 */
function createFakeEtab (nbStructures, mailFormateur, jourPurge, done) {
  // Vérification des arguments
  if (typeof nbStructures !== 'number') throw new Error('Il faut indiquer le nombre d’établissements en premier argument')
  if (typeof mailFormateur !== 'string') throw new Error('Il faut indiquer le mail du formateur en deuxième argument')
  if (typeof jourPurge === 'string') {
    jourPurge = new Date(jourPurge)
  } else if (typeof jourPurge === 'function') {
    done = jourPurge
    jourPurge = moment(new Date()).add(1, 'day').toDate()
  } else {
    throw new Error('Le troisième argument est facultatif mais doit être une date s’il est fourni')
  }

  const Utilisateur = lassi.service('Utilisateur')
  const Structure = lassi.service('Structure')
  const indicesStructures = []
  for (let indiceStructure = 0; indiceStructure < nbStructures; indiceStructure++) indicesStructures.push(indiceStructure)
  let emptyIsDone = false
  let totalEleves = 0
  let structuresIds = []
  const formateurLogin = 'fakeFormateur'

  flow(indicesStructures).seqEach(function (indiceStructure) {
    const nextStep = this
    // création des N structures
    Structure.create({
      datePurge: jourPurge,
      nom: `fake n° ${indiceStructure}`
    }).store(function (error, structure) {
      if (error) return nextStep(error)
      nextStep(null, structure.oid)
    })
  })
    .seq(function (_structuresIds) {
      structuresIds = _structuresIds
      Utilisateur
        .match('login').equals(formateurLogin)
        .grabOne(this)
    })
    .seq(function (utilisateur) {
      const nextStep = this
      if (utilisateur) {
        utilisateur.structures = structuresIds
        utilisateur.store(function (error) {
          if (error) return nextStep(error)
          nextStep(null, structuresIds)
        })
        return
      }
      // Création du formateur à qui on affecte tous les établissements
      Utilisateur.create({
        nom: 'UserFormateur',
        prenom: 'Fake',
        mail: mailFormateur,
        type: constants.TYPE_FORMATEUR,
        login: formateurLogin,
        blocked: false,
        structures: structuresIds
      }).store(function (error) {
        if (error) return nextStep(error)
        nextStep(null, structuresIds)
      })
    }).seqEach(function (structureId) {
      const nextStructure = this
      // Une structure sans élève, les autres avec 0 à 2500 élèves
      const nbEleves = emptyIsDone ? Math.floor(Math.random() * 2500) : 0
      if (!emptyIsDone) emptyIsDone = true
      // Création des élèves
      const indicesEleves = []
      for (let indiceEleve = 0; indiceEleve < nbEleves; indiceEleve++) indicesEleves.push(indiceEleve)
      flow(indicesEleves).seqEach(function (indiceEleve) {
        const nextEleve = this
        Utilisateur.create({
          nom: `UserEleve${indiceEleve}`,
          prenom: 'Fake',
          type: constants.TYPE_ELEVE,
          login: `fakeEleve${indiceEleve}`,
          blocked: Math.random() > 0.8, // 20% true
          structures: [structureId]
        }).store(function (error) {
        // Pas la peine de garder tous les élèves dans la pile du flow (si on avait passé nextEleve directement à store)
          if (error) nextEleve(error)
          totalEleves++
          nextEleve()
        })
      }).done(nextStructure)
    }).seq(function () {
      done(null, `Création OK de ${nbStructures} structures, toutes avec une purge au ${jourPurge},
      avec le formateur ${mailFormateur} avec ${totalEleves} élèves au total`)
    }).catch(done)
}

createFakeEtab.help = function help () {
  console.log(`Créé des établissements avec les arguments
- nb d’établissements
- mail du formateur qui sera créé et seul formateur de ces établissements
- (facultatif) date de purge (YYYY-mm-dd), par défaut le lendemain
  `)
}

module.exports = createFakeEtab
