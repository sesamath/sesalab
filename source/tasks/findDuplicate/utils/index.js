const flow = require('an-flow')
const _ = require('lodash')
const minimist = require('minimist')
const constants = require('sesalab-commun/constants.js')

/**
 * @callback doublonCallbackCallback
 * @param {Error} [error]
 * @param {number} [nbDeleted] Passer le nb d'entités supprimées s'il y en a
 */
/**
 * @callback doublonCallback
 * @param {string} login
 * @param {string[]} oids
 * @param {doublonCallbackCallback} cb à rappeler quand la gestion du doublon est terminée
 */
/**
 * Parse tous les élèves pour trouver les doublons pour login, et applique doublonCb à chaque série de doublons
 * @private
 * @param {doublonCallback} doublonCb
 * @param {string} type Type d'utilisateur
 * @param {string} structure L'identifiant de la structure
 * @param {simpleCallback} done
 */
function doublonsByLogin (doublonCb, type, structure, done) {
  /**
   * Lance un grab et analyse ce qui sort pour remplir duplicatesLogins
   *
   * @param next
   */
  function parse (next) {
    let lastLogin, lastOid, lastStructureOid
    let currentDuplicates
    // Pour conserver les derniers au tour suivant au cas où il y aurait des doublons à suivre
    let skipTmp = 0
    // ici faut une limite basse pour éviter les
    // Sort operation used more than the maximum 33554432 bytes of RAM. Add an index, or specify a smaller limit.
    const limit = Math.max(maxResults, 100)

    flow()
      .seq(function () {
        if (type === constants.TYPE_ELEVE) {
          getQuery()
            .sort('structure')
            .sort('login')
            .sort('__deletedAt') // Pour obtenir les non deleted en premier
            .grab({ limit, skip }, this)
        }

        if (type === constants.TYPE_FORMATEUR) {
          getQuery()
            .sort('login')
            .grab({ limit, skip }, this)
        }
      })
      .seqEach(function (user) {
        const nextUser = this
        if (user.login === lastLogin) {
          if ((type === constants.TYPE_ELEVE && user.structures && user.structures[0] === lastStructureOid) ||
          (type === constants.TYPE_FORMATEUR)) {
            if (!currentDuplicates) currentDuplicates = [lastOid]
            currentDuplicates.push(user.oid)
            skipTmp++
            if (skipTmp < limit) return nextUser()
          }
        }

        // On gère le cas async doublonCb
        flow()
          .seq(function () {
            if (currentDuplicates) {
              nbDuplicates++
              // Doublons en cours de remplissage, il faut les purger
              doublonCb(lastLogin, currentDuplicates, this)
            } else {
              this()
            }
          })
          .seq(function (nbDeleted) {
            if (currentDuplicates) {
              if (nbDeleted) {
                skip -= nbDeleted
                total -= nbDeleted
              }
              currentDuplicates = undefined
              // reset sauf si on a atteint le max
              if (skipTmp < limit) {
                skip += skipTmp
                skipTmp = 0
              } else {
                // pour compenser le ++ qui suit et qu'il ne faudrait pas faire dans ce cas extrême
                skip--
              }
            }
            skip++
            lastLogin = user.login
            lastOid = user.oid
            lastStructureOid = user.structures && user.structures[0]
            nextUser()
          })
          .catch(nextUser)
      })
      .seq(function () {
        if (skipTmp + skip < total) process.nextTick(parse, next)
        else next(null, currentDuplicates)
      })
      .catch(done)
  }

  const $settings = lassi.service('$settings')
  const maxResults = $settings.get('application.maxResults', 500)
  const Utilisateur = lassi.service('Utilisateur')
  const getQuery = () => {
    if (type === constants.TYPE_FORMATEUR) return Utilisateur.match('type').equals(constants.TYPE_FORMATEUR)
    const query = Utilisateur
      .match('type').equals(constants.TYPE_ELEVE)
      .includeDeleted()
    if (structure) query.match('structures').equals(structure)
    return query
  }
  // On wrappe console.error pour ajouter le préfixe ERROR (en cli on est souvent redirigé dans un fichier)
  const consoleError = console.error
  console.error = function () {
    const args = ['ERROR'].concat(Array.prototype.slice.call(arguments))
    consoleError.apply(console, args)
  }
  // On ne le remet pas en sortant car le mode cli n'exécute qu'une commande à la fois

  let nbDuplicates = 0
  let skip = 0
  let total

  flow()
    .seq(function () {
      getQuery().count(this)
    })
    .seq(function (nb) {
      const userName = type === constants.TYPE_ELEVE ? 'élèves' : 'formateurs'
      console.log(`${nb} ${userName}\n`)
      total = nb
      this()
    })
    .seq(function () {
      parse(this)
    })
    .seq(function (duplicates) {
      console.log(`${nbDuplicates} login en doublon trouvés`)
      done()
    })
    .catch(done)
}

/**
 * Utilise $mergeCli pour virer les doublons
 * @param {string} type Type d'utilisateur
 * @param {Object} options Options pour la commande de fix
 * @param {simpleCallback} done
 */
function fixDoublonsByLogin (type, options, done) {
  /**
   * Des logins vides existaient en base, on les vire sans ménagement
   */
  function purgeEmptyLogin (next) {
    flow()
      .seq(function () {
        Utilisateur
          .match('login').equals('')
          .includeDeleted()
          .purge(this)
      })
      .seq(function (nbDeleted) {
        console.error(`${nbDeleted} utilisateurs avec un login vide supprimés`)
        next(null, nbDeleted)
      })
      .catch(done)
  }

  /**
   * Fixe une série de doublons pour un login donné
   * @private
   * @type doublonCallback
   * @param {string} login
   * @param {string[]} oids
   * @param {doublonCallbackCallback} next
   */
  function fixDoublons (login, oids, next) {
    if (login === '') return purgeEmptyLogin(next)
    if (!login) return done(new Error('login falsy impossible à traiter'))
    if (!oids.length) return next(new Error('fixDoublons sans doublons à fixer'))

    // Il faut parser les oids pour en trouver un qui n'est pas softDeleted (si possible)
    // en choisissant parmi eux celui qui s'est connecté en dernier
    let oidToKeep, oidsToDel, validOids
    flow()
      .seq(function () {
        Utilisateur
          .match('oid').in(oids)
          .grab(this)
      })
      .seq(function (valids) {
      // seulement les oid valides, ou tous s'il n'y en a pas
        validOids = valids.length ? _.map(valids, 'oid') : oids
        UtilisateurAcces
          .match('utilisateur').in(validOids)
          .sort('timestamp', 'desc')
          .grabOne(this)
      })
      .seq(function (ua) {
        if (ua) {
        // On garde le dernier élève connecté
          oidToKeep = ua.utilisateur
        } else {
        // le pop modifie validOids donc potentiellement oids,
        // pas grave le filter qui suit remontera alors tout ce qui reste
          oidToKeep = validOids.pop()
        }
        oidsToDel = oids.filter(o => o !== oidToKeep)
        this(null, oidsToDel)
      })

    // pour chaque élément de oidsToDel on appelle mergeEleve pour le fusionner à celui qu'on garde
      .seqEach(function (oidToDel) {
        const options = {
          dryRun,
          force: !dryRun && force,
          keepLastPassword: true,
          toDelete: oidToDel,
          toKeep: oidToKeep,
          verbose
        }
        if (type === constants.TYPE_ELEVE) {
          mergeEleves(options, (error) => {
          // On ne veut pas s'arrêter si une fusion échoue, on le signale simplement
            if (error) console.error(error.message)
            this()
          })
        }
        if (type === constants.TYPE_FORMATEUR) {
          mergeFormateurs(options, (error) => {
          // On ne veut pas s'arrêter si une fusion échoue, on le signale simplement
            if (error) console.error(error.message)
            this()
          })
        }
      })
      .seq(function () {
        next(null, oids.length)
      })
      .catch(next)
  }

  if (type !== constants.TYPE_ELEVE && type !== constants.TYPE_FORMATEUR) {
    throw new Error(`Aucun utilisateur ne possède le type ${type}`)
  }

  const Utilisateur = lassi.service('Utilisateur')
  const UtilisateurAcces = lassi.service('UtilisateurAcces')
  const $mergeCli = lassi.service('$merge-cli')
  const { mergeEleves, mergeFormateurs } = $mergeCli.commands()
  if (typeof options === 'function') {
    done = options
    options = minimist(process.argv.slice(2))
  }
  const { dryRun, force, structure, verbose } = options
  if (force && !structure) throw new Error('Il faut indiquer une structure pour utiliser --force (trop dangereux sur toute la base)')

  doublonsByLogin(fixDoublons, type, structure, done)
}

function printDoublonsByLogin (type, done) {
  function doublonCb (login, oids, next) {
    console.log(`${login} ${oids.join(' ')}`)
    next()
  }

  if (type !== constants.TYPE_ELEVE && type !== constants.TYPE_FORMATEUR) {
    throw new Error(`Aucun utilisateur ne possède le type ${type}`)
  }

  doublonsByLogin(doublonCb, type, false, done)
}

module.exports = {
  fixDoublonsByLogin,
  printDoublonsByLogin
}
