/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesalab (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de Sesalab, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */

'use strict'

if (typeof app === 'undefined') throw new Error('Ce module ne doit être requis qu’après instanciation de lassi avec cli')

const constants = require('sesalab-commun/constants.js')
const { fixDoublonsByLogin, printDoublonsByLogin } = require('./utils')

/**
 * Utilise $mergeCli pour virer les doublons
 * @param {simpleCallback} done
 */
function fixDoublonsElevesByLogin (done) {
  fixDoublonsByLogin(constants.TYPE_ELEVE, done)
}
fixDoublonsElevesByLogin.help = () => console.log(`
La commande fixDoublonsElevesByLogin permet de fusionner tous les comptes élèves ayant le même login.
Ça conservera le password du dernier login qui s'est connecté
Options :
  --dryRun : signale tout ce qui devrait être fait sans le faire
  --force :  force la fusion sans vérifier les pré-requis (noms, login|external et structure identiques), impose de préciser une structure
  --structure oid : se limite à la structure
  --verbose : être plus bavard
`)

/**
 * Affiche les doublons (login suivi des oids qui l'ont)
 * @param done
 */
function printDoublonsElevesByLogin (done) {
  printDoublonsByLogin(constants.TYPE_ELEVE, done)
}
printDoublonsElevesByLogin.help = () => console.log(`
La commande printDoublonsElevesByLogin permet de lister tous les comptes élèves ayant le même login
avec une ligne par login en doublon sous la forme 'login oid1 oid2 oid3 …'
`)

module.exports = {
  fixDoublonsElevesByLogin,
  printDoublonsElevesByLogin
}
