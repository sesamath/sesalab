'use strict'

const { join, resolve } = require('path')
const fs = require('fs')

const flow = require('an-flow')
const minimist = require('minimist')

const privateDir = resolve(__dirname, '../../../_private')
if (!fs.lstatSync(privateDir).isDirectory()) throw Error('pas trouvé le dossier _private à la racine')
const outputDir = join(privateDir, 'stats')
if (!fs.lstatSync(outputDir, { throwIfNoEntry: false })?.isDirectory()) fs.mkdirSync(outputDir, { mode: 0o755 })

let Resultat, statsFile, verbose
let waitMs = 100
let normalEnding = false

const limit = 1000
/**
 * @typedef StatsResultats
 * @property {number} total Le nb de résultats analysés
 * @property {string} periode Le mois analysé sous la forme YYYY-MM
 * @property {Date} firstDate Date du premier résultat
 * @property {Date} lastDate Date du dernier resultat
 * @property {StatsResultatsForType} ecjs
 * @property {StatsResultatsForTypeJ3p} j3p
 * @property {StatsResultatsForType} qcm
 */
/**
 * @typedef StatsResultatsForType
 * @property {number} total Le nb de résultat pour ce type
 * @property {Object} rids Objet dont les clés sont des rids et les valeurs le nb de résultats de ce rid
 */
/**
 * @typedef StatsResultatsForTypeJ3p
 * @extends StatsResultatsForType
 * @property {Object} sections Objet dont les clés sont des noms de section et les valeurs le nb de résultats pour cette section
 */

const options = minimist(process.argv.slice(2))

/**
 * Init verbose + services lassi
 * @private
 * @return {StatsResultats} L'objet stats initialisé
 */
function init () {
  // option --month obligatoire
  let monthOption = options.month
  if (typeof monthOption === 'number') monthOption = String(monthOption).padStart(2, '0')
  const today = new Date()
  /** @type {number} */
  const currentYear = today.getFullYear()

  /** @type {string} */
  const currentMonth = String(today.getMonth() + 1).padStart(2, '0')
  if (!monthOption) {
    if (today.getDate() === 1) {
      // le 1er on prend le mois précédent
      monthOption = today.getMonth() // n° de mois qui démarre à 0
        ? String(today.getMonth()).padStart(2, '0')
        : `${currentYear - 1}-12`
    } else {
      // sinon le mois courant
      monthOption = currentMonth
    }
  }
  const yearMonthWanted = (/^[0-9]{2}$/.test(monthOption)) ? `${currentYear}-${monthOption}` : monthOption
  const chunks = /^([0-9]{4})-([0-9]{2})$/.exec(yearMonthWanted)
  if (!chunks) throw Error(`Option --month invalide, format mm ou yyyy-mm requis, ${monthOption} fourni`)
  const [, year, month] = chunks
  // check demande plausible
  if (Number(year) > currentYear) throw Error(`Option --month invalide, année dans le futur (${monthOption})`)
  if (Number(year) < currentYear - 2) throw Error(`Option --month invalide, année trop ancienne (${monthOption})`)
  if (Number(month) < 1 || Number(month) > 12) throw Error(`Option --month invalide, mois invalide (${monthOption})`)
  if (yearMonthWanted > `${currentYear}-${currentMonth}`) throw Error(`Option --month invalide, année dans le futur (${monthOption})`)
  // year & month ok
  const periode = `${year}-${month}`
  /**
   * Objet qui cumule les stats
   * @type {StatsResultats}
   */
  const stats = {
    total: 0,
    periode,
    firstDate: null,
    lastDate: null
  }

  // on regarde si on a déjà une compilation en fichier json pour cette période
  statsFile = join(outputDir, `resultats_${periode}.json`)
  if (fs.lstatSync(statsFile, { throwIfNoEntry: false })?.isFile()) {
    const existingStats = require(statsFile)
    if (existingStats.periode !== periode) {
      throw Error(`Le fichier ${statsFile} existe mais son contenu n’est pas cohérent (période ${existingStats.periode})`)
    }
    Object.assign(stats, existingStats)
    // attention, faut repasser les dates à Date ! (ça peut être null en cas de souci à l'appel précédent)
    if (typeof stats.firstDate === 'string') stats.firstDate = new Date(stats.firstDate)
    if (typeof stats.lastDate === 'string') stats.lastDate = new Date(stats.lastDate)
  } else {
    stats.periode = periode
  }

  // --slow
  if (options.waitMs > 10) waitMs = options.waitMs
  else if (options.slow) waitMs = 1000
  // --verbose
  verbose = options.v || Boolean(options.verbose)
  if (verbose) console.log('Lancement de statsResultats en mode verbose')

  Resultat = lassi.service('Resultat')

  // ça peut être très long, en cas d'interruption avant la fin on affiche ce qu'on a déjà compilé
  process.on('exit', (code) => {
    if (!normalEnding) console.warn(`Process interrompu (code ${code}) avec une tâche statsResultats en cours`, JSON.stringify(stats, null, 2))
  })

  return stats
}

function getSection (graphe, idNoeud) {
  for (const node of graphe) {
    if (!Array.isArray(node)) {
      console.error(Error('graphe invalide'), graphe)
      return ''
    }
    if (node[0] === idNoeud) return node[1]
  }
  return ''
}

function addJ3pResultat (stats, oid, contenu) {
  if (!stats.j3p.sections) stats.j3p.sections = {}
  // on veut connaître les sections utilisées
  if (!contenu) return console.error(Error(`Le résultat ${oid} n’a pas de contenu`))
  const { noeuds, graphe } = contenu
  for (const noeud of noeuds) {
    // ce sont les nœuds qui ont donné un résultat, ils ont donc tous une section (pas de nœud fin là dedans)
    const section = getSection(graphe, noeud)
    if (!section) return console.error(Error(`Dans le résultat ${oid} on a un nœud ${noeud} dont on ne retrouve pas la section dans le graphe`))
    if (!stats.j3p.sections[section]) stats.j3p.sections[section] = 0
    stats.j3p.sections[section]++
  }
}

function compileStats (cb) {
  const stats = init()
  const periodeStart = new Date(`${stats.periode}-01T00:00:00.000Z`)
  // pour la fin faut récupérer année-mois et aviser pour prendre le tout début du mois suivant
  const [year, month] = stats.periode.split('-')
  const endPrefix = month < '12'
    ? `${year}-${String(Number(month) + 1).padStart(2, '0')}`
    : `${Number(year) + 1}-01`
  const periodeEnd = new Date(`${endPrefix}-01T00:00:00.000Z`)
  let startDate = periodeStart
  if (stats.lastDate) {
    // faut démarrer 1ms plus tard, sinon le résultat correspondant à ce lastDate va être compté une fois de plus
    startDate = new Date(stats.lastDate.getTime() + 1)
  }

  const nextBatch = (skip) => {
    flow().seq(function () {
      // on écrit sur le disque tous les 50 appels de nextBatch (50k résultats)
      if (skip && skip % (50 * limit) === 0) writeStats(stats)
      Resultat.match('date').between(startDate, periodeEnd).sort('date').grab({ skip, limit }, this)
    }).seq(function (resultats) {
      if (!resultats?.length) {
        // y'avait plus de suivant, on avait un multiple de limit, ou bien on avait déjà toutes les stats dans le fichier, ou bien y'a aucun résultat sur la période
        if (!stats.firstDate) stats.firstDate = startDate
        if (!stats.lastDate) stats.lastDate = periodeEnd
        return cb(null, stats)
      }
      if (!stats.firstDate) stats.firstDate = resultats[0].date
      for (const { oid, type, rid, contenu } of resultats) {
        stats.total++
        // init
        if (!stats[type]) stats[type] = { total: 0, rids: {} }
        stats[type].total++
        // comptage par rid
        if (!stats[type].rids[rid]) stats[type].rids[rid] = 0
        stats[type].rids[rid]++
        // ajout d'infos pour le type j3p
        if (type === 'j3p') addJ3pResultat(stats, oid, contenu)
      }
      stats.lastDate = resultats[resultats.length - 1].date
      if (resultats.length < limit) return cb(null, stats)
      setTimeout(() => nextBatch(skip + limit, cb), waitMs)
    }).catch(cb)
  }
  nextBatch(0)
}

function writeStats (stats) {
  if (!stats.firstDate || !stats.lastDate || !stats.periode) throw Error('Objet stats non initialisé')
  fs.writeFileSync(statsFile, JSON.stringify(stats), { encoding: 'utf8', mode: 0o644 })
  if (verbose) console.log(`Stats écrites dans ${statsFile}`)
}

/**
 * Compte le nb de résultats par type de ressource pour un mois donné
 * @param {function} done
 */
function statsResultats (done) {
  if (typeof done !== 'function') throw Error('appel incorrect (pas de callback)')

  const onEnd = (error, resultats) => {
    normalEnding = true
    done(error, resultats)
  }
  if (Resultat) return onEnd(Error('statsResultats must be called only once'))
  try {
    compileStats(function (error, stats) {
      if (error) return onEnd(error)
      writeStats(stats)
      normalEnding = true
      onEnd(null, stats)
    })
  } catch (error) {
    onEnd(error)
  }
}

statsResultats.help = () => console.log(`La commande statsResultats compile les stats de résultats pour un mois donné et les affiche à la fin avec :

Options :
  --month : mm ou yyyy-mm
  --waitMs : le nb de ms à attendre avant de passer aux 1000 résultats suivants (100 par défaut)
  --slow : idem --waitMs 1000
  --verbose : être plus bavard
  --quiet : ne rien sortir sur stdout (seulement dans le fichier json)
`)

module.exports = statsResultats
