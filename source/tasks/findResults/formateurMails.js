/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Sesalab (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de Sesalab, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

if (typeof app === 'undefined') throw new Error('Ce module ne doit être requis qu’après instanciation de lassi avec cli')

const flow = require('an-flow')

/**
 * Parse tous les résultats, puis toutes les séquences correspondantes, puis tous les formateurs liés,
 * pour afficher le nb de résultats par formateur, avec à la fin la liste de leur mails
 * Probablement créé pour trouver tous les formateurs ayant des séquences énormes,
 * et les prévenir qu'au delà de 3k résultats dans la séquence y'a des trucs qui marchent moins bien…
 * Ça sert encore ?
 * @param {errorCallback} done
 */
function findMailOfFormateurWithResults (done) {
  /**
   * Incrémente acc[prop] de ajout
   * @private
   * @param {Object} acc
   * @param {string} prop
   * @param {number} ajout
   */
  function inc (acc, prop, ajout) {
    if (acc[prop]) acc[prop] += ajout
    else acc[prop] = ajout
  }

  /**
   * Parse tous les résultats pour en compter le nombre par séquence
   * (dans la var globlale nbResultatsBySequence), puis lance grabSequences
   */
  function grabResults () {
    Resultat.match().grab({ limit, offset: skip }, function (error, resultats) {
      console.log('traitement des résultats', skip, 'à', skip + resultats.length)
      if (error) return done(error)
      resultats.forEach(function (resultat) {
        if (resultat.sequence) inc(nbResultatsBySequence, resultat.sequence, 1)
        else console.error('résultat sans séquence', resultat)
      })
      if (resultats.length === limit) {
        skip += limit
        process.nextTick(grabResults)
      } else {
        skip = 0
        process.nextTick(grabSequences)
      }
    })
  }

  /**
   * Récupère toutes les séquences avec une clé dans nbResultatsBySequence,
   * et cumule le nb de résultats par formateur, avant d'appeler grabFormateurs
   */
  function grabSequences () {
    flow(Object.keys(nbResultatsBySequence))
      .seqEach(function (seqId) {
        const nextSequence = this
        Sequence.match('oid').equals(seqId).grabOne(function (error, sequence) {
          if (error) return nextSequence(error)
          if (!sequence) console.error('Pas de séquence ' + seqId)
          else if (sequence.owner) inc(nbResultatsByFormateur, sequence.owner, nbResultatsBySequence[seqId])
          else console.error(`séquence ${seqId} sans owner`)
          nextSequence()
        })
      })
      .seq(function () {
        if (nbResultatsBySequence.length === limit) {
          skip += limit
          process.nextTick(grabSequences)
        } else {
          skip = 0
          process.nextTick(grabFormateurs)
        }
      })
      .catch(done)
  }

  /**
   * Récupère les formateurs ayant des résultats et appelle printFormateur pour chacun,
   * affiche la liste des mails à la fin
   * @private
   */
  function grabFormateurs () {
    console.log('nb res\tmail formateur (oid)')
    flow(Object.keys(nbResultatsByFormateur))
      .seqEach(function (utilId) {
        const nextFormateur = this
        entityUtilisateur.match('oid').equals(utilId).grabOne(function (error, utilisateur) {
          if (error) return nextFormateur(error)
          if (!utilisateur) console.error('Pas d’utilisateur ' + utilId)
          printFormateur(utilisateur)
          nextFormateur()
        })
      })
      .seq(function () {
        done(null, mails.join(', '))
      })
      .catch(done)
  }

  /**
   * Affiche une ligne pour cet utilisateur avec son nb de résultat
   * @private
   * @param utilisateur
   */
  function printFormateur (utilisateur) {
    console.log(`${nbResultatsByFormateur[utilisateur.oid]}\t${utilisateur.mail ?? 'sans mail'} (${utilisateur.oid})`)
    if (utilisateur.mail) mails.push(utilisateur.mail)
  }

  const Resultat = lassi.service('Resultat')
  const Sequence = lassi.service('Sequence')
  const entityUtilisateur = lassi.service('Utilisateur')
  const limit = 100
  const nbResultatsBySequence = {}
  const nbResultatsByFormateur = {}
  const mails = []
  // init
  let skip = 0
  // go
  grabResults()
}

module.exports = findMailOfFormateurWithResults
