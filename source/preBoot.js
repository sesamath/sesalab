'use strict'
/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
const { URL } = require('url')
const log = require('sesalab-api/source/tools/appLog')

const config = require('../config')

// on retry toutes les 10s, max 30 fois (5min, au cas où la sésathèque booterait en même temps
// avec un update bloquant un peu long)
const retryDelay = 10
const maxRetries = 30

// plusieurs modules permettent de récupérer les traces async, tous font de grosses surcharges,
// pénalisantes pour les perfs mais surtout non exempte de bug, faut surtout pas utiliser ça en prod
// après avoir regardé
// https://github.com/mattinsler/longjohn/
// https://github.com/tlrobinson/long-stack-traces
// https://github.com/groundwater/node-stackup
// on opte pour https://github.com/AndreasMadsen/trace
// on le lançait ici systématiquement hors prod, mais ça fait planter certains updates (out of memory)
// Quand on veut l'activer il faut lancer l'appli avec `pnpm run start:trace`

/**
 * Récupère notre baseId sur une sésathèque
 * @param {object} settings (avec application.baseUrl et application.sesatheques)
 * @param {number} sesathequeIndex l'index de la sesatheque à appeler
 */
function getBaseIdPromise (sesathequeIndex) {
  return new Promise((resolve, reject) => {
    // config a déjà vérifié le format correct des sesatheques
    const data = {
      baseUrl: config.application.baseUrl,
      sesatheques: config.application.sesatheques
    }
    const stBaseUrl = data.sesatheques[sesathequeIndex].baseUrl
    const urlString = stBaseUrl + 'api/checkSesalab'
    const httpModule = /^https/.test(urlString) ? require('https') : require('http')
    const { protocol, hostname, port, pathname, search } = new URL(urlString)
    const postString = JSON.stringify(data)
    const httpRequestOptions = {
      protocol,
      hostname,
      port,
      path: pathname + search,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postString)
      }
    }
    // pour les certifs autosignés (https://nodejs.org/api/https.html#https_https_request_options_callback)
    // ATTENTION, il faudra aussi faire un `export NODE_TLS_REJECT_UNAUTHORIZED=0`
    // avant de lancer le start de labomep et des biblis pour que les échanges
    // via node-fetch de sesalab-sso fonctionnent.
    if (/(localhost|\.local)$/.test(hostname)) {
      httpRequestOptions.rejectUnauthorized = false // à ne décommenter que pour du test local !
    }
    let retries = 0

    /**
     * Si on a pas atteint le délai max, affiche l'erreur en console et recommence,
     * sinon rejette avec l'erreur
     * @private
     * @param {Error} error
     */
    function retry (error) {
      retries++
      if (retries < maxRetries) {
        log.error(error, `On réessaie dans ${retryDelay} secondes`)
        setTimeout(
          () => fetchBaseId(),
          retryDelay * 1000
        )
      } else {
        // si la sesatheque répond toujours pas, ça doit pas être la bonne url, ou elle est HS
        // Dans les 2 cas on bloque le boot (sesalab fonctionnera pas sans sesathèque…)
        error.message = `La sésathèque ${stBaseUrl} ne répond toujours pas après ${maxRetries * retryDelay}s (${error.message})`
        reject(error)
      }
    }

    function fetchBaseId () {
      const req = httpModule.request(httpRequestOptions, (res) => {
        const statusCode = res.statusCode
        const contentType = res.headers['content-type']
        if (statusCode !== 200 || !/^application\/json/.test(contentType)) {
          return retry(new Error(`La Sésathèque ${stBaseUrl} ne renvoie pas la réponse attendue (${statusCode})`))
        }
        res.setEncoding('utf8')
        let data = ''
        res.on('data', chunk => (data += chunk))
        res.on('end', () => {
          let retour
          try {
            retour = JSON.parse(data)
          } catch (error) {
            return retry(new Error(`La Sésathèque ${stBaseUrl} ne retourne pas de json valide (${error.message}) : ${data}`))
          }
          if (retour.errors) return reject(new Error(`La Sésathèque ${stBaseUrl} retourne les erreurs suivantes :\n  - ${retour.errors.join('\n- ')}`))
          if (retour.baseId) return resolve(retour.baseId)
          // ici on a bien une réponse 200 mais pas au format attendu, c'est un pb de code incohérent
          // entre notre appel ici et sont traitement coté sésathèque
          // On devrait laisser continuer, mais sans baseId ça va coincer plus loin, et c'est probablement
          // le signe de versions incompatibles
          return reject(new Error(`La Sésathèque ${stBaseUrl} ne retourne pas la réponse attendue : ${data}`))
        })
      })
      req.on('error', (error) => {
        // en cli on laisse tomber
        if (lassi.options.cli || lassi.options.test) {
          return resolve()
        }
        retry(new Error(` : ${error}`))
      })
      req.write(postString, 'utf8')
      req.end()
    }

    fetchBaseId()
  })
}

/**
 * Vérifie que la config est correcte, et ajoute à la config
 * le baseId que la première sésathèque déclarée en config nous affecte
 * @param cb sera appelé avec (error)
 */
function preBoot (cb) {
  // il faut lancer le fetch baseId sur chaque instance de node car chacun a son config
  // (auquel il faut ajouter un baseId)
  log(`Environnement ${config.application.staging}`)
  log('Vérification de la configuration sur nos sésathèques (et récupération de notre baseId)')
  const promises = config.application.sesatheques.map((st, index) => getBaseIdPromise(index))
  Promise
    .all(promises)
    .then((baseIdList) => {
      const firstBaseId = baseIdList.shift()
      const errors = []
      baseIdList.forEach((baseId, index) => {
        if (baseId !== firstBaseId) errors.push(`La sésathèque n° ${index + 1} a des infos contradictoires, elle nous connait sous ${baseId} alors que la première nous connait sous ${firstBaseId}`)
      })
      if (errors.length) {
        if (lassi.options.cli || lassi.options.test) {
          return cb()
        }
        return cb(new Error(`Problème de configuration incohérente avec celle des sésathèques :\n  - ${errors.join('- \n')}\n`))
      }
      if (firstBaseId) {
        // on modifie l'objet exporté, comme il avait déjà cette propriété ça la modifie partout
        // le freeze de config.application sera fait juste après le boostrap lassi
        log(`Les sésathèques nous connaissent avec le baseId ${firstBaseId}`)
        config.application.baseId = firstBaseId
      } else {
        log('Pas de baseId (sésathèques hs ?), en cli on continue.')
      }
      cb()
    })
    .catch(cb)
}

module.exports = preBoot
