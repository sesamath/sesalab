#!/usr/bin/env node

'use strict'

/**
 * Manager de scripts exécutables en cli
 *
 * Il faut un service XXX-cli, qui doit avoir une fonction "commands"
 * qui renverra un objet de commandes {nomCommande1: methodeCommande1, …}
 *
 * `node source/cli.js -l` pour lister toutes les commandes
 *
 * Code de sortie
 * 0 : OK
 * 1 : pb de syntaxe dans l'appel
 * 2 : la commande a retourné une erreur
 * 3 : la commande a planté (sans catch de son erreur)
 */
const fs = require('fs')
const path = require('path')
const glob = require('glob')

const settings = require('../config')

// pour pouvoir inclure du sesalab-* sans les mettre en relatif
require('./rootPathSetup')(path.resolve(__dirname, '..'))

const preBoot = require('./preBoot')
const appLog = require('sesalab-api/source/tools/appLog')
appLog.setLogLevel('warning')

// Instanciation de lassi
require('lassi')({ root: path.join(__dirname, '/..'), logger: appLog, cli: true })
global.app = lassi.component('sesalab', ['sesalab-sso'])
// sesalab-sso a besoin de sa conf tout de suite
app.settings = settings

// Instanciation des modules principaux
require('../sesalab-api/source')
require('../sesalab-formateur/source/server')
require('../sesalab-eleve/source/server')
require('../sesalab-gestion/source/server')
require('../sesalab-home/source/server')
// et le sso vers les sesatheques
require('sesalab-sso')

// On ajoute toutes les tasks AVANT le bootstrap, on boucle sur les dossiers de tasks
fs.readdirSync(path.join(__dirname, 'tasks'))
  .map(name => path.join(__dirname, 'tasks', name))
  .filter(absoluteName => fs.lstatSync(absoluteName).isDirectory())
  .forEach(dir => {
    // on défini un service par task
    const serviceName = `$${path.basename(dir)}-cli`
    app.service(serviceName, function () {
      const commandsList = {}
      // on prend tous les js du dossier
      glob.sync(path.join(dir, '*.js')).forEach((file) => {
        const command = path.basename(file, '.js')
        const module = require(file)
        if (typeof module === 'function') {
          // en général c'est une fonction, ça donne une commande du nom du fichier
          commandsList[command] = module
        } else if (typeof module === 'object') {
          // mais si ça exporte une liste, la commande est le nom de la propriété exportée
          Object.keys(module).forEach(name => {
            commandsList[name] = module[name]
          })
        }
      })

      return {
        commands: () => commandsList
      }
    })
  })

// on ajoute d'éventuelles tasks ajoutées en config
if (settings.externalTasks) {
  for (const [taskName, path] of Object.entries(settings.externalTasks)) {
    const commandsList = {}
    const module = require(path)
    if (typeof module === 'function') {
      // en général c'est une fonction, ça donne une commande du nom du fichier
      commandsList[taskName] = module
    } else if (typeof module === 'object') {
      // mais si ça exporte une liste, la commande est le nom de la propriété exportée
      Object.keys(module).forEach(name => {
        commandsList[name] = module[name]
      })
    }
    if (Object.keys(commandsList).length) {
      app.service(`$${taskName}-cli`, function () {
        return {
          commands: () => commandsList
        }
      })
    } else {
      console.error(Error(`Aucune commande trouvée pour ${taskName} (${path})`))
    }
  }
}

// Gestion du composant principal custom
let mainComponent = app
if (settings.mainComponent) {
  mainComponent = require(settings.mainComponent)
}

preBoot((error) => {
  if (error) {
    console.error(error)
    console.error('En CLI on laisse le boot se poursuivre mais settings.application.baseId n’a pas été initialisé')
  }

  // Démarrage de lassi sur le composant principal
  lassi.bootstrap(mainComponent, () => {
    const cli = lassi.service('$cli')
    cli.printInfo('Sesalab’s CLI')

    // On lance la commande
    try {
      cli.run()
    } catch (error) {
      console.error(error)
    }
  })
})
