'use strict'
/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
const { join, resolve } = require('path')

// on commence par rootPathSetup pour faire ensuite des require relatif à la racine
const rootDir = join(__dirname, '..')
const rootPathSetup = require('./rootPathSetup')
rootPathSetup(rootDir)
// et maintenant il connaît sesalab-api
const appLog = require('sesalab-api/source/tools/appLog')

const preBoot = require('./preBoot')

const settings = require('../config')

// nos settings
const { application: appConfig } = settings
const { isProd, name, staging } = appConfig

/* ------ Configuration Loggers ------- */
const defaultLoglevel = appConfig.logLevel || (isProd ? 'warning' : 'info')
appLog.setLogLevel(defaultLoglevel)
appLog.separeErrors(true)

// Instanciation de lassi, pour mettre app en global
// afin que preBoot puis sesalab-sso puissent étoffer les settings
require('lassi')({ root: rootDir, logger: appLog })
global.app = lassi.component('sesalab', ['sesalab-sso'])

// sesalab-sso a besoin de sa conf tout de suite (preBoot aussi mais il fait un require)
// ATTENTION, coté node app.settings correspond à lassi.settings (on a donc app.settings.application)
// mais coté client (formateur ou élève), app.settings correspond à ce que renvoie /api/settings
// qui est une partie de lassi.settings.application
app.settings = settings

preBoot((error) => {
  if (error) {
    console.error('preBoot HS, ABANDON', error)
    process.exit(1)
  }
  appLog(`Démarrage de ${name} en staging ${staging} (isProd=${isProd})`)

  // Instanciation des modules principaux
  require('../sesalab-api/source')
  require('../sesalab-formateur/source/server')
  require('../sesalab-eleve/source/server')
  require('../sesalab-gestion/source/server')
  require('../sesalab-home/source/server')
  // et le sso vers les sesatheques
  require('sesalab-sso')

  // Gestion du composant principal custom
  const mainComponent = settings.mainComponent ? require(settings.mainComponent) : app

  if (!isProd) {
    const docDir = resolve(rootDir, 'docs', 'documentation')
    if (docDir) {
      mainComponent.controller(function () {
        this.serve('/doc', docDir)
      })
    } else {
      console.error(Error(`${docDir} n’existe pas, il faut générer la documentation avec "pnpm run generate:doc"`))
    }
  }

  // Démarrage de lassi sur le composant principal
  lassi.bootstrap(mainComponent)

  // config.application sera potentiellement surchargée dans certains contextes sso,
  // mais on ne veut surtout pas que la config par défaut soit mutée.
  Object.freeze(settings.application)

  const expressApp = lassi.service('$rail').get()
  // Nécessaire pour avoir les IP correctes derrière un proxy (varnish, nginx ou autre ?)
  expressApp.enable('trust proxy')
})
