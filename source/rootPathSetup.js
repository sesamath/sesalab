// code inspired by https://github.com/fabriziomoscon/rootpath/blob/master/pathSetup.js
// because rootpath module doesn't work with pnpm
// (it suppose to be located in {PROJECT_ROOT}/node_modules/rootpath/pathSetup.js)

const path = require('path')

/**
 * Ajoute rootdir aux chemins utilisés par require, pour autoriser par ex du require('sesalab-commun/…')
 * @param {string} rootDir
 */
module.exports = function rootPathSetup (rootDir) {
  // on le met en premier
  let newPath = rootDir
  // et on ajoute les paths existants éventuels
  if (process.env.NODE_PATH) {
    newPath += path.delimiter + process.env.NODE_PATH
  }
  process.env.NODE_PATH = newPath

  // reste à regénérer les paths dans lesquels require fouine
  require('module')._initPaths()
}
