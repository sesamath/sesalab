// on exporte un array de config, chaque elt étant ce qu'exporte un webpack.config.js habituel
const fullConfig = []

// lui ajoute hashFunction à toutes les config
const addConfig = (config) => {
  if (!config.output) config.output = {}
  // sur node ≥ 18 le build plante, il faut alors utiliser `NODE_OPTIONS=--openssl-legacy-provider` avant de lancer webpack
  // on préfère utiliser ici une autre hashFunction que md4, qui est le défaut de webpack et provoque le plantage sur node ≥ 18
  // cf https://v4.webpack.js.org/configuration/output/#outputhashfunction
  // ou https://webpack.js.org/configuration/output/#outputhashfunction
  config.output.hashFunction = 'md5' // ça plante avec xxhash64 & node20
  fullConfig.push(config)
}

for (const configs of [
  // ATTENTION à modifier scripts/published si ces dossiers changent
  require('./sesalab-eleve/webpack.config.js'),
  require('./sesalab-formateur/webpack.config.js'),
  require('./sesalab-gestion/webpack.config.js'),
  require('./sesalab-home/webpack.config.js')
]) {
  // chacun peut retourner un array ou une config unique
  if (Array.isArray(configs)) {
    for (const config of configs) addConfig(config)
  } else {
    addConfig(configs)
  }
}

// pour afficher la config complète
// console.log(JSON.stringify(fullConfig, null, 2))
// process.exit()

module.exports = fullConfig
