/*
Ce script veut dans son scope 3 variables
- externalId
- id1
- id2

# on lance le script de vérif d'intégrité
source/cli.js integrityCheck > integrityCheck.log
# le sed récupère les doublons sur externalId (sesasso)
# et les passe au while qui lance ce script avec
sed -nEe 's/^Doublons externalMech:sesasso--externalId:([0-9]+) - formateur#([^,]+), formateur#([^ ]+).*$/\1 \2 \3/p' integrityCheck.log|while read externalId id1 id2
  do ./scripts/mongoApp -e "const externalId = '$externalId'; const id1 = '$id1'; const id2 = '$id2'" -q -f scripts-mongo/fixSesassoDuplicate.js
  done
*/
/* global db externalId id1 id2 print */
if (typeof externalId === 'undefined') throw new Error('externalId doit être défini dans le scope (avec -e à l’appel de mongoApp, ou --eval avec mongo directement)')
if (typeof id1 === 'undefined') throw new Error('id1 doit être défini dans le scope (avec -e à l’appel de mongoApp, ou --eval avec mongo directement)')
if (typeof id2 === 'undefined') throw new Error('id2 doit être défini dans le scope (avec -e à l’appel de mongoApp, ou --eval avec mongo directement)')

const infos = []
db.Utilisateur.find({ externalMech: 'sesasso', externalId }, { login: 1 }).forEach(u => infos.push([u._id, u.login]))

if (infos.length < 2) throw new Error(`on a pas récupéré 2 formateurs pour sesasso_${externalId}`)

let tok, tod
if (infos[0][1] === 'sesasso_' + externalId) {
  tok = infos[0][0]
  tod = infos[1][0]
} else if (infos[1][1] === 'sesasso_' + externalId) {
  tok = infos[1][0]
  tod = infos[0][0]
} else {
  throw new Error(`aucun des des deux users ${id1} ${id2} n’a le login sesasso_${externalId}`)
}
// on affiche la commande qu'il faudra lancer
print(`source/cli.js mergeFormateurs --toKeep=${tok} --toDelete=${tod}`)
// cette commande vérifie que c'est le même user dans le même établissement, sinon affiche les différences
// (relancer éventuellement avec --force)
