/* global db print */
const hasProp = (obj, prop) => typeof obj === 'object' && Object.prototype.hasOwnProperty.call(obj, prop)

const reducer = (total, score) => {
  const localScore = Number(score)
  if (localScore > 1) print('  # Un score d’un nœud de est invalide (> 1)')
  else if (localScore > 0) total += localScore
  else if (localScore !== 0) print('  # Un score d’un nœud est invalide (pas un nombre)')
  return total
}

const cursor = db.Resultat.find()

while (cursor.hasNext()) {
  const r = cursor.next()
  const d = JSON.parse(r._data)
  if (d.ressType === 'j3p' && d.fin && !hasProp(d, 'score')) {
    print(`ressource ${d.rid} résultat ${r._id}`)
    if (d.contenu && d.contenu.scores && d.contenu.scores.length) {
      const nb = d.contenu.scores.length
      const total = d.contenu.scores.reduce(reducer, 0)
      d.score = total / nb
      r._data = JSON.stringify(d)
      db.Resultat.save(r)
    } else {
      print('  # Pas de score dans le contenu')
    }
  }
}
