/* global db print */
const hasProp = (obj, prop) => typeof obj === 'object' && Object.prototype.hasOwnProperty.call(obj, prop)

// mets les externals null
function cleanUser (user) {
  const data = JSON.parse(user._data)
  if (hasProp(data, 'externalMech') || hasProp(data, 'externalId')) {
    delete data.externalMech
    delete data.externalId
    user._data = JSON.stringify(data)
  }
  user.externalMech = null
  user.externalId = null
  db.Utilisateur.save(user)
}

print('Rectification des externalMech/externalId')

let user, data
let i = 0

// on vire les 'undefined' / 'undefined'
print('Nettoyage des external undefined/undefined')
let cursor = db.Utilisateur
  .find({ externalMech: 'undefined', externalId: 'undefined' })
while (cursor.hasNext()) {
  i++
  user = cursor.next()
  data = JSON.parse(user._data)
  if (data.externalMech || data.externalId) {
    print(`ERROR pb d'index external ${user._id}`)
  } else {
    if (hasProp(data, 'externalMech')) delete data.externalMech
    if (hasProp(data, 'externalId')) delete data.externalId
    user._data = JSON.stringify(data)
    // pour les valeurs d'index, on met du null (pas delete) pour avoir le même comportement que Entity
    user.externalMech = null
    user.externalId = null
    db.Utilisateur.save(user)
    print(`undefined/undefined viré de ${user._id}${user.type === 0 ? '' : ' (formateur)'}`)
  }
}

cursor = db.Utilisateur
  .find({ type: 0, externalMech: 'undefined' })
  .sort({ externalId: 1 })
i = 0
while (cursor.hasNext()) {
  cleanUser(cursor.next())
  i++
}
print(`${i} external undefined/id nettoyés`)

// il reste qq résidus de création ENT foireuses
db.Utilisateur.remove({ type: 0, externalId: 'undefined' })
