/* global db print */

// libère les structures sans élèves de la contrainte onlyFromEntId

// academieId 23 => Nice
// academieId 27 => Corse
const cursor = db.Structure.find({ academieId: '23', onlyFromEntId: '487' })

let nb = 0
let nbCleaned = 0

while (cursor.hasNext()) {
  const e = cursor.next()
  const sid = e._id.toString()
  const nbEleves = db.Utilisateur.countDocuments({ structures: sid, type: 0 })
  if (nbEleves > 0) {
    print(`structure ${sid} à ${e.ville} (${e.departementNumero}) contient ${nbEleves} élèves`)
  } else {
    if (typeof e._data === 'string') e._data = JSON.parse(e._data)
    delete e.onlyFromEntId
    delete e._data.onlyFromEntId
    // https://www.mongodb.com/docs/manual/reference/method/db.collection.updateOne/
    // https://www.mongodb.com/docs/manual/reference/operator/update/unset/#mongodb-update-up.-unset
    db.Structure.updateOne({ _id: e._id }, { $unset: { onlyFromEntId: '', '_data.onlyFromEntId': '' } })
    print(`structure ${sid} à ${e.ville} (${e.departementNumero}) libérée de la contrainte d’accès via le GAR only`)
    nbCleaned++
  }
  nb++
}

print(`${nb} structures étudiées dont ${nbCleaned} libérées de la contrainte ENT`)
