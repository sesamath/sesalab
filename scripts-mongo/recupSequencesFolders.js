/* global db, print */

/*
Ce script passe en revue toutes les séquences des profs et regardent si elles sont rangées dans un dossier de "Mes séquences" autre que "Non trié"
Si ce n'est pas le cas, il regarde dans la collection UtilisateurRecup
 (issu du backup de la veille) pour voir s'il retrouve un dossier
  où cette séquence était rangée la veille.

Pour créer cette collection UtilisateurRecup en local :
(labomep.gz est le backup de la veille)
`mongorestore --host localhost --port 27017 -u labomep -p 'xxx' --authenticationDatabase=labomep --archive --gzip --nsFrom="labomep.Utilisateur" --nsTo="labomep.UtilisateurRecup" --nsInclude='labomep.Utilisateur' < labomep.gz`

puis après test du script on exporte ça
`./scripts/mongoApp -x -c UtilisateurRecup -f mongodumps/UtilisateurRecup.export`
puis filtrage prof
`grep ',"type":1,' mongodumps/UtilisateurRecup.export > mongodumps/UtilisateurRecupProf.export`

puis en prod (faut commenter la ligne de mongoApp qui interdit l'import)
`./scripts/mongoApp -i -c UtilisateurRecup -f mongodumps/UtilisateurRecupProf.export`

et lancement de ce script avec
`./scripts/mongoApp -f scripts-mongo/recupSequencesFolders.js`
 */

/**
 * Retourne le folderId contenant la séquence seqOid, si on le trouve
 * @returns {string|undefined}
 */
function findFolderId (folders, seqOid) {
  for (const folder of folders) {
    if (folder.sequences.includes(seqOid)) {
      return folder.uuid
    }
    if (folder.folders.length) {
      const id = findFolderId(folder.folders, seqOid)
      if (id) return id
    }
  }
}

/**
 * Retourne l'objet folder ayant le bon folderId s'il existe dans folders (ou un descendant)
 * @returns {Object|undefined}
 */
function findFolder (folders, folderId) {
  for (const folder of folders) {
    if (folder.uuid === folderId) {
      return folder
    }
    if (folder.folders.length) {
      const f = findFolder(folder.folders, folderId)
      if (f) return f
    }
  }
}

function getTidySeqOids (folders, seqOids = []) {
  for (const folder of folders) {
    if (folder.nom === 'Non trié') continue
    if (folder.sequences?.length) {
      seqOids.push(...folder.sequences)
    }
    if (folder.folders?.length) {
      getTidySeqOids(folder.folders, seqOids)
    }
  }
  return seqOids
}

try {
  let nbCorrections = 0
  let nbUsers = 0
  const cursor = db.Utilisateur.find({ type: 1 })

  while (cursor.hasNext()) {
    const u = cursor.next()
    const uid = u._id
    nbUsers++
    const folders = u._data.sequenceFolders?.folders
    if (!folders?.length) continue
    const tidySeqs = getTidySeqOids(folders)
    // on cherche toutes les séquence de ce user
    const seqCursor = db.Sequence.find({ owner: uid }, { _id: 1 })
    // pour garder celles qui ne sont pas rangées
    const orphanSeqs = []
    while (seqCursor.hasNext()) {
      const seq = seqCursor.next()._id
      if (!tidySeqs.includes(seq)) orphanSeqs.push(seq)
    }
    if (!orphanSeqs.length) continue

    // y'a des orphelines (non triées)
    const nonTrie = folders.find(f => f.nom === 'Non trié') ?? { sequences: [] }

    // on regarde si on arrive à les ranger d'après le backup
    let nbChanges = 0
    const cursorBk = db.UtilisateurRecup.find({ _id: uid }, { '_data.sequenceFolders': 1 })
    if (cursorBk.hasNext()) {
      const uBak = cursorBk.next()
      const foldersBak = uBak._data.sequenceFolders?.folders
      if (!foldersBak?.length) continue // next user

      for (const orphanSeq of orphanSeqs) {
        const idBak = findFolderId(foldersBak, orphanSeq)
        if (idBak && idBak !== nonTrie.uuid) {
          const correctFolder = findFolder(folders, idBak)
          if (correctFolder) {
            correctFolder.sequences.push(orphanSeq)
            // on vire les null au passage, y'en a plein après le bug qui a perdu le tri
            nonTrie.sequences = nonTrie.sequences.filter(seq => seq && seq !== orphanSeq)
            nbChanges++
          }
        }
      }

      // conclusion
      if (nbChanges) {
        print(`Le user ${uid} aurait récupéré le tri de ${nbChanges} séquences sur ${orphanSeqs.length}`)
        nbCorrections++
        db.Utilisateur.updateOne(
          { _id: uid },
          { $set: { '_data.sequenceFolders': u._data.sequenceFolders } }
        )
      } else {
        print(`Le user ${uid} a ${orphanSeqs.length} séquences non triées mais aucune récupérées (${orphanSeqs.join(', ')})`)
        // printjson(foldersBak)
      }
    } else {
      print(`Le user ${uid} n'a pas de backup`)
    }
  }
  print(`FIN, on a corrigé le tri de ${nbCorrections} sur ${nbUsers} étudiés`)
} catch (error) {
  console.error(error)
}
