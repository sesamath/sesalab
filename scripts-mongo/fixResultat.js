/* global db print */

// fix les pb de validation rencontrés au réindex le 18/06/2018
// type non string
// date string

const cursor = db.Resultat.find()

while (cursor.hasNext()) {
  const r = cursor.next()
  let needSave = false
  // tout le monde a pas forcément été réindexé sans serialize de _data
  if (typeof r._data === 'string') r._data = JSON.parse(r._data)

  // fix type
  if (typeof r._data.ressource.type !== 'string') {
    print(`résultat ${r._id} type ${typeof r._data.ressource.type} ${r._data.ressource.type} (${r._data.ressource.rid}) ≠ ${r._data.ressType} (${r._data.rid}) ${r._data.date}`)
    if (typeof r._data.ressType === 'string') {
      r._data.ressource.type = r._data.ressType
    } else {
      delete r._data.ressource.type
    }
    needSave = true
  }

  // fix date
  if (typeof r._data.date === 'string') {
    print(`résultat ${r._id} date invalide (string) ${r._data.date}`)
    r._data.date = new Date(r._data.date)
    needSave = true
  }
  if (needSave) db.Resultat.save(r)
}
