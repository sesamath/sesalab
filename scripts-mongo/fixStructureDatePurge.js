/* global db print */

// fix les pb de validation rencontrés au réindex le 18/06/2018
// datePurge null ou undefined

const cursor = db.Structure.find()

while (cursor.hasNext()) {
  const e = cursor.next()
  if (typeof e._data === 'string') e._data = JSON.parse(e._data)
  if (!e._data.datePurge) {
    print(`Structure ${e._id} sans date de purge, fixée au 2018-08-19`)
    e._data.datePurge = new Date('2018-08-19')
    db.Structure.save(e)
  }
}
