/* global db print */

// fix les pb de validation rencontrés au réindex le 18/06/2018
// mail null pour des élèves

const cursor = db.Utilisateur.find()

while (cursor.hasNext()) {
  const e = cursor.next()
  let needSave = false
  // tout le monde a pas forcément été réindexé sans serialize de _data
  if (typeof e._data === 'string') e._data = JSON.parse(e._data)
  const d = e._data

  // fix mail null
  if (d.type === 0 && d.mail === null) {
    print(`utilisateur ${e._id} avec mail null viré`)
    delete d.mail
    needSave = true
  }
  // fix nationalId null
  if (d.nationalId === null) {
    print(`utilisateur ${e._id} avec nationalId null viré`)
    delete d.nationalId
    needSave = true
  }

  if (needSave) db.Utilisateur.save(e)
}
