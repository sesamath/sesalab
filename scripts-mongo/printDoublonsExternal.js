/* global db print type */

// affiche les doublons external
// appeler ce script avec, pour les élèves
//   ./scripts/mongoApp -e 'var type = 0' -f scripts-mongo/printDoublonsExternal.js
// et pour les formateurs
//   ./scripts/mongoApp -e 'var type = 1' -f scripts-mongo/printDoublonsExternal.js

const typeToSearch = typeof type === 'undefined' ? 0 : type
if (typeToSearch !== 0 && typeToSearch !== 1) throw new Error('type ne peut valoir que 0 ou 1')
print(`Affichage des doublons externalMech/externalId pour les ${typeToSearch === 0 ? 'élèves' : 'formateurs'}`)

const cursor = db.Utilisateur
  // pkoi ça veut pas avec ça ?
  // .find({type: typeToSearch, $nor: [{externalMech: 'undefined'}, {externalId: 'undefined'}]})
  // .find({$nor: [{externalMech: 'undefined'}, {externalId: 'undefined'}, {type: 1}]})
  .find({ type: typeToSearch })
  .sort({ externalMech: 1, externalId: 1, __deletedAt: 1 })
let old = {}
let user
let toPrint
while (cursor.hasNext()) {
  user = cursor.next()
  if (user.externalId === old.externalId && user.externalMech === old.externalMech) {
    if (toPrint) toPrint.push(user._id)
    else toPrint = [`${user.externalMech}/${user.externalId}`, old._id, user._id]
  } else if (toPrint) {
    print.apply(this, toPrint)
    toPrint = null
  }
  old = user
}
