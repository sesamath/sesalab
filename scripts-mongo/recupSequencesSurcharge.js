/* global db, print */

/*
Ce script compare les collections Sequence et SequenceRecup pour remettre
 dans les ressources de la série de Sequence d'éventuelles surcharges, virées par l'update 30"

Pour créer cette collection SequenceRecup en local :
(labomep.gz est le backup de la veille)
`mongorestore --host localhost --port 27017 -u labomep -p 'xxx' --authenticationDatabase=labomep --drop --archive --gzip --nsFrom="labomep.Sequence" --nsTo="labomep.SequenceRecup" --nsInclude='labomep.Sequence' < labomep.gz`

puis après test du script on exporte ça
`./scripts/mongoApp -x -c SequenceRecup -f mongodumps/SequenceRecup.export`

puis en prod (faut commenter la ligne de mongoApp qui interdit l'import)
`./scripts/mongoApp -i -c SequenceRecup -f mongodumps/SequenceRecup.export`

et lancement de ce script avec
`./scripts/mongoApp -f scripts-mongo/recupSequencesSurcharge.js`
 */

const propsToRecup = ['nonZapping', 'minimumReussite', 'maximumVisionnage']

function hasRessourceOverride (seqData) {
  for (const ssSeq of seqData.sousSequences) {
    if (!ssSeq.serie?.length) return false
    for (const ress of ssSeq.serie) {
      for (const prop of propsToRecup) {
        if (ress[prop] != null) return true
      }
    }
  }
  return false
}

function restore (seq, seqRecup) {
  const changes = new Set()
  for (const [indexSsSeq, ssSeqRecup] of seqRecup._data.sousSequences.entries()) {
    if (!ssSeqRecup.serie?.length) continue
    const ssSeq = seq._data.sousSequences[indexSsSeq]
    if (!ssSeq) return
    for (const ressRecup of ssSeqRecup.serie) {
      if (propsToRecup.some(prop => ressRecup[prop] != null)) {
        // y'a des trucs à récupérer
        const ress = ssSeq.serie.find(r => r.rid === ressRecup.rid)
        if (!ress) {
          print(`pas trouvé la ressource ${ressRecup.rid} dans la série ${indexSsSeq} de la séquence ${seqRecup._id}`)
          continue
        }
        for (const prop of propsToRecup) {
          if (ressRecup[prop] != null) {
            ress[prop] = ressRecup[prop]
            changes.add(ress.rid)
          }
          print(`màj de la ressource ${ress.rid} de la sous-séquence ${indexSsSeq} de la séquence ${seq._id}`)
        }
      }
    }
  }
  if (changes.size > 0) {
    // faut màj en db
    db.Sequence.replaceOne({ _id: seq._id }, seq)
  }
  return changes.size
}

try {
  let nbCorrections = 0
  let nbSequence = 0
  // const cursor = db.SequenceRecup.find({ owner: '1' })
  const cursor = db.SequenceRecup.find()

  while (cursor.hasNext()) {
    const seqRecup = cursor.next()
    nbSequence++
    if (!hasRessourceOverride(seqRecup._data)) continue
    const seq = db.Sequence.findOne({ _id: seqRecup._id })
    if (!seq) {
      print(`Pas trouvé la séquence ${seqRecup._id}`)
      continue
    }
    const nbChanges = restore(seq, seqRecup)
    if (nbChanges) {
      print(`${nbChanges} surcharge(s) dans les séries de la séquence ${seqRecup._id}`)
      nbCorrections++
    }
  }
  print(`FIN, on a corrigé la surcharge de ${nbCorrections} séquences sur ${nbSequence} séquences étudiées`)
} catch (error) {
  console.error(error)
}
