// ce contenu peut être copié dans un ~/.mongorc
// il est utilisé pour précharger ces fonctions utiles lorsque l'on ouvre un shell
// avec ./scripts/mongoApp

let aide = '\nCe shell ajoute plusieurs fonctions utiles pour sesalab: '
/* global db print printjson */
/* eslint-disable no-unused-vars */

// affiche toutes les clés d'index (d'une collection ou de toutes)
const getAllIndexes = (collectionName) => {
  const colls = collectionName ? [collectionName] : db.getCollectionNames()
  colls.forEach((coll) => {
    db[coll].getIndexes().forEach(i => print(coll, Object.keys(i.key).join(', '), `(${i.name})`))
  })
}
aide += '\n  getAllIndexes() retourne les index de toutes les collections (qui peuvent donc être utilisés dans une query)\n'
aide += '  getAllIndexes(collectionName) retourne les index collectionName (qui peuvent donc être utilisés dans une query)\n'

// des helpers
aide += '\nCes fonctions peuvent être utile dans un find().forEach(e => …)'

const printData = (entity) => printjson(entity._data)
aide += '\n  printData(entity) => affiche entity._data'

const printIndexes = (entity) => {
  delete entity._data
  printjson(entity)
}
aide += '\n  printIndexes(entity) => affiche entity sans _data'
const printAll = (entity) => {
  const data = entity._data
  delete entity._data
  printjson({ indexes: entity, data })
}
aide += '\n  printAll(entity) => print {indexes, data} (pretty)'

/**
 * Retourne un critère normé pour find
 * @param {string|object} criteria Si c'est une string la cherchera comme _id, sinon argument passé à find
 * @return {object}
 */
const normCriteria = (criteria) => (typeof criteria === 'string' ? { _id: criteria } : criteria) || {}
/**
 * Si filter est une string renvoie le filtre qui ne remontera que ce champ,
 * sinon renvoie filters (éventuellement undefined)
 * @param {string|object|undefined} filters
 * @return {object|undefined}
 */
const normFilters = (filters) => (typeof filters === 'string') ? { [filters]: true } : filters

aide += '\n\nFonctions génériques'
/**
 * Raccourci pour find
 * @param {string} collection Nom de la collection
 * @param {string|object} criteria Si c'est une string la cherchera comme _id, sinon argument passé à find
 * @param {object} [filters] éventuel 2e argument de find
 */
const find = (collection, criteria, filters) => db.getCollection(collection).find(normCriteria(criteria), normFilters(filters))
aide += '\n  find(collectionName, criteria, filters) : alias de db[collectionName].find(criteria, filters)'
/**
 * Raccourci pour printjson après un find
 * @param collection
 * @param criteria
 * @param filters
 */
const data = (collection, criteria, filters) => find(collection, criteria, filters).limit(50).forEach(printData)
aide += '\n  data(collectionName, criteria, filters) : exécute find puis affiche les 50 premiers résultats (_data only, en clair)'

const all = (collection, criteria, filters) => find(collection, criteria, filters).limit(50).forEach(printAll)
aide += '\n  all(collectionName, criteria, filters) : exécute find puis affiche les 50 premiers résultats ({indexes, data})'

const indexes = (collection, criteria, filters) => find(collection, criteria, filters).limit(50).forEach(printIndexes)
aide += '\n  index(collectionName, criteria, filters) : exécute find puis affiche les 50 premiers résultats ({indexes, data})'

aide += '\n\nLeurs déclinaisons sur nos collections'

/**
 * Recherche parmi Groupe
 * @param criteria
 * @param filters
 */
const fg = (criteria, filters) => find('Groupe', criteria, filters)
aide += '\n  fg(criteria, filters) : alias de db.Groupe.find(criteria, filters)'
const dg = (criteria, filters) => data('Groupe', criteria, filters)
aide += '\n  dg(criteria, filters) : fg + printData'
const ig = (criteria, filters) => indexes('Groupe', criteria, filters)
aide += '\n  ig(criteria, filters) : fg + printIndexes'
const ag = (criteria, filters) => all('Groupe', criteria, filters)
aide += '\n  ag(criteria, filters) : fg + printAll'
/**
 * Recherche parmi Résultat
 * @param criteria
 * @param filters
 */
const fr = (criteria, filters) => find('Resultat', criteria, filters)
aide += '\n  fr(criteria, filters) : alias de db.Resultat.find(criteria, filters)'
const dr = (criteria, filters) => data('Resultat', criteria, filters)
aide += '\n  dr(criteria, filters) : fr + printData'
const ir = (criteria, filters) => indexes('Resultat', criteria, filters)
aide += '\n  ir(criteria, filters) : fr + printIndexes'
const ar = (criteria, filters) => all('Resultat', criteria, filters)
aide += '\n  ar(criteria, filters) : fr + printAll'
/**
 * Recherche parmi Sequence
 * @param criteria
 * @param filters
 */
const fq = (criteria, filters) => find('Sequence', criteria, filters)
aide += '\n  fq(criteria, filters) : alias de db.Sequence.find(criteria, filters)'
const dq = (criteria, filters) => data('Sequence', criteria, filters)
aide += '\n  dq(criteria, filters) : fq + printData'
const iq = (criteria, filters) => indexes('Sequence', criteria, filters)
aide += '\n  iq(criteria, filters) : fq + printIndexes'
const aq = (criteria, filters) => all('Sequence', criteria, filters)
aide += '\n  aq(criteria, filters) : fq + printAll'
/**
 * Recherche parmi Structure
 * @param criteria
 * @param filters
 */
const fs = (criteria, filters) => find('Structure', criteria, filters)
aide += '\n  fs(criteria, filters) : alias de db.Structure.find(criteria, filters)'
const ds = (criteria, filters) => data('Structure', criteria, filters)
aide += '\n  ds(criteria, filters) : fs + printData'
const is = (criteria, filters) => indexes('Structure', criteria, filters)
aide += '\n  is(criteria, filters) : fs + printIndexes'
const as = (criteria, filters) => all('Structure', criteria, filters)
aide += '\n  as(criteria, filters) : fs + printAll'
/**
 * Recherche parmi Utilisateur
 * @param criteria
 * @param filters
 */
const fu = (criteria, filters) => find('Utilisateur', criteria, filters)
aide += '\n  fu(criteria, filters) : alias de db.Utilisateur.find(criteria, filters)'
const du = (criteria, filters) => data('Utilisateur', criteria, filters)
aide += '\n  du(criteria, filters) : fu + printData'
const iu = (criteria, filters) => indexes('Utilisateur', criteria, filters)
aide += '\n  iu(criteria, filters) : fu + printIndexes'
const au = (criteria, filters) => all('Utilisateur', criteria, filters)
aide += '\n  au(criteria, filters) : fu + printAll'
/**
 * Recherche parmi UtilisateurAcces
 * @param criteria
 * @param filters
 */
const fa = (criteria, filters) => find('UtilisateurAcces', criteria, filters)
aide += '\n  fa(criteria, filters) : alias de db.UtilisateurAcces.find(criteria, filters)'
const da = (criteria, filters) => data('UtilisateurAcces', criteria, filters).forEach(printData)
aide += '\n  da(criteria, filters) : fa + printData'
const ia = (criteria, filters) => indexes('UtilisateurAcces', criteria, filters).forEach(printData)
aide += '\n  ia(criteria, filters) : fa + printIndexes'
const aa = (criteria, filters) => all('UtilisateurAcces', criteria, filters).forEach(printAll)
aide += '\n  aa(criteria, filters) : fa + printAll'

aide += '\n\nEt quelques recherches courantes plus spécifiques'
/**
 * Recherche parmi Groupe (avec filtre isClasse ajouté)
 * @param criteria
 * @param filters
 */
const fclasse = (criteria, filters) => {
  criteria = normCriteria(criteria)
  criteria.isClasse = true
  return fg(criteria, filters)
}
aide += '\n  fclasse(criteria, filters) : idem fg en filtrant sur les classes'
const dclasse = (criteria, filters) => fclasse(criteria, filters).limit(50).forEach(printData)
aide += '\n  dclasse(criteria, filters) : fclasse + printData'
const iclasse = (criteria, filters) => fclasse(criteria, filters).limit(50).forEach(printIndexes)
aide += '\n  iclasse(criteria, filters) : fclasse + printIndexes'
const aclasse = (criteria, filters) => fclasse(criteria, filters).limit(50).forEach(printAll)
aide += '\n  aclasse(criteria, filters) : fclasse + printAll'
/**
 * Recherche parmi Utilisateur (avec filtre élève ajouté)
 * @param criteria
 * @param filters
 */
const felv = (criteria, filters) => {
  criteria = normCriteria(criteria)
  criteria.type = 0
  return find('Utilisateur', criteria, filters)
}
aide += '\n  felv(criteria, filters) : idem fu en filtrant sur les élèves'
const delv = (criteria, filters) => felv(criteria, filters).limit(50).forEach(printData)
aide += '\n  delv(criteria, filters) : felv + printData'
const ielv = (criteria, filters) => felv(criteria, filters).limit(50).forEach(printIndexes)
aide += '\n  ielv(criteria, filters) : felv + printIndexes'
const aelv = (criteria, filters) => felv(criteria, filters).limit(50).forEach(printAll)
aide += '\n  aelv(criteria, filters) : felv + printAll'
/**
 * Recherche parmi Utilisateur (avec filtre formateur ajouté)
 * @param criteria
 * @param filters
 */
const fpro = (criteria, filters) => {
  criteria = normCriteria(criteria)
  criteria.type = 1
  return fu(criteria, filters)
}
aide += '\n  fpro(criteria, filters) : idem fu en filtrant sur les formateurs'
const dpro = (criteria, filters) => fpro(criteria, filters).limit(50).forEach(printData)
aide += '\n  dpro(criteria, filters) : fpro + printData'
const ipro = (criteria, filters) => fpro(criteria, filters).limit(50).forEach(printIndexes)
aide += '\n  ipro(criteria, filters) : fpro + printIndexes'
const apro = (criteria, filters) => fpro(criteria, filters).limit(50).forEach(printAll)
aide += '\n  apro(criteria, filters) : fpro + printAll'

aide += '\nTapez `aide` pour revoir cette liste\n'
print(aide)
/* eslint-enable no-unused-vars */
