const fs = require('fs')

// ce qooxdooBuilder.js est généré par sesalab-formateur/qooxdooBuilder/source/classes/qooxdooBuilder/Application.js
const content = fs.readFileSync('./build/script/qooxdooBuilder.js')
const lines = content.toString().split(/\n/)

let skip = 1
for (let i = 0, ii = lines.length; i < ii; i++) {
  const line = lines[i]
  if (skip === 1 && line.indexOf("qx.$$packageData['0']") === 0) {
    skip = false
    continue
  }
  if (!skip && line.indexOf('qx.Class.define("qooxdooBuilder.Application", {') === 0) {
    skip = 2
    continue
  }
  if (skip === 2 && line === '});') {
    skip = false
    continue
  }
  if (line === 'qx.$$loader.init();') {
    skip = 3
    continue
  }
  if (!skip) {
    console.log(line)
  }
}
