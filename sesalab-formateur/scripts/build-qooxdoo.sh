#! /bin/bash

# génération de sesalab-formateur/source/client/classes.js
# (pour lister les classes qooxdoo à inclure dans le build)
$(dirname $0)/before_build

dirIni=$(pwd)
cd $(dirname $0)/../qooxdooBuilder
./generate.py build
# avec uglify-js en dépendance du projet, pour lancer ça il faut avant avoir fait un `pnpm i uglify-js`
#node ../scripts/qooxdooCutter.js | ../../node_modules/uglify-js/bin/uglifyjs -c -m > ../source/assets/qooxdoo.js
# avec uglify-js en global
node ../scripts/qooxdooCutter.js | uglifyjs -c -m > ../source/assets/qooxdoo.js

cd $dirIni
