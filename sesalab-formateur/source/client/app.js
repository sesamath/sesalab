// checkBrowser & bugsnag seront ajouté avant tout ça par webpackConfigBuilder.js
require('../styles/app.scss')

// Par défaut l'intitulé de l'erreur apparait en anglais, on surcharge le nom pour avoir une traduction.
Error.prototype.name = 'Erreur' // eslint-disable-line no-extend-native

require('./classes')
//* global qx */
// qx.$$loader.init() // il est dans le source html
