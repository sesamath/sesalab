const { askImportRid } = require('../../common/ressource')
const { PANEL_MES_RESSOURCES_TITLE } = require('sesalab-commun/constants')

const getRessourcePageId = (ressource) => `ressource-${ressource.$id}`

/**
 * Panneau de gestion de mes ressources
 */
qx.Class.define('sesalab.panels.MesRessources', {
  extend: sesalab.widgets.FolderPanel,

  construct: function (group) {
    this.base(arguments, group, PANEL_MES_RESSOURCES_TITLE, { deleteMessage: 'Supprimer' })
  },

  members: {
    /**
     * Initialise l'élément.
     *
     * @params folders Elements à charger dans l'arbre
     */
    init (folders) {
      // listener
      function onPointerOver (e) {
        // this est bien le panel courant (bind à l'appel de addListener,
        // cf http://www.qooxdoo.org/current/api/#qx.bom.Element~addListener!method_public)
        app.displayRessourceInformation(e, this)
      }

      if (this.initFolderPanel(folders, 'ressources', { sorted: 'asc' })) {
        // le bouton + mis dans la bande du haut du panneau
        this.barButton('onAddResourceFromBar', 'Créer une ressource dans le dossier selectionné', 'add_row', 'ressource-creation')
        // le bouton import à sa droite
        this.barButton('onImportResourceFromBar', 'Importer une ressource dans le dossier selectionné', 'import', 'ressource-import')
        // l'entrée au clic droit sur un dossier
        this.menuButton('onAddResource', 'Créer une ressource', 'add_row')
        this.menuButton('onImportResource', 'Importer une ressource', 'import')
        // les actions sur les ressources
        this.menuButton('onTestResource', 'Tester la ressource', 'start', true)
        this.menuButton('onModifyAlias', 'Modifier cet alias', 'edit')
        this.menuButton('onModifyResource', 'Modifier', 'edit')
        this.menuButton('onDuplicateResource', 'Dupliquer puis modifier', 'database_duplicate')
        this.menuButton('onDeleteResource', 'Supprimer', 'empty_trash')
        this.menuButton('onShowInformations', 'Plus d’informations', 'about')
        this.menuButton('onPreview', 'Aperçu dans un autre onglet', 'external')
        this.menuButton('onNotify', 'Signaler une anomalie', 'support')

        this.unsorted = undefined
        this.tree.addListener('pointerover', onPointerOver, this)
        this.getContentElement().setAttribute('id', 'panel-mes-ressources')
        this.createLoader()
      }
    },

    onAddResourceFromBar: function () {
      const selectedFolder = this.tree.getSelectedNode()
      if (selectedFolder && selectedFolder.instanceOf(sesalab.behaviors.Dossier)) {
        this.onAddResource(selectedFolder)
      } else {
        this.onAddResource()
      }
    },

    onImportResourceFromBar: function () {
      const selectedFolder = this.tree.getSelectedNode()
      if (selectedFolder && selectedFolder.instanceOf(sesalab.behaviors.Dossier)) {
        this.onImportResource(selectedFolder)
      } else {
        this.onImportResource()
      }
    },

    onNotify: function () {
      // s'il y a une ressource sélectionnée on ajoute son rid à la notif
      const rid = this.tree.getSelectedObject()?.rid
      sesalab.dialogs.Contact.execute({ rid })
    },

    onShowInformations: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      if (selectedObject.$describeUrl) {
        window.open(selectedObject.$describeUrl, '_blank')
      }
    },

    onPreview: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      if (selectedObject.$previewUrl) {
        window.open(selectedObject.$previewUrl, '_blank')
      }
    },

    onStateChange: function () {
      this.base(arguments)
      this.init(app.utilisateur.ressourcesFolders)
    },

    /* ************************************************************
    ATTENTION, pour chaque ressource, on a rid ET $id
    Pour une ressource qui n'est pas un alias, les deux sont identiques.
    Pour un alias, $id est le rid de l'alias et rid désigne la ressource d'origine)

    À la construction de l'arbre on autorise plusieurs rid identique (dans la sesathèque on peut créer plusieurs alias de la même ressource)
      => on utilise $id comme critère pour need
   Au drop ou à l'import on interdit une cible déjà présente
      => on teste l'existence de rid
     ************************************************************ */

    decorateTree: function (index, callback) {
      app.cachedGetListPerso((error, ressources) => {
        if (error) return callback && callback(error)
        for (const ressource of ressources) {
          ressource.$behavior = new sesalab.behaviors.Ressource(ressource)
          let parent
          if (ressource.aliasRid && index[ressource.aliasRid]) {
            parent = index[ressource.aliasRid]
          } else if (index[ressource.rid]) {
            // avant le 2024-11-12 c'était indexé sur le rid, mais on ne veut pas que tous les alias rangés avant cette date se retrouvent en non triés
            parent = index[ressource.rid]
          } else {
            // pas trouvé de dossier parent, cette ressource ira dans non-trié
            if (!this.unsorted) {
              this.unsorted = this.getDefaultFolder()
              this.unsorted.data.unsorted = true
            }
            parent = this.unsorted
          }

          this.needResourceNode(parent, ressource)
          this.tree.refresh()
        }
        if (callback) callback()
      })
    },

    _onSelectionChanged: function (selection) {
      // si la sélection est vide (au blur), on quitte sans rien dire,
      // inutile de changer les droits sur un menu déroulant pas déroulable
      if (selection.length === 0) return
      const b = sesalab.behaviors
      const firtElt = selection[0]
      const { data } = firtElt
      const uniqSelection = selection.length === 1
      const isUniqFolder = uniqSelection && sesalab.widgets.Tree.isFolder(firtElt)
      const isSerie = uniqSelection && firtElt.instanceOf(b.Ressource) && data.type === 'serie'
      const isUniqResource = uniqSelection && firtElt.instanceOf(b.Ressource)
      const isUniqAlias = isUniqResource && data.aliasRid
      const isOpen = uniqSelection && !!app.findPageByPageId(getRessourcePageId(data))

      this.buttonEnabled('onAddResource', isUniqFolder)
      this.buttonEnabled('onImportResource', isUniqFolder)

      // on autorise pas l'effacement sur des sélections multiples
      // (trop risqué et inutile dans la vraie vie)
      const isDeletable = isUniqResource && Boolean(data.$deleteUrlApi)
      this.buttonEnabled('onDeleteResource', isDeletable)

      this.buttonEnabled('onTestResource', isUniqResource && !isOpen && !isSerie)

      const isEditable = isUniqResource && !isSerie && !isOpen && Boolean(data.$editUrl)
      this.buttonEnabled('onDuplicateResource', isEditable && !isUniqAlias)
      this.buttonEnabled('onModifyAlias', isEditable && isUniqAlias)
      this.buttonEnabled('onModifyResource', isEditable && !isUniqAlias)
      this.buttonEnabled('onShowInformations', isUniqResource && data.$describeUrl)
      this.buttonEnabled('onPreview', isUniqResource && data.$previewUrl)
      this.buttonEnabled('onNotify', !isUniqFolder && data.rid)
    },

    /**
     * Drop le contenu d'un arbre dans un dossier
     * @param targetFolder
     * @param sourceTree
     */
    addTreeInFolder: function (targetFolder, sourceTree) {
      sourceTree.getChildren().forEach((node) => {
        if (!node.data || !node.data.$behavior) return
        if (node.data.$behavior.constructor === sesalab.behaviors.Arbre) {
          this.addTreeInFolder(targetFolder, node)
        } else if (node.data.$behavior.constructor === sesalab.behaviors.Ressource) {
          const ressource = node.data
          if (targetFolder.exists({ rid: ressource.rid })) return
          const folderNode = targetFolder.data
          this.needResourceNode(folderNode, ressource)
          this.tree.openNode(folderNode)
          folderNode.ressources.push(ressource)
        } else {
          console.error(Error('node qui n’est ni un arbre ni une ressource'), node)
        }
      })
    },

    /**
     * Ajoute un alias de resource sur la sésathèque commun
     * Appelé au drop d'une ressource ou sur l'action Dupliquer
     * @param {sesalab.behaviors.Ressource} resource
     * @param folderNode
     * @param cb
     */
    addClonedResource: function (resource, folderNode, cb) {
      // on ne vérifie pas ici si le rid existe déjà, car c'est normal si on duplique
      // (pour le drop c'est vérifié en amont)
      app.sesatheque.cloneItem(app.sesatheque.baseIdPrivate, resource, (error, newItem) => {
        if (error) {
          console.error(error)
          return app.toaster.toast(error.message, 'error')
        }
        app.ressStore.put(newItem)
        const node = this.addToFolder(folderNode, newItem)
        this.store(() => {
          // enregistrement ok
          app.toaster.toast(`Alias de « ${resource.titre} » créé dans le dossier « ${sesalab.widgets.Tree.getClearPath(folderNode)} »`, 'info', 3)
          if (cb) cb(node)
        })
      })
    },

    exportSerie: function (folderNode, serie) {
      sesalab.widgets.InputDialog.execute(`Export vers "${PANEL_MES_RESSOURCES_TITLE}"`, 'Nom de la nouvelle ressource', null, (titre) => {
        const data = {
          titre,
          parametres: serie
        }
        app.sesatheque.saveSerie(app.sesatheque.baseIdPrivate, data, (error, newItem) => {
          if (error) {
            console.error(error)
            return app.toaster.toast(error.message, 'error')
          }
          this.addToFolder(folderNode, newItem)
          this.store(() => {
            app.toaster.toast('La série a été exportée')
          })
        })
      })
    },

    /**
     * Ajoute ressource dans folderNode (et ressStore) et retourne le node ainsi créé (ou l'existant s'il y était déjà)
     * @param folderNode
     * @param ressource
     * @returns {sesalab.widgets.TreeNode}
     */
    addToFolder: function (folderNode, ressource) {
      ressource.$behavior = new sesalab.behaviors.Ressource(ressource)
      const node = this.needResourceNode(folderNode, ressource)
      this.tree.openNode(folderNode)
      return node
    },

    /**
     * Retourne l'action faisable suivant source et target
     * @param source
     * @param target
     * @return {string|undefined} undefined en cas de source|target invalide (toaster déclenché dans ce cas)
     */
    eventToAction: function (source, target) {
      if (!source || !source.data || !source.data.$behavior || !target) {
        return app.toaster.toast('Données invalides, impossible de déterminer les actions possibles')
      }
      const b = sesalab.behaviors
      const sourceClass = source.data.$behavior.constructor
      if (sourceClass === b.Serie && target === b.Dossier) return 'serie-dossier'
      if (sourceClass === b.Sequence && target === b.Dossier) return 'sequence-dossier'
      if (sourceClass === b.Ressource && target === b.Dossier) return 'ressource-dossier'
      if (sourceClass === b.Arbre && target === b.Dossier) return 'arbre-dossier'
      if (sourceClass === b.Ressource && target === b.Ressource) return 'ressource-ressource'
    },

    onDrop: function (e) {
      // appelle le onDrop de FolderPanel, s'il retourne true il a géré ça et on arrête là
      if (this.base(arguments, e)) return

      let panel, serie
      for (const source of e.sources) {
        // une string source-destination
        const action = this.eventToAction(source, e.targetClass)
        switch (action) {
          case 'serie-dossier': {
            try {
              // FIXME ça marche plus du tout en 2024-11, depuis très longtemps…
              panel = e.sourceComponent.getLayoutParent().getLayoutParent().getLayoutParent()
              serie = panel.exportSerie(source)
              this.exportSerie(e.target, serie)
              return true
            } catch (error) {
              console.error(error)
            }
            return false
          }

          case 'ressource-dossier': {
            const ressource = source.data
            const folderNode = e.target
            // ressource qui vient d'un autre arbre (sinon déjà traité par le onDrop parent)
            const root = folderNode.getRoot()
            // on gère l'unicité d'après le rid de la ressource originale
            if (root.exists({ rid: ressource.rid }, false, true)) {
              return
            }
            // elle n'est pas déjà dans cet arbre, on peut créer l'alias
            this.addClonedResource(ressource, folderNode)
            return true
          }

          case 'arbre-dossier':
            this.addTreeInFolder(e.target, source)
            return true

          case 'ressource-ressource':
            if (source.getParent() !== e.target.getParent()) return

            source.getSiblings().remove(source)
            if (e.position === 'before') {
              e.target.getSiblings().insertBefore(e.target, source)
            } else {
              e.target.getSiblings().insertAfter(e.target, source)
            }
            return true
        }
      }
    },

    onTestResource: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      app.testResource(selectedObject)
    },

    onDeleteResource: function () {
      const node = this.tree.getSelectedNode()
      if (!node) return console.error(Error('Aucune ressource sélectionnée'))
      sesalab.widgets.ConfirmationDialog.execute('Voulez-vous vraiment supprimer cette ressource ?', () => {
        this.deleteNodes([node])
      })
    },

    /**
     * Ajoute ressource dans folderNode (et dans ressStore)
     * @param {sesalab.widgets.TreeNode} folderNode
     * @param {ClientItem} ressource
     * @returns {TreeNode|*}
     */
    needResourceNode: function (folderNode, ressource) {
      if (!folderNode) folderNode = this.getDefaultFolder()
      app.ressStore.put(ressource)
      return folderNode.need(ressource, { $id: ressource.$id })
    },

    /**
     * Retourne un folder (le folder par défaut si on trouve pas mieux)
     * @param {Event|sesalab.behaviors.Dossier|sesalab.behaviors.Ressource} folder
     * @returns {sesalab.behaviors.Dossier}
     * @private
     */
    _enforceFolder: function (folder) {
      const { isFolder } = sesalab.widgets.Tree
      // parentFolder peut être un event
      if (!isFolder(folder)) {
        folder = this.tree.getSelectedNode()
        // si la sélection est une ressource, on tente son dossier parent
        if (!isFolder(folder) && folder?.parentNode) folder = folder.parentNode
      }
      // On peut ne rien récupérer (clic sur le bouton ajouter une ressource sans sélection de dossier), on prend le dossier par défaut
      if (!isFolder(folder)) folder = this.getDefaultFolder()
      return folder
    },

    /**
     * Crée une ressource dans le dossier sélectionné (ouvre le form sesatheque en iframe)
     * @param {qx.event.type.Event|sesalab.behaviors.Dossier} parentFolder
     */
    onAddResource: function (parentFolder) {
      const closerCallback = (error, newItem) => {
        if (error) return app.toaster.toast(error.message, 'error')
        app.showLoader()
        folder.data.ressources.push(newItem.$id)
        this.addToFolder(folder, newItem)
        app.removePage(page)
        this.store()
        this.tree.refresh()
        this.tree.openNode(folder)
      }

      const folder = this._enforceFolder(parentFolder)
      const url = app.sesatheque.addItem(app.sesatheque.baseIdPrivate, closerCallback)
      const page = app.openIframePage('Nouvelle ressource', 'lab', url, 'mes-ressources-lab')
    },

    /**
     * Importe une ressource dans le dossier sélectionné
     * (bouton importer qui demande de saisir un rid)
     * @param {qx.event.type.Event|sesalab.behaviors.Dossier} parentFolder
     */
    onImportResource: function (parentFolder) {
      askImportRid((rid) => {
        // on vérifie que cette ressource n'est pas déjà dans notre arbre
        const folder = this._enforceFolder(parentFolder)
        const root = folder.getRoot()
        // on chercher d'après rid et pas $id, pour bloquer l'import si on a déjà un alias vers ce rid
        if (!root.exists({ rid }, false, true)) {
          // on peut lancer le clone (createAlias, ça gère l'erreur éventuelle et fait le store de notre arbre sinon)
          this.addClonedResource({ rid }, folder)
        }
      })
    },

    /**
     * Duplique et modifie une ressource (cumule createAlias et forkAlias)
     * @param {sesalab.widgets.TreeNode} node La ressource à cloner
     * @param {sesalab.widgets.TreeNode} parentFolder Le dossier où mettre le clone
     * @returns {qx.ui.container.Composite}
     */
    onDuplicateResource: function (node, parentFolder) {
      // node est un event sur l'action du clic droit, mais on peut être appelé par Ressources
      if (!node?.data?.type) node = this.tree.getSelectedNode()
      if (!node?.data) {
        console.error(Error('onDuplicateResource sans node.data'), node)
        return app.toaster.toast('Aucune ressource à dupliquer')
      }
      if (!parentFolder) parentFolder = this._enforceFolder(node.parentNode)
      this.addClonedResource(node.data, parentFolder, (newNode) => {
        this.onModifyResource(newNode)
      })
    },

    onModifyAlias: function (node) {
      this.onModifyResource(node)
    },

    onModifyResource: function (node) {
      // attention, node peut être un event ou un node passé directement
      if (!node?.data?.type) node = this.tree.getSelectedNode()
      if (!node?.data?.type) {
        console.error(Error('onModifyResource sans node.data'), node)
        return app.toaster.toast('Aucune ressource à modifier')
      }
      const resource = node.data
      let page

      const closerCallback = (error, newItem) => {
        if (error) return app.toaster.toast(error.message, 'error')
        // le rid change en cas de modif d'alias (qui n'en est alors plus un)
        // on reset tout
        newItem.$behavior = new sesalab.behaviors.Ressource(newItem)
        node.data = newItem
        node.setCaption(newItem.titre)
        // on met à jour la ressource dans le store
        app.ressStore.put(newItem)
        const folder = node.parentNode
        let msg = 'Vos modifications ont été sauvegardées'
        if (folder?.data?.nom) {
          msg += `\nLa ressource « ${newItem.titre} » est dans le dossier « ${sesalab.widgets.Tree.getClearPath(folder)} »`
        } else {
          console.warn('Pas trouvé le dossier parent de cette ressource', node)
        }
        app.toaster.toast(msg)
        app.removePage(page)
        this.store()
        this.tree.refresh()
        // @todo mettre à jour le menu contextuel (sur les alias), le refresh ne le fait pas
        if (folder) this.tree.openNode(folder)

        // faut màj les séquences qui utilisent cette ressource si
        // - c'était un alias avant modif
        // - le titre a changé
        // - la clé de lecture a changé (statut public modifié)
        const wasAlias = Boolean(resource.aliasRid)
        const hasVisibilityChange = Boolean(resource.cle) !== Boolean(newItem.cle)
        if (wasAlias || resource.titre !== newItem.titre || hasVisibilityChange) {
          const url = `sequences-update/${resource.rid}`
          app.sesalab.put(url, { newItem }, {}, (error, result) => {
            const label = wasAlias ? 'cet alias' : 'cette ressource'
            if (error) {
              console.error(error)
              return app.toaster.toast(`La mise à jour des séquences qui contenaient ${label} a échoué : ${error.message}`, 'error')
            }
            if (result?.nbMod == null) {
              console.error(Error(`L’appel de ${url} ne remonte pas l’objet attendu (pas de nbMod)`), result)
              return app.toaster.toast(`La mise à jour des séquences qui contenaient ${label} ne répond pas au format attendu, échec probable`, 'error')
            }
            if (result.nbMod) {
              let msg = result.nbMod === 1
                ? `Mise à jour de l’unique séquence qui contenait ${label}`
                : `Mise à jour de ${result.nbMod} séquence qui contenaient ${label}`
              if (wasAlias) msg += ' (pour pointer vers cette nouvelle ressource)'
              app.toaster.toast(msg)
            } else {
              app.toaster.toast('Aucune séquence ne contenait cette ressource (pas de mise à jour à faire)')
            }
          })
        }
      }

      app.sesatheque.modifyItem(resource, closerCallback, function (error, src) {
        if (error) return app.toaster.toast(error.message, 'error')
        page = app.openIframePage(resource.titre, 'lab', src, getRessourcePageId(resource))
      })
    },

    /**
     * Appelée par onDeleteResource mais aussi par FolderPanel au moment de la suppression d'un dossier
     * @param {sesalab.widgets.TreeNode[]} nodes
     */
    deleteNodes: function (nodes) {
      let nbCbCalls = 0
      for (const node of nodes) {
        app.sesatheque.deleteItem(node.data, (error) => {
          nbCbCalls++
          if (error) {
            console.error(error, 'avec le node', node)
            app.toaster.toast(error.message, 'error')
          } else {
            // si c'est un alias c'est lui qu'il faut virer (mais pas l'original)
            const rid = node.data.aliasRid || node.data.rid
            app.ressStore.remove(rid)
            node.remove()
          }
          if (nbCbCalls === nodes.length) this.tree.refresh()
          // sinon on attend le dernier
        })
      }
    },

    store: function (next) {
      if (!this.tree.isEnabled()) return
      app.utilisateur.patchFolders(
        { ressourcesFolders: this.serialize() },
        app.errorHandler(`Une erreur est survenue pendant la sauvegarde du panneau "${PANEL_MES_RESSOURCES_TITLE}"`, () => {
          app.hideLoader()
          if (next) next()
        })
      )
    }
  }
})
