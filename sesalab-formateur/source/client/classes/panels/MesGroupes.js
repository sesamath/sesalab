const _ = require('lodash')
const flow = require('an-flow')

qx.Class.define('sesalab.panels.MesGroupes', {
  extend: sesalab.widgets.FolderPanel,

  /**
   * Constructeur
   * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
   */
  construct: function (group) {
    this.base(arguments, group, 'Mes Groupes', {})
  },

  members: {
    init: function (folders) {
      const sorter = (a, b) => a.getCaption().toLowerCase().localeCompare(b.getCaption().toLowerCase())
      if (this.initFolderPanel(folders, 'groupes', { sorted: sorter })) {
        this.tree.addListener('pointerover', function (e) { app.displayEleveInformation(e, this) }, this)
        this.barButton('onAddGroupeFromMenu', 'Créer un groupe dans le dossier selectionné', 'add_people', 'groupe-creation')
        this.menuButton('onAddGroup', 'Ajouter un groupe', 'add_people')
        this.menuButton('onAddStudents', 'Ajouter des élèves', 'add_people')
        this.menuButton('onExportBilans', 'Exporter les bilans', 'combo_chart')
        this.menuButton('onDeleteGroup', 'Mettre dans la corbeille', 'empty_trash')
        this.menuButton('onDuplicateGroup', 'Dupliquer le groupe', 'database_duplicate')
        this.menuButton('onModifyGroup', 'Modifier', 'edit', true)
        this.menuButton('onShowStudentBilan', 'Afficher les bilans', 'combo_chart', true)
        this.menuButton('onDeleteStudents', 'Retirer les élèves sélectionnés', 'empty_trash')
        this.getContentElement().setAttribute('id', 'panel-mes-groupes')
        this.createLoader()

        app.bus.on('mesgroupes::refresh', () => {
          this.tree.refresh()
        })
      }
    },

    onStateChange: function () {
      this.base(arguments)
      this.init(app.utilisateur.groupeFolders)
    },

    decorateTree: function (index, callback) {
      const self = this
      flow().seq(function () {
        app.sesalab.get('groupes/by-owner', {}, this)
      }).seq(function (response) {
        _.each(response.groupes, (groupe, gid) => {
          let parent = index[gid]
          // si y'a aucun dossier on en crée un
          if (!parent) parent = self.getDefaultFolder()
          const group = sesalab.behaviors.Groupe.create(groupe)
          const groupNode = self.needGroupNode(parent, group)
          _.each(groupe.utilisateurs, (eleve) => {
            eleve.$behavior = new sesalab.behaviors.Eleve(eleve)
            self.needStudentNode(groupNode, eleve)
          })
          // on veut afficher l'effectif après le nom
          self.refreshGroupCaption(groupNode)
        })

        callback()
      }).catch(function (error) {
        app.toaster.toast(error.message, 'error')
        if (typeof error === 'string') console.error(new Error(error))
        else console.error(error)
        callback()
      })
    },

    onAddGroupeFromMenu: function () {
      const selectedFolder = this.tree.getSelectedNode()
      if (selectedFolder && selectedFolder.instanceOf(sesalab.behaviors.Dossier)) {
        this.onAddGroup(selectedFolder)
      } else {
        const nonTrieFolder = this.getDefaultFolder()
        this.onAddGroup(nonTrieFolder)
      }
    },

    /**
     * Réponse au changement de sélection de l'arbre.
     */
    _onSelectionChanged: function (selection) {
      const b = sesalab.behaviors
      const uniqSelection = selection.length === 1
      const isFolder = uniqSelection && selection[0].instanceOf(b.Dossier)
      const isGroup = uniqSelection && selection[0].instanceOf(b.Groupe)
      const isStudent = uniqSelection && selection[0].instanceOf(b.Eleve)

      this.buttonEnabled('onAddGroup', isFolder)
      this.buttonEnabled(['onAddStudents', 'onExportBilans', 'onDuplicateGroup', 'onModifyGroup'], isGroup)
      this.buttonEnabled('onShowStudentBilan', isStudent)
      this.buttonEnabled('onDeleteGroup', selection.length && this.tree.isCollectionFullOf(selection, sesalab.behaviors.Groupe))
      this.buttonEnabled('onDeleteStudents', selection.length && this.tree.isCollectionFullOf(selection, sesalab.behaviors.Eleve))
    },

    eventToAction: function (source, target) {
      if (!source || !source.data) return ''
      const b = sesalab.behaviors
      const sourceClass = source.data.$behavior.constructor
      if (sourceClass === b.Eleve && target === b.Eleve) return 'eleve-eleve'
      if (sourceClass === b.Eleve && target === b.Groupe) return 'eleve-groupe'
      if (sourceClass === b.Groupe && target === b.Groupe) return 'groupe-groupe'
    },

    onDrop: function (e) {
      if (this.base(arguments, e)) return

      let sourceGroupe, targetGroupe, groupNode, sourceId, targetId, eleve, duplicate
      _.each(e.sources, (source) => {
        switch (this.eventToAction(source, e.targetClass)) {
          // Il est possible de réordonner les élèves dans les groupes (pas de tri alphabétique)
          case 'eleve-eleve':
            if (source === e.target) return
            sourceGroupe = source.parentNode.data
            targetGroupe = source.parentNode.data
            // Les élèves doivent être dans le même groupe pour pouvoir être réordonnés
            if (sourceGroupe !== targetGroupe) return
            groupNode = source.parentNode
            sourceId = source.data.oid
            targetId = e.target.data.oid
            if (!sourceGroupe.utilisateurs || !sourceGroupe.utilisateurs.length) {
              console.error('drop d’un groupe vide ignoré')
              return
            }
            // On enlève l'élève droppé
            sourceGroupe.utilisateurs.splice(sourceGroupe.utilisateurs.indexOf(sourceId), 1)
            // On ajoute l'élève droppé au-dessus de l'élève ciblé
            sourceGroupe.utilisateurs.splice(sourceGroupe.utilisateurs.indexOf(targetId), 0, sourceId)
            app.sesalab.put('groupe', sourceGroupe, {}, (error, response) => {
              if (error) {
                app.toaster.toast(error.message, 'error')
                console.error(error)
                return
              }
              // On supprime tous les behaviors des élèves pour les reconstruire après avec le nouvel ordre
              _.each(_.clone(groupNode.getChildren().toArray()), (child) => {
                child.remove()
                child.data.$behavior.remove()
              })
              _.each(response.utilisateurs, (eleve) => {
                eleve.$behavior = new sesalab.behaviors.Eleve(eleve)
                const studentNode = this.needStudentNode(groupNode, eleve)
                this.tree.openNode(studentNode)
              })
              this.tree.refresh()
            })
            return true

          case 'eleve-groupe':
            // drop d'un élève dans un groupe
            eleve = source.data
            // s'il y est déjà on fait rien
            if (e.target.exists({ oid: eleve.oid })) return
            sourceGroupe = source.parentNode.data
            targetGroupe = e.target.data

            // Mise à jour de la cible
            if (!targetGroupe.utilisateurs) targetGroupe.utilisateurs = []
            targetGroupe.utilisateurs.push(eleve.oid)

            app.sesalab.put('groupe', targetGroupe, {}, (error, response) => {
              if (error) {
                app.toaster.toast(error.message, 'error')
                console.error(error)
                return
              }

              // Lorsqu'on change un élève de groupe, alors on le supprime de son groupe initial
              if (sourceGroupe.isClass === false) {
                // màj du treeNode puis des data, faut 2 appels
                source.remove()
                sourceGroupe.$behavior.removeEleve(eleve)
                app.sesalab.put('groupe', sourceGroupe, {}, (error) => {
                  if (error) {
                    app.toaster.toast(error.message, 'error')
                    console.error(error)
                  }
                })
              }

              // Ajout UI
              eleve.$behavior = new sesalab.behaviors.Eleve(eleve)
              const studentNode = this.needStudentNode(e.target, eleve)
              this.tree.openNode(studentNode)
              this.tree.refresh()
              // et refresh des titres de nos groupes
              this.refreshGroupCaption(e.target)
              this.refreshGroupCaption(source.parentNode)
            })
            return true

          case 'groupe-groupe':
            if (source.data.oid === undefined) {
              app.toaster.toast('Cet élément ne peut pas être utilisé dans les groupes', 'error')
              return
            }
            duplicate = source.data.structure != null // On duplique les élèves pour une classe
            if (this.moveChildrenTo(source, e.target, duplicate)) {
              // @todo Only one request
              // On met à jour le groupe target
              const self = this
              app.sesalab.put('groupe', e.target.data, {}, (error, response) => {
                // @todo Use app.errorHandler
                if (error) {
                  app.toaster.toast(error.message, 'error')
                  console.error(error)
                  return
                }
                self.refreshGroupCaption(e.target)
                // On met à jour le groupe source
                app.sesalab.put('groupe', source.data, {}, (error) => {
                  if (error) {
                    app.toaster.toast(error.message, 'error')
                    console.error(error)
                    return
                  }
                  self.refreshGroupCaption(source)
                })
              })
            }
            return true
        }
      })
    },

    onAddStudents: function () {
      const node = this.tree.getSelectedNode()
      if (!node) {
        console.error(new Error('Selection invalide (node null)'))
        return
      }

      sesalab.dialogs.Eleves.execute(app.structure, (selection) => {
        // Récupère les élèves dans les classes et en dehors des classes
        const children = []
        for (let i = 0; i < selection.length; i++) {
          if (selection[i].instanceOf(sesalab.behaviors.Eleve)) {
            children.push(selection[i].data)
          } else {
            for (let j = 0; j < selection[i].data.utilisateurs.length; j++) {
              children.push(selection[i].data.utilisateurs[j])
            }
          }
        }

        // Met à jour la vue et les données
        children.forEach((child) => {
          child.$behavior = new sesalab.behaviors.Eleve(child)
          this.tree.openNode(this.needStudentNode(node, child))
        })
        node.data.utilisateurs = node.data.utilisateurs.concat(children)
        app.sesalab.put('groupe', node.data, {}, app.errorHandler('Une erreur s’est produite lors de la mise à jour du groupe'))
        this.tree.refresh()
      })
    },

    onDeleteGroup: function () {
      const nodes = this.tree.getSelectedNodes()
      if (nodes.length === 0) return

      sesalab.widgets.ConfirmationDialog.execute('Voulez-vous vraiment mettre les groupes séléctionnés dans la corbeille ?', () => {
        this.deleteNodes(nodes)
      })
    },

    onDuplicateGroup: function () {
      const node = this.tree.getSelectedNode()
      if (!node) {
        console.error(new Error('Selection invalide (node null)'))
        return
      }

      sesalab.widgets.InputDialog.execute('Dupliquer le groupe', 'Nom', node.data.nom, (value) => {
        let group = {
          nom: value,
          niveau: node.data.niveau,
          owner: app.utilisateur.oid,
          utilisateurs: node.data.utilisateurs
        }

        app.sesalab.put('groupe', group, {},
          app.errorHandler('Une erreur s’est produite lors de la mise à jour du groupe', (error, response) => {
            if (error) {
              console.error(error)
              app.toaster.toast(error.message, 'error')
              return
            }
            group.oid = response.groupe.oid
            group = sesalab.behaviors.Groupe.create(group)
            const groupNode = this.needGroupNode(node.parentNode, group)
            _.each(response.utilisateurs, (eleve) => {
              eleve.$behavior = new sesalab.behaviors.Eleve(eleve)
              this.needStudentNode(groupNode, eleve)
            })
            this.tree.openNode(groupNode)
            this.store()
            this.tree.refresh()
          })
        )
      })
    },

    onExportBilans: function () {
      const groupNode = this.tree.getSelectedNode()
      if (!groupNode) return app.toaster.toast('Impossible d’exporter l’élément séléctionné')
      app.exportGroupBilans(groupNode)
    },

    onDeleteStudents: function () {
      const nodes = this.tree.getSelectedNodes()
      if (!nodes.length) return app.toaster.toast('Impossible de supprimer les éléments séléctionnés')
      this.deleteNodes(nodes)
    },

    onShowStudentBilan: function () {
      const node = this.tree.getSelectedNode()
      if (!node) return app.toaster.toast('Aucun élève, impossible d’afficher son bilan')
      app.showStudentBilan(node.data)
    },

    moveChildrenTo: function (source, target, duplicate) {
      if (source === target) {
        return false
      }

      // Clone
      source.getChildren().forEach(function (child) {
        const studentNode = this.needStudentNode(target, child.data)
        this.tree.openNode(studentNode)
      }.bind(this))

      // ajout sans doublon
      const targetOids = target.data.utilisateurs.map(({ oid }) => oid)
      const usersToAdd = source.data.utilisateurs.filter(({ oid }) => !targetOids.includes(oid))
      target.data.utilisateurs = target.data.utilisateurs.concat(usersToAdd)

      // Remove original
      if (!duplicate) {
        const children = _.clone(source.getChildren().toArray())
        for (let i = 0; i < children.length; i++) {
          children[i].remove()
          children[i].data.$behavior.remove()
        }
        source.data.utilisateurs = []
      }

      this.tree.refresh()

      return true
    },

    grabByExternalId: function (behaviorClass, externalId) {
      function g (root) {
        if (root.instanceOf(behaviorClass) && root.data.externalId === externalId) return root.data
        let found = false
        root.getChildren().some(function (child) {
          found = g(child)
          return found // arrête la boucle some dès que c'est truthy
        })
        if (found) return found
      }
      return g(this.tree.model)
    },

    onAddGroup: function (parentFolder) {
      let parent = parentFolder || this.tree.getSelectedNode()
      if (!parent) {
        console.error(new Error('Selection invalide (parent null)'))
        return
      }

      // @TODO voir pourquoi il a fallu ajouter ça (2019-10-02)
      if (typeof parent.need !== 'function') {
        console.error(Error('Le parent récupéré n’est pas un dossier'), parent)
        parent = this.tree.getSelectedNode()
        if (!parent || typeof parent.need !== 'function') parent = this.getDefaultFolder()
      }

      sesalab.widgets.InputDialog.execute('Ajouter un groupe', 'Nom', null, (value) => {
        let group = {
          nom: value,
          owner: app.utilisateur.oid,
          utilisateurs: [],
          niveau: 0
        }

        app.sesalab.put('groupe', group, {},
          app.errorHandler('Une erreur s’est produite lors de l’ajout du groupe', (error, response) => {
            if (error) {
              console.error(error)
              app.toaster.toast(error.message, 'error')
              return
            }
            group.oid = response.groupe.oid
            group = sesalab.behaviors.Groupe.create(group)
            const groupNode = this.needGroupNode(parent, group)
            this.tree.openNode(groupNode)
            this.store()
            this.tree.refresh()
          })
        )
      })
    },

    onModifyGroup: function () {
      const groupe = this.tree.getSelectedObject()
      if (!groupe) return console.error(Error('aucun groupe à modifier'))
      sesalab.widgets.InputDialog.execute('Changer le nom du groupe', 'Nom', groupe.nom, (value) => {
        groupe.nom = value
        app.sesalab.put('groupe', groupe, {}, (error, response) => {
          if (error) {
            app.toaster.toast(error.message, 'error')
            return
          }
          groupe.$behavior.apply(response.groupe)
          this.store()
          this.tree.refresh()
        }, this)
      }, this)
    },

    needStudentNode: function (groupNode, eleve) {
      return groupNode.need(eleve, { oid: eleve.oid })
    },

    needGroupNode: function (folderNode, group) {
      return folderNode.need(group, { oid: group.oid })
    },

    // @todo il faudrait plutôt créer un Tree.refresh qui ferait ce refreshCaption si c'est un node.instanceOf(sesalab.behaviors.Groupe) puis le refresh de VirtualTree
    refreshGroupCaption: function (groupe) {
      if (typeof groupe.setCaption !== 'function') return console.error(Error('Item sans setCaption'), groupe)
      if (!groupe.data) return console.error(Error('Item sans data'), groupe)
      const nb = (groupe.data.utilisateurs && groupe.data.utilisateurs.length) || 0
      groupe.setCaption(`${groupe.data.nom} (${nb})`)
    },

    store: function () {
      if (!this.tree.isEnabled()) return
      app.utilisateur.patchFolders(
        { groupeFolders: this.serialize() },
        app.errorHandler('Une erreur est survenue pendant la sauvegarde du panneau "Mes groupes"')
      )
    },

    // Egalement appelée dans FolderPanel au moment de la suppression d'un dossier
    deleteNodes: function (nodes) {
      const groupsNodes = _.filter(nodes, (node) => {
        return node.instanceOf(sesalab.behaviors.Groupe)
      })
      const studentsNodes = _.filter(nodes, (node) => {
        return node.instanceOf(sesalab.behaviors.Eleve)
      })

      // Groupes
      const self = this
      flow(groupsNodes)
        .seqEach(function (node) {
          app.sesalab.del('groupe/' + node.data.oid, {}, (error, response) => {
            if (error) {
              app.toaster.toast(`Une erreur est survenue pendant la suppression de ${node.data.nom}`, 'error')
              return this()
            }

            node.remove()
            this()
          })
        })
        .seq(function () {
          app.bus.broadcast('corbeille::refresh')
          self.tree.refresh()
          self.store()
          this()
        })
        .catch(app.errorHandler('Une erreur s’est produite durant la suppression d’un élément'))

      // Élèves
      if (!studentsNodes.length) {
        return
      }

      let groupedStudents = _.groupBy(studentsNodes, (node) => {
        return node.parentNode.getCaption()
      })
      groupedStudents = _.toArray(groupedStudents)

      flow(groupedStudents)
        .seqEach(function (students) {
          // On supprime les éléments du model
          const parentNode = students[0].parentNode
          const studentsOids = _.compact(_.map(students, (node) => {
            return node.data ? node.data.oid : null
          }))

          parentNode.data.utilisateurs = parentNode.data.utilisateurs.filter((user) => !studentsOids.includes(user.oid))

          app.sesalab.put('groupe', parentNode.data, {}, this)
        })
        .seq(function () {
          _.each(studentsNodes, (node) => {
            node.remove()
          })

          this()
        })
        .seq(function () {
          self.tree.refresh()
          this()
        })
        .catch(app.errorHandler('Une erreur s’est produite durant la suppression des élèves'))
    }
  }
})
