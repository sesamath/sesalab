const { PANEL_MES_RESSOURCES_TITLE, PANEL_PRESSE_PAPIER_TITLE } = require('sesalab-commun/constants')

qx.Class.define('sesalab.panels.PressePapier', {
  extend: sesalab.widgets.CollapsablePanel,
  include: [sesalab.widgets.Helper],

  /**
   * Constructeur
   * Le presse papier écoute les postMessage de la sésathèque pour mémoriser des ressources
   * repérées dans des résultats de recherche.
   * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
   */
  construct: function (group) {
    this.base(arguments, group, PANEL_PRESSE_PAPIER_TITLE, {})

    // On écoute les messages externes venant de la sésathèque
    window.addEventListener('message', (messageEvent) => {
      if (messageEvent.type !== 'message' || messageEvent.data.action !== 'ressource-copy') return
      app.sesatheque.getItem(messageEvent.data.rid, (error, ressource) => {
        if (error) {
          console.error(error)
          return
        }

        // Création du noeud
        ressource.$behavior = new sesalab.behaviors.Ressource(ressource)
        this.addNode(ressource)
        this.buttonEnabled('onReset', true)
        this.tree.refresh()
        this.setValue(true)
      })
    })

    // et init de l'arbre vide (pour virer le spinner chargement)
    this.init()
  },

  members: {
    init: function () {
      // rien à faire si l'initialisation a déjà eu lieu
      if (this.tree) return

      // bouton vider (dans toolbar), à mettre avant la construction
      this.barButton('onReset', 'Vider le presse-papier', 'empty_trash')

      // ajout de l'info
      this.infos = new qx.ui.basic.Atom('xx', 'icons/20/info.png')
      this.infos.set({
        label: `Le lien copier dans les résultats de recherche mettra ici de coté la ressource, vous permettant ensuite de la faire glisser dans une séquence ou dans "${PANEL_MES_RESSOURCES_TITLE}".`,
        decorator: 'tooltip',
        padding: 5,
        rich: true,
        selectable: true
      })
      this.add(this.infos)

      // Construction de l'arborescence
      this.tree = new sesalab.widgets.Tree()
      this.tree.set({ hideRoot: true, showTopLevelOpenCloseIcons: true })

      // ajout de l'arbre dans le conteneur
      this.add(this.tree, { flex: 1 })

      // double clic => défaut
      this.tree.addListener('dblclick', this.fireDefaultAction, this)

      // infos sur hover
      this.tree.addListener('pointerover', function (e) { app.displayRessourceInformation(e, this) }, this)

      // actions au clic droit
      this.menuButton('onTest', 'Tester', 'process', true)
      this.menuButton('onShowInformations', 'Plus d’informations', 'about')
      this.menuButton('onPreview', 'Aperçu dans un autre onglet', 'external')
      // on vire le spinner mis par défaut
      this.onReset()
    },

    onReset: function () {
      this.tree.clean()
      this.tree.removeAll()
      this.buttonEnabled('onReset', false)
    },

    onSelectionChanged: function (selection) {
      const uniqSelection = selection.length === 1
      const isRessource = uniqSelection && selection[0].instanceOf(sesalab.behaviors.Ressource)
      const { data } = selection.length ? selection[0] : {}
      this.buttonEnabled('onTest', isRessource)
      this.buttonEnabled('onShowInformations', isRessource && data.$describeUrl)
      this.buttonEnabled('onPreview', isRessource && data.$previewUrl)
    },

    onTest: function (e) {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      app.testResource(selectedObject)
    },

    onShowInformations: function (e) {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      if (selectedObject.$describeUrl) {
        window.open(selectedObject.$describeUrl, '_blank')
      }
    },

    onPreview: function (e) {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      if (selectedObject.$previewUrl) {
        window.open(selectedObject.$previewUrl, '_blank')
      }
    },

    addNode: function (ressource) {
      const existingNode = this.tree.model.getChildren().toArray().find(child => child.rid === ressource.rid)
      if (existingNode) return existingNode
      return this.tree.model.need(ressource, { rid: ressource.rid })
    }
  }
})
