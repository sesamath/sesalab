const flow = require('an-flow')

// dans node on prendrait plutôt crypto.randomUUID() (https://nodejs.org/docs/latest-v20.x/api/crypto.html#cryptorandomuuidoptions)
// (il est nettement meilleur en perfs, cf https://medium.com/@matynelawani/uuid-vs-crypto-randomuuid-vs-nanoid-313e18144d8c)
// mais dans le navigateur c'est trop récent (https://developer.mozilla.org/fr/docs/Web/API/Crypto/randomUUID)
// => on garde notre module maison, vieux et très ligth mais suffisant pour le besoin d'identification des ressources dans l'arbre
const uuid = require('an-uuid')

qx.Class.define('sesalab.panels.Ressources', {
  extend: sesalab.widgets.CollapsablePanel,
  include: [sesalab.widgets.Helper],

  /**
   * Constructeur
   * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
   */
  construct: function (group) {
    this.base(arguments, group, 'Ressources', {})
  },

  members: {
    /**
     * Durée du cache en secondes (default: 24h)
     */
    CACHE_DURATION: 86400,

    init: function () {
      // L'initialisation a déjà eu lieu
      if (this.tree) {
        return
      }

      // Barre d'outils
      this.barButton('onSearchResource', 'Rechercher une ressource', 'search')
      this.buttonEnabled('onSearchResource', true)

      // Construction de l'arbo
      this.tree = new sesalab.widgets.Tree()
      this.tree.set({ hideRoot: true, showTopLevelOpenCloseIcons: true })
      this.tree.addListener('dragstart', function (e) { e.addAction('copy') })
      this.tree.addListener('open', this.onOpenTree, this)
      this.tree.addListener('pointerover', function (e) { app.displayRessourceInformation(e, this) }, this)
      this.addListener('dblclick', function () { this.fireDefaultAction() }, this)
      this.add(this.tree, { flex: 1 })

      // Menu contextuel
      this.menuButton('onTestResource', 'Tester la ressource', 'start', true)
      this.menuButton('onModifyResource', 'Personnaliser', 'database_duplicate')
      this.menuButton('onShowInformations', 'Plus d’informations', 'about')
      this.menuButton('onPreview', 'Aperçu dans un autre onglet', 'external')
      this.menuButton('onNotify', 'Signaler une anomalie', 'support')
      this.getContentElement().setAttribute('id', 'panel-ressources')

      this.reload()
      this.createLoader()
    },

    onNotify: function (e) {
      // un ClientItem
      const selectedObject = this.tree.getSelectedObject()
      const rid = selectedObject && selectedObject.rid
      sesalab.dialogs.Contact.execute({ rid })
    },

    onSearchResource: function () {
      const url = app.sesatheque.getSmartSearchUrl(app.sesatheque.baseIdGlobal)
      app.openIframePage('Rechercher une ressource (Public)', 'search', url, 'recherche-sesatheque-public')
    },

    onStateChange: function () {
      this.base(arguments)
      this.init()
    },

    reload: function () {
      const self = this
      this.tree.clean()
      this.tree.removeAll()
      if (!Array.isArray(app.selectedRessourceRids) || app.selectedRessourceRids.length === 0) {
        self.showLoader(false)
        return
      }

      flow(app.selectedRessourceRids)
        .seqEach(function (rid) {
          const nextSeq = this
          flow()
            .seq(function () {
              app.sesatheque.getItem(rid, this)
            })
            .done(function (err, enfant) {
              if (err) {
                app.errorHandler(`Impossible de charger l'arbre ayant pour rid: ${rid}`)(err)
              } else {
                self.processRessource(enfant)
              }
              // Même en cas d'échec on continue avec les autres noeuds
              nextSeq()
            })
        })
        .done(function (error) {
          if (error) console.error(error)
          self.showLoader(false)
          self.tree.refresh()
        })
    },

    processRessource: function (ressource, parent) {
      if (!ressource) return console.error(new Error('Ressource.processRessource appelé sans ressource'))
      const isArbre = ressource.type === 'arbre'
      const classe = isArbre ? sesalab.behaviors.Arbre : sesalab.behaviors.Ressource
      ressource.$behavior = new classe(ressource)
      ressource.hasChildren = isArbre
      // faut lui ajouter un id unique pour l'utiliser comme filtrage dans need()
      ressource.uid = ressource.rid || ressource.aliasOf || uuid()
      this.getNodeRessources(ressource, parent)
    },

    getNodeRessources: function (ressource, parent) {
      parent = parent || this.tree.model
      return parent.need(ressource, { uid: ressource.uid })
    },

    onOpenTree: function (e) {
      const invalidMsg = 'Données invalides, impossible de modifier l’affichage'
      if (!e) return app.toaster.toast(invalidMsg)
      const node = e.getData()
      if (!node || !node.instanceOf) return app.toaster.toast(invalidMsg)
      if (!node.instanceOf(sesalab.behaviors.Arbre)) {
        // on se retrouve ici au double clic sur une ressource (feuille)
        // ou bien après un clic droit sur une ressource et un clic gauche sur une autre
        // on sort silencieusement…
        return
      }
      const ressource = node.data

      // Évite une nouvelle création du noeud et de ses enfants une fois celui-ci chargé
      if (node.loaded) return

      app.sesatheque.getEnfants(ressource, (error, enfants) => {
        if (error) {
          node.failed()
          let msg = `Impossible de charger la branche ${ressource.titre}`
          console.error(msg, error)
          msg += ` : ${error.message}`
          app.toaster.toast(msg, 'error')
          return true
        }

        node.loaded = true
        node.clean()
        node.data.enfants = enfants
        for (const enfant of enfants) {
          this.processRessource(enfant, node)
        }
        this.tree.openNode(node)
      })
    },

    onSelectionChanged: function (selection) {
      const nodes = selection.filter(Boolean)
      if (nodes.length !== selection.length) console.error(Error('au moins un noeud de la sélection est null'), selection)
      if (nodes.length !== 1) return
      const node = selection[0]
      // node peut ne pas avoir de méthode instanceOf (c'est rare mais ça arrive, cf error bugsnag 67387f6841bcaf15ed1d497e)
      const isArbre = node.instanceOf?.(sesalab.behaviors.Arbre) ?? false
      const res = node.data
      const isEditable = !isArbre && app.sesatheque.isEditable(res.type)
      this.buttonEnabled('onTestResource', !isArbre)
      this.buttonEnabled('onModifyResource', isEditable)
      this.buttonEnabled('onShowInformations', res.$describeUrl)
      this.buttonEnabled('onPreview', res.$previewUrl)
      this.buttonEnabled('onNotify', !isArbre && res.rid)
    },

    /**
     * Action test d'une ressource (ou affichage pour l'élève ?)
     */
    onTestResource: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      app.testResource(selectedObject)
    },

    /**
     * Personnalise une ressource.
     * Appelle mesRessources.onDuplicateResource() qui va créer la copie dans son dossier par défaut (non trié).
     */
    onModifyResource: function () {
      const node = this.tree.getSelectedNode()
      if (!node?.data) return console.error(Error('Aucune ressource sélectionnée'))
      if (!app.sesatheque.isEditable(node.data.type)) return app.toaster.toast('Cette ressource n’est pas modifiable')
      // ça prendra le dossier courant si y'a qqchose de sélectionné dans Mes ressources
      // et sinon ça ira dans non trié
      const dstFolder = app.mesRessources._enforceFolder()
      app.mesRessources.onDuplicateResource(node, dstFolder)
    },

    onShowInformations: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      const ressource = app.sesatheque.normalize(selectedObject)
      if (ressource.$describeUrl) window.open(ressource.$describeUrl, '_blank')
    },

    onPreview: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      const ressource = app.sesatheque.normalize(selectedObject)
      if (ressource.$previewUrl) window.open(ressource.$previewUrl, '_blank')
    },

    bindItem: function (controller, item, index) {
      this.base(arguments, controller, item, index)
      controller.bindProperty('', 'open', {
        converter: function (value, model, source, target) {
          const isOpen = target.isOpen()
          return isOpen
        }
      }, item, index)
    }
  }
})
