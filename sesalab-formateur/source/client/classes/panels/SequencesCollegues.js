const _ = require('lodash')
const flow = require('an-flow')

const { DEFAULT_FOLDER_NAME, PANEL_SEQUENCES_COLLEGUES_TITLE } = require('sesalab-commun/constants')
const { extractSequencesCollegues } = require('sesalab-commun/utils')

/**
 * Panneau de gestion des séquences de mes collègues.
 */
qx.Class.define('sesalab.panels.SequencesCollegues', {
  extend: sesalab.widgets.SequencesPanel,

  /**
   * Constructeur
   * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
   */
  construct: function (group) {
    this.base(arguments, group, PANEL_SEQUENCES_COLLEGUES_TITLE, { deleteMessage: 'Supprimer' })
    this.createLoader()
  },

  members: {
    init: function (folders) {
      if (this.initFolderPanel(folders, 'sequencesCollegues', { sorted: 'asc' })) {
        this.authorButton = this.menuButton('onShowAuthor', 'Auteur :', 'person', true)
        this.menu.add(this.separator = new qx.ui.menu.Separator())
        this.menuButton('onOpenSequence', 'Éditer', 'database', true)
        this.menuButton('onTestSequence', 'Tester', 'start')
        this.menuButton('onShowResults', 'Voir le bilan', 'combo_chart')
        this.menuButton('onDuplicate', 'Dupliquer sans les élèves', 'database_duplicate')
        this.createLoader()
      }
    },

    decorateTree: function (index, callback) {
      const thisPanel = this

      flow().seq(function () {
        thisPanel.getAllSequences('sequences-collegues', [], this)
      }).seq(function (sequencesCollegues) {
        // les séquences qu'on a récupéré dans notre user
        for (const sequence of sequencesCollegues) {
          // Création dans l'arbre
          const parent = index[sequence.oid] || thisPanel.getDefaultFolder()
          thisPanel.needSequenceNode(parent, sequence)
        }
        // on regarde s'il faut mettre le user à jour, le serveur peut nous renvoyer des séquences partagées
        // qui n'étaient pas dans le user.sequenceColleguesFolders au départ
        const initialSeqs = extractSequencesCollegues(app.utilisateur.sequenceColleguesFolders).sort()
        const currentSeqs = sequencesCollegues.map(s => s.oid).sort()
        if (
          initialSeqs.length !== currentSeqs.length ||
          Array.from(initialSeqs.entries()).some(([i, oid]) => currentSeqs[i] !== oid)
        ) {
          thisPanel.store(true)
        }
        callback()
      }).catch(error => {
        // la cb fait juste du toast de l'erreur sans la sortir en console
        console.error(error)
        callback(error)
      })
    },

    // On a déclaré une entrée de menu pour afficher le nom de l'auteur,
    // il faut donc une callback du même nom, qui ne fait rien car c'est juste de l'affichage
    // (cliquer dessus va simplement fermer le menu contextuel)
    onShowAuthor: function () {},

    onDuplicate: function () {
      const sequenceSrc = this.tree.getSelectedObject()
      if (!sequenceSrc || !sequenceSrc.oid) return console.error(new Error('Impossible de dupliquer l’élement sélectionné (sequence invalide)'))
      let nom
      let sequence
      flow().seq(function () {
        const next = this
        sesalab.widgets.InputDialog.execute('Nom de la nouvelle séquence', 'Nom', sequenceSrc.nom, (nom) => next(null, nom))
      }).seq(function (_nom) {
        nom = _nom
        app.sesalab.get(`sequence/${sequenceSrc.oid}`, {}, this)
      }).seq(function (response) {
        sequence = response.sequence
        sequence.nom = nom
        // Supprime l'identifiant de la séquence
        delete sequence.oid
        // on s'approprie la séquence du collègue
        sequence.owner = app.utilisateur.oid
        // et on vide les élèves
        for (const sousSequence of sequence.sousSequences) {
          sousSequence.eleves = []
        }
        app.sesalab.put('sequence', sequence, {}, this)
      }).seq(function (response) {
        // séquence ok en bdd, on la met dans son folder avec son oid créé
        sequence.oid = response.sequence.oid
        sequence.publicAmis = false
        // faut l'ajouter à mesSequences, dans le dossier courant si y'en a un de sélectionné
        let parentFolder = app.mesSequences.tree.getSelectedNode()
        if (!parentFolder || !sesalab.widgets.Tree.isFolder(parentFolder)) {
          parentFolder = app.mesSequences.getDefaultFolder()
        }
        app.mesSequences.needSequenceNode(parentFolder, sequence)
        app.mesSequences.store(() => {
          app.toaster.toast(`La séquence a bien été dupliquée, vous la retrouverez dans ${app.mesSequences.getTitle()} / ${sesalab.widgets.Tree.getClearPath(parentFolder)}`)
          // et on ouvre l'arbre mes séquences
          app.mesSequences.toggleValue()
          app.mesSequences.tree.openNode(parentFolder)
        })
      }).catch(function (error) {
        console.error(error)
        app.toaster.toast(error, 'error')
      })
    },

    // Surcharge de la méthode présente dans FolderPanel
    onDeleteFolder: function () {
      const folderNode = this.tree.getSelectedNode()
      this.getPayloadNodes(folderNode, (error, children) => {
        if (error) {
          console.error(error)
          return app.toaster.toast(error.message, 'error')
        }

        const clonedChildren = _.clone(children)
        const msg = `Voulez-vous vraiment supprimer ce dossier ?<br>Les séquences à l'intérieur du dossier
          seront alors placées dans un dossier "${DEFAULT_FOLDER_NAME}"`
        sesalab.widgets.ConfirmationDialog.execute(msg, () => {
          // Bouge les enfants du dossier vers le dossier par défaut (Non trié)
          folderNode.remove()
          const parentFolder = app.sequencesCollegues.getDefaultFolder()
          _.each(clonedChildren, (child) => {
            parentFolder.need(child.data, { oid: child.data.oid })
          })

          this.tree.refresh()
          this.store()
        })
      })
    },

    _onSelectionChanged: function (selection) {
      const b = sesalab.behaviors
      const uniqSelection = selection.length === 1
      const isFolder = uniqSelection && selection[0].instanceOf(b.Dossier)
      this.separator.set({ visibility: isFolder ? 'excluded' : 'visible' })

      const sequence = uniqSelection && !isFolder && selection[0] ? selection[0].data : null
      if (sequence && sequence.$ownerFull) {
        this.authorButton.setLabel(`Partagée par : ${sequence.$ownerFull.prenom} ${sequence.$ownerFull.nom}`)
      }

      this.buttonEnabled(['onShowAuthor', 'onTestSequence', 'onShowResults', 'onDuplicate'], !isFolder)
      this.buttonEnabled(['onOpenSequence'], sequence && !isFolder && !!sequence.editableAmis)
    },

    store: function (force) {
      if (!force && !this.tree.isEnabled()) return
      const folders = { sequenceColleguesFolders: this.serialize() }
      const errorHandler = app.errorHandler(`Une erreur est survenue pendant la sauvegarde du panneau "${PANEL_SEQUENCES_COLLEGUES_TITLE}"`)
      app.utilisateur.patchFolders(folders, errorHandler)
    }
  }
})
