const _ = require('lodash')
const flow = require('an-flow')
const { PANEL_RESSOURCES_PARTAGEES_TITLE } = require('sesalab-commun/constants')

qx.Class.define('sesalab.panels.RessourcesPartagees', {
  extend: sesalab.widgets.CollapsablePanel,
  include: [sesalab.widgets.Helper],

  /**
   * Constructeur
   * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
   */
  construct: function (group) {
    this.base(arguments, group, PANEL_RESSOURCES_PARTAGEES_TITLE, {})
  },

  members: {
    init: function () {
      // rien à faire si l'initialisation a déjà eu lieu
      if (this.tree) return

      // Boutons
      this.barButton('onSearchResource', 'Rechercher une ressource', 'search')
      this.buttonEnabled('onSearchResource', true)

      // Construction de l'arborescence
      this.tree = new sesalab.widgets.Tree()
      this.tree.set({ hideRoot: true, showTopLevelOpenCloseIcons: true })
      // double clic => défaut
      this.tree.addListener('dblclick', this.fireDefaultAction, this)
      // infos sur hover
      this.tree.addListener('pointerover', function (e) { app.displayRessourceInformation(e, this) }, this)

      this.menuButton('onInstanciateSequenceModel', 'Instancier', 'add_database')
      this.menuButton('onTest', 'Tester', 'process')
      this.menuButton('onModifyResource', 'Modifier', 'edit', true)
      this.menuButton('onShowInformations', 'Plus d’informations', 'about')
      this.menuButton('onPreview', 'Aperçu dans un autre onglet', 'external')
      this.menuButton('onNotify', 'Signaler une anomalie', 'support')

      this.getContentElement().setAttribute('id', 'panel-ressources-partagees')
      this.createLoader()
      this.add(this.tree, { flex: 1 })

      this.buildTree()
    },

    onNotify: function (e) {
      // un ClientItem
      const selectedObject = this.tree.getSelectedObject()
      const rid = selectedObject && selectedObject.rid
      sesalab.dialogs.Contact.execute({ rid })
    },

    onSearchResource: function () {
      const url = app.sesatheque.getSmartSearchUrl(app.sesatheque.baseIdPrivate)
      app.openIframePage('Rechercher une ressource', 'search', url, 'recherche-sesatheque-private')
    },

    onStateChange: function () {
      this.base(arguments)
      this.init()
    },

    buildTree: function () {
      const self = this
      self.tree.clean()

      flow()
        .seq(function () {
          app.sesatheque.getListeAuteurs(app.sesatheque.baseIdPrivate, app.structure.getFormateurPids(), this)
        })
        .seq(function (items) {
          self.showLoader(false)

          const utilisateurPid = app.utilisateur.getPid()
          // On filtre pour enlever les valeurs comme 'success: true' et autres
          items = _.filter(items, (i) => {
            return i.pid && i.pid !== utilisateurPid
          })
          if (!items || items.length === 0) return this()

          const groupe = sesalab.behaviors.GroupePublic.create({ structure: app.structure })
          const groupeNode = self.tree.need(groupe)
          _.each(items, (item) => {
            // Pour chaque auteur on crée un noeud qui contiendra ses ressources
            const auteurData = sesalab.behaviors.GroupePublicAuteur.create({ name: item.label })
            const auteurDataNode = groupeNode.need(auteurData, { name: item.label })

            if (item.liste.length === 0) {
              auteurDataNode.clean()
            }

            _.each(item.liste, (ressource) => {
              const ressourceObject = self.createRessourceBehavior(ressource)
              auteurDataNode.need(ressourceObject, { rid: ressourceObject.rid })
            })
          })

          this()
        })
        .seq(function () {
          app.sesatheque.getGroupesSuivis(app.sesatheque.baseIdPrivate, this)
        })
        .seq(function (groupes) {
          _.each(groupes, (groupe) => {
            groupe.$behavior = new sesalab.behaviors.GroupeSuivi(groupe)
            self.tree.model.need(groupe, { name: groupe.name })
          })

          this()
        })
        .seq(function () {
          self.tree.addListener('open', self.onOpenTree, self)
          self.tree.refresh()
        })
        .catch(app.errorHandler('Une erreur s’est produite durant le chargement des ressources partagées'))
    },

    onAddGroup: function () {
      const url = app.sesatheque.addGroupe(
        app.sesatheque.baseIdPrivate,
        function (error, groupe) {
          if (error) {
            app.toaster.toast('Impossible de créer un groupe : ' + error)
            return
          }

          app.removePage(page)
          this.buildTree()
        }.bind(this)
      )
      const page = app.openIframePage('Nouveau groupe', 'lab', url, 'ressources-suivies-lab')
    },

    onTest: function (e) {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      app.testResource(selectedObject)
    },

    onShowInformations: function (e) {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      if (selectedObject.$describeUrl) {
        window.open(selectedObject.$describeUrl, '_blank')
      }
    },

    onPreview: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      if (selectedObject.$previewUrl) {
        window.open(selectedObject.$previewUrl, '_blank')
      }
    },

    createRessourceBehavior: function (ressource) {
      if (ressource.type === 'sequenceModele') {
        return sesalab.behaviors.SequenceModele.create(ressource)
      }

      return sesalab.behaviors.Ressource.create(ressource)
    },

    onOpenTree: function (e) {
      const invalidMsg = 'Données invalides, impossible de modifier l’affichage'
      if (!e) return app.toaster.toast(invalidMsg)
      const node = e.getData()
      if (!node || !node.instanceOf) return app.toaster.toast(invalidMsg)

      const ressource = node.data
      const b = sesalab.behaviors
      if (!ressource || ressource.$behavior instanceof b.GroupePublic || ressource.$behavior instanceof b.GroupePublicAuteur) {
        // Ressources publiques déjà chargées
        return
      }

      // Évite une nouvelle création du noeud et de ses enfants une fois celui-ci chargé
      if (node.loaded) return

      // Inutile de tenter de charger les ressources d'une séquence
      if (ressource.$behavior instanceof b.Sequence || ressource.$behavior instanceof b.SequenceModele) return

      // Charge les ressources du groupe
      const self = this
      flow()
        .seq(function () {
          node.removeAll()
          app.sesatheque.getListeGroupe(app.sesatheque.baseIdPrivate, ressource, this)
        })
        .seqEach(function (ressource) {
          const ressourceObject = self.createRessourceBehavior(ressource)
          node.need(ressourceObject, { rid: ressourceObject.rid })
          this()
        })
        .done(function () {
          node.loaded = true
          self.tree.refresh()
        })
    },

    onSelectionChanged: function (selection) {
      // si la sélection est vide (au blur), on quitte sans rien dire,
      // inutile de changer les droits sur un menu déroulant pas déroulable
      if (selection.length === 0) return
      const b = sesalab.behaviors
      const firtElt = selection[0]
      const { data } = firtElt
      const uniqSelection = selection.length === 1
      const isSequenceModel = uniqSelection && firtElt.instanceOf(b.SequenceModele)
      const isRessource = uniqSelection && firtElt.instanceOf(b.Ressource)

      this.buttonEnabled('onInstanciateSequenceModel', isSequenceModel)
      this.buttonEnabled('onTest', isRessource)
      this.buttonEnabled('onShowInformations', isRessource && data.$describeUrl)
      this.buttonEnabled('onPreview', isRessource && data.$previewUrl)
      this.buttonEnabled('onModifyResource', isRessource && data.$editUrl)
      this.buttonEnabled('onNotify', isRessource && data.rid)
    },

    onInstanciateSequenceModel: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      const sequenceModel = _.clone(selectedObject)
      app.mesSequences.instanciateSequenceModel(sequenceModel)
    },

    onModifyResource: function () {
      app.editResource(this.tree.getSelectedObject())
    }
  }
})
