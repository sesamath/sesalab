const constants = require('sesalab-commun/constants.js')

/**
 * Panneau de gestion des Niveaux/Classes/Élèves.
 */
qx.Class.define('sesalab.panels.Classes', {
  extend: sesalab.widgets.CollapsablePanel,
  include: [sesalab.widgets.Helper],

  /**
   * Constructeur
   * @param {qx.ui.form.RadioGroup} group Le groupe auquel est associé le panel.
   */
  construct: function (group) {
    this.base(arguments, group, 'Classes', {})
    this.getContentElement().setAttribute('id', 'panel-classes')
    this.createLoader()
  },

  members: {
    init: function () {
      // L'initialisation a déjà eu lieu
      if (this.tree) {
        return
      }

      // Fonction de tri par niveau puis par classe (ordre alphabétique)
      const niveauxManager = sesalab.common.Niveaux.getInstance()
      const sorter = (a, b) => {
        // Tri alphabétique suivant la locale du navigateur quand l'un des deux est un groupe
        if (a.data.oid || b.data.oid) {
          // cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/localeCompare
          return a.getCaption().toLowerCase().localeCompare(b.getCaption().toLowerCase())
        }

        // Sinon on a un dossier, tri par niveau
        return niveauxManager.findOrder(a.data.$$user_intitule) > niveauxManager.findOrder(b.data.$$user_intitule)
      }

      // Construction de l'arbre
      this.tree = new sesalab.widgets.Tree()
      this.tree.set({ sorted: sorter, hideRoot: true, showTopLevelOpenCloseIcons: true })
      this.tree.addListener('dblclick', this.fireDefaultAction, this)
      this.tree.addListener('pointerover', function (e) { app.displayEleveInformation(e, this) }, this)

      // On ignore les classes sans élèves
      this.tree.setFiltered((node) => {
        return node.getChildren().length || node.data.type === constants.TYPE_ELEVE
      })

      // Ajout des actions
      this.menuButton('onExportBilans', 'Exporter les bilans', 'combo_chart')
      this.menuButton('onShowStudentBilan', 'Afficher les bilans', 'combo_chart', true)
      app.bus.on('classes::refresh', function () {
        this.buildTree()
      }, this)

      // Ajout et construction de l'arbre
      this.add(this.tree, { flex: 1 })
      this.isBuildingTree = false
      this.buildTree()
    },

    onStateChange: function () {
      this.base(arguments)
      this.init()
    },

    buildTreeAndOpenNode: function (element) {
      this.buildTree(() => {
        this.tree.openNode(this.tree.model)
        if (!element.data) {
          return
        }

        const node = this.findNode(this.tree.model, element.data.oid)
        if (node) {
          this.tree.openNodeAndParents(node)
        }
      })
    },

    buildTree: function (callback) {
      if (this.isBuildingTree) return
      this.isBuildingTree = true
      this.tree.removeAll()
      sesalab.pages.GestionEleves.buildTree(this, true, callback)
    },

    countClasses: function () {
      return this.tree.countNodeOfType(this.tree.model, sesalab.behaviors.Groupe)
    },

    findNode: function (node, oid) {
      if (node.data.oid === oid) {
        return node
      }

      const children = node.getChildren().toArray()
      if (!children.length) {
        return null
      }

      for (let i = 0; i < children.length; i++) {
        node = this.findNode(children[i], oid)
        if (node) {
          return node
        }
      }

      return null
    },

    onSelectionChanged: function (selection) {
      const b = sesalab.behaviors
      const uniqSelection = selection.length === 1
      const isClass = uniqSelection && selection[0].instanceOf(b.Groupe)
      const isStudent = uniqSelection && selection[0].instanceOf(b.Eleve)
      this.buttonEnabled(['onExportBilans'], isClass)
      this.buttonEnabled(['onShowStudentBilan'], isStudent)
    },

    getLevelNode: function (niveauId) {
      const niveau = sesalab.common.Niveaux.getInstance().grabById(niveauId)
      const node = this.tree.need(niveau, { id: niveauId })
      return node
    },

    getGroupNode: function (nodeLevel, groupe) {
      const node = nodeLevel.need(groupe, { oid: groupe.oid })
      return node
    },

    getNodeEleve: function (nodeClasse, utilisateur) {
      nodeClasse = nodeClasse || this.tree
      const node = nodeClasse.need(utilisateur, { oid: utilisateur.oid })
      return node
    },

    onExportBilans: function () {
      const classeNode = this.tree.getSelectedNode()
      if (!classeNode) {
        console.error(new Error('Impossible d’exporter le bilan de l’élément sélectionné (data null)'))
        return
      }

      app.exportGroupBilans(classeNode)
    },

    onShowStudentBilan: function () {
      const node = this.tree.getSelectedNode()
      if (!node || !node.data) {
        console.error(new Error('Impossible d’afficher le bilan de l’élément sélectionné (data null)'))
        return
      }

      app.showStudentBilan(node.data)
    },

    grabNodeByOid (behaviorClass, oid) {
      function grab (root) {
        if (root.instanceOf(behaviorClass) && root.data.oid === oid) return root
        let found = false
        root.getChildren().some(child => {
          found = grab(child)
          return found // sort de la boucle dès que c'est truthy
        })
        if (found) return found
      }
      return grab(this.tree.model)
    },

    grabByExternalId (behaviorClass, externalId) {
      function grab (root) {
        if (root.instanceOf(behaviorClass) && root.data.externalId === externalId) return root.data
        let found = false
        root.getChildren().some(child => {
          found = grab(child)
          return found // sort de la boucle dès que c'est truthy
        })
        if (found) return found
      }
      return grab(this.tree.model)
    }
  }
})
