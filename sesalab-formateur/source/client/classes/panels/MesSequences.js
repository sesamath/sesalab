const _ = require('lodash')
const flow = require('an-flow')

const { DEFAULT_FOLDER_NAME, PANEL_MES_SEQUENCES_TITLE } = require('sesalab-commun/constants')

/**
 * Panneau de gestion de mes séquences.
 */
qx.Class.define('sesalab.panels.MesSequences', {
  extend: sesalab.widgets.SequencesPanel,

  /**
   * Constructeur
   * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
   */
  construct: function (group) {
    this.base(arguments, group, PANEL_MES_SEQUENCES_TITLE, {})
  },

  members: {
    init: function (folders) {
      if (this.initFolderPanel(folders, 'sequences', { sorted: 'asc' })) {
        this.barButton('onAddSequenceFromMenu', `Créer une séquence dans le dossier selectionné (sinon dans le dossier "${DEFAULT_FOLDER_NAME}")`, 'add_database', 'sequence-creation')
        this.menuButton('onInstanciateSequence', 'Utiliser pour une nouvelle séquence', 'add_database')
        this.menuButton('onAddSequence', 'Ajouter une séquence', 'add_database')
        this.menuButton('onOpenSequence', 'Modifier', 'edit', true)
        this.menuButton('onRenameSequence', 'Renommer', 'signature')
        this.menuButton('onShowResults', 'Voir le bilan', 'combo_chart')
        this.menuButton('onDuplicate', 'Dupliquer', 'database_duplicate')
        this.menuButton('onDuplicateWithoutStudents', 'Dupliquer sans les élèves', 'database_duplicate')
        this.menuButton('onPartageSequenceModel', 'Partager', 'share')
        this.menuButton('onTestSequence', 'Tester', 'start')
        this.menuButton('onDeleteSequence', 'Mettre dans la corbeille', 'empty_trash')
        this.menuButton('onInstanciateSequenceModel', 'Instancier', 'add_database')
        this.getContentElement().setAttribute('id', 'panel-mes-sequences')
        this.createLoader()
      }
    },

    onStateChange: function () {
      this.base(arguments)
      this.init(app.utilisateur.sequenceFolders)
    },

    decorateTree: function (index, callback) {
      const self = this
      // la liste des séquences venant de la sésathèque (type sequenceModele car y'a pas les élèves)
      const sequencesPersosByOid = {}

      flow().seq(function () {
        const next = this
        // Une erreur ne doit pas interrompre le flow, on gère la cb ici
        app.cachedGetListPerso((error, ressources, sequences) => {
          if (error) {
            app.errorHandler('Erreur pendant la récupération des séquences partagées (<a href="/sso/sesatheques">Tenter une nouvelle connexion</a> sur les bibliothèques de ressources)')(error)
            return next() // on ne bloque pas la suite
          }
          for (const sequence of sequences) {
            sequencesPersosByOid[sequence.parametres.oid] = true
          }
          next()
        })
      }).seq(function () {
        // on va chercher celles qui sont en bdd
        self.getAllSequences('sequences', [], this)
      }).seqEach(function (sequence) {
        // Supprime les informations de partage si la séquence a été supprimée de la sésathèque
        if (!sequencesPersosByOid[sequence.oid]) {
          sequence.hasModel = false
          sequence.public = false
          sequence.groupes = []
        } else {
          // partagée si publique ou partagée dans au moins un groupe
          sequence.hasModel = Boolean(sequence.public || (sequence.groupes && sequence.groupes.length))
        }

        // Création dans l'arbre
        let parent = index[sequence.oid]
        if (!parent) parent = self.getDefaultFolder()
        self.needSequenceNode(parent, sequence)

        // On évite un bug visuel Qooxdoo : l'arbre n'affichait pas l'icône d'état du dossier
        self.tree.openNode(parent)
        self.tree.closeNode(parent)
        this()
      }).done(callback)
    },

    /**
     * Verifie si la selection de l'utilisateur est composée de séquences seulement.
     *
     * @param selection Un tableau de noeud Qooxdoo
     * @return True si la selection est composée de séquence seulement
     */
    selectionFullOfSequencesOnly: function (selection) {
      for (const node of selection) {
        if (!(node.instanceOf(sesalab.behaviors.Sequence) || node.instanceOf(sesalab.behaviors.SequenceModele))) {
          return false
        }
      }

      return true
    },

    _onSelectionChanged: function (selection) {
      const b = sesalab.behaviors
      const uniqSelection = selection.length === 1
      const isSequence = uniqSelection && selection[0].instanceOf(b.Sequence)
      const isSequenceModel = uniqSelection && selection[0].instanceOf(b.SequenceModele)
      const isFolder = uniqSelection && selection[0].instanceOf(b.Dossier)
      const isUnsortedFolder = uniqSelection && selection[0] === this.unsortedFolder
      const isInStructure = isSequence && (selection[0].data.structure === app.structure.oid)
      const sequencesOnly = this.selectionFullOfSequencesOnly(selection)

      this.buttonEnabled(['onAddSequenceFromMenu'], true)
      this.buttonEnabled(['onInstanciateSequence'], isSequence && !isInStructure)
      this.buttonEnabled(['onAddSequence'], isFolder && !isUnsortedFolder)
      this.buttonEnabled(['onOpenSequence'], isSequence && isInStructure)
      this.buttonEnabled(['onRenameSequence', 'onDeleteSequence', 'onTestSequence', 'onShowResults', 'onPartageSequenceModel'], isSequence)
      this.buttonEnabled(['onDuplicate', 'onDuplicateWithoutStudents'], isInStructure)
      this.buttonEnabled(['onDeleteSequence'], sequencesOnly)
      this.buttonEnabled(['onInstanciateSequenceModel'], isSequenceModel)
    },

    eventToAction: function (source, target) {
      if (!source || !source.data) return ''
      const b = sesalab.behaviors
      const sourceClass = source.data.$behavior.constructor
      if (sourceClass === b.SequenceModele && target === b.Dossier) return 'model-dossier'
      if (sourceClass === b.SequenceModele && target === b.Sequence) return 'model-sequence'
      // sinon on râle pas, la source peut aussi être un dossier, on ignore…
    },

    onDrop: function (e) {
      if (this.base(arguments, e)) return

      _.each(e.sources, (source) => {
        switch (this.eventToAction(source, e.targetClass)) {
          case 'model-dossier':
            this.instanciateSequenceModel(source.data, e.target.data.nom)
            break
          case 'model-sequence':
            this.instanciateSequenceModel(source.data)
            break
        }
      })
    },

    onInstanciateSequence: function () {
      this.onDuplicateWithoutStudents()
    },

    onAddSequenceFromMenu: function () {
      this.onAddSequence()
    },

    onAddSequence: function () {
      let parentNode = this.tree.getSelectedNode()
      if (!parentNode || !parentNode.instanceOf(sesalab.behaviors.Dossier)) parentNode = this.getDefaultFolder()
      this.createSequence(parentNode, {
        sousSequences: [
          {
            eleves: [],
            nom: 'sous-séquence',
            serie: []
          }
        ]
      })
    },
    createSequence: function (parentNode, data) {
      parentNode = parentNode || this.getDefaultFolder()
      const thisPanel = this
      flow().seq(function () {
        const successCb = (nom) => this(null, nom)
        sesalab.widgets.InputDialog.execute('Ajouter une séquence', 'Nom', null, successCb)
      }).seq(function (nom) {
        data.nom = nom
        data.public = false
        data.groupes = []
        app.sesalab.put('sequence', data, {}, this)
      }).seq(function (result) {
        const sequence = sesalab.behaviors.Sequence.create(result.sequence)
        const node = thisPanel.needSequenceNode(parentNode, sequence)
        thisPanel.tree.openNode(parentNode)
        app.toaster.toast('La séquence a été ajoutée')
        thisPanel.selectNode(node)
        thisPanel.onOpenSequence()
        thisPanel.store()
        thisPanel.tree.refresh()
      }).catch(function (error) {
        console.error(error)
        app.toaster.toast(error, 'error')
      })
    },

    /**
     * Selectionne un noeud de l'arbre.
     * @param {TreeNode} node Noeud a selectionner
     */
    selectNode: function (node) {
      this.tree.select(node)
      this._onSelectionChanged([node])
    },

    onRenameSequence: function () {
      const thisPanel = this
      const sequence = this.tree.getSelectedObject()
      if (!sequence) return console.error(new Error('Impossible de renommer l’élément sélectionné (sequence null)'))
      sesalab.widgets.InputDialog.execute('Renommer cette séquence', 'Nom', sequence.nom, (nom) => {
        app.sesalab.put('rename-sequence', { nom, oid: sequence.oid }, {}, (error) => {
          if (error) {
            console.error(error)
            return app.toaster.toast(error, 'error')
          }
          sequence.nom = nom
          sequence.$behavior.apply({ nom })
          thisPanel.tree.refresh()
        })
      })
    },

    /**
     * Supprime un ensemble de séquences sur les sources externes (Sésathèques)
     *
     * @param sequences Un tableau de séquence
     * @param callback Callback
     */
    deleteFromExternalSources: function (sequences, callback) {
      // Inutile de traiter les séquences non partagées
      const toRemoveFromExternalSource = {}
      for (const sequence of sequences) {
        if (sequence.public || (sequence.groupes && sequence.groupes.length > 0)) {
          toRemoveFromExternalSource[sequence.oid] = true
        }
      }

      flow().seq(function () {
        // faut récupérer des items (visiblement nos objets reçus dans sequences suffisent pas)
        app.cachedGetListPerso(this)
      }).seq(function (ressources, sequences) {
        this(null, sequences.filter(item => toRemoveFromExternalSource[item.oid]))
      }).seqEach(function (itemToDelete) {
        app.sesatheque.deleteItem(itemToDelete, this)
      }).seq(function () {
        callback()
      }).catch(function (error) {
        console.error(error)
        app.toaster.toast('Une erreur s’est produite durant la suppression des séquences, vous devriez rafraîchir la page.')
      })
    },

    /**
     * Suppression d'une selection de séquence.
     */
    onDeleteSequence: function () {
      const selectedNodes = this.tree.getSelectedNodes()
      const sequencesOids = selectedNodes.map(node => node.data?.oid).filter(Boolean)
      if (sequencesOids.length === 0) return

      let message = ''
      if (sequencesOids.length === 1) {
        message = app.sequencePageOpened(selectedNodes[0].data)
          ? 'Cette séance est actuellement en cours d’édition. Voulez-vous vraiment la fermer puis la mettre dans la corbeille ?'
          : 'Voulez-vous vraiment mettre cette séquence dans la corbeille ?'
      } else {
        message = 'Voulez-vous vraiment mettre l’ensemble des séquences selectionnées dans la corbeille ?'
      }
      message += `<br>Si des élèves ont des bilans sur ${sequencesOids.length > 1 ? 'ces séquences' : 'cette séquence'}, ils ne pourront plus les consulter.`

      this.deleteSequences(message, sequencesOids, () => {
        for (const node of selectedNodes) node.remove()
        this.tree.refresh()
        this.store()
        app.bus.broadcast('corbeille::refresh', selectedNodes)
      })
    },

    /**
     * Supprime l'ensemble des séquences données.
     *
     * @param {string} message Message de confirmation à afficher
     * @param {string[]} sequencesOids Un tableau d'identifiant
     * @param {function} callback Callback appelée avec les séquences supprimées
     */
    deleteSequences: function (message, sequencesOids, callback) {
      if (sequencesOids.length === 0) return console.error(Error('deleteSequences appelé sans séquence à supprimer'))

      const self = this
      const errorHandler = app.errorHandler('Une erreur s’est produite lors de la suppression des séquences', callback)

      flow().seq(function () {
        // si l'utilisateur annule la cb ne sera pas appelée
        sesalab.widgets.ConfirmationDialog.execute(message, this)
      }).seq(function () {
        app.sesalab.del('sequence/' + sequencesOids.join(','), {}, this)
      }).seq(function (response) {
        // vérif cohérence
        if (!response) return this(Error('Response invalide (vide)'))

        const { deleted, originals, warnings } = response
        if (!deleted) return this(Error('Aucune séquence deleted mais pas d’erreur'))

        // pluriels pour les strings
        const p = deleted > 1 ? 's' : ''
        const vp = p ? 'nt' : ''

        let nbExpected = sequencesOids.length
        if (warnings.length) {
          warnings.forEach(w => app.toaster.toast(w, 'warning'))
          nbExpected -= warnings.length
        }

        if (deleted < nbExpected || originals.length < nbExpected) {
          // réponse incohérente
          const errorMsg = (deleted < sequencesOids.length)
            ? `suppression partielle parmi les séquences ${sequencesOids.join(', ')}`
            : `Pb sur DEL /api/sequence/:oids qui déclare ${deleted} séquences supprimées comme demandé mais n’en renvoie que ${originals.length}`
          console.error(Error(errorMsg), response)

          app.toaster.toast(`${p ? deleted : 'une'} séquence${p} semble${vp} avoir été supprimée${p} mais la réponse est incohérente, vous devriez recharger la page complète pour réinitialiser son contenu`)
          // on rappelle pas la callback car elle virerait toutes les séquences de l'arbre
          return
        }

        // c'est bon
        if (deleted === 1) {
          app.toaster.toast(`La séquence ${originals[0].nom} a été envoyée dans la corbeille`)
        } else {
          app.toaster.toast(`Les ${deleted} séquences sélectionnées sont désormais dans la corbeille&nbsp;:<br>- ${originals.map(s => s.nom).join('<br>- ')}`)
        }

        // Ferme les éditeurs de séquence
        for (const sequence of response.originals) {
          const editor = app.sequencePageOpened(sequence)
          if (editor) app.removePage(editor)
        }

        // on peut rendre la main
        callback()

        // et on continue en tâche de fond pour supprimer les éventuelles sequencesModeles sauvegardées ailleurs
        self.deleteFromExternalSources(sequencesOids, this)
      }).catch(errorHandler)
    },

    onPartageSequenceModel: function () {
      const seqIni = this.tree.getSelectedObject()
      if (!seqIni) return console.error(new Error('Aucune séquence sélectionnée'))

      // màj dans sesalab
      const updateShares = (seq) => {
        const data = {
          oid: seq.oid,
          groupes: seq.groupes,
          hasModel: seq.hasModel,
          public: seq.public,
          publicAmis: seq.publicAmis,
          editableAmis: seq.editableAmis
        }
        app.sesalab.put('share-sequence', data, {}, function (error) {
          if (error) {
            console.error(error)
            app.toaster.toast(error.message, 'error')
          } else {
            Object.assign(seqIni, data)
            seqIni.$behavior.refreshIcon()
          }
        })
      }

      sesalab.dialogs.SequencePartageDialog.execute(seqIni, function (seq) {
        // Ce callback n'est appelée que si l'action est validée
        if (['public', 'publicAmis', 'editableAmis', 'groupes'].every(p => seq[p] === seqIni[p])) {
          // rien n'a changé, inutile de poster
          return app.toaster.toast('Pas de changement des options de partage')
        }

        // s'il faut poster une sequenceModele on le fait d'abord (c'est le plus susceptible de planter)
        const needModel = seq.public || seq.publicAmis || seq.editableAmis || seq.groupes?.length
        if (needModel || seq.hasModel) {
          // pour créer la séquence modèle faut récupérer la séquence entière (ici on est dans Mes séquences et on a pas tout)
          app.sesalab.get('sequence/' + seqIni.oid, {}, (error, response) => {
            if (error) {
              console.error(error)
              return app.toaster.toast(error.message, 'error')
            }
            const sequence = sesalab.behaviors.Sequence.create(response.sequence)
            const sequenceModele = sesalab.behaviors.SequenceModele.createFromSequence(sequence)
            app.sesatheque.saveSequenceModele(app.sesatheque.baseIdPrivate, sequenceModele, function (error, newItem) {
              if (error) return app.toaster.toast(error.message, 'error')
              if (newItem?.rid) {
                app.toaster.toast('Votre séquence a été partagée')
                seq.hasModel = true
                updateShares(seq)
              } else {
                app.toaster.toast('La séquence n’a pas été correctement partagée', 'error')
              }
            })
          })
        } else {
          updateShares(seq)
        }
      })
    },

    onDuplicate: function () {
      const sequence = this.tree.getSelectedObject()
      if (!sequence) return console.error(new Error('Impossible de dupliquer l’élément sélectionné (sequence null)'))
      sesalab.widgets.InputDialog.execute('Nom de la nouvelle séquence', 'Nom', sequence.nom, (value) => {
        app.sesalab.get('sequence/' + sequence.oid, {}, (error, response) => {
          if (error) {
            app.toaster.toast(error.message, 'error')
            return
          }
          const sequence = response.sequence
          sequence.nom = value
          delete sequence.oid
          app.sesalab.put('sequence', sequence, {}, (error, result) => {
            if (error) {
              app.toaster.toast(error.message, 'error')
              return
            }
            sequence.oid = result.sequence.oid
            app.toaster.toast('La séquence est sauvegardée')
            const folderNode = this.tree.getSelectedNode().parentNode
            this.needSequenceNode(folderNode, sequence)
            this.tree.openNode(folderNode)
            this.store()
          })
        })
      })
    },

    onDuplicateWithoutStudents: function () {
      const sequence = this.tree.getSelectedObject()
      if (!sequence) return console.error(new Error('Impossible de dupliquer l’élément sélectionné (sequence null)'))
      sesalab.widgets.InputDialog.execute('Nom de la nouvelle séquence', 'Nom', sequence.nom, (value) => {
        app.sesalab.get('sequence/' + sequence.oid, {}, (error, response) => {
          if (error) {
            app.toaster.toast(error.message, 'error')
            return
          }
          // la séquence originale, que l'on va vider de ses élèves puis enregistrer sans son oid
          const sequence = response.sequence
          sequence.nom = value
          sequence.structure = app.structure.oid
          for (const ss of sequence.sousSequences) {
            ss.eleves = []
          }
          delete sequence.oid
          app.sesalab.put('sequence', sequence, {}, (error, result) => {
            if (error) {
              app.toaster.toast(error.message, 'error')
              return
            }
            // on lui met son nouvel oid
            sequence.oid = result.sequence.oid
            const folderNode = this.tree.getSelectedNode().parentNode
            // et on peut la mettre dans le même dossier que l'originale
            this.needSequenceNode(folderNode, sequence)
            this.store(() => {
              app.toaster.toast(`La séquence est sauvegardée dans le dossier « ${sesalab.widgets.Tree.getClearPath(folderNode)} »`)
              // le dossier était déjà  ouvert mais faut rappeler ça pour rafraîchir le contenu
              this.tree.openNode(folderNode)
            })
          }, this)
        }, this)
      }, this)
    },

    onInstanciateSequenceModel: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      const sequenceModel = _.clone(selectedObject)
      this.instanciateSequenceModel(sequenceModel)
    },

    instanciateSequenceModel: function (sequenceModel, folderName) {
      sesalab.widgets.InputDialog.execute('Instancier le modèle', 'Nom', sequenceModel.titre, (value) => {
        if (!value) return
        app.sesatheque.getRessource(sequenceModel.rid, (error, ressource) => {
          if (error) {
            app.toaster.toast(error.message, 'error')
            return
          }
          ressource.parametres.nom = value
          const sequence = sesalab.behaviors.Sequence.createFromSequenceModel(ressource.parametres)

          app.sesalab.put('sequence', sequence, {}, (error, result) => {
            if (error) {
              app.toaster.toast(error.message, 'error')
              return
            }
            sequence.oid = result.sequence.oid
            const parent = folderName ? this.needNamedFolder(folderName) : this.getDefaultFolder()
            const node = this.needSequenceNode(parent, sequence)
            this.tree.openNode(parent)
            app.toaster.toast('Le modèle a été instancié')
            this.tree.select(node)
            this.onOpenSequence()
            this.store()
          })
        }, this)
      })
    },

    // Voir FolderPanel
    getConfirmDeletionForFolderItems: function (sequenceNodes) {
      const originalMessage = this.base(arguments)
      if (_.some(sequenceNodes, (s) => app.sequencePageOpened(s.data))) {
        return 'Une ou plusieurs séquences du dossier sont actuellement en cours d’édition.' + originalMessage
      }
      return originalMessage
    },

    // Surcharge de la méthode présente dans FolderPanel
    onDeleteFolder: function () {
      const folderNode = this.tree.getSelectedNode()
      if (!folderNode) return console.error(new Error('Impossible de supprimer l’élément sélectionné (folderNode null)'))

      this.getPayloadNodes(folderNode, (error, nodes) => {
        if (error) return app.toaster.toast(error.message, 'error')

        // On filtre pour garder uniquement les identifiants des séquences
        const sequencesOid = _.map(nodes, (node) => node.data.oid)

        // Même si il n'y a pas de séquences à supprimer, on peut enlever le dossier de l'arbre
        if (!sequencesOid.length) {
          folderNode.remove()
          this.store()
          this.tree.refresh()
          return
        }

        const message = `Voulez-vous vraiment mettre l'ensemble des séquences du dossier dans la corbeille ?<br>
        Si des élèves ont des bilans sur l'une de ces séquences, ils ne pourront plus les consulter.`
        this.deleteSequences(message, sequencesOid, () => {
          folderNode.remove()
          this.store()
          this.tree.refresh()
          app.bus.broadcast('corbeille::refresh', folderNode)
        })
      })
    },

    store: function (cb) {
      if (!this.tree.isEnabled()) return
      app.utilisateur.patchFolders(
        { sequenceFolders: this.serialize() },
        app.errorHandler(`Une erreur est survenue pendant la sauvegarde du panneau "${PANEL_MES_SEQUENCES_TITLE}"`, 'error', cb)
      )
    },

    grabByExternalId: function (behaviorClass, externalId) {
      function g (root) {
        if (root.instanceOf(behaviorClass) && root.data.externalId === externalId) return root.data
        let found = false
        root.getChildren().some(function (child) {
          found = g(child)
          return found
        })
        if (found) return found
      }
      return g(this.tree.model)
    }
  }
})
