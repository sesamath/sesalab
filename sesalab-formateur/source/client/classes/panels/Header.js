qx.Class.define('sesalab.panels.Header', {
  extend: qx.ui.container.Composite,
  construct: function () {
    this.base(arguments, new qx.ui.layout.VBox())

    this.container = new qx.ui.container.Composite(new qx.ui.layout.HBox())
    this.container.setAppearance('sesalab-header')
    this.container.add(app.labelTitle)
    this.container.add(app.labelVersion)
    this.container.add(new qx.ui.core.Spacer(), { flex: 1 })
    this.add(this.container)
  },

  members: {
    createMenu: function () {
      if (this.menu) {
        return
      }

      this.menu = new qx.ui.menu.Menu()

      // ce bouton est activé ou pas dans userLoaded de sesalab-formateur/source/client/classes/Application.js
      this.btnGestion = new qx.ui.menu.Button('Statistiques', 'icons/20/statistics.png')
      this.btnGestion.addListener('execute', () => {
        window.open('/gestion', '_blank')
      })
      this.menu.add(this.btnGestion)

      this.btnProfil = new qx.ui.menu.Button('Mon compte', 'icons/20/person.png')
      this.btnProfil.addListener('execute', () => {
        if (app.settings.sso.urlAccount) {
          window.open(app.settings.sso.urlAccount, '_blank')
        } else {
          sesalab.dialogs.Profil.execute()
        }
      })
      this.menu.add(this.btnProfil)

      this.btnPreferences = new qx.ui.menu.Button('Préférences', 'icons/20/settings.png')
      this.btnPreferences.addListener('execute', () => {
        sesalab.dialogs.Preferences.execute()
      })
      this.menu.add(this.btnPreferences)

      this.btnStructureConfiguration = new qx.ui.menu.Button('Configuration établissement', 'icons/20/department.png')
      this.btnStructureConfiguration.addListener('execute', () => {
        sesalab.dialogs.ConfigurationStructure.execute()
      })
      this.menu.add(this.btnStructureConfiguration)

      this.btnGroupesPartage = new qx.ui.menu.Button('Gestion des groupes de partage', 'icons/20/share.png')
      this.btnGroupesPartage.addListener('execute', () => {
        const privateBaseURL = app.settings.sesatheques[1].baseUrl
        app.openIframePage('Gestion des groupes de partage', 'share', privateBaseURL + 'groupes/perso', 'share')
      })
      this.menu.add(this.btnGroupesPartage)

      this.btnGestionEleves = new qx.ui.menu.Button('Gestion des élèves', 'icons/20/people.png')
      this.btnGestionEleves.addListener('execute', () => {
        app.openPage(new sesalab.pages.GestionEleves())
      })
      this.menu.add(this.btnGestionEleves)

      this.btnCorbeille = new qx.ui.menu.Button('Corbeille', 'icons/20/full_trash.png')
      this.btnCorbeille.addListener('execute', () => {
        app.openPage(new sesalab.pages.Corbeille())
      })
      this.menu.add(this.btnCorbeille)

      this.structureSwitcher = new qx.ui.menu.Button('Changer de structure', 'icons/20/refresh.png')
      this.menu.add(this.structureSwitcher)

      if (app.settings.aide && app.settings.aide.homepage) {
        this.btnAide = new qx.ui.menu.Button('Aide', 'icons/20/help.png')
        this.btnAide.addListener('execute', () => {
          app.openIframePage('Aide', 'help', app.settings.aide.homepage, 'help')
        })
        this.menu.add(this.btnAide)
      }

      if (app.settings.contactMail) {
        this.btnContact = new qx.ui.menu.Button('Signalement et contact', 'icons/20/support.png')
        this.btnContact.addListener('execute', sesalab.dialogs.Contact.execute)
        this.menu.add(this.btnContact)
      }

      this.btnMentionsLegales = new qx.ui.menu.Button('Mentions légales', 'icons/20/about.png')
      this.btnMentionsLegales.addListener('execute', () => {
        window.open('/mentionsLegales', '_blank')
      })
      this.menu.add(this.btnMentionsLegales)

      this.btnLogout = new qx.ui.menu.Button('Déconnexion', 'icons/20/unlock.png')
      this.btnLogout.addListener('execute', () => {
        // une fct onbeforeunload ne veut pas dire qu'il y a des trucs à sauvegarder,
        // faut l'appeler pour savoir
        if (typeof window.onbeforeunload === 'function' && window.onbeforeunload({})) {
          sesalab.widgets.ConfirmationDialog.execute('Il vous reste des travaux non sauvegardés.<br><br> Êtes vous certain de vouloir vous déconnecter ?<br>', () => {
            window.onbeforeunload = null
            app.utilisateur.logout()
          })
        } else {
          app.utilisateur.logout()
        }
      })
      this.menu.add(this.btnLogout)

      this.utilisateurButton = new qx.ui.form.MenuButton('', 'icons/20/menu.png')
      this.utilisateurButton.setMenu(this.menu)
      const label = this.utilisateurButton.getChildControl('label')
      label.getContentElement().setAttribute('id', 'user-account')
      this.utilisateurButton.set({
        iconPosition: 'right',
        appearance: 'sesalab-header/links'
      })
      this.utilisateurButton.addListener('execute', () => {
        this.menu.show()
      }, this)
      this.container.add(this.utilisateurButton)
    }
  }
})
