const _ = require('lodash')

/**
 * Liste les élèves d'une structure.
 */
qx.Class.define('sesalab.dialogs.Eleves', {
  extend: sesalab.widgets.Dialog,

  construct: function (structure) {
    this.base(arguments, 'Élèves')

    this.setLayout(new qx.ui.layout.VBox())
    this.tree = new sesalab.widgets.Tree()
    this.tree.set({ sorted: 'asc', hideRoot: true, showTopLevelOpenCloseIcons: true })

    this.pane = new qx.ui.splitpane.Pane('horizontal')
    this.container = new qx.ui.container.Composite(new qx.ui.layout.Grow())
    this.container.add(this.tree)
    this.pane.add(this.container)
    this.add(this.pane)

    sesalab.pages.GestionEleves.buildTree(this, false)
  },

  statics: {
    execute: function (structure, onSuccess) {
      const dialog = new sesalab.dialogs.Eleves(app.structure)
      dialog.addValidateButton('Valider', function () {
        dialog.close()

        // Supprime les dossiers selectionnés pour éviter de potentiels bugs
        let selectedNodes = dialog.tree.getSelectedNodes()
        selectedNodes = _.filter(selectedNodes, function (n) {
          return !(n.data.classname === 'sesalab.common.Niveau')
        })

        onSuccess(selectedNodes)
      }, dialog)
      dialog.open()
    }
  }
})
