const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')

/**
 * Liste les élèves d'une séquence pour pouvoir tester les exercices
 */
qx.Class.define('sesalab.dialogs.SequenceEleves', {
  extend: sesalab.widgets.Dialog,

  construct: function (sequence, eleves) {
    this.selectedEleve = null
    this.sequence = sequence
    this.base(arguments, 'Elèves dans la séquence ' + this.sequence.nom)

    this.setLayout(new qx.ui.layout.VBox())
    const label = new qx.ui.basic.Atom('xx', 'icons/20/info.png')
    label.set({
      decorator: 'tooltip',
      label: 'Choisissez un élève pour tester la séquence de son point de vue',
      marginBottom: 10,
      padding: 5,
      rich: true
    })
    this.add(label)

    this.testButton = new qx.ui.form.Button('Tester')
    this.testButton.set({ margin: [10, 170, 10, 170] })
    this.showButton()
    this.testButton.addListener('execute', () => {
      app.openPage(new sesalab.pages.TestSequence(this.sequence, this.selectedEleve))
      this.close()
    }, this)
    this.add(this.testButton)

    this.model = new sesalab.widgets.TreeNode(this.sequence)
    this.tree = new sesalab.widgets.Tree(this.model)

    this.pane = new qx.ui.splitpane.Pane('horizontal')
    this.container = new qx.ui.container.Composite(new qx.ui.layout.Grow())
    this.container.add(this.tree)
    this.pane.add(this.container)
    this.add(this.pane)

    this.tree.addListener('click', () => {
      this.selectedEleve = this.getSelectedEleve()
      this.showButton()
    })

    this.tree.addListener('dblclick', () => {
      const eleve = this.getSelectedEleve()
      if (!eleve) return
      app.openPage(new sesalab.pages.TestSequence(this.sequence, eleve))
      this.close()
    })

    this.loadTree(eleves)
  },

  members: {
    showButton: function () {
      this.testButton.set({ visibility: this.selectedEleve ? 'visible' : 'excluded' })
    },
    getSelectedEleve: function () {
      const selection = this.tree.getSelectedNodes()
      if (selection.length !== 1) return
      const node = selection[0]
      const eleve = node.data
      if (!eleve) return null

      if (eleve.type === constants.SEQUENCE_TYPE_ELEVE) return eleve // eleve tout seul
      if (_.get(node, 'parentNode.data.type') === constants.SEQUENCE_TYPE_GROUPE) return eleve // eleve dans un groupe

      return null
    },
    loadTree: function (eleves) {
      eleves = _.compact(eleves)
      for (const i in eleves) {
        const eleve = eleves[i]
        if (eleve.type === 'groupe') {
          eleve.$behavior = new sesalab.behaviors.SequenceGroupe(eleve)
          const node = this.model.addChildren(eleve)
          app.populateGroupe(node)
          this.tree.openNode(node)
        } else {
          eleve.$behavior = new sesalab.behaviors.Eleve(eleve)
          this.model.addChildren(eleve)
        }
      }
      this.tree.openNode(this.model)
    }
  },

  statics: {
    execute: function (sequence, eleves) {
      const dialog = new sesalab.dialogs.SequenceEleves(sequence, eleves)
      dialog.open()
    }
  }
})
