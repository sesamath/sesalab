const _ = require('lodash')

qx.Class.define('sesalab.dialogs.ProfilSelection', {
  extend: sesalab.widgets.Dialog,

  construct: function () {
    this.base(arguments, 'Profil')
    const label = new qx.ui.basic.Label('Vous devez tout d’abord indiquer votre profil')
    label.setFont('bold')
    this.add(label)
    const comboBox = new qx.ui.form.ComboBox()
    _.each(app.settings.profils, (profil, profilName) => {
      if (['all', 'default', 'autonomie'].includes(profilName)) return
      comboBox.add(new qx.ui.form.ListItem(profil.label))
    })
    comboBox.addListener('changeValue', function () {
      const label = comboBox.getValue()
      _.each(app.settings.profils, (profil, profilName) => {
        if (profil.label === label) {
          app.utilisateur.profil.name = profilName
          return false
        }
      })
      this.close()
      this.callback()
    }, this)
    this.add(comboBox)
  },

  members: {
  },

  statics: {
    execute: function (callback) {
      const dialog = new sesalab.dialogs.ProfilSelection()
      dialog.callback = () => {
        app.utilisateur.store(
          app.errorHandler('Une erreur est survenue pendant la sauvegarde du profil', callback)
        )
      }
      dialog.open()
    }
  }
})
