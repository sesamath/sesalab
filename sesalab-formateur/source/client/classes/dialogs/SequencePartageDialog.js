const _ = require('lodash')

qx.Class.define('sesalab.dialogs.SequencePartageDialog', {
  extend: sesalab.widgets.Dialog,
  construct: function (title, sequence, groupes) {
    this.base(arguments, title)
    const content = new qx.ui.container.Composite(new qx.ui.layout.VBox(5))
    content.set({ padding: [0, 10, 10, 10] })
    this.add(content)

    const mainBox = new qx.ui.groupbox.GroupBox('Général', 'icons/20/automatic.png')
    content.add(mainBox)
    mainBox.setLayout(new qx.ui.layout.VBox())

    const label = new qx.ui.basic.Label('Nom du partage')
    mainBox.add(label)
    mainBox.add(this.field = new qx.ui.form.TextField(), { flex: 1 })
    this.field.setValue(sequence.nom)

    this.chkPublic = new qx.ui.form.CheckBox('Séquence publique')
    this.chkPublic.setMarginTop(5)
    mainBox.add(this.chkPublic)
    this.chkPublic.setMarginBottom(5)

    const container = new qx.ui.container.Composite(new qx.ui.layout.HBox())
    mainBox.add(container)
    container.add(new qx.ui.basic.Image('icons/20/info.png'), { flex: 0.2 })
    const description = new qx.ui.basic.Label('<span style="color:blue;">Une séquence publique pourra être affichée dans les résultats de recherche</span>')
    description.set({ rich: true })
    container.add(description, { flex: 0.8 })
    description.setAllowGrowY(false)
    description.setMarginTop(3)
    description.setMarginLeft(3)

    // Groupes
    const box = new qx.ui.groupbox.GroupBox('Partager dans les groupes', 'icons/20/people.png')
    content.add(box)
    box.setLayout(new qx.ui.layout.VBox())

    this.chkGroups = []
    if (groupes?.length) {
      for (const group of groupes) {
        if (group.$publish) {
          const checkbox = new qx.ui.form.CheckBox(group.name)
          if (sequence.groupes.includes(group.name)) {
            checkbox.setValue(true)
          }
          this.chkGroups.push(checkbox)
          box.add(checkbox)
        }
      }
    } else {
      box.add(new qx.ui.basic.Label('Vous devez rejoindre un groupe afin de pouvoir y partager une séquence'))
    }

    // Partager avec mes collègues
    const coworkersBox = new qx.ui.groupbox.GroupBox('Partager avec mes collègues', 'icons/20/collaboration.png')
    content.add(coworkersBox)
    coworkersBox.setLayout(new qx.ui.layout.VBox())
    this.coworkerVisible = new qx.ui.form.CheckBox('Autoriser mes collègues à voir cette séquence et ses bilans')
    coworkersBox.add(this.coworkerVisible)
    this.coworkerEditable = new qx.ui.form.CheckBox('Autoriser mes collègues à éditer cette séquence')
    this.coworkerEditable.setEnabled(false)
    coworkersBox.add(this.coworkerEditable)
    this.coworkerVisible.addListener('changeValue', function () {
      const value = this.coworkerVisible.getValue()
      this.coworkerEditable.setEnabled(value)
      if (!value) {
        this.coworkerEditable.setValue(false)
      }
    }.bind(this))

    if (this.validateButton) this.validateButton.setEnabled(true)
    // Par défaut, on coche le partage public sauf si l'utilisateur l'a déjà décoché précédemment
    if (sequence.public) this.chkPublic.setValue(true)
    if (sequence.publicAmis) this.coworkerVisible.setValue(true)
    if (sequence.editableAmis) this.coworkerEditable.setValue(true)
    this.addListener('appear', function () { this.field.focus() })
  },

  members: {
  },

  statics: {
    execute: function (seq, onSuccess) {
      const _seq = _.clone(seq) // pour préserver la séquence originale
      app.sesatheque.getGroupesSuivis(app.sesatheque.baseIdPrivate, (error, groupes) => {
        if (error) {
          console.error(error)
          return app.toaster.toast(error.message, 'error')
        }
        const dialog = new sesalab.dialogs.SequencePartageDialog('Partager la séquence', _seq, groupes)
        dialog.addValidateButton('Valider', function () {
          dialog.close()
          _seq.public = this.chkPublic.getValue()
          _seq.publicAmis = this.coworkerVisible.getValue()
          _seq.editableAmis = this.coworkerEditable.getValue()

          // il s'agit ici des groupes de partage sur la sesatheque concernée,
          // ceux que l'on a affiché en checkbox
          // (sauf l'entrée "tout le monde" qui passe le boolean public à true)
          _seq.groupes = this.chkGroups
            .map(g => g.getValue() && g.getLabel())
            .filter(Boolean)

          onSuccess(_seq)
        }, dialog)

        dialog.addCancelButton('Annuler')
        dialog.open()
        dialog.field.focus()
      })
    }
  }
})
