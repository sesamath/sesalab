/**
 * Boite de préférence de la structure de l'utilisateur
 */
qx.Class.define('sesalab.dialogs.ConfigurationStructure', {
  extend: sesalab.widgets.PropertyEditorDialog,

  construct: function () {
    this.structure = sesalab.behaviors.Structure.create(app.structure)
    this.base(arguments, 'Configuration de la structure', this.structure, {
      help: app.settings.aide.profil.etablissement,
      scrollable: true
    })
    this.set({ width: 500 })
  },

  members: {
  },

  statics: {
    execute: function () {
      const dialog = new sesalab.dialogs.ConfigurationStructure()
      dialog.execute(function (validate) {
        if (!validate) return
        // faut affecter avant de pouvoir faire le store…
        const oldStructure = { ...app.structure }
        app.structure.set(dialog.structure)
        app.structure.store(function (error, response) {
          if (error) {
            app.structure.set({ ...oldStructure })
            return app.toaster.toast(error)
          }
          // à priori c'est la même chose que ce qu'on a dans le form,
          // mais on remet quand même ce qui sort de l'api (et qui est en db)
          app.structure.set(response.structure)
          app.toaster.toast('La structure a été mise à jour')
          dialog.close()
        })
      })
    }
  }
})
