qx.Class.define('sesalab.dialogs.ProfilItem',
  {
    extend: qx.core.Object,

    construct: function (enfant) {
      this.base(arguments)
      this.enfant = enfant
      this.setLabel(this.enfant.titre)
    },

    properties: {
      label: {
        check: 'String',
        event: 'changeLabel',
        nullable: true
      }
    }
  })
