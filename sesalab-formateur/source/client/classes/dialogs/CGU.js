/**
 * Pop-in affichant les CGU.
 */
qx.Class.define('sesalab.dialogs.CGU', {
  extend: sesalab.widgets.Dialog,

  construct: function (data) {
    this.base(arguments, 'Conditions générales d’utilisation')
    this.set({ width: 600 })
    this.__childControls['close-button'].addListener('click', this.onClose)

    this.scrollContainer = new qx.ui.container.Scroll()
    this.scrollContainer.set({
      height: 500,
      maxHeight: qx.bom.Viewport.getHeight() - 100
    })
    this.add(this.scrollContainer, { flex: 1 })

    this.content = new qx.ui.container.Composite(new qx.ui.layout.VBox(5))
    this.content.setPadding(10, 10, 10, 10)
    this.scrollContainer.add(this.content, { flex: 1 })

    // Content
    const url = app.settings.aide.charte
    const label = new qx.ui.basic.Label(`Si le contenu ne s’affiche pas correctement ci-dessous vous pouvez le consulter dans un autre onglet <a href="${url}" target="_blank">${url}</a>`)
    label.set({
      rich: true,
      padding: 3
    })
    this.content.add(label)
    // @TODO créer une prop supplémentaire charteIframe en config et mettre ce suffixe dedans (plutôt que de supposer que c'est du dokuwiki)
    const frame = app.createIframe(url + '&do=export_xhtmlbody', 'font-size: 9pt;')
    this.content.add(frame, { flex: 1 })

    // Form
    this.checkbox = this.addCheckbox('J’ai lu et j’accepte les termes et conditions d’utilisation')
    this.checkbox.addListener('changeValue', (event) => {
      this.validationButton.setEnabled(event.getTarget().getValue())
    })

    this.validationButton = this.addButton('Continuer', this.doValidate, this)
    this.validationButton.setEnabled(false)
  },

  members: {
    addCheckbox: function (texte) {
      const bar = new qx.ui.container.Composite()
      const layout = new qx.ui.layout.HBox(10)
      bar.set({ layout })
      layout.set({ alignX: 'right' })
      this.add(bar)
      const checkbox = new qx.ui.form.CheckBox(texte)
      bar.add(checkbox)

      return checkbox
    },

    doValidate: function () {
      this.close()
      this.callback()
    },

    onClose: function () {
      window.location.href = '/'
    }
  },

  statics: {
    execute: function (callback) {
      const dialog = new sesalab.dialogs.CGU(callback)
      dialog.callback = callback
      dialog.open()
    }
  }
})
