/**
 * Boite de profil de l'utilisateur
 */
qx.Class.define('sesalab.dialogs.Profil', {
  extend: sesalab.widgets.PropertyEditorDialog,

  construct: function () {
    this.utilisateur = sesalab.behaviors.Utilisateur.create(app.utilisateur)
    this.base(arguments, 'Mon profil', this.utilisateur)
    this.set({ width: 400 })
  },

  members: {
  },

  statics: {
    execute: function () {
      const dialog = new sesalab.dialogs.Profil()
      dialog.execute(function (validate) {
        if (!validate) return

        function closeCallback (error) {
          if (error) return dialog.error(error)
          dialog.close()
        }

        if (dialog.utilisateur.supprimerCompte === true) {
          app.utilisateur.remove(closeCallback)
        } else {
          app.utilisateur.set(dialog.utilisateur)
          app.utilisateur.store(closeCallback)
        }
      })
    }
  }
})
