const _ = require('lodash')
const flow = require('an-flow')

qx.Class.define('sesalab.dialogs.Preferences', {
  extend: sesalab.widgets.Dialog,

  construct: function () {
    this.base(arguments, 'Préférences')

    // Informations sur l'utilisateur actuel et sa structure
    let label = new qx.ui.basic.Label(`Utilisateur : ${app.utilisateur.prenom} ${app.utilisateur.nom} (${app.utilisateur.login})`)
    this.add(label)
    const type = app.structure.type || ''
    const ville = app.structure.ville ? ` (${app.structure.ville})` : ''
    label = new qx.ui.basic.Label(`Structure : ${app.structure.code} - ${type} ${app.structure.nom} ${ville}`)
    label.set({ marginBottom: 10 })
    this.add(label)

    // choix du profil
    this._profilName = app.utilisateur.profil.name
    label = new qx.ui.basic.Label('Vos préférences par défaut')
    label.setFont('bold')
    this.add(label)
    this.comboBox = new qx.ui.form.ComboBox()
    _.each(app.settings.profils, (profil, profilName) => {
      if (['all', 'default', 'autonomie'].includes(profilName)) return
      this.comboBox.add(new qx.ui.form.ListItem(profil.label))
    })
    const profil = app.settings.profils[this._profilName]
    if (profil && profil.label) this.comboBox.setValue(profil.label)

    this.comboBox.addListener('changeValue', this.onPreferencesComboChanged, this)
    this.comboBox.set({ marginBottom: 20 })
    this.add(this.comboBox)

    label = new qx.ui.basic.Label('Ressources proposées (sélectionnez en cliquant dessus celles que vous voulez voir apparaître à gauche de votre interface)')
    this.add(label)
    this.model = new qx.data.Array()
    this.list = new qx.ui.list.List(this.model).set({
      labelPath: 'label',
      selectionMode: 'additive'
    })
    this.list.set({ marginBottom: 20 })
    this.add(this.list, { flex: 1 })

    this.checkbox = new qx.ui.form.CheckBox('Afficher les bilans avec des symboles plutôt que des couleurs')
    this.checkbox.setValue(app.utilisateur.bilanWithSymbols ? app.utilisateur.bilanWithSymbols : false)
    this.add(this.checkbox)

    // Select
    const sizeLayout = new qx.ui.layout.HBox(2)
    sizeLayout.setAlignY('middle')
    const sizeContainer = new qx.ui.container.Composite(sizeLayout)
    sizeContainer.set({ marginTop: 20 })
    sizeContainer.add(new qx.ui.basic.Label('Taille des éléments à l’écran'))

    const sizes = { smallFont: 'Petite', default: 'Moyenne', bigFont: 'Grande' }
    this.selectBox = new qx.ui.form.SelectBox()
    for (const key in sizes) {
      const tempItem = new qx.ui.form.ListItem(sizes[key], null, key)
      this.selectBox.add(tempItem)
      if ((!app.utilisateur.fontSize && key === 'default') || (app.utilisateur.fontSize === key)) {
        this.selectBox.setSelection([tempItem])
      }
    }
    sizeContainer.add(this.selectBox)
    this.add(sizeContainer)

    // Lien vers l'aide correspondante
    this.addHelpButton()

    let button
    button = this.addButton('Réinitialiser', this.doReinitialize, this)
    button = this.addButton('Valider', this.doValidate, this)

    if (!this._profilName) {
      button.setVisibility('excluded')
      label.setValue('Vous devez tout d’abord indiquer votre profil')
      this.list.setVisibility('excluded')
    }

    this.loadData()
  },

  members: {
    addHelpButton: function () {
      const helpLayout = new qx.ui.layout.HBox(2)
      helpLayout.set({ alignX: 'right' })
      const helpContainer = new qx.ui.container.Composite(helpLayout)
      helpContainer.set({ marginBottom: 10 })
      const helpLabel = new qx.ui.basic.Label('Aide')
      helpLabel.set({
        cursor: 'pointer',
        decorator: 'link',
        textColor: 'rgb(61, 114, 201)'
      })
      helpLabel.addListener('click', () => {
        app.openIframePage('Aide préférences', 'help', app.settings.aide.profil.preferences, 'help-preferences')
        this.close()
      })
      helpContainer.add(helpLabel)
      this.add(helpContainer)
    },

    loadData: function () {
      const self = this
      flow()
        // Récup de tous les arbres de ressources
        .seq(function () {
          app.sesatheque.getEnfantsProfil('all', this)
        })
        // traitement de chacun
        .seq(function (enfants) {
          _.each(enfants, (enfant) => {
            self.model.push(new sesalab.dialogs.ProfilItem(enfant))
          })
          self.onPreferencesComboChanged() // pour reconstruire le profil par défaut
        })
        .fail(function (error) {
          app.toaster.toast(error.message, 'error')
          if (typeof error === 'string') console.error(new Error(error))
          else console.error(error)
        })
    },

    onPreferencesComboChanged: function () {
      const label = this.comboBox.getValue()
      let profilName
      _.each(app.settings.profils, (profil, name) => {
        if (profil.label === label) {
          profilName = name
          return false
        }
      })
      if (!profilName) return app.errorHandler(`Profil ${label} inconnu`)(new Error(`Profil ${label} inconnu`))
      this._profilName = profilName
      app.utilisateur.profil.name = profilName
      const self = this
      flow()
        .seq(function () {
          app.sesatheque.getEnfantsProfil(profilName, this)
        })
        .seq(function (enfants) {
          self._defaultRessourcesForProfile = _.map(enfants, 'rid')
          const userSelectedRids = app.utilisateur.getSelectedRids(enfants)
          self.rebuildSelection(userSelectedRids)
        })
        .catch(app.errorHandler('Impossible de récupérer la liste des ressources de ce profil'))
    },

    rebuildSelection: function (ridsToSelect) {
      const self = this
      const selection = new qx.data.Array()

      this.model.forEach((item, index) => {
        if (ridsToSelect.includes(item.enfant.rid)) {
          selection.push(self.model.getItem(index))
        }
      })
      self.list.setSelection(selection)
    },

    doValidate: function () {
      const selection = this.list.getSelection()
      if (!selection.length) {
        app.toaster.toast('Il faut sélectionner au moins un arbre dans les ressources proposées', 'error')
        return
      }

      // On met à jour les ressources sélectionnées sur l'application pour afficher l'arbre de ressources
      const rids = selection.toArray().map((s) => s.enfant.rid)
      app.selectedRessourceRids = rids.filter(rid => rid)
      if (app.selectedRessourceRids.length < rids.length) {
        // @todo ajouter ici un notifyError pour poster l'anomalie vers le serveur afin qu'il la log dans un dataError.log
        console.error(new Error('Liste de rids avec des éléments invalides'), rids)
      }
      const ressourcesToAdd = _.difference(app.selectedRessourceRids, this._defaultRessourcesForProfile)
      const ressourcesToRemove = _.difference(this._defaultRessourcesForProfile, app.selectedRessourceRids)

      app.utilisateur.setCustomProfil(ressourcesToAdd, ressourcesToRemove)

      const fontSize = this.selectBox.getSelection()[0].getModel()
      const sizeChanged = app.utilisateur.fontSize !== fontSize
      app.utilisateur.bilanWithSymbols = this.checkbox.getValue()
      app.utilisateur.fontSize = fontSize

      app.utilisateur.store(
        app.errorHandler('Une erreur est survenue pendant la sauvegarde de vos préférences', () => {
          app.ressources.reload()
          this.close()

          // On recharge la page pour prendre en compte les changements visuels
          if (sizeChanged) {
            window.location.reload()
          }
        })
      )
    },

    doReinitialize: function () {
      app.utilisateur.clearCustomProfil()
      this.onPreferencesComboChanged()
    }
  },

  statics: {
    execute: function (callback) {
      const dialog = new sesalab.dialogs.Preferences()
      dialog.addListener('changeutilisateurData', (e) => {
        callback(e.getData())
      })
      dialog.open()
    }
  }
})
