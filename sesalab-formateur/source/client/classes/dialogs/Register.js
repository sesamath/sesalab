const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')

/**
 * Ajoute les messages fournis par les ≠ ssoProviders, si y'en a
 * @param {sesalab.dialogs.Register} dialog Ce dialog
 */
function addBlocInscription (dialog) {
  if (app.settings.sso.accountCreationBlocks) {
    app.settings.sso.accountCreationBlocks.forEach((block) => {
      const { message, url } = block
      // on vérifie le format renvoyé
      if ([message, url].every((str) => str && typeof str === 'string')) {
        const blocInscription = new qx.ui.basic.Atom(message, 'icons/20/info.png')
        blocInscription.set({
          cursor: 'pointer',
          decorator: 'tooltip',
          padding: 5,
          rich: true
        })
        blocInscription.addListener('click', () => {
          window.location.href = url
        })
        dialog.add(blocInscription)
      } else {
        console.error(new Error('bloc d’inscription invalide'), block)
      }
    })
  }
}

/**
 * Boite de création de compte formateur
 */
qx.Class.define('sesalab.dialogs.Register', {
  extend: sesalab.widgets.Dialog,

  construct: function () {
    this.base(arguments, `Créer un compte ${app.settings.name}`)
    this.rgpdChecked = false
    this.utilisateur = {}
    this.utilisateur.type = 1
    this.etapeCreationStructure()
    this.set({ maxWidth: 400 })
    this.addListener('beforeClose', (e) => {
      if (!app.utilisateur.oid) {
        e.preventDefault()
        window.location.href = '/'
      }
    })
  },

  members: {
    getErrorLabel: function () {
      const errorLabel = new qx.ui.basic.Label()
      errorLabel.set({
        rich: true,
        decorator: 'error',
        padding: 3,
        marginTop: 10,
        visibility: 'excluded'
      })
      return errorLabel
    },
    getErrorMessage: function () {
      const errorMessage = new qx.ui.container.Composite()
      errorMessage.setLayout(new qx.ui.layout.VBox(10))
      return errorMessage
    },
    setErrorMessage: function (errorLabel, message) {
      if (!message || !message.length) return
      errorLabel.setValue(message)
      errorLabel.setVisibility('visible')
    },

    structureField: function (group) {
      const field = group.addField('structure', 'Code ou nom de la structure', new sesalab.widgets.AutoCompleteField(), true)
      const message = group.addLabel('----')
      message.set({ visibility: 'excluded' })

      field.addListener('complete', () => {
        const value = field.getValue()
        app.toaster.clear()

        if (value.length < constants.MIN_AUTOCOMPLETE_INPUT) {
          field.resetCompletion()
          return
        }

        app.sesalab.get(`structure?_nom=${value}&hideClosedStructures`, {}, (error, response) => {
          field.resetCompletion()
          if (error) {
            console.error(error)
            return app.toaster.toast(error.message, 'error')
          }
          if (response.message) return app.toaster.toast(response.message, 'error')
          if (!response.structures || !response.structures.length) return app.toaster.toast('Aucun résultat', 'error')
          response.structures = _.filter(response.structures, (structure) => !structure.ajoutFormateursInterdit)
          response.structures.forEach(s => {
            if (s.code && s.nom) {
              let value = `${s.code} -`
              if (s.type) value += ` ${s.type}`
              value += ` ${s.nom}`
              if (s.ville) value += ` (${s.ville})`
              field.addCompletionItem(value, s)
            } else {
              console.error('Structure reçue en autocomplete invalide', s)
            }
          })
          field.showCompletion()
        })
      })

      field.addListener('completed', (e) => {
        const data = e.getData()
        if (data.code && data.nom) {
          let value = `${data.code} - ${data.nom}`
          this.structure = data
          if (data.ville) value += ` (${data.ville})`
          field.setValue(value)
        } else {
          console.error('Structure invalide', data)
        }
      })

      return field
    },

    /**
     * Création de compte formateur step 1, on demande de choisir une structure ou d'en créer une
     */
    etapeCreationStructure: function () {
      this.removeAll()
      addBlocInscription(this)

      // encart RGPD
      const rgpdContainer = this.createFormGroup('Données personnelles')
      const label = `J’ai pris connaissance des informations relatives à la <a href="${app.settings.aide.rgpd}" target="_blank">gestion des données personnelles`
      // on modifie le grid layout (ça marche pas, on voulait centrer le checkbox dans sa case de grid)
      rgpdContainer.getLayout().setColumnAlign(1, 'center', 'middle')
      const rgpdCheckbox = rgpdContainer.addField('rgpd', label, new qx.ui.form.CheckBox())
      rgpdCheckbox.set({ value: false, rich: true })
      // listener pour activer / désactiver le reste du formulaire
      rgpdCheckbox.addListener('changeValue', () => {
        this.rgpdChecked = rgpdCheckbox.getValue()
        gbStructureExistante.setEnabled(this.rgpdChecked)
        form.setEnabled(this.rgpdChecked)
      })
      // on trafique les tailles via la prop interne _fields du container
      const qxField = rgpdContainer._fields.rgpd
      qxField.label.set({ minWidth: 300, maxWidth: 300 })
      qxField.set({ minWidth: 50 })
      this.add(rgpdContainer)

      const gbStructureExistante = this.createFormGroup('Se rattacher à une structure existante', true)
      gbStructureExistante.setEnabled(false)
      const structure = this.structureField(gbStructureExistante)
      gbStructureExistante.addButton({ label: 'Utiliser cette structure' }, () => {
        let structureId = structure.getValue().split(/\s+-\s+/)
        structureId = structureId[0]
        if (structureId.length === 0) return gbStructureExistante.haveErrors({ field: 'structure', message: 'Ce champ est obligatoire' })
        app.sesalab.get('structure?code=' + structureId, {}, (error, response) => {
          if (error) return app.errorHandler('Une erreur est survenue durant la récupération de la structure')(error)
          if (!response || !response.structures || response.structures.length === 0) {
            const fieldError = { field: 'structure', message: 'Structure inconnue' }
            if (gbStructureExistante.haveErrors(fieldError)) return
          }

          this.structure = response.structures[0]
          if (this.structure.ajoutFormateursInterdit === true) {
            const fieldError = { field: 'structure', message: 'La création de compte formateur est interdit sur cette structure' }
            if (gbStructureExistante.haveErrors(fieldError)) return
          }

          this.utilisateur.structureCode = this.structure.code
          this.etapeCreationCompteFormateur()
        })
      })
      this.add(gbStructureExistante)

      // Zone d'affichage des erreurs
      const errorLabel = this.getErrorLabel()
      const errorMessage = this.getErrorMessage()
      errorMessage.add(errorLabel)
      this.add(errorMessage)

      const form = this.createFormGroup('Créer une nouvelle structure', true)
      form.setEnabled(false)
      form.addField('pays', 'Pays', new qx.ui.form.TextField(''))
      form.addField('ville', 'Ville', new qx.ui.form.TextField(''))
      form.addField('nom', 'Nom', new qx.ui.form.TextField(''))
      form.addField(
        'ajoutElevesAuthorises',
        'Autoriser les élèves à demander leur création de compte, ' +
        'qu’un formateur devra valider', new qx.ui.form.CheckBox())
        .set({ value: true })
      form.addField(
        'ajoutFormateursInterdit',
        'Interdire la création de compte formateur', new qx.ui.form.CheckBox())
        .set({ value: false })

      form.addButton({ label: 'Créer la structure' }, () => {
        app.sesalab.post('structure', form.data(), {}, (error, response) => {
          if (error) {
            // On ne doit pas afficher l'erreur lorsqu'il s'agit d'un problème de throttle
            if (error.field) this.setErrorMessage(errorLabel, error.message)
            if (form.haveErrors(error)) return
          }

          this.structure = response.structure
          this.utilisateur.structureCode = response.code
          this.etapeCreationCompteFormateur()
        })
      })
      this.add(form)

      this.radioGroup = new qx.ui.form.RadioGroup(gbStructureExistante, form)
      gbStructureExistante.set({ value: true })
      structure.focus()
    },

    onShowMessage: function (message) {
      this.removeAll()
      const label = new qx.ui.basic.Label(message)
      label.set({
        maxWidth: 250,
        rich: true,
        textAlign: 'center'
      })
      this.add(label, { flex: 1 })
    },

    /**
     * Création de compte formateur step 2, après le choix d'une structure, infos formateur
     */
    etapeCreationCompteFormateur: function () {
      this.removeAll()

      // Zone d'affichage des erreurs
      const errorLabel = this.getErrorLabel()
      const errorMessage = this.getErrorMessage()
      errorMessage.add(errorLabel)
      this.add(errorMessage)

      addBlocInscription(this)

      const label = new qx.ui.basic.Label(`
        <br>
        <b>Pour créer un compte formateur dans la structure «&nbsp;${this.structure.nom}&nbsp;»,
        veuillez renseigner les champs suivants :</b>`)
      label.set({
        rich: true,
        maxWidth: 400
      })
      this.add(label)

      const form = this.createFormGroup()
      form.addField('nom', 'Nom', new qx.ui.form.TextField(''))
      form.addField('prenom', 'Prénom', new qx.ui.form.TextField(''))
      form.addField('mail', 'Mail', new qx.ui.form.TextField(''))
      form.addField('login', 'Identifiant', new qx.ui.form.TextField(''))
      form.addField('password', 'Mot de passe', new qx.ui.form.PasswordField(''))
      form.addField('passwordBis', 'Répétez le mot de passe', new qx.ui.form.PasswordField(''))
      form.addButton({ label: 'Créer le compte' }, () => {
        const utilisateur = form.data()
        utilisateur.type = this.utilisateur.type
        utilisateur.structureCode = this.utilisateur.structureCode
        app.sesalab.post('formateur', utilisateur, {}, (error, response) => {
          if (error) {
            // On ne doit pas afficher l'erreur lorsqu'il s'agit d'un problème de throttle
            if (error.field) this.setErrorMessage(errorLabel, error.message)
            if (form.haveErrors(error)) return
          }
          this.onShowMessage(response.message)
        })
      })
      this.add(form)
    },

    /**
     * Retourne un groupe d'éléments de formulaire stylés
     * @todo en faire un composant custom utilisable ailleurs
     * @private
     * @param {string} [caption] Si non fourni, retournera un Composite (sinon, RadioGroupBox si radio et GroupBox sinon)
     * @param {boolean} [radio=false] Passer true pour récupérer un RadioGroupBox (si caption)
     * @returns {qx.ui.container.Composite|qx.ui.groupbox.RadioGroupBox|qx.ui.groupbox.GroupBox}
     */
    createFormGroup: function (caption, radio) {
      let groupBox
      radio = radio || false

      if (typeof caption === 'undefined') {
        groupBox = new qx.ui.container.Composite()
      } else {
        if (radio) {
          groupBox = new qx.ui.groupbox.RadioGroupBox(caption)
        } else {
          groupBox = new qx.ui.groupbox.GroupBox(caption)
          // pour le titre du groupe
          groupBox.getChildControl('legend').set({
            rich: true,
            minHeight: 30,
            height: 30,
            iconPosition: 'top'
          })
        }
      }

      // Définition du layout du groupe
      const layout = new qx.ui.layout.Grid()
      layout.setSpacing(6)
      layout.setColumnFlex(0, 1)
      layout.setColumnAlign(0, 'right', 'top')
      groupBox.setLayout(layout)

      // Propriétés internes
      groupBox._row = 0
      groupBox._fields = {}

      // Reset des erreurs du groupe
      groupBox.resetErrors = function () {
        for (const i in groupBox._fields) {
          groupBox._fields[i].removeState('invalid')
        }
      }

      // Lecture des données
      groupBox.data = function () {
        const result = {}
        for (const i in groupBox._fields) {
          result[i] = groupBox._fields[i].getValue()
        }
        return result
      }

      groupBox.haveErrors = function (error) {
        this.resetErrors()
        if (error) {
          groupBox.error(error.field, error.message)
          return true
        }
        return false
      }

      // Affectation d'une erreur au champ
      groupBox.error = function (field, message) {
        field = groupBox._fields[field]
        if (field) {
          field.addState('invalid')
          const tooltip = new qx.ui.tooltip.ToolTip(message, '/formateur/icons/20/decision.png')
          tooltip.set({
            backgroundColor: '#FEE6BA',
            decorator: 'tooltip',
            padding: [2, 5],
            offset: 3,
            offsetBottom: 20
          })
          tooltip.setShowTimeout(1000)
          field.setToolTip(tooltip)
          tooltip.placeToWidget(field)
          tooltip.show()
        }
      }

      // Ajout d'un champ
      groupBox.addField = function (name, label, field, forceNewLine) {
        groupBox._fields[name] = field

        // Ajout du label en colonne 0
        field.label = new qx.ui.basic.Label(label)
        field.label.set({
          rich: true,
          maxWidth: 200
        })
        groupBox.add(field.label, { row: groupBox._row, column: 0 })
        field.set({ minWidth: 200 })

        if (forceNewLine) {
          groupBox._row++
        }

        // Fonction de masquage du champ
        field.visibility = function (v) {
          field.label.set({ visibility: v })
          field.set({ visibility: v })
        }

        // Ajout du champ en colonne 1
        const options = {
          row: groupBox._row,
          colSpan: forceNewLine ? 2 : 1,
          column: forceNewLine ? 0 : 1
        }

        groupBox.add(field, options)
        groupBox._row++

        return field
      }

      // Ajout d'un label
      groupBox.addLabel = function (value) {
        const label = new qx.ui.basic.Label(value)
        groupBox.add(label, { row: groupBox._row, column: 1 })
        groupBox._row++

        return label
      }

      // Ajout d'un bouton
      groupBox.addButton = function (options, cb, context) {
        if (!groupBox.bar) {
          groupBox.bar = new qx.ui.container.Composite()
          const layout = new qx.ui.layout.HBox(10)
          layout.set({ alignX: 'right' })
          groupBox.bar.set({ layout })
          groupBox.add(groupBox.bar, { row: groupBox._row, column: 1 })
        }
        const button = new qx.ui.form.Button(options.label)
        if (options.default) {
          this.addListener('keypress', (e) => {
            if (e.getKeyIdentifier() === 'Enter') {
              button.focus()
              button.execute()
            }
          })
        }
        groupBox.bar.add(button)
        if (options.forceNewLine) {
          groupBox.bar = null
          groupBox._row++
        }
        if (cb) button.addListener('execute', cb, context)

        return button
      }

      return groupBox
    }
  },

  statics: {
    execute: function (callback) {
      const dialog = new sesalab.dialogs.Register()
      dialog.addListener('changeutilisateurData', function (e) {
        callback(e.getData())
      })
      dialog.open()
    }
  }
})
