const flow = require('an-flow')
const { getRidComponents, getBaseUrl } = require('sesatheque-client/src/sesatheques')
const { PANEL_MES_SEQUENCES_TITLE } = require('sesalab-commun/constants')

function getBrowser () {
  // on a qq bugsnag avec `Failed to read a named property 'navigator' from 'Window': Blocked a frame with origin "https://labomep.sesamath.net" from accessing a cross-origin frame.`
  try {
  // Regexp trouvée sur https://gist.github.com/ticky/3909462
    const reg = /(MSIE|(?!Gecko.+)Firefox|(?!AppleWebKit.+Chrome.+)Safari|(?!AppleWebKit.+)Chrome|AppleWebKit(?!.+Chrome|.+Safari)|Gecko(?!.+Firefox))[/ ]([\d.apre]+)/
    const matches = window.navigator.userAgent.match(reg)
    return (matches && matches[0]) || 'unknown'
  } catch (error) {
    console.error(error)
    return 'unknown'
  }
}

/**
 * Formulaire de mise en contact.
 */
qx.Class.define('sesalab.dialogs.Contact', {
  extend: sesalab.widgets.Dialog,

  /**
   * Constructeur du form de contact
   * @param {Object} options
   * @param {string} [options.rid] pour faire un signalement sur une ressource
   */
  construct: function (options) {
    this.base(arguments, 'Signalement et contact')
    this.set({
      width: 600,
      height: Math.min(1000, Math.max(document.body.offsetHeight - 200, 200)),
      resizable: true
    })
    this.options = options || {}
    this.init()
  },

  members: {
    init: function () {
      this.scrollContainer = new qx.ui.container.Scroll()
      // piqué dans ScrollablePage, ça lui fait prendre toute la hauteur du Dialog
      // (mais du coup le bouton peut se mettre par dessus le reste dans le coin inférieur droit,
      // ça tombe bien ça nous arrange en le laissant toujours visible)
      qx.ui.tabview.Page.prototype.add.call(this, this.scrollContainer, { flex: 1 })
      this.content = new qx.ui.container.Composite(new qx.ui.layout.VBox(5))
      this.scrollContainer.add(this.content, { flex: 1 })
      this.add(this.scrollContainer)

      this.content.set({ padding: [0, 10, 10, 10] })

      // on laisse le choix mailto ou formulaire
      const { settings: { name }, structure, utilisateur } = app
      const departement = structure.departement || '-'
      const rid = this.options.rid
      let subject = `Signalement ${name}`
      let ressourceUrl = ''
      if (rid) {
        subject += ` (ressource ${rid})`
        try {
          const [baseId, id] = getRidComponents(rid)
          ressourceUrl = `${getBaseUrl(baseId)}ressource/decrire/${id}`
        } catch (error) {
          console.error(error)
          ressourceUrl = rid
        }
        this.options.ressourceUrl = ressourceUrl
      }
      // Attention à répercuter toute modif faite ici dans getMailTextContact (sesalab-api/source/utilisateur/mailTemplates.js)
      const body = `Merci de laisser ces lignes qui nous permettent de mieux traiter votre signalement
Message de ${utilisateur.nom} ${utilisateur.prenom}
Id : ${utilisateur.login} (${utilisateur.oid})
Mél : ${utilisateur.mail ?? 'non fourni'}
Etablissement : ${departement} / ${structure.ville} / ${structure.nom} (${structure.oid})
Navigateur : ${getBrowser()}
Serveur : ${window.location.host}
Version : ${app.settings.version}
${ressourceUrl ? `Ressource : ${ressourceUrl}\n` : ''}
Votre message :
`
      const mail = app.settings.contactMail
      const hrefMailto = `mailto:${mail}?subject=${subject}&body=${encodeURIComponent(body)}`
      // message commun avec mail ou sans
      let labelTxt = `<p><strong style="color: #c33">Merci de lire ces consignes avant d'envoyer votre signalement !</strong></p>Avant de signaler un problème, vérifiez dans <a href="${app.settings.aide.homepage}" target="_blank">l'aide</a> (notamment la FAQ) que la solution ne s'y trouve pas.<br />
<p><strong>Si le signalement concerne une ressource, merci de le faire via un clic droit sur la ressource</strong> (cela nous donne les informations permettant de la retrouver vite et de manière fiable), et <strong>avec une capture d’écran</strong> (pour que l'on ait le contexte, même si ça vous semble évident et généralisé).</p>
<p>Merci de toujours <strong>donner un exemple</strong> permettant de reproduire le problème que vous signalez, par exemple en précisant <strong>une séquence</strong> (et comment la trouver dans votre arbre "${PANEL_MES_SEQUENCES_TITLE}"), <strong>un exercice</strong> de la séquence ainsi qu'un élève et sa classe (même si cela vous semble généralisé, donnez un exemple).</p>
`
      // la suite dépend si mail dispo ou pas
      labelTxt += utilisateur.mail
        ? `<p>La réponse vous sera envoyée par mail à l'adresse <strong>${app.utilisateur.mail}</strong>.</p>
Vous pouvez rédiger ce message directement dans <a href="${hrefMailto}">votre logiciel de messagerie</a>, si le lien n'ouvre pas le bon logiciel il faut le préciser dans les préférences de votre navigateur ou celles du système.`
        : `Vous n’avez pas renseigné d’adresse mail permettant de vous répondre, le formulaire n'est donc pas disponible mais vous pouvez rédiger le message dans <a href="${hrefMailto}">votre logiciel de messagerie</a>, si le lien n'ouvre pas le bon logiciel il faut le préciser dans les préférences de votre navigateur / système.`
      const label = new qx.ui.basic.Label(labelTxt)
      label.set({
        rich: true,
        padding: 3
      })
      this.content.add(label)
      if (utilisateur.mail) {
        // on peut passer au form

        // fichier
        const label = new qx.ui.basic.Label('<strong>Ajouter une capture d’écran</strong> (en cas de signalement sur une ressource c’est indispensable pour traiter votre demande avec son contexte).<br>Si cela ne fonctionne pas avec votre navigateur vous pouvez également utiliser un service comme <a href="https://plik.ovh/" target="_blank">plik</a> pour héberger le fichier et mettre ensuite le lien dans votre message.')
        label.set({
          rich: true,
          padding: 3
        })
        this.content.add(label)

        // avec qooxdoo 7 on pourra faire
        // const button = new qx.ui.form.FileSelectorButton('Select File')
        // button.addListener('changeFileSelection', function (e) {
        //   const fileList = e.getData()
        //   const form = new FormData()
        //   form.append('file', fileList[0])
        //   const req = new qx.io.request.Xhr('upload', 'POST').set({ requestData: form })
        //   req.addListener('success', (e) => {
        //     const response = req.getResponse()
        //     console.log('upload response', response)
        //   })
        // })

        // ou bien

        // const button = new qx.ui.form.FileSelectorButton('Select File')
        // button.addListener('changeFileSelection', function (e) {
        //   const fileList = e.getData()
        //   /* global FileReader */
        //   const reader = new FileReader()
        //   reader.addEventListener('load', () => {
        //     const response = reader.result
        //     console.log('The first 4 chrs are: ' + response)
        //   })
        //   const file = fileList[0]
        //   reader.readAsText(file.slice(0, 4))
        // })

        // mais en qooxdoo 5 y'a pas tout ça, on contourne le pb…
        // ça marche pas car n'hérite pas de qx.ui.core.LayoutItem
        // this.fileField = new qx.html.Element('input', { margin: '5px' }, { type: 'file', multiple: true })
        // => on se crée un widget perso
        this.fileField = new sesalab.widgets.FileInput(true)
        this.fileField.setPlaceholder('Faire glisser les fichiers ici ou cliquer sur le bouton pour les sélectionner (plusieurs possibles)')

        // faut une box pour mettre input & bouton sur la même ligne
        const fileCt = new qx.ui.container.Composite()
        const layout = new qx.ui.layout.HBox(10)
        layout.setAlignY('middle')
        fileCt.setLayout(layout)

        // le placeholder sert à rien, le navigateur ajoute le bouton avec le champ
        // this.fileField.setPlaceholder('Glisser un fichier ici ou utiliser le bouton pour le sélectionner')
        this.content.add(fileCt)
        fileCt.add(this.fileField, { flex: 1 })
        // bouton flush
        // const fileFlushButton = this.addButton('Supprimer tous les fichiers', () => this.fileField.flush())
        this.flushButton = new qx.ui.form.Button('Supprimer')

        this.flushButton.addListener('execute', () => this.fileField.flush())
        this.flushButton.set({ maxHeight: 22 })
        fileCt.add(this.flushButton)

        // sujet
        this.subjectField = new qx.ui.form.TextField()
        this.subjectField.setPlaceholder('Indiquez l’objet ici')
        if (rid) this.subjectField.setValue(subject)
        this.content.add(this.subjectField)

        // message
        this.messageField = new qx.ui.form.TextArea()
        this.messageField.set({ height: 300, minHeight: 300 })
        this.messageField.setPlaceholder('Décrivez votre signalement ici')
        this.content.add(this.messageField, { flex: 1 })
        this.messageField.addListener('keyup', (event) => {
          const target = event.getTarget()
          const content = target && target.getValue()
          this.validationButton.setEnabled(content && content.length > 0)
        })

        // et la validation
        this.validationButton = this.addButton('Envoyer ma demande', this.doValidate, this)
        this.validationButton.set({ padding: 10 })
        this.validationButton.setEnabled(false)
      }
    },

    setEnabledAll: function (value) {
      for (const messageField of [this.validationButton, this.flushButton, this.messageField, this.subjectField, this.fileField]) {
        messageField.setEnabled(value)
      }
    },

    doValidate: function () {
      const self = this
      const browser = getBrowser()
      const subject = this.subjectField.getValue() || 'Signalement'
      const message = this.messageField.getValue()
      const ressourceUrl = this.options.ressourceUrl

      this.setEnabledAll(false)

      let hasFileUploadPb

      flow().seq(function () {
        const next = this
        self.fileField.getFilesContents().then(files => {
          next(null, files)
        }).catch(next)
      }).seq(function (files) {
        hasFileUploadPb = files.length !== self.fileField.getFilesNb()
        app.sesalab.post(
          'contact',
          {
            subject,
            message,
            browser,
            host: window.location.host,
            ressourceUrl,
            files
          },
          {},
          this
        )
      }).seq(function () {
        self.showSentMessage(hasFileUploadPb)
      }).catch((err) => {
        app.errorHandler('Une erreur est survenue durant l’envoi du message')(err)
        this.setEnabledAll(true)
      })
    },

    showSentMessage: function (hasFileUploadPb) {
      // Cache le bouton de validation
      this.validationButton.setVisibility('excluded')
      // Affiche le message de confirmation
      let msg = 'Votre message a bien été envoyé'
      if (hasFileUploadPb) msg += ', mais votre navigateur n’a pu gérer l’envoi du fichier, vous pouvez l’envoyer par mail à ' + app.settings.contactMail
      const label = new qx.ui.basic.Label(msg)
      label.set({
        decorator: 'tooltip',
        rich: true,
        padding: 3
      })
      label.setAlignX('center')
      this.content.add(label)
      const btn = new qx.ui.form.Button('Fermer')
      btn.addListener('execute', () => this.close())
      this.content.add(btn)
      this.scrollContainer.scrollChildIntoView(btn)
    }
  },

  statics: {
    /**
     * Ouvre le form de contact
     * @param {Object} options
     * @param {string} [options.rid] pour faire un signalement sur une ressource
     */
    execute: function (options) {
      const dialog = new sesalab.dialogs.Contact(options)
      dialog.open()
    }
  }
})
