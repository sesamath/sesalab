const _ = require('lodash')
const flow = require('an-flow')
const uuid = require('an-uuid')
const { TYPE_ELEVE } = require('sesalab-commun/constants.js')

qx.Class.define('sesalab.pages.Corbeille', {
  extend: sesalab.widgets.ScrollablePage,
  include: [sesalab.widgets.Helper],
  construct () {
    this.base(arguments, 'Corbeille', 'full_trash')
    this.addHelpButton()

    // Création de l'arbre
    this.tree = new sesalab.widgets.Tree()
    this.tree.set({ sorted: 'asc', hideRoot: true, showTopLevelOpenCloseIcons: true })
    this.tree.addListener('pointerover', function (e) { app.displayEleveInformation(e, this) }, this)

    this.menuButton('onRestoreStudents', 'Restaurer les élèves sélectionnés', 'refresh')
    this.menuButton('onRestoreClass', 'Restaurer les classes sélectionnées', 'refresh')
    this.menuButton('onRestoreGroup', 'Restaurer les groupes sélectionnés', 'refresh')
    this.menuButton('onRestoreRessource', 'Restaurer les ressources sélectionnées', 'refresh')
    this.menuButton('onRestoreSequences', 'Restaurer les séquences sélectionnées', 'refresh')
    app.bus.on('corbeille::refresh', (event, node) => {
      this.buildTree(() => {
        let source
        if (!node || !node.instanceOf) {
          this.tree.refresh()
          return
        }

        if (node.instanceOf(sesalab.behaviors.Sequence)) {
          source = this.sequencesFolderNode
        } else if (node.instanceOf(sesalab.behaviors.Eleve)) {
          source = this.elevesFolderNode
        } else if (node.instanceOf(sesalab.behaviors.Groupe)) {
          if (node.data.niveau) {
            source = this.classesFolderNode
          } else {
            source = this.groupesFolderNode
          }
        }

        if (!source) return

        const nodeInTree = this.findNode(source, node.data.oid)
        if (nodeInTree) {
          this.tree.openNodeAndParents(nodeInTree)
        }
      })
    })

    const container = new qx.ui.container.Composite(new qx.ui.layout.Grow())
    container.add(this.tree)
    this.add(container, { flex: 1 })
    this.buildTree()
  },
  members: {
    addHelpButton: function () {
      const label = new qx.ui.basic.Atom('xx', 'icons/20/help.png')
      label.set({
        cursor: 'pointer',
        decorator: 'tooltip',
        label: 'Pour plus d’informations, cliquez-ici',
        marginBottom: 20,
        padding: 5,
        rich: true
      })
      label.addListener('click', () => {
        app.openIframePage('Aide corbeille', 'help', app.settings.aide.corbeille, 'help-corbeille')
      })
      this.add(label)
    },

    getId: function () {
      return 'corbeille'
    },

    findNode: function (node, oid) {
      if (node.data.oid === oid) {
        return node
      }

      const children = node.getChildren().toArray()
      if (!children.length) {
        return null
      }

      for (let i = 0; i < children.length; i++) {
        node = this.findNode(children[i], oid)
        if (node) {
          return node
        }
      }

      return null
    },

    getAllDeletedElevesByStructure: function (url, deletedEleves, callback) {
      app.sesalab.get(url, {}, (error, response) => {
        if (error) return callback(error)
        if (!response.utilisateurs) return callback(new Error('Réponse invalide, impossible de récupérer la liste des élèves dans la corbeille'))
        deletedEleves = deletedEleves.concat(response.utilisateurs)
        if (!response.nextUrl) return callback(null, deletedEleves) // Tous les utilisateurs sont désormais présents
        this.getAllDeletedElevesByStructure(response.nextUrl, deletedEleves, callback)
      })
    },

    getAllDeletedSequences: function (url, deletedSequences, callback) {
      app.sesalab.get(url, { timeout: 20000 }, (error, response) => {
        if (error) return callback(error)
        deletedSequences = deletedSequences.concat(response.sequences)
        if (!response.nextUrl) return callback(null, deletedSequences)
        this.getAllDeletedSequences(response.nextUrl, deletedSequences, callback)
      })
    },

    buildTree: function (callback) {
      const self = this
      self.tree.removeAll()

      flow()
        // Création de l'arbre pour les élèves supprimés
        .seq(function () {
          const next = this
          const elevesFolder = sesalab.behaviors.Dossier.create({
            nom: 'Elèves',
            folders: [],
            uuid: uuid()
          })
          self.elevesFolderNode = self.getFolderNode(elevesFolder)

          flow()
            .seq(function () {
              self.getAllDeletedElevesByStructure('utilisateur/by-structure/' + app.structure.oid + '?type=' + TYPE_ELEVE + '&deletedOnly=true',
                [], this)
            })
            .seq(function (deletedEleves) {
              _.each(deletedEleves, (eleve) => {
                eleve = new sesalab.behaviors.Eleve.create(eleve)
                self.getEleveNode(self.elevesFolderNode, eleve)
              })
              this()
            })
            .done(next)
        })
        // Création de l'arbre pour les classes supprimées
        .seq(function () {
          const next = this
          const classesFolder = sesalab.behaviors.Dossier.create({
            nom: 'Classes',
            folders: [],
            uuid: uuid()
          })
          self.classesFolderNode = self.getFolderNode(classesFolder)
          app.sesalab.get(`groupes/by-structure/${app.structure.oid}?onlyDeleted=true`, {}, (error, response) => {
            if (error) return next()
            _.each(response.groupes, (groupe) => {
              groupe.nom = groupe.nom || 'Sans nom'
              groupe.niveau = groupe.niveau || 0
              const levelNode = self.getLevelNode(self.classesFolderNode, groupe.niveau)
              const group = new sesalab.behaviors.Groupe.create(groupe)
              const groupNode = self.getGroupNode(levelNode, group)
              _.each(groupe.utilisateurs, (eleve) => {
                eleve = sesalab.behaviors.Eleve.create(eleve)
                self.getEleveNode(groupNode, eleve)
              })
            })
            return next()
          })
        })
        // Création de l'arbre pour les groupes supprimés
        .seq(function () {
          const next = this
          const groupesFolder = sesalab.behaviors.Dossier.create({
            nom: 'Groupes',
            folders: [],
            uuid: uuid()
          })
          self.groupesFolderNode = self.getFolderNode(groupesFolder)
          app.sesalab.get('groupes/by-owner?onlyDeleted=true', {}, (error, response) => {
            if (error) return next()
            _.each(response.groupes, (groupe) => {
              const group = sesalab.behaviors.Groupe.create(groupe)
              const groupNode = self.getGroupNode(self.groupesFolderNode, group)
              _.each(groupe.utilisateurs, (eleve) => {
                eleve = sesalab.behaviors.Eleve.create(eleve)
                self.getEleveNode(groupNode, eleve)
              })
            })
            return next()
          })
        })
        // Création de l'arbre pour les ressources supprimées
        // .seq(function() {
        //   var next = this;
        //   // TODO Faire un appel similiare à getListePerso pour ne récupérer que les ressources soft deleted
        //   // de cet utilisateur (ou possédant les droits d'accès)
        //   // TODO Traiter le cas où l'utilisateur pne possède aucun ressource softDeleted
        //   // var folders = app.utilisateur.ressourcesFolders.folders;
        //   var ressourcesFolder = sesalab.behaviors.Dossier.create({
        //     nom: 'Ressources',
        //     folders: [],
        //     uuid: uuid()
        //   });
        //   var ressourcesFolderNode = self.getFolderNode(ressourcesFolder);
        //   return next();
        // })
        // Création de l'arbre pour les séquences supprimées
        .seq(function () {
          const next = this
          const sequencesFolder = sesalab.behaviors.Dossier.create({
            nom: 'Séquences',
            folders: [],
            uuid: uuid()
          })
          const sequencesFolderNode = self.getFolderNode(sequencesFolder)
          self.sequencesFolderNode = sequencesFolderNode

          flow()
            .seq(function () {
              self.getAllDeletedSequences('sequences?deletedOnly=true', [], this)
            })
            .seq(function (deletedSequences) {
              _.each(deletedSequences, (sequence) => {
                // Seules les séquences de la structure actuelle sont affichées
                if (sequence.structure === app.structure.oid) self.getSequenceNode(sequencesFolderNode, sequence)
              })
              this()
            })
            .done(next)
        })
        .seq(function () {
          self.tree.openNode(self.tree)
          if (callback) {
            callback()
          }
        })
        .catch(app.errorHandler('Une erreur s’est produite durant la récupération des éléments en corbeille'))
    },
    onSelectionChanged: function (selection) {
      const b = sesalab.behaviors
      if (selection.length < 1) return
      const isEleve = selection[0].parentNode && selection[0].instanceOf(b.Eleve) && selection[0].parentNode.instanceOf(b.Dossier)
      const isClass = selection[0].parentNode && selection[0].instanceOf(b.Groupe) && selection[0].parentNode.parentNode.getCaption() === 'Classes'
      const isGroup = selection[0].parentNode && selection[0].instanceOf(b.Groupe) && selection[0].parentNode.getCaption() === 'Groupes'
      const isRessource = selection.length === 1 && selection[0].instanceOf(b.Ressource)
      const isSequence = selection[0].instanceOf(b.Sequence)

      this.buttonEnabled('onRestoreStudents', isEleve)
      this.buttonEnabled('onRestoreClass', isClass)
      this.buttonEnabled('onRestoreGroup', isGroup)
      this.buttonEnabled('onRestoreRessource', isRessource)
      this.buttonEnabled('onRestoreSequences', isSequence)
    },

    getFolderNode: function (folder) {
      return this.tree.need(folder, { uuid: folder.uuid })
    },
    getLevelNode: function (folderNode, niveauId) {
      const niveau = sesalab.common.Niveaux.getInstance().grabById(niveauId)
      return folderNode.need(niveau, { id: niveauId })
    },
    getGroupNode: function (nodeLevel, groupe) {
      return nodeLevel.need(groupe, { oid: groupe.oid })
    },
    getEleveNode: function (node, eleve) {
      return node.need(eleve, { oid: eleve.oid })
    },
    getFolderInFolderNode: function (folderNode, folder) {
      return folderNode.need(folder, { uuid: folder.uuid })
    },
    getRessourceNode: function (folderNode, ressource) {
      ressource.$behavior = new sesalab.behaviors.Ressource(ressource)
      return folderNode.need(ressource, { rid: ressource.rid })
    },
    getSequenceNode: function (folderNode, sequence) {
      sequence.$behavior = new sesalab.behaviors.Sequence(sequence)
      return folderNode.need(sequence, { oid: sequence.oid })
    },

    onRestoreStudents: function () {
      const nodes = this.tree.getSelectedNodes()
      const nodesOids = _.map(nodes, (node) => { return node.data.oid })

      app.sesalab.put(`eleve/${nodesOids.join(',')}/restore`, {}, {},
        app.errorHandler('Une erreur est survenue pendant la restauration des élèves', (error, response) => {
          if (error) {
            console.error(error)
            app.toaster.toast(error.message, 'error')
            return
          }

          // Mise à jour des panneaux "MesClasses", "Gestion des élèves" ainsi que la corbeille
          app.classesPanel.buildTreeAndOpenNode(nodes[0])
          app.bus.broadcast('gestion-eleves::refresh')

          // Suppression de l'élève dans la corbeille dans le noeud "Eleves" mais aussi dans le noeud "Classes" si besoin
          _.each(response.eleves, (eleve) => {
            const eleveNode = this.findNode(this.classesFolderNode, eleve.oid)
            if (eleveNode) eleveNode.remove()
          })

          // Mise à jour de la vue et notifications
          _.each(response.eleves, (eleve) => {
            let message = `L’élève ${eleve.nom} ${eleve.prenom} a été restauré`
            if (eleve.classe && !eleve.classe.__deletedAt) {
              message += ` dans sa classe d'origine (${eleve.classe.nom})`
            }
            app.toaster.toast(message)

            const node = _.find(nodes, (node) => { return node.data.oid === eleve.oid })
            node.remove()
          })
          this.tree.refresh()
        })
      )
    },
    onRestoreClass: function () {
      const nodes = this.tree.getSelectedNodes()
      this.restoreGroups(nodes)
    },
    onRestoreGroup: function () {
      const nodes = this.tree.getSelectedNodes()
      this.restoreGroups(nodes)
    },
    restoreGroups: function (nodes) {
      if (nodes.length === 0) {
        console.error(new Error('Aucun élément à restaurer'))
        return
      }

      const self = this
      const nodesOids = _.map(nodes, (node) => { return node.data.oid })
      let classes
      let groupes

      flow()
        .seq(function () {
          app.sesalab.put(`groupe/${nodesOids.join(',')}/restore`, {}, {}, this)
        })
        .seq(function (response) {
          classes = _.filter(response.groupes, (groupe) => { return groupe.isClass })
          groupes = _.filter(response.groupes, (groupe) => { return !groupe.isClass })

          // On met à jour les panneaux "Mes classes" et "Gestion des élèves"
          if (classes.length) {
            const firstNode = _.find(nodes, (node) => { return node.data.oid === classes[0].oid })
            app.classesPanel.buildTreeAndOpenNode(firstNode)
            app.bus.broadcast('gestion-eleves::refresh')
          }

          if (groupes.length) {
            const parentFolder = app.mesGroupes.getDefaultFolder()
            _.each(groupes, (groupe) => {
              const groupeBehavior = sesalab.behaviors.Groupe.create(groupe)
              const groupeNode = app.mesGroupes.needGroupNode(parentFolder, groupeBehavior)

              const oldNode = _.find(nodes, (node) => { return node.data.oid === groupe.oid })
              const eleves = oldNode.data.utilisateurs
              for (let i = 0; i < eleves.length; i++) {
                eleves[i].$behavior = new sesalab.behaviors.Eleve(eleves[i])
                app.mesGroupes.needStudentNode(groupeNode, eleves[i])
              }
            })

            app.bus.broadcast('mesgroupes::refresh')
          }

          this(null, response.groupes)
        })
        .seq(function (responseGroupes) {
          // Supprime les élèves du noeud contenant les élèves
          _.each(classes, (classe) => {
            const node = _.find(nodes, (node) => { return node.data.oid === classe.oid })
            const classesStudentsOids = _.map(node.getChildren().toArray(), (node) => {
              return node.data.oid
            })

            const elevesNode = _.clone(self.elevesFolderNode.getChildren().toArray())
            _.each(elevesNode, (node) => {
              if (node && classesStudentsOids.indexOf(node.data.oid) !== -1) {
                node.remove()
              }
            })

            // Supprime le Dossier avec le niveau si celui-ci est vide
            if (node.parentNode.getChildren().toArray().length === 0) {
              node.parentNode.remove()
            }
          })

          // Notification + suppressions
          _.each(responseGroupes, (groupe) => {
            if (groupe.isClass) app.toaster.toast(`La classe ${groupe.nom} a été restaurée`)
            else app.toaster.toast(`Le groupe ${groupe.nom} a été restauré`)

            const node = _.find(nodes, (node) => { return node.data.oid === groupe.oid })
            node.remove()
          })
          self.tree.refresh()

          // Sauvegarde du profil
          app.utilisateur.store(
            app.errorHandler('Une erreur est survenue pendant la sauvegarde du profil')
          )
        })
        .catch(app.errorHandler('Une erreur s’est produite durant la restauration de l’élément'))
    },
    onRestoreRessource: function () {
      // TODO Enlever le soft delete de la ressource sur la sésathèque, et la faire réapparaître dans le menu "Mes ressources"
      // Si elle était publique ou partagée dans un groupe, tant pis il faudra recommencer la manip
    },
    onRestoreSequences: function () {
      const self = this
      const nodes = self.tree.getSelectedNodes()
      const nodesOids = _.map(nodes, (node) => { return node.data.oid })

      flow()
        .seq(function () {
          app.sesalab.put(`sequence/${nodesOids.join(',')}/restore`, {}, {}, this)
        })
        .seq(function (response) {
          const nonTrieFolder = app.mesSequences.getDefaultFolder()
          _.each(response.sequences, (sequence) => {
            app.toaster.toast('La séquence ' + sequence.nom + ' a été restaurée')
            self.getSequenceNode(nonTrieFolder, sequence)

            const node = _.find(nodes, (node) => { return node.data.oid === sequence.oid })
            if (node) node.remove()
          })

          // On met à jour la vue dans le panneau "Mes Séquences"
          app.mesSequences.store()
          app.mesSequences.tree.refresh()
          self.buildTree()
        })
        .catch(app.errorHandler('Une erreur s’est produite durant la restauration de la séquence'))
    }
  }
})
