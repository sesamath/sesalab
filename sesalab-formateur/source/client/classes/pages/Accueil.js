/**
 * L'onglet accueil présent par défaut au chargement de /formateur/
 */
qx.Class.define('sesalab.pages.Accueil', {
  extend: sesalab.widgets.ScrollablePage,
  construct: function () {
    this.base(arguments, 'Accueil', 'home')
    this.set({
      padding: 0,
      allowGrowY: true,
    })
    // le bandeau jaune avec les infos de la structure
    sesalab.desklets.Structure.appendTo(this)
    // les blocs
    // - users à valider
    // - infos acad
    // - dernières séquences
    // - derniers bilans
    // sesalab.desklets.News.appendTo(this)
    sesalab.desklets.News.appendTo(this)
    // - le contenu spécifique à cette instance (qui vient de _private/content/homeInfo.inc.html)
    sesalab.desklets.HomeInfo.appendTo(this)
  },
  members: {
    getId: function () {
      return 'accueil'
    },

    isClosable: function () {
      return false
    }
  }
})
