/**
 * L'onglet accueil présent par défaut au chargement de /formateur/
 */
qx.Class.define('sesalab.pages.Accueil', {
  extend: sesalab.widgets.ScrollablePage,
  construct: function () {
    this.base(arguments, 'Accueil', 'home')
    this.set({
      padding: 0
    })

    sesalab.desklets.Structure.appendTo(this)
    sesalab.desklets.News.appendTo(this)
    sesalab.desklets.HomeInfo.appendTo(this)
  },
  members: {
    getId: function () {
      return 'accueil'
    },

    isClosable: function () {
      return false
    }
  }
})
