/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
qx.Class.define('sesalab.pages.Bilan', {
  extend: sesalab.widgets.Page,
  construct: function (sequence, eleve) {
    this.sequence = sequence
    this.eleve = eleve

    // Si c'est une séquence, on veut le bilan pour cette séquence. Si c'est un éleve, on demande le bilan des exercices en autonomie de cet élève
    const nom = sequence ? sequence.nom : `${eleve.prenom} ${eleve.nom}`
    this.base(arguments, nom, 'combo_chart')

    let frame
    if (sequence && !eleve) {
      frame = app.createIframe(`/eleve/#!/formateur/bilan/liste?sequence=${sequence.oid}`)
    }
    if (!sequence && eleve) {
      frame = app.createIframe(`/eleve/#!/formateur/bilan/liste?eleve=${eleve.oid}`)
    }
    this.add(frame, { flex: 1 })
  },
  members: {
    getId: function () {
      if (this.sequence && !this.eleve) {
        return 'bilan-sequence-' + this.sequence.oid
      }
      if (!this.sequence && this.eleve) {
        return 'bilan-eleve-' + this.eleve.oid
      }
    }
  }
})
