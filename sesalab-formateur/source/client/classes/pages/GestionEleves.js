const _ = require('lodash')
const flow = require('an-flow')
const moment = require('moment')

qx.Class.define('sesalab.pages.GestionEleves', {
  extend: sesalab.widgets.ScrollablePage,
  include: [sesalab.widgets.Helper],

  construct: function () {
    this.base(arguments, 'Gestion des élèves', 'add_people')

    // Accès à la page d'aide pour la corbeille
    const helpLabel = new qx.ui.basic.Atom('xx', 'icons/20/help.png')
    helpLabel.set({
      cursor: 'pointer',
      decorator: 'tooltip',
      label: 'Pour plus d’informations, cliquez-ici',
      margin: [15, 15, 5, 15],
      padding: 5,
      rich: true
    })
    helpLabel.addListener('click', () => {
      app.openIframePage('Aide gestion élèves', 'help', app.settings.aide.panneau.classes, 'help-gestion-eleves')
    })
    this.add(helpLabel)

    // Warning label
    const warningLabel = new qx.ui.basic.Atom('xx', 'icons/20/info.png')
    warningLabel.set({
      decorator: 'tooltip',
      label: 'Attention, les modifications des classes affectent tous les enseignants !',
      margin: [5, 15, 5, 15],
      padding: 5,
      rich: true
    })
    this.add(warningLabel)

    // rappel de la date de purge
    let purgeLabel = 'La prochaine <strong>date de purge des comptes élèves</strong> est fixée au'
    if (app.structure.datePurge) {
      purgeLabel += ` <strong>${moment(app.structure.datePurge).format('DD/MM/YYYY')}</strong><br> (modifiable dans la configuration établissement)`
    }
    // cf http://www.qooxdoo.org/5.0.2/api/#qx.ui.basic.Atom
    const purgeTooltip = new qx.ui.basic.Atom(purgeLabel, 'icons/20/info.png')
    purgeTooltip.set({
      decorator: 'tooltip',
      margin: [5, 15, 5, 15],
      padding: 5,
      rich: true
    })
    this.add(purgeTooltip)
    // si y'avait pas de date de purge faut arrêter là
    if (!app.structure.datePurge) {
      console.error(Error('Aucune date de purge pour cet établissement'), app.structure)
      purgeTooltip.set({
        icon: 'icons/20/high_priority.png',
        label: '<strong>Aucune date de purge fixée, il faut d’abord en choisir une dans la configuration établissement</strong>'
      })
      return
    }

    // Affichage sous forme d'onglets
    const tabView = new qx.ui.tabview.TabView()
    tabView.setWidth(500)
    tabView.setHeight(500)
    tabView.set({
      margin: 10
    })

    const pageGestion = new qx.ui.tabview.Page('Gérer les élèves', 'icons/16/tree_structure.png')
    const pageImport = new qx.ui.tabview.Page('Importer', 'icons/16/import.png')
    const pageExport = new qx.ui.tabview.Page('Exporter', 'icons/16/export.png')
    pageGestion.setLayout(new qx.ui.layout.VBox())
    pageImport.setLayout(new qx.ui.layout.VBox())
    pageExport.setLayout(new qx.ui.layout.VBox())

    // Titre et toolbar pour la gestion des classes
    this.createGestionContainer()
    pageGestion.add(this.gestionContainer)
    // Arbre des classes
    this.createTreeContainer()
    pageGestion.add(this.treeContainer, { flex: 1 })
    // Import d'élèves
    this.createImportContainer()
    pageImport.add(this.importContainer, { flex: 1 })
    // Export d'élèves
    this.createExportContainer()
    pageExport.add(this.exportContainer, { flex: 1 })

    tabView.add(pageGestion)
    tabView.add(pageImport)
    tabView.add(pageExport)
    this.add(tabView)
  },

  members: {
    getId: function () {
      return 'gestion-eleves'
    },
    createGestionContainer: function () {
      this.gestionContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox())
      this.gestionContainer.setPadding(0, 10, 0, 10)
      const titre = new qx.ui.basic.Label('Gestion des classes')
      titre.setFont('bold')
      this.gestionContainer.add(titre)

      // Ajout du bouton pour créer une classe
      this.toolbar = new qx.ui.toolbar.ToolBar()
      this.gestionContainer.add(this.toolbar)
      this.toolbar.set({
        padding: 0,
        show: 'icon'
      })
      const addClassButton = new qx.ui.toolbar.Button('Ajouter une classe', 'icons/20/add_people.png')
      this.toolbar.add(addClassButton)
      addClassButton.set({ toolTipText: 'Ajouter une classe' })
      addClassButton.addListener('click', this.onAddClass, this)
    },
    createTreeContainer: function () {
      this.treeContainer = new qx.ui.container.Composite(new qx.ui.layout.Grow())
      this.tree = new sesalab.widgets.Tree()
      this.treeContainer.add(this.tree)

      // Fonction de tri par niveau puis par classe (ordre alphabétique)
      const niveauxManager = sesalab.common.Niveaux.getInstance()
      const sorter = (a, b) => {
        // Tri alphabétique
        if (a.data.oid || b.data.oid) {
          return a.getCaption().toLowerCase().localeCompare(b.getCaption().toLowerCase())
        }

        // Tri par niveau
        return niveauxManager.findOrder(a.data.$$user_intitule) > niveauxManager.findOrder(b.data.$$user_intitule)
      }

      this.tree.set({ sorted: sorter, hideRoot: true, showTopLevelOpenCloseIcons: true })
      this.tree.addListener('dblclick', this.fireDefaultAction, this)
      this.tree.addListener('pointerover', function (e) { app.displayEleveInformation(e, this) }, this)

      // Ajout des actions
      this.menuButton('onCreateClass', 'Ajouter une classe', 'add')

      this.menuButton('onAddStudent', 'Ajouter un élève', 'add')
      this.menuButton('onExportBilans', 'Exporter les bilans', 'combo_chart')
      this.menuButton('onModifyGroup', 'Modifier', 'edit', true)
      this.menuButton('onDeleteGroup', 'Mettre dans la corbeille', 'empty_trash')

      this.menuButton('onModifyStudent', 'Modifier', 'edit', true)
      this.menuButton('onShowStudentBilan', 'Afficher les bilans', 'combo_chart')
      this.menuButton('onDeleteStudent', 'Mettre dans la corbeille', 'empty_trash')

      app.bus.on('gestion-eleves::refresh', function () {
        this.buildTree()
      }, this)

      this.isBuildingTree = false
      this.buildTree()
    },
    buildTree: function () {
      if (this.isBuildingTree) return
      this.isBuildingTree = true
      this.tree.removeAll()
      sesalab.pages.GestionEleves.buildTree(this, false, false)
    },
    createImportContainer: function () {
      this.importContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox())
      const titre = new qx.ui.basic.Label('Import d’élèves')
      titre.setFont('bold')
      this.importContainer.add(titre)

      // Message de description pour l'import
      this.importMessage = new qx.ui.basic.Label(`
<p class="warning">Si vous utilisez une <strong>connexion via un ENT</strong>, il ne faut pas importer les comptes élèves, ils seront automatiquement créés à leur première connexion (tout import ici génèrera des élèves en double, sous deux identifiants différents, celui généré par l'import et celui de l'ENT).</p>
        <p>Trois imports sont possibles</p>
        <ul>
          <li>copier / coller depuis un fichier tableur</li>
          <li>avec le fichier csv de ONDE pour le primaire</li>
          <li>avec le fichiers xml de SIECLE pour le secondaire</li>
        </ul>
        <p>Voici quelques éléments pour vous permettre de choisir :</p>
        <ul>
          <li>
            Utiliser les fichiers SIECLE (secondaire de l'enseignement public français)
            ou ONDE (primaire de l'enseignement public français) à chaque modification des élèves dans l'établissement
            permettra un suivi des changements de classes, les nouveaux élèves seront créés et les élèves ayant disparu
            du fichier seront déplacés dans une classe "Anciens élèves" afin de laisser l'accès possible aux scores,
            jusqu'à suppression manuelle définitive. C'est la méthode préconisée pour toutes les structures qui y ont accès.
            Cependant, elle ne permet pas de fixer les identifiants (ni le mot de passe initial).
          </li>
          <br />
          <li>
            Un import tableur est aussi disponible pour les structures qui ne disposent pas de ONDE / SIECLE, ou qui souhaitent initialiser des identifiants identique à une autre application (les élèves doivent toujours changer leur mot de passe à la première connexion). Les imports tableurs peuvent permettre de changer les élèves de classe mais n'agissent pas sur les élèves absents du fichier : il est donc possible de faire des imports partiels tout au long de l'année pour des élèves à ajouter, les autres élèves ne seront pas modifiés.
          </li>
          <br />
          <li>
            Il est possible de combiner les deux méthodes en gérant la création des comptes avec des imports SIECLE/ONDE, 
            puis des imports tableurs <strong>contenant les identifiants</strong> (repartir d'un export ou d'un rapport d'importation), car sans identifiant un import tableur ne modifie jamais un élève existant (il en crée toujours un nouveau).
          </li>
        </ul>`)
      this.importMessage.set({ rich: true, selectable: true })
      this.importContainer.add(this.importMessage)

      /* On supprime la possibilité de tout purger qui crée bien plus de pb qu'autre chose * /
      const label = new qx.ui.basic.Label(`Avant un éventuel import, vous pouvez mettre à la corbeille
          l’intégralité des élèves et classes de l’établissement, mais dans ce cas
          <strong>attention</strong> à sauvegarder d’abord d’éventuels onglets en cours d’édition car la page sera rechargée après la suppression des comptes.`)
      label.set({ rich: true, selectable: true })
      this.importContainer.add(label)

      // Création d'un bouton de purge
      const purgeContainer = new qx.ui.container.Composite(new qx.ui.layout.HBox(1))
      this.importContainer.add(purgeContainer)
      const btnPurge = new qx.ui.form.Button('Mettre à la corbeille toutes les classes et leurs élèves')
      btnPurge.set({ width: 300 })
      btnPurge.addListener('click', function () {
        const message = `Vous êtes sur le point de mettre tous les comptes élèves dans la corbeille,
          cette action concerne tous les formateurs.<br />
          <strong>Attention à enregistrer toutes vos modifications en cours</strong> car l’interface se rechargera pour se réinitialiser.<br />
          Voulez-vous continuer ?`
        const options = {
          no: 'Ne rien faire',
          yes: 'Mettre tous les comptes élève dans la corbeille',
          isCancelFocused: true
        }
        // (toujours null pour AskDialog)
        // eslint-disable-next-line handle-callback-err
        const dialogCb = (error, isPurgeWanted) => {
          if (!isPurgeWanted) return
          const body = { structureOid: app.structure.oid }
          const apiCb = (error, response) => {
            // Dans tous les cas on rechargera l'interface après affichage d'un message
            let alertMessage
            if (error) {
              console.error(error)
              alertMessage = error.message || error
            } else if (response && response.message) {
              alertMessage = response.message.replace(/\n/g, '<br />')
            } else {
              console.error(new Error('Réponse de purge incohérente'), response)
              alertMessage = 'La réponse du serveur n’est pas au format attendu'
            }
            sesalab.widgets.AlertDialog.execute(alertMessage, () => window.location.reload())
          }
          app.sesalab.post('purge-eleves', body, {}, apiCb)
        }
        sesalab.widgets.AskDialog.execute(message, dialogCb, options)
      })
      purgeContainer.add(btnPurge)
      /* */

      const labelMeth = new qx.ui.basic.Label('Choisissez votre méthode d’import')
      labelMeth.set({ marginTop: 10 })
      this.importContainer.add(labelMeth)

      // Création des boutons d'import
      const buttonsContainer = new qx.ui.container.Composite(new qx.ui.layout.HBox(5))
      this.importContainer.add(buttonsContainer)

      const btnImportTableur = new qx.ui.form.Button('Import tableur')
      btnImportTableur.set({ width: 150 })
      btnImportTableur.addListener('click', function () {
        const importTableurPage = new sesalab.pages.ImportTableur()
        app.openPage(importTableurPage)
      })
      buttonsContainer.add(btnImportTableur)

      const btnImportOnde = new qx.ui.form.Button('Import ONDE')
      btnImportOnde.set({ width: 150 })
      btnImportOnde.addListener('click', function () {
        const importOndePage = new sesalab.pages.ImportOfficiel('onde')
        app.openPage(importOndePage)
      })
      buttonsContainer.add(btnImportOnde)

      const btnImportSiecle = new qx.ui.form.Button('Import SIECLE')
      btnImportSiecle.set({ width: 150 })
      btnImportSiecle.addListener('click', function () {
        const importSieclePage = new sesalab.pages.ImportOfficiel('siecle')
        app.openPage(importSieclePage)
      })
      buttonsContainer.add(btnImportSiecle)
    },
    createExportContainer: function () {
      this.exportContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox())
      const titre = new qx.ui.basic.Label('Export d’élèves')
      titre.setFont('bold')
      this.exportContainer.add(titre)

      // Message de description pour l'export
      this.exportMessage = new qx.ui.basic.Label(`
        <p>Il est possible d'exporter au format CSV tous les comptes élèves de l'établissement (choisir l’encodage utf8 à l'ouverture du csv dans le tableur sinon il y aura des problèmes sur les caractères accentués).</p>
        <p><strong>Cet export ne contient aucun mot de passe</strong>, car il sont stockés sous forme chiffrés et ne peuvent être retrouvés, mais la colonne existe pour vous permettre d’imposer de nouveaux mots de passe lors d'un import ultérieur (cf l’<a href="https://aide.labomep.sesamath.net/doku.php?id=menu_principal:gestion_des_eleves#reinitialisation_massive_de_mots_de_passe" target="_blank">aide</a>).</p>`)
      this.exportMessage.set({ rich: true, selectable: true })
      this.exportContainer.add(this.exportMessage)

      // Création d'un bouton pour l'export
      const buttonContainer = new qx.ui.container.Composite(new qx.ui.layout.HBox(1))
      this.exportContainer.add(buttonContainer)
      const buttonExport = new qx.ui.form.Button('Exporter tous les comptes élèves')
      buttonExport.set({ width: 300 })
      buttonExport.addListener('click', () => {
        const link = document.createElement('a')
        document.body.appendChild(link)
        link.style = 'display: none'
        link.href = `/api/eleves/by-structure/export/${app.structure.oid}`
        link.download = 'export_comptes_eleves_utf8.csv'
        link.click()
      })
      buttonContainer.add(buttonExport)
    },

    onSelectionChanged: function (selection) {
      const b = sesalab.behaviors
      const uniqSelection = selection.length === 1
      const isNiveau = uniqSelection && selection[0].instanceOf(b.Niveau)
      const isClass = uniqSelection && selection[0].instanceOf(b.Groupe) && selection[0].data.oid !== undefined
      const isStudent = uniqSelection && selection[0].instanceOf(b.Eleve)

      this.buttonEnabled('onCreateClass', isNiveau)
      this.buttonEnabled(['onAddStudent', 'onExportBilans', 'onModifyGroup', 'onDeleteGroup'], isClass)
      this.buttonEnabled(['onDeleteStudent', 'onShowStudentBilan'], isStudent)
      this.buttonEnabled('onModifyStudent', isStudent && !selection[0].data.externalMech)
    },

    onAddClass: function () {
      const groupe = sesalab.behaviors.Groupe.create()
      const dialog = new sesalab.widgets.PropertyEditorDialog('Ajouter une classe', groupe)
      dialog.execute(() => {
        groupe.isClass = true
        groupe.structure = app.structure.oid
        app.sesalab.put('groupe', groupe, {}, (error, response) => {
          if (error) {
            if (error.field) return dialog.error(error)
            console.error(error)
            return app.toaster.toast(error, 'error')
          }
          const group = new sesalab.behaviors.Groupe.create(response.groupe)
          const nodeLevel = this.getLevelNode(response.groupe.niveau)
          this.getGroupNode(nodeLevel, group)
          app.toaster.toast('La classe a été ajoutée')
          this.tree.refresh()
          dialog.close()
        })
      })
    },

    onCreateClass: function () {
      const niveau = this.tree.getSelectedNode()
      if (!niveau) return app.toaster.toast('Aucun niveau, création de classe ignorée')
      const groupe = sesalab.behaviors.Groupe.create()
      sesalab.widgets.InputDialog.execute('Ajouter une classe', 'Nom', null, (value) => {
        groupe.nom = value
        groupe.niveau = niveau.data.getId() || 0
        groupe.structure = app.structure.oid
        groupe.isClass = true
        app.sesalab.put('groupe', groupe, {}, (error, response) => {
          if (error) return app.errorHandler(error.message)(error)

          const group = new sesalab.behaviors.Groupe.create(response.groupe)
          const nodeLevel = this.getLevelNode(response.groupe.niveau)
          this.getGroupNode(nodeLevel, group)
          app.toaster.toast('La classe ' + groupe.nom + ' a été ajoutée')
          this.tree.refresh()
        })
      })
    },

    onAddStudent: function () {
      const classeNode = this.tree.getSelectedNode()
      if (!classeNode) {
        console.error(new Error('Selection invalide (classNode null)'))
        return
      }

      let utilisateur = {
        classe: classeNode.data.oid,
        structures: [app.structure.oid],
        type: 0
      }

      const tmp = sesalab.behaviors.Eleve.create(utilisateur)
      const dialog = new sesalab.widgets.PropertyEditorDialog('Ajouter un élève', tmp)
      dialog.execute(() => {
        const self = this
        flow()
          .seq(function () {
            dialog.validateButton.setEnabled(false)
            app.sesalab.post('eleve', tmp, {}, this)
          })
          .seq(function (response) {
            utilisateur = response.utilisateur
            classeNode.data.utilisateurs.push(utilisateur)
            app.sesalab.put('groupe', classeNode.data, {}, this)
          })
          .seq(function () {
            dialog.validateButton.setEnabled(true)
            dialog.close()
            utilisateur = sesalab.behaviors.Eleve.create(utilisateur)
            const node = self.getNodeEleve(classeNode, utilisateur)
            self.tree.openNodeAndParents(node)
            app.classesPanel.buildTreeAndOpenNode(node)
            app.toaster.toast('L’élève a été ajouté')
          })
          .catch(function (error) {
            dialog.validateButton.setEnabled(true)
            if (error && error.field) return dialog.error(error)
            else if (error) return app.errorHandler(error.message)(error)
          })
      })
    },
    onExportBilans: function () {
      const classeNode = this.tree.getSelectedNode()
      app.exportGroupBilans(classeNode)
    },
    onModifyGroup: function () {
      const source = this.tree.getSelectedObject()
      if (!source || !source.$behavior) return console.error(new Error('Impossible de modifier le groupe sélectionné (data null)'))
      const data = source.$behavior.clone()
      const dialog = new sesalab.widgets.PropertyEditorDialog('Modifier une classe', data)
      dialog.execute(() => {
        app.sesalab.put('groupe', data, {}, (error, response) => {
          if (error) {
            dialog.setErrorMessage(error.message)
            return
          }
          source.$behavior.apply(response.groupe)
          dialog.close()
          this.buildTree()
          app.classesPanel.buildTreeAndOpenNode(source)
        })
      })
    },
    onDeleteGroup: function () {
      const tree = this.tree
      const node = tree.getSelectedNode()
      if (!node || !node.data) {
        console.error(new Error('Impossible de supprimer le groupe sélectionné (data null)'))
        return
      }

      sesalab.widgets.ConfirmationDialog
        .execute('Êtes-vous certain de vouloir mettre le groupe ' + node.data.nom + ' dans la corbeille ?',
          () => {
            flow()
              .seq(function () {
                app.sesalab.del('groupe/' + node.data.oid, {}, this)
              })
              .seq(function () {
                node.data.$behavior.remove()
                node.remove()
                if (node.parentNode.nbChildren() === 0) {
                  node.parentNode.remove()
                }
                tree.refresh()
                app.classesPanel.buildTreeAndOpenNode(node.parentNode.parentNode)
                app.bus.broadcast('corbeille::refresh', node)
              })
              .fail(function (error) {
                app.toaster.toast(error.message, 'error')
                if (typeof error === 'string') console.error(new Error(error))
                else console.error(error)
              })
          })
    },

    onModifyStudent: function () {
      const source = this.tree.getSelectedObject()
      if (!source) return console.error(new Error('La selection est invalide'))
      const data = source.$behavior.clone()
      if (!data || !data.oid) return console.error(new Error('Impossible de modifier la sélection (data null)'))
      // Evite l'interface de modification pour un élève en SSO
      if (data.externalMech) return console.error(Error('Cet élève est géré ailleurs, il ne devrait pas avoir d’action "Modifier"'))

      const dialog = new sesalab.widgets.PropertyEditorDialog('Modifier un élève', data)
      dialog.execute(() => {
        source.$behavior.apply(data)
        data.structures = [app.structure.oid]
        app.sesalab.put('eleve/' + data.oid, data, {}, (error) => {
          if (error) return dialog.error(error)
          source.password = ''
          dialog.close()
        })
      })
    },
    onDeleteStudent: function () {
      const node = this.tree.getSelectedNode()
      if (!node || !node.data || !node.data.oid) return app.toaster.toast('Aucun élève à supprimer')

      const eleve = node.data
      sesalab.widgets.ConfirmationDialog
        .execute('Êtes-vous certain de vouloir mettre l’élève ' + eleve.nom + ' ' + eleve.prenom + ' dans la corbeille ?',
          () => {
            app.sesalab.del('utilisateur/' + eleve.oid, {}, () => {
              node.remove()
              eleve.$behavior.remove()

              const groupe = node.parentNode.data.$behavior
              groupe.removeEleve(eleve)

              this.tree.refresh()
              app.classesPanel.buildTreeAndOpenNode(node.parentNode)
              app.bus.broadcast('corbeille::refresh', node)
            })
          })
    },
    onShowStudentBilan: function () {
      const node = this.tree.getSelectedNode()
      if (!node || !node.data) return app.toaster.toast('Aucun élève, impossible d’afficher son bilan')
      app.showStudentBilan(node.data)
    },

    /**
     * Retourne 'eleve-groupe' si source est un élève et target un groupe, undefined sinon
     * @param source
     * @param target
     * @return {string|undefined}
     */
    eventToAction: function (source, target) {
      if (!source || !source.data || !source.data.$behavior || !target) return app.toaster.toast('Données invalides, impossible d’effectuer l’action demandée')
      const b = sesalab.behaviors
      const sourceClass = source.data.$behavior.constructor
      if (sourceClass === b.Eleve && target === b.Groupe) return 'eleve-groupe'
    },

    onOver: function (e) {
      if (!e || !e.sources || !e.sources[0] || !e.targetClass) return false
      return this.tree.hasAllElementsSameType(e.sources) && this.eventToAction(e.sources[0], e.targetClass)
    },

    onDrop: function (e) {
      if (!e || !e.target || !e.target.data || e.target.data.oid === undefined || !e.sources || !e.targetClass) return
      if (!this.tree.hasAllElementsSameType(e.sources)) return

      switch (this.eventToAction(_.head(e.sources), e.targetClass)) {
        case 'eleve-groupe':
          this.changeStudentsClasse(e.sources, e.target)
          break
      }
    },

    /**
     * Change la classe d'un ensemble d'élèves
     *
     * @param {Array.<qx.TreeNode>} students Collection de noeuds élève
     * @param {qx.TreeNode} newClass Noeud de le classe ciblée
     */
    changeStudentsClasse: function (students, newClass) {
      const self = this

      flow()
        .seq(function () {
          const classesStudentsOids = _.map(students, (student) => {
            return student.data.oid
          })
          app.sesalab.put('groupe/' + newClass.data.oid + '/students/' + classesStudentsOids.join(','), {}, {}, this)
        })
        .seq(function () {
          _.each(students, (student) => {
            const classeOrigineNode = student.parentNode

            // On enlève l'élève de sa classe d'origine
            _.pull(student.data.groupes, classeOrigineNode.data.oid)
            classeOrigineNode.data.$behavior.removeEleve(student.data)
            student.remove()

            // On ajoute l'élève dans sa nouvelle classe
            newClass.data.$behavior.addEleve(student.data)
            self.getNodeEleve(newClass, student.data)
          })

          // On ouvre le dossier destinaire
          newClass.tree.refresh()
          self.tree.openNodeAndParents(newClass)

          // Mise à jour du panneau "MesClasses"
          app.classesPanel.buildTreeAndOpenNode(_.head(students))

          this()
        })
        .catch(function (err) {
          app.toaster.toast('Une erreur s’est produite durant le changement de classe')
          console.error('Erreur durant le changement de classe', err)
        })
    },

    getLevelNode: function (niveauId) {
      const niveau = sesalab.common.Niveaux.getInstance().grabById(niveauId)
      const node = this.tree.need(niveau, { id: niveauId })
      return node
    },
    getGroupNode: function (nodeLevel, groupe) {
      const node = nodeLevel.need(groupe, { oid: groupe.oid })
      return node
    },
    getNodeEleve: function (nodeClasse, utilisateur) {
      nodeClasse = nodeClasse || this.tree
      const node = nodeClasse.need(utilisateur, { oid: utilisateur.oid })
      return node
    }
  },

  statics: {
    /**
     * Création d'un arbre d'élèves (utilisé par sesalab.panels.Classes.buildTree).
     *
     * @param {sesalab.widgets.CollapsablePanel} panel Cible
     * @param {boolean} isLoading Indique l'état de l'arbre (gestion d'un loader)
     * @param {function} callback Fonction appelée une fois l'arbre crée
     */
    buildTree (panel, isLoading, callback) {
      /**
       * Construit l'arbre
       * @param {Object} response (cf sesalab-api/source/groupe/controller.js)
       * @param {Object}         response.groupes   liste des groupes, les props sont les oid de groupe
       * @param {Utilisateurs[]} response.orphelins les élèves qui ne sont dans aucun groupe
       * @param {boolean}        response.success
       */
      const build = (response) => {
        // On tri par niveau puis par groupes
        const niveaux = {}
        _.each(response.groupes, (groupe) => {
          const niveau = groupe.niveau || '0'
          if (!niveaux[niveau]) niveaux[niveau] = {}
          niveaux[niveau][groupe.oid] = groupe
        })

        // On affiche l'arbre par niveau puis par groupe
        _.each(niveaux, (groupes, niveau) => {
          const niveauLabel = sesalab.common.Niveaux.getInstance().grabById(niveau)
          const nodeLevel = panel.tree.need(niveauLabel, { id: niveau })

          _.each(groupes, (groupe) => {
            if (!groupe.nom) groupe.nom = 'Sans nom'
            const group = sesalab.behaviors.Groupe.create(groupe)
            const nodeGroup = nodeLevel.need(group, { oid: group.oid })
            // on veut afficher l'effectif après le nom
            nodeGroup.setCaption(`${group.nom} (${group.utilisateurs ? group.utilisateurs.length : 0})`)

            // Évite les potentiels doublons pouvant exister
            groupe.utilisateurs = _.uniqBy(groupe.utilisateurs, (utilisateur) => {
              return utilisateur.oid
            })

            _.each(groupe.utilisateurs, (eleve) => {
              if (eleve) {
                nodeGroup.addChildren(new sesalab.behaviors.Eleve.create(eleve))
              }
            })
          })
        })

        // Orphelins
        if (response.orphelins.length) {
          const groupe = { nom: 'Orphelins', niveau: '0' } // 0 = Autres
          const niveau = sesalab.common.Niveaux.getInstance().grabById(groupe.niveau)
          const nodeLevel = panel.tree.need(niveau, { id: groupe.niveau })
          const group = new sesalab.behaviors.Groupe.create(groupe)
          const nodeGroup = nodeLevel.need(group, { oid: null })

          _.each(response.orphelins, (orphelin) => {
            nodeGroup.addChildren(new sesalab.behaviors.Eleve.create(orphelin))
          })
        }

        // Done
        if (isLoading) panel.showLoader(false)
        panel.isBuildingTree = false
        panel.tree.openNode(panel.tree)
        if (callback) callback()
      }

      // On utilise le contenu en cache s'il existe
      app.sesalab.get(`groupes/by-structure/${app.structure.oid}`, {}, (error, response) => {
        if (error) {
          app.toaster.toast(error.message, 'error')
          if (typeof error === 'string') console.error(new Error(error))
          else console.error(error)
          return
        }

        build(response)
      })
    }
  }
})
