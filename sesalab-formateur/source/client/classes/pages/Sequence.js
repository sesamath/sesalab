const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')
const { notify } = require('sesalab-commun/notify')
const flow = require('an-flow')
const { askImportRid } = require('../../common/ressource')

/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Onglet de gestion d'une séquence.
 */
qx.Class.define('sesalab.pages.Sequence', {
  extend: sesalab.widgets.ScrollablePage,
  include: [sesalab.widgets.Helper],
  construct (sequence) {
    this.base(arguments, sequence.nom, 'database')
    this._hasChanged = false
    this._hasTitleChanged = false
    // pour savoir si après sauvegarde il faut aussi mettre à jour la sequenceModele associée sur la bibli
    this._hasSequenceModeleChanged = false
    this.sequence = sequence
    this.originalSequence = _.clone(sequence)

    // Création de l'arbre
    this.model = new sesalab.widgets.TreeNode(sequence)
    this.tree = new sesalab.widgets.Tree(this.model)
    this.tree.addListener('pointerover', function (e) { app.displayEleveInformation(e, this) }, this)
    this.tree.setOpenMode('dbltap')
    this.getContentElement().setAttribute('id', `sequence-editor-${sequence.oid}`)

    // Création de la barre d'outils
    this.barButton('onAddSubSequence', 'Nouvelle sous-séquence', 'add_workflow', 'sequence-add-sous-sequence')
    this.barButton('onShowResults', 'Afficher le bilan', 'combo_chart')
    this.barButton('onSaveSequence', 'Sauvegarder', 'upload', 'sequence-save')
    this.barButton('onDisplayHelp', 'Aide', 'help')
    this.buttonEnabled(['onAddSubSequence', 'onShowResults', 'onDisplayHelp'], true)
    this.buttonEnabled('onSaveSequence', false)

    this.menuButton('onAddStudents', 'Ajouter des élèves', 'add_people')
    this.menuButton('onTestRessource', 'Tester la ressource', 'process')
    this.menuButton('onShowInformations', 'Plus d’informations', 'about')
    this.menuButton('onNotify', 'Signaler une anomalie', 'support')
    this.menuButton('onDeleteNode', 'Supprimer', 'empty_trash')
    this.menuButton('onImportResource', 'Importer une ressource', 'import')
    this.menuButton('onSplitEleves', 'Dissocier', 'empty_trash')
    this.menuButton('onTestSequence', 'Tester avec la vue de cet élève', 'add_database')
    this.menuButton('onShowStudentBilan', 'Afficher les bilans', 'combo_chart')
    this.menuButton('onSequenceAsNewSequence', 'Utiliser pour une nouvelle séquence', 'add_database')
    this.menuButton('onSerieAsNewSequence', 'Utiliser pour une nouvelle séquence', 'add_database')
    this.menuButton('onSequenceExpandSettings', 'Propager aux descendants', 'add_database')
    this.menuButton('onSerieExpandSettings', 'Propager aux descendants', 'add_database')

    this.createPropertyEditor()

    // Ajout de l'arbre et du property editor au panneau
    this.splitPane = new qx.ui.splitpane.Pane('horizontal')

    this.treeContainer = new qx.ui.container.Composite(new qx.ui.layout.Grow())
    this.treeContainer.add(this.tree)
    this.treeContainer.getContentElement().setAttribute('class', 'tree')
    this.splitPane.add(this.treeContainer)

    this.propertyEditorContainer = new qx.ui.container.Composite(new qx.ui.layout.Grow())
    this.propertyEditorContainer.add(this.propertyEditor)
    this.splitPane.add(this.propertyEditorContainer)
    this.add(this.splitPane, { flex: 1 })

    this.loadTree()
    this.addListener('close', this.onClose)

    if (window.bugsnagClient) {
      const list = window.bugsnagClient.getMetadata('sequence', 'list') ?? []
      list.push(sequence.oid)
      window.bugsnagClient.addMetadata('sequence', { list })
    }
  },
  members: {
    onClose: function () {
      if (this._hasChanged) {
        sesalab.widgets.ConfirmationDialog.execute('Vous n’avez pas sauvegardé. Êtes vous certain de vouloir fermer ?', () => {
          app.removePage(this)
        })
      } else {
        app.removePage(this)
      }

      if (window.bugsnagClient) {
        const list = window.bugsnagClient.getMetadata('sequence', 'list') ?? []
        const index = list.indexOf(this.sequence.oid)
        if (index !== -1) {
          list.splice(index, 1)
          window.bugsnagClient.addMetadata('sequence', { list })
        }
      }
    },

    hasPendingChanges: function () {
      return this._hasChanged
    },

    /**
     * Indique si la page surcharge sa fermeture
     * @return {boolean}
     */
    hasCustomCloseAction: function () {
      return true
    },

    getId: function () {
      return sesalab.pages.Sequence.getId(this.sequence)
    },

    loadTree: async function () {
      for (const i in this.sequence.sousSequences) {
        // @todo dans le cas d'un drop on peut avoir plusieurs `Sous-séance 1` => les renommer
        await this.getNodeSousSequence(this.sequence.sousSequences[i])
      }
      this.initPropagationButtonsAndFlags()
      this.tree.openNode(this.model)
      this.tree.select(this.model)
      if (this.tree.getSelectedNode()) {
        this.propertyEditor.load(this.tree.getSelectedObject())
      }

      // On detecte les exercices présents plusieurs fois dans la séquence
      const resources = this.getAllResources()
      const nbByOid = new Map()
      const titresDoublons = {}
      for (const { data: { rid, titre } } of resources) {
        const nb = nbByOid.get(rid) ?? 0
        nbByOid.set(rid, nb + 1)
        if (nb) {
          titresDoublons[rid] = titre
        }
      }
      for (const [rid, titre] of Object.entries(titresDoublons)) {
        const nb = nbByOid.get(rid)
        app.toaster.toast(`L'exercice ${titre} est présent dans ${nb} sous-séquences différentes, les bilans apparaitront tous sous la même sous-séquence`)
      }
    },
    reload: function (sequence) {
      this.sequence = { ...sequence }
      this.model.getChildren().removeAll()
      // on prend pas le statut (ni les éventuelles dates qui vont avec), ni prioritaire / description / message
      const { nonZapping, minimumReussite, maximumVisionnage, type, lineaire, uniqEleve } = sequence
      this.model.data.$behavior.apply({ nonZapping, minimumReussite, maximumVisionnage, type, lineaire, uniqEleve })
      this.propertyEditor.clear()
      this.loadTree()
      this._needSave()
    },

    createPropertyEditor: function () {
      this.propertyEditor = new sesalab.widgets.PropertyEditor()

      // Ce listener permet d'ajouter des éléments à la fin des property editor
      this.propertyEditor.addListener('load', () => {
        this.showPropagationInformationMessage()
        this.showPropagationButtonIfRequired()
        this.showRessourceInformation()
      })

      let initPropagationTimeout
      this.propertyEditor.addListener('changed', (event) => {
        // le champ modifié (cf behaviors/Sequence)
        const { property } = event.getData()
        if (property === 'nom') {
          this._hasTitleChanged = true
        }
        const hasPropagation = ['type', 'nonZapping', 'minimumReussite', 'maximumVisionnage']
        if (hasPropagation.includes(property)) {
          // On attend qu'il n'y ait plus de changement pendant au moins 500ms avant
          // de recharger l'état des boutons 'propagation'
          // Side-effect: pour un SelectBox, la nouvelle valeur n'est pas encore set dans les data au moment
          //              de l'event 'changed'. Le setTimeout résoud le problème
          if (initPropagationTimeout) { clearTimeout(initPropagationTimeout) }
          initPropagationTimeout = setTimeout(() => {
            this.initPropagationButtonsAndFlags()
            this.showPropagationButtonIfRequired()
          }, 500)
        }
        this._needSave()
      })
    },

    initPropagationButtonsAndFlags: function () {
      const propagationParams = ['nonZapping', 'minimumReussite', 'maximumVisionnage']
      const sequenceNode = this.model
      const surchargeFlag = '* '
      sequenceNode.$propagationButton = false
      sequenceNode.getChildren().forEach((ssSequenceNode) => {
        const serie = ssSequenceNode.serie
        serie.$propagationButton = false

        serie.setCaption('Série')
        serie.getChildren().forEach((ressource) => {
          ressource.$propagationButton = false
          // Une sous-sequence affiche le bouton de propagation dès qu'une ressource est paramétrée différemment
          _.forEach(propagationParams, (p) => {
            if (ssSequenceNode.data[p] !== ressource.data[p]) {
              // Une ressource est flaggée comme surchargée dès qu'elle diffère de la sous-séquence
              ressource.setCaption(surchargeFlag + ressource.data.titre)
              serie.$propagationButton = true
            }
          })

          // Une séquence affiche le bouton de propagation dès qu'une ressource est paramétrée différemment ...
          _.forEach(propagationParams, (p) => {
            if (sequenceNode.data[p] !== ressource.data[p]) {
              sequenceNode.$propagationButton = true
            }
          })
        })

        // ... ou dès qu'une sous-sequence est paramétrée différemment de la séquence (y compris le type)
        _.forEach(propagationParams.concat('type'), (p) => {
          if (sequenceNode.data[p] !== ssSequenceNode.data[p]) {
            sequenceNode.$propagationButton = true
            serie.setCaption(surchargeFlag + 'Série')
          }
        })
      })
    },
    // On ne veut pas que le label soit affiché lorsqu'on clique sur la partie Eleves
    showPropagationInformationMessage: function () {
      const selection = this.tree.getSelectedNode()
      // FIXME vérifier que selection n'est pas false avant d'appeler instanceOf dessus
      const isSequence = selection.instanceOf(sesalab.behaviors.Sequence)
      const isSerie = selection.instanceOf(sesalab.behaviors.Serie)
      const isRessource = selection.instanceOf(sesalab.behaviors.Ressource)
      if (isSequence || isSerie || isRessource) {
        this.propertyEditor.setInformationMessage(`Le symbole * dans les arbres des ressources désigne
          des sous-éléments dont les paramétrages diffèrent des réglages par défaut ci-dessus`)
      }
    },
    showPropagationButtonIfRequired: function () {
      const selection = this.tree.getSelectedNode()
      if (!selection) return

      const isSequence = selection.instanceOf(sesalab.behaviors.Sequence)
      const isSerie = selection.instanceOf(sesalab.behaviors.Serie)
      if ((isSequence || isSerie) && !selection.$propagationButton) {
        this.propertyEditor.removeUserAction()
        return
      }
      if (isSequence || isSerie) {
        this.propertyEditor.removeUserAction()
        this.propertyEditor.addUserAction('Propager les modifications', () => {
          if (isSequence) { this.onSequenceExpandSettings() }
          if (isSerie) { this.onSerieExpandSettings() }

          this.propertyEditor.removeUserAction()
        })
      }
    },
    showRessourceInformation: function () {
      const selection = this.tree.getSelectedNode()
      if (!selection) return app.toaster.toast('Aucune ressource, impossible d’afficher ses informations')

      const isRessource = selection.instanceOf(sesalab.behaviors.Ressource)
      if (isRessource) {
        const ressource = selection.data
        const style = 'style = "border: 1px solid #000; margin: 5px; padding: 5px;"'
        let data = '<div ' + style + '>'
        data += ressource.resume ? '<b>Résumé</b> : ' + ressource.resume : '<i>Pas de résumé</i>'
        data += '</div><div ' + style + '>'
        data += ressource.commentaires ? '<b>Commentaires</b> : ' + ressource.commentaires : '<i>Pas de commentaires</i>'
        data += '</div>'
        const label = new qx.ui.basic.Label(data)
        label.set({ rich: true })
        const container = new qx.ui.container.Composite()
        container.setLayout(new qx.ui.layout.VBox(10))
        container.add(label)
        this.propertyEditor.add(container)
      }
    },

    onSelectionChanged: function (selection) {
      // On réinitialise l'état des boutons pour éviter des états étranges
      this.buttonEnabled(['onTestRessource', 'onShowInformations', 'onAddStudents',
        'onDeleteNode', 'onImportResource', 'onNotify', 'onSplitEleves', 'onShowStudentBilan', 'onTestSequence',
        'onSequenceAsNewSequence', 'onSequenceExpandSettings', 'onSerieAsNewSequence',
        'onSerieExpandSettings'], false)

      // "onSelectionChanged" est appelé automatiquement au démarrage avec une selection vide
      if (selection.length === 0) return

      // Inutile de continuer avec une séléction multiple, sauf pour une suppression
      if (selection.length > 1) {
        this.buttonEnabled('onDeleteNode', this.tree.hasAllElementsSameType(selection))
        this.propertyEditor.clear()
        return
      }

      const b = sesalab.behaviors
      const target = selection[0]
      const isEleve = target.instanceOf(b.Eleve)
      const isElevesContainer = target.instanceOf(b.Eleves)
      const isGroupe = target.instanceOf(b.SequenceGroupe)
      const isRessource = target.instanceOf(b.Ressource)
      const isSequence = target.instanceOf(b.Sequence)
      const isSerie = target.instanceOf(b.Serie)
      const isSousSequence = target.instanceOf(b.SousSequence)

      function getParentSerie (element) {
        return element.serie || getParentSerie(element.parentNode)
      }
      const parentSerie = isEleve && target ? getParentSerie(target) : null

      // Mise à jour du menu contextuel
      this.buttonEnabled(['onNotify', 'onShowInformations', 'onTestRessource'], isRessource)
      this.buttonEnabled('onAddStudents', isElevesContainer)
      this.buttonEnabled('onDeleteNode', (isEleve && !target.data.$fake) || isGroupe || isRessource || isSousSequence)
      this.buttonEnabled('onSplitEleves', isGroupe)
      this.buttonEnabled('onShowStudentBilan', isEleve)
      this.buttonEnabled('onTestSequence', isEleve && parentSerie && !!parentSerie.getChildren().length)
      this.buttonEnabled(['onSequenceAsNewSequence', 'onSequenceExpandSettings'], isSequence)
      this.buttonEnabled(['onSerieAsNewSequence', 'onSerieExpandSettings', 'onImportResource'], isSerie)

      // Mise à jour de l'interface de droite
      if (isSerie) {
        this.propertyEditor.load(
          target.getParent().data,
          (properties) => _.omit(properties, 'nom')
        )
      } else if (isSousSequence) {
        this.propertyEditor.load(
          target.data,
          ({ nom }) => ({ nom })
        )
      } else if (isSequence) {
        this.propertyEditor.load(this.tree.getSelectedObject())
      } else if (isRessource) {
        const resourceSelection = target.data
        resourceSelection.$parent = target.getParent().data
        this.propertyEditor.load(resourceSelection)
      } else {
        this.propertyEditor.clear()
      }
    },
    onAddSubSequence: async function () {
      const result = await this.addSubSequence()
      if (result) {
        app.toaster.toast('La sous-séquence a été ajoutée')
      } else {
        app.toaster.toast('Une erreur est survenue, impossible d’ajouter la sous-séquence')
      }
    },
    addSubSequence: async function (data) {
      try {
        data = data || {}
        this.transfertSettings(this.sequence, data)
        const sousSequence = sesalab.behaviors.SousSequence.create(data)
        const node = await this.getNodeSousSequence(sousSequence)
        this.tree.openNodeAndParents(node)
        this._needSave()
        return true
      } catch (error) {
        console.error(error)
        return false
      }
    },
    onShowResults: function () {
      app.openPage(new sesalab.pages.Bilan(this.sequence))
    },
    onSaveSequence: function () {
      this.saveSequence(() => {
        app.toaster.toast('La séquence est sauvegardée')
        if (this._hasTitleChanged) {
          // faut aussi mettre à jour l'arbre "Mes séquences"
          app.mesSequences.store()
          app.mesSequences.tree.refresh()
        }
      })
    },
    saveSequence: function (cb) {
      // On notifie l'utilisateur par rapport aux séquences étant invalides (sans élèves et/ou sans exercices)
      const sousSequences = this.model.getChildren().toArray()
      for (let i = 0; i < sousSequences.length; i++) {
        const exercises = sousSequences[i].serie.getChildren().toArray().length
        const groupes = sousSequences[i].groupes.getChildren().toArray().length
        if (!exercises || !groupes) {
          app.toaster.toast(`La série ${sousSequences[i].data.nom} ne contient pas d'élèves et/ou d'exercices, celle-ci ne sera pas visible par les élèves`, 'warning')
        }
      }

      const sequence = this.serializeSequence()
      app.sesalab.put('sequence', sequence, {}, (error, result) => {
        if (error) {
          if (error.field) return this.propertyEditor.error(error)
          return app.errorHandler('Une erreur interne s’est produite durant la sauvegarde de la séquence')(error)
        }
        this.model.data.oid = result.sequence.oid
        if (this.sequence && this.sequence.$behavior) this.sequence.$behavior.refreshIcon()
        if (this.originalSequence && this.originalSequence.$behavior) this.originalSequence.$behavior.refreshIcon()

        if (this._hasSequenceModeleChanged === true && this.model.data.hasModel === true) {
          const sequenceModele = sesalab.behaviors.SequenceModele.createFromSequence(result.sequence)
          app.sesatheque.saveSequenceModele(app.sesatheque.baseIdPrivate, sequenceModele, (error, newItem) => {
            if (error) {
              console.error(error)
              app.toaster.toast(error.message, 'error')
            }
            if (newItem && newItem.rid) {
              app.toaster.toast('Votre séquence a été exportée')
            } else {
              console.error(Error('saveSequenceModele ne remonte ni erreur ni ressource avec rid'))
              app.toaster.toast('La séquence n’a pas été correctement exportée', 'error')
            }

            cb()
            this._needSave(false, false)
          })
        } else {
          cb()
          this._needSave(false, false)
        }
      })
    },
    _needSave: function (hasChanged = true, hasSequenceModeleChanged = true) {
      this._hasChanged = hasChanged
      this._hasSequenceModeleChanged = hasSequenceModeleChanged
      this.buttonEnabled('onSaveSequence', hasChanged)
    },
    onDisplayHelp: function () {
      app.openIframePage('Aide séquences', 'help', app.settings.aide.sequences, 'help-sequences')
    },
    onTestRessource: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      const ressource = app.sesatheque.normalize(selectedObject)
      app.testResource(ressource)
    },
    /**
     * Affiche les informations d'une série
     */
    onShowInformations: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      const ressource = app.sesatheque.normalize(selectedObject)
      if (ressource.$describeUrl) window.open(ressource.$describeUrl, '_blank')
    },
    onAddStudents: function () {
      const node = this.tree.getSelectedNode()
      sesalab.dialogs.Eleves.execute(app.structure, (selection) => {
        for (let i = 0; i < selection.length; i++) {
          if (selection[i].instanceOf(sesalab.behaviors.Eleve)) {
            this.checkEleveOnEleves(selection[i], node)
          } else {
            this.checkGroupOnEleves(selection[i], node)
          }
        }
      })
    },

    onDeleteNode: function () {
      for (const node of this.tree.getSelectedNodes()) {
        node.remove()
        app.toaster.toast('L’élément a été correctement supprimé', 'info')
      }

      this.tree.refresh()
      this._needSave()
    },

    /**
     * Déclenché au clic droit "importer" sur une série
     */
    onImportResource: function () {
      const node = this.tree.getSelectedNode()
      if (!node?.instanceOf(sesalab.behaviors.Serie)) {
        app.toaster.toast('L’élément sélectionné n’est pas une série valide')
        return
      }
      askImportRid((rid) => {
        app.sesatheque.getItem(rid, (error, item) => {
          if (error) {
            console.error(error)
            app.toaster.pop(error.message, 'error')
          } else {
            item.$behavior = new sesalab.behaviors.Ressource(item)
            if (node.exists({ rid: item.rid }, true)) return
            node.addChildren(item)
            this.tree.refresh()
            this._needSave()
            this.tree.openNodeAndParents(node)
          }
        })
      })
    },

    /**
     * Appelé au clic droit sur signalement
     */
    onNotify: function () {
      const selectedObject = this.tree.getSelectedObject()
      // en cas d'alias c'est bien l'original qu'on veut
      const rid = selectedObject && selectedObject.rid
      sesalab.dialogs.Contact.execute({ rid })
    },

    onSplitEleves: function () {
      const node = this.tree.getSelectedNode()
      if (!node || !node.data) return app.toaster.toast('Élément selectionné invalide, impossible de dissocier')

      const gid = node.data.oid
      const target = node.getParent()
      const result = app.store.get({ type: 'Groupe', id: gid })
      if (Array.isArray(result.utilisateurs)) {
        for (let eleve of result.utilisateurs) {
          if (typeof eleve !== 'object') {
            eleve = app.store.get({ type: 'Eleve', id: eleve })
          }
          if (!target.exists({ oid: eleve.oid }, true)) {
            const data = _.clone(eleve)
            data.$behavior = new sesalab.behaviors.Eleve(data)
            target.addChildren(data)
          }
        }
      } else {
        console.error(Error(`Aucun groupe ${gid} dans le store`))
      }
      node.remove()
      this.tree.refresh()
      this._needSave(true, false)
    },

    sortStudents: function (target) {
      let children = target.getChildren().copy().toArray()
      children = children.sort(function (a, b) {
        const fullName = a.data.nom.toLowerCase() + (a.data.prenom ? ' ' + a.data.prenom.toLowerCase() : '')
        const secondFullName = b.data.nom.toLowerCase() + (b.data.prenom ? ' ' + b.data.prenom.toLowerCase() : '')
        return fullName.localeCompare(secondFullName)
      })

      target.removeAll()
      for (let i = 0; i < children.length; i++) {
        if (children[i].type === 'eleve') {
          target.addChildren(children[i].data)
        } else {
          app.populateGroupe(target.addChildren(children[i].data), true)
        }
      }
    },
    refreshStudents: function (target, students) {
      const children = target.getChildren().toArray()
      for (const child of children) {
        child.remove()
      }

      students = _.compact(students)
      students = students.sort((a, b) => {
        return a.nom.toLowerCase().localeCompare(b.nom.toLowerCase())
      })

      let changed = false
      students = _.filter(students, (e) => {
        if (!e) {
          notify(Error('refreshStudents appelé avec un élève invalide'), { students, sequence: this.data })
          return false
        }
        if (e.type === constants.SEQUENCE_TYPE_GROUPE) {
          const groupe = app.store.get({ type: 'Groupe', id: e.oid })
          if (!groupe) {
            changed = true
            app.toaster.toast(`Le groupe ${e.nom} était dans cette séquence mais n'existe plus, il a été supprimé de la séquence`)
            return false
          }

          if (e.nom !== groupe.nom) {
            changed = true
            e.nom = groupe.nom
          }

          return true
        }

        if (e.type === constants.SEQUENCE_TYPE_ELEVE) {
          const eleve = app.store.get({ type: 'Eleve', id: e.oid })
          if (!eleve || eleve.__deletedAt) {
            changed = true
            app.toaster.toast(`L'élève ${e.nom} était dans cette séquence mais n'est plus dans l’établissement, il a été supprimé de la séquence`)
            return false
          }

          ['nom', 'prenom'].forEach(function (attribute) {
            if (e[attribute] !== eleve[attribute]) {
              changed = true
              e[attribute] = eleve[attribute]
            }
          })

          return true
        }
      })

      for (const i in students) {
        const eleve = students[i]

        if (eleve.type === constants.SEQUENCE_TYPE_GROUPE) {
          eleve.$behavior = new sesalab.behaviors.SequenceGroupe(eleve)
          const nodeGroupe = target.addChildren(eleve)
          app.populateGroupe(nodeGroupe, true)
        } else {
          eleve.$behavior = new sesalab.behaviors.Eleve(eleve)
          target.addChildren(eleve)
        }
      }

      return changed
    },
    onShowStudentBilan: function () {
      const node = this.tree.getSelectedNode()
      app.showStudentBilan(node.data)
    },
    onTestSequence: function () {
      function onSaved () {
        const eleve = this.tree.getSelectedNode().data
        app.openPage(new sesalab.pages.TestSequence(this.sequence, eleve))
      }
      if (this._hasChanged) {
        this.saveSequence(() => {
          app.toaster.toast('La séquence est sauvegardée pour visualisation')
          onSaved.bind(this)()
        })
      } else {
        onSaved.bind(this)()
      }
    },
    onSequenceAsNewSequence: function () {
      app.mesSequences.createSequence(undefined, this.serializeSequence())
    },
    onSequenceExpandSettings: function () {
      this.model.getChildren().forEach((child) => {
        this.transfertSettings(this.model.data, child.data)
        child.serie.getChildren().forEach((resource) => {
          // Pour les ressources, on n'écrase pas le 'type' de la resource avec le 'type' de la série
          this.transfertSettings(child.data, resource.data, ['nonZapping', 'minimumReussite', 'maximumVisionnage'])
        })
      })
      this._needSave()
      this.initPropagationButtonsAndFlags()
    },
    onSerieAsNewSequence: function () {
      const node = this.tree.getSelectedNode()
      if (!node || !node.instanceOf(sesalab.behaviors.Serie)) {
        app.toaster.toast('L’élément selectionné n’est pas une série valide')
        return
      }

      let subSequence = this.tree.getSelectedNode().getParent()
      subSequence = this.serializeSubSequence(subSequence)
      app.mesSequences.createSequence(null, {
        sousSequences: [
          {
            eleves: [],
            nom: 'sous-séquence',
            serie: subSequence.serie
          }
        ]
      })
    },
    onSerieExpandSettings: function () {
      const serie = this.tree.getSelectedNode()
      if (!serie) return
      serie.getChildren().forEach((resource) => {
        // Pour les ressources, on n'écrase pas le 'type' de la resource avec le 'type' de la série
        this.transfertSettings(serie.getParent().data, resource.data, ['nonZapping', 'minimumReussite', 'maximumVisionnage'])
      })
      this._needSave()
      this.initPropagationButtonsAndFlags()
    },
    eventToAction: function (source, target) {
      if (!source?.data?.$behavior?.constructor) return ''
      // source.data.$behavior?.classname ou target.classname sont du genre sesalab.behaviors.Ressource
      // console.log(`drop event avec source ${source.data.$behavior?.classname} et target ${target.classname}`)
      const b = sesalab.behaviors
      const sourceClass = source.data.$behavior.constructor
      let isRessource = sourceClass === b.Ressource
      let isSerie = false
      if (isRessource && source.data.type === 'serie') {
        isSerie = true
        isRessource = false
      }

      if (sourceClass === b.SousSequence && target === b.SousSequence) return 'sous-sequence-sous-sequence'
      if (sourceClass === b.Arbre && target === b.Serie) return 'arbre-serie'
      if (sourceClass === b.Eleve && target === b.Eleves) return 'eleve-eleves'
      if (sourceClass === b.Groupe && target === b.Eleves) return 'groupe-eleves'
      if (sourceClass === b.Groupe && !target) return 'groupe-panel'
      if (sourceClass === b.SequenceModele && !target) return 'model-panel'
      if (sourceClass === b.SequenceModele && target === b.Sequence) return 'model-sequence'
      if (sourceClass === b.SequenceModele && target === b.Serie) return 'model-serie'
      if (sourceClass === b.SequenceModele && target === b.SousSequence) return 'model-sous-sequence'
      if (isRessource && target === b.Ressource) return 'ressource-ressource'
      if (isRessource && target === b.Serie) return 'ressource-serie'
      if (sourceClass === b.Sequence && target === b.Sequence) return 'sequence-sequence'
      if (sourceClass === b.Sequence && target === b.Serie) return 'sequence-serie'
      if (sourceClass === b.Sequence && target === b.SousSequence) return 'sequence-sous-sequence'
      if (isSerie && !target) return 'serie-panel'
      if (isSerie && target === b.Sequence) return 'serie-sequence'
      if (isSerie && target === b.Serie) return 'serie-serie'
      // pas de notify, y'a par ex du source sesalab.behaviors.Eleve et target sesalab.behaviors.SequenceGroupe non géré ici mais qui fonctionne quand même comme attendu
      return ''
    },
    onOver: function (e) {
      return this.tree.hasAllElementsSameType(e.sources) && this.eventToAction(e.sources[0], e.targetClass)
    },
    onDrop: function (e) {
      for (const source of e.sources) {
        let ressource = null
        let sousSequence = null
        let rid = null
        const data = _.clone(source.data)

        // donne le type de source et de destination (model signifie SequenceModele)
        const action = this.eventToAction(source, e.targetClass)
        if (!action) continue // y'a déjà eu un console.error pour signaler le cas, pas de notify pour un drop d'un truc non prévu
        // console.debug(`drop sur la page séquence, cas ${action} de`, data)
        switch (action) {
          case 'arbre-serie':
            this.extractResourcesFromTree(data, e.target)
            break

          case 'eleve-eleves':
            this.checkEleveOnEleves(source, e.target)
            break

          case 'groupe-eleves':
            if (source.data.oid === undefined) {
              console.error(Error('Groupe sans oid'), source.data)
              return app.toaster.toast('Cet élément ne peut pas être utilisé dans une séquence', 'error')
            }
            this.checkGroupOnEleves(source, e.target)
            break

          case 'groupe-panel': {
            const groupe = { type: constants.SEQUENCE_TYPE_GROUPE }
            Object.assign(groupe, _.pick(data, ['nom', 'oid']))
            this.addSubSequence({ eleves: [groupe] })
            break
          }
          case 'model-panel' :
            this.fetchSequenceModeleOrSerie({ rid: data.rid }, async (ressource) => {
              for (const sousSequence of ressource.parametres.sousSequences) {
                const ss = sesalab.behaviors.SousSequence.create(sousSequence)
                ss.serie = []
                for (const resource of sousSequence.serie) {
                  ss.serie.push(resource)
                }
                await this.getNodeSousSequence(ss)
              }
              this.tree.refresh()
              this._needSave()
            })
            break

          case 'model-sequence':
          case 'sequence-sequence': {
            const askStart = (next) => {
              const isEmpty = this.model.getChildren().toArray()
                .every((child) => !child.serie.getChildren().length && !child.groupes.getChildren().length)
              if (isEmpty) {
                return next()
              }
              const question = 'Vous allez remplacer tout le contenu de cette séquence par celui de ' + data.nom
              sesalab.widgets.ConfirmationDialog.execute(question, next)
            }
            askStart(() => {
              if (action === 'sequence-sequence') {
                if (!data.oid) {
                  const errMsg = 'Séquence source invalide'
                  console.error(Error(errMsg), data)
                  return app.toaster.toast(errMsg, 'error')
                }
                this.fetchFullSequence(data.oid, (seq) => {
                  this.reload(seq)
                })
              } else {
                if (!data.rid) {
                  const errMsg = 'Séquence modèle source invalide'
                  console.error(Error(errMsg), data)
                  return app.toaster.toast(errMsg, 'error')
                }
                this.fetchSequenceModeleOrSerie({ rid: data.rid }, (ressource) => {
                  this.reload(ressource.parametres)
                })
              }
            })
            break
          }

          case 'sous-sequence-sous-sequence':
            this.moveSousSequence(source, e.target)
              .catch((error) => {
                app.toaster.toast(error, 'error')
              })
            break

          case 'model-serie':
          case 'model-sous-sequence':
            this.fetchSequenceModeleOrSerie({ rid: data.rid }, async (ressource) => {
              // Copie les éléments de la source + recherche de la cible
              const sousSequences = this.computeSousSequences(ressource.parametres, e)

              // Ajoute le rendu final à l'arbre
              this.model.getChildren().removeAll()
              for (const ssSeq of sousSequences) {
                await this.getNodeSousSequence(this.copySousSequence(ssSeq))
              }

              this.tree.refresh()
              this._needSave()
            })
            break

          case 'ressource-ressource':
            if (source.getParent() === e.target.getParent()) {
              source.getSiblings().remove(source)
            } else {
              if (e.target.getParent().exists({ rid: data.rid })) {
                continue
              }
            }

            ressource = _.clone(source.data)
            sousSequence = e.target.getParent().getParent().data
            ressource.nonZapping = sousSequence.nonZapping
            ressource.minimumReussite = sousSequence.minimumReussite
            ressource.maximumVisionnage = sousSequence.maximumVisionnage
            ressource.$behavior = new sesalab.behaviors.Ressource(ressource)
            e.target.getParent().addSibling(e.target, ressource, e.position)
            this.tree.refresh()
            this._needSave()
            break

          case 'ressource-serie':
            // Recherche pour la sous-séquence actuelle
            if (this.checkResourceExists(source, e.target)) {
              continue
            }

            // Recherche dans l'ensemble des sous-séquences
            this.checkResourceExists(source)

            if (data.type === 'arbre') {
              this.extractResourcesFromTree(data, e.target)
              continue
            }

            sousSequence = e.target.getParent().data
            data.nonZapping = sousSequence.nonZapping
            data.minimumReussite = sousSequence.minimumReussite
            data.maximumVisionnage = sousSequence.maximumVisionnage
            data.$behavior = new sesalab.behaviors.Ressource(data)
            e.target.addChildren(data)
            this.tree.refresh()
            this.tree.openNodeAndParents(e.target)
            this._needSave()
            break

          case 'sequence-serie':
          case 'sequence-sous-sequence':
            this.fetchFullSequence(data.oid, async (seq) => {
              const sousSequences = this.computeSousSequences(seq, e)
              this.model.getChildren().removeAll()
              for (const ssSeq of sousSequences) {
                await this.getNodeSousSequence(this.copySousSequence(ssSeq))
              }

              this.tree.refresh()
              this._needSave()
            })
            break

          case 'serie-panel':
          case 'serie-sequence':
            // si la source est une série, c'est qu'elle a un rid car elle vient d'une sésathèque
            if (!data.rid) notify(Error('drop d’une série sans rid dans une séquence'), { serie: data })
            // on touche pas à ce code mais c'est une anomalie, notify ci-dessus ajouté le 2024-11-29,
            // @todo virer ça (et ne garder que data.rid) si y'a rien dans bugsnag en qq mois
            rid = data.rid || app.sesatheque.baseIdPrivate + '/' + data.id
            this.fetchSequenceModeleOrSerie({ rid, isSerie: true }, async (ressource) => {
              const parametres = ressource.parametres
              const sousSequence = sesalab.behaviors.SousSequence.create({
                type: parametres.type,
                nonZapping: parametres.nonZapping,
                minimumReussite: parametres.minimumReussite,
                maximumVisionnage: parametres.maximumVisionnage
              })
              sousSequence.serie = [...parametres.serie]
              const node = await this.getNodeSousSequence(sousSequence)
              this.tree.openNodeAndParents(node)
              this._needSave()
            })
            continue

          case 'serie-serie':
            // si la source est une série, c'est qu'elle a un rid car elle vient d'une sésathèque
            if (!data.rid) notify(Error('drop d’une série sans rid dans une séquence'), { serie: data })
            // on touche pas à ce code mais c'est une anomalie, notify ci-dessus ajouté le 2024-11-29,
            // @todo virer ça (et ne garder que data.rid) si y'a rien dans bugsnag en qq mois
            rid = data.rid || app.sesatheque.baseIdPrivate + '/' + data.id
            this.fetchSequenceModeleOrSerie({ rid, isSerie: true }, (ressource) => {
              const parametres = ressource.parametres
              // Statut par défault, si l'utilisateur garde le statut de l'élément original on ignore cette donnée
              parametres.type = parametres.type || constants.SERIE_STATUT_LIBRE
              parametres.minimumReussite = parametres.minimumReussite || constants.DEFAULT_MINIMUM_REUSSITE
              parametres.nonZapping = parametres.nonZapping || constants.DEFAULT_NON_ZAPPING
              parametres.maximumVisionnage = parametres.maximumVisionnage || constants.DEFAULT_MAX_VISIONNAGE
              const sousSequence = e.target.parentNode.data

              const process = function (mergeParameters) {
                _.each(parametres.serie, (r) => {
                  const normalizedResource = app.sesatheque.normalize(r)
                  if (!e.target.exists({ rid: normalizedResource.rid }, true)) {
                    /** @type {RichResource} */
                    normalizedResource.$behavior = new sesalab.behaviors.Ressource(normalizedResource)
                    e.target.addChildren(normalizedResource)
                  }
                })
                // On attribue les attributs de la série droppée à l'ensemble des exercices de la série destination
                // En revanche, lorsque l'on conserve les paramètres de la série destination,
                // on ne met pas à jour les paramètres des exercices importées de la série droppée
                // Certaines séries importées par LaboMEP v1 peuvent contenir un tableau vide de paramètres,
                // dans ce cas, on ne merge pas les paramètres de la série droppée
                if (mergeParameters && !_.isEmpty(parametres)) {
                  this.transfertSettings(parametres, sousSequence)
                  // On met à jour les attributs pour chaque exercice dans la série
                  if (sousSequence.serie) {
                    _.each(sousSequence.serie, (ressource) => {
                      if (ressource.$behavior) {
                        const parametersToApply = _.find(parametres.serie, { rid: ressource.rid }) || parametres
                        this.transfertSettings(parametersToApply, ressource, ['nonZapping', 'minimumReussite', 'maximumVisionnage'])
                      }
                    })
                  }
                  this.initPropagationButtonsAndFlags()
                }
                this._needSave()
                this.tree.refresh()
              }.bind(this)

              const labelize = (r) => {
                switch (r.type) {
                  case constants.SERIE_STATUT_LIBRE: return 'libre'
                  case constants.SERIE_STATUT_ORDONNEE: return 'ordonnée'
                  case constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE: return 'ordonnée avec un minimum de ' + r.minimumReussite + '% de réussite'
                  default:
                    console.error(Error(`statut ${r.type} inconnu => libre`))
                    return 'libre'
                }
              }

              // Si les paramètres entre la série source et la série destination sont différents,
              // on laisse à l'utilisateur la possibilité de conserver certains paramètres
              if (sousSequence.type !== parametres.type || sousSequence.maximumVisionnage !== parametres.maximumVisionnage ||
                sousSequence.minimumReussite !== parametres.minimumReussite || sousSequence.nonZapping !== parametres.nonZapping) {
                // Textes et valeurs présentes uniquement sur les séries ayant un minimum de réussite
                let minReussiteSequenceText = 'Minimum réussite : ' + sousSequence.minimumReussite + '<br>'
                let minReussiteParametresText = 'Minimum réussite : ' + parametres.minimumReussite + '<br>'
                if (sousSequence.type !== constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE && parametres.type !== constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE) {
                  minReussiteSequenceText = minReussiteParametresText = ''
                  delete parametres.minimumReussite
                }

                sesalab.widgets.AskDialog.execute(
                  'La série importée contient des paramètres différents. <br><br>' +
                  'Paramètres par défaut :<br>' +
                  'Type : ' + labelize(sousSequence) + '<br>' +
                  'Non zapping : ' + sousSequence.nonZapping + '<br>' +
                  minReussiteSequenceText +
                  'Maximum visionnage : ' + sousSequence.maximumVisionnage + '<br><br>' +
                  'Paramètres de la série importée :<br>' +
                  'Type : ' + labelize(parametres) + '<br>' +
                  'Non zapping : ' + parametres.nonZapping + '<br>' +
                  minReussiteParametresText +
                  // (toujours null pour AskDialog)

                  'Maximum visionnage : ' + parametres.maximumVisionnage + '<br><br>', (error, r) => {
                    if (error) console.error(error)
                    process(r)
                  }, { yes: 'Appliquer les nouveaux paramètres', no: 'Conserver les paramètres par défaut' }
                )
              // Si les paramètres de la série source et destination sont identiques,
              // on ajoute les ressources de la série dropée à la suite des ressources existantes
              } else {
                process(true)
              }
            })
            break

          default:
            notify(Error(`Action inconnue au drop : ${action}`))
            app.toaster.toast('Action non gérée (ignorée)', 'error', 3)
        }
      }
    },

    /**
     * Crée les éléments présents dans un noeud de sous-séquence.
     *
     * @param node Noeud de sous-séquence
     * @return True si le contenu de la sous-séquence a changé
     */
    populateNodeSousSequence: async function (node) {
      node.groupes = node.addChildren({ $behavior: new sesalab.behaviors.Eleves() })
      node.serie = node.addChildren({ $behavior: new sesalab.behaviors.Serie() })
      const sousSequence = node.getData()
      const changed = this.refreshStudents(node.groupes, sousSequence.eleves)

      if (!sousSequence.serie) return changed

      // lorsque le prof modifie un alias via le clic droit dans sesalab,
      // ça met normalement à jour les séquences qui l'utilisent, mais
      // - cette màj peut échouer
      // - le prof peut modifier cet alias directement dans la sesathèque
      // => on demande au ressStore un truc frais
      const ressChanges = []
      let needOpenSerie = false
      const getOrFetch = async (rid, titre) => {
        let ress
        try {
          ress = await app.ressStore.getOrFetch(rid)
        } catch (error) {
          if (error.status === 404) {
            app.toaster.toast(`La ressource « ${titre} » n’existe plus`, 'error')
            needOpenSerie = true
          } else {
            console.error(error)
            app.toaster.toast(`Impossible de récupérer la ressource « ${titre} » : ${error.message}`, 'error')
          }
        }
        return ress
      }

      for (const ressource of sousSequence.serie) {
        let ress // ressource "fraîche"
        // si c'est un alias, on regarde d'abord si c'est toujours le cas
        if (ressource.aliasRid) {
          // la ressource de ressStore (donc récupérée précédemment par l'appel des ressources perso)
          ress = await getOrFetch(ressource.aliasRid, ressource.titre)
          if (ress && !ress.aliasRid) {
            // c'était un alias et ça n'en est plus un
            delete ressource.aliasRid
            ressource.rid = ress.rid
            let msg = `La ressource « ${ress.titre} » n’est plus un alias`
            if (ressource.titre !== ress.titre) {
              msg += ` (et son ancien titre était « ${ressource.titre} »)`
              ressource.titre = ress.titre
            }
            ressChanges.push(msg)
            needOpenSerie = true
          }
        } else {
          // pas un alias
          ress = await getOrFetch(ressource.rid, ressource.titre)
        }
        if (ress && ress.titre !== ressource.titre) {
          ressChanges.push(`Le titre de la ressource « ${ress.titre} » a changé (il était ${ressource.titre})`)
          ressource.titre = ress.titre
          needOpenSerie = true
        }
        ressource.$behavior = new sesalab.behaviors.Ressource(ressource)
        node.serie.addChildren(ressource)
      }
      if (ressChanges.length) {
        app.toaster.toast(`Il faudra enregistrer la séquence pour sauvegarder ces modifications :<br>- ${ressChanges.join('<br>- ')}`, 'info', 10)
      }
      if (needOpenSerie) {
        this.tree.openNode(node.serie)
      }

      return changed || ressChanges.length
    },

    /**
     * Crée la sous-séquence et retourne le node correspondant
     * @param modelSousSequence
     * @returns {Promise<sesalab.widgets.TreeNode>}
     */
    getNodeSousSequence: async function (modelSousSequence) {
      modelSousSequence.$behavior = new sesalab.behaviors.SousSequence(modelSousSequence)
      const node = this.model.addChildren(modelSousSequence)
      const hasChanges = await this.populateNodeSousSequence(node)
      if (hasChanges) this._needSave()
      this.tree.openNode(node)

      return node
    },

    /**
     * Récupère la séquence complète d'après son oid
     */
    fetchFullSequence: function (oid, next) {
      app.sesalab.get('sequence/' + oid, {}, (error, response) => {
        if (error) {
          console.error(error)
          app.toaster.toast(error.message, 'error')
          return
        }
        const sequence = sesalab.behaviors.Sequence.create(response.sequence)
        next(sequence)
      })
    },
    /**
     * Récupère une séquence modèle ou une série et la passe à next (si y'a des sous-séquences)
     * En cas de pb affiche une erreur sans appeler next
     * @param {Object} opts
     * @param {string} opts.rid
     * @param {boolean} [opts.isSerie] Passer true pour récupérer une série (sinon sequenceModele supposée
     * @param {RessourceCallback} next
     */
    fetchSequenceModeleOrSerie: function ({ rid, isSerie }, next) {
      app.sesatheque.getRessource(rid, (error, ressource) => {
        let errorMsg
        if (error) {
          console.error(error)
          errorMsg = error.message
        } else if (isSerie) {
          // check serie valide
          if (ressource.type !== 'serie') {
            errorMsg = `La ressource ${rid} n’est pas une série d’exercices`
          } else if (!Array.isArray(ressource.parametres.serie) || !ressource.parametres.serie.length) {
            errorMsg = `La série ${rid} ne contient aucun exercice`
          }
        } else {
          // sequenceModele
          if (ressource.type !== 'sequenceModele') {
            errorMsg = `La ressource ${rid} n’est pas un modèle de séquence`
          } else
            if (!ressource.parametres.sousSequences.length) {
              errorMsg = 'La séquence modèle ne possède pas de sous-séquence'
            }
        }
        if (errorMsg) {
          app.toaster.toast(errorMsg, 'error')
        } else {
          next(ressource)
        }
      })
    },

    serializeSequence: function () {
      const result = this.model.data
      const sousSequences = this.model.getChildren().toArray()
      result.sousSequences = []
      for (let i = 0; i < sousSequences.length; i++) {
        result.sousSequences.push(this.serializeSubSequence(sousSequences[i]))
      }
      return result
    },
    serializeSubSequence: function (sousSequenceNode) {
      const groupes = sousSequenceNode.groupes.getChildren().toArray()
      const series = sousSequenceNode.serie.getChildren().toArray()
      const sousSequenceObj = sousSequenceNode.data
      const sousSequence = {
        uuid: sousSequenceObj.uuid,
        nom: sousSequenceNode.data.nom,
        type: sousSequenceObj.type,
        eleves: [],
        serie: []
      }
      this.transfertSettings(sousSequenceObj, sousSequence)
      for (const grp of groupes) {
        const g = grp.data
        const groupe = {
          oid: g.oid,
          nom: g.nom
        }

        if (sesalab.behaviors.Eleve.check(g)) {
          groupe.type = constants.SEQUENCE_TYPE_ELEVE
          groupe.prenom = g.prenom
        } else {
          groupe.type = constants.SEQUENCE_TYPE_GROUPE
        }
        sousSequence.eleves.push(groupe)
      }

      for (const serie of series) {
        sousSequence.serie.push(serie.data)
      }
      return sousSequence
    },
    transfertSettings: function (from, to, attributes) {
      attributes = attributes || ['type', 'nonZapping', 'minimumReussite', 'maximumVisionnage'] // default values
      Object.assign(to, _.pick(from, attributes))
      if (to.$behavior) to.$behavior.apply(_.pick(from, attributes))
    },

    addOnEleves: function (source, target, type) {
      const data = _.clone(source.data)
      data.type = type
      if (type === constants.SEQUENCE_TYPE_ELEVE) {
        delete data.$fake
        data.$behavior = new sesalab.behaviors.Eleve(data)
        target.addChildren(data)

        // On supprime la source uniquement si elle provient du même arbre
        if (source.tree === target.tree) {
          source.remove()
        }
      }
      if (type === constants.SEQUENCE_TYPE_GROUPE) {
        data.$behavior = new sesalab.behaviors.SequenceGroupe(data)
        app.populateGroupe(target.addChildren(data), true)
      }
      if (!target.parentNode.data.eleves) target.parentNode.data.eleves = []
      target.parentNode.data.eleves.push(data)
      this._needSave(true, false)

      // Ouvre le noeud venant d'être mis à jour
      this.tree.openNode(target)
      this.sortStudents(target)
    },
    addEleveOnEleves: function (source, target) {
      this.addOnEleves(source, target, constants.SEQUENCE_TYPE_ELEVE)
    },
    addGroupOnEleves: function (source, target) {
      this.addOnEleves(source, target, constants.SEQUENCE_TYPE_GROUPE)
    },
    /**
     * Lance une notif et retourne true si la ressource source est déjà dans target, retourne false sinon
     * @param source
     * @param target
     * @returns {boolean}
     */
    checkResourceExists: function (source, target) {
      if (target) {
        const existing = target.exists({ rid: source.data.rid }, true)
        if (!existing) {
          return false
        }

        if (source.data.titre === existing.data.titre) {
          app.toaster.toast('Cet élément existe déjà dans l’arbre', 'error', 5)
        } else {
          app.toaster.toast('Cet élément existait déjà dans l’arbre sous un ancien nom, il a été mis à jour')
          existing.data.$behavior.apply({ titre: source.data.titre })
          existing.setCaption(source.data.titre)
          this._needSave()
        }
      } else {
        const similarResources = this.findSimilarResources(source.data.rid)
        if (similarResources.length) {
          const parentSequence = _.head(similarResources).parentNode.parentNode
          app.toaster.toast(`Cet exercice est déjà présent dans la série de la sous-séquence ${parentSequence.getCaption()}, l'ajouter ici une nouvelle fois reste possible mais les bilans de cet exercice seront fusionnés`)
          return false
        }
      }

      return true
    },
    checkEleveOnEleves: function (source, target) {
      if (!source || !target) return
      const eleveId = source.data.oid
      // On vérifie si l'élève n'est pas déjà présent hors groupe
      if (target.exists({ oid: eleveId, type: constants.SEQUENCE_TYPE_ELEVE }, false)) return

      // On parcourt le noeud "Elèves" pour détecter si l'élève que l'on souhaite importer est déjà présent dans un groupe
      const groups = []
      target.getChildren().forEach((child) => {
        if (child.data.type === constants.SEQUENCE_TYPE_ELEVE) return
        child.getChildren().forEach((eleve) => {
          if (eleveId === eleve.data.oid) groups.push(child)
        })
      })

      if (groups.length <= 0) {
        this.addEleveOnEleves(source, target)
        return
      }

      let message = `L’élève ${source.data.nom} ${source.data.prenom} est déjà présent dans `
      message += groups.length === 1 ? ' le groupe ' : ' les groupes '
      const names = groups.map((elt) => elt.data.nom)
      message += names.join(', ')
      // (toujours null pour AskDialog)

      sesalab.widgets.AskDialog.execute(message, (error, importEleve) => {
        if (error) console.error(error)
        if (!importEleve) return
        this.addEleveOnEleves(source, target)
      }, {
        title: 'Importer cet élève ?'
      }, this)
    },
    checkGroupOnEleves: function (source, target) {
      if (!source || !target) return app.toaster.toast('Données invalides, impossible de savoir si l’action est permise')

      // On vérifie si le groupe n'est pas déjà présent
      if (target.exists({ oid: source.data.oid, type: constants.SEQUENCE_TYPE_GROUPE }, false)) return

      // On récupère l'ensemble des élèves présents dans la source
      // Attention : pour une classe, eleves est un tableau d'objets tandis que pour un groupe, il s'agit d'un tableau d'ids
      const eleves = source.data.utilisateurs

      const existingEleves = []
      // On parcourt le noeud "Elèves" pour détecter si le groupe que l'on souhaite importer
      // possède un élève déjà présent (seul ou dans un autre groupe)
      target.getChildren().forEach((child) => {
        if (child.data.type === constants.SEQUENCE_TYPE_ELEVE) {
          // Comme le format d'élèves diffère entre une classe et un groupe, on doit tester la présence d'un élève
          // de deux manières différentes
          if (_.find(eleves, { oid: child.data.oid }) || eleves.indexOf(child.data.oid) >= 0) existingEleves.push(child)
        }
      })

      // Si un élève est déjà inscrit individuellement, on propose au formateur de le supprimer ou non
      if (existingEleves.length > 0) {
        let message = existingEleves.length === 1 ? 'L’élève ' : 'Les élèves '
        const names = _.map(existingEleves, (e) => e.data.prenom + ' ' + e.data.nom)
        message += names.join(', ')
        message += existingEleves.length === 1 ? ' est ' : ' sont '
        message += 'déjà présent'
        message += existingEleves.length === 1 ? ' ' : 's '
        message += 'dans cette sous-séquence.'
        // (toujours null pour AskDialog)

        sesalab.widgets.AskDialog.execute(message, (error, deleteEleves) => {
          if (error) console.error(error)
          if (deleteEleves) {
            target.getChildren().forEach((child) => {
              if (child.data.type === 'eleve') {
                existingEleves.forEach((existingEleve) => {
                  if (existingEleve.data.oid === child.data.oid) child.remove()
                })
              }
            })
          }
          this.addGroupOnEleves(source, target)
        }, {
          title: 'Supprimer les élèves inscrits individuellement ?'
        }, this)
        return
      }

      // On importe finalement le groupe
      this.addGroupOnEleves(source, target)
    },

    getAllResources: function () {
      const allResources = []
      const subSequences = this.model.getChildren().toArray()
      for (const subSequence of subSequences) {
        const ressources = subSequence.serie.getChildren().toArray()
        if (ressources.length) allResources.push(...ressources)
      }

      return allResources
    },

    /**
     * Déclenché par le drop d'un arbre dans une série
     * @param {sesalab.widgets.Tree} tree
     * @param {sesalab.widgets.TreeNode} target
     */
    extractResourcesFromTree: function (tree, target) {
      this.extractChildrenFromTree(tree, (error, resources) => {
        if (error) {
          app.toaster.toast(error.message, 'error')
          return
        }

        if (resources && resources.length > app.settings.maxResourcesDropped) {
          app.toaster.toast('Limite maximum atteinte, seuls les ' + app.settings.maxResourcesDropped +
            ' premiers éléments ont été importés', 'error')
        }

        resources = _.take(resources, app.settings.maxResourcesDropped)
        for (const resource of resources) {
          const r = _.clone(resource)
          r.$behavior = new sesalab.behaviors.Ressource(r)
          if (!target.exists({ rid: r.rid }, true)) {
            target.addChildren(r)
          }
        }

        this.tree.refresh()
        this._needSave()
        this.tree.openNodeAndParents(target)
      })
    },

    findSimilarResources: function (rid) {
      const resources = this.getAllResources()
      return _.filter(resources, (resource) => { return resource.data.rid === rid })
    },

    moveSousSequence: async function (source, target) {
      // on va modifier l'array des enfants sans créer de nouveaux objets
      // (y'avait un bug avec la version précédente lorsqu'une ressource était dans plusieurs séries,
      // une série pouvait se retrouver vide après le déplacement)
      const siblings = target.parentNode.getChildren().toArray()
      const indexToMove = siblings.indexOf(source)
      let indexTarget = siblings.indexOf(target)
      // il faut insérer indexToMove juste avant indexTarget
      if (indexToMove === indexTarget - 1) return // rien à faire
      const [treeNodeToMove] = siblings.splice(indexToMove, 1)
      if (indexToMove < indexTarget) indexTarget--
      siblings.splice(indexTarget, 0, treeNodeToMove)
      target.parentNode.setChildren(new qx.data.Array(siblings))
      this._needSave()
      this.tree.refresh()
    },

    extractChildrenFromTree: function (element, callback) {
      const self = this

      // On ignore les dossiers vides
      if (!element.enfants || !element.enfants.length) {
        return callback(null, [])
      }

      flow(element.enfants)
        .seqEach(function (child) {
          if (child.type === 'arbre' && child.rid) {
            return app.sesatheque.getItem(child.rid, (error, item) => {
              if (error) return this(error)
              self.extractChildrenFromTree(item, this)
            })
          }

          // Arbre ou série
          if (child.enfants && child.enfants.length) {
            return self.extractChildrenFromTree(child, this)
          }

          // Si c'est un arbre sans enfants (dossier vide) ou une série sans enfant, on doit l'ignorer
          if (['arbre', 'serie'].includes(child.type)) return this(null, [])
          this(null, child)
        })
        .seq(function (resources) {
          callback(null, _.flatten(resources))
        })
        .catch(callback)
    },
    copySousSequence: function (model) {
      const sousSequence = sesalab.behaviors.SousSequence.create(model)
      // le create ne garanti pas d'avoir une série et des élèves,
      // seulement les properties de la classe qx
      sousSequence.serie = model.serie ? [...model.serie] : []
      return sousSequence
    },
    computeSousSequences: function (source, e) {
      let sousSequences = []
      const children = this.model.getChildren()
      children.forEach((child, index) => {
        if (child === e.target.parentNode || (e.targetClass === sesalab.behaviors.SousSequence && child === e.target)) {
          // An non-empty serie must be added too
          if (child.serie.getChildren().length > 0 || child.groupes.getChildren().length > 0) {
            sousSequences.push(child.data)
          }
          sousSequences = sousSequences.concat(source.sousSequences)
        } else {
          sousSequences.push(child.data)
        }
      })

      return sousSequences
    },

    // FIXME Utilisée dans le panneau MesRessources (au drop d'une série sur un dossier), HS
    exportSerie: function (serieNode) {
      const sousSequence = this.serializeSubSequence(serieNode.parentNode)
      delete sousSequence.eleves
      delete sousSequence.nom
      return sousSequence
    }
  },

  statics: {
    getId: function (sequence) {
      return 'edition-sequence-' + sequence.oid
    }
  }
})
