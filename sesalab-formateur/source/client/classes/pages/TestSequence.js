/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
qx.Class.define('sesalab.pages.TestSequence', {
  extend: sesalab.widgets.ScrollablePage,
  construct: function (sequence, eleve) {
    this.base(arguments, `Test ${eleve.prenom} ${eleve.nom}`, 'diploma')
    this.sequence = sequence
    this.eleve = eleve

    // Ajout du bouton pour éditer la séquence
    this.toolbar = new qx.ui.toolbar.ToolBar()
    this.add(this.toolbar)
    this.toolbar.set({
      padding: 5
    })
    this.toolbar.setSpacing(5)
    const editButton = new qx.ui.toolbar.Button('Éditer la séquence', 'icons/20/edit.png')
    this.toolbar.add(editButton)
    editButton.set({
      toolTipText: 'Éditer la séquence'
    })
    editButton.addListener('click', function () {
      app.openPage(new sesalab.pages.Sequence(sequence))
    }, this)

    const frame = app.createIframe(`/eleve/#!/formateur/test/${sequence.oid}/${eleve.oid}`)
    this.add(frame, { flex: 1 })
  },
  members: {
    getId: function () {
      return `test-sequence-${this.sequence.oid}-${this.eleve.oid}`
    }
  }
})
