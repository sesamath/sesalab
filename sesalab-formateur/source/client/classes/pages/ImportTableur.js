const flow = require('an-flow')
const isEmail = require('sesalab-commun/isEmail')

qx.Class.define('sesalab.pages.ImportTableur', {
  extend: sesalab.widgets.ScrollablePage,
  include: [sesalab.widgets.Helper],
  construct: function () {
    this.base(arguments, 'Import tableur', 'add_people')
    this.content.setPadding(10, 10, 10, 10)
    this.add(this.message = new qx.ui.basic.Label(`
<p>
Pour cette méthode d'importation, vous devez disposer d'un fichier tableur
(OpenOffice ou Excel par exemple) contenant la liste des élèves à importer avec
un élève par ligne, <strong>sans la ligne de titre</strong>.
</p>
<ul>
  <li>A) La <strong>première</strong> colonne doit <strong>impérativement</strong> contenir la <strong>classe</strong> de l'élève.</li>
  <li>B) La <strong>deuxième</strong> colonne doit <strong>impérativement</strong> contenir le <strong>nom</strong> de l'élève.</li>
  <li>C) La <strong>troisième</strong> colonne doit <strong>impérativement</strong> contenir le <strong>prénom</strong> de l'élève.</li>
  <li>D) La <strong>quatrième</strong> colonne peut contenir une <strong>adresse email</strong>, elle permettra à
  l'élève de recevoir un email avec un lien pour réinitialiser son mot de passe
  (sinon il devra demander à un enseignant de le faire).</li>
  <li>E) La <strong>cinquième</strong> colonne peut contenir l'<strong>identifiant</strong> de l'élève
    (qui lui est demandé pour se connecter).
    Cette cinquième colonne peut être laissée vide et c'est alors LaboMEP qui génère automatiquement l’identifiant.<br />
    <strong>Attention, si l'identifiant n’est pas précisé cela créera toujours un nouvel élève</strong>, donc lors d'imports successifs il faut préciser l'identifiant pour éviter les doublons (avec seulement nom et prénom, aucun moyen de savoir si c'est un homonyme ou le même, donc dans le doute un nouvel élève sera ajouté avec un suffixe numérique sur l'identifiant).<br />
        En cas d'erreur de colonne (par exemple), <strong>il vaut mieux repartir d'un export</strong>, après avoir restauré les élèves en corbeille s'il y en a, pour qu'il contienne tous les identifiants existants.
  <li>F) La <strong>sixième</strong> colonne peut contenir le <strong>mot de passe provisoire de l'élève</strong> (il devra le changer à sa première connexion). Lorsque cette sixième colonne est vide, si l'élève existe déjà il conserve son mot de passe, sinon c'est LaboMEP qui génère automatiquement un mot de passe provisoire. Les contraintes sur le mot de passe (longueur et contenu) ne s'appliquent pas lors de cet import.</li>
</ul>
<p>
  Sélectionnez ensuite dans votre tableur les cellules intéressantes et copiez-collez
  celles-ci dans le cadre ci-dessous puis cliquez sur "Importer".<br />
  <strong>Attention</strong>, il faut ensuite attendre que la procédure soit terminée pour récupérer le rapport d'importation. Ne partez pas sans l'avoir récupéré, c’est le seul moyen de connaître les mots de passe générés (ils sont stockés chiffrés dans Labomep, vous ne pourrez pas les retrouver ultérieurement).
</p>
<p>
  <strong>Cliquez dans le rectangle ci-dessous puis utilisez ctrl+v (pomme+v sous mac) ou le clic droit pour y coller les cellules de votre tableur, préalablement copiées (via clic droit ou ctrl+c).</strong>
</p>
  `))
    this.message.set({ rich: true, selectable: true })
    this.field = new qx.ui.form.TextArea()
    this.field.setNativeContextMenu(true)
    this.add(this.field, { flex: 1 })
    this.field.set({ height: 200, minHeight: 200 })
    this.add(this.message1 = new qx.ui.basic.Label(`
<p>
  Si vous n'indiquez pas d’identifiant, précisez ici votre préférence pour les générer automatiquement :
</p>
    `))
    this.message1.set({ rich: true, selectable: true })
    this.cbFormats = new qx.ui.form.ComboBox()
    const formats = ['nom.prenom', 'prenom.nom', 'nomp', 'pnom', 'nom.p', 'p.nom']
    for (let i = 0, ii = formats.length; i < ii; i++) {
      this.cbFormats.add(new qx.ui.form.ListItem(formats[i]))
    }
    this.cbFormats.setValue('nom.prenom')
    this.add(this.cbFormats)

    const flowlayout = new qx.ui.layout.Flow()
    flowlayout.setAlignX('center')
    const buttons = new qx.ui.container.Composite(flowlayout)
    buttons.set({ padding: [10, 10, 10, 10] })
    this.add(buttons)
    this.importButton = new qx.ui.form.Button('Importer')
    buttons.add(this.importButton)
    this.add(this.result = new qx.ui.basic.Label(''))
    this.result.set({ rich: true, selectable: true })

    this.importButton.addListener('click', function () {
      this.setImportButtonVisible(false)
      this.process()
    }, this)
  },

  members: {
    getId: function () {
      return 'import-tableur'
    },
    setImportButtonVisible: function (isVisible) {
      this.importButton.set({ visibility: isVisible ? 'visible' : 'excluded' })
    },
    process: function () {
      const self = this
      const value = this.field.getValue()
      const data = (value && value.trim()) || ''
      const loginFormat = this.cbFormats.getValue()
      const lines = data.split(/\n/)
      const eleves = []
      for (let i = 0; i < lines.length; i++) {
        const line = lines[i]
        // (seulement password pourrait être const ici)

        let [classeNom, nom, prenom, mail, login, password] = line.split(/\t/)
        if (classeNom) {
          classeNom = classeNom.trim()
        } else {
          const nbLine = i + 1
          app.toaster.toast(`Impossible de réaliser l’import des données.
            Le nom de la classe n'est pas renseigné à la ligne ${nbLine}.`, 'error')
          self.setImportButtonVisible(true)
          return
        }
        if (nom) {
          nom = nom.trim()
        } else {
          const nbLine = i + 1
          app.toaster.toast(`Impossible de réaliser l’import des données.
            Le nom de l'élève n'est pas renseigné à la ligne ${nbLine}.`, 'error')
          self.setImportButtonVisible(true)
          return
        }
        if (prenom) {
          prenom = prenom.trim()
        } else {
          const nbLine = i + 1
          app.toaster.toast(`Impossible de réaliser l’import des données.
            Le prénom de l'élève n'est pas renseigné à la ligne ${nbLine}.`, 'error')
          self.setImportButtonVisible(true)
          return
        }
        if (mail) {
          mail = mail.trim()
          if (!isEmail.validate(mail)) {
            const nbLine = i + 1
            app.toaster.toast(`Impossible de réaliser l’import des données.
              Le mail de l'élève n'est pas correct à la ligne ${nbLine}.`, 'error')
            self.setImportButtonVisible(true)
            return
          }
        }

        // Le login peut ne pas être renseigné dans l'import tableur
        if (login) login = login.trim()

        // et l'élève
        eleves.push({
          nom,
          prenom,
          mail: mail || '',
          login,
          password: password || '',
          structures: [app.structure.oid],
          classeNom,
          type: 0
        })
      }

      app.importProgress = sesalab.widgets.ProgressDialog.execute('Import tableur')
      app.importProgress.init('Envoi des données...', 2)

      let nbClasses = 0
      let nbEleves = 0

      flow()
        .seq(function () {
          app.sesalab.put('import-tableur', {
            structureOid: app.structure.oid,
            eleves,
            loginFormat
          }, {}, this)
        })
        .seq(function (response) {
          const { stats } = response
          const errMsg = response.message || 'Le serveur n’a pas renvoyé la réponse attendue'
          if (!stats) return this(Error(errMsg))
          if (['nbGroupsAdded', 'nbGroupsUpdated', 'nbUsersAdded', 'nbUsersUpdated'].some(p => typeof stats[p] !== 'number')) return this(Error(errMsg))
          if (!Array.isArray(stats.eleves)) return this(Error(errMsg))

          // ok, stats est bien l'objet attendu, on continue
          nbClasses = stats.nbGroupsAdded + stats.nbGroupsUpdated
          nbEleves = stats.nbUsersAdded + stats.nbUsersUpdated

          app.bus.broadcast('classes::refresh')
          app.bus.broadcast('gestion-eleves::refresh')
          app.importProgress.close()
          self.setImportButtonVisible(true)
          app.openPage(new sesalab.pages.ImportSynthese(stats))
          app.toaster.toast(`Import réalisé de ${nbEleves} élèves et ${nbClasses} classes.`)
          this()
        })
        .catch(function (error) {
          if (app.importProgress) app.importProgress.close()
          self.setImportButtonVisible(true)
          app.toaster.toast(error.message, 'error')
          console.error(error)
        })
    }
  }
})
