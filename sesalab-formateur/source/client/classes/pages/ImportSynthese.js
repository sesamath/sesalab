const { CSV_SEPARATOR } = require('sesalab-commun/constants.js')
const { formatCsvCell } = require('sesalab-commun/formatString.js')
const helpers = require('sesalab-commun/helpers.js')

const getLabel = (text) => new qx.ui.basic.Label().set({ value: text, rich: true, selectable: true })

qx.Class.define('sesalab.pages.ImportSynthese', {
  extend: sesalab.widgets.ScrollablePage,
  include: [sesalab.widgets.Helper],
  construct: function (stats) {
    // stats est un objet de type import~Stats, cf sesalab-api/source/import/service.js
    this.base(arguments, 'Synthèse de l’import', 'view_details')
    const hasErrors = Array.isArray(stats.errors) && stats.errors.length
    const hasEleves = Array.isArray(stats.eleves) && stats.eleves.length

    // Titre
    const titre = getLabel('<h1>Résultats de l’import</h1>')
    this.add(titre)

    if (hasEleves) {
      const avertissement = getLabel(`<strong>Attention</strong>, vous devez sauvegarder ce rapport car il contient les mots de passe élève qui ne seront pas récupérable ensuite.<br>
Vous pouvez télécharger le fichier au format csv (à l'ouverture précisez si besoin encodage UTF-8 et séparateur ;) pour l'ouvrir dans un tableur, ou copier / coller le tableau ci-dessous.`)
      avertissement.set({ marginBottom: 20 })
      this.add(avertissement)
    }

    // On affiche la synthèse
    const titreSynthese = getLabel('<h2>Résumé</h2>').set({ padding: 2 })
    this.add(titreSynthese)

    const formatSynthese = (formatString, nb) => {
      if (!nb) return
      const plural = (nb > 1) ? 's' : ''
      const ligneLabel = getLabel(formatString.replace('#', `<strong>${nb}</strong>`).replace(/§/g, plural))
      // ligneLabel.setFocusable(true)
      this.add(ligneLabel)
    }
    formatSynthese('- # classe§ ajoutée§', stats.nbGroupsAdded)
    formatSynthese('- # classe§ mise§ à jour', stats.nbGroupsUpdated)
    formatSynthese('- # élève§ ajouté§', stats.nbUsersAdded)
    formatSynthese('- # élève§ mis à jour', stats.nbUsersUpdated)
    formatSynthese('- # élève§ ignoré§ (erreur ou indécidable entre ajout et mise à jour)', stats.nbUsersIgnored)

    if (hasErrors) {
      this.add(getLabel(`<ul class="error">Erreurs<li>${stats.errors.join('</li><li>')}</li></ul>`))
    }

    // on passe au rapport
    this.add(getLabel('<h2>Rapport d’importation</h2>'))
    if (hasEleves) {
      // Label d'aide pour les utilisateurs
      const clueMessage = (hasErrors)
        ? '<strong>Il y a eu des erreurs d’importation</strong>>, vous pouvez utiliser ce rapport pour un nouvel import (après rectification des lignes concernées, les copier / coller dans un nouvel import tableur).'
        : '<strong>Conservez ce rapport</strong>, vous pourrez l’utiliser plus tard pour refaire un import afin de rectifier quelques informations (vous pourrez alors copier / coller les lignes concernées dans un import tableur, la présence de l’identifiant vous garantira une modification plutôt qu’un ajout d’homonyme)'
      const help = getLabel(clueMessage).set({ marginTop: 20 })
      this.add(help)

      this.add(getLabel('Si le fichier csv s’il vous pose des problèmes d’ėncodage des accents, vous pouvez copier / coller le rapport ci-dessous dans votre tableur)'))
      const downloadButton = getLabel('<button>Télécharger le csv</button>').set({ cursor: 'pointer' })
      this.add(downloadButton)
      downloadButton.addListener('click', () => {
        helpers.download(csvContent, 'resultat_import_utf8.csv')
      })

      const header = ['Classe', 'Nom', 'Prénom', 'Mail', 'Identifiant', 'Mot de passe', 'Rmq mot de passe', 'Créé', 'Modifié', 'Erreur', 'Commentaires']

      let csvContent = `${header.join(CSV_SEPARATOR)}\n`
      let csvErrorsLines = ''
      const getCsvLine = (ligne) => {
        const commentaire = this.stateToComment(ligne)
        return [
          ligne.classeNom,
          ligne.nom,
          ligne.prenom,
          ligne.mail,
          // Si le login n'existe pas, on laisse vide pour faciliter un éventuel import tableur par la suite
          ligne.login,
          // Si le mot de passe n'existe pas, on laisse vide pour faciliter un éventuel import tableur par la suite
          ligne.nouveauPassword, // restera vide si y'a pas de nouveau pass, facilite un éventuel import tableur par la suite
          ligne.passwordComment,
          ligne.isEleveCreated ? 1 : '',
          ligne.isEleveUpdated ? 1 : '', // (inclus restauré ou changé de classe)
          ligne.error,
          commentaire
        ].map(formatCsvCell).join(CSV_SEPARATOR) + '\n'
      }

      stats.eleves.forEach((ligne) => {
        const csvLine = getCsvLine(ligne)
        if (ligne.error) csvErrorsLines += csvLine
        else csvContent += csvLine
      })
      if (csvErrorsLines) {
        // on crée une ligne de séparation
        csvContent += `${CSV_SEPARATOR.repeat(10)}\n`
        csvContent += `Erreurs${CSV_SEPARATOR.repeat(10)}\n`
        csvContent += csvErrorsLines
      }

      const resultsTable = this.createInlineResults(csvContent)
      this.add(resultsTable, { flex: 1 })
    } else {
      this.add(getLabel('Aucun élève n’a été importé ou modifié'))
    }
  },

  members: {
    getId: function () {
      // faut retourner un id toujours différents, sinon ça ne met pas à jour l'onglet
      // (on pourrait corriger ça, mais il faut que la fermeture de l'onglet de résultat d'import soit une action de l'utilisateur)
      // et laisse affiché le précédent (si on l'avait pas fermé avant de relancer un import)
      return 'import-resultats' + Date.now()
    },

    /**
     * Génère le commentaire d'après l'objet student
     * @private
     * @param student
     * @return {string}
     */
    stateToComment: function (student) {
      const commentaires = []

      // c'est déjà dans la colonne erreur
      if (student.error) return ''
      // le service peut proposer un message particulier
      if (student.message) {
        commentaires.push(student.message)
      } else {
        // sinon on le déduit des booléens
        if (student.isEleveCreated) {
          commentaires.push('Compte créé')
        } else if (student.isEleveUpdated) {
          commentaires.push('Compte mis à jour')
          if (student.isEleveRestored) commentaires.push('Récupéré dans la corbeille')
          if (student.aChangeDeClasse) commentaires.push('Changement de classe')
          // à priori ça n'arrive plus jamais…
          if (student.initialLogin && student.initialLogin !== student.login) commentaires.push(`Login modifié pour éviter un doublon (initialement ${student.initialLogin})`)
        } else {
          commentaires.push('Compte existant conservé à l’identique')
        }
      }

      if (!commentaires.length) return ''
      if (commentaires.length === 1) return commentaires[0] + '.'
      return commentaires.join('. ') + '.'
    },

    /**
     * Affiche le tableau complet dans la page (pour copier / coller dans un tableur)
     * @param {string} csvContent
     * @return {qx.ui.container.Scroll}
     */
    createInlineResults: function (csvContent) {
      const resultTable = getLabel('')
      resultTable.set({
        allowGrowX: true,
        allowGrowY: true,
        allowShrinkY: false
      })

      const lines = csvContent.split('\n')
      const titleLine = lines.shift().split(CSV_SEPARATOR)
      const lineToHtml = (line) => `<tr><td>${line.split(CSV_SEPARATOR).map(cell => cell.replace(/^"|"$/g, '')).join('</td><td>')}</td></tr>`
      const htmlTable = `<table>
  <thead>
    <tr>
      <th>${titleLine.join('</th><th>')}</th>
    </tr>
  </thead>
  <tbody>
    ${lines.map(lineToHtml).join('\n')}
  </tbody>
</table>`
      resultTable.setValue(htmlTable)

      const importScrollContainer = new qx.ui.container.Scroll()
      importScrollContainer.set({
        height: 400,
        minWidth: 1100,
        maxHeight: 400,
        allowGrowX: true,
        allowStretchX: true
      })
      importScrollContainer.add(resultTable, { flex: 1 })

      return importScrollContainer
    }
  }
})
