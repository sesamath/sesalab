/* global FormData */
// importOfficiel regroupe les import par fichier, ONDE ou SIECLE
const constants = require('sesalab-commun/constants.js')
const flow = require('an-flow')

qx.Class.define('sesalab.pages.ImportOfficiel', {
  extend: sesalab.widgets.ScrollablePage,
  include: [sesalab.widgets.Helper],
  construct: function (type) {
    this.type = type
    this.base(arguments, 'Import ' + type.toUpperCase(), 'add_people')
    this.content.setPadding(10, 10, 2, 10)

    // Choix du message descriptif
    let label = '<p>'
    if (type === 'onde') {
      label += 'L’import des comptes élèves à partir de ONDE nécessite de disposer du fichier CSVExtraction.csv produit par ONDE à partir d’une base à jour. Vous pouvez vous le procurer sur une clé USB ou vous le faire envoyer par mél par le directeur / la directrice de l’école si vous n’y avez pas accès vous-même.<br/><br/>'
    }
    if (type === 'siecle') {
      label += `L’import des comptes élèves à partir de SIECLE nécessite de disposer du fichier ElevesSansAdresses.xml ou sa version compressée ElevesSansAdresses.zip.<br/>
        Vous pouvez par exemple vous faire envoyer ce fichier par e-mail ou vous le procurer sur une clé USB auprès du secrétariat de votre établissement. <br/><br/>`
    }
    label += `Lors de cette procédure d’import, les noms d’utilisateur et les mots de passe provisoires (chaque élève devra le changer à sa première connexion) seront
        automatiquement générés par LaboMEP (attention à bien récupérer le rapport d'importation qui les contient, ils ne seront plus récupérables ensuite).
      </p>
      <p>
        Précisez ici votre préférence pour générer automatiquement les noms d'utilisateur :
      </p>`

    const message = new qx.ui.basic.Label(label)
    message.set({ rich: true, selectable: true })
    this.add(message)

    // Choix du format des noms d'utilisateur
    this.cbFormats = new qx.ui.form.ComboBox()
    const formats = ['nom.prenom', 'prenom.nom', 'nomp', 'pnom', 'nom.p', 'p.nom']
    for (let i = 0; i < formats.length; i++) {
      this.cbFormats.add(new qx.ui.form.ListItem(formats[i]))
    }
    this.cbFormats.setValue('nom.prenom')
    this.add(this.cbFormats)

    // Bouton de choix de fichier
    this.chooseFileBtn = new sesalab.widgets.UploadButton()
    this.chooseFileBtn.set({ marginTop: 15 })
    this.add(this.chooseFileBtn)

    // Bouton d'import
    const flowlayout = new qx.ui.layout.Flow()
    flowlayout.setAlignX('center')
    const btnContainer = new qx.ui.container.Composite(flowlayout)
    this.add(btnContainer)
    this.importButton = new qx.ui.form.Button('Importer')
    btnContainer.add(this.importButton)

    this.importButton.addListener('click', function () {
      this.setImportButtonVisible(false)
      this.process()
    }, this)
  },

  members: {
    getId: function () {
      return 'import-' + this.type
    },
    setImportButtonVisible: function (isVisible) {
      this.importButton.set({ visibility: isVisible ? 'visible' : 'excluded' })
    },
    process: function () {
      const self = this

      flow()
        .seq(function () {
          const thisSeq = this
          const files = self.chooseFileBtn.getFiles()
          if (files.length > 0) {
            const formData = new FormData()
            const file = files[0]

            formData.append('structureOid', app.structure.oid)
            formData.append('loginFormat', self.cbFormats.getValue())
            formData.append('file', file)

            const ext = file.name.match(/\.([a-z]+)$/)[1]
            if (self.type === 'onde' && ext !== 'csv') {
              self.setImportButtonVisible(true)
              return app.toaster.toast('Le fichier choisi doit être au format CSV.', 'error')
            }
            if (self.type === 'siecle' && ext !== 'xml' && ext !== 'zip') {
              self.setImportButtonVisible(true)
              return app.toaster.toast('Le fichier choisi doit être au format XML ou ZIP.', 'error')
            }
            // on a la bonne extension, on envoie
            const title = `Import ${self.type.toUpperCase()}`
            // import-onde ou import-siecle, mis ici pour être trouvé par une recherche globale du path sur tout le code
            const path = `import-${self.type}`
            app.importProgress = sesalab.widgets.ProgressDialog.execute(title)
            app.importProgress.init('Envoi des données...', 2)
            app.importProgress.tick()
            app.sesalab.post(path, formData, { timeout: constants.IMPORT_TIMEOUT }, thisSeq)
          } else {
            self.setImportButtonVisible(true)
            app.toaster.toast('Aucun fichier n’est sélectionné.', 'error')
          }
        })
        .seq(function (response) {
          app.importProgress.close()
          const { stats } = response
          const errMsg = response.message || 'Le serveur n’a pas renvoyé la réponse attendue'
          if (!stats) return this(Error(errMsg))
          if (['nbGroupsAdded', 'nbGroupsUpdated', 'nbUsersAdded', 'nbUsersUpdated'].some(p => typeof stats[p] !== 'number')) return this(Error(errMsg))
          if (!Array.isArray(stats.eleves)) return this(Error(errMsg))

          // ok, stats est bien l'objet attendu, on affiche l'info
          const nbClasses = stats.nbGroupsAdded + stats.nbGroupsUpdated
          const nbEleves = stats.nbUsersAdded + stats.nbUsersUpdated
          app.toaster.toast(`Import réalisé de ${nbEleves} élèves et ${nbClasses} classes`)
          // on rafraîchit ce qui doit l'être
          app.bus.broadcast('classes::refresh')
          app.bus.broadcast('gestion-eleves::refresh')
          // on ouvre la synthèse et ferme cet onglet
          app.openPage(new sesalab.pages.ImportSynthese(stats))
          app.removePage(self)
        })
        .catch(function (error) {
          if (app.importProgress) app.importProgress.close()
          self.setImportButtonVisible(true)
          app.toaster.toast(error.message, 'error')
          console.error(error)
        })
    }
  }
})
