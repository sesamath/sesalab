qx.Theme.define('sesalab.theme.Color', {
  extend: qx.theme.indigo.Color,

  colors: {
    'background-selected': '#CADBE7',
    'text-selected': '#3B3B3B',
    'sesalab-orange': '#E16400',
    'bg-side-panel': '#525252',
    'fg-side-panel': '#EEEEEE',
    tooltip: '#FFFFE1',
    'tooltip-text': 'black'

  }
})
