qx.Theme.define('sesalab.theme.Theme', {
  meta: {
    color: sesalab.theme.Color,
    decoration: sesalab.theme.Decoration,
    font: sesalab.theme.Font,
    icon: sesalab.theme.icons,
    appearance: sesalab.theme.Appearance
  }
})
