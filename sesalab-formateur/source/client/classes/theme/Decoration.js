/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

qx.Theme.define('sesalab.theme.Decoration', {
  extend: qx.theme.indigo.Decoration,

  decorations: {
    hoverDecorator: {
      style:
      {
        backgroundColor: '#E8E8E8',
        widthTop: 1,
        styleTop: 'solid',
        colorTop: '#D5D5D5',
        widthBottom: 1,
        styleBottom: 'solid',
        colorBottom: '#D5D5D5'
      }
    },
    dragDecoratorTop: {
      style:
      {
        backgroundColor: '#E8E8E8',
        widthTop: 1,
        styleTop: 'dotted',
        colorTop: 'orange',
        widthBottom: 1,
        styleBottom: 'solid',
        colorBottom: '#D5D5D5'
      }
    },
    dragDecoratorBottom: {
      style:
      {
        backgroundColor: '#E8E8E8',
        widthBottom: 1,
        styleBottom: 'dotted',
        colorBottom: 'orange',
        widthTop: 1,
        styleTop: 'solid',
        colorTop: '#D5D5D5'
      }
    },
    'scrollbar-button':
    {
      style:
      {
        backgroundColor: '#F4F4F4'
      }
    },
    'scroll-knob':
    {
      style:
      {
        backgroundColor: '#BCBCBC',
        width: 2,
        innerWidth: 1,
        innerColor: '#989898',
        color: '#F4F4F4'
      }
    },
    plain: {
      style: {
        backgroundColor: '#FFFFFF'
      }
    },
    'page-header': {
      style: {
        innerWidthBottom: 1,
        innerColorBottom: 'sesalab-orange',
        widthBottom: 3,
        colorBottom: '#FCA95D',

        gradientStart: ['#FFFFFF', 0],
        gradientEnd: ['#E5E5FF', 100],

        backgroundColor: '#323335'
      }
    },
    'panel-head': {
      style: {
        colorBottom: '#EFEFEF',
        widthBottom: 1
      }
    },
    'panel-head-open': {
      include: 'panel-head',
      style: {
        colorBottom: '#FFCF00',
        widthBottom: 3
      }
    },
    outline: {
      style: {
        width: 1,
        color: 'red'
      }
    },
    toolbar: {
      style: {
        widthBottom: 1,
        colorBottom: '#FFEFAD',
        gradientStart: ['#FFFFFF', 0],
        gradientEnd: ['#FFF7D3', 100]
      }
    },
    toaster: {
      style: {
        width: 0,
        color: '#BBBBBB',
        gradientStart: ['#FFFFFF', 0],
        gradientEnd: ['#E6E6E6', 100]
      }
    },
    'toaster-error': {
      style: {
        widthLeft: 3,
        colorLeft: '#E50E10',
        widthRight: 3,
        colorRight: '#E50E10',
        widthBottom: 1,
        colorBottom: '#E50E10'
      }
    },
    'toaster-warning': {
      style: {
        widthLeft: 3,
        colorLeft: '#DCAB2B',
        widthRight: 3,
        colorRight: '#DCAB2B',
        widthBottom: 1,
        colorBottom: '#DDDDDD'
      }
    },
    'toaster-info': {
      style: {
        widthLeft: 3,
        colorLeft: '#82AAC2',
        widthRight: 3,
        colorRight: '#82AAC2',
        widthBottom: 1,
        colorBottom: '#DDDDDD'
      }
    },
    inset: {
      style: {
        color: '#E0E0E0',
        width: 1
      }
    },
    'panel-head-label': {
      style: {
        color: '#FF00FF'
      }
    },
    error: {
      style: {
        width: 1,
        color: '#D8000C',
        backgroundColor: '#FFBABA',
        shadowLength: 1,
        shadowBlurRadius: 5,
        shadowColor: '#BDBDBD'
      }
    },
    tooltip: {
      style: {
        width: 1,
        gradientStart: ['#FDFCAF', 0],
        gradientEnd: ['#F8F594', 100],
        color: '#C4C298',
        shadowLength: 1,
        shadowBlurRadius: 5,
        shadowColor: '#BDBDBD'
      }
    },
    'toolbar-button-box': {
      style: {
        width: 0
      }
    },
    'button-box-pressed': {
      style: {
        width: 0,
        gradientStart: ['#FFDA35', 0],
        gradientEnd: ['#FFDA35', 100],
        radius: 2
      }
    },
    link: {
      style: {
        width: 1,
        colorBottom: 'rgb(61, 114, 201)'
      }
    }
  }
})
