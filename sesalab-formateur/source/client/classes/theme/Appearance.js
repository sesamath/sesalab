/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/* ***

#asset(icons/20/*)
#asset(icons/16/*)
#asset(/icons/ressources/*)
#asset(images/*)

*/
qx.Theme.define('sesalab.theme.Appearance', {
  extend: qx.theme.indigo.Appearance,

  appearances: {
    root:
     {
       style: function (states) {
         let font = 'default'
         if (states.smallFont || states.bigFont) {
           font = states.smallFont ? 'small' : 'big'
         }

         return {
           font
         }
       }
     },
    'virtual-tree': {
      style: function () {
        return { decorator: 'plain' }
      }
    },
    'virtual-tree/scrollbar-x': 'scrollbar',
    'virtual-tree/scrollbar-y': 'scrollbar',
    scrollbar: {},
    'scrollbar/slider': {
      style: function () {
        return { decorator: 'scrollbar-button' }
      }
    },
    'scrollbar/button': {
      style: function (states) {
        const styles = {}
        styles.padding = 4

        let icon = ''
        if (states.left) {
          icon = 'left'
          styles.marginRight = 2
        } else if (states.right) {
          icon += 'right'
          styles.marginLeft = 2
        } else if (states.up) {
          icon += 'up'
          styles.marginBottom = 2
        } else {
          icon += 'down'
          styles.marginTop = 2
        }

        styles.icon = qx.theme.simple.Image.URLS['arrow-' + icon]

        styles.cursor = 'pointer'
        styles.decorator = 'scrollbar-button'
        return styles
      }
    },

    'scrollbar/button-begin': 'scrollbar/button',
    'scrollbar/button-end': 'scrollbar/button',

    'sesalab-header': {
      style: function () {
        return {
          decorator: 'page-header'
        }
      }
    },
    'sesalab-header/label': {
      style: function () {
        return {
          font: 'header',
          textColor: '#252525',
          padding: [10, 10]
        }
      }
    },

    'sesalab-header/version': {
      style: function () {
        return {
          font: 'version',
          textColor: '#252525',
          padding: [30, 0, 0, 0],
          marginLeft: 15
        }
      }
    },

    'sesalab-header/links': {
      style: function (states) {
        const active = states.pressed || states.focused
        return {
          backgroundColor: active ? 'rgb(220, 220, 255)' : undefined,
          textColor: '#252525',
          padding: [10, 10],
          cursor: 'pointer'
        }
      }
    },

    'collapsable-panel': {
      style: function (/* states */) {
        return {
          margin: 0
        }
      }
    },

    'collapsable-panel/bar': {
      style: function (states) {
        return {
          padding: [6, 8, 0, 8],
          decorator: states.opened ? 'panel-head-open' : 'panel-head',
          height: 20,
          textColor: states.opened ? '#000000' : '#777777',
          allowGrowY: !states.opened,
          allowGrowX: states.opened
        }
      }
    },
    'collapsable-panel/bar/label': {
      style: function () {
        return {
          font: 'title',
          padding: [0, 0],
          margin: [-1, 0, 4, 0],
          decorator: 'panel-head-label'
        }
      }
    },
    'collapsable-panel/bar/icon': {
      style: function () {
        return {
          margin: [-2, 0]
        }
      }
    },
    'collapsable-panel/toolbar': {
      style: function (/* states */) {
        return {
        }
      }
    },

    /*
    "collapsable-panel/tree" :
    {
      include : "tree",
      alias : "tree",

      style : function(states) {
        return {
          decorator: "borderless"
        };
      }
    }, */

    'sesalab-main-panel': {
      style: function (/* states */) {
        return {
          padding: [10, 1]
        }
      }
    },

    toolbar: {
      style: function () {
        return {
          padding: 0,
          decorator: 'toolbar'
        }
      }
    },

    toaster: {
      style: function () {
        return {
          padding: 10,
          decorator: 'toaster'
        }
      }
    },

    'toolbar-button': {
      alias: 'atom',

      style: function (states) {
        let decorator = 'toolbar-button-box'
        if (states.disabled) {
          decorator = 'toolbar-button-box'
        } else if (states.pressed || states.checked) {
          decorator = 'button-box-pressed'
        }

        // set the margin
        let margin = [3, 5, 3, 5]
        if (states.left || states.middle || states.right) {
          margin = [7, 0]
        }

        return {
          cursor: states.disabled ? undefined : 'pointer',
          decorator,
          margin,
          padding: [3, 5]
        }
      }
    },

    'datefield/button': {
      alias: 'combobox/button',
      include: 'combobox/button',

      style: function () {
        return {
          icon: 'icons/20/calendar.png',
          padding: [0, 0, 0, 3],
          backgroundColor: undefined,
          decorator: undefined,
          width: 19
        }
      }
    },

    'tabview-page/button': {
      style: function (states) {
        let decorator, padding

        if (states.barTop || states.barBottom) {
          padding = [3, 6, 0, 5]
        } else {
          padding = [8, 4, 8, 4]
        }

        // decorator
        if (states.checked) {
          if (states.barTop) {
            decorator = 'tabview-page-button-top'
          } else if (states.barBottom) {
            decorator = 'tabview-page-button-bottom'
          } else if (states.barRight) {
            decorator = 'tabview-page-button-right'
          } else if (states.barLeft) {
            decorator = 'tabview-page-button-left'
          }
        } else {
          for (let i = 0; i < padding.length; i++) {
            padding[i] += 1
          }
          // reduce the size by 1 because we have different decorator border width
          if (states.barTop) {
            padding[2] -= 1
          } else if (states.barBottom) {
            padding[0] -= 1
          } else if (states.barRight) {
            padding[3] -= 1
          } else if (states.barLeft) {
            padding[1] -= 1
          }
        }

        return {
          zIndex: states.checked ? 10 : 5,
          decorator,
          textColor: states.disabled ? 'text-disabled' : states.checked ? null : 'link',
          padding,
          cursor: 'pointer'
        }
      }
    }

  }

})
