const family = ['Roboto', 'sans-serif', 'Noto Emoji']

qx.Theme.define('sesalab.theme.Font', {
  extend: qx.theme.indigo.Font,

  fonts: {
    header: {
      size: 22,
      family,
      lineHeight: 1
    },
    version: {
      size: 10,
      family,
      lineHeight: 1
    },
    title: {
      size: 14,
      family,
      lineHeight: '20px'
    },
    default: {
      size: 12.5,
      family
    },
    big: {
      size: 14,
      family
    },

    bold: {
      size: 13,
      family,
      bold: true
    },

    headline: {
      size: 24,
      family
    },

    small: {
      size: 11.5,
      family
    },

    monospace: {
      size: 11,
      family: ['DejaVu Sans Mono', 'Courier New', 'monospace']
    }
  }
})
