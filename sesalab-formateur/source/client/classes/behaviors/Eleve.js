qx.Class.define('sesalab.behaviors.Eleve',
  sesalab.behaviors.Common.build({
    name: 'Eleve',
    icon: 'icons/20/person.png',
    init: function (data) {
      this.setCaption(this.data.nom + ' ' + this.data.prenom)
      // On supprime le password pour éviter d'avoir le password crypté au chargement
      delete this.data.password
    },
    information: function () {
      if (this.data.oid) return 'Laissez le mot de passe vide pour le conserver'
      else return ''
    }
  }, {
    nom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(this.data.nom + ' ' + this.data.prenom)
      },
      widget: {
        label: "Nom de l'élève"
      }
    },
    prenom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.prenom = value
        this.setCaption(this.data.nom + ' ' + this.data.prenom)
      },
      widget: {
        label: 'Prénom'
      }
    },
    login: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Identifiant'
      }
    },
    password: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Mot de passe',
        type: 'PasswordField'
      }
    }
  })
)
