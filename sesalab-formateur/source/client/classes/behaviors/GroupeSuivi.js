qx.Class.define('sesalab.behaviors.GroupeSuivi',
  sesalab.behaviors.Common.build({
    name: 'GroupeSuivi',
    icon: 'icons/20/people.png',
    idField: 'name',
    init: function (data) {
      this.setCaption(this.data.name)
    },
    mayHaveChildren: function () {
      return true
    }
  })
)
