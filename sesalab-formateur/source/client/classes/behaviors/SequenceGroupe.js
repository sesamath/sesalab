qx.Class.define('sesalab.behaviors.SequenceGroupe',
  sesalab.behaviors.Common.build({
    name: 'SequenceGroupe',
    icon: 'icons/20/people.png',
    init: function (data) {
      this.setCaption(this.data.nom)
    },
    information: function () {
      const groupe = app.store.get({ type: 'Groupe', id: this.data.oid })
      if (!groupe) return
      return 'Contient les élèves<br><ul><li>' + groupe.utilisateurs.map(function (u) {
        if (typeof u !== 'object') {
          u = app.store.get({ type: 'Eleve', id: u })
        }
        return u.prenom + ' ' + u.nom
      }).join('</li><li>') + '</li></ul>'
    }
  }, {
    nom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(this.data.nom)
      },
      widget: {
        label: 'Nom'
      }
    }
  })
)
