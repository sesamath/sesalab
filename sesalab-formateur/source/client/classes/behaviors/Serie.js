qx.Class.define('sesalab.behaviors.Serie', {
  extend: sesalab.behaviors.Common,
  construct: function (data) {
    this.base(arguments, data)
    this.setIcon('icons/20/parallel_tasks.png')
    this.setCaption('Série')
  }
})
