const _ = require('lodash')

/**
 * Définition du comportement UI d'une séquence
 */
qx.Class.define('sesalab.behaviors.SequenceModele',
  sesalab.behaviors.Common.build({
    name: 'SequenceModele',
    icon: 'icons/ressources/sequenceModele.png',
    statics: {
      createFromSequence: function (sequence) {
        const clonedSequence = _.cloneDeep(sequence)

        delete clonedSequence.$behavior
        _.each(clonedSequence.sousSequences, (sousSequence) => {
          sousSequence.eleves = []
        })

        return sesalab.behaviors.SequenceModele.create(clonedSequence)
      }
    },
    idField: 'rid',
    init: function (data) {
      this.setCaption(this.data.titre || this.data.nom)

      const icon = app.icons.sequence({
        sequenceModel: true,
        width: 20,
        height: 20,
        strokeWidth: 4,
        flags: {
          prioritaire: this.data.prioritaire,
          inactive: this.data.statut === 0,
          group: !this.data.public && this.data.partage,
          friends: !this.data.public && !this.data.partage,
          limiteTemps: this.data.statut === 2,
          uniqEleve: this.data.uniqEleve
        }
      })
      this.setIcon(app.icons.toBase64(icon))
    }
  }))
