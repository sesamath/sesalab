qx.Class.define('sesalab.behaviors.Eleves',
  sesalab.behaviors.Common.build({
    name: 'Eleves',
    icon: 'icons/20/people.png',
    init: function () {
      this.setCaption('Élèves')
    }
  }, {})
)
