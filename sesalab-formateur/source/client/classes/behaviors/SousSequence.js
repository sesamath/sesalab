const constants = require('sesalab-commun/constants.js')

/**
 * Définition du comportement UI d'une séquence
 */
qx.Class.define('sesalab.behaviors.SousSequence',
  sesalab.behaviors.Common.build({
    name: 'SousSequence',
    icon: 'icons/20/workflow.png',
    init: function (data) {
      this.data.nom = this.data.nom || 'sous-séquence'
      this.setCaption(this.data.nom)
    }
  }, {
    nom: {
      type: 'String',
      defaults: 'sous-séquence',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(value)
      },
      widget: {
        label: 'Nom'
      }
    },
    type: {
      group: 'Propriétés appliquables aux sous-éléments',
      type: 'Number',
      defaults: constants.SERIE_STATUT_LIBRE,
      widget: {
        type: 'SelectBox',
        label: 'Type',
        help:
          'L’élève peut naviguer à sa guise dans une séquence <strong>libre</strong>. Dans une séquence <strong>ordonnée</strong>, il doit parcourir les ressources dans l’ordre où le professeur les a placées.' +
          'Dans une <strong>séquence ordonnée avec minimum de réussite</strong>, pour certains types de ressources dont les exercices interactifs, il doit atteindre un seuil minimum de réussite défini par le' +
          'professeur (par défaut 70% de réussite, paramétrable par un clic droit sur chaque exercice).',
        items: {
          [constants.SERIE_STATUT_LIBRE]: 'Libre',
          [constants.SERIE_STATUT_ORDONNEE]: 'Ordonnée',
          [constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE]: 'Ordonnée avec un minimum de réussite'
        },
        onChange: function (data, fields, propertyName) {
          const type = parseInt(data.$behavior.get(propertyName))
          fields.minimumReussite.set({ enabled: type === constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE })
          fields.maximumVisionnage.set({ enabled: type === constants.SERIE_STATUT_LIBRE })
        }
      }
    },

    nonZapping: {
      type: 'Number',
      defaults: constants.DEFAULT_NON_ZAPPING,
      widget: {
        label: 'Non Zapping',
        help:
          'Pour éviter que les élèves ne «zappent», il existe un délai de 10 secondes pendant lequel l’application ne les autorisera pas à changer d’exercice, une fois celui-ci en marche. Vous pouvez imposer un délai «de non-zapping» supérieur.' +
          'Par ailleurs, vous pouvez imposer un <strong>maximum pour le nombre de visionnage général</strong> d’une ressource (pour limiter le nombre de fois où un exercice est fait par exemple).',
        minimum: 0
      }
    },

    minimumReussite: {
      type: 'Number',
      defaults: constants.DEFAULT_MINIMUM_REUSSITE,
      widget: {
        label: 'Min. réussite (%)',
        help: '',
        minimum: 0
      }
    },

    maximumVisionnage: {
      type: 'Number',
      defaults: constants.DEFAULT_MAX_VISIONNAGE,
      widget: {
        label: 'Max. visionnage',
        help: 'Imposer un maximum de visionnage général. 0 indique "aucune limite". Attention, les exercices issus de la première version de Mathenpoche ou de Calculatice contiennent un bouton "Recommencer" à la fin de l’exercice qui n’est pas désactivable par Labomep.',
        minimum: 0
      }
    }
  })
)
