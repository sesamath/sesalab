qx.Class.define('sesalab.behaviors.Niveau', {
  extend: sesalab.behaviors.Common,
  construct: function (data) {
    this.base(arguments, data)
    this.setIcon('icons/20/folder.png')
    this.setCaption(this.data.getIntitule())
  }
})
