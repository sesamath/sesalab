qx.Class.define('sesalab.behaviors.Utilisateur',
  sesalab.behaviors.Common.build({
    name: 'Utilisateur',
    icon: 'icons/20/person.png',
    init: function (data) {
      this.setCaption(this.data.nom + ' ' + this.data.prenom)
    }
  }, {
    login: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Identifiant',
        enabled: function () { return false }
      }
    },
    nom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(this.data.prenom + ' ' + this.data.nom)
      },
      widget: {
        label: 'Nom'
      }
    },
    prenom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.prenom = value
        this.setCaption(this.data.prenom + ' ' + this.data.nom)
      },
      widget: {
        label: 'Prénom'
      }
    },
    mail: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'eMail'
      }
    },
    password: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Nouveau mot de passe',
        type: 'PasswordField'
      }
    },
    passwordBis: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Confirmer mot de passe',
        type: 'PasswordField'
      }
    },
    passwordCurrent: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Mot de passe actuel',
        type: 'PasswordField'
      }
    },
    supprimerCompte: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: 'Supprimer mon compte',
        type: 'CheckBox'
      }
    }
  })
)
