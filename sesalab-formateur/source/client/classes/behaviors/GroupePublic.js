qx.Class.define('sesalab.behaviors.GroupePublic',
  sesalab.behaviors.Common.build({
    name: 'GroupePublic',
    icon: 'icons/20/organization.png',
    idField: 'name',
    init: function (data) {
      const caption = (data.structure && data.structure.nom) || 'Public'
      this.setCaption(caption)
    },
    mayHaveChildren: function () {
      return true
    }
  })
)
