const _ = require('lodash')
const { tz } = require('moment-timezone')
const { TYPE_FORMATEUR } = require('sesalab-commun/constants.js')

qx.Class.define('sesalab.behaviors.Structure',
  sesalab.behaviors.Common.build({
    name: 'Structure',
    icon: 'icons/20/people.png',
    init: function () {
      this.setCaption(this.data.nom)
    }
  }, {
    // ATTENTION à modifier aussi sesalab-formateur/source/client/libraries/structure/index.js
    // si la liste des champs change
    nom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(this.data.nom)
      },
      widget: {
        label: 'Nom de la structure',
        enabled: () => !app.settings.sso.hasStructureManagement
      }
    },
    code: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Code de la structure',
        enabled: () => false
      }
    },

    type: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Type',
        enabled: () => !app.settings.sso.hasStructureManagement
      }
    },
    $utilisateurs: {
      type: 'String',
      widget: {
        label: "Liste des formateurs. Attention, toute modification de l'établissement concerne aussi vos collègues, merci de vous concerter avec eux.",
        type: 'List',
        getValues: function (data) {
          return _.map(_.filter(data, function (f) {
            return f.type === TYPE_FORMATEUR
          }), function (f) {
            return f.nom + ' ' + f.prenom
          })
        },
        enabled: () => false
      }
    },
    ville: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Ville',
        enabled: () => !app.settings.sso.hasStructureManagement
      }
    },
    departement: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Département',
        enabled: () => !app.settings.sso.hasStructureManagement
      }
    },
    region: {
      type: 'String',
      defaults: '',
      widget: {
        label: 'Région',
        enabled: () => !app.settings.sso.hasStructureManagement
      }
    },
    pays: {
      type: 'String',
      defaults: 'france',
      widget: {
        label: 'Pays',
        enabled: () => !app.settings.sso.hasStructureManagement
      }
    },
    timezone: {
      type: 'String',
      nullable: true,
      defaults: tz.guess(),
      widget: {
        type: 'SelectBox',
        label: 'Fuseau horaire',
        // Ils nous faut les fuseaux horaires sous le format clé -> valeur,
        // ici la clé et la valeur sont identiques
        items: _.keyBy(tz.names(), _.identity)
      }
    },
    datePurge: {
      type: 'Date',
      nullable: false,
      defaults: null,
      widget: {
        label: 'Date de la prochaine purge des comptes élèves',
        format: 'dd/MM/yyyy',
        enabled: (structure) => !structure.onlyFromEntId
      }
    },
    ajoutElevesAuthorises: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: "Autoriser les élèves à demander leur création de compte, qu'un formateur devra valider",
        type: 'CheckBox',
        enabled: (structure) => !structure.onlyFromEntId
      }
    },
    ajoutFormateursInterdit: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: 'Interdire la demande de création de compte formateur',
        type: 'CheckBox',
        enabled: (structure) => !structure.onlyFromEntId
      }
    },
    accesMultiple: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: 'Autoriser les élèves à se connecter à plusieurs',
        type: 'CheckBox',
        enabled: (structure) => !structure.onlyFromEntId
      }
    },
    bloquerExerciceAutonomie: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: "Couper l'accès aux exercices à réaliser en autonomie",
        type: 'CheckBox'
      }
    }
  })
)
