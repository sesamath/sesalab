qx.Class.define('sesalab.behaviors.GroupePublicAuteur',
  sesalab.behaviors.Common.build({
    name: 'GroupePublicAuteur',
    icon: 'icons/20/person.png',
    idField: 'name',
    init: function (data) {
      this.setCaption(data.name)
    },
    mayHaveChildren: function () {
      return true
    }
  })
)
