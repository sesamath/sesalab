qx.Class.define('sesalab.behaviors.Groupe',
  sesalab.behaviors.Common.build({
    name: 'Groupe',
    icon: 'icons/20/people.png',
    init: function (data) {
      this.setCaption(this.data.nom)
      if (!data.utilisateurs) {
        this.data.utilisateurs = []
      }
    },
    removeEleve: function (eleve) {
      this.data.utilisateurs = this.data.utilisateurs.filter(({ oid }) => oid !== eleve.oid)
      this.updateStore()
    },
    addEleve: function (eleve) {
      this.data.utilisateurs.push(eleve)
      this.updateStore()
    }
  }, {
    nom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(this.data.nom)
      },
      widget: {
        label: 'Nom'
      }
    },
    niveau: {
      type: 'String',
      defaults: '1',
      widget: {
        label: 'Niveau',
        type: 'SelectBox',
        items: function () {
          const items = {}
          const niveaux = sesalab.common.Niveaux.getInstance().toArray()
          for (const i in niveaux) {
            items[niveaux[i].getId()] = niveaux[i].getIntitule()
          }
          return items
        }
      }
    }

  })
)
