qx.Class.define('sesalab.behaviors.Dossier',
  sesalab.behaviors.Common.build({
    name: 'Dossier',
    icon: 'icons/20/folder.png',
    idField: 'uuid',
    init: function () {
      this.setCaption(this.data.nom)
    }
  }, {
    nom: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(this.data.nom)
      },
      widget: {
        label: 'Nom du dossier'
      }
    }
  }
  ))
