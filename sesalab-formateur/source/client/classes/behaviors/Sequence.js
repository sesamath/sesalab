const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')
const moment = require('moment')

/**
 * Définition du comportement UI d'une séquence
 */
qx.Class.define('sesalab.behaviors.Sequence',
  sesalab.behaviors.Common.build({
    name: 'Sequence',
    icon: 'icons/20/database.png',
    statics: {
      createFromSequenceModel: function (sequenceModel) {
        // Clean-up useless data
        delete sequenceModel.oid
        _.each(sequenceModel.sousSequences, (sousSequence) => {
          sousSequence.eleves = []
        })

        // Enlève le propriétaire + assigne la structure actuelle
        delete sequenceModel.owner
        sequenceModel.structure = app.structure.oid

        return sesalab.behaviors.Sequence.create(sequenceModel)
      }
    },
    mayHaveChildren: function () { return false },
    refreshIcon: function () {
      const isPrivate = !this.data.public
      const hasGroups = this.data.groupes?.length > 0
      const icon = app.icons.sequence({
        sequenceModel: this.data.structure !== app.structure.oid,
        width: 20,
        height: 20,
        strokeWidth: 4,
        flags: {
          prioritaire: this.data.prioritaire,
          inactive: this.data.statut === 0,
          group: isPrivate && hasGroups,
          friends: this.data.publicAmis,
          limiteTemps: this.data.statut === 2,
          uniqEleve: this.data.uniqEleve
        }
      })
      this.setIcon(app.icons.toBase64(icon))
    },
    init: function () {
      this.refreshIcon()
    },
    information: function () {
      if (!this.data.lastChange) return null
      return 'Dernière modification le ' + moment(this.data.lastChange.date).format('DD/MM/YYYY')
    }
  }, // constructor
  // properties
  {
    owner: { hidden: true },
    sousSequences: { hidden: true },
    groups: {
      main: 'truc',
      misc: 'machin'
    },

    /**
     * Nom de la séquence
     */
    nom: {
      group: 'Configuration principale',
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.nom = value
        this.setCaption(value)
      },
      widget: {
        label: 'Nom'
      }
    },

    /**
     * Description de la séquence
     */
    description: {
      type: 'String',
      defaults: '',
      widget: {
        type: 'TextArea',
        label: 'Description',
        attributes: {
          height: 155
        },
        help: 'Le descriptif de la séquence est facultatif et visible uniquement par le professeur.'
      }
    },

    /**
     * Statut de la séquence
     */
    statut: {
      type: 'Number',
      defaults: 1,
      widget: {
        label: 'Statut',
        help:
          'Une séquence <strong>Active</strong> est toujours utilisable par ' +
          'les élèves (sans contrainte horaire).<br>' +
          'Au contraire, une séquence ' +
          '<strong>inactive</strong> ne peut pas (ou plus) être utilisée par les élèves.<br/>' +
          'Enfin, une séquence <strong>Bornée dans le temps</strong> est ' +
          'utilisable par les élèves entre les heures de début et de fin ' +
          'choisies par le professeur.',
        type: 'SelectBox',
        items: {
          [constants.SEANCE_STATUT_INACTIF]: 'Inactive',
          [constants.SEANCE_STATUT_ACTIF]: 'Active',
          [constants.SEANCE_STATUT_BORNEE]: 'Bornée dans le temps'
        },
        onChange: function (data, fields, propertyName) {
          let value = data.$behavior.get(propertyName)
          value = value === constants.SEANCE_STATUT_BORNEE ? 'visible' : 'excluded'
          fields.fromDate.set({ visibility: value })
          fields.fromTime.set({ visibility: value })
          fields.toDate.set({ visibility: value })
          fields.toTime.set({ visibility: value })
        }
      }
    },

    fromDate: {
      type: 'Date',
      defaults: null,
      nullable: true,
      widget: {
        label: 'du',
        format: 'dd/MM/YYYY'
      }
    },

    fromTime: {
      type: 'Number',
      defaults: 0,
      widget: {
        type: 'TimeBox',
        label: 'à'
      }
    },

    toDate: {
      type: 'Date',
      nullable: true,
      defaults: null,
      widget: {
        label: 'au',
        format: 'dd/MM/YYYY'
      }
    },

    toTime: {
      type: 'Number',
      defaults: 0,
      widget: {
        type: 'TimeBox',
        label: 'à'
      }
    },

    prioritaire: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: 'Prioritaire',
        help: 'Si une ou plusieurs séquences sont <strong>prioritaires</strong> et actives, elles seules peuvent être faites par les élèves. Dans ce cas, les séquences actives mais non prioritaires sont considérées comme inactives.'
      }
    },

    message: {
      type: 'String',
      defaults: '',
      widget: {
        type: 'TextArea',
        label: 'Message',
        help: 'Le message adressé aux élèves est facultatif : il est affiché dans la page d’accueil des élèves sous le nom de la séquence.'
      }
    },

    lineaire: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: 'Lineaire',
        help: `
        Les sous-séquences d'une séquence linéaire doivent être traitées dans
        l'ordre. Si une séquence n'est pas linéaire, ses différentes
        sous-séquences peuvent être traitées dans n'importe quel ordre, mais à
        l'intérieur de chaque sous-séquence le paramétrage de la sous-séquence
        détermine les ressources accessibles.
        `
      }
    },

    uniqEleve: {
      type: 'Boolean',
      defaults: false,
      widget: {
        label: 'Elève unique',
        help: 'Les séquences à élève unique permettent de restreindre l’accès des exercices à un seul élève y compris lors d’une connexion multiple.'
      }
    },

    type: {
      group: 'Propriétés appliquables aux sous-éléments',
      type: 'Number',
      defaults: 0,
      widget: {
        type: 'SelectBox',
        label: 'Type',
        help:
          'L’élève peut naviguer à sa guise dans une séquence <strong>libre</strong>. Dans une séquence <strong>ordonnée</strong>, il doit parcourir les ressources dans l’ordre où le professeur les a placées.' +
          'Dans une <strong>séquence ordonnée avec minimum de réussite</strong>, pour certains types de ressources dont les exercices interactifs, il doit atteindre un seuil minimum de réussite défini par le' +
          'professeur (par défaut 70% de réussite, paramétrable par un clic droit sur chaque exercice).',
        items: {
          [constants.SERIE_STATUT_LIBRE]: 'Libre',
          [constants.SERIE_STATUT_ORDONNEE]: 'Ordonnée',
          [constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE]: 'Ordonnée avec un minimum de réussite'
        },
        onChange: function (data, fields, propertyName) {
          const type = parseInt(data.$behavior.get(propertyName))
          fields.minimumReussite.set({ enabled: type === constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE })
          fields.maximumVisionnage.set({ enabled: type === constants.SERIE_STATUT_LIBRE })
        }
      }
    },

    nonZapping: {
      type: 'Number',
      defaults: constants.DEFAULT_NON_ZAPPING,
      widget: {
        label: 'Non Zapping',
        help:
          'Pour éviter que les élèves ne «zappent», il existe un délai de 10 secondes pendant lequel l’application ne les autorisera pas à changer d’exercice, une fois celui-ci en marche. Vous pouvez imposer un délai «de non-zapping» supérieur.' +
          'Par ailleurs, vous pouvez imposer un <strong>maximum pour le nombre de visionnage général</strong> d’une ressource (pour limiter le nombre de fois où un exercice est fait par exemple).',
        minimum: 0
      }
    },

    minimumReussite: {
      type: 'Number',
      defaults: constants.DEFAULT_MINIMUM_REUSSITE,
      widget: {
        label: 'Min. réussite (%)',
        help: '',
        minimum: 0
      }
    },

    maximumVisionnage: {
      type: 'Number',
      defaults: constants.DEFAULT_MAX_VISIONNAGE,
      widget: {
        label: 'Max. visionnage',
        help: 'Imposer un maximum de visionnage général. 0 indique "aucune limite". Attention, les exercices issus de la première version de Mathenpoche ou de Calculatice contiennent un bouton "Recommencer" à la fin de l’exercice qui n’est pas désactivable par Labomep.',
        minimum: 0
      }
    }
  }
  ))
