const _ = require('lodash')
// const hasProp = (obj, prop) => typeof obj === 'object' && Object.prototype.hasOwnProperty.call(obj, prop)
const { hasProp } = require('sesajstools')

/**
 * Retourne text avec la première lettre en majuscule
 * @param {string} text
 * @return {string}
 * @private
 */
const firstUpper = (text) => text.charAt(0).toUpperCase() + text.substr(1)

/**
 * Factory générique de construction des classes sesalab.behaviors.*
 */
qx.Class.define('sesalab.behaviors.Common', {
  extend: qx.core.Object,

  construct: function (data) {
    this.data = data
  },

  properties: {
    caption: {
      init: 'sans-titre',
      event: 'changeCaption'
    },
    icon: {
      init: '/formateur/icons/20/decision.png',
      event: 'changeIcon'
    }

  },

  members: {
    mayHaveChildren: function () { return false }
  },

  statics: {
    /**
     * Retourne une classe pouvant être passée en 2e arg d'un qx.Class.define
     * @param classDesc
     * @param properties
     * @return {{extend: sesalab.behaviors.Common, construct: construct, statics: {create: (function(*=)), check: (function(*): boolean)}, members: {clone: (function()), apply: apply, hasProperty: (function(*=): boolean), getProperties: (function(): *), getProperty: (function(*): *), updateStore: updateStore, remove: remove, init: (*|Function)}, properties: {}}}
     */
    build: function (classDesc, properties) {
      classDesc.init = classDesc.init || function () {}
      classDesc.idField = classDesc.idField || 'oid'
      properties = properties || {}

      // la classe qui sera retournée
      const desc = {
        extend: sesalab.behaviors.Common,

        construct: function (data) {
          this.base(arguments, data)
          this.setIcon(classDesc.icon)
          for (const name in properties) {
            if (!hasProp(data, name)) {
              data[name] = properties[name].defaults
            }
            if (!properties[name].hidden) {
              this['init' + firstUpper(name)](data[name])
            }
          }
          this.init(data)
        },

        statics: {
          /**
           * Création d'un objet à stocker
           * @param {Object} data l'objet source "raw" qui sera cloné
           * @return {Object} l'objet avec comportement
           */
          create: function (data) {
            data = data || {}
            const result = {}

            // Initialisation des champs avec des valeurs par défault
            for (const name in properties) {
              result[name] = properties[name].defaults
            }

            // shallow copy des propriétés de data
            for (const name in data) {
              result[name] = data[name]
            }
            Object.defineProperty(result, '$behavior', {
              value: new sesalab.behaviors[classDesc.name](result)
            })
            const id = result[classDesc.idField]
            if (id) {
              app.store.put({
                type: classDesc.name,
                key: classDesc.idField,
                value: result
              })
            }
            return result
          },

          check: function (data) {
            return data.$behavior.constructor === sesalab.behaviors[classDesc.name]
          }

        },
        members: {
          clone: function () {
            return this.constructor.create(this.data)
          },

          apply: function (values) {
            for (const name in values) {
              if (this.hasProperty(name)) {
                this.set(name, values[name])
              } else {
                this.data[name] = values[name]
              }
            }
          },
          hasProperty: function (name) {
            return hasProp(properties, name)
          },
          getProperties: function () {
            return properties
          },
          getProperty: function (name) {
            return properties[name]
          },
          updateStore: function () {
            const id = this.data[classDesc.idField]
            if (!id) {
              console.warn("Pas d'id pour", classDesc.name, this.data)
            } else {
              app.store.put({
                type: classDesc.name,
                key: classDesc.idField,
                value: this.data
              })
            }
          },
          remove: function () {
            const id = this.data[classDesc.idField]
            if (!id) {
              console.warn("Pas d'id pour", classDesc.name, this.data)
            } else {
              app.store.remove({
                type: classDesc.name,
                key: classDesc.idField,
                value: this.data
              })
            }
          },
          init: classDesc.init
        },
        properties: {}
      }

      // on override les statics définis ci-dessus par les éventuels de classDesc
      desc.statics = _.assign(desc.statics, classDesc.statics)
      // toutes les autres props de classDesc sont copiées sur desc.members
      for (const [prop, value] of Object.entries(classDesc)) {
        // ceux-là sont pas des members
        if (['name', 'init', 'icon', 'statics'].includes(prop)) continue
        desc.members[prop] = value
      }

      // et on init properties
      _.each(properties, (value, prop) => {
        if (!value.hidden) {
          desc.properties[prop] = {
            check: value.type || 'String',
            deferredInit: true,
            nullable: value.nullable || false,
            event: 'change' + firstUpper(prop),
            apply: 'apply' + firstUpper(prop)
          }

          // on ajoute du applyProp aux members pour chaque prop
          // @TODO: Voir pour enlever le "new Function"
          // eslint-disable-next-line no-new-func
          desc.members['apply' + firstUpper(prop)] = value.apply || new Function('value', 'this.data.' + prop + ' = value')
        }
      })
      return desc
    } // build
  }
})
