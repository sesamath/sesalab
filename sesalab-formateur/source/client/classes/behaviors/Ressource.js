const constants = require('sesalab-commun/constants.js')

qx.Class.define('sesalab.behaviors.Ressource',
  sesalab.behaviors.Common.build({
    name: 'Ressource',
    icon: 'icons/20/lab.png',
    init: function (data) {
      // check pour ce qui sort de l'api (sans $id)
      if (!data.$id) {
        data.$id = data.aliasRid || data.rid
      }
      this.setCaption(data.titre)

      // si public pas de suffixe, si partagé dans des groupes suffixe _g (groupe) sinon _p (privé)
      const isPublic = data.public && data.restriction === 0
      // ces icones sont dans sesalab-formateur/source/assets/icons/ressources
      // suffixe _g si restreint et partagé dans un groupe
      // suffixe _p si restreint sans partage
      // suffixe _a si alias
      this.setIcon('icons/ressources/' + data.type + (isPublic ? '' : data.partage ? '_g' : '_p') + '.gif')
    },
    // Common.build considère le reste comme des members (méthodes)
    // On implémente simplement getCaption pour ajouter notre picto alias, ça suffit
    getCaption: function () {
      if (!this.data?.titre) return 'Sans titre'
      if (this.data.aliasRid && this.data.aliasRid !== this.data.rid) {
        // c'est un alias
        return '🔗 ' + this.data.titre
      }
      return this.data.titre
    }
    // implémenter setCaption avec ça (appel de this.base) conduit à une boucle infinie…
    // setCaption: function (value) {
    //     // on appelle le setCaption de la classe parente (sesalab.behaviors.Common qui étend qx.core.Object)
    //   this.data.titre = value
    //   this.base(arguments, getTitre(this.data))
    // }
  }, {
    titre: {
      type: 'String',
      defaults: '',
      apply: function (value) {
        this.data.titre = value
        this.setCaption(value)
      }
    },
    nonZapping: {
      type: 'Number',
      defaults: constants.DEFAULT_NON_ZAPPING,
      widget: {
        label: 'Non zapping',
        help:
          'Pour éviter que les élèves ne «zappent», il existe un délai de 10 secondes pendant lequel l’application ne les autorisera pas à changer d’exercice, une fois celui-ci en marche. Vous pouvez imposer un délai «de non-zapping» supérieur.' +
          'Par ailleurs, vous pouvez imposer un <strong>maximum pour le nombre de visionnage général</strong> d’une ressource (pour limiter le nombre de fois où un exercice est fait par exemple).<br/>' +
          'Si un délai de non-zapping est choisi ici, il se substitue à l’éventuel délai ' +
          'de non-zapping général défini dans les « Paramètres » de la séquence. ' +
          'Il en va de même pour le nombre de visionnage.',
        minimum: 0
      }
    },
    minimumReussite: {
      type: 'Number',
      defaults: constants.DEFAULT_MINIMUM_REUSSITE,
      widget: {
        label: 'Min. réussite (%)',
        help:
          'Dans une séquence ordonnée avec minimum de réussite, vous pouvez décider ' +
          'du pourcentage de bonnes réponses minimal à atteindre pour cet exercice ' +
          '<strong>afin de pouvoir poursuivre la séquence</strong>. ' +
          'Dans le cas où ce minimum n’est pas atteint, l’élève doit recommencer ' +
          'cet exercice.',
        minimum: 0
        // le code ci-dessous marche pas, data.$parent ne contient que $behavior et on a pas d'infos sur la Serie ou la SousSequence
        // enabled: function (data) {
        //   return data.$parent.type === constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE
        // }
      }
    },
    maximumVisionnage: {
      type: 'Number',
      defaults: constants.DEFAULT_MAX_VISIONNAGE,
      widget: {
        label: 'Max. visionnage',
        help: 'Imposer un maximum de visionnage général. 0 indique "aucune limite". Attention, les exercices issus de la première version de Mathenpoche ou de Calculatice contiennent un bouton "Recommencer" à la fin de l’exercice qui n’est pas désactivable par Labomep.',
        minimum: 0
        // avant le 23/06/2020 enabled était une fct qui retournait data.$parent.type === constants.SERIE_STATUT_LIBRE
        // mais data.$parent n'a pas de propriété type, seulement un data.$parent.$behavior qui est celui de la série
      }
    }
  }))
