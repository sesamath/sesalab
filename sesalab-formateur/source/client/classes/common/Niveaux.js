const { niveaux: stNiveaux, ordre: { niveaux: niveauxIdOrdered } } = require('sesatheque-client/src/config')

/**
 * Classe statique (Singleton) contenant les niveaux d'étude
 * (Array de sesalab.common.Niveau, avec props niveaux, _byId et labelOrdered)
 */
qx.Class.define('sesalab.common.Niveaux', {
  extend: qx.data.Array,
  type: 'singleton',

  /** Constructeur. */
  construct: function () {
    this.base(arguments)

    const niveaux = { ...stNiveaux }
    // niveaux est un objet, on lui colle une prop nommée '0' pour un niveau inconnu
    niveaux['0'] = 'Autre'
    this.niveaux = niveaux
    this._byId = {}
    Object.entries(niveaux).forEach(([id, label]) => {
      this._byId[id] = new sesalab.common.Niveau(id, label)
      this.push(this._byId[id])
    })

    this.labelOrdered = []
    niveauxIdOrdered.forEach(id => {
      const label = stNiveaux[id]
      this.labelOrdered.push(label)
    })
  },

  members: {
    /**
     * Récupère un niveau par Id.
     * @param id {Integer} L'id du niveau
     * @return {String} Le libellé du niveau.
     */
    grabById: function (id) {
      return this._byId['' + id]
    },

    findOrder: function (label) {
      return this.labelOrdered.indexOf(label.toString())
    }
  }
})
