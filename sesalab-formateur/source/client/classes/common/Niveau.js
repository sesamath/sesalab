/**
 * Définition d'un niveau de classe
 */
qx.Class.define('sesalab.common.Niveau', {
  extend: qx.core.Object,

  /**
   * Constructeur
   * @param {integer} id identifiant du niveau
   * @param {string} intitule intitulé du niveai
   */
  construct: function (id, intitule) {
    this.base(arguments)
    this.set({
      id,
      intitule
    })
    this.$behavior = new sesalab.behaviors.Niveau(this)
  },
  properties: {
    id: { },
    intitule: { }
  }
})
