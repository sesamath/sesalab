/**
 * Boite de dialogue pour figer l'UI tant que l'utilisateur clique pas sur un bouton "Vu"
 */
qx.Class.define('sesalab.widgets.AlertDialog', {
  extend: sesalab.widgets.Dialog,
  construct: function (message) {
    this.base(arguments, 'Alerte')
    const content = new qx.ui.container.Composite(new qx.ui.layout.VBox(10))
    this.add(content)
    const label = new qx.ui.basic.Label(message)
    label.setRich(true)
    content.add(label)
  },

  statics: {
    execute: function (message, callback, context) {
      const dialog = new sesalab.widgets.AlertDialog(message)
      dialog.addValidateButton('Vu', function () {
        dialog.close()
        callback = callback.bind(context)
        callback()
      })
      dialog.open()
    }
  }
})
