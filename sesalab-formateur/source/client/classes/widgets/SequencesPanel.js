/**
 * Mutualisation de code entre MesSequences et SequencesCollegues
 */
qx.Class.define('sesalab.widgets.SequencesPanel', {
  extend: sesalab.widgets.FolderPanel,
  /**
   * Constructeur
   * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
   */
  construct: function (group, title, options) {
    this.base(arguments, group, title, options)
  },

  members: {
    /**
     * Récupère les séquences via l'api
     * @param {string} url
     * @param {Sequence[]} sequences La liste des séquences déjà récupérées (pagination)
     * @param {sequencesCb} callback
     */
    getAllSequences: function (url, sequences, callback) {
      app.sesalab.get(url, { timeout: 20000 }, (error, response) => {
        if (error) return callback(error)
        sequences = sequences.concat(response.sequences)
        if (!response.nextUrl) return callback(null, sequences)
        this.getAllSequences(response.nextUrl, sequences, callback)
      })
    },

    needSequenceNode: function (folderNode, sequence) {
      // ATTENTION, on lui colle un behavior Sequence mais il lui manque pas mal de chose par rapport à une vraie Sequence (pas les sousSequences par ex)
      sequence.$behavior = new sesalab.behaviors.Sequence(sequence)
      return folderNode.need(sequence, { oid: sequence.oid })
    },

    onOpenSequence: function () {
      const sequence = this.tree.getSelectedObject()
      if (!sequence) return console.error(new Error('Aucune séquence sélectionnée'))
      app.editSequence(sequence.oid)
    },

    onShowResults: function () {
      const selectedObject = this.tree.getSelectedObject()
      if (!selectedObject) return console.error(Error('aucun objet sélectionné'))
      this.showResults(selectedObject)
    },

    onTestSequence: function () {
      const sequence = this.tree.getSelectedObject()
      if (!sequence || !sequence.$behavior) return console.error(new Error('Aucune séquence sélectionnée'))
      // faut la séquence complète
      app.sesalab.get('sequence/' + sequence.oid, {}, (error, response) => {
        if (error) {
          console.error(error)
          app.toaster.toast(error.message, 'error')
          return
        }
        const sequence = sesalab.behaviors.Sequence.create(response.sequence)
        const matchers = []
        const known = new Set()
        sequence.sousSequences.forEach((sousSequence) => {
          sousSequence.eleves.forEach((matcher) => {
            // faudrait comparer type + oid, mais la probabilité pour qu'un groupe et un élève
            // aient le même oid peut être arrondie à 0 sans risque => on ne regarde que l'oid
            if (!known.has(matcher.oid)) {
              matchers.push(matcher)
              known.add(matcher.oid)
            }
          })
        })
        if (matchers.length > 0) {
          sesalab.dialogs.SequenceEleves.execute(sequence, matchers)
        } else {
          app.toaster.toast(`La séquence sélectionnée n'a aucun élève associé,
          impossible de la tester (il faut au moins un élève pour avoir sa vue)`)
        }
      })
    },

    showResults: function (sequence) {
      if (!sequence) return app.toaster.toast('Aucune séquence fournie pour afficher son bilan', 'error')
      app.openPage(new sesalab.pages.Bilan(sequence))
    }
  }
})

/**
 * @typedef sequencesCb
 * @param {Error|null} error
 * @param {Sequence[]} sequences
 */
