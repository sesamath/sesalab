const _ = require('lodash')
const { notify } = require('sesalab-commun/notify')

qx.Class.define('sesalab.widgets.PropertyEditor', {
  extend: qx.ui.container.Composite,

  construct: function (modal, context) {
    this.base(arguments, new qx.ui.layout.VBox())
    this.set({ width: 100, padding: 10 })

    this.modal = modal || false
    if (context) this.modal = this.modal.bind(context)

    this.fields = {}
    this.informationMessages = {}
  },

  events: {
    changed: 'qx.event.type.Data',
    load: 'qx.event.type.Data'
  },

  members: {
    init: function () {
      // Prépare la zone d'affichage des erreurs
      this.errorLabel = new qx.ui.basic.Label()
      this.errorLabel.set({
        rich: true,
        decorator: 'error',
        padding: 3,
        margin: 6
      })

      const errorMessage = new qx.ui.container.Composite()
      errorMessage.setLayout(new qx.ui.layout.VBox(10))
      errorMessage.add(this.errorLabel)
      this.add(errorMessage)
      this.errorLabel.setVisibility('excluded')
    },

    setErrorMessage: function (message) {
      if (!message || !message.length) return
      this.errorLabel.setValue(message)
      this.errorLabel.setVisibility('visible')
    },

    setInformationMessage: function (message) {
      const informationLabel = new qx.ui.basic.Label(message)
      informationLabel.set({
        rich: true,
        decorator: 'tooltip',
        padding: 3,
        margin: 6
      })
      this.informationMessage = new qx.ui.container.Composite()
      this.informationMessage.setLayout(new qx.ui.layout.VBox(10))
      this.informationMessage.add(informationLabel)
      this.add(this.informationMessage)
    },

    addButton: function (label, callback, context) {
      const button = new qx.ui.form.Button(label)
      this.form.addButton(button)
      button.addListener('execute', callback, context)
      return button
    },

    addUserAction: function (label, callback, context) {
      this.userAction = new qx.ui.form.Button(label)
      this.userAction.setMargin(10)
      this.add(this.userAction)
      this.userAction.addListener('execute', callback, context)
    },

    removeUserAction: function () {
      if (this.userAction) {
        this.remove(this.userAction)
        this.userAction = undefined
      }
    },

    error: function (error) {
      if (typeof error === 'string') error = Error(error)
      if (!error.message) {
        notify(Error('Appel de PropertyEditor.error sans message'), { error })
        this.setErrorMessage('Erreur interne')
        return
      }

      // reset du form
      for (const field of Object.values(this.fields)) {
        field.resetInvalidMessage()
        field.resetValid()
      }

      if (error.field) {
        const field = this.fields[error.field]
        if (field) {
          // une erreur sur un champ connu
          field.set({ invalidMessage: error.message, valid: false })
          return
        }
        // à priori c'est pas normal d'avoir un field qui n'est pas dans la liste
        // mais ça arrive par ex si on sauvegarde une Séquence et qu'on est en train d'éditer
        // une de ses sous-séquences, on a pas le champ toDate qui peut poser pb (s'il est avant fromDate)
        console.warn(Error(`champ ${error.field} inconnu (pas dans [${Object.keys(this.fields).join(', ')}]), ça peut être normal à la sauvegarde d’une séquence si on est en train d’éditer une sous-partie qui ne contient pas ce champ`), { error })
      }
      this.setErrorMessage(error.message)
    },

    clear: function () {
      if (this.form) this.form.dispose()
      if (this.renderer) this.renderer.destroy()
      if (this.controller) this.controller.dispose()
      this.form = new qx.ui.form.Form()
      this.removeAll()
    },

    load: function (data, filter = _.identity) {
      // Ménage et création du formulaire
      this.fields = {}
      this.userAction = undefined
      this.clear()

      // Si rien n'est éditable, on part
      if (!data || !data.$behavior.getProperties) return
      const properties = filter(data.$behavior.getProperties())
      if (!properties) return

      if (data.$behavior.information) {
        // parfois la méthode existe mais ne renvoie rien (suivant les data présentes)
        const message = data.$behavior.information()
        if (message) this.setInformationMessage(message)
      }

      this.init()

      _.each(properties, (property, propertyName) => {
        if (!property.widget) return

        // Si l'on n'a pas de type sur le widget, on tente
        // de deviner avec le type de l'objet manipulé
        let widgetType = property.widget.type
        if (!widgetType) {
          switch (property.type) {
            case 'String':
              widgetType = 'TextField'
              break
            case 'Boolean':
              widgetType = 'CheckBox'
              break
            case 'Number':
              widgetType = 'Spinner'
              break
            case 'Date':
              widgetType = 'DateField'
              break
          }
        }

        if (widgetType === 'TimeBox') {
          property.widget.items = {}
          // On incrémente les choix possibles toutes les 15 minutes
          for (let i = 0; i < 24 * 60 / 15; i++) {
            let minutes = i * 15
            let hours = Math.floor(minutes / 60)
            if (hours < 10) hours = '0' + hours
            minutes = Math.floor(minutes % 60)
            if (minutes < 10) minutes = '0' + minutes
            property.widget.items[i] = hours + ':' + minutes
          }
          widgetType = 'SelectBox'
        }

        let binding = 'value'
        let field, lastGroup
        switch (widgetType) {
          case 'TextArea':
            field = new qx.ui.form.TextArea(data.$behavior.get(propertyName))
            field.set(_.assign({ liveUpdate: true }, property.widget.attributes))
            break

          case 'List': {
            const fdata = data.$behavior.get(propertyName)
            const values = property.widget.getValues(fdata)
            field = new qx.ui.form.List()
            _.each(values, (f) => {
              field.add(new qx.ui.basic.Label(f))
            })
            binding = undefined
            break
          }

          case 'TextField':
            field = new qx.ui.form.TextField(data.$behavior.get(propertyName))
            field.set({ liveUpdate: true })
            break

          case 'PasswordField':
            field = new qx.ui.form.PasswordField(data.$behavior.get(propertyName))
            field.set({ liveUpdate: true })
            break

          case 'CheckBox':
            field = new qx.ui.form.CheckBox()
            field.setValue(data.$behavior.get(propertyName))
            break

          case 'Spinner':
            field = new qx.ui.form.Spinner()
            field.setValue(data.$behavior.get(propertyName))
            if (property.widget.minimum) field.setMinimum(property.widget.minimum)
            if (property.widget.maximum) field.setMaximum(property.widget.maximum)
            break

          case 'Slider':
            field = new qx.ui.form.Slider()
            field.setValue(data.$behavior.get(propertyName))
            if (property.widget.minimum) field.setMinimum(property.widget.minimum)
            if (property.widget.maximum) field.setMaximum(property.widget.maximum)
            break

          case 'DateField': {
            field = new qx.ui.form.DateField()
            if (property.widget.format) field.setDateFormat(new qx.util.format.DateFormat(property.widget.format))
            const value = data.$behavior.get(propertyName)
            if (value) field.setValue(new Date(value))
            break
          }

          case 'SelectBox': {
            field = new qx.ui.form.SelectBox()
            const valueParser = (v) => property.type === 'Number'
              ? parseInt(v, 10)
              : v
            const selected = data.$behavior.get(propertyName)
            let items = property.widget.items
            if (typeof items === 'function') {
              items = items()
            }
            for (const i in items) {
              const item = new qx.ui.form.ListItem(items[i], null, i)
              item.index = valueParser(i)
              field.add(item)
              if (item.index === selected) {
                field.setSelection([item])
              }
            }
            binding = undefined

            const fields = this.fields
            field.addListener('changeSelection', function (e) {
              const property = data.$behavior.getProperty(propertyName)
              const selection = field.getSelection()
              const index = selection[0].index
              data.$behavior.set(propertyName, index)
              if (property.widget.onChange) {
                property.widget.onChange(data, fields, propertyName)
              }
              this.fireDataEvent('changed', {
                property: propertyName,
                value: valueParser(e.getData()[0].getModel())
              })
            }, this)
            break
          }
        } // switch widgetType

        if (field) {
          const enabled = !property.widget.enabled || property.widget.enabled(data)
          if (!enabled) {
            field.set({ enabled })
          }
          if (property.widget.help) {
            const tooltip = new qx.ui.tooltip.ToolTip(property.widget.help, 'icons/20/help.png')
            tooltip.setWidth(200)
            tooltip.setRich(true)
            // pour éviter que ça "clignote" quand la souris traverse l'écran, on attend un peu au rollover avant d'afficher le tooltip
            tooltip.setShowTimeout(300)
            // on voudrait que le tooltip reste présent tant qu'on est sur le rollover, mais faut un timeout, on met 60s
            tooltip.setHideTimeout(60000)
            field.setToolTip(tooltip)
          }
          if (binding) field.bind(binding, data.$behavior, propertyName)

          field.addListener('changeValue', function () {
            if (field.getValue) {
              this.fireDataEvent('changed', {
                property: propertyName,
                value: field.getValue()
              })
            }
          }, this)

          field.addListener('changeInvalidMessage', () => {
            if (field.getInvalidMessage) {
              this.setErrorMessage(field.getInvalidMessage())
            }
          })

          if (lastGroup !== property.group) {
            lastGroup = property.group
            if (lastGroup) {
              this.form.addGroupHeader(lastGroup)
            }
          }
          this.form.add(field, property.widget.label, undefined, propertyName)
          field.property = property
          // et on ajoute ce champ à la liste
          this.fields[propertyName] = field
        }
      }) // fin boucle propriétés

      for (const [name, field] of Object.entries(this.fields)) {
        if (field.property.widget.onChange) {
          field.property.widget.onChange(data, this.fields, name)
        }
      }

      if (this.modal) {
        this.cancelButton = this.addButton('Annuler', () => {
          this.modal(false)
        })
        this.validateButton = this.addButton('Valider', () => {
          this.modal(true)
        })
      }
      this.controller = new qx.data.controller.Form(null, this.form)
      this.add(this.renderer = new sesalab.widgets.FormRendererSingle(this.form))

      this.fireDataEvent('load')
    }
  }
})
