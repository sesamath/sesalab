/**
 * Une page dans laquelle on autorise le scroll vertical
 */
qx.Class.define('sesalab.widgets.ScrollablePage', {
  extend: sesalab.widgets.Page,
  type: 'abstract',

  /**
   * Constructeur.
   *
   * @param {string} title Nom de la page
   * @param {string} icon Nom de l'îcone présente à côté du nom de la page
   */
  construct: function (title, icon) {
    this.base(arguments, title, icon)

    // Création d'un scroll-container qui sera notre wrapper sur la page
    const scrollContainer = new qx.ui.container.Scroll()
    // Note that this class can only have one child widget. This container has a fixed layout, which cannot be changed.
    // cf https://archive.qooxdoo.org/5.0.2/api/#qx.ui.container.Scroll

    // on hérite de qx.ui.tabview.Page, pourquoi appeler directement this.add plante ?
    // en tout cas faut passer par son prototype…
    qx.ui.tabview.Page.prototype.add.call(this, scrollContainer, { flex: 1 })

    this.content = new qx.ui.container.Composite(new qx.ui.layout.VBox(10))
    this.content.set({
      allowGrowX: true,
      allowGrowY: true,
      allowShrinkX: true,
      allowShrinkY: true,
    })
    scrollContainer.add(this.content)
  },

  members: {
    add: function (element, options = {}) {
      this.content.add(element, options)
    }
  }
})
