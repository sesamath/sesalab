/**
 * Une page dans laquelle on autorise le scroll verticale
 */
qx.Class.define('sesalab.widgets.ScrollablePage', {
  extend: sesalab.widgets.Page,
  type: 'abstract',

  /**
   * Constructeur.
   *
   * @param {string} title Nom de la page
   * @param {string} icon Nom de l'îcone présente à côté du nom de la page
   */
  construct: function (title, icon) {
    this.base(arguments, title, icon)

    // Création d'un scroll-container qui sera notre wrapper sur la page
    const scrollContainer = new qx.ui.container.Scroll()
    qx.ui.tabview.Page.prototype.add.call(this, scrollContainer, { flex: 1 })

    this.content = new qx.ui.container.Composite(new qx.ui.layout.VBox())
    scrollContainer.add(this.content, { flex: 1 })
  },

  members: {
    add: function (element, options = {}) {
      this.content.add(element, options)
    }
  }
})
