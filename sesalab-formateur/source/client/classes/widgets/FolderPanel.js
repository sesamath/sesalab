const _ = require('lodash')
const flow = require('an-flow')
const uuid = require('an-uuid')

const { notify } = require('sesalab-commun/notify')

const { DEFAULT_FOLDER_NAME } = require('sesalab-commun/constants')

/**
 * Le folder panel est un composant utilisé à chaque fois que l'on a besoin d'une panneau
 * générique contenant un arbre, une barre d'outils et des opérations contextuelles.
 *
 * Il va ainsi gérer :
 *   - la lecture
 *   - le stockage
 *   - le drag & drop
 *   - les opérations de base de type "dossier"
 */
qx.Class.define('sesalab.widgets.FolderPanel', {
  extend: sesalab.widgets.CollapsablePanel,
  include: [sesalab.widgets.Helper],
  type: 'abstract',

  /**
   * Constructeur
   * @param {qx.ui.form.RadioGroup} group Le groupe de panels auquel appartient celui-ci
   *         (cf Application, "Classes" est avec RessourcesPartagees,
   *         Ressources, et PressePapier dans le groupe sharedGroupe)
   * @param {string} title titre du panneau
   * @param {Object} options Pour surcharger le comportement par défaut
   * @param {string} [deleteMessage='Mettre dans la corbeille'] Label de l'entrée delete des items du panel
   *          (si l'entrée est active)
   */
  construct: function (group, title, options) {
    this.options = options
    this.base(arguments, group, title)
  },

  members: {
    /**
     * Initialise le folder panel.
     *
     * @param {Object} folders objet décrivant les dossiers
     * @param {string} payloadField id d'un dossier dans `folders` (sequences|ressources|groupes|sequencesCollegues)
     * @param {Object} options
     * @param {string} [options.sorted=asc] asc|desc
     * @return {boolean} false si l'initialisation a eu déjà eu lieu (dans ce cas ne fait rien)
     */
    initFolderPanel: function (folders, payloadField, options) {
      if (this.tree) {
        return false
      }

      // Sauvegarde des données
      this.folders = folders
      this.payloadField = payloadField
      this.ndx = {}

      // Construction de l'arbre
      this.tree = new sesalab.widgets.Tree()
      this.tree.addListener('dblclick', function () {
        if (!this.tree.getSelectedNode()) return
        this.fireDefaultAction()
      }, this)
      const sorted = options.sorted !== undefined ? options.sorted : 'asc'
      this.tree.set({ sorted, hideRoot: true, showTopLevelOpenCloseIcons: true })

      // Creation du menu contextuel
      this.barButton('onAddFolder', 'Créer un dossier', 'add_folder')
      this.menuButton('onAddSubFolder', 'Créer un dossier', 'add_folder')
      this.menuButton('onModifyFolder', 'Renommer ce dossier', 'signature', true)
      this.refreshButton = this.barButton('buildTree', 'Rafraichir l’arbre', 'refresh')
      this.setRefreshButtonEnabled(false)

      // Ajout et construction de l'arbre
      this.add(this.tree, { flex: 1 })
      this.buildTree()
      this.menuButton('onDeleteFolder', this.options.deleteMessage || 'Mettre dans la corbeille', 'empty_trash')

      return true
    },

    /**
     * Cette méthode sera appelée lors de la construction réelle de l'arbre.
     * @param foldersIndex une table de hash entre le FID (Folder ID) et le noeud contenu
     * @param callback Callback à appeler une fois l'arbre construit
     */
    decorateTree: function (foldersIndex, callback) { throw new Error('Méthode abstraite') },

    setRefreshButtonEnabled: function (enabled) {
      this.refreshButton.setVisibility(enabled ? 'visible' : 'excluded')
      this.refreshButton.setEnabled(enabled)
    },

    /**
     * Désactive l'édition du contenu.
     */
    setContentEnabled: function (enabled) {
      this.tree.setEnabled(enabled)
      this.toolbar.setEnabled(enabled)

      const children = this.toolbar.getChildren()
      for (let i = 0; i < children.length; i++) {
        children[i].setEnabled(enabled)
      }

      this.setRefreshButtonEnabled(!enabled)
    },

    /**
     * Méthode abstraite appelée au moment de la sauvegarde de l'arbre.
     */
    store: function () { throw new Error('Méthode abstraite') },

    /**
     * Construction de l'arbre de la séquence
     */
    buildTree: function () {
      this.setContentEnabled(false)
      this.tree.clean()

      this.ndx = {}
      if (this.folders) {
        const folders = this.folders.folders
        this._buildTree(undefined, folders)
      }

      this.decorateTree(this.ndx, (error) => {
        this.showLoader(false)

        if (error) {
          this.setContentEnabled(false)
          app.errorHandler(error.message)(error)
        } else {
          this.setContentEnabled(true)
        }
      })
      this.tree.refresh()
    },

    /**
     * Construction du sous-arbre de données
     * @see buildTree
     */
    _buildTree: function (root, folders) {
      _.each(folders, (folder) => {
        const folderObject = sesalab.behaviors.Dossier.create(folder)
        const newRoot = this.needFolderNode(root, folderObject)
        _.each(folder[this.payloadField], (fid) => {
          this.ndx[fid] = newRoot
        })
        folder.folders = folder.folders || []
        this._buildTree(newRoot, folder.folders)
      })
    },

    /**
     * Renvoie le node associé au dossier, ou le crée le cas
     * échéant.
     */
    needFolderNode: function (parentFolderNode, folder) {
      parentFolderNode = parentFolderNode || this.tree.model
      return parentFolderNode.need(folder, { uuid: folder.uuid })
    },

    /**
     * Permet la création de l'ensemble des
     * noeud amenant à la constitution d'un chemin.
     * Pensez à `mkdir -p`
     *
     * @param skipStore "true" pour désactiver la sauvegarde automatique
     */
    needPathNode: function (path, skipStore = false) {
      const self = this
      function p (root, path) {
        let found = false
        root.getChildren().some(function (child) {
          if (child.getCaption() === path[0]) {
            found = child
            return found // sort de la boucle si truthy
          }
          return false
        })
        if (!found) {
          const folder = sesalab.behaviors.Dossier.create({
            nom: path[0],
            folders: [],
            uuid: uuid()
          })
          folder[self.payloadField] = []
          const parent = root === self.tree.model ? undefined : root
          found = self.needFolderNode(parent, folder)
          if (self.tree) self.tree.openNode(found)
        }
        path.shift()
        if (path.length) {
          return p(found, path)
        } else {
          return found
        }
      }

      if (!skipStore) {
        this.store()
      }

      return p(this.tree.model, path)
    },

    /**
     * Définition du menu contextuel en fonction d'une sélection
     * d'items.
     */
    onSelectionChanged: function (selection) {
      if (!this.toolbar.isEnabled()) {
        return
      }

      const uniqSelection = selection.length === 1
      const isFolder = uniqSelection && selection[0].instanceOf(sesalab.behaviors.Dossier)
      this.buttonEnabled('onAddFolder', true)
      this.buttonEnabled(['onDeleteFolder', 'onAddSubFolder', 'onModifyFolder'], isFolder)
      this._onSelectionChanged(selection)
    },

    /**
     * Modifie le nom d'un dossier.
     */
    onModifyFolder: function () {
      const folder = this.tree.getSelectedNode()
      if (!folder) {
        console.error(new Error('Impossible de modifier le dossier sélectionné (data null)'))
        return
      }

      sesalab.widgets.InputDialog.execute('Modifier le dossier', 'Nom', folder.data.nom, (value) => {
        folder.data.$behavior.apply({ nom: value })
        this.store()
        this.tree.refresh()
      })
    },

    /**
     * Création d'un nouveau dossier à la racine.
     * @param title String son nom
     */
    createRootFolder: function (title) {
      const folder = sesalab.behaviors.Dossier.create({
        nom: title,
        folders: [],
        uuid: uuid()
      })
      folder[this.payloadField] = []
      const folderNode = this.needFolderNode(undefined, folder)
      if (this.tree) this.tree.openNode(folderNode)
      return folderNode
    },

    /**
     * Renvoie un dossier nommé `name` s'il existe ou le crée à la racine le
     * cas échéant.
     * @param String name le nom du dossier
     */
    needNamedFolder: function (name) {
      let result
      this.tree.model.getChildren().forEach((node) => {
        if (node.getCaption() === name) {
          result = node
        }
      })

      return result || this.createRootFolder(name)
    },

    /**
     * Retourne le dossier par défaut du panel (Non trié)
     * @return {sesalab.behaviors.Dossier}
     */
    getDefaultFolder: function () {
      return this.needNamedFolder(DEFAULT_FOLDER_NAME)
    },

    /**
     * Action UI de création d'un nouveau dossier
     */
    onAddFolder: function () {
      sesalab.widgets.InputDialog.execute('Ajouter un dossier', 'Nom', null, (value) => {
        this.createRootFolder(value)
        this.store()
      })
    },

    /**
     * Action UI de création d'un sous-dossier
     */
    onAddSubFolder: function () {
      sesalab.widgets.InputDialog.execute('Ajouter sous-un dossier', 'Nom', null, (value) => {
        const folder = sesalab.behaviors.Dossier.create({
          nom: value,
          folders: [],
          uuid: uuid()
        })
        folder[this.payloadField] = []
        const folderNode = this.needFolderNode(this.tree.getSelectedNode(), folder)
        if (this.tree) this.tree.openNode(folderNode)
        this.store()
      })
    },

    /**
     * Gestion du drag/over
     */
    onOver: function (e) {
      if (!e.targetClass) return
      if (e.sourceComponent === this.tree) {
        // Cas simple: drop d'un seul élément
        if (e.sources && e.sources.length === 1) {
          // drop d'un élément sur lui-même : interdit
          if (e.sources[0] === e.target) return false

          // drop d'un élément non-dossier dans un dossier : autorisé
          if (e.sourceClass !== sesalab.behaviors.Dossier && e.targetClass === sesalab.behaviors.Dossier) {
            return true
          }

          // drop d'un dossier dans un dossier
          // Il est impossible de dropper un dossier parent sur l'un de ses dossiers enfants sinon l'ensemble sera supprimé
          if (e.sourceClass === sesalab.behaviors.Dossier && e.targetClass === sesalab.behaviors.Dossier) {
            const childFolders = this.getChildFolders([], e.sources[0])
            const child = _.find(childFolders, (folder) => {
              return folder.data.uuid === e.target.data.uuid
            })
            return !child
          }
        }
      }

      if (this.eventToAction) return this.tree.hasAllElementsSameType(e.sources) && this.eventToAction(e.sources[0], e.targetClass)
      return false
    },

    getChildFolders: function (childFolders, folder) {
      _.each(folder.getChildren().toArray(), (child) => {
        if (child.instanceOf(sesalab.behaviors.Dossier)) childFolders.push(child)
        if (child.getChildren().toArray().length > 0) this.getChildFolders(childFolders, child)
      })
      return childFolders
    },

    /**
     * Gestion du drag/drop
     *
     * @param {qx.Event} e Événement Qooxdoo
     * @return {boolean}
     */
    onDrop: function (e) {
      if (this.tree.hasAllElementsSameType(e.sources) && e.sourceComponent === this.tree && e.targetClass === sesalab.behaviors.Dossier) {
        _.each(e.sources, (source) => {
          source.move(e.target)
        })

        this.tree.closeNode(e.target)
        this.tree.openNodeAndParents(e.target)
        this.store()
        this.tree.refresh()

        // on retourne true pour signifier qu'on a traité l'action
        // (le onDrop de la classe fille n'a rien à faire)
        return true
      }

      return false
    },

    /**
     * Renvoie l'ensemble des items stockés dans les dossiers
     * @param root la racine de départ de la récupération
     * @param callback Callback à appeller
     * @return la liste des objets
     */
    getPayloadNodes: function (root, callback) {
      if (!root) return callback(null, [])

      const self = this
      const children = root.getChildren().toArray()

      if (!children || !children.length) {
        return callback(null, [])
      }

      flow(children)
        .seqEach(function (child) {
          if (child.instanceOf(sesalab.behaviors.Dossier)) {
            _.delay(() => {
              self.getPayloadNodes(child, this)
            }, 0)
          } else {
            this(null, child)
          }
        })
        .seq(function (nodes) {
          callback(null, _.flatten(nodes))
        })
        .catch(callback)
    },

    // A surcharger si besoin
    getConfirmDeletionForFolderItems: function () {
      return 'Voulez-vous vraiment supprimer ce dossier et son contenu ?'
    },

    /**
     * Action UI de suppression d'un dossier.
     */
    onDeleteFolder: function () {
      const node = this.tree.getSelectedNode()
      if (!node) {
        return notify(Error('onDeleteFolder appelé sans dossier sélectionné'))
      }
      this.getPayloadNodes(node, (error, items) => {
        if (error) {
          app.toaster.toast(error.message, 'error')
          return
        }
        const msg = this.getConfirmDeletionForFolderItems(items)
        sesalab.widgets.ConfirmationDialog.execute(msg, () => {
          this.deleteNodes(items)
          node.remove()
          app.bus.broadcast('corbeille::refresh', node)
          this.store()
          this.tree.refresh()
        })
      })
    },

    /**
     * Serialisation d'un node de l'arbre
     * @param node
     * @return le node serialisé (avec folders et sequences)
     */
    _serialize: function (node) {
      let result
      if (node.instanceOf(sesalab.behaviors.Dossier)) {
        result = { uuid: node.data.uuid, nom: node.data.nom }
      } else {
        result = {}
      }
      result[this.payloadField] = []
      result.folders = []
      node.getChildren().forEach((child) => {
        if (!child.instanceOf(sesalab.behaviors.Dossier)) {
          // c'est un item, qui peut être une ressource, une séquence, etc.
          // pour les ressources perso, on a pas d'oid, faut prendre $id
          // (ou rid, mais $id est unique alors que plusieurs alias pourraient avoir le même rid)
          const id = child.data.$id || child.data.oid
          if (id) {
            result[this.payloadField].push(id)
          } else {
            const error = Error('Pas trouvé d’id pour ce node, impossible de le sérialiser')
            notify(error, { treeNode: child })
          }
        } else {
          result.folders.push(this._serialize(child))
        }
      })
      return result
    },

    /**
     * Serialisation de tous les dossiers.
     * @return les dossiers serialisés
     */
    serialize: function () {
      return this._serialize(this.tree.model)
    }
  }
})
