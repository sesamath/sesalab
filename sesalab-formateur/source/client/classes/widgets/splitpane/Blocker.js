qx.Class.define('sesalab.widgets.splitpane.Blocker', {
  extend: qx.html.Element,

  construct: function (orientation) {
    const styles = {
      position: 'absolute',
      zIndex: 11
    }

    // IE needs some extra love here to convince it to block events.
    if ((qx.core.Environment.get('engine.name') === 'mshtml')) {
      styles.backgroundImage = 'url(' + qx.util.ResourceManager.getInstance().toUri('qx/static/blank.png') + ')'
      styles.backgroundRepeat = 'repeat'
    }

    this.base(arguments, 'div', styles)

    if (orientation) {
      this.setOrientation(orientation)
    } else {
      this.initOrientation()
    }
  },

  properties: {
    orientation: {
      init: 'horizontal',
      check: ['horizontal', 'vertical'],
      apply: '_applyOrientation'
    }
  },

  members: {
    _applyOrientation: function (value, old) {
      if (value === 'horizontal') {
        this.setStyle('height', '100%')
        this.setStyle('cursor', 'col-resize')
        this.setStyle('top', null)
      } else {
        this.setStyle('width', '100%')
        this.setStyle('left', null)
        this.setStyle('cursor', 'row-resize')
      }
    },

    setWidth: function (offset, spliterSize) {
      const width = spliterSize + 2 * offset
      this.setStyle('width', width + 'px')
    },

    setHeight: function (offset, spliterSize) {
      const height = spliterSize + 2 * offset
      this.setStyle('height', height + 'px')
    },

    setLeft: function (offset, splitterLeft) {
      const left = splitterLeft - offset
      this.setStyle('left', left + 'px')
    },

    setTop: function (offset, splitterTop) {
      const top = splitterTop - offset
      this.setStyle('top', top + 'px')
    }
  }
})
