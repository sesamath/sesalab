Ce composant est une copie de celui de la version 4.0.1 du framework Qooxdoo.

La raison de cette copie est qu'il est compliqué de modifier le comportement de ce type de
composant dans la mesure où :
1. il s'agit d'un composant "en couche" qui comprend une barre et par dessus un blocker
   qui prend concrètent les événements.
2. L'icône est un sous élément de la barre gérée par le mécaniser étrange du
   getChildControl
3. le tout est wrappé dans un psoeudo dual-pane

Le principe de la modification est de simuler le comportement du dragStart/dragEnd au
travers d'un événement 'click' sur la barre :
1. on détecte la direction d'un éventuel drag
2. à la détection du click on calcule la distance parcourue. Si elle est inf. à 10 pixels, on
   envoie un redimensionnement, sinon on annule le drag.

