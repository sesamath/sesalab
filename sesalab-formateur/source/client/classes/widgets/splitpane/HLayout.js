qx.Class.define('sesalab.widgets.splitpane.HLayout', {
  extend: qx.ui.layout.Abstract,
  members: {
    verifyLayoutProperty: qx.core.Environment.select('qx.debug', {
      true: function (item, name, value) {
        this.assert(name === 'type' || name === 'flex', "The property '" + name + "' is not supported by the split layout!")

        if (name === 'flex') {
          this.assertNumber(value)
        }

        if (name === 'type') {
          this.assertString(value)
        }
      },

      false: null
    }),

    renderLayout: function (availWidth, availHeight, padding) {
      const children = this._getLayoutChildren()
      const length = children.length
      let child
      let type
      let begin
      let splitter
      let end
      const paddingLeft = padding.left || 0
      const paddingTop = padding.top || 0

      for (let i = 0; i < length; i++) {
        child = children[i]
        type = child.getLayoutProperties().type

        if (type === 'splitter') {
          splitter = child
        } else if (!begin) {
          begin = child
        } else {
          end = child
        }
      }

      if (begin && end) {
        let beginFlex = begin.getLayoutProperties().flex
        let endFlex = end.getLayoutProperties().flex

        if (beginFlex == null) {
          beginFlex = 1
        }

        if (endFlex == null) {
          endFlex = 1
        }

        const beginHint = begin.getSizeHint()
        const splitterHint = splitter.getSizeHint()
        const endHint = end.getSizeHint()

        let beginWidth = beginHint.width
        const splitterWidth = splitterHint.width
        let endWidth = endHint.width

        if (beginFlex > 0 && endFlex > 0) {
          const flexSum = beginFlex + endFlex
          const flexAvailable = availWidth - splitterWidth

          beginWidth = Math.round((flexAvailable / flexSum) * beginFlex)
          endWidth = flexAvailable - beginWidth

          const sizes = qx.ui.layout.Util.arrangeIdeals(beginHint.minWidth, beginWidth, beginHint.maxWidth,
            endHint.minWidth, endWidth, endHint.maxWidth)

          beginWidth = sizes.begin
          endWidth = sizes.end
        } else if (beginFlex > 0) {
          beginWidth = availWidth - splitterWidth - endWidth
          if (beginWidth < beginHint.minWidth) {
            beginWidth = beginHint.minWidth
          }

          if (beginWidth > beginHint.maxWidth) {
            beginWidth = beginHint.maxWidth
          }
        } else if (endFlex > 0) {
          endWidth = availWidth - beginWidth - splitterWidth
          if (endWidth < endHint.minWidth) {
            endWidth = endHint.minWidth
          }

          if (endWidth > endHint.maxWidth) {
            endWidth = endHint.maxWidth
          }
        }

        begin.renderLayout(paddingLeft, paddingTop, beginWidth, availHeight)
        splitter.renderLayout(beginWidth + paddingLeft, paddingTop, splitterWidth, availHeight)
        end.renderLayout(beginWidth + splitterWidth + paddingLeft, paddingTop, endWidth, availHeight)
      } else {
        // Hide the splitter completely
        splitter.renderLayout(0, 0, 0, 0)

        // Render one child
        if (begin) {
          begin.renderLayout(paddingLeft, paddingTop, availWidth, availHeight)
        } else if (end) {
          end.renderLayout(paddingLeft, paddingTop, availWidth, availHeight)
        }
      }
    },

    _computeSizeHint: function () {
      const children = this._getLayoutChildren()
      const length = children.length
      let child
      let hint
      let props
      let minWidth = 0
      let width = 0
      let maxWidth = 0
      let minHeight = 0
      let height = 0
      let maxHeight = 0

      for (let i = 0; i < length; i++) {
        child = children[i]
        props = child.getLayoutProperties()

        // The slider is not relevant for auto sizing
        if (props.type === 'slider') {
          continue
        }

        hint = child.getSizeHint()

        minWidth += hint.minWidth
        width += hint.width
        maxWidth += hint.maxWidth

        if (hint.minHeight > minHeight) {
          minHeight = hint.minHeight
        }

        if (hint.height > height) {
          height = hint.height
        }

        if (hint.maxHeight > maxHeight) {
          maxHeight = hint.maxHeight
        }
      }

      return {
        minWidth,
        width,
        maxWidth,
        minHeight,
        height,
        maxHeight
      }
    }
  }
})
