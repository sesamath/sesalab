qx.Class.define('sesalab.widgets.splitpane.Slider',
  {
    extend: qx.ui.core.Widget,
    properties: {
      allowShrinkX: {
        refine: true,
        init: false
      },
      allowShrinkY: {
        refine: true,
        init: false
      }
    }
  })
