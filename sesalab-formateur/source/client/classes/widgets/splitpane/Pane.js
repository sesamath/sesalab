qx.Class.define('sesalab.widgets.splitpane.Pane', {
  extend: qx.ui.core.Widget,

  construct: function (orientation, side) {
    this.base(arguments)
    this.__children = []

    if (orientation) {
      this.setOrientation(orientation)
    } else {
      this.initOrientation()
    }

    this.__blocker.addListener('click', () => {
      if (this.__firstPointerX && Math.abs(this.__firstPointerX - this.__lastPointerX) > 2) return
      this.__activeDragSession = false
      this.releaseCapture()
      const size = this.__children[side].getWidth()
      if (this._contracted) {
        this.__children[side].setWidth(this.__storedSize)
        this._contracted = false
      } else {
        this.__storedSize = size
        this.__children[side].setWidth(0)
        this._contracted = true
      }
      this._applyOrientation('horizontal', 'horizontal')
    })
    this.__blocker.addListener('pointerdown', this._onPointerDown, this)
    this.__blocker.addListener('pointerup', this._onPointerUp, this)
    this.__blocker.addListener('pointermove', this._onPointerMove, this)
    this.__blocker.addListener('pointerout', this._onPointerOut, this)
    this.__blocker.addListener('losecapture', this._onPointerUp, this)
  },

  properties: {
    appearance: {
      refine: true,
      init: 'splitpane'
    },

    offset: {
      check: 'Integer',
      init: 6,
      apply: '_applyOffset'
    },

    orientation: {
      init: 'horizontal',
      check: ['horizontal', 'vertical'],
      apply: '_applyOrientation'
    }
  },

  members: {
    __splitterOffset: null,
    __activeDragSession: false,
    __lastPointerX: null,
    __lastPointerY: null,
    __isHorizontal: null,
    __beginSize: null,
    __endSize: null,
    __children: null,
    __blocker: null,

    _createChildControlImpl: function (id, hash) {
      let control

      switch (id) {
        // Create and add slider
        case 'slider':
          control = new sesalab.widgets.splitpane.Slider(this)
          control.exclude()
          this._add(control, { type: id })
          break

        // Create splitter
        case 'splitter':
          control = new sesalab.widgets.splitpane.Splitter(this)
          this._add(control, { type: id })
          control.addListener('move', this.__onSplitterMove, this)
          break
      }

      return control || this.base(arguments, id)
    },

    __onSplitterMove: function (e) {
      this.__setBlockerPosition(e.getData())
    },

    /**
     * Creates a blocker for the splitter which takes all bouse events and
     * also handles the offset and cursor.
     *
     * @param orientation {String} The orientation of the pane.
     */
    __createBlocker: function (orientation) {
      this.__blocker = new sesalab.widgets.splitpane.Blocker(orientation)
      this.getContentElement().add(this.__blocker)

      const splitter = this.getChildControl('splitter')
      const splitterWidth = splitter.getWidth()
      if (!splitterWidth) {
        splitter.addListenerOnce('appear', function () {
          this.__setBlockerPosition()
        }, this)
      }

      // resize listener to remove the blocker in case the splitter
      // is removed.
      splitter.addListener('resize', function (e) {
        const bounds = e.getData()
        if (bounds.height === 0 || bounds.width === 0) {
          this.__blocker.hide()
        } else {
          this.__blocker.show()
        }
      }, this)
    },

    getBlocker: function () {
      return this.__blocker
    },

    _applyOrientation: function (value, old) {
      const slider = this.getChildControl('slider')
      const splitter = this.getChildControl('splitter')

      // Store boolean flag for faster access
      this.__isHorizontal = value === 'horizontal'

      if (!this.__blocker) {
        this.__createBlocker(value)
      }

      // update the blocker
      this.__blocker.setOrientation(value)

      // Dispose old layout
      const oldLayout = this._getLayout()
      if (oldLayout) {
        oldLayout.dispose()
      }

      // Create new layout
      const newLayout = value === 'vertical'
        ? new sesalab.widgets.splitpane.VLayout()
        : new sesalab.widgets.splitpane.HLayout()
      this._setLayout(newLayout)

      // Update states for splitter and slider
      splitter.removeState(old)
      splitter.addState(value)
      splitter.getChildControl('knob').removeState(old)
      splitter.getChildControl('knob').addState(value)
      splitter.getChildControl('toggle').removeState(old)
      splitter.getChildControl('toggle').addState(value)
      slider.removeState(old)
      slider.addState(value)

      // flush (needs to be done for the blocker update) and update the blocker
      qx.ui.core.queue.Manager.flush()
      this.__setBlockerPosition()
    },

    // property apply
    _applyOffset: function (value, old) {
      this.__setBlockerPosition()
    },

    /**
     * Helper for setting the blocker to the right position, which depends on
     * the offset, orientation and the current position of the splitter.
     *
     * @param bounds {Map?null} If the bounds of the splitter are known,
     *   they can be added.
     */
    __setBlockerPosition: function (bounds) {
      const splitter = this.getChildControl('splitter')
      const offset = this.getOffset()
      const splitterBounds = splitter.getBounds()
      const splitterElem = splitter.getContentElement().getDomElement()

      // do nothing if the splitter is not ready
      if (!splitterElem) {
        return
      }

      // recalculate the dimensions of the blocker
      if (this.__isHorizontal) {
        // get the width either of the given bounds or of the read bounds
        let width = null
        if (bounds) {
          width = bounds.width
        } else if (splitterBounds) {
          width = splitterBounds.width
        }
        let left = bounds && bounds.left

        if (width) {
          if (isNaN(left)) {
            left = qx.bom.element.Location.getPosition(splitterElem).left
          }
          this.__blocker.setWidth(offset, width)
          this.__blocker.setLeft(offset, left)
        }

      // vertical case
      } else {
        // get the height either of the given bounds or of the read bounds
        let height = null
        if (bounds) {
          height = bounds.height
        } else if (splitterBounds) {
          height = splitterBounds.height
        }
        let top = bounds && bounds.top

        if (height) {
          if (isNaN(top)) {
            top = qx.bom.element.Location.getPosition(splitterElem).top
          }
          this.__blocker.setHeight(offset, height)
          this.__blocker.setTop(offset, top)
        }
      }
    },

    /*
    ---------------------------------------------------------------------------
      PUBLIC METHODS
    ---------------------------------------------------------------------------
    */

    /**
     * Adds a widget to the pane.
     *
     * Sets the pane's layout to vertical or horizontal split layout. Depending on the
     * pane's layout the first widget will be the left or top widget, the second one
     * the bottom or right widget. Adding more than two widgets will overwrite the
     * existing ones.
     *
     * @param widget {qx.ui.core.Widget} The widget to be inserted into pane.
     * @param flex {Number} The (optional) layout property for the widget's flex value.
     */
    add: function (widget, flex) {
      if (flex == null) {
        this._add(widget)
      } else {
        this._add(widget, { flex })
      }
      this.__children.push(widget)
    },

    /**
     * Removes the given widget from the pane.
     *
     * @param widget {qx.ui.core.Widget} The widget to be removed.
     */
    remove: function (widget) {
      // on a parfois l'erreur `qx.ui.container.Composite[3640-0] is not a child of this widget!` avec le toaster, peut-être à cause d'un rechargement entre temps
      try {
        this._remove(widget)
        qx.lang.Array.remove(this.__children, widget)
      } catch (error) {
        console.error(error)
      }
    },

    /**
     * Returns an array containing the pane's content.
     *
     * @return {qx.ui.core.Widget[]} The pane's child widgets
     */
    getChildren: function () {
      return this.__children
    },

    /*
    ---------------------------------------------------------------------------
      POINTER LISTENERS
    ---------------------------------------------------------------------------
    */

    /**
     * Handler for pointerdown event.
     *
     * Shows slider widget and starts drag session if pointer is near/on splitter widget.
     *
     * @param e {qx.event.type.Pointer} pointerdown event
     */
    _onPointerDown: function (e) {
      // Only proceed if left pointer button is pressed and the splitter is active
      if (!e.isLeftPressed()) {
        return
      }

      this.noClick = false
      this.__firstPointerX = undefined
      const splitter = this.getChildControl('splitter')

      // Store offset between pointer event coordinates and splitter
      const splitterLocation = splitter.getContentLocation()
      const paneLocation = this.getContentLocation()
      this.__splitterOffset = this.__isHorizontal
        ? e.getDocumentLeft() - splitterLocation.left + paneLocation.left
        : e.getDocumentTop() - splitterLocation.top + paneLocation.top

      // Synchronize slider to splitter size and show it
      const slider = this.getChildControl('slider')
      const splitterBounds = splitter.getBounds()
      slider.setUserBounds(
        splitterBounds.left, splitterBounds.top,
        splitterBounds.width, splitterBounds.height
      )

      slider.setZIndex(splitter.getZIndex() + 1)
      slider.show()

      // Enable session
      this.__activeDragSession = true
      this.__blocker.capture()

      e.stop()
    },

    /**
     * Handler for pointermove event.
     *
     * @param e {qx.event.type.Pointer} pointermove event
     */
    _onPointerMove: function (e) {
      this._setLastPointerPosition(e.getDocumentLeft(), e.getDocumentTop())

      // Check if slider is already being dragged
      if (this.__activeDragSession) {
        if (!this.__firstPointerX) {
          this.__firstPointerX = this.__lastPointerX
        }
        // Compute new children sizes
        this.__computeSizes()

        // Update slider position
        const slider = this.getChildControl('slider')
        const pos = this.__beginSize

        if (this.__isHorizontal) {
          slider.setDomLeft(pos)
          this.__blocker.setStyle('left', (pos - this.getOffset()) + 'px')
        } else {
          slider.setDomTop(pos)
          this.__blocker.setStyle('top', (pos - this.getOffset()) + 'px')
        }

        e.stop()
      }
    },

    /**
     * Handler for pointerout event
     *
     * @param e {qx.event.type.Pointer} pointerout event
     */
    _onPointerOut: function (e) {
      this._setLastPointerPosition(e.getDocumentLeft(), e.getDocumentTop())
    },

    /**
     * Handler for pointerup event
     *
     * Sets widget sizes if dragging session has been active.
     *
     * @param e {qx.event.type.Pointer} pointerup event
     */
    _onPointerUp: function (e) {
      if (!this.__activeDragSession) {
        return
      }

      // Set sizes to both widgets
      this._finalizeSizes()

      // Hide the slider
      const slider = this.getChildControl('slider')
      slider.exclude()

      // Cleanup
      this.__activeDragSession = false
      this.releaseCapture()

      e.stop()
    },

    /*
    ---------------------------------------------------------------------------
      INTERVAL HANDLING
    ---------------------------------------------------------------------------
    */

    /**
     * Updates widgets' sizes based on the slider position.
     */
    _finalizeSizes: function () {
      const beginSize = this.__beginSize
      const endSize = this.__endSize

      if (beginSize == null) {
        return
      }

      const children = this._getChildren()
      const firstWidget = children[2]
      const secondWidget = children[3]

      // Read widgets' flex values
      const firstFlexValue = firstWidget.getLayoutProperties().flex
      const secondFlexValue = secondWidget.getLayoutProperties().flex

      // Both widgets have flex values
      // eslint-disable-next-line eqeqeq
      if ((firstFlexValue != 0) && (secondFlexValue != 0)) {
        firstWidget.setLayoutProperties({ flex: beginSize })
        secondWidget.setLayoutProperties({ flex: endSize })
      } else {
      // Update both sizes
        // Set widths to static widgets
        if (this.__isHorizontal) {
          firstWidget.setWidth(beginSize)
          secondWidget.setWidth(endSize)
        } else {
          firstWidget.setHeight(beginSize)
          secondWidget.setHeight(endSize)
        }
      }
    },

    /**
     * Computes widgets' sizes based on the pointer coordinate.
     */
    __computeSizes: function () {
      let min
      let size
      let max
      let pointer
      if (this.__isHorizontal) {
        min = 'minWidth'
        size = 'width'
        max = 'maxWidth'
        pointer = this.__lastPointerX
      } else {
        min = 'minHeight'
        size = 'height'
        max = 'maxHeight'
        pointer = this.__lastPointerY
      }

      const children = this._getChildren()
      const beginHint = children[2].getSizeHint()

      const endHint = children[3].getSizeHint()

      // Area given to both widgets
      const allocatedSize = children[2].getBounds()[size] + children[3].getBounds()[size]

      // Calculate widget sizes
      let beginSize = pointer - this.__splitterOffset
      let endSize = allocatedSize - beginSize

      // Respect minimum limits
      if (beginSize < beginHint[min]) {
        endSize -= beginHint[min] - beginSize
        beginSize = beginHint[min]
      } else if (endSize < endHint[min]) {
        beginSize -= endHint[min] - endSize
        endSize = endHint[min]
      }

      // Respect maximum limits
      if (beginSize > beginHint[max]) {
        endSize += beginSize - beginHint[max]
        beginSize = beginHint[max]
      } else if (endSize > endHint[max]) {
        beginSize += endSize - endHint[max]
        endSize = endHint[max]
      }

      // Store sizes
      this.__beginSize = beginSize
      this.__endSize = endSize
    },

    /**
     * Determines whether this is an active drag session
     *
     * @return {Boolean} True if active drag session, otherwise false.
     */
    _isActiveDragSession: function () {
      return this.__activeDragSession
    },

    /**
     * Sets the last pointer position.
     *
     * @param x {Integer} the x position of the pointer.
     * @param y {Integer} the y position of the pointer.
     */
    _setLastPointerPosition: function (x, y) {
      this.__lastPointerX = x
      this.__lastPointerY = y
    }
  },

  destruct: function () {
    this.__children = null
  }
})
