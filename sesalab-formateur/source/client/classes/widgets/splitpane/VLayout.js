/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2004-2008 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's left-level directory for details.

   Authors:
     * Sebastian Werner (wpbasti)
     * Fabian Jakobs (fjakobs)
     * Jonathan Weiß (jonathan_rass)

************************************************************************ */

/**
 * Layouter for vertical split panes.
 *
 * @internal
 */
qx.Class.define('sesalab.widgets.splitpane.VLayout',
  {
    extend: qx.ui.layout.Abstract,

    /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

    members:
  {
    /*
    ---------------------------------------------------------------------------
      LAYOUT INTERFACE
    ---------------------------------------------------------------------------
    */

    // overridden
    verifyLayoutProperty: qx.core.Environment.select('qx.debug', {
      true: function (item, name, value) {
        this.assert(['type', 'flex'].includes(name), `The property ${name} is not supported by the split layout!`)

        if (name === 'flex') this.assertNumber(value)
        else if (name === 'type') this.assertString(value)
      },

      false: null
    }),

    // overridden
    renderLayout: function (availWidth, availHeight, padding) {
      const children = this._getLayoutChildren()
      const length = children.length
      let child, type
      let begin, splitter, end
      const paddingLeft = padding.left || 0
      const paddingTop = padding.top || 0

      for (let i = 0; i < length; i++) {
        child = children[i]
        type = child.getLayoutProperties().type

        if (type === 'splitter') {
          splitter = child
        } else if (!begin) {
          begin = child
        } else {
          end = child
        }
      }

      if (begin && end) {
        let beginFlex = begin.getLayoutProperties().flex
        let endFlex = end.getLayoutProperties().flex

        if (beginFlex == null) {
          beginFlex = 1
        }

        if (endFlex == null) {
          endFlex = 1
        }

        const beginHint = begin.getSizeHint()
        const splitterHint = splitter.getSizeHint()
        const endHint = end.getSizeHint()

        let beginHeight = beginHint.height
        const splitterHeight = splitterHint.height
        let endHeight = endHint.height

        if (beginFlex > 0 && endFlex > 0) {
          const flexSum = beginFlex + endFlex
          const flexAvailable = availHeight - splitterHeight

          beginHeight = Math.round((flexAvailable / flexSum) * beginFlex)
          endHeight = flexAvailable - beginHeight

          const sizes = qx.ui.layout.Util.arrangeIdeals(beginHint.minHeight, beginHeight, beginHint.maxHeight,
            endHint.minHeight, endHeight, endHint.maxHeight)

          beginHeight = sizes.begin
          endHeight = sizes.end
        } else if (beginFlex > 0) {
          beginHeight = availHeight - splitterHeight - endHeight
          if (beginHeight < beginHint.minHeight) {
            beginHeight = beginHint.minHeight
          }

          if (beginHeight > beginHint.maxHeight) {
            beginHeight = beginHint.maxHeight
          }
        } else if (endFlex > 0) {
          endHeight = availHeight - beginHeight - splitterHeight
          if (endHeight < endHint.minHeight) {
            endHeight = endHint.minHeight
          }

          if (endHeight > endHint.maxHeight) {
            endHeight = endHint.maxHeight
          }
        }

        begin.renderLayout(paddingLeft, paddingTop, availWidth, beginHeight)
        splitter.renderLayout(paddingLeft, beginHeight + paddingTop, availWidth, splitterHeight)
        end.renderLayout(paddingLeft, beginHeight + splitterHeight + paddingTop, availWidth, endHeight)
      } else {
        // Hide the splitter completely
        splitter.renderLayout(0, 0, 0, 0)

        // Render one child
        if (begin) {
          begin.renderLayout(paddingLeft, paddingTop, availWidth, availHeight)
        } else if (end) {
          end.renderLayout(paddingLeft, paddingTop, availWidth, availHeight)
        }
      }
    },

    // overridden
    _computeSizeHint: function () {
      const children = this._getLayoutChildren()
      const length = children.length
      let child
      let hint
      let props
      let minHeight = 0
      let height = 0
      let maxHeight = 0
      let minWidth = 0
      let width = 0
      let maxWidth = 0

      for (let i = 0; i < length; i++) {
        child = children[i]
        props = child.getLayoutProperties()

        // The slider is not relevant for auto sizing
        if (props.type === 'slider') {
          continue
        }

        hint = child.getSizeHint()

        minHeight += hint.minHeight
        height += hint.height
        maxHeight += hint.maxHeight

        if (hint.minWidth > minWidth) {
          minWidth = hint.minWidth
        }

        if (hint.width > width) {
          width = hint.width
        }

        if (hint.maxWidth > maxWidth) {
          maxWidth = hint.maxWidth
        }
      }

      return {
        minHeight,
        height,
        maxHeight,
        minWidth,
        width,
        maxWidth
      }
    }
  }
  })
