qx.Class.define('sesalab.widgets.splitpane.Splitter',
  {
    extend: qx.ui.core.Widget,

    construct: function (parentWidget) {
      this.base(arguments)
      if (parentWidget.getOrientation() === 'vertical') {
        this._setLayout(new qx.ui.layout.HBox(0, 'center'))
        this._getLayout().setAlignY('middle')
      } else {
        this._setLayout(new qx.ui.layout.VBox(0, 'middle'))
        this._getLayout().setAlignX('center')
      }

      this._createChildControl('knob')
      this._createChildControl('toggle')
    },

    /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

    properties: {
    // overrridden
      allowShrinkX: {
        refine: true,
        init: false
      },

      // overrridden
      allowShrinkY: {
        refine: true,
        init: false
      }
    },

    members: {
      _createChildControlImpl: function (id, hash) {
        let control

        switch (id) {
          case 'toggle':
          case 'knob':
            control = new qx.ui.basic.Image()
            this._add(control)
            break
        }

        return control || this.base(arguments, id)
      }
    }
  })
