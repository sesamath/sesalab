/**
 * Dialogue basic que de gestion des formulaires.
 */
qx.Class.define('sesalab.widgets.Dialog', {
  extend: qx.ui.window.Window,

  /**
   * Constructeur
   * @param {string} title Titre du formulaire.
   * @param {object} [options]
   * @param {boolean} [options.isCancelFocused] passer true pour mettre le focus sur cancel par défaut
   */
  construct: function (title, options) {
    this.base(arguments, title)
    this.options = options || {}
    this.validateButton = this.cancelButton = undefined

    // Layout
    this.set({
      layout: new qx.ui.layout.VBox(),
      modal: true,
      alwaysOnTop: true,
      movable: true,
      allowMaximize: false,
      allowMinimize: false,
      resizable: false,
      minWidth: 300
    })

    this.addListener('appear', () => {
      this.center()
      this.focus()
    }, this)

    this.addListener('resize', this.center, this)

    this.addListener('keypress', (e) => {
      if (this.validateButton && e.getKeyIdentifier() === 'Enter') {
        if (this.options.isCancelFocused) {
          this.cancelButton.execute()
        } else {
          this.validateButton.execute()
        }
      }
      if (this.cancelButton && e.getKeyIdentifier() === 'Escape') {
        this.cancelButton.execute()
      }
      // @todo ajouter la capture de tab pour changer le focus
      // (le tab change le focus mais la différence est quasi invisible)
    })
  },

  members: {
    addButton: function (label, callback, context) {
      const button = new qx.ui.form.Button(label)
      if (!this.bar) {
        this.bar = new qx.ui.container.Composite()
        const layout = new qx.ui.layout.HBox(10)
        this.bar.set({ layout })
        layout.set({ alignX: 'right' })
        this.add(this.bar)
      }
      this.bar.add(button)
      button.addListener('execute', callback, context)
      return button
    },

    addValidateButton: function (label, callback, context) {
      this.validateButton = this.addButton(label, callback, context)
      this.validateButton.setFocusable(true)
      return this.validateButton
    },

    addCancelButton: function (label, callback, context) {
      callback = callback || this.close.bind(this)
      this.cancelButton = this.addButton(label, callback, context)
      this.cancelButton.setFocusable(true)
      return this.cancelButton
    }
  }
})
