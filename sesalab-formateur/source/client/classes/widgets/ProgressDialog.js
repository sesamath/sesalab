qx.Class.define('sesalab.widgets.ProgressDialog', {
  extend: qx.ui.window.Window,

  construct: function (title) {
    this.base(arguments, title)

    this.set({
      layout: new qx.ui.layout.VBox(),
      modal: true,
      alwaysOnTop: true,
      movable: true,
      allowMaximize: false,
      allowMinimize: false,
      resizable: false,
      minWidth: 250
    })

    this.addListener('appear', function () {
      this.center()
      this.focus()
    }, this)

    const content = new qx.ui.container.Composite(new qx.ui.layout.VBox(5))
    content.set({ padding: [0, 10, 10, 10] })
    this.add(content)

    this.progressBar = new qx.ui.indicator.ProgressBar()
    app.toaster.errorOnly = true
    content.add(this.progressBar)

    this.label = new qx.ui.basic.Label('...')
    this.label.set({ rich: true })
    content.add(this.label)
  },

  members: {
    tick: function (message) {
      if (message) this.label.setValue(message)
      this.progressBar.setValue(this.progressBar.getValue() + 1)
    },

    init: function (message, max) {
      this.label.setValue(message)
      this.progressBar.setMaximum(max)
      this.progressBar.setValue(0)
    },

    close: function () {
      app.toaster.errorOnly = false
      this.base(arguments)
    }
  },

  statics: {
    execute: function (title) {
      const dialog = new this(title)
      dialog.open()
      return dialog
    }
  }
})
