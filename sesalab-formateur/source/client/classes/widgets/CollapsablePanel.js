/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

/**
 * Constructeur
 * @param group {qx.ui.form.RadioGroup} Le groupe auquel est associé le panel.
 * @param title String le titre du panneau
 * @param options Object Options permettant de surcharger le comportement par défaut
 */
qx.Class.define('sesalab.widgets.CollapsablePanel', {
  extend: qx.ui.core.Widget,
  include: [qx.ui.core.MRemoteChildrenHandling, qx.ui.core.MRemoteLayoutHandling, qx.ui.core.MContentPadding],
  implement: [qx.ui.form.IRadioItem],

  construct: function (group, title, options) {
    this.base(arguments)
    // il faut appeler cette méthode privée pour pouvoir ensuite appeler
    // getChildControl (@todo expliquer pourquoi)
    this._setLayout(new qx.ui.layout.VBox())
    const bar = this.getChildControl('bar')
    bar.setCursor('pointer')

    // Titre du panneau
    this._title = title
    const children = bar.getChildren()
    children[0].set({
      value: title,
      appearance: 'collapsable-panel/bar/label'
    })

    // Icone des flêches
    this.nextIcon = children[1]
    this.nextIcon.setAppearance('collapsable-panel/bar/icon')

    // Icone d'aide
    this.helpIcon = children[2]
    this.helpIcon.setAppearance('collapsable-panel/bar/icon')
    this.helpIcon.set({ visibility: 'excluded' })

    this.initValue()
    this.setLayout(new qx.ui.layout.VBox())
    this.setGroup(group)
  },

  properties: {
    appearance: {
      refine: true,
      init: 'collapsable-panel'
    },

    value: {
      check: 'Boolean',
      init: null,
      apply: '_applyValue',
      event: 'changeValue'
    },

    group: {
      check: 'qx.ui.form.RadioGroup',
      nullable: true,
      apply: '_applyGroup'
    }
  },

  members: {
    // overridden
    _forwardStates: {
      opened: true
    },

    // overridden
    _createChildControlImpl: function (id) {
      let control

      switch (id) {
        case 'bar':
          control = new qx.ui.container.Composite(new qx.ui.layout.Dock())
          control.add(new qx.ui.basic.Label('xxxxxxxxxxxxxxxxx'), { edge: 'west', height: '100%' })
          control.add(new qx.ui.basic.Image('icons/20/next.png'), { edge: 'east' })
          control.add(new qx.ui.basic.Image('icons/20/help.png'), { edge: 'east' })
          control.addListener('click', this.onStateChange, this)
          this._add(control, { flex: 1 })
          break

        case 'container':
          control = new qx.ui.container.Composite()
          this._add(control, { flex: 1 })
          break
      }

      return control || this.base(arguments, id)
    },

    getTitle: function () {
      return this._title ?? ''
    },

    onStateChange: function () {
      this.toggleValue()
    },

    createLoader: function () {
      if (this.loader) {
        return
      }

      this.loader = new qx.ui.basic.Atom(null, 'images/loading22.gif').set({
        center: true,
        show: 'icon'
      })
      this.add(this.loader, { flex: 1 })
    },

    showLoader: function (value) {
      if (this.loader) {
        this.loader.setVisibility(value ? 'visible' : 'excluded')
      }
    },

    /**
     * {@link qx.ui.core.MRemoteChildrenHandling}
     */
    getChildrenContainer: function () {
      return this.getChildControl('container')
    },

    /**
     * {@link qx.ui.core.MRemoteChildrenHandling}
     */
    _getContentPaddingTarget: function () {
      return this.getChildControl('container')
    },

    _applyValue: function (value) {
      this.state = value

      if (value) {
        this.addState('opened')
        this.getChildControl('container').show()
        this.nextIcon.getContentElement().addClass('rotate-90')
        if (this.getLayoutParent() && this.getGroup()) {
          let p = this.getLayoutParent().getBounds().height
          p -= 21 * this.getGroup().getItems().length
          this.setHeight(p)
        }
      } else {
        this.removeState('opened')
        this.getChildControl('container').exclude()
        this.nextIcon.getContentElement().removeClass('rotate-90')
      }
    },

    _applyGroup: function (value, old) {
      if (old) old.remove(this)
      if (value) value.add(this)
    },

    // overridden
    _computeSizeHint: function () {
      const hint = this.base(arguments)
      if (!this.getValue()) {
        const child = this.getChildControl('bar').getSizeHint()
        hint.maxHeight = Math.min(hint.maxWidth, child.height + this.getPaddingTop() + this.getPaddingBottom())
      }
      return hint
    }
  }
})
