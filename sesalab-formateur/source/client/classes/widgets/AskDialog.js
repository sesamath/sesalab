const { hasProp } = require('sesajstools')

/**
 * Boite d'ajout de classes
 */
qx.Class.define('sesalab.widgets.AskDialog', {
  extend: sesalab.widgets.Dialog,
  construct: function (message, title, options) {
    // sesalab.widgets.Dialog ne gère que l'option isCancelFocused
    const dialogOptions = (options && hasProp(options, 'isCancelFocused')) ? { isCancelFocused: options.isCancelFocused } : {}
    this.base(arguments, title, dialogOptions)
    this.set('maxWidth', 800)
    const content = new qx.ui.container.Composite(new qx.ui.layout.VBox(10))
    this.add(content)
    const label = new qx.ui.basic.Label(message)
    label.setRich(true)
    content.add(label)
  },

  statics: {
    /**
     * Crée une boite de dialogue et l'affiche
     * @param caption
     * @param callback
     * @param options
     * @param context
     */
    execute: function (caption, callback, options, context) {
      const title = (options && options.title) || 'Question'
      const dialog = new sesalab.widgets.AskDialog(caption, title, options)
      const yes = (options && options.yes) || 'Oui'
      const no = (options && options.no) || 'Non'
      dialog.addCancelButton(no, function () {
        dialog.close()
        callback = callback.bind(context)
        callback(null, false)
      })
      dialog.addValidateButton(yes, function () {
        dialog.close()
        callback = callback.bind(context)
        callback(null, true)
      })
      dialog.open()
    }
  }
})
