/**
 * Bouton permettant de sélectionner un fichier à uploader, par exemple opur les imports
 */
qx.Class.define('sesalab.widgets.UploadButton', {
  extend: qx.ui.core.Widget,
  members: {
    _createContentElement: function () {
      return new qx.html.Element(
        'input',
        { marginTop: 15 },
        { type: 'file' }
      )
    },

    getFiles: function () {
      return this.getContentElement().getDomElement().files
    }
  }
})
