qx.Class.define('sesalab.widgets.InputDialog', {
  extend: sesalab.widgets.Dialog,
  construct: function (title, caption, defaultValue, { vertical } = {}) {
    this.base(arguments, title)
    this.getContentElement().setAttribute('class', 'input-dialog')
    const layout = vertical ? new qx.ui.layout.VBox(2) : new qx.ui.layout.HBox(5)
    const content = new qx.ui.container.Composite(layout)
    content.set({ padding: [0, 10, 10, 10] })
    this.add(content)
    const label = new qx.ui.basic.Label(caption)
    label.set({ selectable: true })
    content.add(label)
    content.add(this.field = new qx.ui.form.TextField(), { flex: 1 })
    if (defaultValue) this.field.setValue(defaultValue)
    this.addListener('appear', function () { this.field.focus() })
  },

  statics: {
    execute: function (title, caption, defaultValue, callback, context) {
      let options
      if (defaultValue && typeof defaultValue === 'object') {
        options = defaultValue
        defaultValue = options.defaultValue
      }
      const dialog = new this(title, caption, defaultValue, options)
      dialog.addValidateButton('Valider', function () {
        const value = this.field.getValue()
        if (!value || value.length === 0) {
          return dialog.error('Le champ est obligatoire')
        }

        dialog.close()
        callback = callback.bind(context)
        callback(value)
      }, dialog)
      dialog.addCancelButton('Annuler')
      dialog.open()
      dialog.field.focus()

      return dialog
    }
  },
  members: {
    error: function (errorMessage) {
      this.field.set({ invalidMessage: errorMessage, valid: false })
    }
  }
})
