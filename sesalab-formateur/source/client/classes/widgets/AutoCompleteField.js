qx.Class.define('sesalab.widgets.AutoCompleteField', {
  extend: qx.ui.form.TextField,
  construct: function (completeCb) {
    this.base(arguments, '')
    this.addListener('keypress', function () {
      clearTimeout(this.timer)
      this.timer = setTimeout(function () {
        this.fireDataEvent('complete')
      }.bind(this), 500)
    }, this)
  },

  members: {
    resetCompletion: function () {
      if (this.popup) {
        this.list.destroy()
        this.popup.destroy()
        this.popup = null
      }
    },
    addCompletionItem: function (caption, data) {
      if (!this.popup) {
        this.popup = new qx.ui.popup.Popup(new qx.ui.layout.Canvas()).set({
          backgroundColor: '#FFFFFF',
          padding: 0,
          offset: 0,
          offsetBottom: 20
        })
        const bounds = this.getBounds()
        this.list = new qx.ui.form.List().set({
          backgroundColor: 'transparent',
          padding: 0,
          minWidth: bounds.width
        })
        this.list.addListener('changeSelection', function (e) {
          const selection = e.getData()
          if (!selection.length) return
          this.fireDataEvent('completed', selection[0].data)
          this.resetCompletion()
        }, this)
        this.popup.add(this.list)
      }
      const item = new qx.ui.form.ListItem(caption)
      item.data = data
      this.list.add(item)
    },
    showCompletion: function () {
      if (this.popup) {
        this.popup.placeToWidget(this)
        this.popup.show()
      }
    }

  }
})
