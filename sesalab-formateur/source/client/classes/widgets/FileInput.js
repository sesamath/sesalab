// faut définir son propre <input type="file"> car ça n'existe pas dans qooxdoo 5
// et utiliser new qx.html.Element('input', null, { type: 'file' })
// marche pas car ça n'hérite pas de qx.ui.core.LayoutItem
qx.Class.define('sesalab.widgets.FileInput', {
  // https://archive.qooxdoo.org/5.0.2/api/#qx.ui.form.AbstractField
  extend: qx.ui.form.AbstractField,

  // le code de https://github.com/qooxdoo/qooxdoo/blob/branch_5_0_x/framework/source/class/qx/ui/form/TextField.js ne nous est pas d'une grande aide pour en faire un input
  // mais https://github.com/qooxdoo/qooxdoo/blob/branch_5_0_x/framework/source/class/qx/ui/form/TextArea.js
  // implémente _createInputElement
  construct: function (multiple) {
    // base appelle _createInputElement, faut donc affecter ça avant
    this._multiple = Boolean(multiple)
    this.base(arguments, '')
  },

  members: {
    _createInputElement: function () {
      this._input = new qx.html.Input('input', {}, { type: 'file', multiple: this._multiple })
      return this._input
    },
    /**
     * Vide l'input
     */
    flush: function () {
      const inputElt = this._input.getDomElement()
      inputElt.value = ''
    },
    /**
     * Retourne le contenu des fichiers
     * (content est une string "dataUrl", par ex 'data:image/png;base64,xxx')
     * @return {Promise<Array<{name: string, content:string}>>}
     */
    getFilesContents: function () {
      return new Promise((resolve, reject) => {
        const inputElt = this._input?.getDomElement()
        if (!inputElt?.files) {
          return reject(Error('Aucun input de type file'))
        }
        Promise.allSettled(Array.from(inputElt.files).map(file => new Promise((resolve, reject) => {
          // on gère un timeout à 10s
          let timerId = setTimeout(() => {
            // et on invalide next pour qu'il ne soit pas appelé une 2e fois si la lecture du fichier finissait par arriver
            reject(Error(`Pas réussi à récupérer le contenu de ${file.name} après 10s d’attente`))
            timerId = null
          }, 10_000)

          /* global FileReader */
          const reader = new FileReader()
          reader.addEventListener('load', () => {
            if (!timerId) return
            clearTimeout(timerId)
            resolve({ name: file.name, content: reader.result })
          })
          reader.readAsDataURL(file)
        }))).then(result => {
          const resultsOk = result.map(({ reason, value }) => {
            if (reason) {
              console.error(reason)
              return null
            }
            return value
          }).filter(Boolean) // pour virer les éventuels null
          resolve(resultsOk)
        }).catch(reject)
      })
    },
    /**
     * Retourne le nb de fichiers
     * @return {number}
     */
    getFilesNb: function () {
      return this._input?.getDomElement()?.files?.length
    }
  }
})
