/**
 * Boite d'ajout de classes
 */
qx.Class.define('sesalab.widgets.PropertyEditorDialog', {
  extend: sesalab.widgets.Dialog,
  construct: function (title, data, options) {
    this.base(arguments, title)
    options = options || {}

    this.propertyEditor = new sesalab.widgets.PropertyEditor(function (validate) {
      if (!validate) {
        this.close()
      } else {
        this.callback(validate)
      }
    }, this)
    this.propertyEditor.load(data)
    this.cancelButton = this.propertyEditor.cancelButton
    this.validateButton = this.propertyEditor.validateButton

    if (options.scrollable) {
      const scrollContainer = new qx.ui.container.Scroll()
      scrollContainer.set({
        // TODO: idéalement il faudrait initialiser cette hauteur
        // sur la hauteur du propertyEditor que l'on met dans le container,
        // mais this.propertyEditor.getHeight() renvoie null ...
        height: 700,
        // Sur un petit écran on ne veut pas que la fenêtre dépasse donc on
        // limite la hauteur du scroll container
        maxHeight: qx.bom.Viewport.getHeight() - 100
      })
      scrollContainer.add(this.propertyEditor)
      this.add(scrollContainer)
    } else {
      this.add(this.propertyEditor)
    }

    this.addListener('appear', function () {
      // on met le focus sur la première valeur
      const fields = Object.values(this.propertyEditor.fields)
      if (fields.length) fields[0].focus()
    }, this)

    if (options.help) {
      // Lien vers l'aide correspondante
      const helpLayout = new qx.ui.layout.HBox(2)
      helpLayout.set({ alignX: 'right' })
      const helpContainer = new qx.ui.container.Composite(helpLayout)
      const helpLabel = new qx.ui.basic.Label('Aide')
      helpLabel.set({
        cursor: 'pointer',
        decorator: 'link',
        textColor: 'rgb(61, 114, 201)'
      })
      helpLabel.addListener('click', () => {
        const text = title.toLowerCase()
        app.openIframePage('Aide ' + text, 'help', options.help, 'help-' + text)
        this.close()
      })
      helpContainer.add(helpLabel)
      this.add(helpContainer)
    }
  },

  members: {
    execute: function (callback, context) {
      if (context) callback = callback.bind(context)
      this.callback = callback
      this.open()
    },
    setErrorMessage: function (msg) {
      this.propertyEditor.setErrorMessage(msg)
    },
    error: function (error) {
      this.propertyEditor.error(error)
    },

    onValidate: function () {
      if (this.invalid()) return
      this.validate(this.model)
      this.close()
    }
  }
})
