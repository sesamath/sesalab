/**
 * Boite d'ajout de classes
 */
qx.Class.define('sesalab.widgets.ConfirmationDialog', {
  extend: sesalab.widgets.Dialog,
  construct: function (message) {
    this.base(arguments, 'Question')
    this.set('maxWidth', 400)
    const content = new qx.ui.container.Composite(new qx.ui.layout.VBox(10))
    this.add(content)
    const label = new qx.ui.basic.Label(message)
    label.setRich(true)
    content.add(label)
  },

  statics: {
    execute: function (caption, callback, context) {
      const dialog = new sesalab.widgets.ConfirmationDialog(caption)
      const okBtn = dialog.addValidateButton('Oui', function () {
        dialog.close()
        callback.call(context)
      })
      dialog.addCancelButton('Non')
      dialog.open()
      // @todo le rendre plus visible (on voit vraiment pas trop la différence)
      okBtn.focus()
    }
  }
})
