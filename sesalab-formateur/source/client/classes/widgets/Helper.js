/**
 * Helper/Mixin définissant des comportements communs à tous
 * les panneaux
 */
qx.Mixin.define('sesalab.widgets.Helper', {
  members: {
    /**
     * Ajoute un bouton dans la barre d'outils du panneau
     * @param name nom du bouton
     * @param title titre du bouton
     * @param icon chemin de l'icône relatif à icons/20
     * @param identifier Identifiant unique dans le DOM
     */
    barButton: function (name, title, icon, identifier) {
      if (!this.toolbar) {
        this.toolbar = new qx.ui.toolbar.ToolBar()
        this.toolbar.set({
          padding: 0,
          show: 'icon'
        })
        this.add(this.toolbar)
      }
      if (!this.buttons) this.buttons = {}
      const button = this.buttons[name] = new qx.ui.toolbar.Button(title, 'icons/20/' + icon + '.png')
      const listener = this[name]
      if (!listener) throw new Error('Pas de listener ' + name)
      button.addListener('execute', listener, this)
      button.set({
        toolTipText: title,
        enabled: false,
        visibility: 'visible'
      })

      if (identifier) {
        button.getContentElement().setAttribute('id', identifier)
      }

      this.toolbar.add(button)
      return button
    },

    /**
     * Ajout d'une entrée de menu dans le menu contextuel
     * @param {string} name nom de l'entrée de menu (interne), conditionne le listener qui doit avoir le même nom
     * @param {string} title label de l'entrée de menu
     * @param {string} icon chemin de l'icône relatif à icons/20 (png sans extension)
     * @param {boolean} [isDefault=false] définit si c'est l'action par défaut qui sera accesible par double-click
     * @param {string} [identifier] id qui sera mis sur l'élément html
     */
    menuButton: function (name, title, icon, isDefault, identifier) {
      if (typeof this[name] !== 'function') return console.error(Error(`Impossible d’ajouter un bouton ${name} sans méthode avec ce nom.`))
      if (!this.menu) {
        this.menu = new qx.ui.menu.Menu()
      }
      if (!this.buttons) this.buttons = {}

      const button = this.buttons[name] = new qx.ui.menu.Button(title, './icons/20/' + icon + '.png')
      const listener = this[name]
      button.set({ enabled: false, visibility: 'excluded' })
      if (!listener) throw new Error('Pas de listener ' + name)
      button.$$listener = listener
      button.addListener('execute', listener, this)
      button.$$default = !!isDefault

      if (identifier) {
        button.getContentElement().setAttribute('id', identifier)
      }

      this.menu.add(button)

      return button
    },

    /**
     * Active l'action définie par défaut.
     * @see menuButton
     */
    fireDefaultAction: function () {
      for (const b of Object.values(this.buttons)) {
        if (b.$$default) {
          if (b.isEnabled()) {
            b.$$listener.call(this)
            // on s'arrête au premier trouvé, car il peut arriver qu'il y ait plusieurs boutons avec $$default true
            // normalement on devrait en avoir qu'un d'actif, mais au cas où
            // (et pour des questions de perf, inutile de continuer la boucle si on a trouvé)
            return
          }
        }
      }
    },

    /**
     * Active ou désactive des boutons (toolbar ou menu).
     * Si un seul argument est fourni il sera considéré comme value et affecté à tous les boutons
     * @param {string|string[]} [buttonNames] Le ou les noms de boutons
     * @param {boolean} value
     */
    buttonEnabled: function (buttonNames, value) {
      if (arguments.length === 1) {
        value = buttonNames
        // et on l'affectera à tous les boutons
        buttonNames = Object.keys(this.buttons)
      }
      if (typeof value !== 'boolean') value = Boolean(value)
      if (!Array.isArray(buttonNames)) buttonNames = [buttonNames]
      for (const name of buttonNames) {
        if (typeof this.buttons[name] === 'undefined') {
          throw new Error('Pas de bouton ' + name)
        }
        this.buttons[name].set({ enabled: value, visibility: value ? 'visible' : 'excluded' })
      }
    }
  }
})
