/**
 * @typedef RichItem
 * @extends ClientItem
 * @property {sesalab.behaviors.Common} $behavior
 */
/**
 * @typedef RichResource
 * @extends RichItem
 * @property {sesalab.behaviors.Ressource} $behavior
 */

/**
 * Modéle de gestion d'un noeud du composant tree.
 */
qx.Class.define('sesalab.widgets.TreeNode', {
  extend: qx.core.Object,
  include: qx.data.marshal.MEventBubbling,

  /**
   * Constructeur
   * @param {Object} data objet de donnée métier. Les arguments facultatifs seront pris dedans (s'il a une propriété $behavior)
   * @param {string} [caption] titre du noeud
   * @param {boolean} [mayHaveChildren] un booléen pour déterminer (à priori) s'il s'agit
   * @param {string} [icon] le chemin vers l'icône du noeud
   * d'un feuille ou d'un noeud.
   */
  construct: function (data, caption, mayHaveChildren, icon) {
    this.base(arguments)

    if (data?.$behavior) {
      caption = data.$behavior.getCaption()
      icon = data.$behavior.getIcon()
      mayHaveChildren = data.$behavior.mayHaveChildren && data.$behavior.mayHaveChildren()

      data.$behavior.bind('caption', this, 'caption')
      data.$behavior.bind('icon', this, 'icon')
    }

    this.setCaption(caption || 'root')
    this.setIcon(icon || null) // null est accepté mais pas undefined

    this.data = data
    this.setChildren(new qx.data.Array())

    if (mayHaveChildren) {
      // Enfant par défaut, un loader en attendant de charger réellement les enfants
      this.getChildren().push(this.loadingPlaceHolder = qx.data.marshal.Json.createModel({
        caption: 'Chargement…',
        icon: 'loading',
        children: []
      }, true))
    }
  },

  /// ///////////////////////////////////////////////////////////////////
  // Propriétés
  /// ///////////////////////////////////////////////////////////////////
  properties: {
    /** L'icône associée à la feuille.  */
    icon: {
      check: 'String',
      event: 'changeIcon',
      nullable: true
    },

    /** Le texte de la feuille. */
    caption: {
      check: 'String',
      event: 'changeCaption'
    },

    /** Noeuds enfants. */
    children: {
      check: 'qx.data.Array',
      event: 'changeChildren'
    }
  },

  /// ///////////////////////////////////////////////////////////////////
  // Membres
  /// ///////////////////////////////////////////////////////////////////
  members: {
    /**
     * Renvoie l'objet métier associé au noeud.
     */
    getData: function () {
      return this.data
    },

    /**
     * Marque le noeud en erreur.
     */
    failed: function () {
      this.clean()
      this.getChildren().push(this.loadingPlaceHolder = qx.data.marshal.Json.createModel({
        caption: 'Impossible de charger',
        icon: 'icons/20/delete.png',
        children: []
      }, true))
      this.tree.refresh()
    },

    /**
     * Détermine si le noeud héberge un objet métier compatible avec `classe`.
     */
    instanceOf: function (classe) {
      if (!this.data || !this.data.$behavior) return false
      return this.data.$behavior instanceof classe
    },

    /**
     * Ajout d'un noeud enfant.
     * @param {RichItem} data objet de donnée métier. Si des arguments (titre, icon) manquent,
     * ils seront puisés là.
     * @return Le noeud crée
     */
    addChildren: function (data) {
      if (!data.$behavior) {
        throw new Error('addChildren doit être appelé avec un objet possédant un $behavior')
      }
      const node = new sesalab.widgets.TreeNode(data)
      this.clean()
      this.getChildren().push(node)

      node.parentNode = this
      node.tree = this.tree
      if (data.$behavior.mounted) {
        data.$behavior.mounted(node)
      }
      return node
    },

    /**
     * Ajout d'un noeud frère.
     * @param sibling Élèment frère
     * @param data objet de donnée métier.
     * @param offset Chaine ayant pour valeur "before" ou "after"
     * @return Le noeud crée
     */
    addSibling: function (sibling, data, offset) {
      const node = new sesalab.widgets.TreeNode(data)
      node.parentNode = sibling.getParent()
      node.tree = sibling.getParent().tree
      if (data.$behavior.mounted) {
        data.$behavior.mounted(node)
      }

      if (offset === 'before') {
        sibling.getSiblings().insertBefore(sibling, node)
      } else {
        sibling.getSiblings().insertAfter(sibling, node)
      }

      return node
    },

    /**
     * Nettoie le noeud de tout placeholder (ex. gif de chargement).
     */
    clean: function () {
      if (this.loadingPlaceHolder) {
        this.getChildren().pop()
        delete this.loadingPlaceHolder
        this.loadingPlaceHolder = undefined
      }
    },

    /**
     * Suppression d'un noeud enfant.
     */
    removeChild: function (child) {
      this.getChildren().remove(child)
    },

    /**
     * Suppression de tous les noeuds enfant.
     */
    removeAll: function () {
      this.getChildren().removeAll()
    },

    /**
     * Renvoie le noeud parent.
     */
    getParent: function () {
      return this.parentNode
    },

    /**
     * Renvoie toute la fratrie du nœud (lui compris)
     */
    getSiblings: function () {
      if (!this.parentNode) return []
      return this.parentNode.getChildren()
    },

    /**
     * Suppression de ce noeud.
     */
    remove: function () {
      if (!this.parentNode) {
        console.error('Impossible de supprimer le noeud racine d’un arbre')
        return
      }

      this.parentNode.removeChild(this)
    },

    /**
     * Relocalisation du noeud sur une nouvelle racine.
     */
    move: function (newRoot) {
      if (newRoot === this.parentNode) {
        return
      }
      this.remove()
      this.parentNode = newRoot
      newRoot.getChildren().push(this)
    },

    /**
     * Renvoie le nombre d'enfants.
     */
    nbChildren: function () {
      return this.getChildren().getLength()
    },

    /**
     * Opérateur de tri des nœuds.
     * @see sort.
     */
    sort: function (a, b) {
      if (sesalab.behaviors.Eleve.check(a.data)) {
        a = a.data.nom.toLowerCase()
        b = b.data.nom.toLowerCase()
        return a === b ? 0 : (a > b ? 1 : -1)
      } else if (sesalab.behaviors.Sequence.check(a.data)) {
        a = a.data.nom.toLowerCase()
        b = b.data.nom.toLowerCase()
        return a === b ? 0 : (a > b ? 1 : -1)
      } else if (sesalab.behaviors.Groupe.check(a.data)) {
        a = a.data.nom.toLowerCase()
        b = b.data.nom.toLowerCase()
        return a === b ? 0 : (a > b ? 1 : -1)
      } else if (b.data.getId) {
        return b.data.getId() - a.data.getId()
      }
    },

    /**
     * Recherche les noeuds enfants selon les critères (hash field/value) et
     * optionnellement affiche un message s'il est trouvé (pour dire qu'il est déjà dans l'arbre).
     * @param {Object} criterias un critère de recherche (par ex `{rid}`)
     * @param {boolean} [noMessage=false] Passer true pour ne rien dire si on trouve l'élément
     * @return {false|Object} false ou l'élément de l'arbre
     */
    exists: function (criterias, noMessage, recursive = false) {
      const result = this.find(criterias, recursive)
      if (result) {
        if (!noMessage) {
          app.toaster.toast(`L’élément « ${result.getCaption()} » existe déjà dans l’arbre`, 'error', 5)
          if (result.parentNode) this.tree.openNode(result.parentNode)
        }
        return result
      }
      return false
    },

    /**
     * Recherche les noeuds enfants selon les critères (hash field/value), directement dans le dossier.
     * @param {Object} criterias un critère de recherche (par ex `{k:v}` va chercher tous les child.data.k === v, ou child.data.$$user_k === v si data.k n'existe pas). Avec plusieurs critères ils doivent tous être vérifiés
     * @param {boolean} [recursive=false] Passer true pour chercher dans tous les dossiers enfants
     */
    find: function (criterias, recursive = false) {
      // attention, important de "cloner" sinon le pop plus loin modifie l'arbre
      const children = [...this.getChildren().toArray()]
      let valid, value
      while (children.length) {
        const child = children.pop()
        if (!child.data) continue
        valid = true
        for (let key in criterias) {
          value = criterias[key]
          if (typeof child.data[key] === 'undefined') key = '$$user_' + key
          if (child.data[key] !== value) {
            valid = false
            break
          }
        }
        if (valid) return child
        if (recursive) {
          const granChildren = child.getChildren().toArray()
          if (granChildren.length) children.push(...granChildren)
        }
      }
    },

    /**
     * Retourne le nœud racine de l'arbre
     * @returns {sesalab.widgets.TreeNode}
     */
    getRoot: function () {
      let root = this
      while (root.parentNode) {
        root = root.parentNode
      }
      return root
    },

    /**
     * Recherche un noeud enfant et le crée s'il n'existe pas.
     * @param {Object} item à ajouter si besoin
     * @param {Object} criterias le ou les critères de recherche
     * @param {function} [newCallback] Une cb à appeler avec l'item créé si on en a créé un
     * @returns {TreeNode} Le node existant ou créé
     */
    need: function (item, criterias, newCallback) {
      let node = this.find(criterias)
      if (!node) {
        node = this.addChildren(item)
        if (newCallback) newCallback(node)
      }
      return node
    },

    commit: function () {
      this._applyEventPropagation(this.getChildren(), 'Z', 'children')
    }
  }
})
