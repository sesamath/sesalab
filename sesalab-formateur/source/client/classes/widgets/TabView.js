const { addMetadata } = require('sesalab-commun/notify')

qx.Class.define('sesalab.widgets.TabView', {
  extend: qx.ui.tabview.TabView,
  members: {
    /**
     * @info Surcharger cette méthode Qooxdoo nous permet de gérer manuellement
     * la fermeture d'un onglet. C'est utile lors de l'édition d'une séquence par
     * exemple, on souhaite afficher un avertissement si l'utilisateur n'a pas
     * sauvegardé ses changements.
     */
    _onPageClose: function (event) {
      const target = event.getTarget()
      if (!target.hasCustomCloseAction || target.hasCustomCloseAction() === false) {
        qx.ui.tabview.TabView.prototype._onPageClose.call(this, event)
      }
    },
    /**
     * Wrapper de la méthode originale pour mettre la page courante dans le contexte bugsnag
     * @param event
     * @private
     */
    _onChangeSelection: function (event) {
      qx.ui.tabview.TabView.prototype._onChangeSelection.call(this, event)
      // faudrait pas que ce code plante la suite…
      try {
        const target = event.getTarget()
        if (!target) return console.error('pas de target sur cet event', event)
        // target est à priori le tabView, son target.getChildren retourne tous les onglets ouverts
        // target.getSelection() retourne à priori un tableau d'un seul élément,
        // qui est le qx.ui.tabview.TabButton de l'onglet
        // (il a un getLabel mais pas de getId)
        // Pour trouver la page courante y'a apparemment pas d'autres moyens que
        for (const child of this.getChildren()) {
          // à priori c'est une Page, mais elle a pas de méthode pour savoir si elle est sélectionnée,
          // on peut seulement appeler isSelected
          // @see https://archive.qooxdoo.org/5.0.2/api/#qx.ui.tabview.TabView
          if (this.isSelected(child)) {
            let page = child.getId?.() ?? ''
            if (page) page += ' - '
            page += child.getLabel?.() ?? ''
            addMetadata({ page })
            return
          }
        }
      } catch (error) {
        console.error(error)
      }
    }
  }
})
