const _ = require('lodash')

/**
 * Composant 'arbre' utilisé un peu partout
 * dans sesalab (partout en fait ;-)
 */
qx.Class.define('sesalab.widgets.Tree', {
  extend: qx.ui.tree.VirtualTree,
  include: qx.ui.core.MDragDropScrolling,

  construct: function (model) {
    this.base(arguments)

    // Assignation du modèle
    /** @type {sesalab.widgets.TreeNode} */
    this.model = model || new sesalab.widgets.TreeNode({ type: 'root', data: null }, 'Racine', true)

    this.model.tree = this
    this.setOpenMode('tap')
    this.getContentElement().setAttribute('class', 'tree')
    this.getChildControl('pane').getContentElement().setAttribute('class', 'tree-content')

    // Assigne une taille correcte suivant la police utilisée
    let preferedLineHeight = 25
    if (app.utilisateur) {
      if (app.utilisateur.fontSize === 'bigFont') {
        preferedLineHeight = 29
      } else if (app.utilisateur.fontSize === 'smallFont') {
        preferedLineHeight = 23
      }
    } else {
      console.error(Error('app.utilisateur n’a pas encore été affecté'))
    }

    // Réglage des propriétés du composant
    this.set({
      draggable: true,
      droppable: true,
      itemHeight: preferedLineHeight,
      labelPath: 'caption',
      iconPath: 'icon',
      childProperty: 'children',
      selectionMode: 'multi',
      showTopLevelOpenCloseIcons: false,
      model: this.model,
      iconOptions: {
        converter: function (value, model) {
          if (model.getIcon() === 'loading') return 'images/loading22.gif'
          return value
        }
      },
      delegate: {
        configureItem: function (item) {
          item.set({
            droppable: true
          })
        },
        bindItem: this.bindItem.bind(this)
      }
    })

    // Gestion du changement visuel lorsque l'on survole un item
    // --------------------------------------------------------------
    this.addListener('mouseover', function (e) {
      const target = e.getTarget()
      if (target && target.classname === 'qx.ui.tree.VirtualTreeItem') {
        target.setDecorator('hoverDecorator')
      }
    })

    this.addListener('mouseout', function (e) {
      const target = e.getTarget()
      if (target && target.classname === 'qx.ui.tree.VirtualTreeItem') {
        target.setDecorator(null)
      }
    })

    // init à l'affichage, un listener qui s'auto détruit au premier lancement
    const appearListener = function () {
      let parent = this.$$parent
      while (parent.tree !== this) parent = parent.$$parent
      this.parentPane = parent
      this.implementDragAndDrop()
      this.addSelectionChangedListener()
      this.removeListener('appear', appearListener, this)
    }
    this.addListener('appear', appearListener, this)

    this.addListener('dragstart', e => {
      e.addAction('move')
    })

    this.addListener('drag', e => {
      const target = e.getOriginalTarget()
      if (!target) {
        return
      }
      target.set({ droppable: true })

      if (target.classname === 'qx.ui.tree.VirtualTreeItem') {
        this.dragTarget = target.getModel()
        const location = target.getContentLocation()
        const bounds = target.getBounds()
        const limit = location.top + bounds.height / 2
        if (e.getDocumentTop() < limit) {
          e.position = 'before'
          target.setDecorator('dragDecoratorTop')
        } else {
          e.position = 'after'
          target.setDecorator('dragDecoratorBottom')
        }
      }
    })

    // Gestion de la sélection d'items
    // --------------------------------------------------------------
    this.addListener('mousedown', event => {
      const isCtrlPressed = event.isCtrlPressed() || (qx.core.Environment.get('os.name') === 'osx' && event.isMetaPressed())
      const isShiftPressed = event.isShiftPressed()
      if (!isCtrlPressed && !isShiftPressed) {
        const target = event.getTarget()
        if (typeof target.getModel === 'function') {
          // c'est un objet qu'on gère
          const item = target.getModel()
          // il est dans la sélection ?
          const isSelected = this.getSelection().some(elt => elt.toHashCode() === item.toHashCode())
          if (!isSelected) {
            // ça semble toujours le cas, bizarre… qooxdoo n'a pas encore init getSelection
            // quand il appelle ce listener ?
            this.getSelection().removeAll()
            this.getSelection().push(item)
          }
        } else {
          // target sans model, par ex le + devant les dossiers
          this.getSelection().removeAll()
        }
      }
      // sinon on laisse faire la sélection classique qooxdoo
    })
  },

  members: {

    /**
     * Filtre l'ensemble des éléments de l'arbre suivant le filtre donné
     *
     * @param filter La fonction a executer sur chaque élément de l'arbre
     */
    setFiltered: function (filter) {
      if (filter) {
        this.getDelegate().filter = filter
      } else {
        delete this.getDelegate().filter
      }
    },

    /**
     * Trie l'ensemble des éléments de l'arbre alphabétiquement si la propriété sorted est instanciée à true
     */
    setSorted: function (sorter) {
      if (sorter) {
        if (typeof sorter === 'string' && sorter === 'asc') {
          this.getDelegate().sorter = function (a, b) {
            return a.getCaption().toLowerCase().localeCompare(b.getCaption().toLowerCase())
          }
        } else if (typeof sorter === 'function') {
          this.getDelegate().sorter = sorter
        }
      } else {
        delete this.getDelegate().sorter
      }
    },

    /**
     * Récupération des items qui ont été déplacés
     * @returns {sesalab.widgets.TreeNode[]} liste éventuellement vide
     */
    getDragSourceNodes: function (e) {
      const relatedTarget = e.getRelatedTarget()
      return relatedTarget ? relatedTarget.getSelection().toArray() : []
    },

    /**
     * Tableau des items sélectionnés.
     * @returns {sesalab.widgets.TreeNode[]} liste éventuellement vide
     */
    getSelectedNodes: function () {
      // _.compact car il arrive que qooxdoo retourne un tableau contenant des valeurs "undefined"
      return _.compact(this.getSelection().toArray())
    },

    /**
     * Renvoie le premier item sélectionné
     * @returns {sesalab.widgets.TreeNode|null}
     */
    getSelectedNode: function () {
      const items = this.getSelectedNodes()
      if (items.length) return items[0]
      return null
    },

    /**
     * Renvoie l'objet métier stocké dans le premier item sélectionné.
     */
    getSelectedObject: function () {
      const item = this.getSelectedNode()
      if (item) return item.data
      return null
    },

    /**
     * Appelé lors du click droit sur un item
     * pour afficher le menu contextuel.
     */
    onContextMenu: function (e) {
      if (!this.parentPane?.menu) return
      this.parentPane.menu.openAtPointer(e)
    },

    /**
     * Internal Qooxdoo appelé pour binder chaque item d'un arbre.
     */
    bindItem: function (controller, item, index) {
      controller.bindDefaultProperties(item, index)
      if (!item.$$binded) {
        item.$$binded = true
        item.addListener('contextmenu', this.onContextMenu, this)
      }
    },

    /**
     * Vide tous les artefacts des noeuds (ex. symboles de chargement).
     */
    clean: function () {
      this.model.clean.apply(this.model, arguments)
      this.refresh()
    },

    /**
     * Compte le nombre de noeud ayant le type indiqué.
     *
     * @param rootNode Noeud de départ
     * @param type Type de noeud à compter
     * @return {Number}
     */
    countNodeOfType: function (rootNode, type) {
      if (rootNode && rootNode.instanceOf && rootNode.instanceOf(type)) {
        return 1
      }

      let counter = 0
      rootNode.getChildren().forEach(child => {
        counter += this.countNodeOfType(child, type)
      })

      return counter
    },

    /**
     * Suppression de tous les items de l'arbre.
     */
    removeAll: function () {
      this.model.removeAll()
      this.refresh()
    },

    /**
     * Proxy sur la fonction `need` du model associé.
     */
    need: function () {
      return this.model.need.apply(this.model, arguments)
    },

    /**
     * Ajoute un listener change à la sélection courante pour appeler onSelectionChanged
     * (qui propagera vers le panneau parent)
     */
    addSelectionChangedListener: function () {
      function onChange () {
        const selection = this.getSelection().toArray()
        if (selection.length) this.onSelectionChanged(selection)
      }

      // @todo ce truc ajoute bcp de listener (qq milliers dans une session), faudrait les virer au fur et à mesure, ou au moins ne pas mettre plusieurs fois le même sur le même élément
      const selection = this.getSelection()
      // console.log('addSelectionChangedListener ajoute des listener sur', selection)
      // this.getSelection() retourne un http://www.qooxdoo.org/current/api/#qx.data.Array
      // qui hérite de http://www.qooxdoo.org/current/api/#qx.core.Object
      // qui a une méthode addListener
      // bizarrement ça fonctionne même si à l'init selection a une longueur 0
      // (ajouter ici un if (selection.length) supprime les menus au clic droit)
      selection.addListener('change', onChange, this)

      setTimeout(() => this.onSelectionChanged([]))
    },

    /**
     * Proxy vers parentPane.onSelectionChanged s'il existe
     */
    onSelectionChanged: function () {
      if (!this.parentPane) return
      if (typeof this.parentPane.onSelectionChanged !== 'function') return
      this.parentPane.onSelectionChanged.apply(this.parentPane, arguments)
    },

    /**
     * Vérifie si la collection donnée possède des objets tous du même type
     * @param {Object[]} collection Tableau de noeuds
     * @return {boolean}
     */
    hasAllElementsSameType: function (collection) {
      // si y'en a qu'un il est de même type que lui même
      if (collection.length < 2) return true
      let firstNodeType = _.head(collection)
      firstNodeType = firstNodeType.data.$behavior.constructor
      for (const node of collection) {
        if (!node.instanceOf(firstNodeType)) {
          return false
        }
      }

      return true
    },

    /**
     * Verifie si la selection de l'utilisateur est composée uniquement d'un même type d'élément.
     *
     * @param {Object[]} selection Un tableau de noeuds Qooxdoo
     * @param {string} type Type souhaité
     * @return True si la selection est composée du même type de noeud
     */
    isCollectionFullOf: function (selection, type) {
      for (const node of selection) {
        if (!node.instanceOf(type)) {
          return false
        }
      }

      return true
    },

    /**
     * Sélectionne programmatiquement un noeud.
     */
    select: function (node) {
      // console.log('selection forcée', node); // bien pratique en débug
      this.setSelection(new qx.data.Array([node]))
      this.addSelectionChangedListener()
    },

    /**
     * Surcharge du "__onDrag" du mixin "qx.ui.core.MDragDropScrolling" afin de corriger un bug de Qooxdoo.
     *
     * @param e {qx.event.type.Drag} The drag event instance.
     */
    __onDrag: function (e) {
      if (this.__dragScrollTimer) this.__dragScrollTimer.stop()
      const originalTarget = e.getOriginalTarget()
      const target = (originalTarget instanceof qx.ui.core.Widget)
        ? originalTarget
        : qx.ui.core.Widget.getWidgetByElement(originalTarget)
      if (!target) return

      let scrollable = this._isScrollable(target) ? target : this._findScrollableParent(target)

      // on boucle pour remonter au parent avec scrollbar
      while (scrollable) {
        const bounds = this._getBounds(scrollable)
        const xPos = e.getDocumentLeft()
        const yPos = e.getDocumentTop()
        const diff = {
          left: bounds.left - xPos,
          right: bounds.right - xPos,
          top: bounds.top - yPos,
          bottom: bounds.bottom - yPos
        }
        let edgeType = null
        let axis = ''
        let exceedanceAmount = 0

        edgeType = this._getEdgeType(diff, this.getDragScrollThresholdX(), this.getDragScrollThresholdY())
        if (!edgeType) {
          scrollable = this._findScrollableParent(scrollable)
          continue
        }
        axis = this._getAxis(edgeType)

        if (this._isScrollbarVisible(scrollable, axis)) {
          // on a le bon parent
          exceedanceAmount = this._calculateThresholdExceedance(diff[edgeType], this._getThresholdByEdgeType(edgeType))
          if (this.__dragScrollTimer) this.__dragScrollTimer.dispose()
          this.__dragScrollTimer = new qx.event.Timer(50)
          this.__dragScrollTimer.addListener('interval', () => this._scrollBy(scrollable, axis, exceedanceAmount))
          this.__dragScrollTimer.start()
          e.stopPropagation()
          return
        } else {
          scrollable = this._findScrollableParent(scrollable)
        }
      }
    },

    /**
     * Implémente la callback vers le panneau parent pour réagir au drag &
     * drop.
     */
    implementDragAndDrop: function () {
      const cbDrop = this.parentPane.onDrop
      const cbOver = this.parentPane.onOver
      this.addListener('dragstart', e => {
        e.sourceComponent = this
        e.addAction('copy')
      })

      if (cbOver) {
        this.addListener('dragover', e => {
          if (!e.getOriginalTarget()) {
            e.preventDefault()
            return
          }

          let target
          let targetClass
          const sources = _.clone(this.getDragSourceNodes(e))

          // Il est possible de faire un over sur une zone ne comportant pas de modèle
          if (e.getOriginalTarget().getModel) {
            target = e.getOriginalTarget().getModel()
            targetClass = target.data.$behavior.constructor
          }

          const allowOver = cbOver.call(this.parentPane, {
            sources,
            sourceComponent: e.sourceComponent,
            target,
            targetClass,
            targetComponent: this
          })
          if (!allowOver) e.preventDefault()
        })
      }

      if (cbDrop) {
        this.addListener('drop', e => {
          let target
          let targetClass
          let targetIndex
          const sources = _.clone(this.getDragSourceNodes(e))

          // Il est possible de faire un drop sur une zone ne comportant pas de modèle
          if (e.getOriginalTarget().getModel) {
            target = e.getOriginalTarget().getModel()
            targetClass = target.data.$behavior.constructor
            targetIndex = target.getParent() ? target.getParent().getChildren().indexOf(target) : 0
          }

          cbDrop.call(this.parentPane, {
            overState: e.overState,
            position: e.position,
            sources,
            sourceComponent: e.sourceComponent,
            target,
            targetClass,
            targetComponent: this,
            targetIndex
          })
        })
      }
    }
  },

  statics: {
    isFolder: function (node) {
      return node?.instanceOf && node.instanceOf(sesalab.behaviors.Dossier)
    },
    /**
     * Retourne le chemin de l'item sélectionné (le nom de tous les dossiers parents séparés par des /)
     * @param {sesalab.widgets.TreeNode} node
     * @param {Object} [options]
     * @param {boolean} [options.withLeaf=false] Passer true pour avoir aussi le nom de la feuille (même si ce n'est pas un dossier)
     * @returns {string}
     */
    getClearPath: function (node, { withLeaf = false } = {}) {
      if (!(node instanceof sesalab.widgets.TreeNode)) {
        console.error(Error('node n’est pas un TreeNode'), node)
        return ''
      }
      const parents = []
      const add = () => {
        parents.unshift(node.getCaption())
        node = node.getParent()
      }
      // pour le premier on regarde si c'est un dossier, ou si l'on demande le nom de la feuille
      if (sesalab.widgets.Tree.isFolder(node) || withLeaf) {
        add()
      }
      while (node.getParent()) {
        add()
      }
      return parents.join(' / ')
    }
  }
})
