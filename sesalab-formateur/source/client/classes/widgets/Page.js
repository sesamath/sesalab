const _ = require('lodash')

/**
 * La page est un composant utilisé à chaque fois que l'on souhaite créer une page unique identifiée par son id
 */
qx.Class.define('sesalab.widgets.Page', {
  extend: qx.ui.tabview.Page,
  type: 'abstract',

  /**
   * Constructeur
   */
  construct: function (title, icon) {
    this.base(arguments, title, `icons/20/${icon}.png`)
    this.setLabel(_.truncate(title, { length: 20 }))
    this.set({
      layout: new qx.ui.layout.VBox()
    })
  },

  members: {
    /**
     * Retourne l'identifiant unique de la page
     */
    getId: function () {
      throw new Error('Méthode abstraite')
    },

    /**
     * Indique si la page peut être fermée ou non
     * @return {boolean}
     */
    isClosable: function () {
      return true
    },

    /**
     * Indique si la page surchage sa fermeture
     * @return {boolean}
     */
    hasCustomCloseAction: function () {
      return false
    }
  }
})
