qx.Class.define('sesalab.widgets.Toaster', {
  extend: qx.ui.popup.Popup,

  /**
   * Constructeur
   */
  construct: function () {
    this.base(arguments, new qx.ui.layout.VBox())

    /**
     * Un flag pour faire taire le toaster sur autre chose que des erreurs
     * Mis à true dans sesalab.widgets.ProgressDialog
     * @type {boolean}
     */
    this.errorOnly = false

    // Layout
    this.set({
      backgroundColor: '#B2B2B2',
      padding: 0,
      offset: 3,
      width: 250,
      height: 0,
      offsetBottom: 20,
      autoHide: false,
      appearance: 'toaster'
    })
  },

  members: {
    close: function (container) {
      clearTimeout(container.timeout)
      this.remove(container)
      if (this.getChildren().length === 0) {
        this.hide()
      }
    },

    /**
     * Supprime l'ensemble des toasters présents à l'écran.
     *
     * Note : L'objet Popup hérite de Composite qui fourni la méthode "removeAll"
     * @see http://www.qooxdoo.org/current/api/#qx.ui.container.Composite
     */
    clear: function () {
      this.removeAll()
    },

    /**
     * Affiche un toaster et le retourne
     * Si title est une Error, la log en console (et n'affiche que le message en toaster)
     * @param {string|Error} title
     * @param {string} [type=info] info|warning|error, pour décorer le toaster (qui disparaît après 10s si info)
     * @param {number} [delay] Délai d'affichage en secondes (par défaut c'est 10s pour info et pas de limite pour le reste)
     * @return {qx.ui.container.Composite}
     */
    toast: function (title, type = 'info', delay = 0) {
      if (this.errorOnly && type === 'info') return
      if (title instanceof Error) {
        console.error(title)
        title = title.message
        if (type === 'info') type = 'error' // sinon on peut laisser warning
      }
      const container = new qx.ui.container.Composite(new qx.ui.layout.HBox())
      container.set({
        decorator: 'toaster-' + type,
        width: 250
      })
      const message = new qx.ui.basic.Label(title)
      message.set({
        padding: [8, 8],
        value: title,
        rich: true,
        textColor: '#121212',
        textAlign: 'left',
        minWidth: 230
      })
      container.add(message, { flex: 1 })
      const button = new qx.ui.basic.Atom('', 'icons/20/delete.png')
      button.set({
        padding: [8, 8],
        iconPosition: 'right',
        maxWidth: 30,
        cursor: 'pointer'
      })
      container.add(button, { flex: 0 })
      this.add(container)
      const closer = this.close.bind(this, container)
      container.addListener('click', closer)
      if (type === 'info' || delay) {
        delay = delay ? delay * 1000 : 10000
        container.timeout = setTimeout(closer, delay)
      }
      const width = qx.bom.Viewport.getWidth()
      this.moveTo((width - 200) / 2, 30)
      this.show()

      return container
    }
  }
})
