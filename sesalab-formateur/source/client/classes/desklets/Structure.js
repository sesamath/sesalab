const moment = require('moment')
/**
 * Desklet qui correspond au cadre jaune en haut de l'accueil avec le nom de la structure et l'éventuel rappel de date de purge)
 */
qx.Class.define('sesalab.desklets.Structure', {
  extend: qx.ui.container.Composite,
  construct: function () {
    this.base(arguments, new qx.ui.layout.VBox())
    const label = new qx.ui.basic.Atom('xx', 'icons/20/info.png')
    this.set({ decorator: 'tooltip' })
    let labelText = `Vous êtes connecté à la structure ${app.structure.nom}`
    // On affiche la prochaine date de purge si celle-ci est proche
    if (app.structure.datePurge) {
      const datePurge = moment(app.structure.datePurge)
      if (moment() <= datePurge && datePurge <= moment().add(app.settings.joursDisplayDatePurgeStructure, 'days')) {
        labelText += ` (prochaine purge des comptes élèves le ${datePurge.format('DD/MM/YYYY')})`
      }
    }
    label.set({
      label: labelText,
      padding: 5,
      selectable: true
    })
    this.add(label)
  },
  statics: {
    appendTo: function (parent) {
      parent.add(new sesalab.desklets.Structure())
    }
  }
})
