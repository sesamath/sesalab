/**
 * Desklet pour le bloc venant de _private/content/homeInfo.inc.html
 * (placé dans app.settings.content par sesalab/config/index.js)
 * Ce bloc devrait contenir un div#main
 */
qx.Class.define('sesalab.desklets.HomeInfo', {
  // https://archive.qooxdoo.org/5.0.2/api/#qx.ui.container.Composite
  extend: qx.ui.container.Composite,

  construct: function (content) {
    // https://archive.qooxdoo.org/5.0.2/api/#qx.ui.layout.VBox
    this.base(arguments)
    this.setLayout(new qx.ui.layout.VBox(10))

    this.info = new qx.ui.basic.Label(content)
    this.info.set({ rich: true, selectable: true })

    this.add(this.info)
  },
  statics: {
    appendTo: function (parent) {
      const content = window.app.settings.content
      if (!content || !content.homeInfo) return
      const desklet = new sesalab.desklets.HomeInfo(content.homeInfo)
      parent.add(desklet)
    }
  }
})
