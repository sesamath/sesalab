const constants = require('sesalab-commun/constants.js')
const flow = require('an-flow')

/**
 * Desklet qui affiche toutes les infos contextuelles
 * - actualités de l'académie
 * - utilisateurs à valider
 * - 10 dernières séquences modifiées
 * - séquences avec des scores récents (<15j)
 */
qx.Class.define('sesalab.desklets.News', {
  extend: qx.ui.container.Composite,
  /**
   * Ce constructeur devrait être privé, passer par appendTo pour créer une instance (et l'ajouter qqpart)
   */
  construct: function () {
    this.base(arguments)
    this.setLayout(new qx.ui.layout.VBox(10))

    const news = this
    this.pendingUsers = []
    this.actualites = {}

    this.info = new qx.ui.embed.Html('')
    this.info.set({
      // il faut préciser ça pour que le resize fonctionne
      // normalement on aura pas de scroll à l'intérieur de ce widget,
      // mais si le calcul de hauteur échoue on pourra au moins voir ce qui aurait été tronqué
      overflowX: 'auto',
      overflowY: 'auto',
      allowGrowX: true,
      allowGrowY: true,
      allowShrinkX: true,
      allowShrinkY: true,
      selectable: true,
    })
    this.add(this.info)

    window.gererFormateur = function (uid, actionType) {
      if (!uid || !actionType) {
        console.error(new Error('Impossible de valider ce compte pour le moment'))
        return
      }

      app.sesalab.get('utilisateur/validate/' + uid + '/' + actionType, {}, (error, response) => {
        const message = response?.message || error.message
        if (message) {
          app.toaster.toast(message, error ? 'error' : 'info')
        } else {
          app.toaster.toast('aucune réponse', 'error')
        }
        if (!error) {
          // on vire le user qu'on vient de valider
          news.pendingUsers = news.pendingUsers.filter((user) => user.oid !== uid)
          news.refreshView()
        }
      })
    }

    // faut le mettre en global car on a mis du `<a onclick="voirBilan…`
    window.voirBilan = function (sequenceOid, sequenceNom) {
      const bilanPage = new sesalab.pages.Bilan({
        oid: sequenceOid,
        nom: sequenceNom
      })
      app.openPage(bilanPage)
    }
  },
  members: {
    refreshView () {
      let htmlContent = '<div>'

      // Pending actions
      const values = []
      for (const utilisateur of this.pendingUsers) {
        const type = utilisateur.type === constants.TYPE_FORMATEUR ? 'le formateur' : 'l’élève'

        values.push(`
          Vous devez <a onclick="gererFormateur('${utilisateur.oid}', 'accept')">accepter</a>
          ou <a onclick="gererFormateur('${utilisateur.oid}', 'decline')">refuser</a>
          ${type} <b>${utilisateur.prenom} ${utilisateur.nom}</b> sur la structure ${app.structure.type || ''} ${app.structure.nom}`)
      }
      if (values.length > 0) {
        htmlContent += (`
          <div class="home">
            <h2>Utilisateurs en attente de validation</h2>
            <div class="bloc">
              <ul>
                <li>${values.join('</li><li>')}</li>
              </ul>
            </div>
          </div>
        `)
      }

      // News académie
      if (this.actualitesAcademie && this.actualitesAcademie.length) {
        htmlContent += (`
          <div class="home">
            <h2>Informations de l'académie</h2>`)

        for (const i in this.actualitesAcademie) {
          htmlContent += '<div class="bloc">' + this.actualitesAcademie[i].content + '</div>'
        }

        htmlContent += '</div>'
      }

      // News
      const sequencesWithNewResultats = this.actualites?.sequencesWithNewResultats ?? []
      const valuesSequencesWithNewResultats = []
      const valuesChangedSequences = []

      const labelSuffix = (app.utilisateur.structures.length > 1)
        ? ' (sur cette structure)'
        : ''
      if (this.actualites?.changedSequences) {
        for (const sequence of this.actualites.changedSequences) {
          if (sequence.lastChange) {
            valuesChangedSequences.push(`<strong>${sequence.nom}</strong> : ` +
              `modifiée par ${sequence.lastChange.modifier.prenom} ${sequence.lastChange.modifier.nom} ` +
              `le ${('0' + (new Date(sequence.lastChange.date).getDate())).slice(-2)}/${('0' + (new Date(sequence.lastChange.date).getMonth() + 1)).slice(-2)} ` +
              `<a onclick="app.editSequence('${sequence.oid}')"><img src="icons/20/edit.png" style="vertical-align:-3px; cursor:pointer"></a>`)
          }
        }
      }

      if (valuesChangedSequences.length > 0) {
        htmlContent += (`
          <div class="home">
            <h2>10 dernières séquences modifiées${labelSuffix}</h2>
            <div class="bloc">
              <ul>
                <li>${valuesChangedSequences.join('</li><li>')}</li>
              </ul>
            </div>
          </div>
        `)
      }

      for (const sequence of sequencesWithNewResultats) {
        const plural = sequence.nombreResultats > 1 ? 's' : ''
        const pluralNew = sequence.nombreNouveauxResultats > 1 ? 's' : ''
        valuesSequencesWithNewResultats.push(`<strong>${sequence.nom}</strong> : ${sequence.nombreResultats} score${plural} (dont ${sequence.nombreNouveauxResultats} récent${pluralNew}) <a onclick="voirBilan('${sequence.oid}', '${sequence.nom.replace(/'/g, '’')}')"><img src="icons/20/combo_chart.png" style="vertical-align:-3px; cursor:pointer"></a>`)
      }

      if (valuesSequencesWithNewResultats.length > 0) {
        htmlContent += (`
          <div class="home">
            <h2>Séquences avec des scores de moins de 15 jours${labelSuffix}</h2>
            <div class="bloc">
              <ul>
                <li>${valuesSequencesWithNewResultats.join('</li><li>')}</li>
              </ul>
            </div>
          </div>
        `)
      }

      htmlContent += '</div>'
      this.info.setHtml(htmlContent)
      // Pour forcer un recalcul du layout, il faut le faire dans un setTimeout
      // mettre ça dans un `this.info.addListener('changeHtml', () => {`
      // ne fonctionne pas, quand il est appelé l'élément html n'a pas encore le bon scrollHeight
      setTimeout(() => {
        // elt DOM HTML natif, on prend le div parent mis dans htmlContent
        // (donc 1er div enfant du getDomElement de info)
        // car en prenant getDomElement ça fonctionne au 1er chargement mais ça ne réduit plus ensuite
        // (si on valide|rejette un formateur)
        const domElement = this.info.getContentElement().getDomElement()?.querySelector('div')
        if (domElement) {
          this.info.setHeight(domElement.scrollHeight + 4)
        }
        // finalement inutile de tout recalculer
        // cf https://archive.qooxdoo.org/5.0.2/api/#qx.ui.core.queue.Layout~flush
        // qx.ui.core.queue.Layout.flush()

        // Pour informer le parent que la taille a changé, utile quand la taille diminue
        // (à la validation / rejet d'un formateur)
        // const parent = this.getLayoutParent()
        // if (parent) {
        //   parent.invalidateLayoutCache()
        //   parent.getLayoutParent()?.invalidateLayoutCache()
        // }
      }, 100) // avec un delay de 0 le retour de scrollHeight n'est pas encore correct !
    } // refreshView
  },
  statics: {
    appendTo: function (parent) {
      const news = new sesalab.desklets.News()
      // faut l'ajouter en sync (sinon il sera affiché après la suite)
      // et y mettre ensuite le contenu
      parent.add(news)
      flow()
        .seq(function () {
          app.sesalab.get('utilisateur/pending', {}, this)
        })
        .seq(function (response) {
          news.pendingUsers = response.utilisateurs
          app.sesalab.get('utilisateur/actualites', {}, this)
        })
        .seq(function (response) {
          news.actualites = response
          // on débranche ça tant que le pb de perf n'est pas réglé
          //   app.sesalab.get('current-structure-news', {}, this)
          // })
          // .seq(function (response) {
          //   news.actualitesAcademie = response.messages
          news.actualitesAcademie = []
          news.refreshView()
        })
        .catch(app.errorHandler('Une erreur est survenue durant la récupération des actualités'))
    }
  }
})
