/** @type FlowModifier */
const flow = require('an-flow')
const getClient = require('sesatheque-client/src').default

const { formatStringForFilename } = require('sesalab-commun/formatString.js')
const {
  PANEL_MES_RESSOURCES_TITLE,
  PANEL_MES_GROUPES_TITLE,
  PANEL_MES_SEQUENCES_TITLE,
  PANEL_SEQUENCES_COLLEGUES_TITLE,
  PANEL_CLASSES_TITLE,
  PANEL_RESSOURCES_TITLE,
  PANEL_RESSOURCES_PARTAGEES_TITLE
} = require('sesalab-commun/constants')

/**
 * Formate un champ texte de ressource pour affichage en tooltip
 * @param {string} txt
 * @returns {string}
 */
const txtFormatter = (txt) => txt
  .replace(/<br *\/?>/ig, '\n') // au cas où y'aurait déjà des <br> ou <BR />
  .replace(/\n\s*\n/g, '\n\n') // on vire les espaces entre deux \n
  .replace(/\n\n+/g, '\n\n') // pas plus de 2 \n qui se suivent
  .replace(/\n/g, '<br>') // remplacement par le html

/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

qx.Class.define('sesalab.Application', {
  extend: qx.application.Standalone,
  members: {
    main: function () {
      this.base(arguments)

      // Init
      window.app = this
      app.bus = require('../libraries/bus')
      app.icons = require('../libraries/icons')
      app.utilisateur = require('../libraries/utilisateur')
      app.structure = require('../libraries/structure')
      app.sesalab = require('../libraries/sesalab')
      app.store = require('../libraries/store')
      app.ressStore = require('../libraries/store/ressStore')
      app.xhr = require('../libraries/xhr')
      app.constants = require('sesalab-commun/constants.js')
      app.log = require('an-log')('app')

      this.initLanguage()
      this.initLayout()

      app.bus.on('error', function (event, error) {
        app.toaster.toast(error.message, 'error')
        app.log.error(error.stack)
      })

      // on refuse de rester en iframe
      if (window.top.location !== window.location) {
        window.top.location = window.location
        return
      }

      // Redimensionnement initial
      this.__onResize()

      // Chargement de la session en cours
      const isRegister = window.location.pathname === '/formateur/inscription/'
      flow()
        .seq(function () {
          const url = 'settings?formateur=true' + (isRegister ? '' : '&connected=true')
          app.sesalab.get(url, {}, this)
        })
        .seq(function ({ settings, postLogin, message, redirectTo }) {
          if (redirectTo) {
            if (!message) message = 'Une erreur est survenue.'
            message += '\nRedirection dans 2s'
            app.toaster.toast(message, 'error')
            setTimeout(() => { window.location.href = redirectTo }, 2000)
            // on arrête là
            return
          }
          app.settings = settings
          if (postLogin) app.toaster.toast(postLogin.message, postLogin.type)
          document.title = app.settings.name
          // app.labelTitle.setValue(app.settings.name)
          app.log.debug('Settings', app.settings)
          app.labelVersion.set({ value: 'version ' + app.settings.version })
          // idem sesalab-eleve/source/client/services/app/index.js
          if (!app.settings.baseId) {
            const realError = Error('baseId manquante')
            return app.errorHandler('Configuration incomplète, impossible de contacter les bibliothèques de ressource (settings.baseId manquant)')(realError)
          }
          if (!Array.isArray(app.settings.sesatheques) || !app.settings.sesatheques.length) {
            const realError = Error('settings.sesatheques manquant')
            return app.errorHandler('Configuration incomplète, impossible de contacter les bibliothèques de ressource (settings.sesatheques invalide)')(realError)
          }
          if (isRegister) {
            sesalab.dialogs.Register.execute()
            return
          }

          // sinon on continue
          app.sesatheque = getClient(
            app.settings.sesatheques,
            app.settings.baseId
          )
          // on ajoute aussi 2 raccourcis vers les baseId des deux sésathèques
          app.sesatheque.baseIdGlobal = app.settings.sesatheques[0].baseId
          app.sesatheque.baseIdPrivate = app.settings.sesatheques[1].baseId
          // et une méthode pour récupérer les enfants d'un profil
          app.sesatheque.getEnfantsProfil = (profilName, next) => {
            if (!profilName) profilName = 'default'
            const profils = app.settings.profils
            if (!profils[profilName]) {
              console.error(`Le profil ${profilName} n’existe pas`)
              profilName = 'default'
            }
            const rid = profils[profilName].rid
            const isPublic = !(profils[profilName].public === false)
            app.sesatheque.getItem(rid, isPublic, (error, item) => {
              if (error) return next(error)
              app.sesatheque.getEnfants(item, next)
            })
          }

          // on va chercher l'utilisateur
          app.sesalab.get('utilisateur', {}, this)
        })
        .seq(function (response) {
          const next = this
          // Il s'agit d'une session élève : redirection vers l'interface adéquate
          if (response.eleves && response.eleves.length) {
            window.top.location = '/eleve/'
            return
          }

          if (!response.utilisateur) {
            // pas authentifié, on redirige vers le login sur la home
            window.top.location = '/'
            return
          }

          if (response.authBundles) {
            app.sesatheque.addTokens(response.authBundles)
          }

          if (window.bugsnagClient) {
            window.bugsnagClient.addMetadata('user', {
              oid: response.utilisateur.oid,
              login: response.utilisateur.login
            })
          }

          if (response.utilisateur.cguAccepted === true) {
            app.loadWorkspace(response.utilisateur, response.currentStructureOid)
            return
          }

          // faut accepter les CGU
          sesalab.dialogs.CGU.execute(() => {
            response.utilisateur.cguAccepted = true
            app.sesalab.put(`formateur/${response.utilisateur.oid}`, response.utilisateur, {}, (error) => {
              if (error) return next(error)
              app.loadWorkspace(response.utilisateur, response.currentStructureOid)
            })
          })
        })
        .fail(app.errorHandler('Une erreur s’est produite durant le chargement de l’utilisateur'))
    },

    initLanguage: function () {
      const locale = 'fr'
      qx.locale.Manager.getInstance().setLocale(locale)

      // Chargement des dates au format français
      const monthes = {}
      const year = new Date().getFullYear()
      for (let i = 0; i < 12; i++) {
        monthes['cldr_month_stand-alone_wide_' + (i + 1)] = (new Date(year, i)).toLocaleString(locale, { month: 'long' })
      }

      qx.locale.Manager.getInstance().addLocale(locale, Object.assign(monthes, {
        'cldr_day_stand-alone_abbreviated_mon': 'lun',
        'cldr_day_stand-alone_abbreviated_tue': 'mar',
        'cldr_day_stand-alone_abbreviated_wed': 'mer',
        'cldr_day_stand-alone_abbreviated_thu': 'jeu',
        'cldr_day_stand-alone_abbreviated_fri': 'ven',
        'cldr_day_stand-alone_abbreviated_sat': 'sam',
        'cldr_day_stand-alone_abbreviated_sun': 'dim'
      }))
    },

    initLayout: function () {
      const bodyContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox())

      // Définition du toaster (pour les messages utilisateur)
      this.loader = null
      this.toaster = new sesalab.widgets.Toaster()

      // Création du header

      // TITRE
      // mis à jour dans le constructeur avec `app.labelTitle.setValue(app.settings.name)`
      // this.labelTitle = new qx.ui.basic.Label('Sesalab')
      // this.labelTitle.setAppearance('sesalab-header/label')
      // remplacé par
      this.labelTitle = new qx.ui.basic.Image('/images/logoLabomep.png')
      this.labelTitle.addListener('click', () => {
        window.location = '/formateur'
      })

      // version
      this.labelVersion = new qx.ui.basic.Label('')
      this.labelVersion.setAppearance('sesalab-header/version')

      this.headerPanel = new sesalab.panels.Header()
      bodyContainer.add(this.headerPanel)

      // Définition des conteneurs de l'UI
      this.outerContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox())
      bodyContainer.add(this.outerContainer)

      // Définition du blocker (pour les fenêtres modales)
      this.root = this.getRoot()
      this.root.setBlockerOpacity(0.5)
      this.root.setBlockerColor('black')
      this.root.addListener('resize', this.__onResize, this)
      this.root.add(bodyContainer)

      // Initialisation du système d'onglets et création de la page d'accueil
      this.pages = new sesalab.widgets.TabView()
      this.pages.set({
        barPosition: 'top',
        contentPadding: 0
      })

      // on prévient d'une fermeture si on a des pages avec des modifs non sauvegardées
      // Cf https://developer.mozilla.org/fr/docs/Web/API/Window/beforeunload_event
      // => on attache ça au body et pas à window ou document
      document.body.onbeforeunload = (event) => {
        const msg = 'Il reste des travaux non sauvegardés.'
        // ça retourne un array (cf https://archive.qooxdoo.org/5.0.2/api/#qx.ui.tabview.TabView)
        const pages = this.pages.getChildren()
        for (const page of pages) {
          if (typeof page.hasPendingChanges === 'function' && page.hasPendingChanges()) {
            event.returnValue = msg // Gecko, Trident, Chrome 34+
            return msg // Gecko, WebKit, Chrome <34
          }
        }
      }

      // Panneaux de droite
      this.rightPane = new qx.ui.container.Composite(new qx.ui.layout.VBox())
      this.rightPane.set({ width: 250, minWidth: 0, appearance: 'side-panel' })
      app.rightSplitPane = new sesalab.widgets.splitpane.Pane('horizontal', 1)

      // Workspace
      const workspacePane = new qx.ui.container.Composite()
      workspacePane.setLayout(new qx.ui.layout.VBox(10))
      workspacePane.add(this.pages, { flex: 1 })
      workspacePane.setPadding(5)
      app.rightSplitPane.add(workspacePane, 1)
      app.rightSplitPane.add(this.rightPane, 0)

      // Panneaux de gauche
      this.leftPane = new qx.ui.container.Composite(new qx.ui.layout.VBox())
      this.leftPane.set({ width: 250, minWidth: 0, appearance: 'side-panel' })

      app.leftSplitPane = new sesalab.widgets.splitpane.Pane('horizontal', 0)
      app.leftSplitPane.add(this.leftPane, 0)
      app.leftSplitPane.add(app.rightSplitPane, 1)
      app.outerContainer.add(app.leftSplitPane, { flex: 1 })

      const sharedGroupe = new qx.ui.form.RadioGroup()
      sharedGroupe.setAllowEmptySelection(false)
      this.leftPane.add(app.classesPanel = new sesalab.panels.Classes(sharedGroupe), { flex: 1 })
      this.leftPane.add(app.ressourcesPartagees = new sesalab.panels.RessourcesPartagees(sharedGroupe), { flex: 1 })
      this.leftPane.add(app.ressources = new sesalab.panels.Ressources(sharedGroupe), { flex: 1 })
      this.leftPane.add(app.pressePapier = new sesalab.panels.PressePapier(sharedGroupe), { flex: 1 })

      const privateGroup = new qx.ui.form.RadioGroup()
      privateGroup.setAllowEmptySelection(false)
      this.rightPane.add(app.mesGroupes = new sesalab.panels.MesGroupes(privateGroup), { flex: 1 })
      this.rightPane.add(app.mesRessources = new sesalab.panels.MesRessources(privateGroup), { flex: 1 })
      this.rightPane.add(app.mesSequences = new sesalab.panels.MesSequences(privateGroup), { flex: 1 })
      this.rightPane.add(app.sequencesCollegues = new sesalab.panels.SequencesCollegues(privateGroup), { flex: 1 })
    },

    /**
     * Wrap callback pour intercepter une erreur éventuelle.
     * En cas d'erreur affiche message en toaster et log l'erreur,
     * sinon fait suivre le 2e argument à callback (si elle est fournie)
     * @param {string} message
     * @param {string} [level=error] info|warning|error
     * @param {function(null, *)} [callback] appelée seulement en cas de réussite
     * @return {function(Error, *)} Le wrapper de callback
     */
    errorHandler: (message, level, callback) => {
      if (typeof level === 'function') {
        callback = level
        level = 'error'
      } else if (!level) {
        level = 'error'
      }
      return (error, data) => {
        if (error) {
          // on veut la stacktrace en console
          if (typeof error === 'string') {
            error = Error(error)
          } else {
            while (error.cause) error = error.cause
          }
          console.error(error)
          const feedback = error.userFriendly ? error.message : message
          app.toaster.toast(feedback, level)
        } else if (callback) {
          callback(null, data)
        }
      }
    },

    /**
     * Retourne l'url de base du domaine courant (sans slash de fin)
     * @return {string}
     */
    baseUrl: function () {
      const split = document.URL.split('/')
      return split[0] + '//' + split[2]
    },

    editResource: function (resource) {
      this.openIframePage('Ressource', 'lab', resource.$editUrl, 'edit-resource-' + resource.rid)
    },

    editSequence: function (sequenceOid) {
      if (!sequenceOid) {
        console.error(Error('Impossible d’éditer une séquence sans préciser laquelle'))
        return
      }

      app.sesalab.get('sequence/' + sequenceOid, {}, (error, response) => {
        if (error) {
          console.error(error)
          app.toaster.toast(error.message, 'error')
          return
        }
        const sequence = sesalab.behaviors.Sequence.create(response.sequence)
        app.openPage(new sesalab.pages.Sequence(sequence))
      })
    },

    testResource: function (resource) {
      if (!resource?.$displayUrl) return app.toaster.toast('Ressource à tester invalide')
      const url = resource.$displayUrl + '#formateur'
      this.openIframePage('Ressource', 'lab', url, 'test-resource-' + resource.rid)
    },

    loadWorkspace: function (utilisateur, currentStructureOid) {
      flow()
        .seq(function () {
          app.utilisateur.load(
            utilisateur,
            currentStructureOid,
            app.errorHandler('Une erreur est survenue pendant le chargement de vos données utilisateur', this)
          )
        })
        .seq(function () {
          app.utilisateur.loadUserRessources(
            app.errorHandler('Une erreur est survenue pendant le chargement de l’arbre des ressources', this)
          )
        })
        .done((err, selectedRessourceRids) => {
          // ça ne devrait pas arriver, mais parfois…
          if (err) {
            while (err.cause) err = err.cause
            return console.error(err)
          }
          // On enregistre les ressources sélectionnées sur l'application pour afficher l'arbre de ressources
          // mais parfois y'a du falsy dedans
          app.selectedRessourceRids = selectedRessourceRids.filter(rid => rid)
          if (app.selectedRessourceRids.length < selectedRessourceRids) {
          // @todo ajouter ici un notifyError pour poster l'anomalie vers le serveur afin qu'il la log dans un dataError.log
            console.error(new Error('app.utilisateur.loadUserRessources remonte des éléments falsy'), selectedRessourceRids)
          }
          this.headerPanel.createMenu()
          this.userLoaded()
        })
    },

    addHelpIcons: function () {
      this.classesPanel.helpIcon.set({ visibility: 'visible' })
      this.classesPanel.helpIcon.addListener('click', () => {
        app.openIframePage(
          `Aide panneau "${PANEL_CLASSES_TITLE}"`,
          'help',
          app.settings.aide.panneau.classes,
          'help-panneau-classes'
        )
      }, this)

      this.mesGroupes.helpIcon.set({ visibility: 'visible' })
      this.mesGroupes.helpIcon.addListener('click', () => {
        app.openIframePage(
          `Aide panneau "${PANEL_MES_GROUPES_TITLE}"`,
          'help',
          app.settings.aide.panneau.groupes,
          'help-panneau-groupes'
        )
      }, this)

      this.mesRessources.helpIcon.set({ visibility: 'visible' })
      this.mesRessources.helpIcon.addListener('click', () => {
        app.openIframePage(
          `Aide panneau "${PANEL_MES_RESSOURCES_TITLE}"`,
          'help',
          app.settings.aide.panneau.mesRessources,
          'help-panneau-mes-ressources'
        )
      }, this)

      this.ressources.helpIcon.set({ visibility: 'visible' })
      this.ressources.helpIcon.addListener('click', () => {
        app.openIframePage(
          `Aide panneau "${PANEL_RESSOURCES_TITLE}"`,
          'help',
          app.settings.aide.panneau.ressources,
          'help-panneau-ressources'
        )
      }, this)

      this.ressourcesPartagees.helpIcon.set({ visibility: 'visible' })
      this.ressourcesPartagees.helpIcon.addListener('click', () => {
        app.openIframePage(
          `Aide panneau "${PANEL_RESSOURCES_PARTAGEES_TITLE}"`,
          'help',
          app.settings.aide.panneau.ressourcesPartagees,
          'help-panneau-ressources-partagees'
        )
      }, this)

      this.mesSequences.helpIcon.set({ visibility: 'visible' })
      this.mesSequences.helpIcon.addListener('click', () => {
        app.openIframePage(
          `Aide panneau "${PANEL_MES_SEQUENCES_TITLE}"`,
          'help',
          app.settings.aide.panneau.sequences,
          'help-panneau-sequences'
        )
      }, this)

      this.sequencesCollegues.helpIcon.set({ visibility: 'visible' })
      this.sequencesCollegues.helpIcon.addListener('click', () => {
        app.openIframePage(
          `Aide panneau "${PANEL_SEQUENCES_COLLEGUES_TITLE}"`,
          'help',
          app.settings.aide.panneau.sequencesCollegues,
          'help-panneau-sequences-collegues'
        )
      }, this)
    },
    userLoaded: function () {
      // Active / désactive certaines entrées du menu
      this.headerPanel.btnGestion.set({ visibility: app.utilisateur.$isAdminGestion ? 'visible' : 'excluded' })

      let structurePart = app.structure.nom
      if (app.structure.type) structurePart = app.structure.type + ' ' + structurePart
      const caption = app.utilisateur.prenom + ' ' + app.utilisateur.nom + ' - ' + structurePart
      this.headerPanel.utilisateurButton.set({ label: caption, enabled: true })

      // Chargement de la page d'accueil
      this.openPage(new sesalab.pages.Accueil())

      // Il est nécessaire de charger l'ensemble des arbres afin d'éviter des problèmes de références manquantes
      // Ex: Ouverture d'une séquence = impossible de trouver l'arbre contenant les élèves
      this.ressources.init()
      app.classesPanel.init()
      app.mesGroupes.init(app.utilisateur.groupeFolders)
      app.mesSequences.init(app.utilisateur.sequenceFolders)
      app.mesRessources.init(app.utilisateur.ressourcesFolders)
      app.sequencesCollegues.init(app.utilisateur.sequenceColleguesFolders)

      // On force l'ouverture de "Mes sequences"
      app.mesSequences.setValue(true)
      app.ressources.setValue(true)

      // Ajout des icones d'aide sur les panneaux
      this.addHelpIcons()

      if (app.utilisateur.structures.length > 1) {
        const menuAutresStructures = new qx.ui.menu.Menu()
        for (const structure of app.utilisateur.structures) {
          const button = new qx.ui.menu.Button(structure.nom + ' ' + structure.type)
          button.addListener('execute', () => {
            app.sesalab.post('current-structure/' + structure.oid, {}, {}, function () {
              window.location.reload()
            })
          })

          button.setEnabled(structure.oid !== app.utilisateur.structureId)
          menuAutresStructures.add(button)
        }
        this.headerPanel.structureSwitcher.setMenu(menuAutresStructures)
      } else {
        this.headerPanel.structureSwitcher.setVisibility('excluded')
      }
    },

    addPage: function (newPage, pageId, notClosable) {
      if (!notClosable) newPage.set({ showCloseButton: true })
      if (newPage.isClosable) newPage.set({ showCloseButton: newPage.isClosable() })
      newPage.$pageId = pageId

      this.pages.add(newPage, { flex: 1 })
      this.pages.setSelection([newPage])

      return newPage
    },
    openPage: function (page) {
      if (page.getId()) {
        const existingPage = this.findPageByPageId(page.getId())
        if (existingPage) {
          app.pages.setSelection([existingPage])
          return page
        }
      }

      return this.addPage(page, page.getId())
    },
    removePage: function (page) {
      if (!page) return
      this.pages.remove(page)
    },
    findPageByPageId: function (pageId) {
      return this.pages
        .getChildControl('pane').getChildren()
        .find((p) => p.$pageId === pageId)
    },
    createIframe: function (src, additionalStyle) {
      const frame = new qx.ui.embed.Html()
      const style = 'width:99%;height:99%;border:none;' + (additionalStyle || '')
      frame.setHtml(`<iframe style="${style}" class="loader" src="${src}" onload="this.classList.remove('loader');" allowfullscreen="true" ></iframe>`)
      return frame
    },
    openIframePage: function (title, icon, src, pageId) {
      const page = new qx.ui.tabview.Page(title, 'icons/20/' + icon + '.png')
      page.set({
        layout: new qx.ui.layout.VBox(),
        padding: 10
      })
      const frame = this.createIframe(src)
      page.add(frame, { flex: 1 })

      if (pageId) {
        const existingPage = this.findPageByPageId(pageId)
        if (existingPage) {
          app.pages.setSelection([existingPage])
          return page
        }
      }

      return this.addPage(page, pageId)
    },
    sequencePageOpened: function (sequence) {
      const pageId = sesalab.pages.Sequence.getId(sequence)
      return this.findPageByPageId(pageId)
    },

    hideLoader: function () {
      if (this.loader) {
        app.toaster.close(this.loader)
        this.loader = null
      }
    },
    showLoader: function () {
      this.hideLoader()
      this.loader = app.toaster.toast('Enregistrement en cours', 'info')
    },

    checkBeforeDisplayTooltip (event, panel) {
      if (!event.getTarget || !event.getTarget() || !event.getTarget().getModel) {
        event.preventDefault()
        return false
      }

      // On évite d'afficher une tooltip si le menu contextuel est visible
      const target = event.getTarget()
      if (panel.menu.isVisible()) {
        target.setToolTip(null)
        return false
      }

      return true
    },
    createAndDisplayInformationTooltip (text, target) {
      if (!this.tooltip) this.tooltip = new qx.ui.tooltip.ToolTip(text, 'icons/20/info.png')
      this.tooltip.setWidth(300)
      this.tooltip.setRich(true)
      // marche pas avec
      // this.tooltip.set(({ rich: true, selectable: true }))
      // ni
      // this.tooltip.setSelectable(true)
      this.tooltip.setLabel(text)
      this.tooltip.setShowTimeout(250)
      this.tooltip.setHideTimeout(20000)
      // https://archive.qooxdoo.org/5.0.2/api/#qx.ui.core.Widget~setToolTip
      target.setToolTip(this.tooltip)
    },
    displayEleveInformation (event, panel) {
      if (!this.checkBeforeDisplayTooltip(event, panel)) return

      const target = event.getTarget()
      if (!target || !target.getModel()) return // c'était pas un rollover sur un élève
      let eleve = target.getModel().data
      if (!eleve || !eleve.$behavior || eleve.$behavior.constructor !== sesalab.behaviors.Eleve) return

      // On commence par chercher si l'élève est dans le panneau "Classes" pour avoir l'ensemble des informations
      // nécessaires à l'affichage quelque soit l'endroit où le tooltip doit être créé
      const eleveNode = app.classesPanel.grabNodeByOid(sesalab.behaviors.Eleve, eleve.oid)
      if (eleveNode) eleve = eleveNode.data

      let classeNode, classeName
      if (eleve.classe) {
        classeNode = app.classesPanel.grabNodeByOid(sesalab.behaviors.Groupe, eleve.classe)
      }
      if (classeNode) classeName = classeNode.data.nom
      // La classe n'est pas dans le panneau "Classes", elle est donc dans la "Corbeille"
      else classeName = 'Classe supprimée'

      let text = `<p>${classeName} - ${eleve.prenom} ${eleve.nom} (${eleve.login})</p>`
      if (eleve.lastConnexionAsText) text += `<p>${eleve.lastConnexionAsText}</p>`
      else text += '<p>Pas de connexion récente</p>'

      this.createAndDisplayInformationTooltip(text, target)
    },

    /**
     * Affiche les infos de la ressource dans le panneau de droite
     * @param event
     * @param panel
     */
    displayRessourceInformation (event, panel) {
      if (!this.checkBeforeDisplayTooltip(event, panel)) return

      const target = event.getTarget()
      if (!target || !target.getModel()) return
      const ressource = target.getModel().data
      if (!ressource) return

      const texts = []

      if (ressource.$behavior && ressource.$behavior.constructor === sesalab.behaviors.Ressource) {
        // pour mettre l'id en info, mais tant que c'est pas sélectionnable ça sert pas à grand chose
        // const id = ressource.rid || ressource.aliasRid || ressource.aliasOf
        // if (id) texts.push(`<b>Id&nbsp;:</b> ${id}`)
        if (ressource.resume) {
          texts.push('<b>Résumé</b> : ' + ressource.resume)
        } else {
          texts.push('<em>Pas de résumé</em>')
        }

        if (ressource.commentaires) {
          texts.push('<b>Commentaires</b> : ' + ressource.commentaires)
        } else {
          texts.push('<em>Pas de commentaires</em>')
        }
      }

      if (texts.length === 0) return

      const text = '<p>' + texts.map(txtFormatter).join('</p><p>').replace(/\n+/g, '<br>') + '</p>'
      this.createAndDisplayInformationTooltip(text, target)
    },

    exportGroupBilans: function (groupNode) {
      if (!groupNode || !groupNode.data) return app.toaster.toast('Pas de groupe transmis, impossible d’exporter son bilan')
      // force le download de l'export en simulant un click sur un lien de download
      const link = document.createElement('a')
      document.body.appendChild(link)
      link.style = 'display: none'
      link.href = `/api/resultat/export/${groupNode.data.oid}`
      link.download = formatStringForFilename(`bilan_groupe_${groupNode.data.nom}_utf8.csv`)
      link.click()
    },

    showStudentBilan: function (eleve) {
      if (!eleve) return app.toaster.toast('Aucun élève pour afficher son bilan')
      if (!eleve.oid) return app.toaster.toast('Élève sans identifiant, impossible d’afficher son bilan')

      const resultsPage = new sesalab.pages.Bilan(null, {
        oid: eleve.oid,
        nom: eleve.nom,
        prenom: eleve.prenom
      })
      app.openPage(resultsPage)
    },

    populateGroupe: function (node, sortAsc) {
      const groupe = app.store.get({ type: 'Groupe', id: node.data.oid })
      if (!groupe) return
      if (!groupe.utilisateurs) return
      groupe.utilisateurs = groupe.utilisateurs.filter(u => u)

      if (sortAsc) {
        groupe.utilisateurs = groupe.utilisateurs.sort(function (a, b) {
          if (!a.nom || !a.prenom || !b.nom || !b.prenom) return 0

          const fullName = a.nom.toLowerCase() + ' ' + a.prenom.toLowerCase()
          const secondFullName = b.nom.toLowerCase() + ' ' + b.prenom.toLowerCase()
          return fullName.localeCompare(secondFullName)
        })
      }

      for (let u of groupe.utilisateurs) {
        if (typeof u !== 'object') u = app.store.get({ type: 'Eleve', id: u })
        if (!u) continue
        const user = { ...u }
        user.$fake = true
        node.addChildren(sesalab.behaviors.Eleve.create(user))
      }
    },

    updateCaption: function () {
      if (!this.utilisateur || !this.headerPanel || !this.headerPanel.utilisateurButton) {
        return
      }

      const caption = this.utilisateur.prenom + ' ' + this.utilisateur.nom + ' - ' + this.structure.nom
      this.headerPanel.utilisateurButton.set({ label: caption, enabled: true })
    },

    /**
     * Réaction au redimensionnement de la fenêtre
     */
    __onResize: function () {
      const width = qx.bom.Viewport.getWidth()
      const height = qx.bom.Viewport.getHeight()
      this.outerContainer.set({ width, height: height - 52 })
    },

    /**
     * @typedef fetchPersoCb
     * @param {Error|null} error
     * @param {ClientItem[]} ressources Les ressource de l'utilisateur (avec alias)
     * @param {ClientItem[]} sequences Les SequenceModele sauvegardée
     */
    /**
     * Retourne toutes les ressources persos de l'utilisateur, récupérées une seule fois au premier chargement
     * @param {fetchPersoCb} callback
     * @returns {void}
     */
    cachedGetListPerso: function (callback) {
      if (!app._listPersoCallbacks) {
        app._listPersoCallbacks = []
        app._listPersoResults = undefined
      }

      // Note: ce serait plus élégant avec une promise :)
      if (app._listPersoResults) {
        return callback.apply(this, app._listPersoResults)
      }
      app._listPersoCallbacks.push(callback)
      if (this._listPersoCallbacks.length === 1) { // On déclenche l'appel une seule fois
        const ressources = []
        const sequences = []
        app.sesatheque.getListePerso(app.sesatheque.baseIdPrivate, function (error, liste) {
          if (error) {
            if (error.status === 401) return app.toaster.toast('Session expirée sur la Sésathèque.<div style="text-align: center; padding-top: 5px;"><b><u><a href="/sso/sesatheques">Me reconnecter</a></u></b></div>', 'error')
            console.error(error)
            return app.toaster.toast(error.message || error, 'error')
          }
          for (const item of liste) {
            if (item.type === 'sequenceModele') {
              sequences.push(item)
            } else {
              ressources.push(item)
            }
          }
          app._listPersoResults = [error, ressources, sequences]
          for (const cb of app._listPersoCallbacks) {
            cb(error, ressources, sequences)
          }
        })
      }
    }
  },

  destruct: function () {
    this._disposeObjects('leftSplitPane', 'leftContainer', 'rightContainer', 'mainPanel')
  }
})
