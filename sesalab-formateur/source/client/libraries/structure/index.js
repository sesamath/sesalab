const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')

const _structure = {}

function set (data) {
  if (data.oid && _structure.oid && data.oid !== _structure.oid) {
    throw Error(`Données invalides (structure ${data.oid}) pour modifier la structure courante (${_structure.oid})`)
  }
  // on vérifie data
  const cleaned = {}
  for (const prop of [
    // y'en a besoin pour le set initial
    'oid', '$utilisateurs',
    // les strings
    'nom', 'code', 'type', 'ville', 'departement', 'region', 'pays', 'timezone',
    // les booléens
    'ajoutElevesAuthorises', 'ajoutFormateursInterdit', 'accesMultiple', 'bloquerExerciceAutonomie'
  ]) {
    if (prop in data) cleaned[prop] = data[prop]
  }
  // reste les dates
  for (const prop of ['datePurge', 'dernierePurge']) {
    if (prop in data) {
      cleaned[prop] = typeof data[prop] === 'string'
        ? new Date(data[prop])
        : data[prop]
    }
  }
  // app.log.debug('On affecte à la structure courante', cleaned)
  Object.assign(_structure, cleaned)
}

function store (callback) {
  const xfert = { }

  xfert.ajoutElevesAuthorises = _structure.ajoutElevesAuthorises
  xfert.ajoutFormateursInterdit = _structure.ajoutFormateursInterdit
  xfert.accesMultiple = _structure.accesMultiple
  xfert.datePurge = _structure.datePurge
  xfert.bloquerExerciceAutonomie = _structure.bloquerExerciceAutonomie
  xfert.oid = _structure.oid
  xfert.timezone = _structure.timezone

  if (!app.settings.sso.hasStructureManagement) {
    xfert.nom = _structure.nom
    xfert.type = _structure.type
    xfert.ville = _structure.ville
    xfert.departement = _structure.departement
    xfert.region = _structure.region
    xfert.pays = _structure.pays
  }

  app.log.debug('Stockage de la structure', xfert)
  app.sesalab.put('structure', xfert, {}, function (error, structure) {
    app.updateCaption()
    if (callback) callback(error, structure)
  }, this)
}

function getFormateurPids () {
  return _.map(_.filter(_structure.$utilisateurs, { type: constants.TYPE_FORMATEUR }), (u) => {
    if (u.externalMech) {
      return u.externalMech + '/' + u.externalId
    }

    return app.settings.baseId + '/' + u.oid
  })
}

_structure.store = store
_structure.set = set
_structure.getFormateurPids = getFormateurPids

module.exports = _structure
