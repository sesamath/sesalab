/* global btoa */
// cf https://developer.mozilla.org/fr/docs/Web/API/WindowBase64/btoa

const _cache = {}

function addLine (x, y, w, h, r, color, outline) {
  if (outline) {
    return '<rect stroke-width="1" stroke="' + color + '" fill-opacity="0" x="' + x + '" y="' + y + '" width="' + w + '%" height="' + h + '%" rx="' + r + '"/>'
  }

  return '<rect fill="' + color + '" x="' + x + '" y="' + y + '" width="' + w + '%" height="' + h + '%" rx="' + r + '"/>'
}

/**
 * Génère l'icône de la séquence (en fonction de ses params)
 * @param {Object} parametres
 * @returns {string} Le svg de l'icône
 */
function sequence (parametres) {
  let svgContent = ''

  // Generate
  const cacheKey = getCacheKey(parametres)
  if (cacheKey && _cache[cacheKey]) {
    return _cache[cacheKey]
  }

  // On passe le dessin en gris si c'est inactif
  const lines = parametres.flags.inactive
    ? ['#e2e5e0', '#e2e5e0', '#e2e5e0']
    : ['#ff9f95', '#74b4ff', '#9de082']

  // Dessine le motif d'une séquence
  const startY = 4
  const lineWidth = 0.7
  const lineHeight = 0.16
  const radius = 3
  const padding = 5
  const linesX = parametres.width / 2.0 - (parametres.width * lineWidth) / 2.0
  for (let i = 0; i < lines.length; i++) {
    svgContent += addLine(linesX, startY + (padding * i), lineWidth * 100, lineHeight * 100, radius, lines[i], parametres.sequenceModel)
  }

  // Ajout d'un contour orange
  if (parametres.flags.friends) {
    svgContent += '<rect fill="#ef2864" stroke-width="' + parametres.strokeWidth / 2.0 + '" fill-opacity="0"  width="100%" height="100%" stroke="#ff7f00"/>'
  }

  // Ajout d'une pastille rouge dans le cas d'une séquence prioritaire
  if (parametres.flags.prioritaire) {
    svgContent += '<ellipse ry="4" rx="4" cy="20%" cx="80%" stroke-width="0" stroke="#000" fill="#ef2864"/>'
  }

  // Ajout d'un cadre orange autour de l'icône
  if (parametres.flags.group) {
    svgContent += addLine(0, 0, parametres.strokeWidth, 100, 0, '#ff7f00')
    svgContent += addLine(0, 0, 100, parametres.strokeWidth, 0, '#ff7f00')
  }

  // Ajout d'un sablier
  if (parametres.flags.limiteTemps) {
    svgContent += '<text stroke-width="0" text-anchor="end" font-size="14" y="100%" x="100%" fill="#000000">⌛</text>'
  }

  // Ajout du chiffre 1 si la séquence est unique
  if (parametres.flags.uniqEleve) {
    svgContent += '<text stroke="#000" font-family="Arial" text-anchor="start" font-size="14" y="100%" x="3%" fill="#000000">1</text>'
  }

  // Sauvegarde de l'icone en cache
  const output = '<svg width="' + parametres.width + '" height="' + parametres.height + '" xmlns="http://www.w3.org/2000/svg"><g>' + svgContent + '</g></svg>'
  if (cacheKey) {
    _cache[cacheKey] = output
  }
  return output
}

/**
 * Génération d'une clé suivant l'objet donné
 * @param {Object} parametres
 * @return {string}
 */
function getCacheKey (parametres) {
  const keys = []
  if (parametres.sequenceModel) {
    keys.push('sequenceModel')
  }

  for (const attribute in parametres.flags) {
    if (parametres.flags[attribute]) keys.push(attribute)
  }

  return keys.join(';')
}

/**
 * Transforme un rendu SVG en une chaine en base64
 *
 * @param svg Chaine au format SVG
 * @return Chaine en base 64
 */
function toBase64 (svg) {
  return 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svg)))
}

module.exports = {
  sequence,
  toBase64
}
