const libXhr = require('../xhr')

function processResult (error, result, callback, context) {
  if (context) callback = callback.bind(context)
  if (error) return callback(error)
  if (result.success === false) {
    error = new Error(result.message)
    if (result.field) error.field = result.field
    // on marque ça pour que le code fasse suivre le message à l'utilisateur
    error.userFriendly = true
    callback(error)
  } else {
    callback(null, result)
  }
}

function get (path, options, callback, context) {
  xhr('GET', path, undefined, options, function (error, result) {
    processResult(error, result, callback, context)
  })
}

function put (path, data, options, callback, context) {
  xhr('PUT', path, data, options, function (error, result) {
    processResult(error, result, callback, context)
  })
}

function del (path, options, callback, context) {
  xhr('DELETE', path, undefined, options, function (error, result) {
    processResult(error, result, callback, context)
  })
}

function post (path, data, options, callback, context) {
  xhr('POST', path, data, options, function (error, result) {
    processResult(error, result, callback, context)
  })
}

function xhr (verb, path, data, options, callback) {
  path = '/api/' + path
  libXhr(verb, path, data, options, callback)
}

module.exports = {
  post,
  del,
  put,
  get
}
