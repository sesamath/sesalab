const _ = require('lodash')
const flow = require('an-flow')

/**
 * Attention, variable globale au module, important de ne require ce module qu'une seule fois et de ne traiter qu'un seul utilisateur !!!
 * @module utilisateur
 * @todo créer un vrai constructeur et ajouter les méthodes au prototype, en exportant une factory, pour éviter ce _utilisateur global au module source de bons gros bugs)
 */

function set (data) {
  Object.assign(_utilisateur, data)
  _utilisateur.sequenceFolders = _utilisateur.sequenceFolders || {}
  _utilisateur.sequenceColleguesFolders = _utilisateur.sequenceColleguesFolders || {}
  _utilisateur.groupeFolders = _utilisateur.groupeFolders || {}
  _utilisateur.ressourcesFolders = _utilisateur.ressourcesFolders || {}
  app.log.debug('Nouvel utilisateur', data)
}

function clearCustomProfil () {
  _utilisateur.profil.values = []
}

// @todo: au lieu d'utiliser ces deux booléens, on pourra simplement enregistrer ce format
//        (mais ça demande une update de données)
// _utilisateur.profil = {
//  toAdd: ["rid1", "rid2"],
//  toRemove: ["rid3"]
// }
// On notera que toute la logique liée à ces deux booléens est encapsulée dans les 2 méthodes ci-dessous
function setCustomProfil (ressourcesToAdd, ressourcesToRemove) {
  _utilisateur.profil.values = ressourcesToAdd.map((rid) => ({
    rid, checked: true, ressourceDuProfil: false // === to add
  }))
    .concat(ressourcesToRemove.map((rid) => ({
      rid, checked: false, ressourceDuProfil: true // === to remove
    })))
}

/**
 * Retourne la liste des rid des arbres à afficher
 * @private
 * @param {SesathequeItem[]} items Liste d'arbres ou d'enfants d'un arbre
 * @returns {string[]} La liste des rid
 */
function getSelectedRids (items) {
  // Il semblerait que la sésathèque puisse envoyer des ressources sans rid donc on les filtre
  // Ce n'est peut-être plus le cas, mais on sait jamais.
  const defaultRessourcesForProfileRids = _.map(_.filter(items, 'rid'), 'rid')

  // _utilisateur.profil contient :
  //    - les ressources à enlever du profil (ressourceDuProfil: true, checked: false)
  const toRemove = _.map(_.filter(_utilisateur.profil.values, (ressource) => ressource.ressourceDuProfil && !ressource.checked), 'rid')
  //    - les ressources à ajouter en plus (ressourceDuProfil: false, checked: true)
  const toAdd = _.map(_.filter(_utilisateur.profil.values, (ressource) => !ressource.ressourceDuProfil && ressource.checked), 'rid')
  //    - (legacy) les ressources du profil non décochée (ressourceDuProfil: true, checked: true)
  //               On n'en a plus besoin ici

  const rids = _.difference(defaultRessourcesForProfileRids, toRemove).concat(toAdd)

  // On peut trouver des doublons :
  // - ressources ajouté hors profil par l'utilisateur, puis ajouté dans le profil par défaut
  // - anciennes données en bas
  return _.uniq(rids)
}

function getAllUsersForStructure (url, users, callback) {
  app.sesalab.get(url, { timeout: 20000 }, (error, response) => {
    if (error) return callback(error)
    if (!response.utilisateurs) return callback(new Error('Réponse invalide, impossible de récupérer la liste des utilisateurs de la structure'))
    users = users.concat(response.utilisateurs)
    if (!response.nextUrl) return callback(null, users) // Tous les utilisateurs sont désormais présents
    getAllUsersForStructure(response.nextUrl, users, callback)
  })
}

function load (utilisateur, currentStructureOid, callback) {
  set(utilisateur)

  if (!currentStructureOid) {
    return callback(new Error('Une erreur s’est produite durant la sauvegarde de votre profil'))
  }

  if (_utilisateur.type === constants.TYPE_ELEVE) {
    window.location = '/eleve/'
    return
  }

  let currentStructureData

  flow()
    .seq(function () {
      const structures = []

      // On charge les informations pour chaque structure
      flow(_utilisateur.structures)
        .seqEach(function (id) {
          if (!id) return this()

          flow()
            .seq(function () {
              app.sesalab.get('structure/' + id, {}, this)
            })
            .seq(function (response) {
              if (!response.structure) {
                // il vaut mieux arrêter là
                return this(Error(`La structure ${id} n’existe pas`))
              }
              if (id === currentStructureOid) currentStructureData = response.structure
              structures.push(response.structure)
              this()
            })
            .done(this)
        })
        .seq(function () {
          _utilisateur.structures = structures
          _utilisateur.structureId = currentStructureOid
          this()
        })
        .done(this)
    })
    .seq(function () {
      getAllUsersForStructure('utilisateur/by-structure/' + currentStructureOid, [], this)
    })
    .seq(function (utilisateurs) {
      app.structure.set(_.assign({},
        currentStructureData,
        {
          $utilisateurs: utilisateurs
        }
      ))
      _utilisateur.utilisateurs = utilisateurs
      callback()
    })
    .catch(app.errorHandler('Une erreur s’est produite durant le chargement de l’utilisateur'))
}

/**
 * Fetch les oid des Ressources à mettre au 1er niveau du panneau Ressources
 * @param callback
 */
function loadUserRessources (callback) {
  flow()
    .seq(function () {
      if (!_utilisateur.profil || !_utilisateur.profil.name) {
        _utilisateur.profil = { name: undefined, values: [] }
        sesalab.dialogs.ProfilSelection.execute(this)
      } else {
        this()
      }
    })
    .seq(function () {
      app.sesatheque.getEnfantsProfil(_utilisateur.profil.name, this)
    })
    .seq(function (enfants) {
      callback(null, getSelectedRids(enfants))
    })
    .catch(app.errorHandler('Une erreur s’est produite durant le chargement des ressources de l’utilisateur'))
}

function logout () {
  window.location.href = '/api/utilisateur/logout'
}

function pickAttributes () {
  // On ne prend pas les folders qui sont sauvegardés séparément
  const formateur = _.pick(_utilisateur, ['oid', 'prenom', 'nom', 'login', 'mail', 'passwordCurrent', 'profil', 'type', 'bilanWithSymbols', 'fontSize'])
  if (_utilisateur.password && _utilisateur.password.length > 0) {
    formateur.password = _utilisateur.password
    formateur.passwordBis = _utilisateur.passwordBis
  }

  return formateur
}

function countTeachersInStructure () {
  let teacherCount = 0
  for (let i = 0; i < app.structure.$utilisateurs.length; i++) {
    const utilisateur = app.structure.$utilisateurs[i]
    teacherCount += (utilisateur.type === app.constants.TYPE_FORMATEUR && utilisateur.validators.account && utilisateur.validators.account.accepted) ? 1 : 0
  }

  return teacherCount
}

function remove (callback) {
  const formateur = pickAttributes(_utilisateur)

  const removeWarning = 'Vous êtes le dernier formateur de la structure.\nSupprimer votre compte entrainera la suppression de la structure.\n\nÊtes-vous sûr de vouloir continuer ?'
  // eslint-disable-next-line no-alert
  if (countTeachersInStructure() > 1 || window.confirm(removeWarning)) {
    app.log.debug('Suppression de l’utilisateur', formateur)
    app.sesalab.del('formateur', {}, function (error, utilisateur) {
      if (error) console.error(error)
      else app.toaster.toast('Une demande de confirmation vous a été adressée par mail, cette confirmation rendra la suppression du compte effective')

      if (callback) callback(error, utilisateur)
    })
  }
}

/**
 * Met à jour la structure des dossiers de l'utilisateur
 * @param {Object} folders Objet contenant la liste des dossiers à mettre à jour
 * @param {function} callback Callback a appeler
 */
function patchFolders (folders, callback) {
  if (!_utilisateur || !folders) return callback()
  app.sesalab.put(`formateur/${_utilisateur.oid}/folders`, folders, {}, callback)
}

function store (callback, dontNotify) {
  // .store ne sauvegarde pas le contenu des dossiers
  // ils sont sauvés séparément via patchFolders()
  const formateur = pickAttributes(_utilisateur)
  if (!formateur.oid) {
    return callback(new Error('Une erreur s’est produite durant la sauvegarde de votre profil'))
  }

  // On supprime ces champs pour ne pas qu'ils réparaissent dans l'application (notamment
  // on ne les veut pas pré-initialisés dans la modale de préférences)
  delete _utilisateur.password
  delete _utilisateur.passwordBis
  delete _utilisateur.passwordCurrent
  delete _utilisateur.supprimerCompte

  // Update
  app.log.debug('Stockage de l’utilisateur', formateur)
  app.sesalab.put(`formateur/${formateur.oid}`, formateur, {}, function (error, utilisateur) {
    if (!error) {
      if (!dontNotify) {
        app.toaster.toast('Vos modifications ont été prises en compte')
      }
      app.updateCaption()
    }
    if (callback) callback(error, utilisateur)
    else if (error) console.error(error)
  })
}

function getPid () {
  if (_utilisateur.type !== constants.TYPE_FORMATEUR) return
  if (_utilisateur.externalMech) return _utilisateur.externalMech + '/' + _utilisateur.externalId
  return app.settings.baseId + '/' + _utilisateur.oid
}

const constants = require('sesalab-commun/constants.js')

/**
 * Un objet utilisateur pour qooxdoo (formateur) ou angular (élève)
 * @typeDef UtilisateurFront
 * @property {string} oid
 * @property {string} prenom
 * @property {string} nom
 * @property {string} mail
 * @property {string} profil
 * @property {number} type 0 => élève, 1 => formateur
 * @property {json} ressourcesFolders
 * @property {json} sequenceFolders
 * @property {json} sequenceColleguesFolders St
 * @property {json} groupeFolders
 */
const _utilisateur = {}

_utilisateur.load = load
_utilisateur.loadUserRessources = loadUserRessources
_utilisateur.remove = remove
_utilisateur.store = store
_utilisateur.set = set
_utilisateur.patchFolders = patchFolders
_utilisateur.logout = logout
_utilisateur.getPid = getPid
_utilisateur.getSelectedRids = getSelectedRids
_utilisateur.setCustomProfil = setCustomProfil
_utilisateur.clearCustomProfil = clearCustomProfil

module.exports = _utilisateur
