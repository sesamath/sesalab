/* global FormData */
/* globals XMLHttpRequest: false */
/* globals ActiveXObject: false */
function xhr (verb, url, data, options, callback, context) {
  let request
  const timeout = options.timeout || 20000 // 20s de timeout par défaut
  if (context) callback = callback.bind(context)
  if (typeof window.XMLHttpRequest !== 'undefined') {
    request = new XMLHttpRequest()
  } else {
    const versions = ['MSXML2.XmlHttp.5.0', 'MSXML2.XmlHttp.4.0', 'MSXML2.XmlHttp.3.0', 'MSXML2.XmlHttp.2.0', 'Microsoft.XmlHttp']
    for (let i = 0, ii = versions.length; i < ii; i++) {
      try { request = new ActiveXObject(versions[i]); break } catch (e) {}
    }
  }
  if (typeof request === 'undefined') return callback(new Error('XHR indisponible'))
  request.open(verb, url, true)
  if (options.withCredentials) {
    request.withCredentials = true
  }

  const isFormData = data instanceof FormData

  // eslint-disable-next-line eqeqeq
  if (verb != 'GET' && !isFormData) request.setRequestHeader('Content-Type', 'application/json')

  const timer = setTimeout(function () {
    console.error('TIMEOUT (' + timeout + 'ms)', url)
    request.$timeout = true
    request.abort()
  }, timeout)

  request.onreadystatechange = function () {
    // eslint-disable-next-line eqeqeq
    if (this.readyState == this.DONE) {
      clearTimeout(timer)
      let parsedResponse
      if (this.response.length) {
        try {
          parsedResponse = JSON.parse(this.response)
        } catch (e) {
          // La réponse n'est pas au format json, on passe la string complète
          parsedResponse = this.response
          // Normalement c'est toujours une string, mais on a déjà vu des implémentations
          // exotiques de XMLHttpRequest ;-)
          if (typeof this.response !== 'string') {
            console.error(new Error('xhr.response n’est pas une string'), this.response, e)
          }
        }
      }

      if (this.status !== 200) {
        let message
        const errorMessage = parsedResponse ? (parsedResponse.message || parsedResponse) : null
        if (this.$timeout) {
          message = `Délai d’attente dépassé sur ${url}`
        } else if (this.status === 403) {
          message = `Accès refusé sur ${url}`
          if (errorMessage) message += ` : ${errorMessage}`
        } else if (this.status === 0) {
          message = `Plus de connexion réseau, impossible d’atteindre ${url}`
        } else if (errorMessage) {
          message = `Erreur ${this.status} sur ${url} : ${errorMessage}`
        } else {
          message = 'Une erreur interne s’est produite, merci de réessayer et de le signaler si cela se reproduit.'
        }

        const error = new Error(message)
        error.status = this.status
        error.callback = callback
        error.content = parsedResponse
        const handled = callback(error)
        if (!handled) {
          app.bus.broadcast('error', error)
        }
      } else {
        callback(null, parsedResponse)
      }
    }
  }
  if (data && !isFormData) {
    data = serialize(data)
  }
  request.timeout = timeout
  request.send(data)
}

function serialize (data) {
  return JSON.stringify(data, function (key, value) {
    if (key.length > 0 && key.charAt(0) === '$') return undefined
    return value
  })
}

module.exports = xhr
