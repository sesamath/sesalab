const _store = {}

/**
 * Ajoute un objet dans le store
 * @param {string} type
 * @param {string} key La propriété de value qui désigne son id
 * @param {Object} value L'objet à stocker (à récupérer avec {type, id})
 */
function put ({ type, key, value }) {
  if (!type || !key || !value || !value[key]) return console.error(Error('valeur incorrecte pour le store'), arguments)
  const id = value[key]
  if (!_store[type]) {
    _store[type] = {
      key,
      values: {}
    }
  }
  _store[type].values[id] = value
}

/**
 * Récupère un objet du store
 * @param {string} type
 * @param {string} id
 * @returns {*|void}
 */
function get ({ type, id }) {
  if (!type || !id) return console.error(Error('Appel invalide'), arguments)
  return _store[type]?.values[id]
}

/**
 * Retire un objet du store
 * @param {string} type
 * @param {string} key La propriété de value qui désigne son id
 * @param {Object} value L'objet à virer (avec {type, [key]})
 */
function remove ({ type, key, value }) {
  if (!type || !key || !value || !value[key]) return console.error(Error('valeur incorrecte pour le store'), arguments)
  if (!_store[type]) {
    _store[type] = {
      key,
      values: {}
    }
  }
  const id = value[key]
  delete _store[type].values[id]
}

module.exports = {
  put,
  dump: function () { return _store },
  get,
  remove
}
