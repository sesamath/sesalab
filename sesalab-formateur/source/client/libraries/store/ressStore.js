// 30min max pour
const maxTtl = 30 * 60 * 1000
/**
 * @typedef RessInfos
 * @property {string} titre
 * @property {string} rid
 * @property {string} [aliasRid]
 * @property {string} [cle]
 */
/**
 * @typedef RessSummary
 * @property {string} titre
 * @property {string} rid
 * @property {string} [aliasRid]
 * @property {string} [cle]
 * @property {number} lastUpdate
 */
/**
 * @typedef {Object.<string, RessSummary>} RessStore
 */

/** @type RessStore */
const _store = {}

/**
 * Ajoute un objet dans le store
 * @param {RessInfos} ress
 * @return {RessSummary}
 */
function put ({ titre, aliasRid, rid, cle }) {
  _store[rid] = { rid, titre, lastUpdate: Date.now() }
  if (cle) _store[rid].cle = cle
  if (aliasRid) {
    // on stocke aussi l'info pour l'alias
    // (c'est ça qui sera écrasé en cas de fork de l'alias)
    _store[aliasRid] = _store[rid]
    _store[aliasRid].aliasRid = aliasRid
  }
  return _store[rid]
}

/**
 * Récupère un résumé de ressource du store
 * @param {string} rid
 * @returns {RessSummary|undefined}
 */
function get (rid) {
  const result = _store[rid]
  if (result) return { ...result, rid }
}

/**
 * Retire un objet du store
 * @param {string} rid
 */
function remove (rid) {
  delete _store[rid]
}

/**
 * Retourne les infos de la ressource (depuis le cache s'il a moins de 30min, sinon récupéré en ligne puis mis en cache)
 * @param {string} rid
 * @returns {Promise<RessSummary>}
 */
async function getOrFetch (rid) {
  return new Promise((resolve, reject) => {
    const result = get(rid)
    if (result && result.lastUpdate + maxTtl > Date.now()) {
      return resolve(result)
    }
    app.sesatheque.getItem(rid, (error, item) => {
      if (error) reject(error)
      else resolve(put(item))
    })
  })
}

module.exports = {
  get,
  getOrFetch,
  put,
  remove
}
