const _listeners = {}

// si on voulait pouvoir retirer un listener, faudrait générer un id et le retourner (et le stocker dans _listener)
// mais y'en a jamais eu besoin pour le moment

/**
 * Ajoute un écouteur d'événement sur le bus
 * @param {string} event
 * @param {function} callback (sera rappelé avec la string de l'event en 1er argument, et d'autres arguments s'il y en a associé à l'event lors de son broadcast)
 * @param {Object} [context] un éventuel contexte (à mettre en this de la callback)
 */
function on (event, callback, context) {
  if (context) callback = callback.bind(context)
  if (!_listeners[event]) _listeners[event] = []
  _listeners[event].push(callback)
}

/**
 * Envoie un événement sur le bus
 * @param {string} event
 * @param {...*} [args] des arguments éventuels (qui seront passés au listener)
 */
function broadcast (event, ...args) {
  args.unshift(event)
  if (!_listeners[event]) return
  for (const l of _listeners[event]) {
    l.apply(window, args)
  }
}

module.exports = {
  on,
  broadcast
}
