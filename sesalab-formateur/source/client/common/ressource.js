module.exports = {
  askImportRid: (cb) => {
    const caption = `Identifiant (préfixe ${app.sesatheque.baseIdGlobal} par défaut, préciser ${app.sesatheque.baseIdPrivate}/ si nécessaire)`
    sesalab.widgets.InputDialog.execute('Importer une ressource', caption, { vertical: true }, (value) => {
      value = value.trim()
      if (!value) return // importer un id vide revient à ne rien faire
      const chunks = /^(?:(\w+)\/)?([a-z0-9]+)$/.exec(value)
      if (!chunks) return app.toaster.toast(`Identifiant "${value}" invalide`, 'error')
      let [, baseId, oid] = chunks
      if (baseId) {
        if (!([app.sesatheque.baseIdGlobal, app.sesatheque.baseIdPrivate].includes(baseId))) {
          return app.toaster.toast(`${baseId} ne correspond à aucune Sésathèque connue`, 'error')
        }
      } else {
        baseId = app.sesatheque.baseIdGlobal
      }
      cb(`${baseId}/${oid}`)
    })
  }
}
