// on veut garder ce loader compatible es5 car non-babelisé,
// il doit être chargé avant qooxdoo pour initialiser window.qx
/* eslint no-var:off */

(function () {
  if (!window.qx) window.qx = {}

  qx.$$start = new Date()
  if (!qx.$$environment) qx.$$environment = {}
  var envinfo = {
    'qx.application': 'sesalab.Application',
    'qx.debug': false,
    'qx.debug.databinding': false,
    'qx.debug.dispose': false,
    'qx.debug.io': false,
    'qx.debug.ui.queue': false,
    'qx.revision': '',
    'qx.theme': 'sesalab.theme.Theme',
    'qx.version': '5.0.3'
  }
  for (var k in envinfo) qx.$$environment[k] = envinfo[k]

  qx.$$loader = {
    init: function () {
      qx.$$loader.scriptLoaded = true
      // rarement on a qx.event undefined ou qx.event.handler undefined…
      // (cf https://app.bugsnag.com/sesamath/sesalab/errors/5e7a7a981d0e440018b4ea06)
      if (qx.event && qx.event.handler && qx.event.handler.Application && typeof qx.event.handler.Application.onScriptLoaded === 'function') {
        qx.event.handler.Application.onScriptLoaded()
      } else {
        console.error(Error('pas de qx.event.handler.Application.onScriptLoaded à lancer…'))
      }
      qx.$$loader.applicationHandlerReady = true
    }
  }
})()
