const path = require('path')

app.controller(function ($settings) {
  const completePath = path.join(__dirname, '/../../public')
  this.serve('formateur', completePath)
  this.get('formateur/inscription', { fsPath: completePath })
})
