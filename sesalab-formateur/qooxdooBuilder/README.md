# Build Qooxdoo

```
# Il faut d'abord récupérer les sources, ici la 5.0.2
# (dans ce dossier qooxdooBuilder)
wget https://github.com/qooxdoo/qooxdoo/archive/release_5_0_2.tar.gz
tar xf release_5_0_2.tar.gz

# on renomme la release en qooxdoo pour pas modifier tous nos chemins
mv qooxdoo-release_5_0_2 qooxdoo

# Et on build
# (en avril 2019 uglify-js n'est plus dans les dépendances sesalab
# et le script utilise un uglifyjs installé en global, 
# installer uglify-js localement puis décommenter la bonne ligne dans le script si besoin)
../scripts/build-qooxdoo.sh
```

En cas de pb commencer par vérifier que `./generate.py` fonctionne dans sesalab-formateur/qooxdooBuilder/qooxdoo/framework

Pour régler le bug de firefox66 on rebuild sesalab 2.12.0 avec qooxdoo v5.0.3-beta (7ea08e2a61 du dépôt https://github.com/qooxdoo/qooxdoo.git)

## migration v7
Cf https://qooxdoo.org/documentation/6.0/#/development/compiler/migration

Il y a une tentative de migration vers v6 dans la branche qx6 (et v7 dans qx7),
mais sans succès (en 2024-12), bcp de FATAL error au build qui n'aboutit donc pas.
