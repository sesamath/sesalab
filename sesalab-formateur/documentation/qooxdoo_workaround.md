## Utilisation du DOM
Il est possible de modifier le DOM afin d'y ajouter des classes et identifiants aux objets Qooxdoo, cela peut être pratique dans certains cas :
- Écraser le style par défault Qooxdoo
- Ajouter des identifiants afin de pouvoir faire des tests fonctionnels

## Mise à jour visuelle
Parfois Qooxdoo ne met pas à jour les éléments à l'écran, tous les widgets possèdent une méthode permettant de forcer cette mise à jour :
`widget.updateAppearance()`
Voir la [Documentation API](https://www.qooxdoo.org/current/api/#qx.ui.core.Widget)
