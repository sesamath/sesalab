## Ressources
- Site officiel : https://www.qooxdoo.org
- API : https://www.qooxdoo.org/current/api
- Demos : http://www.qooxdoo.org/5.0.2/demobrowser (très pratique pour comprendre et tester les widgets)
- Code source : https://github.com/qooxdoo/qooxdoo

## Architecture
**Behaviors** : Un behavior définit une vue
**Desklets** : Des objets non-réutilisables contrairement aux widgets, seulement utilisé pour la page d'accueil actuellement
**Dialogs** : Équivalent des modales
**Pages** : Équivalent d'un onglet
**Panels** : Panneaux dépliables (ex "Classes" dans la colonne de gauche)
**Theme** : Permet de personnaliser le style visuel du site
**Widgets** : Des widgets utilisables partout

## Modification du style
Qooxdoo applique un style aux objets en s'appuyant sur deux choses :
- L'apparence : Le style général des éléments, par défaut Qooxdoo applique un thème qu'il est possible de surcharger
- Les décorations : Une décoration permet de modifier spécifiquement le style d'un widget afin de le différencier des widgets similaires à celui-ci (`widget.setAppearance("custom-appearance")`)

