const webpack = require('webpack')
const webpackConfigBuilder = require('../webpackConfigBuilder')
const WebpackShellPlugin = require('webpack-shell-plugin')
const path = require('path')

const beforeBuildPath = path.resolve(__dirname, './scripts/before_build')

// before_build script will modify classes.js, so we need to ignore this file
// from the watch to not end in an infinite loop!
const qooxdooClasses = path.resolve(__dirname, './source/client/classes.js')

const config = webpackConfigBuilder(__dirname, 'formateur')

config.plugins.push(
  new WebpackShellPlugin({
    // FIXME it run 3 times
    onBuildStart: [`node ${beforeBuildPath}`],
    dev: false // required to run on every build (watch)
  }),
  new webpack.WatchIgnorePlugin([qooxdooClasses])
)

module.exports = config
