module.exports = {
  TYPE_ELEVE: 0,
  TYPE_FORMATEUR: 1,

  SEANCE_STATUT_INACTIF: 0,
  SEANCE_STATUT_ACTIF: 1,
  SEANCE_STATUT_BORNEE: 2,

  SEQUENCE_TYPE_ELEVE: 'eleve',
  SEQUENCE_TYPE_GROUPE: 'groupe',

  SERIE_STATUT_LIBRE: 0,
  SERIE_STATUT_ORDONNEE: 1,
  SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE: 2,

  DEFAULT_NON_ZAPPING: 10,
  DEFAULT_MINIMUM_REUSSITE: 70,
  DEFAULT_MAX_VISIONNAGE: 0,

  PANEL_CLASSES_TITLE: 'Classes',
  PANEL_RESSOURCES_PARTAGEES_TITLE: 'Ressources partagées',
  PANEL_RESSOURCES_TITLE: 'Ressources',
  PANEL_PRESSE_PAPIER_TITLE: 'Presse-papier',
  PANEL_MES_GROUPES_TITLE: 'Mes groupes',
  PANEL_MES_RESSOURCES_TITLE: 'Mes ressources',
  PANEL_MES_SEQUENCES_TITLE: 'Mes séquences',
  PANEL_SEQUENCES_COLLEGUES_TITLE: 'Séquences de mes collègues',

  // attention à rester < à config.$server.maxTimeout (sinon ce sera ramené à cette valeur
  // et va générer des avertissements dans les logs)
  IMPORT_TIMEOUT: 300000, // Timeout de 5 minutes pour les imports SIECLE et ONDE

  MIN_PASSWORD_LENGTH: 6,

  MIN_AUTOCOMPLETE_INPUT: 3,

  CSV_SEPARATOR: ';',

  // on verra si on met ça en settings plus tard
  DEFAULT_TIMEZONE: 'Europe/Paris',

  // nom du dossier par défaut (mes ressources & mes séquences)
  DEFAULT_FOLDER_NAME: 'Non trié',

  // externalMech pour les profs venant du SSO Sésamath (sesaprof ou GAR)
  SESASSO_EXTERNAL_MECH: 'sesasso',
  // externalMech pour les élèves venant du SSO Sésamath (GAR)
  ENT_EXTERNAL_MECH: 'ssoent',

  // propriété password de ceux qui ne peuvent pas se connecter directement (seulement via du SSO externe)
  // pour les retrouver si besoin
  NO_PASSWORD_HASH: 'noDirectLogin',

  // pour remplacer un oid disparu (sequence.owner par ex)
  OID_DELETED: 'deleted',

  /**
   * Une structure sera supprimée s'il n'y a aucun formateur dedans
   * et qu'aucun élève ne s'est connecté depuis ce délai (en jours)
   */
  MAX_CONNEXION_DELAY_BEFORE_STRUCTURE_REMOVAL: 30,

  /**
   * Age max (en jours) d'une entrée de UtilisateurAcces
   * @type {number}
   */
  MAX_AGE_UTILISATEUR_ACCESS_MS: 550 * 24 * 3_600_000,
  /**
   * Âge max (en ms) des backups
   */
  MAX_AGE_BACKUP_MS: 30 * 24 * 3_600_000,
  /**
   * Âge max (en ms) d'une entrée de journal
   */
  MAX_AGE_JOURNAL_MS: 370 * 24 * 3_600_000,
  /**
   * Période du throttle pour l'envoi de mails à une même adresse
   */
  MAIL_THROTTLE_DELAY: 30,
  /**
   * Nb max de mail que l'on peut envoyer à une même adresse en MAIL_THROTTLE_DELAY s
   */
  MAX_MAIL_IN_DELAY: 3,
  /**
   * Le nb max de résultats qu'on renverra en json (via l'api)
   */
  MAX_RESULTATS_JSON: 3000,
  /**
   * Le nb max de résultats qu'on renverra dans un csv
   */
  MAX_RESULTATS_CSV: 10000
}
