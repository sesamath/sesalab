// code ES5, si on veut passer ça avant les polyfill de babel & core-js
// cf eslintConfig override pour ce fichier (faut utiliser le parser esprima
// car le parser par défaut d'eslint veut pas de require en es5, il sort :
// Parsing error: sourceType 'module' is not supported when ecmaVersion < 2015. Consider adding `{ ecmaVersion: 2015 }` to the parser options

// ce fichier ne passe pas par babel ni webpack, il est minifié en public/app.js et suivant le navigateur il
// - redirige vers la page des navigateurs obsolètes les trop vieux
// - chargera app.module.js si le navigateur le supporte (pas de polyfill)
// - chargera app.es5.js sinon (avec les polyfill)

// IIFE pour être exécuté au 1er import
(function checkBrowser () {
  'use strict'

  function redirect () {
    window.location = '/navigateurObsolete.html'
  }

  function listenPong (event) {
    if (event.data === 'pong') {
      console.info('test de postMessage avec la sésathèque OK')
      clearTimeout(timeoutId)
      document.body.removeChild(iframe)
      window.removeEventListener('message', listenPong)
    }
  }

  // si on est ajouté dans le head
  function addToBody (elt, cb) {
    if (!document.body) {
      return setTimeout(function () {
        retryMs = retryMs * 2 // en général 1ms suffit, mais au cas où on double à chaque appel
        addToBody(elt, cb)
      }, retryMs)
    }
    document.body.appendChild(elt)
    if (cb) cb()
  }

  if (typeof window === 'undefined') {
    throw new Error('this code should only be executed in a browser')
  }

  if (
    typeof window.addEventListener !== 'function' ||
    typeof window.postMessage !== 'function' ||
    typeof window.XMLHttpRequest === 'undefined' || // https://caniuse.com/#feat=mdn-api_xmlhttprequest
    typeof window.addEventListener !== 'function' || // vire IE < 9 https://caniuse.com/#search=addEventListener
    // et qq features es5
    typeof Function.prototype.bind !== 'function' || // http://kangax.github.io/compat-table/es5/#test-Function.prototype.bind
    typeof Array.prototype.forEach !== 'function' ||
    typeof Array.isArray !== 'function' || // => vire IE ≤ 8
    typeof Blob === 'undefined' // => enlève d'anciens navigateurs (Safari 5 sous Windows par exemple)
  ) {
    console.error(Error('Fonctionnalité es5 basiques non supportées par ce navigateur'))
    return redirect()
  }

  // on teste un postMessage A/R avec une iframe contenant le ping.html de la sesatheque
  var timeoutId, iframe
  var retryMs = 1

  try {
    // on teste aussi le postMessage en iframe cross-domain
    if (window.sesathequeBaseUrl) {
      iframe = document.createElement('iframe')
      addToBody(iframe, function () {
        iframe.style.visibility = 'hidden'
        iframe.addEventListener('load', function onLoadIframe () {
          try {
            // ping.html renverra un message sur notre window
            window.addEventListener('message', listenPong)
            // on lui demande de nous rappeler
            iframe.contentWindow.postMessage('ping', '*')
            // et on attend 1s max
            timeoutId = setTimeout(function () {
              console.error(Error('Toujours pas de réponse après 1s, postMessage ne doit pas fonctionner sur ce navigateur'))
            }, 1000)
          } catch (error) {
            console.error(error)
            redirect()
          }
        }) // onLoadIframe listener
        iframe.src = window.sesathequeBaseUrl + 'ping.html'
      })
    } else {
      // ça peut être normal si on est en iframe, par ex le test d'une séquence par un formateur
      if (window.self === window.top) console.info('pas de variable globale sesathequeBaseUrl disponible, impossible de tester le postMessage')
    }
  } catch (error) {
    console.error(error)
    redirect()
  }
})()
