/**
 * Envoie l'erreur à bugsnag s'il est dispo
 * @param {Error} error
 * @param {Object} [metaData]
 */
module.exports = {
  notify: function notify (error, metaData) {
    if (typeof window === 'undefined') return console.error(new Error('notify appelé sans window disponible'))
    console.error(error, { metaData })
    if (window.bugsnagClient) {
      if (metaData) {
        window.bugsnagClient.addMetadata('custom', metaData)
      }
      window.bugsnagClient.notify(error)
    }
  },
  addMetadata: function addMetadata (metadata) {
    if (!window?.bugsnagClient?.addMetadata) return
    if (!Object.keys(metadata).length) return console.error(Error('metadata invalide'), metadata)
    window.bugsnagClient.addMetadata('custom', metadata)
  }
}
