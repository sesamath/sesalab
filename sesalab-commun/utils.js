const nodeFetch = require('node-fetch')

const defaultTimeout = 5000

/**
 * Retourne la liste des oid de séquence à partir d'un utilisateur.sequenceColleguesFolders
 * @param {Object} folder la propriété sequenceColleguesFolders d'un utilisateur, ou un élément d'un de ses folders
 * @returns {string[]}
 */
function extractSequencesCollegues (folder) {
  const sequences = []
  // if (folder?.sequencesCollegues?.length) { // passe pas le build
  if (folder && folder.sequencesCollegues && folder.sequencesCollegues.length) {
    sequences.splice(sequences.length, 0, ...folder.sequencesCollegues)
  }
  if (folder && folder.folders && folder.folders.length) {
    for (const f of folder.folders) {
      const sequencesToAdd = extractSequencesCollegues(f)
      if (sequencesToAdd.length) sequences.splice(sequences.length, 0, ...sequencesToAdd)
    }
  }
  return sequences
}

function pick (obj, properties) {
  const result = {}
  for (const [p, v] of Object.entries(obj)) {
    if (properties.includes(p)) result[p] = v
  }
  return result
}

/**
 * Fetch url et récupère les data qu'elle retourne (en json), avec gestion de timeout
 * @param {string} url
 * @param {Object} [options] Les options possibles pour un fetch (cf https://github.com/node-fetch/node-fetch/tree/2.x#readme)
 * @param {Object} [options.timeout=5000] timeout en ms
 * @returns {Promise<unknown>} Promesse résolue avec les données retournée par url
 */
function fetchJson (url, options = {}) {
  const timeout = options.timeout || defaultTimeout
  return new Promise((resolve, reject) => {
    const timerId = setTimeout(() => reject(Error(`Timeout: ${url} n’a pas répondu en ${Math.round(timeout / 1000)}s`)), timeout)

    nodeFetch(url, options)
      .then(response => {
        if (!response.ok) throw Error(`${url} répond : ${response.status} ${response.statusText}`)
        clearTimeout(timerId)
        return response.json()
      })
      .then(resolve)
      .catch(reject)
  })
}

module.exports = {
  extractSequencesCollegues,
  fetchJson,
  pick
}
