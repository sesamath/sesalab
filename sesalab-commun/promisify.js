/**
 * Prend une fonction qui gère une callback en dernier argument et retourne une promesse
 * Par exemple on peut remplacer `maFct(arg1, arg2, (error, result) => { … })`
 * par `promisify(maFct, arg1, arg2).then(result => …).catch(error => …)`
 * @param {function} fn
 * @params {*...} arg passer autant d'argument que l'on veut, (ils seront passé à fn avec la cb en dernier)
 * @return {function(...[*]): Promise<unknown>}
 */
module.exports = (fn, ...params) => new Promise((resolve, reject) => {
  fn(...params, (error, result) => {
    if (error) return reject(error)
    resolve(result)
  })
})
