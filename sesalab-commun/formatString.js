const { toAscii } = require('sesajstools')

// dans un navigateur ce code fonctionne, mais node n'est par défaut pas compilé avec les codes ICU,
// cf https://nodejs.org/dist/latest-v10.x/docs/api/intl.html
// const numberFormatter = (new Intl.NumberFormat('fr-FR')).format
// const numberFormatter = (nb) => Number.prototype.toLocaleString.call(nb, 'fr-FR')

// on fait donc ça à la main en ne gérant que le séparateur décimal
//  deepcode ignore GlobalReplacementRegex: <il ne peut y avoir qu'un seul point dans nb>
const numberFormatter = (nb) => nb.toString().replace('.', ',')

/**
 * trim + enlève les accents + remplace tout ce qui n'est pas une lettre / chiffre / . / - par _
 * @param {string} filename Le nom du fichier à formater
 * @return {string}
 */
function formatStringForFilename (filename) {
  return filename && typeof filename === 'string' && toAscii(filename.trim()).replace(/[^\w._-]+/g, '_')
}

/**
 * Formatte une valeur pour être compatible avec le format CSV
 * - trim + ajout de " en doublant les " internes + \n\r\v\f virés
 * - formatte les number avec le séparateur décimal virgule (sans séparateur de milliers)
 * @param {string|number|undefined} value La valeur à formater
 * @param {boolean} [keepCr=false] passer true pour en pas éliminer les \n\r\v\f (ils sont correctement interprétés par Excel & OpenOffice, mais ce n'est pas très orthodoxe)
 * @return {string} La string à ajouter au csv
 */
function formatCsvCell (value, keepCr) {
  switch (typeof value) {
    case 'undefined':
      // on accepte undefined (=> chaîne vide)
      return '""'
    case 'number':
      // on laisse les number sans " autour, pour pouvoir totaliser dans un tableur
      // mais on modifie le séparateur décimal
      return numberFormatter(value)
    case 'string':
      value = value.trim().replace(/"/g, '""')
      if (!keepCr) value = value.replace(/[\n\r\v\f]+/g, ' - ')
      // on ne met pas de guillemet pour date / heure
      if (/^\d\d\/\d\d\/\d{4}$/.test(value) || /^\d\d:\d\d$/.test(value)) return value
      return `"${value}"`
    default:
      console.error(new TypeError(`not a string : ${typeof value}`), value)
      return '""'
  }
}

module.exports = {
  formatStringForFilename,
  formatCsvCell
}
