// avant de faire un upgrade de bugsnag, regarder https://github.com/bugsnag/bugsnag-js/blob/master/UPGRADING.md

// À noter, ce fichier est inclus par le build webpack, pas importé ailleurs

const Bugsnag = require('@bugsnag/js')
const { get } = require('lodash')
// const { hasProp } = require('sesajstools')

// releaseStage devrait être précisé dans _private/config
// Attention: ici on n'a accès qu'aux valeurs de config exposées par config/webpackConfigLoader
const localSettingsFromWebpackConfigLoader = require('../config')
let releaseStage = get(localSettingsFromWebpackConfigLoader, 'application.staging', 'unknown')
if (releaseStage === 'prod') releaseStage = 'production'
else if (releaseStage === 'preprod') releaseStage = '-pre-prod-'
// ne pas modifier ce -pre-prod-, le script de déploiement le remplace par production au déploiement en prod
// (car ça ne recompile pas les js)

const version = localSettingsFromWebpackConfigLoader.application.version
const apiKey = get(localSettingsFromWebpackConfigLoader, 'bugsnag.apiKey')

// on exporte le résultat du 1er appel de cette fct, au 1er import
module.exports = (function startBugsnag () {
  if (!apiKey) {
    const dummy = () => undefined
    console.info('Bugsnag is disabled (no api key)')
    // On ne vérifie pas toujours que bugsnagClient est défini, on met un faux client ici
    window.bugsnagClient = {
      notify: function fakeNotify (error, metadata) {
        console.error('bugsnag n’a pas été instancié, il reçoit l’erreur', error, 'avec les metadata', metadata)
      },
      addMetadata: dummy,
      getMetadata: dummy
    }
    return window.bugsnagClient
  }

  // @see https://docs.bugsnag.com/platforms/javascript/configuration-options/
  const bugsnagClient = Bugsnag.start({
    apiKey,
    appVersion: version,
    releaseStage,
    // cf https://docs.bugsnag.com/platforms/javascript/customizing-error-reports/#updating-events-using-callbacks
    onError: (event) => {
      // @see https://docs.bugsnag.com/platforms/javascript/customizing-error-reports/#the-event-object
      // pour le format de event
      console.error('erreur qui serait envoyée à bugsnag', event)
      // on vire les plantages dans un html local
      if (/^file:\/\//.test(event.request.url)) return false
      if (/^[A-Z]:/.test(event.request.url)) return false // sous windows y'a parfois du C:\…
      const { errorMessage, stacktrace } = event.originalError
      // stacktrace est de type Stackframe[], @see https://docs.bugsnag.com/platforms/javascript/customizing-error-reports/#errors
      // stacktrace[i].file est une string, mais avant application du mapping, donc le plus souvent app.js, pas très utile…
      // Attention, on a des cas avec errorMessage undefined !

      // on vire tous les plantages qui concernent une extension firefox
      if (stacktrace?.some(({ file }) => /^moz-extension:\/\//.test(file))) return false
      // ou un truc interne à webkit
      if (stacktrace?.some(({ file }) => /^webkit-masked-url:\/\/hidden/.test(file))) return false
      // un truc interne à qooxdoo
      if (stacktrace?.every(({ file }) => file?.includes('qooxdoo'))) return false

      // si c'est une perte de session on laisse tomber
      const user = event.getMetadata('user')
      if (user?.urlRedirect === '/lost-session') return false
      // si c'est un plantage sans stacktrace dans window.onerror, ça sert à rien de l'envoyer on pourra rien en faire
      if (errorMessage === 'Script error.') return false
      if (/out of memory/.test(errorMessage)) {
        // eslint-disable-next-line no-alert
        window.alert('Le navigateur n’a plus de mémoire disponible, veuillez fermer des onglets ou des applications et recharger cette page')
        return false
      }

      // spécifique j3p

      // dead object dans esm, on lâche l'affaire
      if (errorMessage?.includes('dead object') && stacktrace?.some(({ file }) => /esm\.js\/\//.test(file))) return false
      // des bugs mathquill qu'on ne règlera pas…
      if (errorMessage?.includes('object is not extensible') && stacktrace?.some(({ file }) => /mathquill\/\//.test(file))) return false

      return true
    }
    // on pourra ajouter endpoint si on veut traiter nous-même les retours
  })

  // on ajoute notre client en global pour que le code puisse facilement lui ajouter des infos de contexte
  window.bugsnagClient = bugsnagClient

  return bugsnagClient
})()
