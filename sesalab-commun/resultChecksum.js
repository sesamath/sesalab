const { notify } = require('./notify')

function stringToHex (str) {
  let output = ''
  for (let i = 0; i < str.length; i++) {
    output += str.charCodeAt(i).toString(16)
  }
  return output
}

// Produit un hash simpliste pour éviter que les élèves envoient un score falsifié.
// Le but est juste de rendre la triche un peu plus complexe
// (Ce commentaire ne devrait pas apparaitre dans le code minifié en production,
// mais il reste public sur github)
module.exports = function (ressourceRid, eleveOids, score) {
  if (typeof score === 'string') {
    notify(new Error('score en string'), { checksumArgs: { ressourceRid, eleveOids, score } })
    score = parseFloat(score)
  }
  if (typeof score !== 'number') score = -1
  // pour limiter le risque d'arrondi ≠ coté client et serveur on ne prend que les 5 premiers chiffres significatifs
  const scoreStr = Math.round(score * 10000).toString()
  // string en hexa de l'ensemble (eleveOids est à priori déjà en hexa mais rien ne le garanti)
  const hex = scoreStr + stringToHex(ressourceRid + eleveOids.sort().join(''))
  // le nb que l'on construit
  let sum = 0

  // On fait plusieurs passes avec des blocks de tailles différentes pour tout mélanger
  for (const blocksize of [2, 3, 4, 5]) {
    let i = 0
    while (i < hex.length) {
      sum += parseInt(hex.substring(i, i + blocksize), 16)
      i += blocksize
    }
  }

  return sum.toString(16)
}
