/**
 * sur http://emailregex.com/ on indique
 * /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
 * qui laisse passer les accents sur le 1er caractère mais pas ensuite
 * celle de la RFC 5322 serait
 * (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])
 * mais elle autorise un tld à une seule lettre, bizarre…
 *
 * Par ailleurs, la RFC https://tools.ietf.org/html/rfc6530 autorise tous les caractères utf8,
 * mais pour sesalab on a uniquement des francophones dont les FAI ne gèrent pas les accents,
 * on préfère éliminer ces adresses qui sont plutôt la marque d'une faute de frappe
 *
 * https://www.npmjs.com/package/email-validator utilise
 *  /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-?\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/
 * et mentionne
 * http://fightingforalostcause.net/misc/2006/compare-email-regex.php
 * http://thedailywtf.com/Articles/Validating_Email_Addresses.aspx
 * http://stackoverflow.com/questions/201323/what-is-the-best-regular-expression-for-validating-email-addresses/201378#201378
 */
/**
 * Regexp (trop) stricte de validation d'une adresse
 * Elle ne tolère pas les accents (pourtant valides d'après la rfc 6530, mais pas gérés dans nos contrées)
 * ni pas mal de caractères spéciaux valides [!#$%&'"]
 * ni les ip comme nom de domaine (valide aussi)
 * ni un nom de domaine à un seul caractère
 * @type {RegExp}
 */
const regexp = /^[a-zA-Z0-9][a-zA-Z0-9_.+-]*[a-zA-Z0-9_]@[a-zA-Z0-9][a-zA-Z0-9.-]*[a-zA-Z0-9]\.[a-zA-Z]{2,}$/
// @todo gérer deux validation, une qui suggère qu'il y a une erreur de frappe et une autre plus laxiste (et proche des RFC) pour la validation réelle en bdd
/**
 * Vérifie qu'une adresse email est plausible
 * @param {string} email
 * @return {boolean}
 */
const validate = (email) => {
  // checks basiques
  if (typeof email !== 'string') return false
  if (email.length > 254 || email.length < 6) return false
  // la regexp laisse passer un accent sur le premier caractère
  try {
    // au cas où la string contient des caractères bizarres qui plaisent pas à Regexp
    // on veut renvoyer false sans planter
    return regexp.test(email)
  } catch (error) {
    console.error(error)
    return false
  }
}

module.exports = {
  validate,
  // faut exporter ça pour ceux qui voudraient la source (jsonSchema)
  regexp
}
