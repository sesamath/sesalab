// On ajoutait les @babel/polyfill via webpackConfigBuilder.js
// Depuis babel 7.4, y'a plus de @babel/polyfill, c'est remplacé par ce fichier
// qui utilise directement core-js (qui s'occupe d'ajouter les polyfill nécessaires au navigateur courant)
// cf https://www.thebasement.be/updating-to-babel-7.4/
// https://babeljs.io/blog/2019/03/19/7.4.0

// import 'core-js' // devenu inutile avec useBuiltins usage
// bug IE11, cf https://github.com/babel/babel/issues/9872
import 'core-js/modules/es.array.iterator'
// lui est nécessaire pour les générateurs (on en a pas) mais aussi pour async/await (y'en a pas non plus)
// import 'regenerator-runtime/runtime'
