[![js-standard-style](https://cdn.rawgit.com/standard/standard/master/badge.svg)](http://standardjs.com)

## Présentation

Sésalab est une application web en nodeJs pour l'enseignement.

Il permet de gérer des séquences pédagogiques personnalisée. Chaque formateur peut préparer des séquences pour des groupes d'élèves (voire un seul), éventuellement priorisées, avec des dates de réalisation, etc.

Créé par l'association [Sésamath](http://www.sesamath.net) pour [Labomep](http://labomep.sesamath.net), il est mis à disposition de ceux qui souhaitent sous licence [AGPL](https://www.gnu.org/licenses/agpl-3.0.html) ([article wikipédia](https://fr.wikipedia.org/wiki/GNU_Affero_General_Public_License)) sur [github](https://github.com/Sesamath/sesalab/)

Voir les [notes destinées aux développeurs](docs/dev.md) pour plus d'information sur l'installation et les modifications du code.

Merci à [Bugsnag](https://www.bugsnag.com/) pour son soutien aux projets open-source (c'est un outil permettant de tracer les erreurs qui peuvent se produire dans les navigateurs des utilisateurs).

## Bugs connus

Avec node ≥18 il faut faire un `export NODE_OPTIONS='--openssl-legacy-provider'` avant de lancer le build sinon webpack plante (mettre cette option dans les commandes de script du package.json plante sur node16).
