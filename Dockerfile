FROM node:10

WORKDIR sesalab

# On copie tout (sauf .dockerignore)
COPY . ./

# Start with npm install to keep in cache and run it only
# if package.json changes
RUN npm install -g pnpm
RUN pnpm install

# Injection de configuration
# Si on retirait _private de .dockerignore, il serait copié par la ligne précédente,
# et on pourrait le renommer ici avec
# RUN sh -c "[ -d _private ] && mv _private ./_privateLocal || (mkdir _privateLocal && echo 'module.exports = {}' > _privateLocal/config.js)"
# mais ce serait fait une seule fois au build, on on préfère exporter ça via docker-compose
# pour le faire au runtime

COPY ./docker/sesalab/_privateConfig ./_private
# faut lui donner aussi les conf des sésathèques, pour baseId & baseUrl
COPY ./docker/sesatheque/_privateConfig/config.js ./_private/bibliConfig.js
COPY ./docker/sesatheque/_privateConfig/commun.js ./_private/communConfig.js
# idem pour le test qui utilise cette même image
COPY ./docker/sesatheque/_privateConfig/testBibli.js ./_private/testBibli.js
COPY ./docker/sesatheque/_privateConfig/testCommun.js ./_private/testCommun.js

# et notre exemple de contenu pour la home
COPY ./_private.exemple/content ./_private/content
