CHANGELOG 2.6
=============

Résumé des modifications (pour le détail cf https://framagit.org/Sesamath/sesalab/commits/master)

* 2.6.46 (2017-11-02)
  * montée de version de sesatheque-client
* 2.6.45 (2017-10-26)
  * Bugfix sur des classes ENT en doublon
* 2.6.44 (2017-10-24)
  * Refonte de la gestion des classes (unicité, doublons sur les classes ENT, gestion des erreurs)
  * Amélioration sur la gestion de la corbeille
* 2.6.43 (2017-10-23)
  * Accents supprimés dans les identifiants générés automatiquement
  * Modif export qui s'ouvrait dans le bloc-note sous windows
* 2.6.42 (2017-10-20)
  * Amélioration import sur la détection des classes
  * Erreurs sur les formulaires affichées sur la page et pas uniquement au survol
* 2.6.41 (2017-10-17)
  * Bugfix ordre des sous-séquences
* 2.6.40 (2017-10-17)
  * Bugfix import tableur
* 2.6.39 (2017-10-16)
  * Bugfix test des séquences depuis "mes séquences"
* 2.6.38 (2017-10-16)
  * Bugfix import tableur
* 2.6.37 (2017-10-12)
  * Bugfix import siecle
* 2.6.36  (2017-10-12)
  * refactoring interne
* 2.6.35 (2017-10-11)
  * amélioration de la gestion des doublons
* 2.6.34 (2017-10-10)
  * Plus de sauvegarde de résultat en cas d'erreur
* 2.6.33 (2017-10-10)
  * Bugfix ordre des exos dans les séries
  * Améliore le rapport d'importation pour les homonymes
* 2.6.32 (2017-10-04)
  * Bugfix sur la suppression multiple
  * Optimisations
  * Ajout suppression multiple sur les groupes / classes / élèves
* 2.6.31 (2017-10-03)
  * Ajout de l'export des élèves
  * Bugfix de chargement pour safari
  * Bugfix sur une redirection automatique lors de la lecture des bilans par l'élève
* 2.6.30 (2017-10-02)
  * Ajout de la consultation du bilan pour un élève
  * Amélioration de l'affichage des classes en cas de doublon
  * Amélioration de la gestion de la corbeille
* 2.6.29 (2017-09-29)
  * Optimisation gestion des dates de bilan
  * Bugfix suppression des dossiers de séquences vides
  * Optimisation Corbeille
  * Amélioration affichage des bilans
* 2.6.28 (2017-09-29)
  * Bugfix affichage des classes (qui montraient parfois des élèves de la corbeille)
  * Bugfix re-chiffrement du mot de passe
