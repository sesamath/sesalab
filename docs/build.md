# Structure d'un projet

Dans le même principe que maven, le builder se base sur la détection de dossiers normés
pour activer les plugins de génération d'artefact.

```

public/ # le dossier contenant les artefacts

source/

  /source # le dossier de source

    /assets # les fichiers à recopier dans le dossier public

    /client # le dossier du code front
      app.js # point d'entrée du code front. Le résultat compilé ira dans public/app.js

    /server # le dossier du code back
      index.js # point d'entrée du code back

    /styles  # dossiers des styles
      app.scss # point d'entrée de la feuille de style. Le résultat compilé ira dans
               # public/app.css
```

# compilation du code client

Si un fichier `client/app.js` existe dans `source` le plugin de fabrication d'artefact front/js
est activé, ce qui implique les étapes suivantes :

1. lecture du fichier `source/client/app.js` et recherche des dépendances (require) pour
   les fusionner au code (via browserify). **Attention**, le require permet l'utilisation de
   globing patterns (ex. `require('./**/index.js')`)

2. Lorsque le système détecte des fonction de la forme `\s+=\s+function(\$\S+,\s+)` (en
   gros des injections de dépendances angularJS), il les transforme automatiquement dans
   leur forme "tableau". Ainsi ` = function($dep, ...) {}` devient ` = ['$dep',
   function($dep, ...){}]`. Cela permet l'usage de l'obfuscation/compression de code (voir
   plus loin)

3. Lorsque le système détecte un `require('./xxx.html')` le fichier HTML associé est
   automatiquement intégré sous la forme d'une chaîne de caractère optimisée (code HTML
   compressé, commentaires et espaces retirés, etc).

4. Enfin lorsque la "compilation" est terminée, le système maintient un cache pour
   accélérer les prochaines compilations `.builder-cache.json`.

5. Une fois l'`app.js` généré, si l'on est en mode `release` le code est transféré à
   `glyfyjs2` qui le compresse et l'obsfusque.

6. Si en mode `release` dans la configuration du builder, l'option `standalone` a été
   utilisée, les `requires` sont supprimés du code final pour permettre l'usage de
   l'artefact en tant que librairie.

7. Si le mode `debug` est activé, le source map est ajouté à l'artefact.

# compilation du style client

Si un fichier `source/client/styles/app.scss` est présent, il est compilé dans
`public/app.css` via `node-sass`

1. lecture du fichier `source/client/app.scss` est lu. **Attention** le globbing est ici
   aussi autorisé (ex. `@include("../client/**/styles.scss")`).

2. Une fois le scss compilé il est passé à postcss/autoprefixer

3. si l'on est en mode `release` le résultat est passé à postcss/nanocss

# copie des assets

Les assets sont copiés tel-quel, avec respect de l'arborescence, dans le dossier `public` (/public/foo sera alors joignable via http://leDomaine.tld/foo)


