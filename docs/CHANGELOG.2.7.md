CHANGELOG 2.7
=============

Résumé des modifications (pour le détail cf https://framagit.org/Sesamath/sesalab/commits/master)

* 2.7.17 (2017-12-08)
  * fix sur les ressources des collègues quand y'en a plus de 1000
* 2.7.16 (2017-12-07)
  * ajout d'un formulaire pour les signalements (en plus du lien mailto)
  * amélioration bugsnag
  * divers fixes de pb rapportés par bugsnag
  * fix d'un pb sur la reconnexion sur les sesathèques (sessions disparues là-bas mais pas sur sesalab)
  * (dev) amélioration des fonctions ajoutées au shell mongo
  * fix du message de confirmation en double sur la fermeture de séquence sans sauvegarde
  * fix sur la gestion du cache des fichiers statiques
  * fix du chargement sans fin des ressources partagées (lorsque les collègues n'en partagent aucune)
  * fix d'un pb d'import sur les homonymes en corbeille
* 2.7.15 (2017-12-04)
  * ajout d'un "error catcher" générique (bugsnap)
* 2.7.14 (2017-12-01)
  * Vérification qu'un item d'arbre vient du même arbre avant de faire un move (sinon toujours copy, apparement dans certains cas un refresh pouvait afficher un arbre ayant perdu des items alors que c'était resté intact en base)
  * Ajout d'un lien pour reconnecter sur les sésathèques en cas de session disparue
  * fix sur la persistence de l'option daltonisme
  * fix sur le script integrityCheck
  * éventuels items de séquence de type error masqué coté élève (se produit en cas d'import de données non gérées par ex) 
* 2.7.13 (2017-11-28)
  * ajout des urls des arbres de ressources par profil en configuration
  * fix vérification des adresses mails avant d'envoyer (si undefined par ex)
  * ajout d'un access.log avec temps de réponse (pour avoir ces temps vu de node et pas seulement du frontal web)
  * avertissement du formateur s'il met deux fois la même ressource dans une séquence (cela sera cumulé dans les bilans)
  * ajout d'avertissements en cas d'erreurs
* 2.7.12 (2017-11-22)
  * amélioration checkBrowser (le test du postMessage en cross-domain est fait aussi sur la home formateur et élève)
* 2.7.11 (2017-11-21)
  * fix récupérations des élèves (pb s'ils étaient trop nombreux)
  * fix sur la liste des profs d'un établissement (il pouvait en manquer)
  * délai d'affichage des tooltip augmenté à 20s (si la souris reste au dessus).
* 2.7.10 (2017-11-21)
  * fix sur la liste des séquences d'un élève (en n'utilisant que eleve.classe et plus groupe.utilisateurs pour la classe)
  * fix infos sur la page de profil élève
  * comparaison des noms/prénoms des élèves existants / à créer en virant les accents
  * optimisations diverses
* 2.7.9 (2017-11-17)
  * fix pb IE11
  * Déplacer et non copier un élève au drag&drop dans l'édition de séquence
  * Ajout d'un script pour lister les incohérences de données qui pourraient subsister
* 2.7.8 (2017-11-13)
  * fix calcul des meilleurs scores
* 2.7.7 (2017-11-11)
  * Vérification du navigateur (avec postMessage cross-domain)
* 2.7.6 (2017-11-10)
  * Affichage de la classe sur le profil élève
  * Correction de quelques messages d'erreur
  * Fix sur le changement de classe d'un orphelin
  * Fix sur les bilans table
  * date du résultat imposé par le serveur
  * modification de la configuration babel
  * fix ordre alphabétique des élèves dans l'édition de séquence en cas de changement de classe
  * fix sur les séquences prioritaires qui interdisent désormais effectivement les autres
* 2.7.5 (2017-11-08)
  * Ajout de l'export des bilans sur l'onglet table (les meilleurs scores)
  * Bugfix pb de rafraîchissement de "mes Ressources"
  * Ajout d'une remarque coté élève pour IE
* 2.7.4 (2017-11-06)
  * Bugfix sur l'affichage parfois fantaisiste du tableau des meilleurs scores
* 2.7.3 (2017-11-06)
  * Bugfix sur le chargement qui plantait sur les vieux navigateurs
* 2.7.2 (2017-11-03)
  * Bugfix mineur
* 2.7.1 (2017-11-03)
  * Bugfix sur l'envoi du dernier résultat lors du démarrage d'un exercice j3p
* 2.7.0 (2017-11-03)
  * Changement du moteur de cache (memcache => redis), on passe à lassi 2.2
  * Fusion des comptes élèves en doublons qui peuvent être résolu, résolutions d'incohérences dans les données
