CHANGELOG 2.12
==============

* 2.12.11 (2019-10-02)
  * bugfix création de groupe
  * ajout de la date de purge sur la page de gestion des élèves
  * la date de purge doit être au plus tôt 3j plus tard
* 2.12.10 (2019-09-27)
  * précisions sur le mot de passe provisoire (instructions des pages d'import)
  * fix export csv sous firefox (blob: ajouté en content-security-policy)
* 2.12.9 (2019-09-25)
  * bugfix duplication de séquence des collègues
  * amélioration des consignes d'import tableur
  * bugfix url de gestion des groupes de partage
* 2.12.8 (2019-09-12)
  * bugfix sur les bilans des séquences des collègues
  * bugfix sur le login multiple élève (suivant l'ordre la récupération des séquences pouvait planter)
* 2.12.7 (2019-09-09)
  * séparateur csv autorisé dans les mots de passe
  * mot de passe mis dans un import tableur avec identifiants met bien à jour le mot de passe d'un élève récupéré en corbeille (il était toujours re-généré)
  * Toujours un nouvel onglet pour le rapport d'importation
* 2.12.6 (2019-09-03)
  * Vérification du domaine de l'adresse mail
  * Fix d'un pb sur le message d'erreur de mot de passe s'il n'était constitué que de lettres accentuées
* 2.12.5 (2019-09-03)
  * Modification de la compilation des fichiers javascript et des polyfill (pour la prise en charge des vieux navigateurs, passage à core-js 3)
* 2.12.4 (2019-08-26)
  * Amélioration des messages de log (et autres modifs de cuisine interne)
* 2.12.3 (2019-05-24)
  * Amélioration des remontées d'anomalie via bugsnag
* 2.12.2 (2019-04-20)
  * mise à jour de dépendances et correction de code devenu obsolète
* 2.12.1 (2019-04-17)
  * refonte test de séquence par le formateur
  * harmonisation de la gestion d'erreur
  * bugfix mineur sur la détection de navigateurs obsolètes
* 2.12.0 (2019-04-01)
  * rebuild qooxdoo.js pour corriger un bug apparu avec firefox 66
  * modif du délai d'affichage des infobulles
