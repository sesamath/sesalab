CHANGELOG 2.10
==============

Résumé des modifications (pour le détail cf https://framagit.org/Sesamath/sesalab/commits/master)

* 2.10.14 (2018-10-23)
  * bugfixes mineurs [1]
* 2.10.13 (2018-10-19)
  * bugfixes mineurs [1]
* 2.10.12 (2018-10-18)
  * refacto gestion de la session, avec ajout du service $session pour rationalisation du code
  * meilleur filtrage des structures à la création de compte (ne pas afficher des structures dans lesquelles on pourra pas créer de compte)
  * fix édition de comptes élèves SSO (masquer les champs qui ne peuvent être modifiés)
  * fix tri alphabétique des groupes
  * synthèse de l'import dans un nouvel onglet
  * fix téléchargement du rapport pour IE11
* 2.10.11 (2018-09-28)
  * fix édition de séquence (propriétés au chargement et au changement de sélection)
* 2.10.10 (2018-09-27)
  * fix affectation oid en cas d'external identiques (on veut aussi login identiques)
  * fix IE11 + fix properties sur édition de séquence + fix showParcours
  * màj sesajstools pour fix IE11
  * ajout d'un lien contextuel pour les infos de ressource
  * fix sélection multiple dans l'édition de séquence
* 2.10.9 (2018-09-27)
  * fix de pb causés par les doublons de mail 
  * fix mineur édition de séquence
  * fix mineur reset pass
  * améliorations cosmétiques
* 2.10.8 (2018-09-25)
  * unicité imposée du mail (avec nettoyage de la base, pas mal de fusions de comptes formateur, supprimé des mails identiques mis à de nombreux élèves, etc.)
  * restauration du menu par défaut au clic droit sur l'import (pour coller)
  * évolutions webpack
  * améliorations cosmétiques (styles CSS)
  * passage à node 10 dans docker (node 10 en prod depuis plusieurs semaines|mois…)
  * amélioration des filtres bugsnag pour limiter le bruit inutile
  * bugfix mineur throttle api
* 2.10.7 (2018-09-17)
  * reformulation des conditions de meilleur score pour une meilleur lisibilité
  * fix table des meilleurs scores
  * bugfix notif des autres prof de la décision du premier qui décide
  * on affiche le mail à valider sur la page de validation
* 2.10.6 (2018-09-13)
  * réindexation des UtilisateurAcces (pour des questions de performances, timestamp plutôt que date)
  * améliorations diverses de robustesse
* 2.10.5 (2018-09-09)
  * refacto purge élève
  * fix purgeStructure en ajoutant la purge des acces (et timestamp sur UtilisateurAcces)
  * refacto validation de compte (avec renommage validators)
  * preboot ne bloque plus en mode cli si les sésathèques répondent pas
  * modif purgeFormateur (blindage pour ne pas supprimer de classe + ajout checkStructureToRemove)
  * fix classe.delete() (on vire les élèves plutôt que de leur mettre une classe undefined)
* 2.10.4 (2018-09-04)
  * fix fermeture onglet sur création de ressource
* 2.10.3 (2018-09-04)
  * fix création de groupe
  * fix mathgraph formatter
  * aj des droits pour activer l'entrée Modifier sur une ressource fraîchement créée
  * bugfixes mineurs [1]
* 2.10.2 (2018-09-03)
  * fix màj mes ressources à la création (+fermer l'onglet, sur modif aussi) et simplification import
  * fix logout pour les élèves venant d'un ENT (ils ont un message spécifique et ne reviennent plus sur la home)
* 2.10.1 (2018-08-30)
  * upgrade sesatheque-client pour bugfix bilan mathgraph
* 2.10.0 (2018-08-30)
  * màj lassi pour passer à mongo4
  * On ne supprime plus automatiquement une structure sans formateur si elle a des accès élèves récents
  * fix cron & conf docker pour compatibilité mongoDb v4
  * fix api/resultat/last
  * fix pb import
  * ajout du critères "sélectionnable" sur des textes qui ne l'étaient pas

[1] par bugfix mineur on entend la correction d'un problème assez rare qui ne touche que certains navigateurs ou ne se produit que dans un contexte très particulier, ou l'amélioration d'un message d'erreur en cas d'incohérence. Il y en a quasiment à chaque version sans que ce soit toujours mentionné.
