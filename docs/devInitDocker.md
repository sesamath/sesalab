# Mise en place initiale du projet avec docker

Cette documentation utilise docker, pour une installation classique cf [[devInit.md]]

## Pré-requis
Il faut disposer de :
* docker https://docs.docker.com/install/
* docker-compose https://docs.docker.com/compose/install/
* code source de sesalab (cf [[dev.md]])

## Configuration

Il y a une configuration locale minimale à copier et adapter la première fois

```bash
mkdir _private
cp -a _private.exemple/config.docker.minimal.js _private/config.js
# éditer _private/config.js si besoin
```

La configuration utilise les noms de host sesalab.local, bibliotheque.local et commun.local.

## Usage

### build

Pour relancer un build, avec mise à jour des dépendances et compilation du code client dans le container.

```
pnpm run docker:build
```

Il faudra donc relancer cette opération après une mise à jour du code (via un git pull) ou une modification des dépendances listées dans le package.json.

### run

Pour lancer les serveurs sesalab et sesathèques
```
pnpm run docker:dev
```

`pnpm run docker:dev` démarre automatiquement les containers suivants :
- **sesalab.local** qui sera visible via http://sesalab.local:3000
- **bibliotheque.local** sur http://bibliotheque.local:3001 (sesatheque générale)
- **commun.local** sur http://commun.local:3002
- **mailhog**
- **mongo**
- **redis**

Les fichiers de configuration de sesalab reprennent ceux de _private/ en surchargeant les valeurs imposées par
docker-compose.yml et la conf des sésathèques docker.
Les fichiers de configuration des sesathèques sont injectés depuis `docker/sesatheques/_privateConfig`

Avec cette commande, docker lance la commande `pnpm run dev`, le serveur sera donc automatiquement relancé à chaque
modification de code nodeJs, et le build front automatiquement reconstruit.

### logs

Pour suivre les logs d'un container en particulier

```
docker-compose logs -f sesalab.local|bibliotheque.local|commun.local|redis|mongo
```

### tests

Pour lancer les tests sous docker:

```
pnpm run docker:test
```

## Nettoyage docker

`docker system prune` permet d'effacer toutes les images et containers qui ne tournent pas et ne sont pas utilisés.

Pour supprimer tout sauf les containers sesalab :

```
# on démarre nos containers
docker-compose start
docker-compose -f docker-compose.test.yml start
# On efface tout le reste
docker system prune
# On arrête éventuellement nos containers
pnpm run docker:stop
```
