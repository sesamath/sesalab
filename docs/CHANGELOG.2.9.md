CHANGELOG 2.9
=============

Résumé des modifications (pour le détail cf https://framagit.org/Sesamath/sesalab/commits/master)

* 2.9.18 (2018-08-30)
  * fix contenu home venant de _private/content/homePublic.inc.html qui n'était plus affiché
* 2.9.17 (2018-08-22)
  * sécurisation des tests (qui refusent de se lancer si la base de test est la même que la base "normale")
  * bugfix classe en corbeille à l'import
* 2.9.16 (2018-08-03)
  * incrément sans modif pour régler des pbs de cache
* 2.9.15 (2018-07-17)
  * bugfix mineur (au select sur un item en erreur)
* 2.9.14 (2018-06-19)
  * Ajout de la page statique de mentions légales
  * Amélioration de l'affichage des bilans de séquence
  * Améliorations internes (split de la conf docker entre test et dev, un resultFormatter n'a plus besoin d'implémenter toutes les méthodes, vérification de configuration croisée avec les sésathèques)
  * Le code migre de github vers framagit
* 2.9.13 (2018-06-05)
  * Ajout de l'accord préalable lié à la RGPD (c'était après la création de compte avec la charte, pour mutualiser avec les comptes SSO)
  * Ajout d'une page spécifique au logout pour un compte SSO
  * Amélioration de l'affichage des erreurs côté professeurs et élèves
  * Correction d'un bug lié au scroll dans les panneaux côté professeurs
* 2.9.12 (2018-05-29)
  * Meilleur gestion de la perte de session à la sauvegarde (si le navigateur est resté plus de 20h sur un exercice sans action de l'utilisateur), avec un message d'erreur adapté
* 2.9.11 (2018-05-18)
  * Activation du contrôle des dates au niveau schéma de données
  * Modifs pour usage avec docker
* 2.9.10 (2018-05-17)
  * revert de modifs de la 2.9.9 qui pose pb, en attendant correction
* 2.9.9 (2018-05-16)
  * Plus de localStorage pour les arbres de ressources (on laisse le cache gérer)
  * Qq ajouts de tests automatisés
  * Amélioration de la gestion d'erreur autours des mails
  * Édition des propriétés de la sous-séquence sur la sous-séquence (c'était mis avec la série)
  * Diverses améliorations de robustesse
* 2.9.8 (2018-04-27)
  * status 403 sur session invalide à la sauvegarde des résultats
  * meilleure résistance à des anomalies de resultatId
* 2.9.7 (2018-04-27)
  * Modifications pour rendre impossible l'ouverture d'une nouvelle session si on en avait déjà une
    (c'etait assez fréquent de voir un élève se connecter dans un 2e onglet alors qu'un premier élève l'était dans un 1er onglet, lorsque le 1er revenait sur sa session elle était évidemment perdue mais cela ne se voyait pas)
* 2.9.6 (2018-04-26)
  * ajout d'information dans les notifications d'anomalies
* 2.9.5 (2018-04-25)
  * incrémentation de version pour résoudre un pb de cache (le build de la 2.9.4 posait un pb à puffin sous iOS sans déranger aucun autre navigateur, même puffin pour Android)
* 2.9.4 (2018-04-24)
  * bugfix mineurs[1]
  * amélioration des scripts de vérification d'intégrité à postériori (chaque objet a désormais un format strict mais il faut vérifier qu'il n'y a pas d'anomalie sur leurs relations entre eux)
* 2.9.3 (2018-04-23)
  * Ajout de bugsnag sur la page d'accueil (pour tenter de comprendre le pb de puffin iOS)
  * Amélioration de la documentation du code
* 2.9.2 (2018-04-21)
  * Amélioration de l'import en cas de données partiellement invalides
  * Améliorations des notifications d'erreur
  * Ajout de tests automatisés
* 2.9.1 (2018-04-18)
  * Application stricte des règles de validation du format de toutes les données manipulées, règles ajoutées en 2.9.0 mais ne pouvaient s'appliquer aussitôt pour laisser les scripts de rectification faire leur travail.
* 2.9.0 (2018-04-18)
  * Évolution importante avec l'utilisation généralisée de jsonSchema pour décrire le format des données et contrôler leur intégrité. Ajout de ces contraintes d'intégrité au niveau du stockage (et plus seulement au niveau du code qui manipule les données avant de les enregistrer).

[1] par bugfix mineur on entend la correction d'un problème assez rare qui ne touche que certains navigateurs ou ne se produit que dans un contexte très particulier, ou l'amélioration d'un message d'erreur en cas d'incohérence. Il y en a quasiment à chaque version sans que ce soit toujours mentionné.
