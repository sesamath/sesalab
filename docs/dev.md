# Notes aux développeurs sesalab

## Documentations
Ces liens ne sont plus valides tant que la génération de la documentation n'est pas remise en route :-/
  * [lassi](../lassi/index.html)
  * [api](../sesalab-api/documentation/index.html)
  * [back office](../sesalab-formateur/documentation/index.html)
  * [front office](../sesalab-eleve/documentation/index.html)
  * [sesatheque](http://bibliotheque.sesamath.dev/doc/)

## Mise en place du projet

La première étape consiste à cloner le projet en local

```bash
git clone git@framagit.org:Sesamath/sesalab.git
// ou
git clone https://framagit.org/Sesamath/sesalab.git
// si vous êtes réfractaire à git vous pouvez aussi récupérer un zip (ou tar.gz) sur https://framagit.org/Sesamath/sesalab

// ensuite on se place dans le dossier du code récupéré
cd sesalab
```

Dans toute la doc, on utilise pnpm (`npm -g i pnpm` pour l'installer globalement), mais vous pouvez utiliser npm ou yarn si vous préférez.

La motivation principale est la rapidité et le gain d'espace disque :
- https://pnpm.js.org/docs/en/motivation.html
- https://www.kochan.io/nodejs/why-should-we-use-pnpm.html

## Configuration

Sesalab est conçu pour fonctionner avec deux [Sésathèques](https://git.sesamath.net/sesamath/sesatheque) (ça peut être la même, mais il faut lui en donner deux en configuration), une pour les ressources mises à disposition et l'autre pour les ressources créés par les utilisateurs de sesalab.

Il faut donc avoir au préalable deux Sésathèques fonctionnelles et qui se connaissent (chacune doit être déclarée chez l'autre pour qu'elles puissent échanger des informations).

Une notion importante est le baseId d'une Sésathèque, qui est associé à sa baseUrl, et utilisé dans tous les identifiants de ressources. Changer de baseId impose donc de modifier toutes les ressources que la Sésathèque contient et tout ce qui les référence, ça peut être lourd.

Les Sésathèques déclarées dans sesalab doivent aussi connaître ce sesalab (la première lui attribue aussi
 son baseId), via sa liste config.sesalabs.

## Première installation

Cf [init](devInit.md) ou [init via docker](devInitDocker.md)

## Mise en production

Varnish est utilisable en frontal, mais pas obligatoire. Si vous l'utilisez attention à lancer `./scripts/purge_varnish`
 après chaque build. Il y a aussi quelques [remarques supplémentaires](README_PROD.md).

### Lancer l'appli en mode "production"

L'usage de pm2 est recommandé, cf sa documentation et `_private.exemple/pm2App.json5` (sinon `pnpm start` après un build:prod)

```bash
# pour un lancement de node en console sans pm2
pnpm start
# sinon
scripts/run -h
```

### Déployer

Installer pm2
```bash
pnpm install pm2
```

Adapter `_private/pm2Deploy.json5` à votre configuration

Initialiser le dépôt (ici de dev) la première fois avec

```
./scripts/deploy -d -s
```

Puis pour déployer (ici en dev, -p pour la prod)
```
./scripts/deploy -d
```

Pour afficher les options possibles du déploiement
```
./scripts/deploy
```

### Tests unitaires
`pnpm run test`

### Tests fonctionnels
`pnpm run test:browser` permet de lancer les tests fonctionnels (selenium
avec webdriver), mais il faut avoir deux sésathèques et un sesalab qui
tournent.

#### Avec docker

#### Sans docker
1) Dans le dossier du projet sesatheque et lancer `pnpm run
start:testBoth`, qui va lancer les deux sésathèques de test sur les ports
précisés dans _private/testBibli.js et _private/testCommun.js, à priori 3011
et 3012

2) Dans le dossier sesalab, lancer `pnpm run build` puis `pnpm run test:browser` pour lancer les
tests fonctionnels.

On peut aussi lancer le même sesalab que celui testé par chromedriver avec start:test (il faut avoir lancé un build avant) 
