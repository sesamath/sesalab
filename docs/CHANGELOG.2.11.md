CHANGELOG 2.11
==============
* 2.11.13 (2019-03-25)
  * bugfix mineur sur la gestion des résultats
* 2.11.12 (2019-03-20)
  * refonte gestion des résultats
* 2.11.11 (2019-03-18)
  * Ajout du lien pour un signalement sur une ressource
  * Bugfix export des résultats de classe
  * optimisation de la purge d'un élève
  * refonte gestion des séquence coté client, optimise les échanges avec le serveur
* 2.11.10 (2019-03-04)
  * normalisation des icones de ressources
  * référence au presse papier retirée (il était mentionné mais la gestion n'était pas implémentée)
* 2.11.9 (2019-02-19)
  * fix test de séquence
  * amélioration de la gestion des résultats pour les exercices en autonomie
* 2.11.8 (2019-02-12)
  * fix du nb d'essais parfois erroné coté élève
  * bugfix mineur sur la sauvegarde de resultat
* 2.11.7 (2019-01-10)
  * fix ajout de ressource (dossier dans lequel la ranger)
* 2.11.6 (2018-12-18)
  * fix petits pb sur suppression / édition d'alias
* 2.11.5 (2018-12-13)
  * màj lassi 2.4.11 avec gestion des doublons (message d'erreur spécifique en cas de plantage d'un enregistrement pour cause d'un index unique, aj de onDuplicate)
  * gestion des doublons de groupe (si on crée une classe / groupe du même nom qu'une en corbeille ça la récupère)
  * compatibilité pnpm
  * màj bugsnag 5
  * fix d'un bug qui empêchait de modifier les groupes issus d'un import labomep1
  * fix softDelete sequence (ça modifiait la séquence)
  * simplifications de code
* 2.11.4 (2018-12-03)
  * fix bilan des classes qui n'ont pas de résultat
  * modification des csv de bilans, la durée de l'exercice est désormais un nombre (sans l'unité seconde), les nombres ont le séparateur virgule
  * ajout de l'effectif entre parenthèses après les noms de groupe (reste un petit bug de màj en tps réel dans la gestion des élèves)
  * ajout de documentation du code
* 2.11.3 (2018-11-29)
  * fix des exercices qui disparaissaient parfois dès le chargement ou juste après (avec un peu de refacto seance/sequence dans le code angular)
* 2.11.2 (2018-11-28)
  * fix élèves "sans classe" dans les bilans (+élèves n'ayant pas participé parfois invisibles)
  * incrément du nb d'essais après chargement et pas juste avant (n'incrémente donc plus si le chargement plante)
* 2.11.1 (2018-11-26)
  * bugfixes mineurs [1]
  * améliorations cosmétiques
  * fix recherche de structure (pb d'espace)
  * fix pb de scroll sur les séquences
  * noms complets de tous les élèves dans le bandeau en cas de connexion multiple
  * suppression de tout ce qui concernait l'import labomep v1 (qui a fermé)
* 2.11.0 (2018-10-30)
  * refacto complète des imports : 
    * on autorise notamment n'importe quel mot de passe élève mais il doit en changer à la première connexion
    * rapport d'importation plus complet
    * amélioration des messages d'erreur
    * onglet synthèse qui affiche le rapport complet (permet un copier / coller dans un tableur pour avoir le même résultat que le csv) 
  * fix pb de scroll
  * checkBrowser et bugsnag exécuté au début de tous les app.js (mis par webpackConfigBuilder)
  * amélioration de la gestion des erreurs sur des pbs de droit (si session perdue sur les sésathèques on ajoute un lien de reconnexion)
  
Remarque :  bugfix mineur suppose la correction d'un problème assez rare qui ne touche que certains navigateurs ou ne se produit que dans un contexte très particulier, ou l'amélioration d'un message d'erreur en cas d'incohérence. Il y en a quasiment à chaque version sans que ce soit toujours mentionné.
