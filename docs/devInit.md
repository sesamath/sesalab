# Installation initiale

Si vous préférez utiliser docker, cf [[devInitDocker.md]]

## pré-requis
- [nodeJs](https://nodejs.org/en/) en version 8 ou supérieure
- [pnpm](https://pnpm.js.org/) ou [npm](https://www.npmjs.com/)
- [mongoDb](https://www.mongodb.com/)
- [redis](https://redis.io/)

## Configuration

Il y a une configuration d'exemple à copier et adapter la première fois

```bash
cp -a _private.exemple _private
# éditer _private/config.js pour l'adapter à votre environnement
```

La configuration par défaut utilise les noms de domaine sesalab.local
ainsi que bibliotheque.local et commun.local, c'est plus lisible mais ce n'est pas une obligation
(ça marche très bien aussi avec localhost:xxxx avec 3 ports différents).

Pour utiliser ces alias, vous devez ajouter à votre `/etc/hosts` (sous windows c'est le fichier hosts dans un dossier système, qui dépend de la version de windows) ces 3 noms à la ligne qui commence par 127.0.0.1, laisser localhost en dernier, cela pourrait ressembler à

```
# /etc/hosts
127.0.0.1	bibliotheque.local commun.local sesalab.local localhost
```

Sinon, modifier les baseId de sesathèque pour utiliser
- bibli sur localhost3001 => http://localhost:3001
- commun sur localhost3002 => http://localhost:3002
et indiquer sur la première bibliothèque que notre sesalab est sur http://localhost:3000

Pour le reste, préciser les accès à mongo & redis, ainsi que les autres personnalisations voulues dans les différents fichiers de _private

## Installation classique des dépendances

(à relancer après une modification du package.json)
```bash
pnpm install
```

## build
Le code client est construit avec l'une de ces commandes
```
# compilation non-minifiée
pnpm run build
# idem, avec re-compilation automatique à chaque changement de fichier
pnpm run build:watch
```

Pour une compilation déstinée à la **production** on utilisera plutôt `pnpm run build:prod` (minification, et variable NODE_ENV=production)

Pour mettre à jour les dépendances et rebuild (après un git pull par exemple), c'est `pnpm run refresh`

Cf les détails du [build](build.md)

## run
Le serveur NodeJs est responsable de l'API de données (sesalab-api), mais aussi de servir le code "client" des différents modules (sesalab-eleve, sesalab-formateur, etc).

Pour du test local en https (avec un serveur web qui proxy vers nodeJs) et des certificats autosignés, 
il faudra faire un `export NODE_TLS_REJECT_UNAUTHORIZED=0` avant de lancer le start de labomep et des biblis pour que les échanges via node-fetch de sesalab-sso fonctionnent.

Pour lancer le serveur Sesalab en local, il faut utiliser l'une de ces deux commandes
```bash
# lance le serveur
pnpm start
# idem, mais redémarre après chaque changement du code serveur
# (pas de rebuild du code client, pour ça faut lancer un `pnpm run build:watch`)
pnpm start:dev
```
Le service sera accessible sur le port précisé en configuration (3000 par défaut).

## Déploiement

Varnish est utilisable en frontal, mais pas obligatoire. Si vous l'utilisez attention à lancer `./scripts/purge_varnish` après chaque build. Il y a aussi quelques [remarques supplémentaires](README_PROD.md).

Pour le déploiement, vous pouvez utiliser pm2 (adapter `_private/pm2Deploy.json5` à votre configuration)

```bash
pnpm install pm2
```

Initialiser le dépôt (ici de dev) la première fois avec
```
./scripts/deploy -d -s
```

Puis pour déployer (ici en dev, -p pour la prod)
```
./scripts/deploy -d
```

Pour afficher les options possibles du déploiement
```
./scripts/deploy
```

## Lancer l'appli en mode "production"

L'usage de pm2 est recommandé, cf sa documentation et `_private.exemple/pm2App.json5` (sinon `pnpm start` après un build:prod)

```bash
# pour un lancement de node en console sans pm2
pnpm start
# sinon
scripts/run -h
```
