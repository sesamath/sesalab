# Évolutions souhaitables

# Interface élève
Il faudra migrer pour sortir d'angular 1.x qui devient vraiment périmé.

Passer à [lit](https://lit.dev/) directement semble la meilleure option (car les upgrades successifs vers la version actuelle d'angular seront très long et fastidieux, et on aimerait progressivement basculer vers lit qui est plus accessible au développeur lambda). 

Cf un exemple de migration réussie, celui de la console angular (désormais écrite en lit, c'est dire…) :
https://17.nx.dev/blog/nx-console-gets-lit

# interface prof
Passer à lit serait un travail énorme, il vaudrait mieux dans un premier temps passer à qooxdoo 7 qui serait déjà une grosse avancée (avec un build plus standard, des classes ts plus orthodoxes que le système actuel), sans forcément demander un travail monstrueux

# préalable
Avant de commencer, il faudra évidemment créer des tests fonctionnels étendus, pour vérifier que l'on récupère bien les fonctionnalités attendues.

Cf 
