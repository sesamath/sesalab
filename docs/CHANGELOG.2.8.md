CHANGELOG 2.8
=============

Résumé des modifications (pour le détail cf https://framagit.org/Sesamath/sesalab/commits/master)

* 2.8.18 (2018-04-17)
  * mails de notification tous en texte brut
  * amélioration des tests automatisés
* 2.8.17 (2018-04-03)
  * améliorations diverses (robustesse et tests)
  * amélioration d'ergonomie sur les arbres sur l'édition de séquence (cliquer sélectionne sans modifier l'état d'ouverture / fermeture)
* 2.8.16 (2018-03-28)
  * amélioration des performances sur la gestion des arbres
  * ajout de tests fonctionnels automatisés (navigateur piloté par du code)
* 2.8.15 (2018-03-20)
  * fix d'un bug avec firefox 59
  * meilleure gestion du rafraichissement de la vue élève en fin d'exercice (délai variable suivant le contexte)
* 2.8.14 (2018-03-05)
  * fix disparition des groupes (introduit en 2.8.13)
* 2.8.13 (2018-03-02)
  * ajout d'une réponse sur /alive pour permettre un test depuis une sonde externe
  * amélioration des tests unitaires et du shell mongo
  * bugfix sur la suppression de groupe et classe
  * ajout de tests fonctionnels automatisés
  * ajout d'une configuration docker pour lancer sesalab & deux sésathèque en une fois, avec un container docker pour lancer les tests fonctionnels
  * ajout d'un message en cas de sous-séquence sans élève ou sans exercice
  * amélioration du message en cas d'utilisation d'un lien de validation déjà utilisé précédemment
  * amélioration des metadata bugsnag
  * ajout de tests suite à des erreurs bugsnag (pour rendre le code plus robuste face aux aléas de contexte)
  * sécurisation de la création de compte (on pouvait créer des doublons en cas de pb réseau + clics multiples)
  * amélioration de la gestion de résultats incohérents
* 2.8.12 (2018-02-01)
  * fix sur les liens vers les bilans de la home (hs si y'avait une apostrophe dedans)
  * fix sur autocomplete (on gardait l'état précédent avec 4 char puis 3 puis rien)
  * modifs mineures pour bugsnag
* 2.8.11 (2018-01-31)
  * fix pb de séquence prioritaire (apparu en 2.8.10)
* 2.8.10 (2018-01-31)
  * Ajout d'une limite sur le nombre de classes par structure
  * Ajout des tests unitaires (import tableur)
  * Déplacement de la logique de validation de compte vers sesalab-home
  * Gestion des séquences prioritaires par professeur
  * Mise à jour du module SSO
  * Suppression des élèves et classes en "soft delete" avant un import
  * Verification des arguments d'une route HTTP pour capter les /undefined/ en amont et les traiter
  * Correction d'un bug sur les pictogrammes de séquences (ils sont maintenant mis à jour en temps réel)
  * Correction d'un bug sur la vue des bilans (si on demandait la vue table avant que la page n'ait fini son initialisation)
  * Correction de bugs divers (Bugsnag)
  * Durcissements de sécurité
* 2.8.9 (2018-01-24)
  * Refonte de la gestion des erreurs coté élève (+usage de Promise généralisé)
  * Refonte des statistiques
  * Ajout des infos élève dans bugsnag
  * Retour à la home après la création d'un compte élève
  * Amélioration de la gestion de l'autocomplete de structure sur la création de compte
* 2.8.8 (2018-01-19)
  * ajout d'avertissement sur diverses anomalies qooxdoo (event pas au bon format, selection vide, etc.)
  * refus des saisies vide sur les input texte (dès la saisie et plus seulement à l'enregistrement)
  * ajout des filtres sur la vue table des bilans
  * fix sur l'envoi de mail de demande de validation aux formateurs pas encore valides
  * en cas de pb réseau sur l'enregistrement d'un résultat, on insiste
  * fix cronDaily
  * refactoring des réponses api en html (mise sur sesalab-home)
  * bouton imprimer sur les bilans
  * meilleurs bilans signalés par un * dans la vue liste
  * ajout de tests
  * ajout de CircleCI
  * durcissement de certaines validations, update de nettoyage d'anomalies dans la base
  * type d'établissement affiché à coté du nom
* 2.8.7 (2018-01-05)
  * Optimisation /api/actualites
  * Clarification de owner pour les séquences, fix d'anomalies en base
  * Refonte des commandes cli
* 2.8.6 (2018-01-04)
  * Fix invalid data during Registration
  * Update uirouter to the last version
  * upgrade de lassi en 2.2.10 pour avoir countBy
  * Amélioration sur le get actualités, pour réduire le nb de requêtes (avec countBy)
  * fix autocomplete structures dans le dialog de création élève
  * Fix ui-router update on /admin
  * fix d'un bug sur les élèves n'ayant pas fait la séquence toujours absent des bilans
* 2.8.5 (2017-12-28)
  * Scroll automatique lorsque l'on fait glisser un item vers une zone hors de l'écran (utiles pour déplacer un item d'un arbre qui déborde au dessus ou en dessous)
  * On prévient des élèves supprimés d'une séquence car ils sont dans la corbeille
  * Message adapté pour la récupération du mot de passe si c'est une authentification externe
  * Scroll automatique vers la popup qui montre les détails d'un bilan j3p
  * mise à jour angular
* 2.8.4 (2017-12-22)
  * Amélioration de la gestion d'erreur à la sauvegarde des résultats
* 2.8.3 (2017-12-21)
  * Restauration correcte de la séquence dans son état précédent quand on quitte sans sauvegarder (ça ne sauvegardait pas mais éditer de nouveau montrait les modifications qui auraient dues être abandonnées)
  * Fix double clic sur les ressource
  * Sécurisation des dossiers de séquences (le tri pouvait être perdu en cas de modification des préférences qui échouaient pour un pb réseau)
  * Simplification de la gestion des résultats, meilleure remontée d'erreur
  * Fix un pb de rafraichissement des 10 dernières séquences qui apparaissent en page d'accueil
  * Allègement des réponses de l'api sur les bilans
* 2.8.2 (2017-12-18)
  * fix d'un pb d'import quand il y avait des élèves déjà présent sans nom ni prénom (créés par un import précédent avec un bug qui l'avait laissé passer)
* 2.8.1 (2017-12-18)
  * correction en base de données d'anciens résultats j3p avec scores globaux incohérents
  * Amélioration sur la gestion du localStorage (bugfix pour Safari en navigation privée)
  * Améliorations des infos envoyées en cas de pb js dans un navigateur qcq
  * fix pb de resize coté élève
  * diverses améliorations / consolidations de l'existant
* 2.8.0 (2017-12-08)
  * ajout de tracker sur les pbs de sauvegarde des scores, meilleure gestion de l'erreur à la sauvegarde
  * modif babel pour limiter les transformer et ajouter les polyfill
  * gestion de la sélection multiple pour restaurer des éléments de la corbeille (modif de la gestion des événements au passage)
  * fix pour débloquer les autres séquences quand les prioritaires sont faites
  * amélioration de la gestion d'erreur sur les appels de l'api
