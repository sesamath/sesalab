Changelog 2.2
=============

Le code pour formater les résultats (score et reponse), qui est dans sesalab-eleve/source/client/services/formatter/index.js est externalisé dans sesatheque-client/resultatFormatters pour tous les types (tous ne sont pas encore codés, c'est default sinon)

### Breaking changes sesatheque-client@1.2

Afin d'avoir des id uniques cross-sesathèques, on généralise des id sous la forme `baseId/oid`, qui deviennent la propriété rid de tous les sesathequeItem
Coté Sésathèque, ça correspond à la propriété rid sur les objets Ressource et aliasOf sur les Ref (une ressource pouvant aussi avoir un aliasOf, si c'est un alias d'une autre)

#### format des sesathèque en configuration
config.sesatheques doit désormais être un tableau de {baseId, baseUrl}

#### getItem
`getItem(baseId, id, next)` devient `getItem(rid, next)` 

#### getRessource
`getRessource(baseId, id, next)` devient `getRessource(rid, next)` 
