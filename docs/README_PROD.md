## Routes nécessitant un délai (timeout) étendu :
- api/import-groupe-siecle

## Routes ne contenant que du statique
 Avec varnish, le cookie peut être viré sur ces chemins dans le vcl_recv, pour que ce soit mis en cache par le navigateur
 - /formateur/*
 - /eleve/*
 - /front/*

 ça donne dans vcl_recv

 ```
  if (
    (req.method == "GET" || req.method == "HEAD")
    && req.http.host == "ledomaineConcerné"
    && (req.url ~ "^/eleve/" || req.url ~ "^/formateur/" || req.url ~ "^/front/")
  ) {
    unset req.http.cookie;
    # On ajoute un header pour montrer que l'on a effacé nous-même les cookies,
    # (faut pas en remettre, au cas qqun ajouterait une session sur ces chemins)
    set req.http.X-NoCookie = "1";
  }
```
et dans vcl_backend_response
```
    # ne pas ajouter de cookie si on les a virés à l'aller
    if (beresp.http.Set-Cookie && bereq.http.X-NoCookie) {
        unset beresp.http.Set-Cookie;
        # On ajoute ce header pour montrer que varnish a viré les cookies de la réponse
        set beresp.http.x-no-cookie = "1";
    }
```

## Routes non-authentifiées permettant des créations d'entités :
- Création de structure : POST api/structure
- Création de compte formateur : POST api/formateur
- Création de compte élève : POST api/eleve
