CHANGELOG
=========

Voici le résumé des changements concernant Sesalab, pour les détails (et les "trous" dans les descriptions), cf https://framagit.org/Sesamath/sesalab/commits/master
* [2.12.*](CHANGELOG.2.12.md) (2019-04-01)
* [2.11.*](CHANGELOG.2.11.md) (2018-10-30)
* [2.10.*](CHANGELOG.2.10.md) (2018-08-30)
* [2.9.*](CHANGELOG.2.9.md) (2018-04-18)
* [2.8.*](CHANGELOG.2.8.md) (2017-12-08)
* [2.7.*](CHANGELOG.2.7.md) (2017-11-03)
* [2.6.*](CHANGELOG.2.6.md) (2017-07-26)
* [2.2.*](CHANGELOG.2.2.md) (2017-03-03)
* 2.1.* (2017-01-20)
* 2.0.* (2015-04-20)

Et pour les changement des Sésathèques (que sesalab utilise), c'est sur https://framagit.org/Sesamath/sesatheque/blob/master/doc_src/CHANGELOG.md
