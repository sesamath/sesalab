/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@arnumeral.fr
 *    Site   : http://arnumeral.fr
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
'use strict'

const path = require('path')
const _ = require('lodash')
const formidable = require('formidable')
const appLog = require('sesalab-api/source/tools/appLog')

const { hasProp } = require('sesajstools')

/**
 * Retourne la clé de cache pour le throttle dans ce contexte
 * @private
 * @param {Context} context
 * @returns {string}
 */
function getThrottleKey (context) {
  return 'throttle-' + ['ip', 'method', 'path'].map((field) => context.request[field]).join('-')
}

app.config(function ($cache, $settings, $session) {
  // const isProd = $settings.get('application.isProd', false)
  const sesathequesOrigins = $settings.get('application.sesatheques', []).map(s => s.baseUrl.replace(/\/$/, ''))

  /**
  * On ajoute des fonctions utilitaires au contexte lassi
  * @external Context
  */
  const Context = lassi.Context

  /**
   * Ajoute success: true aux propriétés de l'objet retourné, en virant les propriétés qui démarrent par $ (dont $loadState)
   * Surcharge de la méthode originale  qui fait
   * ```js
   *     data = _.clone(data)
   *     data.success = true
   *     this.json(data)
   * ```
   * @param data
   */
  Context.prototype.rest = function restModified (data) {
    if (!data || typeof data !== 'object') {
      throw Error('context.rest appelé sans objet !')
    }
    const result = { success: true }
    for (const [prop, value] of Object.entries(data)) {
      if (/^\$/.test(prop)) continue
      result[prop] = value
    }
    this.json(result)
  }

  /**
   * Retourne true si l'utilisateur (formateur) appartient à la structure
   * @function external:Context#isFormateurInStructure
   * @param {string} structureOid
   * @return {boolean}
   */
  Context.prototype.isFormateurInStructure = function (structureOid) {
    // Note: si l'utilisateur est connecté c'est que son compte sur la structure *a été validé*
    // et qu'il en fait partie
    const formateur = $session.getCurrentFormateur(this)
    return formateur && formateur.structures.includes(structureOid)
  }

  /**
   * Retourne true si l'utilisateur (formateur) courant appartient à une des structures
   * @function external:Context#isFormateurInOneOfStructures
   * @param {string[]} structureOids
   * @returns {boolean}
   */
  Context.prototype.isFormateurInOneOfStructures = function (structureOids) {
    const formateur = $session.getCurrentFormateur(this)
    if (!formateur) return false
    // Note: si l'utilisateur est connecté c'est que son compte sur la structure *a été validé*
    // et qu'il en fait partie
    return formateur.structures.some(sOid => structureOids.includes(sOid))
  }

  /**
   * Rappelle next si authentifié, accessDenied sinon
   * @function external:Context#forceAuthentifie
   * @param next
   * @returns {*}
   */
  Context.prototype.forceAuthentifie = function (next) {
    if ($session.isAuthenticated(this)) return next()
    this.accessDenied('Accès refusé, vous devez être authentifié')
  }

  /**
   * Rapelle next si c'est un formateur, accessDenied sinon
   * @function external:Context#forceFormateur
   * @param next
   */
  Context.prototype.forceFormateur = function (next) {
    if ($session.isAuthenticatedAsFormateur(this)) return next()
    this.accessDenied('Accès refusé, vous devez être formateur')
  }

  /**
   * Rappelle next si c'est un formateur admin local, sinon accessDenied
   * @function external:Context#forceFormateurAdmin
   * @param {function} next
   */
  Context.prototype.forceFormateurAdmin = function (next) {
    if ($session.isAuthenticatedAsAdmin(this)) return next()
    this.accessDenied('Accès refusé, vous devez être administrateur')
  }

  /**
   * Retourne true si le user courant a le droit de modifier le groupe
   * @function external:Context#peutEditerGroupe
   * @param {Groupe} groupe
   * @returns {boolean}
   */
  Context.prototype.peutEditerGroupe = function (groupe) {
    const formateur = $session.getCurrentFormateur(this)
    if (!formateur) return false
    // Le formateur ne peut éditer que les classes d'une structure à laquelle il appartient
    if (groupe.isClass && groupe.structure) return formateur.structures.includes(groupe.structure)
    // L'utlisateur ne peut éditer que les groupes dont il est propriétaire
    if (groupe.owner) return formateur.oid === groupe.owner
    return false
  }

  /**
   * Rappelle cb si y'a pas de throttle en cours (sinon sort avec le message passé précédemment)
   * @function external:Context#checkThrottle
   * @param {errorCallback} cb
   */
  Context.prototype.checkThrottle = function (cb) {
    // Visiblement ce test dérange plus qu'autre chose, mais pour avoir du test valide
    // il faudrait pas que l'api réagisse différemment suivant l'environnement.
    // par ailleurs, ça empêche de tester que ce throttle fonctionne…
    // @todo voir où il coince en test et si on peut contourner le pb autrement (ne pas bourriner les endpoints qui throttlent)
    if (process.env.NODE_ENV === 'test') return cb()

    const context = this
    const cacheKey = getThrottleKey(context)
    $cache.get(cacheKey, function (error, throttleMessage) {
      if (error) return cb(error)
      if (throttleMessage) {
        // On envoie une erreur 429 (too many requests)
        context.status = 429
        context.plain(throttleMessage)
      } else {
        cb()
      }
    })
  }

  /**
   * Enregistre le message pour un temps d'attente minimal entre deux appels à une même route
   * @function external:Context#setThrottle
   * @param {number} delay (s)
   * @param {string} throttleMessage
   */
  Context.prototype.setThrottle = function (delay, throttleMessage) {
    const cacheKey = getThrottleKey(this)
    $cache.set(cacheKey, throttleMessage, delay, (error) => {
      if (error) console.error(error)
    })
  }

  /**
   * On impose le json avant la sortie
   */
  lassi.on('beforeTransport', function (context, data) {
    // const reqUrl = `${context.request.method} ${context.request.url}`
    // appLog(reqUrl)
    // Au cas où deux controleurs veulent envoyer une réponse, on coupe le 2e tout de suite
    // (pas la peine d'appeler le transport qui dira que la réponse est déjà partie)
    // mais apparemment lassi se protège contre ça et sort une erreur sans nous appeler une 2e fois
    if (context.isSent) return appLog.error(Error('trop tard c’est déjà parti (2e passage dans beforeTransport)'))
    context.isSent = true
    // appLog('avec context', {status: context.status, error: context.error, contentType: context.contentType}, {})
    // appLog('et data', data, {})

    // On passe ici après le dernier context.next, ou bien des méthodes qui envoient un contentType
    // - context.denied() => plain avec status 403 sans error
    // - plantage qq part non catché (ou context.next(Error)) => plain avec error
    // - context.next('une string') => plain sans error, avec data = {content: 'une string'}
    // - context.json, context.rest => json
    // - context.html => html
    // On impose du json sur /api/, sauf si quelqu'un a imposé autre chose,
    // On converti les erreurs 50x en json 200 avec message, mais on laisse les 403 & 404 en text/plain

    if (/^\/api\//.test(context.request.url)) {
      // Par défaut on désactive le cache sur toutes les requêtes de l'API.
      // Sans ce flag no-cache, IE11 se met à cacher des requêtes GET ajax
      // ce qui pose pas mal (!) de soucis
      context.setNoCache()

      // data.content en string avec text/plain est provoqué par du context.next('erreur pas instance de Error')
      // on convertit ça ici en erreur ordinaire, mais lassi aurait dû le faire
      if (
        context.contentType === 'text/plain' &&
        data && typeof data.content === 'string' &&
        !context.error &&
        !context.status
      ) {
        appLog.error('text/plain + data.content sur /api/ sans erreur, ça ressemble à une string passée à context.next', data)
        context.contentType = 'application/json'
        data = { success: false, message: data.content }

      // On transforme les Error catchées par lassi en réponse json 200
      } else if (context.error) {
        if (!context.error.noLog) appLog.error(context.error, context.error.properties)
        let errorMessage = context.error.message || context.error

        // Masque les erreurs internes
        if (!context.error.status || context.error.status >= 500) {
          errorMessage = 'Désolé, une erreur interne s’est produite.'
          if (data) delete data.message
        }

        context.error = undefined
        context.status = 200
        context.contentType = 'application/json'
        if (!data) data = {}
        data.success = false
        data.internalError = true
        if (data.message) data.message += ` (erreur : ${errorMessage})`
        else data.message = errorMessage

      // On regarde les autres cas, un contentType imposé non json
      } else if (context.contentType !== 'application/json') {
        if (context.status && context.status !== 200) {
          // une erreur envoyée exprès, on laisse (probablement du text/plain avec 403 ou 404 ou redirect)
          appLog.debug(`status ${context.status} en ${context.contentType} avec`, data)

        // } else {
          // qqun sait ce qu'il fait, on laisse continuer, mais ça devrait pas être sur une route /api/, on râle
          // trop bruyant…
          // console.error(new Error(`contentType non json (${context.contentType}) sur ${context.request.url}, devrait être hors /api/*`))
        }

      // Pas de contentType précisé, on impose json et ajoute success:true si la propriété manque
      } else if (!context.contentType) {
        context.contentType = 'application/json'
        if (data && Object.keys(data).length && !hasProp(data, 'success')) data.success = true
      }
    }
    appLog.debug('Le transport va envoyer ' + (context.status || 200), data)
  })

  lassi.on('afterRailUse', function (rail, name) {
    /**
     * Middleware qui doit être ajouté avant body-parser, pour parser les fichiers postés (import)
     * @private
     * @param req
     * @param next
     */
    function parseFormData (req, next) {
      const form = new formidable.IncomingForm()
      form.keepExtensions = true
      req.body = req.body || {}
      req._body = true // To prevent extra parsing from body-parser

      form.parse(req, (err, fields, files) => {
        if (err) return next(err)

        _.forEach(fields, (value, key) => {
          req.body[key] = value
        })
        _.forEach(files, (value, key) => {
          req.body[key] = value
        })

        next()
      })
    }

    // ajout le CORS après cookie
    // on peut ajouter les arguments , settings, middleware puis appLog(middleware)
    // pour voir le code de chaque middleware
    if (name === 'cookie') {
      appLog('$rail', 'adding cors middleware')
      rail.use('/', function (req, res, next) {
        // on regarde qui nous appelle de l'extérieur
        const origin = req.header('Origin')
        if (origin && sesathequesOrigins.includes(origin)) {
          res.header('Access-Control-Allow-Origin', origin)
          res.header('Access-Control-Allow-Credentials', 'true')
          res.header('Access-Control-Allow-Headers', 'X-Requested-With')
          appLog.debug(`cors, origin ${origin} ok`)
        } else {
          appLog.debug(`cors, origin ${origin} denied`)
        }
        next()
      })

      // active parseFormData sur les routes d'import de fichier
      appLog('$rail', 'add parsing formData for import ONDE')
      rail.post('/api/import-onde', function (req, res, next) {
        parseFormData(req, next)
      })
      appLog('$rail', 'add parsing formData for import SIECLE')
      rail.post('/api/import-siecle', function (req, res, next) {
        parseFormData(req, next)
      })
    }
  })
}) // app.config

// On veut un contrôleur générique de session valide, qui doit donc être exécuté avant tous les autres
// (déclarés par le require suivant)
app.controller(function ($session) {
  // resultat/last est appelé depuis un autre domaine, sans session mais avec token
  const publicRouteRegExp = /^\/api\/(login|settings|formateur|eleve|utilisateur|structure|groupes|resultat\/last)/
  this.all('api/*', function (context) {
    if (publicRouteRegExp.test(context.request.url)) return context.next()
    if ($session.isAuthenticated(context)) return context.next()
    // Sur une page html il faudrait répondre que la page requiert une authentification,
    // mais ici sur l'api on suppose que le client a perdu sa session.
    context.setStatus(401)
    // pour éviter l'exécution des contrôleurs suivants présents sur la route
    context.skipNext = true
    context.restKo({
      message: 'Session invalide ou expirée',
      urlRedirect: '/lost-session'
    })
  })
})

// $session en premier pour qu'il soit dispo en dépendance pour tous les autres
require('./session/service')
// on charge tous nos contrôleurs/entity/services, à priori par ordre alphabétique des dossiers
require('glob').sync(path.join(__dirname, '/**/index.js')).forEach((r) => {
  require(r)
})
