// une entity pour stocker les autres entity supprimées pendant un certain temps,
// un genre de "super corbeille", pour se prémunir contre une suppression accidentelle
// (bug dans un script ou autre, cf cronDaily pour suppression définitive)
// cf le beforeDelete des entity supprimées
const appLog = require('../tools/appLog')

app.entity('Backup', function () {
  this.validateJsonSchema(
    {
      // JSON-Schema
      type: 'object',
      properties: {
        oid: { type: 'string' },
        id: { type: 'string' },
        timestamp: { type: 'number' },
        entity: {
          type: 'object',
          properties: {
            oid: { type: 'string' }
          },
          additionalProperties: true
        }
      },
      required: ['id', 'entity'],
      additionalProperties: false
    }
  )

  // indexes
  this.defineIndex('id')
  this.defineIndex('timestamp')

  // hooks
  this.beforeStore(function (cb) {
    if (this.oid) {
      appLog.error(Error(`Une entity Backup ne peut pas être modifiée (${this.oid}`))
      delete this.oid // ça va en créer une nouvelle
    }
    this.timestamp = Date.now()
    cb()
  })
})
