/**
 * Représente les données d'accès d'un utilisateur.
 *
 * @Entity UtilisateurAcces
 * @class UtilisateurAcces
 * @property {string} utilisateur Identifiant de l'{@link Utilisateur}
 * @property {Number} type Type d'accès
 * @property {string} structure Identifiant de la {@link Structure}
 * @property {Date} timestamp Date d'accès
 * @property {string} origine Point de connection (ENT, sesaprof, etc)
 * @property {Number} departement Departement (1 pour l'Ain et 20 pour la Corse sans distinguer 20A et 20B)
 * @property {string} pays Pays
 * @property {Number} academie Academie
 */
const { TYPE_ELEVE, TYPE_FORMATEUR } = require('../../../sesalab-commun/constants')

app.entity('UtilisateurAcces', function () {
  // JSON-Schema
  this.validateJsonSchema({
    type: 'object',
    properties: {
      oid: { type: 'string' },
      utilisateur: { type: 'string' },
      type: {
        enum: [TYPE_ELEVE, TYPE_FORMATEUR]
      },
      structure: { type: 'string' },
      timestamp: { type: 'integer' },
      uai: { type: 'string' },
      origine: { type: 'string' },
      // Attention, departement et academie sont des integer
      departement: { type: 'integer' },
      academie: { type: 'integer' },
      pays: { type: 'string' }
    },
    required: ['utilisateur', 'type', 'structure', 'timestamp']
  })

  this.defineIndex('utilisateur')
  this.defineIndex('type')
  this.defineIndex('structure')
  this.defineIndex('timestamp')
  // ceux-là sont facultatifs, on indexe rien s'ils n'existent pas
  this.defineIndex('origine', { sparse: true })
  this.defineIndex('departement', { sparse: true })
  this.defineIndex('pays', { sparse: true })
  this.defineIndex('academie', { sparse: true })
})
