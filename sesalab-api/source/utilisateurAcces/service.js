/**
 * @module services/$utilisateurAcces
 */
const { stringify } = require('sesajstools')

/**
 * @service $utilisateurAcces
 */
app.service('$utilisateurAcces', function (UtilisateurAcces) {
  /**
   * Ajoute une entity UtilisateurAccess dans la table en tâche de fond (retourne avant que l'écriture soit effective)
   *
   * @param {Utilisateur} utilisateur
   * @param {Structure} structure
   * @memberof $utilisateurAcces
   */
  function log (utilisateur, structure) {
    // Note : La structure reste présente, dans certain cas, en tant qu'attribut de l'utilisateur
    // c'est le cas du module sesasso-labomep par exemple.
    if (!utilisateur || !utilisateur.oid) throw Error('Utilisateur manquant')
    if (!structure || !structure.oid) throw Error('Structure manquante')
    const record = UtilisateurAcces.create({
      utilisateur: utilisateur.oid,
      type: utilisateur.type,
      structure: structure.oid,
      timestamp: Date.now(),
      uai: structure.uai || null,
      origine: utilisateur.externalMech || 'sesalab',
      // la Corse est le seul département avec un departementNumero qui n'est pas un équivalent à un number
      // avec ' 20A' ou ' 20B', mais parseInt donnera 20 dans ce cas
      departement: structure.departementNumero ? parseInt(structure.departementNumero, 10) : null,
      pays: structure.pays,
      academie: parseInt(structure.academieId, 10) || null
    })
    record.store(function (error, record) {
      if (error) console.error(error)
    })
  }

  /**
   * Compte les accès pour une structure, un département ou une academie.
   *
   * @param {object} filters filtres, il faut impérativement after et (oid structure|utilisateur ou un critère géographique)
   * @param {Number|Date} filters.after Timestamp (ou Date ou moment())
   * @param {Number|Date} [filters.before] Timestamp  (ou Date ou moment())
   * @param {string} [filters.structure] Identifiant d'une {@link Structure}
   * @param {string} [filters.ville] Ville
   * @param {Number} [filters.departement] 1 pour l'Ain, 20 pour la Corse
   * @param {string} [filters.departementNumero] Numéro de département (01 pour l'Ain, 20A ou 20B pour la corse)
   * @param {Number} [filters.academie] idem academieId
   * @param {Number} [filters.academieId]
   * @param {Number} [filters.type] type utilisateur
   * @param {Number} [filters.utilisateur] oid utilisateur
   * @param {Number} [filters.origine] ExternalMech utilisateur
   * @param {Number} [filters.externalMech] ExternalMech utilisateur
   * @param callback
   * @memberof $utilisateurAcces
   */
  function count (filters, callback) {
    if (!filters) return callback(Error('filters obligatoires'))
    if (!filters.after) return callback(Error('filtre after manquant'))

    // retourne le timestamp correspondant au filter prop (plante si on le trouve pas)
    const getTsFilter = (prop) => {
      if (Number.isInteger(filters[prop])) return filters[prop]
      // Date ou moment()
      if (typeof filters[prop].valueOf === 'function') return filters[prop].valueOf()
      throw Error(`filters.${prop} invalide (${stringify(filters[prop])})`)
    }

    // on accepte des filters de structure, faut translater les propriétés,
    // et faire les cast (car on a  pas les même types que Structure !), sans muter l'original
    const myFilters = {}
    // filtre géographique, on n'en prend qu'un, en commençant par les plus restrictifs
    // soit ['structure', 'ville', 'departement', 'academie']
    let hasGeoFilter = true // mis ensuite à false si on ne passe dans aucun des cas ci-dessous
    if (filters.oid || filters.structure) {
      myFilters.structure = filters.oid || filters.structure
    } else if (filters.ville) {
      myFilters.ville = filters.ville
    } else if (filters.departement || filters.departementNumero) {
      myFilters.departement = filters.departement || filters.departementNumero
      // faut un cast en number
      if (typeof myFilters.departement === 'string') myFilters.departement = Number(myFilters.departement)
      else if (Array.isArray(myFilters.departement)) myFilters.departement = myFilters.departement.map(Number)
    } else if (filters.academieId || filters.academie) {
      myFilters.academie = filters.academieId || filters.academie
      // faut un cast en number
      if (typeof myFilters.academie === 'string') myFilters.academie = Number(myFilters.academie)
      else if (Array.isArray(myFilters.academie)) myFilters.academie = myFilters.academie.map(Number)
    } else {
      hasGeoFilter = false
    }

    // on accepte aussi de chercher pour un user donné, même sans critère géographique
    if (filters.utilisateur) {
      myFilters.utilisateur = filters.utilisateur
    } else if (!hasGeoFilter) {
      return callback(Error('filtre géographique ou utilisateur manquant'))
    }
    // minimum requis ok, on passe au facultatif
    if (filters.origine || filters.externalMech) myFilters.origine = filters.origine || filters.externalMech
    if (filters.type) myFilters.type = filters.type
    // fin du facultatif (before particulier traité à la fin)

    // on peut passer à la requête
    const after = getTsFilter('after')
    // attention, faut utiliser greaterThan et pas after (qui cast en date, on cherche un int)
    const query = UtilisateurAcces.match('timestamp').greaterThan(after)

    const addFilter = (key) => {
      const value = myFilters[key]
      if (!value) return
      if (['string', 'number'].includes(typeof value)) query.match(key).equals(value)
      else if (Array.isArray(value) && value.length) query.match(key).in(value)
      else console.error(Error(`valeur du filtre ${key} incorrecte`), value)
    }
    // myFilter ne contient qu'un seul filtre géographique, plus besoin de s'arrêter au 1er rencontré
    for (const prop of ['structure', 'ville', 'departement', 'academie', 'utilisateur', 'type', 'origine']) addFilter(prop)

    // et le before éventuel qui fait pas de in ni equals
    if (filters.before) {
      const before = getTsFilter('before')
      query.match('timestamp').lowerThanOrEquals(before)
    }

    query.count(callback)
  }

  return {
    count,
    log
  }
})
