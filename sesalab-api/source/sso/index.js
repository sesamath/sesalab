/**
 * @module services/$sso
 */
const appLog = require('sesalab-api/source/tools/appLog')

const returnNull = () => null
const returnFalse = () => false

const defaultProviderMethods = {
  getAccountCreationInfos: returnNull,
  getHasStructureManagement: returnFalse,
  getUrlDeconnexion: returnNull,
  getUrlAccount: returnNull,
  getUrlProvider: returnNull,
  overrideSettings: (userSsoProperties, settings) => settings
}

/**
 * @typedef $sso~providerAccountCreationInfos
 * @property {string} message Message affiché sur le formulaire de création de compte (html autorisé)
 * @property {string} url Url du lien|bouton
 */
/**
 * Retourne un objet {message, url} pour afficher un bloc html cliquable sur le formulaire de création de compte
 * @callback $sso~getAccountCreationInfos
 * @param {Object} userSsoProperties
 * @return {$sso~providerAccountCreationInfos|null}
 */

/**
 * @callback $sso~getUrlDeconnexion
 * @param {Object} userSsoProperties
 * @return {string|null} L'url de déconnexion éventuelle
 */
/**
 * @callback $sso~getUrlProvider
 * @param {Object} userSsoProperties
 * @return {string|null} L'url du provider externe (sa home, pour éventuellement afficher un lien)
 */
/**
 * Retourne les settings du user (sans muter originalSettings)
 * @callback $sso~overrideSettings
 * @param {Object} userSsoProperties propriété sso du user (que le sso avait mise au login)
 * @param {Object} originalSettings Les settings par défaut de l'appli pour ce user
 * @return {Object} settings modifiés (sans muter les originaux !)
 */
/**
 * @callback $sso~getUrlAccount
 * @param {Object} userSsoProperties propriété sso du user (que le sso avait mise au login)
 * @return {string} L'url de modification des données du compte SSO
 */

/**
 * Un provider pour $sso, c'est une liste de méthodes :
 * - toutes sont facultatives pour enregistrer un provider (ce sera la mếthode par défaut si non fournie)
 * - toutes seront appelées avec l'objet utilisateur.sso mis par le login du sso en question (si c'est lui qui a connecté l'utilisateur, sinon ça peut être undefined)
 * @typedef $sso~provider
 * @type Object
 * @property {$sso~getAccountCreationInfos} [getAccountCreationInfos]
 * @property {function} [getHasStructureManagement] retourne true si la structure est gérée par le SSO (donc pas éditable par sesalab)
 * @property {$sso~getUrlDeconnexion} [getUrlDeconnexion] retourne une url de deconnexion spécifique au SSO
 * @property {$sso~getUrlProvider} [getUrlProvider] retourne l'url de la home du SSO (ou null)
 * @property {$sso~getUrlAccount} [getUrlAccount] Retourne une url vers la page de gestion du compte SSO
 *                                      sinon le compte sesalab sera affiché mais pas modifiable
 *                                      (hors propriétés spécifiques sesalab)
 * @property {$sso~overrideSettings} [overrideSettings] retourne une version modifiée des settings pour le user
 */

app.service('$sso', function () {
  const providers = {}

  return {
    /**
     * Retourne tous les providers dans un array (sans leur mech).
     * @return {Object[]}
     * @memberof $sso
     */
    getAll: () => Object.values(providers),

    /**
     * Retourne le provider de externalMech.
     * @param {string} externalMech
     * @return {object}
     * @memberof $sso
     */
    get: (externalMech) => {
      // on signale mais on throw pas car il peut rester dans mongo des externalMech farfelus
      if (!providers[externalMech]) console.error(new Error(`Aucun provider pour "${externalMech}"`))
      return providers[externalMech]
    },

    /**
     * Retourne la liste des providers enregistrés (donc la liste des externalMech gérés)
     * @return {string[]}
     * @memberof $sso
     */
    getProvidersIds: () => Object.keys(providers),

    /**
     * Enregistre un provider pour un externalMech
     * @param {string} externalMech
     * @param provider
     * @throws {Error} si externalMech avait déjà un provider enregistré
     * @memberof $sso
     */
    register: (externalMech, provider) => {
      if (!externalMech) throw new Error('externalMech non défini')
      if (providers[externalMech]) throw new Error(`$sso pour "${externalMech}" déjà enregistré`)
      const providerCompleted = {}
      for (const [methodName, defaultMethod] of Object.entries(defaultProviderMethods)) {
        if (provider[methodName]) {
          if (typeof provider[methodName] !== 'function') throw new Error(`${methodName} n’est pas une fonction (provider "${externalMech}")`)
          providerCompleted[methodName] = provider[methodName]
        } else {
          providerCompleted[methodName] = defaultMethod
        }
      }
      providers[externalMech] = providerCompleted
      appLog(`sso provider ${externalMech} registered with methods {${Object.keys(provider).join(', ')}}`)
    }
  }
})
