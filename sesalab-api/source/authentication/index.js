app.controller('*', function () {
  this.options(function (context) {
    // context.setHeader('Access-Control-Allow-Origin', '*');
    context.setHeader('Access-Control-Allow-Methods', 'GET POST OPTIONS')
    context.setHeader('Access-Control-Allow-Headers', 'Origin,Content-Type,Accept')
    context.setHeader('Access-Control-Max-Age', 1728000)
    context.contentType = 'text/plain'
    context.next(null, 'ok')
  })
})
