/**
 * @module services/$crypto
 */
const _ = require('lodash')
const crypto = require('crypto')

/**
 * @Service Crypto
 */
app.service('$crypto', function ($settings) {
  const defaultSalt = '11db850-7563-11e4-a2f1-b5758-b580ba7'
  const salt = $settings.get('$crypto.salt', defaultSalt)
  if (salt === defaultSalt) {
    throw new Error('Il faut spécifier un $crypto.salt dans _private/config AVANT que des utilisateurs ne soient créés avec un mot de passe')
  }

  const maxInt = Math.pow(26, 5) - 1

  /**
   * Hash un password.
   *
   * @param passwd
   * @return {string}
   * @memberof $crypto
   */
  function hash (passwd) {
    // On force l'encodage en binary, pour garder les même hash en node6 (default utf8) qu'on node4 (default binary)
    return crypto.createHmac('sha256', salt).update(passwd, 'binary').digest('hex')
  }

  /**
   * Génère un token au format hexadecimal.
   *
   * @return {string}
   * @memberof $crypto
   */
  function token () {
    return crypto.randomBytes(64).toString('hex')
  }

  /**
   * Nettoie code et lance une Error si le code n'est pas de la forme attendue.
   *
   * @param code
   * @return {string}
   * @throws {Error|TypeError} Si code n'est pas une string ou contient plus de 5 lettres
   * @memberof $crypto
   */
  function cleanCode (code) {
    if (typeof code !== 'string') throw new TypeError('Code invalide')
    if (code.length > 5) throw new Error('Code invalide (5 caractères max)')

    return _.padStart(code.toUpperCase(), 5, 'A')
  }

  /**
   * Transforme un entier en code.
   *
   * @see /test/sesalab-api/source/tools/crypto/index.test.js
   * @param entier
   * @return {string}
   * @throws {RangeError|TypeError} Si entier n'est pas un entier entre 0 et 26^5
   * @memberof $crypto
   */
  function fromIntToCode (entier) {
    if (typeof entier !== 'number') throw new TypeError('fromIntToCode n’accepte que des entiers')
    if (entier !== Math.floor(entier)) throw new TypeError('fromIntToCode n’accepte pas les décimaux (que des entiers)')
    if (entier < 0) throw new RangeError('fromIntToCode n’accepte que des entiers positifs')
    if (entier > maxInt) throw new RangeError('fromIntToCode n’accepte que des entiers inférieurs à ' + maxInt)
    let code = ''
    while (entier) {
      code = String.fromCharCode(65 + entier % 26) + code
      entier = Math.floor(entier / 26)
    }
    return _.padStart(code, 5, 'A')
  }

  /**
   * Transforme un code en entier
   * Cf. test/sesalab-api/source/tools/crypto/index.test.js
   *
   * @param {string} code Code
   * @return {Number}
   * @memberof $crypto
   */
  function fromCodeToInt (code) {
    code = cleanCode(code)
    let num = 0
    _.forEach(code, (letter, index) => {
      num += (letter.charCodeAt(0) - 65) * Math.pow(26, 4 - index)
    })

    return num
  }

  /**
   * Incrémente le code fourni.
   *
   * @param {string} code
   * @return {string}
   * @throws {RangeError} Si le code généré à atteint la limite 'ZZZZZ'
   * @memberof $crypto
   */
  function incrementCode (code) {
    code = cleanCode(code)
    if (code === 'ZZZZZ') throw new RangeError('Maximum atteint, impossible d’incrémenter davantage')
    return fromIntToCode(fromCodeToInt(code) + 1)
  }

  return {
    hash,
    token,
    fromIntToCode,
    fromCodeToInt,
    incrementCode
  }
})
