/**
 * @module services/$mail
 */
'use strict'

const moment = require('moment')
const nodemailer = require('nodemailer')
const flow = require('an-flow')
const appLog = require('sesalab-api/source/tools/appLog')
const htmlToText = require('html-to-text')
const { hasProp } = require('sesajstools')

const isEmail = require('sesalab-commun/isEmail')
const { MAIL_THROTTLE_DELAY, MAX_MAIL_IN_DELAY } = require('sesalab-commun/constants')

function logIfError (error) {
  if (error) appLog.error(error)
}
// le console.erorr de node affichera aussi data (pas celui d'un navigateur)
const getErrorWithData = (errorMessage, data) => Object.assign(new Error(errorMessage), { data })

/**
 * @Service $mail
 */
app.service('$mail', function ($cache, $settings) {
  let _transport
  let isProcessing = false
  const _queue = []
  let appMail
  let from
  let prefix
  let lastNotification
  let notificationTimer
  let pendingNotifications = []

  const isProd = $settings.get('application.isProd', false)

  /**
   * Défini transport & variables d'après la conf.
   *
   * @param {simpleCallback} cb
   * @memberof $mail
   */
  function setup (cb) {
    if (_transport) return cb() // déjà initialisé
    const appName = $settings.get('application.name', 'Sesalab')
    appMail = $settings.get('application.mail')
    if (!isEmail.validate(appMail)) throw new Error(`L'adresse settings.application.mail ${appMail} n'est pas valide.`)
    const fromMail = $settings.get('application.fromMail')
    if (!isEmail.validate(fromMail)) throw new Error(`L'adresse settings.application.fromMail ${fromMail} n'est pas valide.`)
    const smtp = $settings.get('smtp')
    if (smtp) {
      _transport = nodemailer.createTransport(smtp)
    } else {
      _transport = nodemailer.createTransport({
        sendmail: true,
        path: $settings.get('sendmail', '/usr/sbin/sendmail')
      })
    }
    from = `"${appName}" <${fromMail}>`
    prefix = `[${appName}] `

    // si le cli se termine, ou que l'on restart lassi, on essaie quand même d'envoyer ce qu'on a en attente
    // (ça fonctionnera probablement pas car le délai d'ouverture de socket vers le smtp
    // sera probablement supérieur au délai de grace accordé par lassi au shutdown, mais on tente…)
    lassi.on('shutdown', () => {
      // on les ajoute les notifs dans le log, ce qui ne partira pas en smtp sera au moins là
      if (pendingNotifications.length) {
        for (const notif of pendingNotifications) {
          appLog.error('Notification en attente qui n’a peut-être pas été envoyée au shutdown', JSON.stringify(notif))
        }
        purgePendingNotifications()
      }
      if (_queue.length) {
        for (const mail of _queue) {
          appLog('Mail en attente qui n’a peut-être pas été envoyé au shutdown', JSON.stringify(mail))
        }
        processQueue()
      }
    })

    cb()
  }

  /**
   * Traite les mails en queue.
   *
   * @private
   * @memberof $mail
   */
  function processQueue () {
    if (!_transport) throw new Error('Il faut lancer $mail.setup() avant de l’utiliser')
    if (isProcessing) return
    if (!_queue.length) return
    isProcessing = true

    const mail = _queue.shift()
    formatAndSendMail(mail, (error, messageId) => {
      if (error) appLog.error('Erreur d’envoi de mail', mail, error.stack)
      else appLog('sent', messageId)
      isProcessing = false
      // s'il en reste en queue on les traite tout de suite,
      // c'est au smtp local de gérer les files d'envoi
      if (_queue.length) process.nextTick(processQueue)
    })
  }

  /**
   * Vide les notifications en attente
   * @private
   */
  function purgePendingNotifications () {
    if (pendingNotifications.length) {
      // on prend la première comme sujet / contenu principal
      const data = pendingNotifications.shift()
      // on l'envoie à l'admin
      data.to = appMail
      // on ajoute les précédentes
      for (const notif of pendingNotifications) {
        data.text += `
Date: ${notif.date}
Subject: ${notif.subject}
Content: ${notif.text}
`
      }
      pendingNotifications = []
      clearTimeout(notificationTimer)
      sendMail(data)
    }
  }

  /**
   * Envoie un mail en html
   * @param {string} to
   * @param {string} subject
   * @param {string|object} template un body html ou un objet template avec une méthode render (un require dust)
   * @param {object} [variables] objet passé à template.render pour construire le body html
   * @param {function} [callback=logIfError] Rappelée dès que le mail est ajouté en file d'attente et que le processing de la file a démarré, ne garanti pas qu'il est parti
   * @memberof $mail
   */
  function send (to, subject, template, variables, callback = logIfError) {
    try {
      if (!to) throw new Error('to obligatoire')
      if (!subject) throw new Error('subject obligatoire')
      const mail = { to, subject }
      if (!template) throw new Error('template obligatoire (peut être un body déjà rendu)')
      if (typeof template === 'string') {
        mail.html = template
      } else {
        if (typeof template.render !== 'function') throw new Error('template invalide')
        mail.template = template
        mail.variables = variables
      }
      _queue.push(mail)
      // On lance le traitement
      processQueue()
      // et répond tout de suite
      callback()
    } catch (error) {
      callback(error, 'avec les données', { to, subject, template, variables })
    }
  }

  /**
   * Envoie un mail, accepte toutes les options qui peuvent être passées à nodemailer
   * @see https://nodemailer.com/message/
   * @param {MailData} data
   * @param {string} [data.goFast] passer true pour utiliser le tampon interne (flag interne)
   *                               (réponse plus rapide mais ne permet pas de récupérer dans la callback
   *                               la réponse de nodemailer),
   * @param {nodemailerCallback} [callback=logIfError] idem nodemailer (sauf si bufferize, dans ce cas elle est rappelée dès que le mail est ajouté en file d'attente et que le processing de la file a démarré)
   * @memberof $mail
   */
  function sendMail (data, callback = logIfError) {
    try {
      if (!data) throw new Error('paramètres obligatoire')
      if (!data.to) throw new Error('to obligatoire')
      if (!data.subject) throw new Error('subject obligatoire')
      if (!data.html && !data.text) throw new Error('html ou text obligatoire')
      if (data.goFast) {
        delete data.goFast
        _queue.push(data)
        processQueue()
        callback()
      } else {
        formatAndSendMail(data, callback)
      }
    } catch (error) {
      callback(error)
    }
  }

  /**
   * Objet à passer à nos fonctions pour envoyer un mail
   * Vous pouvez ajouter toutes les propriétés gérées par nodemailer.
   *
   * @see https://nodemailer.com/message/
   * @typedef MailData
   * @property {string} subject
   * @property {string} text Le body
   * @property {string} [html] Le body html éventuel
   * @property {string} [from] Ignoré si fourni (imposé par la config de l'appli)
   * @property {string} [sender] à utiliser à la place de from pour préciser que l'on envoie de la part de qqun
   * @property {string[]} [attachments] @see https://nodemailer.com/message/attachments/
   * @property {string} [bcc]
   * @property {string} [cc]
   * @memberof $mail
   */

  /**
   * Wrapper de sendMail pour envoyer une notification par mail à settings.application.mail, avec
   * - to imposé
   * - Max une notif par 5min (par instance de lassi), les supplémentaires seront différées
   *   (dans la limite de 50 en attente, les suivantes mises en console.error)
   * - En cas de data invalide, rappelle callback avec l'erreur (ou log) mais ne throw pas.
   *
   * @param {Error|MailData} data Les données du mail, non muté, si c'est pas une Error faut les propriétés subject et text
   * @param {boolean} [data.doNotThrottle=false] passer true pour envoyer sans throttle (et être sûr que la callback récupère les infos de nodemailer)
   * @param {nodemailerCallback} [callback=logIfError]
   * @memberof $mail
   */
  function sendNotification (data, callback = logIfError) {
    if (!data) return console.error(new Error('Contenu obligatoire'))
    // pour éviter de muter data
    let d
    // si data ressemble à une Error, on formate sujet & contenu
    if (data.message && data.stack) {
      d = {
        subject: data.message,
        text: data.stack
      }
    } else {
      if (!data.subject) return callback(getErrorWithData('Propriété subject obligatoire', data))
      if (!data.text) return callback(getErrorWithData('Propriété text obligatoire', data))
      d = Object.assign({}, data)
    }

    // on throttle avec un mail toutes les 5min max, sauf avec isImportant
    if (!d.doNotThrottle && lastNotification && moment(lastNotification).add(5, 'minutes').isAfter(Date.now())) {
      // on drop à la 50e notif
      if (pendingNotifications.length < 50) {
        d.date = new Date()
        pendingNotifications.push(d)
        // de fait ça diffère de 5min après le premier mail mis en queue, donc 10min max
        // (si on a eu une notif 4'59" après la précédente), pas grave
        if (!notificationTimer) notificationTimer = setTimeout(purgePendingNotifications, 5 * 60 * 1000)
      } else {
        appLog.error(new Error('Trop de notifications en attente, dropped dans ce log'), d)
      }
      return callback()
    }
    // on envoie tout de suite
    if (hasProp(d, 'doNotThrottle')) delete d.doNotThrottle
    d.to = appMail
    lastNotification = Date.now()
    sendMail(d, callback)
  }

  /**
   * @callback nodemailerCallback
   * @param {Error} error
   * @param {string} messageId
   */
  /**
   * Format an email for NodeMailer
   * @private
   * @param {MailData} mail Mail data
   * @param {nodemailerCallback} callback
   * @memberof $mail
   */
  function formatAndSendMail (mail, callback) {
    const { subject, template, to, variables } = mail
    // on évite de muter mail, mais on doit imposer le from et tout garder…
    const data = Object.assign({}, mail, { from })
    // …sauf template et variables
    if (template) delete data.template
    if (variables) delete data.variables

    if (!isEmail.validate(data.to)) return callback(Error(`L'adresse mail ${data.to} n'est pas valide.`))
    const cacheKey = `nbMailsTo${data.to}`

    flow().seq(function () {
      // check throttle
      $cache.get(cacheKey, this)
    }).seq(function (nbMails) {
      if (nbMails) nbMails++
      else nbMails = 1
      if (nbMails > MAX_MAIL_IN_DELAY && data.to !== appMail) {
        console.error(`On en est à ${nbMails} mails à envoyer à ${data.to} dans un délai de ${MAIL_THROTTLE_DELAY}s`)
        return callback(Error(`Impossible d’envoyer plus de ${MAX_MAIL_IN_DELAY} mails par période de ${MAIL_THROTTLE_DELAY}s`))
      }
      $cache.set(cacheKey, nbMails, MAIL_THROTTLE_DELAY, this)
    }).seq(function () {
      // build du body
      if (template && typeof template.render === 'function') template.render(variables, this)
      else this()
    }).seq(function (bodyFromTemplate) {
      if (bodyFromTemplate) data.html = bodyFromTemplate
      if (!data.html && !data.text) return callback(Error('mail sans contenu'))
      // hors prod on force le to vers appMail
      if (!isProd && data.to !== appMail) {
        data.to = appMail
        const rmq = `PS: Ce mail était destiné à ${to} mais il est envoyé à ${data.to} car on est pas en environnement de production.`
        if (data.html) {
          if (/<\/body>/.test(data.html)) data.html = data.html.replace('</body>', `${rmq}</body>`)
          else data.html += `<p>${rmq}</p>`
        }
        if (data.text) data.text += '\n\n' + rmq
      }
      // on ajoute la partie texte si elle est pas fournie (déduite du html)
      if (data.html && !data.text) data.text = htmlToText.fromString(data.html, { wordwrap: 80, ignoreImage: true })
      // ajout du préfixe dans le sujet
      data.subject = prefix + subject

      _transport.sendMail(data, callback)
    }).catch(callback)
  }

  return {
    setup,
    send,
    sendMail,
    sendNotification
  }
})
