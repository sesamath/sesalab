const dns = require('dns')

/**
 * Retourne une promesse résolue avec true si hostname a une ip (v4 ou v6), avec false sinon (ne rejette jamais)
 * @private
 * @param {string} hostname
 * @return {Promise<boolean>}
 */
const lookupExist = (hostname) => new Promise((resolve) => {
  // https://nodejs.org/dist/latest-v10.x/docs/api/dns.html#dns_dns_lookup_hostname_options_callback
  dns.lookup(hostname, (error, ip) => {
    if (error) return resolve(false)
    if (ip) return resolve(true)
    resolve(false)
  })
})

/**
 * Vérifie que le domaine de l'adresse email est valide (MX déclaré et connu des dns)
 * (on vérifie seulement que l'adresse passe le test /.+@.+\..+/,
 * mais elle devrait passer une regex plus évoluée avant)
 * @param {string} email
 * @return {Promise}
 */
const checkMx = (email) => new Promise((resolve, reject) => {
  if (!email) return reject(Error('Adresse de courriel vide'))
  if (typeof email !== 'string') return reject(Error('Adresse de courriel invalide'))
  const [login, domain] = email.split('@')
  // on ne teste pas vraiment la validité du mail, juste que ça ressemble à un domaine
  if (!login || !domain || !/.+\..+/.test(domain)) return reject(Error('Adresse de courriel invalide'))
  // @see https://nodejs.org/dist/latest-v10.x/docs/api/dns.html#dns_dns_resolvemx_hostname_callback
  dns.resolveMx(domain, (error, records) => {
    if (error) {
      if (error.code === 'ENOTFOUND') return reject(Error(`Aucun serveur de mail déclaré pour ${domain}`))
      // pas normal, pb avec nos dns
      console.error(error)
      return reject(error)
    }
    const servers = []
    records.forEach(r => {
      if (r.exchange) servers.push(r.exchange)
    })
    // devrait jamais arriver (si y'en a pas on aurait dû avoir une erreur ENOTFOUND),
    // mais ça coûte pas cher de le vérifier
    if (!servers.length) return reject(Error(`Aucun serveur de mail déclaré pour ${domain}`))

    // on vérifie que ces domaines ont une entrée A ou AAAA dans les dns (CNAME interdit pour un MX)
    Promise.all(servers.map(lookupExist)).then(results => {
      if (results.every(r => !r)) {
        if (servers.length > 1) return reject(Error(`Aucun des serveurs de mail déclarés pour ${domain} n’existe (${servers.join(', ')})`))
        return reject(Error(`Le serveur de mail déclaré pour ${domain} n’existe pas (${servers[0]})`))
      }
      resolve()
    }).catch(reject)
  })
})

module.exports = checkMx
