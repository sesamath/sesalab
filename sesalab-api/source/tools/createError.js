const httpErrors = require('http-errors')

const createError = (status, message, data) => httpErrors(status, message, { userFriendly: true, properties: data })

module.exports = createError
