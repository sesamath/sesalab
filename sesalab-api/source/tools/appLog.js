/**
 * module de log pour écrire dans un fichier infos/warning/error de l'application,
 * celles liées à des incohérences de données, du processing, etc.
 * Les plantages de code devraient continuer à être écrit sur le stderr "normal" via console.error
 * mais ils peuvent aussi être mis dans un autre log (utiliser appLog.getLogger())
 */
const { existsSync, mkdirSync } = require('node:fs')

const config = require('../../../config')
const Logger = require('./Logger')

let logDir = config.appLog?.dir || config.logs?.dir
if (!logDir) {
  throw Error('il manque logs.dir dans la config')
}
if (config.appLog?.addBaseIdSuffix) logDir += '.' + config.application.baseId
if (!existsSync(logDir)) mkdirSync(logDir, { recursive: true, mode: 0o775 })

const loggers = {
  /** @type CallableLogger */
  app: new Logger('app')
}

/**
 * Ajoute un message dans app.log (si logLevel ≥ info, sinon ne fait rien)
 * Nombre d'arguments variables
 */
const appLog = loggers.app

// et on lui ajoute une méthode getLogger (à cette instance)
/**
 * Retourne un nouveau logger (le même si on avait déjà demandé un logger avec ce nom)
 * @param {string} name
 * @param {Object} [options]
 * @param {string} [options.logLevel=warning]
 * @return {Logger}
 */
appLog.getLogger = (name, options) => {
  if (!loggers[name]) {
    loggers[name] = new Logger(name, options)
  }
  return loggers[name]
}

/**
 * Retourne une fct qui va logguer dans {name}.log si appLog était en mode debug
 * au moment l'appel de getDebugLogger (et ne fera rien sinon)
 * La fct retournée n'a pas de méthode, elle est l'équivalent d'un getLogger(name).debug()
 * @param {string} name
 * @return {Function}
 */
appLog.getDebugLogger = (name) => {
  // si le appLog par défaut est en mode debug on retourne un logger
  if (loggers.app.getLogLevel() === 'debug') {
    if (loggers[name]) {
      loggers[name].setLogLevel('debug')
    } else {
      loggers[name] = new Logger(name, { logLevel: 'debug' })
    }
    const logger = loggers[name]
    return logger.debug.bind(logger)
  }
  // sinon on retourne une fct qui ne fait rien
  return () => undefined
}

// et faut fermer les streams sur du process.exit
const exitListener = (code) => {
  for (const [name, logger] of Object.entries(loggers)) {
    try {
      logger.stream?.close()
      delete loggers[name]
    } catch (error) {
      console.error(`Pb à la fermeture du log ${name}`, error)
    }
  }
}
// on le lance sur ces signaux (pas SIGHUP avec un listener faut appeler ensuite soi-même process.exit())
for (const signal of ['exit', 'SIGINT', 'SIGTERM']) {
  process.on(signal, exitListener)
}

module.exports = appLog
