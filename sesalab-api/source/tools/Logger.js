/**
 * module de log pour écrire dans un fichier infos/warning/error de l'application,
 * celles liées à des incohérences de données, du processing, etc.
 * Les plantages de code devraient continuer à être écrit sur le stderr "normal" via console.error
 * mais ils peuvent aussi être mis dans un autre log (utiliser appLog.getLogger())
 */
const { createWriteStream, existsSync, mkdirSync } = require('node:fs')
const { join } = require('node:path')

const { formatDateTime } = require('sesajs-date')
const { stringify } = require('sesajstools')

const config = require('../../../config')
let logDir = config.appLog?.dir || config.logs?.dir
if (!logDir) {
  throw Error('il manque logs.dir dans la config')
}
if (config.appLog?.addBaseIdSuffix) logDir += '.' + config.application.baseId
if (!existsSync(logDir)) mkdirSync(logDir, { recursive: true, mode: 0o775 })

const defaultLogLevel = config.appLog?.logLevel ?? 'warning'

// on ajoute à notre préfixe de date l'instance du cluster node, si y'en a un (c'est une string, '0' ou '1' ou …)
// cf https://pm2.keymetrics.io/docs/usage/environment/#specific-environment-variables
const clusterPrefix = globalThis.process?.env?.NODE_APP_INSTANCE ? `[${globalThis.process.env.NODE_APP_INSTANCE}] ` : ''

/**
 * La liste des niveaux de log possible
 * @private
 */
const levels = {
  debug: 0,
  info: 1,
  warn: 2,
  warning: 2,
  error: 3
}

/**
 *
 * @type {StreamOptions}
 */
const streamOptions = {
  flags: 'a',
  mode: 0o644,
  autoClose: true, // pas besoin d'appeler stream.end() au shutdown lassi
  emitClose: false
}

/**
 * @typedef CallableLogger
 * @type Function
 * @mixin Logger
 */

class Logger {
  /**
   * @param {string} name
   * @param {string} logLevel
   * @return {CallableLogger}
   */
  constructor (name, { logLevel = defaultLogLevel } = {}) {
    /**
     * Le logLevel courant, modifiable via setLoglevel
     * @type {number}
     * @private
     */
    this.logLevel = 2
    this.setLogLevel(logLevel)

    if (!/\.log/.test(name)) name += '.log'
    /** @type {PathLike} */
    const path = join(logDir, name)
    /**
     * stream vers le fichier de log
     * @type {WriteStream}
     * @private
     */
    this.stream = createWriteStream(path, streamOptions)
    /**
     * Eventuel stream supplémentaire pour les erreurs seules (error.log)
     * @type {WriteStream|null}
     */
    this.errorStream = null

    // et on retourne une fonction pour qu'un `new Logger()` puisse être appelé sans devoir invoquer sa méthode info
    const callableLogger = this.info.bind(this)
    // faut lui ajouter ses méthodes
    callableLogger.debug = this.debug.bind(this)
    callableLogger.info = this.info.bind(this)
    callableLogger.warn = this.warn.bind(this)
    callableLogger.error = this.error.bind(this)
    callableLogger.getLogLevel = this.getLogLevel.bind(this)
    callableLogger.setLogLevel = this.setLogLevel.bind(this)
    callableLogger.separeErrors = this.separeErrors.bind(this)
    return callableLogger
  }

  /**
   * Écrit dans le log en préfixant par la date (avec ms) et l'éventuel n° de child pm2 (si on tourne dans un cluster node lancé par pm2)
   * @private
   */
  _write (...args) {
    if (!args.length) return console.error(Error('appel de appLog sans argument'))
    if (!args[0]) return console.error(Error('Le premier argument passé à appLog ne doit pas être falsy, arguments reçus : '), ...args)
    if (!this.stream) return // déjà fermé
    const prefix = `[${formatDateTime()}] ${clusterPrefix}`
    const content = args
      .map(arg => {
        if (arg instanceof Error) {
          // on évite de logguer plusieurs fois la même stack (au cas où qqun log l'erreur puis la re-throw)
          if (arg.logged) {
            return arg.message + ' (stack already logged)'
          }
          let message = arg.message + ' ' + arg.stack
          let prefix = '\n    '
          while (arg?.cause instanceof Error) {
            prefix += '    '
            // rencontré un cas où error.cause est une Error mais avec stack undefined
            message += `${prefix}cause : ${arg.cause.message} ${arg.cause.stack?.replace(/\n/g, prefix)}`
            arg = arg.cause
          }
          if (arg.cause) {
            // une cause qui n'est pas une Error !
            message += `${prefix}cause : ${arg.cause} (not an Error)`
          }
          return message
        }
        // pour le reste on rend ça lisible
        return typeof arg === 'object' ? stringify(arg) : arg
      })
      .join(' ')
    this.stream.write(prefix + content + '\n')
    if (this.errorStream && args[0] === 'ERROR') {
      this.errorStream.write(prefix + content + '\n')
    }
    // et on marque les erreurs comme loggées (on pouvait pas le faire dans le map sinon ça log aussi cette propriété)
    for (const arg of args) {
      if (arg instanceof Error) arg.logged = true
    }
  }

  /**
   * Ajoute un message de debug dans app.log (si logLevel ≥ debug, sinon ne fait rien)
   * Nombre d'arguments variables
   */
  debug (...args) {
    if (this.logLevel > 0) return
    args.unshift('DEBUG')
    this._write(...args)
  }

  /**
   * Ajoute un message dans app.log (si logLevel ≥ info, sinon ne fait rien)
   * Nombre d'arguments variables
   */
  info () {
    if (this.logLevel > 1) return
    this._write(...arguments)
  }

  /**
   * Ajoute un message de warning dans app.log (si logLevel ≥ warning, sinon ne fait rien)
   * Nombre d'arguments variables
   */
  warn (...args) {
    if (this.logLevel > 2) return
    args.unshift('WARNING')
    this._write.apply(this, args)
  }

  /**
   * Ajoute un message d'erreur dans app.log
   * Nombre d'arguments variables
   */
  error (...args) {
    args.unshift('ERROR')
    this._write.apply(this, args)
  }

  /**
   * Retourne le logLevel courant (la string, pas la valeur numérique)
   * @return {string}
   */
  getLogLevel () {
    // si le find plante c'est qu'il y a un bug dans setLogLevel !
    return Object.entries(levels)
      .find(([name, value]) => value === this.logLevel)[0]
  }

  /**
   * Change le logLevel courant
   * @param {string} level passer debug|info|warning|error
   */
  setLogLevel (level) {
    if (level in levels) {
      this.logLevel = levels[level]
    } else {
      console.error(Error(`logLevel ${level} invalide`))
    }
  }

  /**
   * Démarre ou stoppe le error.log
   * @param {boolean} withErrorLog passer true pour ajouter un error.log séparé (les erreurs seront toujours dans le log ordinaire également), false pour arrêter d'y écrire
   */
  separeErrors (withErrorLog) {
    if (withErrorLog) {
      if (!this.errorStream) {
        this.errorStream = createWriteStream(this.stream.path.replace('.log', '.error.log'))
      }
    } else if (this.errorStream) {
      this.errorStream.close()
      this.errorStream = null
    }
  }
}

module.exports = Logger
