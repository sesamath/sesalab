module.exports = {
  /**
   * Retourne la liste des oids avec séparateur, (par ordre alphabétique d'oid).
   * @param {Utilisateur[]|string[]} eleves
   * @return {string}
   */
  createIndexFromEleves: (eleves) => eleves.map(e => e.oid || e).sort().join(',')
}
