/**
 * @module services/$resultat
 */
const flow = require('an-flow')
const _ = require('lodash')
const moment = require('moment-timezone')
const { createIndexFromEleves } = require('./helpers')
const { MAX_RESULTATS_CSV } = require('sesalab-commun/constants.js')

// le nb max de séquences retourné par getSequences
const MAX_NB_SEQ = 100

/**
 * @Service Resultat
 *
 * @param Resultat L'entité "Resultat"
 */
app.service('$resultat', function (Resultat, Sequence, $settings, $utilisateur) {
  const maxBilans = $settings.get('application.maxBilans', 3000)
  const maxResults = $settings.get('application.maxResults', 500)

  /**
   * @callback resultatsCallback
   * @param {Error} [error]
   * @param {Resultat[]} resultats
   */

  /**
   * Fetch les résultats appartenant aux utilisateurs donnés
   * @param {string[]} usersIds Tableau contenant les identifiants des utilisateurs
   * @param {Object} options
   * @param {boolean} [options.includeDeleted] Passer true si on veut aussi les deleted
   * @param {boolean} [options.limit=maxResult] La limite du nb de résultats renvoyés, maxResult est fixé en configuration
   * @param {resultatsCallback} callback Avec la liste des résultats des users demandés
   * @throws {Error} Si options.limit > MAX_RESULTATS_CSV
   * @memberof $resultat
   */
  function forUsers (usersIds, options, callback) {
    function grab (skip) {
      const query = Resultat.match('eleve').in(usersIds)
      if (options && options.includeDeleted) query.includeDeleted()
      const nbMissing = allLimit - allResultats.length
      if (nbMissing < limit) limit = nbMissing
      query.grab({ limit, skip }, (error, resultats) => {
        if (error) return callback(error)
        allResultats = allResultats.concat(resultats)
        if (resultats.length === limit && allResultats.length < allLimit) grab(skip + limit)
        else callback(null, allResultats)
      })
    }

    if (!usersIds.length) return callback(null, [])
    let allResultats = []
    // la limite du nb de résultats renvoyés
    const allLimit = options.limit || maxResults
    if (allLimit > MAX_RESULTATS_CSV) throw Error(`On ne peut pas récupérer plus de ${MAX_RESULTATS_CSV} résultats en une fois`)
    // la limite par batch, qui pourra être ajustée au dernier batch
    let limit = Math.min(allLimit, maxResults)
    grab(0)
  }

  /**
   * Ajoute aux résultats des infos propres à la séquence liée (infos préfixées par $ pour ne pas les stocker en base)
   * - $indexRessource La position de la ressource dans la série (démarre à 1, dernière série où la ressource apparaît si elle est dans plusieurs)
   * - $indexSousSequence la position de la sous-séquence dans la séquence
   * - $reachLast True si c'est la dernière ressource de la série
   * - $sousSequenceName Le nom de la sous-séquence
   *
   * @param {Sequence} sequence  Séquence liée au résultat
   * @param {Resultat[]}  resultats Tableau de résultats à traiter
   * @memberof $resultat
   */
  function addSequenceInfoToResultats (sequence, resultats) {
    // On assigne un $indexRessource sur le résultat qui identifie la position de la ressource dans la sous-séquence,
    // et un $indexSousSequence qui identifie la position de la sous-séquence
    const indexRessoureByRid = {}
    const indexSousSequenceByRid = {}
    // On assigne un $sousSequence sur le résultat qui identifie la sous-séquence liée à la ressource
    const ssSeqByRid = {}
    // On note le rid de la dernière ressource;
    let lastRessourceRid
    for (const [indexSousSequence, ssSeq] of sequence.sousSequences.entries()) {
      for (const [indexRessource, ressource] of ssSeq.serie.entries()) {
        // @todo ajouter des uuids aux ssSeq et les mettre dans les résultats, car impossible de différencier une même ressource placée dans deux sous-séquences
        indexRessoureByRid[ressource.rid] = indexRessource
        indexSousSequenceByRid[ressource.rid] = indexSousSequence
        ssSeqByRid[ressource.rid] = ssSeq
        lastRessourceRid = ressource.rid
      }
    }

    for (const resultat of resultats) {
      resultat.$indexRessource = indexRessoureByRid[resultat.ressource.rid] + 1 // on commence à 1;
      resultat.$indexSousSequence = indexSousSequenceByRid[resultat.ressource.rid] + 1
      resultat.$reachLast = lastRessourceRid === resultat.ressource.rid
      resultat.$sousSequenceName = ssSeqByRid[resultat.ressource.rid]
        ? ssSeqByRid[resultat.ressource.rid].nom
        : 'La ressource n’est plus dans la séquence'
    }
  }

  /**
   * @typedef ressourceState
   * @type Object
   * @property {string} rid
   * @property {number} nbEssais
   * @property {number} pourcentageSucces
   * @property {boolean} fin
   */
  /**
   * @callback seanceDataCallback
   * @param {Error|null} error
   * @param {{sequence: string, eleve: string, ressources: ressourceState[]}}
   */

  /**
   * Retourne les données d'une séance construites d'après tous les résultats de cette paire séquence-élève
   * (résultats à plusieurs omis)
   * @param {string} sequenceOid
   * @param {string} eleveOid
   * @param {seanceDataCallback} callback
   * @memberOf $resultat
   */
  function buildSeanceIndivData (sequenceOid, eleveOid, callback) {
    let lastRid
    let dernierVisionnage
    // pour stocker les résultats par rid de ressource (dans cette séquence)
    const statesByRid = {}

    // retourne un state de ressource par défaut
    const getEmptyState = (rid) => ({
      rid,
      nbEssais: 0,
      pourcentageSucces: 0,
      fin: false
    })

    // traite le résultat pour l'ajouter à statesByRid
    const parseResultat = (resultat, next) => {
      const { rid } = resultat.ressource
      if (lastRid !== rid) {
        statesByRid[rid] = getEmptyState(rid)
        lastRid = rid
      }
      const s = statesByRid[rid]
      s.nbEssais++
      if (resultat.fin && resultat.score * 100 > s.pourcentageSucces) s.pourcentageSucces = Math.round(resultat.score * 100)
      if (resultat.fin) s.fin = true
      // et le dernier visionnage, qui sera la date du dernier résultat enregistré
      if (!dernierVisionnage || resultat.date > dernierVisionnage) dernierVisionnage = resultat.date
      next()
    }

    flow().seq(function () {
      Resultat
        .match('sequence').equals(sequenceOid)
        // attention au s de eleves (index concaténé) => on ne veut que les résultats individuels
        .match('eleves').equals(eleveOid)
        .sort('ressource')
        .forEachEntity(parseResultat, this)
    }).seq(function () {
      const seanceData = {
        sequence: sequenceOid,
        eleve: eleveOid,
        dernierVisionnage, // undefined si y'avait pas de résultat
        ressources: Object.values(statesByRid)
      }
      callback(null, seanceData)
    }).catch(callback)
  }

  /**
   * Retourne le résultat avec les dates obfusquées.
   * @param {string} timezone Timezone de la structure
   * @param {Object} resultat Résultat à traiter
   * @memberof $resultat
   */
  function getResultatWithPrivacyDate (timezone, resultat) {
    const resultatDate = resultat.date
    return {
      ...resultat,
      $date: moment.tz(resultatDate, timezone),
      date: $utilisateur.formatDateWithPrivacy(resultatDate, timezone, true),
      csvDate: $utilisateur.formatDateWithPrivacy(resultatDate, timezone, false)
    }
  }

  /**
   * Lecture des résultats pour un groupe d'élèves.
   *
   * @param {Utilisateur[]|string[]} eleves La liste des élèves
   * @param {string}   timezone Timezone de la structure
   * @param {boolean}  autonomie Résultats en autonomie uniquement ou non
   * @param {number}  limit    Entier pour limiter le nombre de résultats récupérés
   * @param {resultatsCallback} callback
   * @memberof $resultat
   */
  function getResultatsPourEleves (eleves, timezone, autonomie, limit, callback) {
    // Si un élève seul demande ses résultats, il faut renvoyer ses résultats seuls OU en groupe, on va donc matcher sur l'index eleve :
    // il y a une ligne d'index élève (au singulier) par participant
    // Si c'est un groupe d'élèves, on ne cherche que les résultats qui correspondent exactement à ce groupe, on demande donc l'index éleves
    function grab (skip, allResultats, cb) {
      const matcher = eleves.length > 1 ? 'eleves' : 'eleve'
      const query = Resultat.match(matcher).equals(createIndexFromEleves(eleves))
      // on ajoute le filtre facultatif
      if (autonomie) query.match('autonomie').true()
      // reste à trier et récupérer
      query
        .sort('date', 'desc')
        .grab({ limit: limitByGrab, skip }, (error, resultats) => {
          if (error) return cb(error)
          allResultats = allResultats.concat(resultats)
          if (resultats.length < limitByGrab || (limit && allResultats.length >= limit)) return cb(null, allResultats)
          // on regarde s'il faut baisser la limite pour le dernier tour
          if (limit && skip + limitByGrab > limit) limitByGrab = limit - skip
          grab(skip + limitByGrab, allResultats, cb)
        })
    }

    let resultats
    let limitByGrab = (limit && limit < maxResults) || maxResults

    flow().seq(function () {
      grab(0, [], this)
    }).seq(function (_resultats) {
      resultats = _resultats
      // attention, les résultats du travail en autonomie n'ont pas de sequence
      const sequenceOids = _.uniq(resultats.map(r => r.sequence).filter(Boolean))
      if (!sequenceOids.length) return this(null, [])
      Sequence
        .match('oid').in(sequenceOids)
        .grab(this)
    }).seq(function (sequences) {
      const sequencesByOid = {}
      sequences.forEach((sequence) => {
        sequencesByOid[sequence.oid] = sequence
        const resultatsForSequence = resultats.filter(r => r.sequence === sequence.oid)
        addSequenceInfoToResultats(sequence, resultatsForSequence)
      })

      resultats = resultats.map((resultat) => getResultatWithPrivacyDate(timezone, resultat))

      callback(null, { resultats, sequencesByOid })
    }).catch(callback)
  }

  /**
   * Retourne tous les résultats d'une séquence
   *
   * @param {Utilisateur[]} eleves   Tableau pour limiter à cette liste d'élèves (laisser vide pour ne pas limiter)
   * @param {Sequence}   sequence Séquence liée au résultat
   * @param {string}   timezone Timezone de la structure
   * @param {resultatsCallback} callback
   * @memberof $resultat
   */
  function getResultatsPourSequence (eleves, sequence, timezone, callback) {
    function grab (skip) {
      const query = Resultat
        .match('sequence').equals(sequence.oid)
        .match('autonomie').false()
        .sort('date', 'desc')
      if (eleves) {
        // Un élève verra ses résultats et ceux qu'il a effectué en groupe
        // Un groupe d'élève ne verra que ses résultats avec le même groupe
        const eleveSelector = eleves.length > 1 ? 'eleves' : 'eleve'
        query.match(eleveSelector).equals(createIndexFromEleves(eleves))
      }
      query.grab({ limit: maxResults, skip }, (error, resultats) => {
        if (error) return callback(error)
        // Modification des données de chaque résultat
        addSequenceInfoToResultats(sequence, resultats)
        for (const resultat of resultats) {
          allResultats.push(getResultatWithPrivacyDate(timezone, resultat))
        }
        if (resultats.length === maxResults && allResultats.length < maxBilans) grab(skip + maxResults)
        else callback(null, allResultats)
      })
    }

    const allResultats = []
    grab(0)
  }

  /**
   * @callback CallbackSequences
   * @param {Error|null} error
   * @param {Sequence[]} sequences
   */
  /**
   * Récupère les oid des séquences ayant des résultats pour des élèves donnés, hors séquences déjà connues
   * @param {string[]} eleves Les oids des élèves concernés
   * @param {string[]} excludedSequencesOids Les séquences déjà connues à exclure
   * @param {CallbackSequences} callback
   * @memberOf $resultat
   */
  function getSequences (eleves, excludedSequencesOids, callback) {
    // on ne veut que les oid de séquences des résultats de ces élèves
    const db = Resultat.getDb()
    // https://docs.mongodb.com/manual/aggregation/
    // http://mongodb.github.io/node-mongodb-native/2.0/api/Collection.html#aggregate
    // http://mongodb.github.io/node-mongodb-native/2.0/api/AggregationCursor.html
    const $match = { autonomie: false }
    if (eleves.length > 1) $match.eleves = createIndexFromEleves(eleves)
    else if (eleves.length === 1) $match.eleve = eleves[0]
    else return callback(Error('Aucun élève'))

    // https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#aggregation-expressions
    if (excludedSequencesOids.length) $match.sequence = { $nin: excludedSequencesOids }

    const pipeline = [
      { $match },
      { $group: { _id: '$sequence' } },
      { $limit: MAX_NB_SEQ }
    ]

    db.collection('Resultat')
      .aggregate(pipeline, { allowDiskUse: true })
      .toArray()
      .then(results => {
        if (!results.length) return callback(null, [])
        const oids = results.map(r => r._id)
        Sequence.match('oid').in(oids).grab(callback)
      })
      .catch(callback)
  }

  /**
   * Supprime tous les résultats où l'élève était seul (mais le laisse dans les résultats qu'il a eu à plusieurs,
   * pour conserver l'info "à plusieurs" dans les bilans qui seront affichés)
   * @param {string} eleveOid
   * @param callback
   */
  function purgeEleve (eleveOid, callback) {
    flow().seq(function () {
      // on purge tous les résultats où il est seul (index eleves et pas eleve)
      Resultat.match('eleves').equals(eleveOid).purge(this)
    }).done(callback)
  }

  /**
   * Purge les résultats associés à la séquence donnée
   * @param {string} sequenceOid
   * @param callback Callback
   * @memberof $resultat
   */
  function purgeSequence (sequenceOid, callback) {
    Resultat.match('sequence').equals(sequenceOid).purge(callback)
  }

  return {
    purgeSequence,
    forUsers,
    addSequenceInfoToResultats,
    getResultatWithPrivacyDate,
    buildSeanceIndivData,
    getSequences,
    getResultatsPourEleves,
    getResultatsPourSequence,
    purgeEleve,
    MAX_NB_SEQ
  }
})
