/**
 * @module controllers/resultat
 */
const flow = require('an-flow')
const _ = require('lodash')
const { CSV_SEPARATOR, MAX_RESULTATS_CSV } = require('sesalab-commun/constants.js')
const { formatCsvCell, formatStringForFilename } = require('sesalab-commun/formatString.js')
const resultChecksum = require('sesalab-commun/resultChecksum')

const { stringify } = require('sesajstools')
const { getLabel } = require('sesatheque-client/dist/server/config')
const formatters = require('sesatheque-client/dist/server/resultatFormatters')

const { createIndexFromEleves } = require('./helpers')
const appLog = require('../tools/appLog')

/**
 * @controller Resultat
 */
app.controller(function (Groupe, Resultat, Seance, Sequence, Utilisateur, $resultat, $seance, $sequence, $settings, $session, $structure) {
  const maxBilans = $settings.get('application.maxBilans', 3000)

  function getDeletedEleveData (oid) {
    return {
      oid,
      nom: 'Supprimé',
      prenom: 'Elève'
    }
  }

  /**
   * Retourne tous les oids de tous les participants à ces résultats
   * @private
   * @param {Resultat[]} resultats
   * @return {string[]}
   */
  function getAllParticipantsOids (resultats) {
    if (!resultats.length) return []
    const ids = new Set()
    resultats.forEach(({ participants }) => {
      participants.forEach(p => ids.add(p))
    })
    return Array.from(ids)
  }

  /**
   * Retourne le tableau avec tous les ids des participants de tous les résultats
   * @private
   * @param {Resultat[]} resultats
   * @param {Utilisateur[]} eleves
   * @param {string[]} [participantsOids]
   */
  function getElevesByOidForResultats (resultats, eleves, participantsOids) {
    if (!participantsOids) participantsOids = getAllParticipantsOids(resultats)
    // On construit notre 'annuaire' d'élève
    const elevesByOid = {}
    eleves.forEach(({ oid, nom, prenom }) => {
      elevesByOid[oid] = { oid, nom, prenom }
    })
    // On ajoute un "faux élève" avec pour nom "Eleve Supprimé" pour chaque participant disparu en bdd
    // Note: ça ne devrait plus arriver car la suppression d'un élève l'enlève des résultats auxquels il a participé
    participantsOids.forEach((eleveOid) => {
      if (!elevesByOid[eleveOid]) {
        console.error(`L’élève ${eleveOid} n’existe plus dans Utilisateurs mais a encore des résultats`)
        elevesByOid[eleveOid] = getDeletedEleveData(eleveOid)
      }
    })
    return elevesByOid
  }

  /**
   * Construit un objet avec tous les participants de tous les résultats (clé oid de l'élève)
   * @param {Resultat[]} resultats
   * @param cb
   */
  function fetchElevesByOidForResultats (resultats, cb) {
    const participantsOids = getAllParticipantsOids(resultats)
    if (!participantsOids.length) return cb(null, {})

    // On va chercher tous les participants
    Utilisateur.match('oid').in(participantsOids).grab((error, eleves) => {
      if (error) return cb(error)
      const elevesByOid = getElevesByOidForResultats(resultats, eleves, participantsOids)
      cb(null, elevesByOid)
    })
  }

  // C'est très majoritairement Classe en 1re colonne, on remplacera s'il faut mettre "Groupe"
  const CSV_HEADER = ['Classe', 'Élève', 'À plusieurs ?', 'Séquence', 'Sous-séquence', 'Index', 'Type', 'Titre', 'Score', 'Détail', 'Jour', 'Heure', 'Durée (s)', 'Avec', 'Essai']

  /**
   * Retourne une ligne de csv (même si y'a pas de résultat pour l'élève)
   * @private
   * @param groupeNom
   * @param eleve
   * @param resultat
   * @param sequencesByOid
   * @param participantsByOid
   * @return {string}
   */
  function getCsvRow (groupeNom, eleve, resultat, sequencesByOid, participantsByOid) {
    const eleveFullName = eleve.nom + ' ' + eleve.prenom

    // shortcut si on a pas de résultat
    if (!resultat) {
      return formatCsvCell(groupeNom) + CSV_SEPARATOR +
        formatCsvCell(eleveFullName) + CSV_SEPARATOR +
        CSV_SEPARATOR.repeat(CSV_HEADER.length - 3)
    }
    if (!resultat.ressource || !resultat.ressource.type) throw Error(`Résultat invalide : ${stringify(resultat)}`)

    // faut parser
    const { type } = resultat.ressource
    const typeLabel = getLabel(type)
    const autresParticipantsNames = []
    if (resultat.participants.length > 1) {
      resultat.participants.filter((oid) => oid !== eleve.oid).forEach(oid => {
        const name = participantsByOid[oid]
          ? `${participantsByOid[oid].nom} ${participantsByOid[oid].prenom}`
          : 'compte supprimé'
        autresParticipantsNames.push(name)
      })
    }
    const sequence = sequencesByOid[resultat.sequence]
    const txtReponse = formatters[type] ? formatters[type].getTxtReponse(resultat) : ''

    // csvDate contient "jj/mm/YYYY hh:mm" ou "jj/mm/YYYY après 18h" ou "jj/mm/YYYY avant 8h",
    // on veut deux colonnes pour que le tableur puisse formater la colonne jour correctement
    const [jour, heure] = resultat.csvDate.split(' ', 2)

    return [
      groupeNom,
      eleveFullName,
      autresParticipantsNames.length > 1 ? 'Oui' : 'Non',
      sequence ? sequence.nom : 'Autonomie',
      resultat.$sousSequenceName ? resultat.$sousSequenceName : 'Autonomie',
      resultat.$indexRessource || '',
      typeLabel,
      resultat.ressource.titre,
      resultat.score,
      txtReponse,
      jour,
      heure,
      resultat.duree,
      autresParticipantsNames.join(' et '),
      resultat.essai
    ].map(formatCsvCell).join(CSV_SEPARATOR)
  }

  /**
   * @callback resultat~rowBuilder
   * @param {Object} eleve
   * @param {Object} resultat
   */
  /**
   * Construit le csv à partir d'une liste d'élèves et de résultats (avec au moins une ligne par élève)
   * @private
   * @param {Object[]} resultats
   * @param {Object[]} eleves
   * @param {string[]} header Tableau d'en-têtes du CSV
   * @param {resultat~rowBuilder} rowBuilder construit la ligne du CSV pour cet élève et ce résultat
   * @param {boolean} isClasse pour déterminer le titre de la 1re colonne, Classe|Groupe
   * @return {string[]} Les lignes du csv
   */
  function buildCsvRowsForResultats (resultats, eleves, rowBuilder, isClasse) {
    const rows = ['"' + CSV_HEADER.join('"' + CSV_SEPARATOR + '"') + '"']
    if (!isClasse) rows[0] = rows[0].replace('Classe', 'Groupe')

    // On trie les élèves alphabétiquement
    eleves = _.sortBy(eleves, (e) => {
      return e.nom.toLowerCase() + e.prenom.toLowerCase()
    })
    eleves.forEach((eleve) => {
      const resultatsEleve = resultats.filter((r) => r.participants.includes(eleve.oid))
      // si y'a pas de résultat on veut quand même une ligne pour cet élève
      if (!resultatsEleve.length) return rows.push(rowBuilder(eleve, null))

      resultatsEleve.forEach((resultat) => {
        rows.push(rowBuilder(eleve, resultat))
      })
    })

    return rows
  }

  /**
   * @callback csvRowsCallback
   * @param {Error} [error]
   * @param {string[]} csvDataRows
   */
  /**
   * Construit le csv en allant chercher les élèves correspondants à ces résultats
   * (classe en première colonne) utilisé pour séquence/élève
   * @param {Resultat[]} resultats
   * @param {Object} sequencesByOid Les séquences concernées (clé oid)
   * @param {Eleve[]} eleves
   * @param {csvRowsCallback} cb
   */
  function fetchCsvRowsForResultats (resultats, sequencesByOid, eleves, cb) {
    flow().seq(function () {
      fetchElevesByOidForResultats(resultats, this)
    }).seq(function (elevesByOid) {
      const rowBuilder = (eleve, resultat) => getCsvRow(eleve.classeName, eleve, resultat, sequencesByOid, elevesByOid)
      const rows = buildCsvRowsForResultats(resultats, eleves, rowBuilder, true)
      cb(null, rows)
    }).catch(cb)
  }

  /**
   * Retourne le CSV pour les résultats d'un groupe
   * (groupe en première colonne)
   * @param {Array}  resultats         Tableau des résultats
   * @param {Object} groupe            Groupe
   * @param {Object} sequencesByOid    Séquences liées aux résultats obtenus
   * @param {String} timezone          Timezone de la structure
   * @param {Object} elevesDuGroupe    Représente l'ensemble des élèves du groupe
   * @param {Object} participantsByOid Regroupe l'ensemble des élèves ayant participé à l'un des résultats du groupe
   */
  function getCsvDataForResultatsGroupe (resultats, groupe, sequencesByOid, timezone, elevesDuGroupe, participantsByOid) {
    const rowBuilder = (eleve, resultat) => getCsvRow(groupe.nom, eleve, resultat, sequencesByOid, participantsByOid)

    // on récupère d'abord les dates pour éviter de muter le tableau resultats reçu
    resultats = resultats.map(resultat => $resultat.getResultatWithPrivacyDate(timezone, resultat))

    // ça paraît idiot de boucler sur les séquences puis pour chacune reboucler sur tous les résultats
    // mais addSequenceInfoToResultats fait aussi des boucles et il vaut mieux traiter tous les résultats d'une séquence ensemble
    for (const [sequenceOid, sequence] of Object.entries(sequencesByOid)) {
      const resultatsForSequence = resultats.filter(r => r.sequence === sequenceOid)
      $resultat.addSequenceInfoToResultats(sequence, resultatsForSequence)
    }

    return buildCsvRowsForResultats(resultats, elevesDuGroupe, rowBuilder, groupe.isClasse)
  }

  /**
   * Récupère une séquence (rappellera cb avec undefined si elle n'existe pas
   * ou si c'est un prof qui n'a pas de readAccess dessus)
   * @param {Context} context
   * @param {string} sequenceOid
   * @param {sequenceCallback} cb
   */
  function grabSequence (context, sequenceOid, cb) {
    grabSequences(context, [sequenceOid], (error, sequences) => {
      if (error || !sequences.length) return cb(error)
      cb(null, sequences[0])
    })
  }

  /**
   * Récupère les séquences et les filtre (hasReadAccess) si on est formateur
   * @param {Context} context
   * @param {string[]} sequenceOids
   * @param {sequencesCallback} cb
   */
  function grabSequences (context, sequenceOids, cb) {
    if (!Array.isArray(sequenceOids) || !sequenceOids.length) {
      // on plante pas mais ça reste pas très normal
      console.error(new Error('grabSequences appelée sans séquences'))
      return cb(null, [])
    }
    flow()
      .seq(function () {
        Sequence
          .match('oid').in(sequenceOids)
          .grab(this)
      })
      .seq(function (_sequences) {
        if (!_sequences.length) return cb(null, [])

        // Pas de filtre pour les élèves
        if (!$session.isAuthenticatedAsFormateur(context)) return cb(null, _sequences)

        // Filtre pour les formateurs (prend une sequence en argument)
        const readAccessFilter = $sequence.hasReadAccess.bind(null, $session.getCurrentFormateur(context))
        cb(null, _sequences.filter(readAccessFilter))
      })
      .catch(cb)
  }

  /**
   * @typedef bilan~EleveInfo
   * @property {string} classe oid de la classe
   * @property {string} classeName
   * @property {string} nom
   * @property {string} prenom
   * @property {string} oid oid de l'élève
   */
  /**
   * Objet dont chaque propriété est un oid élève et la valeur un bilan~EleveInfo
   * @typedef bilan~ElevesInfos
   * @type {Object<string, bilan~EleveInfo>}
   */
  /**
   * @callback CallbackElevesInfos
   * @param {Error} error
   * @param {bilan~ElevesInfos} elevesInfos
   */
  /**
   * Passe à cb l'objet ElevesInfos construit d'après currentEleves complété avec le nom de la classe
   * @param {Utilisateur[]} currentEleves
   * @param {CallbackElevesInfos} cb
   */
  function fetchElevesInfosPourSequenceEtResultats (currentEleves, cb) {
    const allElevesInfos = {}
    const classesOids = []
    for (const { oid, nom, prenom, classe } of currentEleves) {
      allElevesInfos[oid] = { oid, nom, prenom, classe, classeName: '' }
      if (!classesOids.includes(classe)) classesOids.push(classe)
    }
    // il ne manque que le nom de la classe
    if (!classesOids.length) return cb(null, allElevesInfos)
    Groupe.match('oid').in(classesOids).grab({ fields: ['oid', 'nom'] }, function (error, groupes) {
      if (error) return cb(error)
      if (!groupes?.length) return cb(null, allElevesInfos)
      for (const { oid, nom } of groupes) {
        for (const eleve of currentEleves) {
          if (eleve.classe === oid) {
            allElevesInfos[eleve.oid].classeName = nom
          }
        }
      }
      cb(null, allElevesInfos)
    })
  }

  /**
   * Retourne les infos de tous les élèves de la séquence + ceux présents dans les résultats
   * (qui ont pu être retirés de la séquence depuis) ET mute sequence pour ajouter $participantOids
   * à chaque sous-séquence
   * @param {Sequence} sequence
   * @param {Resultat[]} resultats
   * @param {CallbackElevesInfos} cb
   */
  function fetchElevesInfosPourSequenceEtResultatsAll (sequence, resultats, cb) {
    // liste des élèves trouvés dans les résultats
    const elevesAvecResultatOids = new Set()
    for (const resultat of resultats) {
      if (!resultat?.participants?.length) {
        appLog.error('resultat sans participants', resultat)
        continue
      }
      for (const oid of resultat.participants) {
        elevesAvecResultatOids.add(oid)
      }
    }

    /**
     * Infos des élèves
     * @type {bilan~ElevesInfos}
     */
    const allElevesInfos = {}
    const classesOids = new Set()
    /** @type {string[]} */
    let elevesDeLaSequenceOids

    flow().seq(function () {
      // On a les participants des résultats existants
      // Mais ils ne sont plus forcément dans la séquence…
      // On veut le savoir mais aussi récupérer les élèves sans résultat
      $sequence.getTousLesElevesOids(sequence, this)
    }).seq(function (oids) {
      elevesDeLaSequenceOids = oids
      // union dispo en es2024, pas dans node20…
      // const allElevesOids = (new Set(oids)).union(elevesAvecResultatOids)
      const allElevesOids = new Set([...oids, ...Array.from(elevesAvecResultatOids)])

      // faut aller chercher les infos concernant les élèves (des résultats ou de la séquence)
      // (s'ils ont été mis en corbeille depuis il ne remonteront pas)
      Utilisateur.match('oid').in(Array.from(allElevesOids)).grab(this)
    }).seq(function (eleves) {
      for (const { oid, nom, prenom, classe } of eleves) {
        allElevesInfos[oid] = { oid, nom, prenom, classe }
        classesOids.add(classe)
      }

      // on va chercher les infos de classe
      if (classesOids.size) {
        Groupe.match('oid').in(Array.from(classesOids)).grab({ fields: ['oid', 'nom'] }, this)
      } else {
        this(null, [])
      }
    }).seq(function (classes) {
      // on ajoute classeName à tous nos ElevesInfo, on les trie d'abord par oid
      const classesByOid = {}
      for (const { oid, nom } of classes) {
        classesByOid[oid] = nom
      }
      for (const { oid, classe } of Object.values(allElevesInfos)) {
        if (!classe || !classesByOid[classe]) {
          appLog(`L’élève ${oid} n’a pas de classe`)
          allElevesInfos[oid].classeName = 'Sans classe'
          continue
        }
        allElevesInfos[oid].classeName = classesByOid[classe]
        // On flag les élèves actuellement présents dans la séquence
        allElevesInfos[oid].$dansLaSequence = elevesDeLaSequenceOids.includes(oid)
      }

      // et on envoie
      cb(null, allElevesInfos)
    }).catch(cb)
  }

  /**
   * Helper pour envoyer le fichier csv
   * @param {Context} context
   * @param {string} filenamePrefix Sera nettoyée avant d'ajouter _utf8.csv pour le nom de fichier
   * @param {string[]} dataRows Les lignes du csv
   */
  const sendCsv = (context, filenamePrefix, dataRows) => {
    const filename = formatStringForFilename(`${filenamePrefix}_utf8.csv`)
    const headers = {
      'Content-Disposition': `attachment; filename=${filename}`
    }
    context.contentType = 'application/csv'
    context.raw(dataRows.join('\n'), { headers })
  }

  /**
   * Sauvegarde un résultat pour une séance ou un exercice en autonomie.
   *
   * @name ResultatSauvegarde
   * @route {POST} api/resultat/sauvegarde
   * @authentication Cette route nécessite un utilisateur connecté
   * @bodyparam {object} ressource Une ressource
   * @bodyparam {Sequence} sequence Une séquence
   */
  this.post('api/resultat/sauvegarde', function (context) {
    // pour tester la gestion de l'erreur coté client, on retourne une erreur dans ~50% des cas
    // if (Math.random() < 0.5) return context.next(new Error('fake Error'))

    // pour tracer des pbs d'enregistrements on ajoute une écriture dans le log d'erreur si on a pas répondu dans les temps
    const startingTs = Date.now()
    const logTimeoutMs = 1000 // en ms
    let logMsg = `Le traitement de POST api/resultat/sauvegarde a pris plus de ${logTimeoutMs}ms (starting at ${startingTs} pour ${context.request.ip})`
    const logTimeoutId = setTimeout(function () {
      console.error(logMsg, 'avec le résultat', resultat)
    }, logTimeoutMs)

    const currentEleves = $session.getCurrentEleves(context)
    if (!currentEleves) {
      console.error('Pas d’élève en session sur un POST api/resultat/sauvegarde')
      context.setStatus(403)
      return context.restKo('Session invalide, vous devez vous reconnecter (ou rafraîchir la page si vous êtes connecté différemment dans un autre onglet ou fenêtre)')
    }
    const participantOids = currentEleves.map(e => e.oid)
    const resultat = context.post
    if (!resultat) return context.restKo('Aucun résultat fourni')
    if (!resultat.ressource || !resultat.ressource.rid) return context.restKo('Aucune ressource fournie')

    const { resultatId, sequence } = resultat
    // dans resultat.ressource on ne garde que ces propriétés
    const { rid, type, titre } = resultat.ressource

    // Dans le cas d'un exercice en autonomie, sequence n'est pas renseigné
    const autonomie = !sequence

    const checksum = resultChecksum(rid, participantOids, resultat.score)
    if (checksum !== resultat.checksum) {
      // juste une ligne de log, plus simple pour l'analyse
      console.error(`Failed checksum: received ${resultat.checksum} expected ${checksum} with rid ${rid} users ${participantOids.join(',')} and score ${resultat.score} pour ${context.request.ip}`)
      // le calcul du checksum a changé le 31/01/2023, on commente la ligne suivante pendant 24h
      // FIXME virer la ligne précédente et décommenter la suivante le 2023-02-01
      // return context.accessDenied(`Le checksum ${resultat.checksum} est incorrect (Checksum attendu ${checksum})`)
    }

    let isNewEssai, dbResultat
    flow()
      .seq(function () {
        // On regarde s'il y a déjà un résultat pour l'exercice en cours (identifié entre autre par 'resultatId')
        Resultat
          .match('ressource').equals(rid)
          .match('resultatId').equals(resultatId)
          .match('eleves').equals(createIndexFromEleves(currentEleves))
          // au cas où, mais normalement il ne peut y en avoir qu'un
          .sort('date', 'desc')
          .grabOne(this)
      })
      .seq(function (_dbResultat) {
        resultat.date = new Date() // On écrase la date envoyée par l'élève par celle du serveur
        resultat.participants = participantOids
        resultat.ressource = { rid, type, titre }

        isNewEssai = !_dbResultat
        if (isNewEssai) {
          logMsg += `\npas de résultat précédent, après ${Date.now() - startingTs}ms`
          _dbResultat = Resultat.create(resultat)
        } else {
          logMsg += `\nrecup résultat précédent ok après ${Date.now() - startingTs}ms`
          Object.assign(_dbResultat, resultat)
        }
        // on ne store pas tout de suite car on voudra ajouter la propriété essai plus loin
        dbResultat = _dbResultat
        // sauf si autonomie (pas de séance associée)
        if (autonomie) {
          dbResultat.store(this)
        } else {
          // on va chercher les séances concernées
          Seance
            .match('sequence').equals(sequence)
            .match('eleve').in(participantOids)
            .grab(this)
        }
      })
      .seq(function (seances) {
        if (autonomie) {
          // (seances est le resultat et pas les seances, osef)
          clearTimeout(logTimeoutId)
          context.rest({})
          // et on arrête là
          return
        }

        logMsg += `\nrecup séances ok après ${Date.now() - startingTs}ms`
        // Pour chaque élève on met à jour sa séance (état d'avancement de cette séquence)
        let essai
        flow(participantOids)
          .seqEach(function (eleveOid) {
            // findOrCreateEleveSeanceInArray() crée la séance si elle n'existait pas encore
            // (et l'ajoute au tableau)
            // Il nettoie les doublons le cas échéant (avec ménage en db aussi)
            const seance = $seance.findOrCreateEleveSeanceInArray(seances, eleveOid, rid, sequence)
            seance.dernierVisionnage = new Date()
            // On met à jour le state de cette ressource
            const state = seance.ressources.find(r => r.rid === rid)
            const previousPercent = state.pourcentageSucces || 0
            const newPercent = resultat.fin ? Math.round((100 * resultat.score) || 0) : 0
            state.pourcentageSucces = Math.max(previousPercent, newPercent)
            // On met à jour le status "fini" : si un exercice a été fini il le reste
            state.fin = state.fin || resultat.fin
            // incrément du nb d'essai au premier enregistrement de résultat pour ce resultatId
            // (resultatId qui correspond à une ouverture d'iframe de l'exo)
            if (isNewEssai) {
              if (state.nbEssais > 0) state.nbEssais++
              else state.nbEssais = 1
            } else if (!state.nbEssais) {
              appLog.error('seance sans nbEssais', seance)
              seance.nbEssais = 1
            }
            // quand y'a plusieurs élèves on prend le min (pour ne pas bloquer en cas de maxEssais), on fait la même chose ici
            if (essai == null || state.nbEssais < essai) {
              essai = state.nbEssais
            }
            seance.store(this)
          })
          .seq(function () {
            // on peut ajouter le numero de l'essai au résultat
            dbResultat.essai = essai ?? 1
            dbResultat.store(this)
          })
          .seq(function () {
            // faut passer les séances au step suivant (l'array initial a été modifié dans le forEach précédent)
            this(null, seances)
          })
          .done(this)
      })
      .seq(function (seances) {
        const elapsed = Date.now() - startingTs
        logMsg += `\nmàj seance ok après ${elapsed}ms`
        // on cherche le ressState pour màj de la séance coté client
        const virtualSeance = $seance.buildVirtualSeanceForParticipants(seances, participantOids, sequence)
        const ressState = virtualSeance.ressources.find(r => r.rid === rid)
        logMsg += `\nenregistrement résultat ok après ${elapsed}ms`
        clearTimeout(logTimeoutId)
        context.rest(ressState)
        if (elapsed > logTimeoutMs) {
          logMsg += `\nla réponse a été envoyée après ${elapsed}ms`
          console.error(logMsg, '(fin du log précédent)')
        }
      })
      .catch(context.next)
  })

  /**
   * Exporte l'ensemble des résultats d'un groupe au format CSV.
   *
   * @name ResultatExport
   * @route {GET} api/resultat/export/:groupeId
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string} groupeId Identifiant du {@link Groupe}
   * @param {object} ressource Une ressource
   * @param {Sequence} sequence Une séquence
   */
  this.get('api/resultat/export/:groupeId', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const currentStructureOid = $session.getCurrentStructureOid(context)
    const groupeId = context.arguments.groupeId
    if (!groupeId) return context.restKo('Aucun groupe fourni')

    let timezone
    let groupe
    let elevesDuGroupe
    let resultatsDuGroupe
    let participantsByOid

    flow().seq(function () {
      $structure.grabByOid(currentStructureOid, this)
    }).seq(function (structure) {
      timezone = $structure.getTimezone(structure)
      Groupe.match('oid').equals(groupeId).grabOne(this)
    }).seq(function (_groupe) {
      groupe = _groupe
      if (!groupe) return context.notFound()
      if (!context.peutEditerGroupe(groupe)) return context.accessDenied()
      if (groupe.isClass) {
        Utilisateur.match('classe').equals(groupe.oid).grab(this)
      } else if (groupe.utilisateurs.length) {
        Utilisateur.match('oid').in(groupe.utilisateurs).grab(this)
      } else {
        this(null, [])
      }
    }).seq(function (eleves) {
      elevesDuGroupe = eleves
      const oids = elevesDuGroupe.map(e => e.oid)
      // on limite à 6k résultats
      $resultat.forUsers(oids, { limit: MAX_RESULTATS_CSV }, this)
    }).seq(function (resultats) {
      // @todo vérifier l'affirmation suivante, avec mongo ça parait louche
      // On utilise uniqBy pour éviter les doublons (un résultat fait par deux élèves du groupe est récupéré 2 fois)
      resultatsDuGroupe = _.uniqBy(resultats, 'oid')
      participantsByOid = getElevesByOidForResultats(resultatsDuGroupe, elevesDuGroupe)
      // Les résultats en autonomie n'ont pas de séquence, d'où le filter
      const sequencesOids = resultatsDuGroupe.map(r => r.sequence).filter(oid => oid)
      if (sequencesOids.length) grabSequences(context, sequencesOids, this)
      else this()
    }).seq(function (sequences) {
      const sequencesByOid = _.keyBy(sequences, 'oid') // objet vide si sequences est un array vide
      // Si grabSequences() ne trouve pas la séquence d'un résultat c'est que le formateur n'y a pas accès.
      const resultatsAccessibles = resultatsDuGroupe.filter(({ sequence }) => sequence && sequencesByOid[sequence])
      const dataRows = getCsvDataForResultatsGroupe(resultatsAccessibles, groupe, sequencesByOid, timezone, elevesDuGroupe, participantsByOid)
      sendCsv(context, `bilan_groupe_${groupe.nom}_utf8.csv`, dataRows)
    }).catch(context.next)
  })

  /**
   * Affiche le dernier résultat d'un utilisateur.
   *
   * @name ResultatLast
   * @route {GET} api/resultat/last
   * @authentication Cette route nécessite un utilisateur connecté
   * @param {Sequence} sequence Une séquence
   */
  this.get('api/resultat/last', function (context) {
    // inutile pour les formateurs
    if ($session.isAuthenticatedAsFormateur(context)) return context.rest({})
    // pour les élèves, on a pas la session car on est appelé depuis l'iframe, on passe par un token
    const query = context.get
    if (!query.token) return context.accessDenied()

    let elevesIndex
    const sequenceOid = query.sequence

    flow()
      .seq(function () {
        $session.getDataForReadonlyToken(context, query.token, this)
      })
      .seq(function (readonlyTokenData) {
        if (!readonlyTokenData?.eleves?.length) {
          // console.log(`rien en cache pour le token ${query.token}`, readonlyTokenData)
          return context.rest({})
        }

        elevesIndex = createIndexFromEleves(readonlyTokenData.eleves)

        Resultat
          .match('eleves').equals(elevesIndex)
          .match('sequence').equals(sequenceOid)
          .match('ressource').equals(query.ressource)
          .sort('date', 'desc')
          .grabOne(this)
      })
      .seq(function (resultat) {
        const response = {}
        if (resultat) response.resultat = resultat
        context.rest(response)
      })
      .catch(context.next)
  })

  /**
   * Lecture d'un bilan pour les exercices en autonomie d'un ou de plusieurs élèves.
   *
   * @name ResultatAutonomie
   * @route {GET} api/resultat/autonomie
   * @authentication Cette route nécessite un utilisateur connecté
   */
  this.get('api/resultat/autonomie', function (context) {
    const currentEleves = $session.getCurrentEleves(context)
    if (!currentEleves) return context.accessDenied()

    let timezone
    let resultats

    flow()
      .seq(function () {
        $structure.grabByOid(currentEleves[0].structures[0], this)
      })
      .seq(function (structure) {
        timezone = $structure.getTimezone(structure)
        $resultat.getResultatsPourEleves(currentEleves, timezone, true, maxBilans, this)
      })
      .seq(function (data) {
        resultats = data.resultats
        fetchElevesByOidForResultats(resultats, this)
      })
      .seq(function (eleves) {
        context.rest({
          resultats,
          eleves,
          ressources: {}
        })
      })
      .catch(context.next)
  })

  /**
   * Lecture d'un bilan pour les exercices d'un élève demandé par un formateur.
   *
   * @name ResultatEleve
   * @route {GET} api/resultat/eleve/:eleve
   * @authentication Cette route nécessite un formateur connecté
   * @routeparam {string} eleve Identifiant d'un élève
   */
  this.get('api/resultat/eleve/:eleve', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    if (!context.arguments.eleve) return context.accessDenied()
    const currentStructureOid = $session.getCurrentStructureOid(context)

    let eleve
    let timezone
    let resultats
    let classeName

    flow()
      .seq(function () {
        $structure.grabByOid(currentStructureOid, this)
      })
      .seq(function (structure) {
        timezone = $structure.getTimezone(structure)
        Utilisateur
          .match('oid').equals(context.arguments.eleve)
          .grabOne(this)
      })
      .seq(function (_eleve) {
        eleve = _eleve
        if (!eleve.classe) return this()
        Groupe
          .match('oid').equals(eleve.classe)
          .match('isClass').true()
          .grabOne(this)
      })
      .seq(function (_classe) {
        classeName = _classe ? _classe.nom : 'Sans classe'
        // On vérifie que c'est bien un formateur dans le même établissement que l'élève demandé
        if (!context.isFormateurInOneOfStructures(eleve.structures)) return context.accessDenied()
        $resultat.getResultatsPourEleves([eleve], timezone, false, maxBilans, this)
      })
      .seq(function (data) {
        resultats = data.resultats
        fetchElevesByOidForResultats(resultats, this)
      })
      .seq(function (eleves) {
        // On ajoute le nom de la classe à l'élève concerné seulement
        if (eleves[eleve.oid]) eleves[eleve.oid].classeName = classeName
        context.rest({
          eleves,
          resultats
        })
      })
      .catch(context.next)
  })

  /**
   * Export d'un bilan pour les exercices d'un élève demandé par un formateur.
   *
   * @name ResultatEleveExport
   * @route {GET} api/resultat/export/eleve/:eleveOid
   * @authentication Cette route nécessite un formateur connecté
   * @routeparam {string} eleveOid Identifiant d'un élève
   */
  this.get('api/resultat/export/eleve/:eleveOid', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const { eleveOid } = context.arguments
    // @todo vérifier si la ligne suivante est utile (si la string est vide on devrait pas être appelé)
    if (!eleveOid) return context.restKo('Requête invalide, identifiant manquant')
    const currentStructureOid = $session.getCurrentStructureOid(context)

    let eleve
    let timezone

    flow().seq(function () {
      $structure.grabByOid(currentStructureOid, this)
    }).seq(function (structure) {
      if (!structure) return context.restKo(Error(`La structure en session (${currentStructureOid}) n’existe pas ou plus`))
      timezone = $structure.getTimezone(structure)
      // on va chercher l'élève en imposant la structure
      Utilisateur
        .match('oid').equals(eleveOid)
        .match('structures').equals(currentStructureOid)
        .grabOne(this)
    }).seq(function (_eleve) {
      if (!_eleve) context.restKo(`Aucun élève ${eleveOid} dans la structure courante (${currentStructureOid})`)
      eleve = _eleve
      if (!eleve.classe) return this()
      Groupe
        .match('oid').equals(eleve.classe)
        .match('isClass').true()
        .grabOne(this)
    }).seq(function (classe) {
      eleve.classeName = classe ? classe.nom : 'Sans classe'
      $resultat.getResultatsPourEleves([eleve], timezone, false, null, this)
    }).seq(function (data) {
      fetchCsvRowsForResultats(data.resultats, data.sequencesByOid, [eleve], this)
    }).seq(function (dataRows) {
      sendCsv(context, `bilan_eleve_${eleve.nom}-${eleve.prenom}`, dataRows)
    }).catch(context.next)
  })

  /**
   * Renvoi de tous les bilans d'une séquence
   * @name ResultatSequence
   * @route {GET} api/resultat/sequence/:sequenceOid
   * @authentication Cette route nécessite un formateur connecté
   * @routeparam {string} sequenceOid Identifiant d'une {@link Sequence}
   */
  this.get('api/resultat/sequence/:sequenceOid', function (context) {
    if (!$session.isAuthenticated(context)) return context.accessDenied()
    const currentEleves = $session.getCurrentEleves(context)
    const sequenceOid = context.arguments.sequenceOid
    if (!sequenceOid) return context.restKo('Aucune séquence fournie')
    let sequence
    let resultats

    flow().seq(function () {
      grabSequence(context, sequenceOid, this)
    }).seq(function (_sequence) {
      if (!_sequence) return context.restKo('Cette séquence n’existe plus (ou bien vous n’avez pas la permission de la consulter)')
      sequence = _sequence
      // faut aussi la structure pour récupérer sa timezone (et filtrer les heures dans ce qu'on renvoie)
      $structure.grabByOid(sequence.structure, this)
    }).seq(function (structure) {
      const timezone = $structure.getTimezone(structure)
      // cette méthode renvoie tous les résultats de la séquence (si on est prof)
      // ou seulement ceux qui concernent currentEleves
      $resultat.getResultatsPourSequence(currentEleves, sequence, timezone, this)
    }).seq(function (_resultats) {
      resultats = _resultats
      if (currentEleves) {
        // c'est affiché à un ou des élèves
        fetchElevesInfosPourSequenceEtResultats(currentEleves, this)
      } else {
        // pour le prof, faut ajouter $participantOids aux sous-séquences
        fetchElevesInfosPourSequenceEtResultatsAll(sequence, resultats, this)
      }
    }).seq(function (elevesInfos) {
      context.rest({
        eleves: elevesInfos,
        // on vire les résultats d'élèves supprimés
        resultats: resultats.filter(r => r.participants.some(oid => Boolean(elevesInfos[oid]))),
        // et on ajoute la séquence
        sequence
      })
    }).catch(context.next)
  })

  /**
   * Export CSV pour une séquence.
   *
   * @name ResultatSequenceExport
   * @route {GET} api/resultat/export/sequence/:sequenceOid
   * @authentication Cette route nécessite un utilisateur connecté
   * @routeparam {string} sequenceOid Identifiant d'une {@link Sequence}
   */
  this.get('api/resultat/export/sequence/:sequenceOid', function (context) {
    if (!$session.isAuthenticated(context)) return context.accessDenied()
    const currentEleves = $session.getCurrentEleves(context)
    const sequenceOid = context.arguments.sequenceOid
    if (!sequenceOid) return context.restKo('Aucune séquence fournie')
    let sequence
    let resultats

    flow().seq(function () {
      grabSequence(context, sequenceOid, this)
    }).seq(function (_sequence) {
      if (!_sequence) return context.notFound('Cette séquence n’existe plus (ou bien vous n’avez pas la permission de la consulter)')
      sequence = _sequence
      $structure.grabByOid(sequence.structure, this)
    }).seq(function (structure) {
      $resultat.getResultatsPourSequence(currentEleves, sequence, $structure.getTimezone(structure), this)
    }).seq(function (_resultats) {
      resultats = _resultats
      if (currentEleves) {
        fetchElevesInfosPourSequenceEtResultats(currentEleves, this)
      } else {
        fetchElevesInfosPourSequenceEtResultatsAll(sequence, resultats, this)
      }
    }).seq(function (elevesInfos) {
      const sequencesByOid = { [sequence.oid]: sequence }
      fetchCsvRowsForResultats(resultats, sequencesByOid, elevesInfos, this)
    }).seq(function (dataRows) {
      sendCsv(context, `bilan_sequence_${sequence.nom}`, dataRows)
    }).catch(context.next)
  })
})
