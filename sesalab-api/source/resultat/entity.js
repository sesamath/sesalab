const { createIndexFromEleves } = require('./helpers')
/**
 * Représente un résultat.
 *
 * @Entity Resultat
 * @class Resultat
 * @property {string} oid Identifiant
 * @property {string[]} participants oids des participants
 * @property {object} ressource Quelques props de la ressource
 * @property {string} ressource.rid
 * @property {string} ressource.titre
 * @property {string} ressource.type
 * @property {string} sequence oid de la séquence
 * @property {Number} score Score normalisé entre 0 et 1
 * @property {Date} date Date du résultat
 * @property {Number} duree Durée en secondes
 * @property {boolean} fin Indique si l'utilisateur a terminé l'exercice
 * @property {String} resultatId Timestamp généré à l'ouverture de l'iframe de l'exercice, utilisé pour éventuellement merger différents résultats pendant le déroulement de la séance (on aura donc un résultat en base par ouverture d'iframe)
 */
app.entity('Resultat', function (Backup) {
  this.validateJsonSchema(
    {
      // JSON-Schema
      type: 'object',
      properties: {
        oid: { type: 'string' },
        participants: { type: 'array', items: { type: 'string' } },
        ressource: {
          type: 'object',
          properties: {
            rid: { type: 'string' },
            titre: { type: 'string' },
            type: { type: ['string'] }
          }
        },
        sequence: { type: ['string', 'null'] },
        score: { type: 'number' },
        date: { instanceof: 'Date' },
        duree: { type: 'number' },
        fin: { type: 'boolean' },
        // pour distinguer les appels multiples à saveResultat pour le même exo
        resultatId: { type: 'string' },
        // ajouté le 2025-01-24 pour l'afficher dans les bilans
        essai: { type: 'number' }
      },
      required: ['participants', 'ressource'],
      additionalProperties: true // l'objet peut contenir d'autres attribus ajoutés par la sésathèque, le contenu par ex (avec une figure, une réponse qualitative, etc.)
    }
  )
  // Attention, cet index "eleves" est une seule string des oid compactés (séparateur virgule)
  this.defineIndex('eleves', 'string', function () {
    return createIndexFromEleves(this.participants)
  })
  // et ici on a bien une indexation habituelle, les oid un par un
  this.defineIndex('eleve', 'string', function () {
    return this.participants
  })
  this.defineIndex('autonomie', 'boolean')
  this.defineIndex('sequence', 'string')
  this.defineIndex('date', 'date')
  this.defineIndex('ressource', 'string', function () {
    return this.ressource.rid
  })
  this.defineIndex('resultatId', 'string')

  this.beforeDelete(function (cb) {
    Backup.create({ id: `Resultat-${this.oid}`, entity: this }).store(cb)
  })

  this.beforeStore(function (cb) {
    this.autonomie = !this.sequence
    // On a d'anciens résultats en DB qui ont un resultatId en number.
    // On convertit en string, pour ne pas bloquer une éventuelle réindexation.
    if (typeof this.resultatId === 'number') this.resultatId = String(this.resultatId)
    cb()
  })
})
