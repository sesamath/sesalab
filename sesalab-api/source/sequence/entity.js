const flow = require('an-flow')
const { formatDateTime } = require('sesajs-date')
const constants = require('sesalab-commun/constants')

const ressProps = ['titre', 'type', 'rid', 'aliasRid', 'cle', 'nonZapping', 'minimumReussite', 'maximumVisionnage']

/**
 * @typedef Sequence~sousSequence
 * @type {Object}
 * @property {string} nom
 * @property {number} type Cf constantes SERIE_STATUT_*
 * @property {Number} nonZapping
 * @property {Number} minimumReussite
 * @property {Number} maximumVisionnage
 * @property {Sequence~serie[]} serie
 * @property {Sequence~eleve[]} eleves Un tableau d'élèves ou de groupes/classes
 */

/**
 * @typedef Sequence~serie
 * @type {Object}
 * @property {object} properties Propriétés
 * @property {string} properties.titre Titre de la série
 * @property {string} rid Identifiant de la ressource utilisée par la série
 */

/**
 * @typedef Sequence~eleve
 * @type {Object}
 * @property {string} oid Identifiant d'un {@link Utilisateur} ou d'un {@link Groupe}
 * @property {string} nom Nom d'un {@link Utilisateur} ou d'un {@link Groupe}
 * @property {number} prenom Prénom d'un {@link Utilisateur} ou d'un {@link Groupe}
 * @property {number} type Type d'un {@link Utilisateur} ou d'un {@link Groupe}
 */

/**
 * Représente une séquence, un ensemble d'exercices destinés à des élèves.
 *
 * @Entity Sequence
 * @class Sequence
 * @property {string} oid Identifiant unique
 * @property {string} structure Identifiant de la {@link Structure} possédant la séquence
 * @property {string} owner Identifiant de l'{@link Utilisateur} propriétaire de la séquence
 * @property {string} nom Nom
 * @property {string} description Description visible par les formateurs ayant accès à la séquence
 * @property {string} message Message visible par les élèves ayant accès à la séquence
 * @property {Sequence~sousSequence[]} sousSequences Un tableau de sous-séquences
 * @property {Object} [lastChange] Données concernant la dernière modification
 * @property {Date} lastChange.date Données concernant la dernière modification
 * @property {Object} lastChange.modifier Données concernant l'auteur de la dernière modification
 * @property {string} lastChange.modifier.oid Identifiant de l'utilisateur
 * @property {string} lastChange.modifier.nom Nom de l'utlisateur ayant modifié la séquence
 * @property {string} lastChange.modifier.prenom Prénom de l'utlisateur ayant modifié la séquence
 * @property {Number} type Type de séquence (libre, ordonnée ou ordonnée avec un minimum de réussite)
 * @property {Date} [fromDate] Date à laquelle la séquence deviendra accessible
 * @property {Date} [toDate] Date à laquelle la séquence deviendra inaccessible
 * @property {Date} [fromTime] Heure à laquelle la séquence deviendra accessible
 * @property {Date} [toTime] Heure à laquelle la séquence deviendra inaccessible
 * @property {boolean} prioritaire=false Indique si la séquence est prioritaire sur les autres
 * @property {boolean} lineaire=true Indique si les exercices de la séquence doivent être lancé dans l'ordre établi par l'enseignant
 * @property {boolean} uniqEleve=false Indique si la séquence n'est pas réalisable à plusieurs
 * @property {Number} nonZapping Durée pendant laquelle l'élève ne peut pas changer d'exercice
 * @property {Number} minimumReussite Score minimum nécessaire pour valider la séquence
 * @property {Number} maximumVisionnage Nombre d'essais maximum
 * @property {boolean} public=false Permet de rendre la séquence accessible publiquement à l'ensemble des formateurs
 * @property {boolean} publicAmis=false Permet de rendre la séquence accessible publiquement à l'ensemble des formateurs d'une même structure
 * @property {boolean} editableAmis=false Autorise l'édition de la séquence par les autres formateurs de la structure
 * @property {boolean} hasModel ?
 * @property {string[]} groupes La liste des noms des groupes de partage de cette séquence (ils sont gérés dans la sésathèque)
 */
app.entity('Sequence', function (Backup, Journal) {
  let $resultat, $seance

  this.validateJsonSchema(
    {
      // JSON-Schema
      type: 'object',
      properties: {
        oid: { type: 'string' },
        // @todo à virer quand on aura plus besoin de la référence à labomep1
        externalId: { type: 'string' },
        structure: { type: 'string' },
        owner: { type: 'string' },
        nom: { type: 'string' },
        description: { type: ['string', 'null'] },
        message: { type: 'string' },
        statut: {
          enum: [
            constants.SEANCE_STATUT_INACTIF,
            constants.SEANCE_STATUT_ACTIF,
            constants.SEANCE_STATUT_BORNEE
          ]
        },
        lastChange: {
          type: 'object',
          properties: {
            date: { instanceof: 'Date' },
            modifier: {
              type: 'object',
              properties: {
                oid: { type: 'string' },
                nom: { type: 'string' },
                prenom: { type: ['string', 'null'] }
              }
            }
          }
        },

        // Contraintes
        type: { enum: [constants.SERIE_STATUT_LIBRE, constants.SERIE_STATUT_ORDONNEE, constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE] },
        fromDate: { instanceof: 'Date' },
        toDate: { instanceof: 'Date' },
        fromTime: { type: 'number' },
        toTime: { type: 'number' },
        prioritaire: { type: 'boolean' },
        lineaire: { type: 'boolean' },
        uniqEleve: { type: 'boolean' },
        nonZapping: { type: 'number' },
        minimumReussite: { type: 'number' },
        maximumVisionnage: { type: 'number' },

        // Visibilité, droits et partage
        public: { type: 'boolean' },
        publicAmis: { type: 'boolean' },
        editableAmis: { type: 'boolean' },
        hasModel: { type: 'boolean' },
        groupes: { type: 'array', items: { type: 'string' } },

        sousSequences: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              nom: { type: 'string' },
              type: {
                enum: [
                  constants.SERIE_STATUT_LIBRE,
                  constants.SERIE_STATUT_ORDONNEE,
                  constants.SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE
                ]
              },
              nonZapping: { type: 'number' },
              minimumReussite: { type: 'number' },
              maximumVisionnage: { type: 'number' },
              serie: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    // ATTENTION à mettre à jour la constante ressProps si ça change
                    titre: { type: 'string', minLength: 1 },
                    type: { type: 'string', minLength: 1 },
                    rid: { type: 'string', minLength: 1 },
                    aliasOf: { type: 'string', minLength: 1 },
                    aliasRid: { type: 'string', minLength: 1 },
                    cle: { type: 'string', minLength: 1 },
                    nonZapping: { type: 'number' },
                    minimumReussite: { type: 'number' },
                    maximumVisionnage: { type: 'number' }
                  },
                  additionalProperties: false,
                  required: ['titre', 'type', 'rid']
                }
              },
              eleves: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    oid: { type: 'string' },
                    nom: { type: 'string' },
                    prenom: { type: 'string' },
                    type: { enum: ['eleve', 'groupe'] }
                  }
                }
              }
            }
          }
        }
      },
      required: ['owner', 'nom']
    }
  )

  this.defaults(function () {
    this.sousSequences = []
    this.publicAmis = false
  })

  /**
   * string de la forme groupe:oid ou eleve:oid
   */
  this.defineIndex('eleve', 'string', function () {
    if (!this.sousSequences) return
    const elevesOrGroupes = []

    this.sousSequences.forEach(ssSeq => {
      if (!ssSeq || !ssSeq.eleves || !ssSeq.eleves.length) return
      ssSeq.eleves.forEach(item => {
        //  deepcode ignore ForEachReturns: <return as break>
        if (!item) return console.error(`Sous-sequence avec un élément eleves falsy (${this.oid || this.nom})`)
        const eleveOrGroupe = `${item.type}:${item.oid}`
        if (!elevesOrGroupes.includes(eleveOrGroupe)) elevesOrGroupes.push(eleveOrGroupe)
      })
    })

    return elevesOrGroupes
  })

  this.defineIndex('lastChangeTimestamp', function () {
    return (this.lastChange && this.lastChange.date && this.lastChange.date.valueOf()) || null
  })
  this.defineIndex('nom', 'string')
  this.defineIndex('owner', 'string')
  this.defineIndex('publicAmis', 'boolean')
  this.defineIndex('statut', 'integer')
  this.defineIndex('structure', 'string')
  this.defineIndex('rids', 'string', function () {
    if (!Array.isArray(this.sousSequences) || !this.sousSequences.length) return []
    const rids = []
    this.sousSequences.forEach(sseq => {
      if (Array.isArray(sseq.serie) && sseq.serie.length) {
        sseq.serie.forEach(exo => {
          if (exo.rid) rids.push(exo.rid)
        })
      }
    })
    return rids
  })

  this.beforeStore(function (cb) {
    // on nettoie les ressources pour ne garder que ce qui nous intéresse
    for (const ssSeq of this.sousSequences) {
      for (const ressource of ssSeq.serie) {
        for (const key of Object.keys(ressource)) {
          if (!ressProps.includes(key)) delete ressource[key]
        }
      }
    }
    cb()
  })

  /**
   * Crée un backup de la séquence avant suppression définitive
   * (et supprime définitivement séances et résultats associés sans backup)
   */
  this.beforeDelete(function (cb) {
    if (!$resultat) $resultat = lassi.service('$resultat')
    if (!$seance) $seance = lassi.service('$seance')
    const oid = this.oid
    const entity = this

    flow().seq(function () {
      Backup.create({ id: `Resultat-${oid}`, entity }).store(this)
    }).seq(function () {
      $seance.purgeSequence(oid, this)
    }).seq(function () {
      $resultat.purgeSequence(oid, this)
    }).done(cb)
  })

  /**
   * On ne journalise la suppression que s'il y a un $deleteMessage
   */
  this.afterDelete(function (cb) {
    let message = this.$deleteMessage
    if (!message && this.__deletedAt) message = `En corbeille depuis le ${formatDateTime(this.__deletedAt)}`
    if (!message) return cb()
    // la suppression a eu lieu, on appelle pas cb avec une erreur si qqchose plante ici
    Journal.create({ id: `Sequence-${this.oid}`, action: 'delete', message: this.$deleteMessage }).store(error => {
      if (error) console.error(error)
      cb()
    })
  })
})
