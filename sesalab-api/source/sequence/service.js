'use strict'

/**
 * @module services/$sequence
 */
const flow = require('an-flow')
const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')

/**
 * @service $sequence
 */
app.service('$sequence', function (Groupe, Sequence, Utilisateur, $settings) {
  const maxResults = $settings.get('application.maxResults', 500)

  /**
   * @typedef ElevePatterns
   * @type {string[]} Une série de patterns qui correspondent à cet élève, à priori au moins deux eleve:xxx et groupe:yyy mais il peut y avoir bcp d'autres groupes
   */
  /**
   * Ventile les séquences données en actives/inactives (d'après leur statut et la date si c'est borné)
   * et retire celles qui ne s'appliqueraient pas à tous les élèves
   * @private
   * @param {Sequence[]} sequences Tableau de séquences
   * @param {ElevePatterns[]} elevesPatterns
   * @return {{sequencesInactives: Sequence[], sequencesActives: Sequence[]}}
   */
  function filterActivesSequences (sequences, elevesPatterns) {
    const sequencesActives = []
    const sequencesInactives = []

    for (const sequence of sequences) {
      const sequenceLight = lightenSequence(sequence, elevesPatterns)
      // si la séquence ne s'appliquait pas à tous les élèves on passe à la suivante
      if (!sequenceLight) continue
      // et on regarde si la séquence est active ou pas
      let isActive = true
      if (sequence.statut === constants.SEANCE_STATUT_INACTIF) {
        isActive = false
      } else if (sequence.statut === constants.SEANCE_STATUT_BORNEE) {
        // on compare des timestamps (en ms)
        const nowTs = Date.now()
        // ça vient de la base donc fromDate et toDate sont des dates
        if (nowTs < getTimestampFrom(sequence)) isActive = false
        if (nowTs > getTimestampTo(sequence)) isActive = false
      }
      if (isActive) sequencesActives.push(sequenceLight)
      else sequencesInactives.push(sequenceLight)
    }

    return { sequencesActives, sequencesInactives }
  }

  /**
   * Renvoie la liste des patterns d'un élève (au moins deux, eleve:xxx et groupe:yyy pour sa classe)
   * permettant de retrouver les séquences auxquelles un élève a accès.
   * Renvoie un tableau vide si l'élève n'existe pas.
   * @private
   * @param {string} eleveOid Identifiant de l'élève
   * @param {function} callback Callback
   * @memberof $seance
   */
  function getElevePatterns (eleveOid, callback) {
    let eleve
    flow().seq(function () {
      Utilisateur.match('oid').equals(eleveOid).grabOne(this)
    }).seq(function (_eleve) {
      if (!_eleve) return callback(null, [])
      eleve = _eleve
      Groupe.match('utilisateurs').equals(eleveOid).grab(this)
    }).seq(function (groupes) {
      const patterns = [`eleve:${eleveOid}`, `groupe:${eleve.classe}`]
      groupes.forEach(({ oid }) => {
        // normalement y'a plus les utilisateurs pour groupes classe… mais ça arrive encore en mars 2019
        if (oid !== eleve.classe) patterns.push(`groupe:${oid}`)
      })
      callback(null, patterns)
    }).catch(callback)
  }

  /**
   * Retourne la liste des patterns, une liste par élève
   * @private
   * @param {string[]} eleveOids Identifiants des élèves
   * @param {function} callback
   */
  function getElevesPatterns (eleveOids, callback) {
    flow(eleveOids).seqEach(function (eleveOid) {
      getElevePatterns(eleveOid, this)
    }).done(callback)
  }

  /**
   * Indique si la Sous-Séquence s'applique à cet élève
   * @private
   * @param sousSequence
   * @param {ElevePatterns} elevePatterns Les patterns de l'élève
   * @return {boolean}
   */
  function isSsSeqValideFor (sousSequence, elevePatterns) {
    // une sous-séquence est valide pour un élève si elle contient un de ses patterns
    return sousSequence.eleves.some(matcher => elevePatterns.includes(`${matcher.type}:${matcher.oid}`))
  }

  /**
   * Retourne la séquence allégée (on vire la propriété eleves de chaque sous-séquence,
   * et les sous-séquences qui ne s'appliquent pas aux élèves voulus)
   * @private
   * @param {Sequence} sequence
   * @param {ElevePatterns[]} elevesPatterns Un array avec la liste des patterns de chacun des élèves
   * @return {Object|undefined} undefined si la séquence ne s'appliquait à aucun élève
   */
  function lightenSequence (sequence, elevesPatterns) {
    // on vire la liste des élèves de la séquence, pour ne garder que ce qui sera utile coté front
    // il faut donc virer les sous-séquences qui ne sont pas affectée à tous les élèves (le front pourra pas filtrer)

    const sousSequencesLight = []
    for (const ssSeq of sequence.sousSequences) {
      if (elevesPatterns.some(elevePatterns => !isSsSeqValideFor(ssSeq, elevePatterns))) {
        continue
      }
      // cette sous-séquence s'applique à tous les élèves, on allège la série des commentaires et autres trucs inutiles à l'élève
      const { nom, type } = ssSeq
      const ssSeqLight = { nom, type }
      ssSeqLight.serie = ssSeq.serie.map((item) => {
        // on peut pas déstructurer la propriété public (nom de variable interdit en strict mode),
        // d'où ces 4 lignes…
        const { rid, type, titre, resume, description, maximumVisionnage, minimumReussite, nonZapping } = item
        const itemLight = { rid, type, titre, resume, description, maximumVisionnage, minimumReussite, nonZapping }
        itemLight.public = item.public
        // ATTENTION, en cas d'alias on file ici le rid original, mais si on lui envoyait
        // le rid de l'alias ça ne permettrait plus de récupérer le lastResultat
        // (il y aurait une redirection lors de l'affichage par l'élève et dans le résultat
        // on aura toujours le rid de l'original)
        return itemLight
      })
      sousSequencesLight.push(ssSeqLight)
    }
    // si rien en commun à tous les élèves, on prend pas la séquence
    if (!sousSequencesLight.length) return

    sequence.sousSequences = sousSequencesLight
    return sequence
  }

  // ////////////////////////////////////
  // Les méthodes exportées du service //
  // ////////////////////////////////////

  /**
   * Génère une liste d'identifiant dans le même format que l'index "eleve".
   *
   * @param {Utilisateur[]|Groupe[]|string[]} entities Tableau d'entités ou d'identifiants
   * @param {string} type Type d'entité (eleve ou groupe)
   * @return {string[]} Un tableau de chaines (les index à utiliser)
   * @memberof $sequence
   */
  function getIndexFromEntities (entities, type) {
    return entities.map((entity) => {
      return `${type}:${entity.oid || entity}`
    })
  }

  /**
   * @callback sequencesCallback
   * @param {Error} error
   * @param {Sequence[]} sequences
   */
  /**
   * Retourne la liste des séquences appartenants aux utilisateurs donnés.
   *
   * @param {string} ownerOid
   * @param {Object} [options]
   * @param {boolean} [options.includeDeleted] passer true pour inclure les séquences softDeleted
   * @param {boolean} [options.structureOid] Se limiter à cette structure
   * @param {sequencesCallback} callback
   * @memberof $sequence
   */
  function getSequencesOf (ownerOid, options, callback) {
    function grab (skip) {
      const query = Sequence.match('owner').equals(ownerOid)
      if (options.includeDeleted) query.includeDeleted()
      if (options.structureOid) query.match('structure').equals(options.structureOid)
      query.grab({ limit: maxResults, skip }, (error, sequences) => {
        if (error) return callback(error)
        allSequences = allSequences.concat(sequences)
        if (sequences.length === maxResults) {
          process.nextTick(grab, skip + maxResults)
        } else {
          callback(null, allSequences)
        }
      })
    }

    if (!ownerOid) return callback(Error('Formateur manquant'))
    let allSequences = []
    grab(0)
  }

  /**
   * Récupère une séquence préparée pour un élève (les sous-séquences qui ne le concerne pas
   * ont été retirées, les propriétés eleves vidées, mais la séquence est renvoyée même inactive)
   * @param sequenceOid
   * @param eleveOid
   * @param callback
   */
  function getSequencePourEleve (sequenceOid, eleveOid, callback) {
    let elevePatterns
    flow().seq(function () {
      getElevePatterns(eleveOid, this)
    }).seq(function (_elevePatterns) {
      console.debug(`pour l’élève ${eleveOid} on remonte les patterns`, _elevePatterns)
      if (!_elevePatterns.length) return callback(Error(`Aucun élève ${eleveOid}`))
      elevePatterns = _elevePatterns
      Sequence.match('oid').equals(sequenceOid).grabOne(this)
    }).seq(function (sequence) {
      if (!sequence) return callback(Error(`Aucune séquence ${sequenceOid}`))
      const sequenceLight = lightenSequence(sequence, [elevePatterns])
      if (!sequenceLight) return callback(Error(`L’élève ${eleveOid} n’est pas dans la séquence ${sequenceOid}`))
      callback(null, sequenceLight)
    }).catch(callback)
  }

  /**
   * @typedef $sequence~getSequencesPourElevesCallback
   * @param {Error|undefined} error
   * @param {{sequencesInactives: Sequence[], sequencesActives: Sequence[]}} Attention, si c'est pour un formateur toutes les séquences sont actives
   */
  /**
   * Récupère la liste des séquences pour les élèves donnés, triées en actives / inactives
   * @private
   * @param {string[]} eleveOids Les élèves connectés ensemble
   * @param {$sequence~getSequencesPourElevesCallback} callback
   */
  function getSequencesPourEleves (eleveOids, callback) {
    /** Tableau de tableaux */
    let elevesPatterns
    let sequences

    flow().seq(function () {
      getElevesPatterns(eleveOids, this)
    }).seq(function (_patterns) {
      elevesPatterns = _patterns
      // On récupère les séquences pour le premier élève du groupe connecté
      Sequence
        .match('eleve').in(elevesPatterns[0])
        .grab(this)
    }).seq(function (_sequences) {
      sequences = _sequences
      // faut les noms des proprios
      const owners = sequences.map(s => s.owner)
      if (owners.length) Utilisateur.match('oid').in(owners).grab(this)
      else this(null, [])
    }).seq(function (owners) {
      const formateursByOid = {}
      for (const { oid, nom, prenom } of owners) {
        formateursByOid[oid] = { oid, nom, prenom }
      }

      // on vire les séquences sans formateur
      sequences = sequences.filter(s => {
        if (formateursByOid[s.owner]) return true
        console.error(`La séquence ${s.oid} est orpheline (owner ${s.owner} a disparu)`)
        return false
      })

      // reste à trier les séquences actives / inactives en nettoyant les datas inutiles
      // (et en virant les séquences qui s'appliquent pas à tous les élèves)
      const response = filterActivesSequences(sequences, elevesPatterns)

      // et ajouter les formateurs
      response.formateursByOid = formateursByOid
      callback(null, response)
    }).catch(callback)
  }

  /**
   * Pour une séquence, retourne les oids des élèves mis individuellement (si type = eleve)
   * ou les oids de groupes (si type = groupe).
   * @param {Sequence} sequence
   * @param {string} type eleve|groupe
   * @return {string[]} Liste d'oids (d'élèves ou de groupes suivant type)
   * @memberof $sequence
   */
  function getSequenceParticipantsOids (sequence, type) {
    if (!sequence.sousSequences || !sequence.sousSequences.length) return []
    const oids = []
    sequence.sousSequences.forEach(ssSeq => {
      if (!ssSeq || !ssSeq.eleves || !ssSeq.eleves.length) return
      ssSeq.eleves.forEach(item => {
        if (item.type === type && !oids.includes(item.oid)) oids.push(item.oid)
      })
    })
    return oids
  }

  /**
   * Retourne la date de début sous forme de timestamp en ms (ou undefined si y'en a pas)
   * @param {Sequence} sequence
   * @return {number|undefined} Le timestamp (en ms)
   */
  function getTimestampFrom (sequence) {
    // fromDate donne le jour et fromTime le nb de quart d'heure depuis minuit
    // (dans le select on choisit par intervalle de 15 minutes et ça renvoie le nb d'intervalles :-/)
    if (sequence.fromDate) return sequence.fromDate.valueOf() + sequence.fromTime * 15 * 60000
  }

  /**
   * Retourne la date de fin sous forme de timestamp en ms (undefined si y'en a pas)
   * @param {Sequence} sequence
   * @return {number} Le timestamp (en ms) ou undefined si y'a pas de prop toDate
   */
  function getTimestampTo (sequence) {
    if (sequence.toDate) return sequence.toDate.valueOf() + sequence.toTime * 15 * 60000
  }

  /**
   * @callback ElevesOidsCallback
   * @param {Error} [error]
   * @param {string[]} elevesOids
   */
  /**
   * Fetch tous les oids élève de la séquence (en allant voir les groupes si besoin)
   * et modifie sequence pour ajouter une propriété $participantOids à chaque sous-séquence
   * @param {Sequence} sequence
   * @param {ElevesOidsCallback} cb
   * @memberof $sequence
   */
  function getTousLesElevesOids (sequence, cb) {
    const groupesOids = getSequenceParticipantsOids(sequence, constants.SEQUENCE_TYPE_GROUPE)
    const elevesOids = getSequenceParticipantsOids(sequence, constants.SEQUENCE_TYPE_ELEVE)
    const classesOids = []
    const oidsEleveByGroup = {}

    // faut aller récupérer les élèves actuels de ces groupes
    flow().seq(function () {
      if (!groupesOids.length) {
        return this(null, [])
      }
      Groupe.match('oid').in(groupesOids).grab(this)
    }).seqEach(function (groupe) {
      if (groupe.isClass) {
        classesOids.push(groupe.oid)
      } else {
        elevesOids.push(...groupe.utilisateurs)
        oidsEleveByGroup[groupe.oid] = groupe.utilisateurs
      }
      this()
    }).seq(function () {
      if (!classesOids.length) return this(null, [])
      // si y'avait des classes faut aller chercher leurs élèves
      Utilisateur.match('classe').in(classesOids).grab({ fields: ['oid', 'classe'] }, this)
    }).seq(function (usersInfos) {
      for (const { oid, classe } of usersInfos) {
        elevesOids.push(oid)
        // … et à la liste par groupe
        if (!oidsEleveByGroup[classe]) oidsEleveByGroup[classe] = []
        oidsEleveByGroup[classe].push(oid)
      }

      // reste à ajouter $participantOids sur chaque sous-séquence
      for (const ssSeq of sequence.sousSequences) {
        const oids = []
        for (const { oid, type } of ssSeq.eleves) {
          if (type === constants.SEQUENCE_TYPE_ELEVE) {
            oids.push(oid)
          } else if (type === constants.SEQUENCE_TYPE_GROUPE) {
            if (oidsEleveByGroup[oid]?.length) oids.push(...oidsEleveByGroup[oid])
          } else {
            console.error(Error(`Type inconnu dans la propriété eleves d’une sous-séquence (${type}, sequence ${sequence.oid})`))
          }
        }
        ssSeq.$participantOids = Array.from(new Set(oids))
      }

      // et retourner les oid élève
      cb(null, _.uniq(elevesOids))
    }).catch(cb)
  }

  /**
   * @callback ElevesCallback
   * @param {Error} [error]
   * @param {Utilisateur[]} eleves
   */
  /**
   * Récupère tous les élèves de la séquence (et ajoute $participantOids à chaque sous-séquence)
   *
   * @param {Sequence} sequence
   * @param {ElevesCallback} cb
   * @memberof $sequence
   */
  function getTousLesEleves (sequence, cb) {
    flow()
      .seq(function () {
        getTousLesElevesOids(sequence, this)
      })
      .seq(function (elevesOids) {
      // @todo Gérer le hardLimit… (devrait pas arriver, mais…)
      // Ça peut aussi être géré dans la constitution de la séquence, interdire de mettre plus de N élèves dedans,
      // et ici juste vérifier elevesOids.length et renvoyer une erreur si > maxElevesInSequence
        if (!elevesOids.length) return this(null, [])
        Utilisateur.match('oid').in(elevesOids).grab(this)
      })
      .done(cb)
  }

  /**
   * Indique si l'utilisateur a les droits nécessaires pour voir la séquence donnée.
   *
   * @param utilisateur L'utilisateur a tester
   * @param sequence Une séquence
   * @return "true" si la séquence est accessible à l'utilisateur
   * @memberof $sequence
   */
  function hasReadAccess (utilisateur, sequence) {
    return sequence.owner === utilisateur.oid || (sequence.publicAmis === true && utilisateur.structures.includes(sequence.structure))
  }

  /**
   * Indique si l'utilisateur a les droits nécessaires pour modifier la séquence donnée.
   *
   * @param utilisateur L'utilisateur a tester
   * @param sequence Une séquence
   * @return "true" si la séquence est modifiable par l'utilisateur
   * @memberof $sequence
   */
  function hasWriteAccess (utilisateur, sequence) {
    return sequence.owner === utilisateur.oid || (sequence.editableAmis === true && utilisateur.structures.includes(sequence.structure))
  }

  /**
   * Retourne les résultats appartenant aux entités en paramètre.
   *
   * @param {Utilisateur[]|Groupe[]|string[]} entities Tableau contenant des identifiants d'élèves/groupes ou des entités (élèves/groupes)
   * @param {string} type Type à rechercher (eleve ou groupe)
   * @param {Object} options Tableau d'options pour filtrer les séquences
   * @param {Function} callback Callback
   * @memberof $sequence
   */
  function withEntities (entities, type, options, callback) {
    function grab (skip) {
      const query = Sequence.match('eleve').in(getIndexFromEntities(entities, type))
      if (options.includeDeleted) query.includeDeleted()
      query.grab({ limit: maxResults, skip }, (error, sequences) => {
        if (error) return callback(error)
        allSequences = allSequences.concat(sequences)
        if (sequences.length === maxResults) grab(skip + maxResults)
        else callback(null, allSequences)
      })
    }

    let allSequences = []
    if (!entities.length) return callback(null, allSequences)
    grab(0)
  }

  return {
    getIndexFromEntities,
    getSequencesOf,
    getSequencePourEleve,
    getSequencesPourEleves,
    getTimestampFrom,
    getTimestampTo,
    getTousLesEleves,
    getTousLesElevesOids,
    hasReadAccess,
    hasWriteAccess,
    withEntities
  }
})
