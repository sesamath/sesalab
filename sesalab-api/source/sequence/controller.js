'use strict'

/**
 * @module controllers/sequence
 */
const flow = require('an-flow')
const _ = require('lodash')
const appLog = require('sesalab-api/source/tools/appLog')

const { OID_DELETED, SEANCE_STATUT_BORNEE } = require('sesalab-commun/constants.js')
const { extractSequencesCollegues } = require('sesalab-commun/utils')

const errorLoggerCb = error => { error && console.error(error) }

/**
 * API liées aux séquences.
 *
 * @Controller Sequence
 */
app.controller(function (Sequence, Utilisateur, $resultat, $sequence, $session) {
  const maxResults = app.settings.application.maxResults || 500

  function fetchSequences (getBaseQuery, from, callback) {
    const response = {}
    flow()
      .seq(function () {
        getBaseQuery().count(this)
      })
      .seq(function (count) {
        response.count = count
        if (!count) return this(null, [])
        const query = getBaseQuery()
        const options = {
          // il faut mettre les champs dont SequencesPanel a besoin (pour Mes séquences et Séquences de mes collègues)
          // donc aussi le constructeur de behavior.Sequence
          fields: ['groupes', 'lastChange', 'nom', 'oid', 'owner', 'prioritaire', 'public', 'publicAmis', 'editableAmis', 'statut', 'structure', 'uniqEleve'],
          limit: maxResults,
          offset: from
        }
        query.grab(options, this)
      })
      .seq(function (sequences) {
        from += maxResults
        if (from < response.count) {
          response.nextUrl = `sequences?from=${from}`
        }
        response.sequences = sequences
        callback(null, response)
      })
      .catch(callback)
  }

  /**
   * Retourne la liste des séquences du formateur connecté (oid et titre)
   * @route {GET} api/sequences
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {object} query Objet contenant les filtres à appliquer
   * @queryparam {boolean} query.deletedOnly Récupère seulement les séquences supprimées
   * @queryparam {Number} query.from Offset de récupération
   */
  this.get('api/sequences', function (/** @type {Context} */ context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const owner = currentFormateur.oid
    const deletedOnly = context.request.query.deletedOnly

    const from = parseInt(context.request.query.from) || 0

    function getQuery () {
      let query = Sequence.match('owner').equals(owner)
      if (deletedOnly) query = query.onlyDeleted()
      return query
    }
    fetchSequences(getQuery, from, (error, response) => {
      if (error) {
        console.error(error)
        return context.restKo('Erreur interne, impossible de récupérer les séquences')
      }
      context.rest(response)
    })
  })

  /**
   * Affiche les séquences des collègues du formateur connecté.
   *
   * @name SequencesCollegues
   * @authentication Cette route nécessite un formateur connecté
   * @route {GET} api/sequences-collegues
   */
  this.get('api/sequences-collegues', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const currentStructureOid = $session.getCurrentStructureOid(context)
    const owner = currentFormateur.oid

    const from = parseInt(context.request.query.from) || 0

    const getQuery = () => Sequence
      .match('owner').notIn([owner])
      .match('publicAmis').true()
      .match('structure').equals(currentStructureOid)

    const errorCb = (error) => {
      console.error(error)
      context.restKo('Erreur interne, impossible de récupérer les séquences des collègues')
    }

    fetchSequences(getQuery, from, (error, response) => {
      if (error) return errorCb(error)
      if (!response.count) return context.rest(response)
      let { sequences } = response
      // maintenant on est sûr d'en avoir
      // on va chercher les copains concernés
      // les séquences sans owner ça existe pas, mais si on en trouve quand même on veut le signaler et continuer
      const oids = sequences.map(s => s.owner).filter(Boolean)
      if (oids.length < sequences.length) {
        // on ajuste le total
        response.count = oids.length
        // on log le pb
        const orphans = sequences.filter(s => !s.owner)
        console.error(Error(`Séquences sans owner : ${orphans.map(s => s.oid).join(' ')} => on les fixe avec ${OID_DELETED}`))
        for (const orphan of orphans) {
          orphan.owner = OID_DELETED
          // on enregistre en tâche de fond
          orphan.store(errorLoggerCb)
        }
        if (!oids.length) {
          response.sequences = []
          return context.rest(response)
        }
        // faut màj la liste
        sequences = sequences.filter(s => s.owner)
        response.sequences = sequences
      }
      // on peut aller chercher les proprios de ces séquences
      Utilisateur.match('oid').in(oids).grab((error, users) => {
        if (error) return errorCb(error)
        const deletedUser = {
          prenom: 'Compte',
          nom: 'Supprimé',
          type: 1
        }
        // on remplace l'oid du owner par le user complet, si on le trouve
        // (car il peut avoir disparu et ses séquences conservées)
        for (const s of sequences) {
          if (s.owner === OID_DELETED) {
            // on en crée un fake pour affichage dans les séquences partagées
            s.$ownerFull = deletedUser
          } else {
            const owner = users.find(u => u.oid === s.owner)
            if (owner) {
              s.$ownerFull = owner
            } else {
              // l'utilisateur a été supprimé et la séquence existe encore
              // (pour que les collègues puissent y accéder, ou les élèves
              // voir leurs résultats)
              appLog.warn(Error(`La séquence ${s.oid} avait un owner ${s.owner} disparu, on le remplace par ${OID_DELETED}`))
              s.owner = OID_DELETED
              s.$ownerFull = deletedUser
              s.store(errorLoggerCb)
            }
          }
        }

        if (from + maxResults < response.count) {
          // on a pas tout, faut donner le lien pour la suite
          response.nextUrl = 'sequences-collegues?from=' + (from + maxResults)
        }

        context.rest(response)
      })
    })
  })

  /**
   * Renvoie une séquence telle qu'elle serait vue par l'élève (pour le test formateur)
   */
  this.get('api/sequence-eleve/:sequenceOid/:eleveOid', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const { sequenceOid, eleveOid } = context.arguments
    $sequence.getSequencePourEleve(sequenceOid, eleveOid, (error, sequence) => {
      if (error) return context.restKo(error)
      if (sequence.owner !== currentFormateur.oid) {
        let isDenied = true
        // il faut regarder si c'est une séquence partagée, et si oui avec qui
        if (sequence.public) isDenied = false
        // partagée avec les profs de l'établissement
        else if (sequence.publicAmis && currentFormateur.structures.includes(sequence.structure)) isDenied = false
        // partagée dans un groupe, compliqué et long d'interroger la sésathèque dans laquelle est stockée cette séquence pour savoir si ce prof est dans ce groupe
        // Si elle est partagée via un groupe auquel appartient ce prof, alors elle doit être listée dans sequenceColleguesFolders
        // (ce sequenceColleguesFolders est peuplé coté client mais il doit nous tenir au courant via un PUT à l'init du panneau)
        else if (extractSequencesCollegues(currentFormateur.sequenceColleguesFolders).includes(sequence.oid)) isDenied = false
        if (isDenied) return context.accessDenied()
      }
      context.rest({ sequence })
    })
  })

  /**
   * Renvoie la liste des séquences pour le ou les élève(s) associés à la session courante.
   * @route {GET} /api/sequences-eleves
   * @authentication Cette route nécessite un utilisateur connecté
   * @return {{sequencesActives: Sequence[], sequencesInactives: Sequence[], sequencesDesafectees: Sequence[]}}
   */
  this.get('api/sequences-eleves', function (context) {
    let eleveOids
    let response

    flow().seq(function () {
      const currentEleves = $session.getCurrentEleves(context)
      if (!currentEleves) return context.accessDenied()
      eleveOids = currentEleves.map(eleve => eleve.oid)
      $sequence.getSequencesPourEleves(eleveOids, this)
    }).seq(function (_response) {
      response = _response
      // il faut ajouter celles qui ne lui sont plus affectées
      // mais pour lesquelles il a des résultats, on récupère donc tous ses résultats hors de ces séquences qu'on connait déjà
      const excludedOids = response.sequencesActives.map(s => s.oid).concat(response.sequencesInactives.map(s => s.oid))
      $resultat.getSequences(eleveOids, excludedOids, this)
    }).seq(function (oldSequencesOids) {
      // on ajoute cette propriété à la réponse
      response.sequencesDesaffectees = oldSequencesOids
      context.rest(response)
    }).catch(function (error) {
      console.error(error)
      context.restKo('Une erreur est survenue, impossible de récupérer les séquences')
    })
  })

  /**
   * Retourne les propriétés d'une séquence (prop sequence dans le retour json)
   * @authentication Cette route nécessite un formateur connecté
   * @route {GET} /api/sequence/:oid
   * @routeparam {Number} oid Identifiant de la séquence
   * @return {{sequence: Sequence}}
   */
  this.get('api/sequence/:oid', function (context) {
    function errorCb (error) {
      console.error(error)
      context.restKo('Erreur interne, impossible de récupérer cette séquence')
    }

    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const sequenceOid = context.arguments.oid
    if (!sequenceOid) return context.restKo('Aucune identifiant de séquence fourni, impossible de la récupérer')
    if (sequenceOid === 'undefined') return context.restKo('Identifiant de séquence invalide')

    Sequence
      .match('oid').equals(sequenceOid)
      .grabOne(function (error, sequence) {
        if (error) return errorCb(error)
        if (!$sequence.hasReadAccess(currentFormateur, sequence)) return context.accessDenied()
        context.rest({ sequence })
      })
  })

  /**
   * Vérification des données fournies lors de la création ou la modification d'une séquence
   */
  function checkFields (data, context, callback) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()

    if (_.isEmpty(data.nom)) return context.fieldError('nom', 'Le nom de la séquence ne doit pas être vide')
    if (!_.isString(data.nom)) return context.fieldError('nom', 'Le nom de la séquence n’est pas correct')

    if (data.statut === SEANCE_STATUT_BORNEE) {
      if (!data.fromDate) return context.fieldError('fromDate', 'Vous devez choisir une date de début')
      if (!data.toDate) return context.fieldError('toDate', 'Vous devez choisir une date de fin')
      // Dans le select, il est possible de choisir uniquement des heures incrémentées toutes les 15 minutes
      // console.log('tsfrom', $sequence.getTimestampFrom(data))
      // console.log('tsto', $sequence.getTimestampTo(data))
      if ($sequence.getTimestampFrom(data) >= $sequence.getTimestampTo(data)) return context.fieldError('toDate', 'La date de fin doit être après celle de début')
    } else {
      delete data.fromDate
      delete data.fromTime
      delete data.toDate
      delete data.toTime
    }

    if (data.sousSequences) return callback()

    if (!_.isArray(data.sousSequences)) return context.fieldError('sousSequences', 'Sous-séquences incorrectes')
    try {
      for (const ssSeq of data.sousSequences) {
        if (ssSeq.eleves) {
          if (!_.isArray(ssSeq.eleves)) throw new Error('Une sous-séquence est incorrecte (élèves)')
          for (const eleve of ssSeq.eleves) {
            if (!eleve || !eleve.oid) throw new Error('Une sous-séquence est incorrecte (élève sans oid)')
          }
        }
        if (ssSeq.serie) {
          if (!_.isArray(ssSeq.serie)) throw new Error('Une sous-séquence est incorrecte (série)')
          for (const ressource of ssSeq.serie) {
            if (!ressource || !ressource.type) throw new Error('Une sous-séquence est incorrecte (ressource sans type)')
            if (ressource.type !== 'error' && !ressource.rid) throw new Error('Une sous-séquence est incorrecte (ressource sans rid)')
          }
        }
      }
      callback()
    } catch (error) {
      return context.fieldError('sousSequences', error.message)
    }
  }

  /**
   * Mise à jour d'une séquence.
   *
   * @name Sequence
   * @authentication Cette route nécessite un formateur connecté
   * @route {PUT} api/sequence
   * @bodyparam {object} data Une {@link Sequence}
   */
  this.put('api/sequence', function (context) {
    const data = context.post
    if (!data) context.restKo('Aucune séquence fournie')
    const currentStructureOid = $session.getCurrentStructureOid(context)
    const utilisateur = $session.getCurrentFormateur(context)
    if (!utilisateur) return context.accessDenied()

    flow()
      .seq(function () {
        checkFields(data, context, this)
      })
      .seq(function () {
        const sequence = Object.assign({}, data, {
          lastChange: {
            date: new Date(),
            modifier: _.pick(utilisateur, ['nom', 'prenom', 'oid'])
          },
          structure: currentStructureOid
        })

        // On évite de s'approprier une ressource
        if (!sequence.owner) {
          sequence.owner = utilisateur.oid
        } else if (!$sequence.hasWriteAccess(utilisateur, sequence)) {
          return context.accessDenied('Vous n’avez pas les droits d’édition sur cette séquence')
        }

        Sequence.create(sequence).store(this)
      })
      .seq(function (sequence) {
        context.rest({ sequence })
      })
      .catch(context.next)
  })

  /**
   * Modifie le nom de la séquence
   * (appelé par MesSequences.onRenameSequence)
   */
  this.put('api/rename-sequence', function (context) {
    const { oid, nom } = context.post
    if (!nom || !oid) context.restKo('Donnée manquante (il faut nom et oid)')
    const utilisateur = $session.getCurrentFormateur(context)
    if (!utilisateur) return context.accessDenied()

    const errorCb = (error) => {
      console.error(error)
      context.restKo('Erreur interne, impossible de renommer cette séquence')
    }

    Sequence
      .match('oid').equals(oid)
      .grabOne(function (error, sequence) {
        if (error) return errorCb(error)
        if (!sequence) {
          return context.restKo(`La séquence ${oid} n’existe pas`)
        }
        if (sequence.owner !== utilisateur.oid) {
          return context.restKo('Seul le propriétaire peut renommer une séquence')
        }
        if (sequence.nom === nom) {
          return context.rest({})
        }
        sequence.nom = nom
        sequence.store(function (error) {
          if (error) return errorCb(error)
          context.rest({})
        })
      })
  })

  /**
   * Modifie les options de partage de la séquence
   * (appelé par MesSequences.onPartageSequenceModel)
   */
  this.put('api/share-sequence', function (context) {
    const utilisateur = $session.getCurrentFormateur(context)
    if (!utilisateur) return context.accessDenied()
    const errorCb = (error) => {
      console.error(error)
      context.restKo('Erreur interne, impossible de partager cette séquence')
    }
    const props = ['public', 'publicAmis', 'editableAmis', 'groupes', 'hasModel']
    const { oid } = context.post
    Sequence
      .match('oid').equals(oid)
      .grabOne(function (error, sequence) {
        if (error) return errorCb(error)
        if (!sequence) {
          return context.restKo(`La séquence ${oid} n’existe pas`)
        }
        if (sequence.owner !== utilisateur.oid) {
          return context.restKo('Seul le propriétaire peut renommer une séquence')
        }
        if (props.every(prop => sequence[prop] === context.post[prop])) {
          // y'a rien à changer…
          return context.rest({})
        }
        for (const prop of props) {
          sequence[prop] = context.post[prop]
        }
        sequence.store(function (error) {
          if (error) return errorCb(error)
          context.rest({})
        })
      })
  })

  /**
   * Restauration de plusieurs séquences.
   *
   * @name SequencesRestore
   * @authentication Cette route nécessite un formateur connecté
   * @route {PUT} api/sequence/:oids/restore
   * @queryparam {string} oids Liste d'oid de {@link Sequence} séparés par des virgules
   */
  this.put('api/sequence/:oids/restore', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const sequencesOids = context.arguments.oids.split(',')
    if (!sequencesOids.length) return context.restKo('Aucune séquence fournie')

    flow().seq(function () {
      // on veut renvoyer les séquences complètes, donc faut aller les chercher
      // (sinon c'était bcp plus efficace de faire un updateMany mongo)
      Sequence
        .match('oid').in(sequencesOids)
        .onlyDeleted()
        .grab(this)
    }).seqEach(function (sequence) {
      sequence.restore(this)
    }).seq(function (sequences) {
      context.rest({ sequences })
    }).catch(context.next)
  })

  /**
   * Met à jour les séquences qui contenaient un alias lorsque cet alias devient une ressource
   * Il faut passer en post aliasRid (celui de l'alias devenu ressource qui doit remplacer baseId/oid)
   * @route {PUT} api/sequences-update/:baseId/:oid
   * @queryparam {string} baseId La baseId de la sésathèque concernée (le début du rid)
   * @queryparam {string} oid L'oid de la ressource originale
   */
  this.put('api/sequences-update/:baseId/:oid', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    // le rid qui était indiqué dans l'exo de la série
    // (un alias qui pointait dessus et est devenu une vraie ressource, on doit remplacer ce rid par celui de l'alias)
    const rid = context.arguments.baseId + '/' + context.arguments.oid
    const newItem = context.post?.newItem
    if (!newItem?.rid) context.restKo(`nouvel item pour remplacer ${rid} manquant ou incomplet`)
    const newRid = newItem.rid
    const wasAlias = newRid !== rid
    let nbMod = 0
    flow().seq(function () {
      Sequence
        .match('rids').equals(rid)
        .includeDeleted()
        // FIXME, limiter à HARD_LIMIT_GRAB et recommencer si besoin
        .grab(this)
    }).seqEach(function (sequence) {
      let hasChanged = false
      for (const ssSeq of sequence.sousSequences) {
        for (const ressource of ssSeq.serie) {
          if (ressource.rid === rid) {
            if (wasAlias && ressource.aliasRid !== newRid) {
              // c'est un fork d'alias et ici il s'agit de la ressource originale et pas de l'alias, on touche à rien
              continue
            }
            ressource.rid = newRid
            ressource.titre = newItem.titre
            if (newItem.cle) {
              ressource.cle = newItem.cle
            } else if (ressource.cle) {
              delete ressource.cle
            }
            delete ressource.aliasRid
            hasChanged = true
          }
        }
      }
      if (!hasChanged) return this()
      nbMod++
      sequence.store(this)
    }).seq(function () {
      context.rest({ nbMod })
    }).catch(context.next)
  })

  /**
   * Supprime un ensemble de séquences.
   *
   * @name SequencesDelete
   * @authentication Cette route nécessite un formateur connecté
   * @route {DEL} api/sequence/:oids
   * @queryparam {string} oids Liste d'oid de {@link Sequence} séparés par des virgules
   */
  this.delete('api/sequence/:oids', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const sequenceOids = context.arguments.oids.split(',')
    if (!sequenceOids.length) return context.restKo('Aucune séquence fournie')
    const originalSequences = []
    const warnings = []

    flow().seq(function () {
      Sequence
        .match('oid').in(sequenceOids)
        .match('owner').equals(currentFormateur.oid)
        .grab(this)
    }).seqEach(function (sequence, index) {
      if (sequence) {
        // On garde une référence de l'originale pour la renvoyer
        originalSequences.push(_.clone(sequence))
        sequence.softDelete(this)
      } else {
        warnings.push(`La séquence ${sequenceOids[index]} n’existe plus`)
        this()
      }
    }).seq(function (sequences) {
      context.rest({ originals: originalSequences, deleted: sequences.length, warnings })
    }).catch(context.next)
  })
})
