const flow = require('an-flow')
const { hasProp } = require('sesajstools')
const createError = require('../tools/createError')
const normalizer = require('./normalizer')

function uniqueNomBuilder () {
  const groupe = this
  if (!groupe) throw Error('groupe invalide')
  // unicité du nom, si classe par établissement, sinon par établissement et owner
  if (groupe.isClass) return `${groupe.structure}---${normalizer(groupe.nom)}`
  return `${groupe.structure}---${groupe.owner}---${normalizer(groupe.nom)}`
}

/**
 * Représente un groupe d'élèves (ou une classe)
 *
 * @Entity Groupe
 * @class Groupe
 * @property {oid} oid Identifiant
 * @property {string} nom Nom
 * @property {boolean} isClass Indique s'il s'agit d'une classe
 * @property {string} owner oid du propriétaire du groupe
 * @property {string} structure oid de la structure du groupe
 * @property {string} niveau Identifiant du niveau (cf sesatheque-client/src/config.js)
 * @property {string[]} utilisateurs oids des {@link Utilisateur}s élèves du groupe
 */
app.entity('Groupe', function (Backup, Journal) {
  const GroupeDefinition = this
  let Utilisateur, $settings, $structure, maxClasses

  this.validateJsonSchema(
    {
      // JSON-Schema
      type: 'object',
      properties: {
        oid: { type: 'string' },
        // @todo à virer quand on aura plus besoin de la référence à labomep1
        // (sert à dédoublonner plusieurs import successifs)
        externalId: { type: 'string' },
        nom: { type: 'string', minLength: 1 },
        isClass: { type: 'boolean' },
        owner: { type: 'string' },
        structure: { type: 'string' },
        niveau: { type: ['string', 'null'] },
        utilisateurs: { type: 'array', items: { type: 'string' } }
      },
      required: ['nom'],
      errorMessage: {
        properties: {
          nom: 'doit avoir un champ "nom"'
        }
      }
    }
  )

  function checkLimit (groupe, cb) {
    if (!$settings) {
      $settings = lassi.service('$settings')
      maxClasses = $settings.get('application.maxClasses', 200)
    }
    if (!$structure) $structure = lassi.service('$structure')
    flow().seq(function () {
      $structure.countClasses(groupe.structure, this)
    }).seq(function (amount) {
      if (amount >= maxClasses) return cb(createError(409, `Vous avez atteint le nombre maximum de classe par structure : ${maxClasses}`))
      cb()
    }).catch(cb)
  }

  // on empêche de dépasser la limite sur une restauration
  this.validateOnChange(['isDeleted'], function (cb) {
    // Note: validateOnChange est toujours appelé à la création
    const groupe = this
    // la vérif de limite concerne seulement les classes
    if (!groupe.isClass) return cb()
    // Pas de contrainte sur un groupe supprimé (contrainte à la restauration, quand isDeleted change)
    if (groupe.isDeleted()) return cb()

    checkLimit(groupe, cb)
  })

  this.defaults(function () {
    this.utilisateurs = []
  })

  // oid du prof proprio d'un groupe
  this.defineIndex('owner', 'string')
  this.defineIndex('utilisateurs', 'string')
  // oid de la structure d'une classe
  this.defineIndex('structure', 'string')
  // Attention, pour l'indexation on passe en bas de casse, mais dans les data ce sera la casse d'origine
  this.defineIndex('nom', { normalizer })
  // On pourrait utiliser un index composite unique (structure/nom), mais en attendant que lassi
  // gère des index composites on en crée un en composant nous même,
  // pour interdire 2 classes de même nom sur la même structure
  this.defineIndex('uniqueNom', 'string', { unique: true, sparse: true }, uniqueNomBuilder)
  // imposé au beforeStore (si structure et pas de owner)
  this.defineIndex('isClass', 'boolean')

  /**
   * Si on supprime une classe, on supprime tous ses élèves
   */
  this.beforeDelete(function (cb) {
    const groupe = this
    Backup.create({ id: `Groupe-${this.oid}`, entity: this }).store(error => {
      if (error) return cb(error)
      if (!groupe.isClass) return cb()
      if (!Utilisateur) Utilisateur = lassi.service('Utilisateur')
      // on vire tous ses élèves
      flow().seq(function () {
        // (faut un grab et pas directement un purge pour passer par le beforeDelete de l'élève)
        Utilisateur
          .match('classe').equals(groupe.oid)
          .includeDeleted()
          .grab(this)
      }).seqEach(function (eleve) {
        eleve.delete(this)
      }).done(cb)
    })
  })

  /**
   * On ne journalise la suppression que s'il y a un $deleteMessage
   */
  this.afterDelete(function (cb) {
    if (!this.$deleteMessage) return cb()
    // la suppression a eu lieu, on appelle pas cb avec une erreur si qqchose plante ici
    Journal.create({ id: `Groupe-${this.oid}`, action: 'delete', message: this.$deleteMessage }).store(error => {
      if (error) console.error(error)
      cb()
    })
  })

  this.beforeStore(function (cb) {
    const groupe = this
    // le validate est pas encore passé
    if (typeof groupe.nom === 'string') groupe.nom = groupe.nom.trim()

    if (typeof groupe.niveau === 'number') {
      groupe.niveau = groupe.niveau.toString()
    }

    // on fixe isClass si structure et pas de proprio
    const isClass = Boolean(groupe.structure && !groupe.owner)
    // si ça change, on prévient car c'est pas très normal
    if (groupe.oid && hasProp(groupe, 'isClass') && groupe.isClass !== isClass) {
      if (isClass) console.error(`Le groupe ${groupe.nom} devient une classe (${groupe.oid})`)
      else console.error(`Le groupe ${groupe.nom} était une classe mais ne l’est plus (${groupe.oid})`)
    }
    // mais on laisse faire
    groupe.isClass = isClass

    // @todo virer utilisateurs si isClasse (quand on s'en servira plus nulle part)
    const cleanUsers = groupe.utilisateurs.filter(Boolean)
    if (cleanUsers.length < groupe.utilisateurs.length) {
      console.error(`Le groupe ${groupe.oid} avait des utilisateurs invalides`, groupe.utilisateurs)
      groupe.utilisateurs = cleanUsers
    }
    cb()
  })

  // en cas de doublon sur une création avec l'autre en corbeille,
  // on remplace celui en corbeillle par le nouveau
  this.onDuplicate(function groupeDuplicateCallback (duplicateError, cb) {
    const groupe = this
    // uniqueNom est le seul index unique

    // on devrait jamais modifier un deleted…
    if (groupe.isDeleted()) return cb(duplicateError)
    // si celui qu'on voulait sauver existait déjà avant c'est louche, on retourne l'erreur sans rien faire
    if (groupe.oid) return cb(duplicateError)

    // c'est une création, on va chercher le deleted du même uniqueNom qui prend la place,
    // pour se mettre à la sienne
    const uniqueNom = uniqueNomBuilder.call(groupe)
    flow().seq(function () {
      GroupeDefinition.match('uniqueNom').equals(uniqueNom).onlyDeleted().grabOne(this)
    }).seq(function (groupeDeleted) {
      // y'en a pas, c'est une création en double, on le signale
      if (!groupeDeleted) return cb(duplicateError)
      // il vient d'être créé avec un doublon en corbeille, on lui donne son oid
      // => celui qui était en corbeille va se faire écraser
      // => si des élèves en corbeille étaient dans ce groupe, les restaurer les remettra dedans
      groupe.oid = groupeDeleted.oid
      groupe.store(cb)
    }).catch(cb)
  })
})
