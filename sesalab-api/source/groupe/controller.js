/**
 * @module controllers/groupe
 */
const constants = require('sesalab-commun/constants.js')
const flow = require('an-flow')
const _ = require('lodash')

/**
 * API liées au groupe.
 *
 * @controller Groupe
 */
app.controller(function (Groupe, Utilisateur, $groupe, $utilisateur, $structure, $session) {
  /**
   * Affiche un groupe.
   *
   * @name Groupe
   * @route {GET} api/groupe/:oid
   * @authentication Cette route nécessite un formateur connecté
   * @routeparam {string} oid Identifiant du groupe
   */
  this.get('api/groupe/:oid', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const groupeOid = context.arguments.oid
    if (!groupeOid) return context.restKo('Aucun groupe fourni')
    const deletedOnly = context.request.query.deletedOnly
    const response = {}

    flow()
      .seq(function () {
        let query = Groupe
          .match('oid').equals(groupeOid)
          // On ne peut récupérer que les groupes que l'on a créé
          .match('owner').equals(currentFormateur.oid)
        if (deletedOnly) query = query.onlyDeleted()
        query.grabOne(this)
      })
      .seq(function (groupe) {
        if (!groupe) return context.rest({ message: 'NOT FOUND' })
        response.groupe = groupe
        $utilisateur.loadUsers(groupe.utilisateurs, deletedOnly, this)
      })
      .seq(function (utilisateurs) {
      // Les utilisateurs doivent être triés dans le même ordre que le tableau d'ids lié au groupe
        utilisateurs = _.sortBy(utilisateurs, function (utilisateur) {
          return response.groupe.utilisateurs.indexOf(utilisateur.oid)
        })

        response.utilisateurs = utilisateurs
        context.rest(response)
      })
      .catch(context.next)
  })

  /**
   * Récupère la classe d'un élève.
   *
   * @name Classe
   * @route {GET} api/eleve/:eleveOid/classe
   * @routeparam {string} eleveOid Identifiant de l'élève
   */
  this.get('api/eleve/:eleveOid/classe', function (context) {
    const eleveOid = context.arguments.eleveOid
    if (!eleveOid) return context.restKo('Aucun élève fourni')
    const fields = context.get.fields ? context.get.fields.split(',') : []

    flow()
      .seq(function () {
        Utilisateur
          .match('type').equals(constants.TYPE_ELEVE)
          .match('oid').equals(eleveOid)
          .grabOne(this)
      })
      .seq(function (eleve) {
        if (!eleve) return context.notFound()
        if (!eleve.classe) return this()

        Groupe.match('oid').equals(eleve.classe).grabOne(this)
      })
      .seq(function (classe) {
        if (!classe) return context.rest({ message: 'Classe non trouvée' })
        if (fields.length) classe = _.pick(classe, fields)

        return context.rest(classe)
      })
      .catch(context.next)
  })

  /**
   * Affiche les groupes d'un formateur.
   *
   * @name GroupeByOwner
   * @route {GET} api/groupes/by-owner
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {boolean} onlyDeleted True pour récupérer les groupes supprimés seulement
   */
  this.get('api/groupes/by-owner', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const onlyDeleted = context.request.query.onlyDeleted
    const response = { groupes: {} }

    flow()
      .seq(function () {
        $groupe.byOwner([currentFormateur.oid], { onlyDeleted }, this)
      })
      .seqEach(function (groupe) {
        response.groupes[groupe.oid] = groupe
        let eleves
        // Lorsque l'on récupère des groupes non deleted, alors il faut récupérer l'ensemble des élèves non deleted
        // Lorsque l'on récupère des groupes soft deleted, alors il faut récupérer l'ensemble des élèves
        flow()
          .seq(function () {
            $utilisateur.loadUsers(groupe.utilisateurs, false, this)
          })
          .seq(function (_utilisateurs) {
            eleves = _utilisateurs
            if (!onlyDeleted) return this(null, [])
            $utilisateur.loadUsers(groupe.utilisateurs, true, this)
          })
          .seq(function (_utilisateurs) {
            groupe.utilisateurs = _.union(eleves, _utilisateurs)
            this()
          })
          .done(this)
      })
      .seq(function () {
        context.rest(response)
      })
      .catch(context.next)
  })

  /**
   * Retourne les classes d'une structure ({@link Structure}) et leurs élèves
   * (reconstruit la prop utilisateurs pour chaque classe, et renvoie les orphelins dans une propriété orphelins)
   *
   * @name GroupeByStructure
   * @route {GET} api/groupes/by-structure/:oid
   * @queryparam {boolean} onlyDeleted True pour récupérer les groupes supprimés seulement
   */
  this.get('api/groupes/by-structure/:oid', function (context) {
    const structureId = context.arguments.oid
    if (!structureId) return context.restKo('Aucune structure fournie')
    if (structureId === 'undefined') return context.restKo('Structure fournie invalide')
    const onlyDeleted = context.request.query.onlyDeleted
    // Cette route est appellée au login élève donc on doit laisser la porte ouverte...
    // TODO: voir si on ne pourrait pas l'appeler *après* avoir loggé l'élève sur la structure ?
    // if (!context.isFormateurInStructure(structureId)) return context.accessDenied();
    const response = {
      groupes: {},
      orphelins: []
    }
    let eleves
    flow().seq(function () {
      $structure.getAllSpecificUsers(structureId, { type: constants.TYPE_ELEVE, onlyDeleted }, this)
    }).seq(function (_eleves) {
      eleves = _eleves
      let query = Groupe
        .match('structure').equals(structureId)
        .match('isClass').equals(true)

      if (onlyDeleted) query = query.onlyDeleted()

      query.grab(this)
    }).seqEach(function (classe) {
      // On remplace l'id du classe par son instance complète
      response.groupes[classe.oid] = classe
      // et on ajoute les élèves (la propritété utilisateurs n'existe en bdd
      // que pour les groupes qui ne sont pas des classes)
      classe.utilisateurs = _.filter(eleves, (u) => u.classe === classe.oid)
      this()
    }).seq(function () {
      // reste les orphelins éventuels
      response.orphelins = _.filter(eleves, (u) => !u.classe || !response.groupes[u.classe])
      // et envoyer tout ça
      context.rest(response)
    }).catch(context.next)
  })

  /**
   * Supprime un groupe
   *
   * @name GroupeRemove
   * @route {DEL} api/groupe/:oid
   * @authentication Cette route nécessite un formateur connecté
   * @routeparam {string} oid Identifiant du {@link Groupe}
   */
  this.delete('api/groupe/:oid', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const groupeOid = context.arguments.oid
    if (!groupeOid) return context.restKo('Aucun groupe fourni')
    let groupe

    flow()
      .seq(function () {
        Groupe.match('oid').equals(groupeOid).grabOne(this)
      })
      .seq(function (_groupe) {
        groupe = _groupe
        if (!groupe) return context.notFound()
        if (!context.peutEditerGroupe(groupe)) return context.accessDenied()
        groupe.softDelete(this)
      })
      .seq(function () {
        if (!groupe.isClass) return context.rest({ deleted: true, utilisateurs: [] })
        $utilisateur.loadUsers(groupe.utilisateurs, false, this)
      })
      .seqEach(function (utilisateur) {
        utilisateur.softDelete(this)
      })
      .seq(function () {
        context.rest({
          deleted: true,
          utilisateurs: _.compact(groupe.utilisateurs)
        })
      })
      .catch(context.next)
  })

  /**
   * Change le groupe d'un ensemble d'élèves
   *
   * @name GroupeStudents
   * @route {PUT} api/groupe/:oid/students/:elevesOids
   * @authentication Cette route nécessite un formateur connecté
   * @routeparam {string} oid Identifiant du {@link Groupe}
   * @routeparam {string[]} oids Identifiants des élèves
   */
  this.put('api/groupe/:oid/students/:elevesOids', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const students = context.arguments.elevesOids.split(',')
    if (!students.length) return context.restKo('Argument oids incorrect')
    const newClasseOid = context.arguments.oid

    flow()
      .seq(function () {
        Utilisateur
          .match('oid').in(students)
          .match('type').equals(constants.TYPE_ELEVE)
          .grab(this)
      })
      .seqEach(function (eleve) {
        if (!eleve) return context.notFound()
        if (!context.isFormateurInOneOfStructures(eleve.structures)) return context.accessDenied()

        // le afterStore de Utilisateur va mettre à jour le groupe.utilisateurs[]
        eleve.classe = newClasseOid
        eleve.store(this)
      })
      .seq(function () {
        context.rest({})
      })
      .catch(context.next)
  })

  /**
   * Met à jour un groupe.
   *
   * @name GroupeUpdate
   * @route {PUT} api/groupe
   * @authentication Cette route nécessite un formateur connecté
   * @bodyparam {object} groupe Un {@link Groupe}
   */
  this.put('api/groupe', function (context) {
    const groupe = context.post
    if (!groupe) return context.restKo('Aucun groupe fourni')
    if (!context.peutEditerGroupe(groupe)) return context.accessDenied()
    if (!groupe.nom) return context.fieldError('nom', 'Un nom est obligatoire')

    const response = {}
    flow().seq(function () {
      $groupe.storeGroup(groupe, this)
    }).seq(function (dbGroupe) {
      response.groupe = dbGroupe
      $utilisateur.loadUsers(dbGroupe.utilisateurs, false, this)
    }).seq(function (utilisateurs) {
    // Les utilisateurs doivent être triés dans le même ordre que le tableau d'ids lié au groupe
      utilisateurs = _.sortBy(utilisateurs, function (utilisateur) {
        return response.groupe.utilisateurs.indexOf(utilisateur.oid)
      })
      response.utilisateurs = utilisateurs
      context.rest(response)
    }).catch(error => {
      // context.rest fixe success à true, on utilise context.json
      if (/doublon/.test(error.message)) return context.json({ success: false, message: 'Ce nom existe déjà (éventuellement en corbeille)' })
      console.error(error)
      error = Error()
      context.next(error)
    })
  })

  /**
   * Restaure un ensemble de groupes.
   *
   * @name GroupesRestore
   * @route {PUT} api/groupe/:oids/restore
   * @routeparam {string[]} oids Un tableau d'identifiants de {@link Groupe}
   */
  this.put('api/groupe/:oids/restore', function (context) {
    const groupeOids = context.arguments.oids.split(',')
    if (groupeOids.length === 0) return context.restKo('Aucun groupe fourni')

    let restoredGroups = []
    flow()
      .seq(function () {
        Groupe
          .match('oid').in(groupeOids)
          .onlyDeleted() // Le groupe récupéré a été soft deleted
          .grab(this)
      })
      .seqEach(function (groupe) {
        if (!context.peutEditerGroupe(groupe)) return this()
        groupe.markToRestore().store(this) // On évite d'utiliser "restore" pour forcer le passage par le "beforeStore" classique
      })
      .seq(function (_groupes) {
        restoredGroups = _.compact(_groupes)
        const classes = _.filter(_groupes, (groupe) => { return groupe.isClass })
        const classesOids = _.map(classes, (classe) => { return classe.oid })

        Utilisateur
          .match('classe').in(classesOids)
          .onlyDeleted()
          .grab(this)
      })
      .seqEach(function (user) {
        user.markToRestore().store(this)
      })
      .seq(function () {
        context.rest({ groupes: restoredGroups })
      })
      .catch(context.next)
  })

  /**
   * Affiche une classe.
   *
   * @name GroupesRestore
   * @authentication Cette route nécessite un formateur connecté
   * @route {PUT} api/classe/:classeId
   * @routeparam {string} classeId Identifiant du {@link Groupe}
   */
  this.get('api/classe/:classeId', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const classeId = context.arguments.classeId
    if (!classeId) return context.restKo('Aucune classe fournie')

    flow()
      .seq(function () {
        Groupe
          .match('oid').equals(classeId)
          .grabOne(this)
      })
      .seq(function (classe) {
        if (!classe) return context.rest({ message: 'NOT FOUND' })
        if (!context.isFormateurInStructure(classe.structure)) return context.accessDenied()
        context.rest({ classe })
      })
      .catch(context.next)
  })
})
