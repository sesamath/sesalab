/**
 * @module services/$groupe
 */
const flow = require('an-flow')
const { difference } = require('lodash')
const { guessNiveau } = require('sesatheque-client/dist/server/config')
const { hasProp } = require('sesajstools')

const normalizer = require('./normalizer')
const createError = require('../tools/createError')

/**
 * @service $groupe
 */
app.service('$groupe', function (Groupe, $settings) {
  const maxResults = $settings.get('application.maxResults', 500)

  /**
   * Ajoute un élève au groupe (mais ne le sauve pas).
   *
   * @param {Groupe} groupe
   * @param {Utilisateur|string} student
   * @param {groupeCallback} next
   * @memberof $groupe
   */
  function addStudent (groupe, student, next) {
    if (!groupe) throw new Error('empty group')
    const studentOid = typeof student === 'string' ? student : (student && student.oid)
    if (!studentOid) throw new Error('invalid student')
    if (!Array.isArray(groupe.utilisateurs)) groupe.utilisateurs = []
    if (!hasStudent(groupe, studentOid)) groupe.utilisateurs.push(studentOid)
  }

  /**
   * Retourne tous les groupes appartenant à des formateurs.
   *
   * @param {string[]} usersIds Tableaux d'identifiants d'utilisateurs
   * @param {Object} options Options pour filtrer les groupes
   * @param {groupesCallback} callback
   * @memberof $groupe
   */
  function byOwner (usersIds, options, callback) {
    function grab (skip) {
      const query = Groupe
        .match('owner').in(usersIds)
        .match('isClass').false()
      if (options.onlyDeleted) query.onlyDeleted()
      if (options.includeDeleted) query.includeDeleted()
      query.grab({ limit: maxResults, skip }, (error, groupes) => {
        if (error) return callback(error)
        allGroupes = allGroupes.concat(groupes)
        if (groupes.length === maxResults) grab(skip + maxResults)
        else callback(null, allGroupes)
      })
    }

    let allGroupes = []
    if (!usersIds.length) return callback(null, allGroupes)
    grab(0)
  }

  /**
   * @callback groupesCallback
   * @param {Error|undefined} error
   * @param {Groupe[]} groupes (au moins un array vide si y'a pas d'erreur)
   */

  /**
   * Récupère un groupe d'après son nom (éventuellement pour une structure ou un owner)
   * /!\ Methode utilisée par sesasso-labomep
   *
   * @param {string} nom
   * @param {object} [options]
   * @param {string} [options.owner] Filtrer sur ce owner (utilisateur.oid)
   * @param {string} [options.structure] Filtrer sur cette structure (oid)
   * @param {boolean} [options.isClass] Filtrer sur les classes (passer true pour les classes, false pour avoir les groupes "non classe")
   * @param {groupesCallback} next appelée avec (error, groupes)
   * @memberof $groupe
   */
  function findByNom (nom, options, next) {
    if (!options) options = {}
    const query = Groupe.match('nom').equals(nom)
    if (options.owner) query.match('owner').equals(options.owner)
    if (options.structure) query.match('structure').equals(options.structure)
    if (options.isClass) query.match('isClass').equals(options.isClass)
    if (options.includeDeleted) query.includeDeleted()
    query.grab(next)
  }

  /**
   * Passe à next la classe groupeNom sur structureOid, en la créant en bdd si besoin
   * (la classe retournée aura donc toujours un oid)
   * Si elle était en corbeille ça la restaure sans ses élèves (qui restent en corbeille
   * mais retourneront dans cette classe s'ils sont restaurés, car elle ne change pas d'oid)
   *
   * @param {string} nom
   * @param {string} structureOid
   * @param {Object} [stats] objets pour cumuler les infos des modifs d'un import
   * @param {number} [stats.nbGroupsUpdated] nb de classes modifiés (ou pas, en fait existantes, éventuellement en corbeille)
   * @param {number} [stats.nbGroupsAdded] nb de classes créées
   * @param {groupeCallback} next
   * @memberof $groupe
   */
  function findOrCreateClasse (nom, structureOid, stats, next) {
    if (!structureOid) throw new Error('id de structure manquant')
    if (!nom) throw new Error('Nom de Groupe manquant')
    if (!normalizer(nom)) throw new Error('Nom de Groupe invalide')
    if (typeof stats === 'function') {
      next = stats
      stats = {}
    }
    if (typeof next !== 'function') throw new Error('Fonction de rappel manquante')
    flow().seq(function () {
      Groupe
        .match('structure').equals(structureOid)
        .match('nom').equals(nom)
        .match('isClass').equals(true)
        .includeDeleted()
        .grab(this)
    }).seq(function (classes) {
      let classe
      if (classes.length === 1) {
        classe = classes[0]
      } else if (classes.length > 1) {
        // faut prendre celle qui n'est pas deleted si y'en a une
        const notDeleted = classes.filter(c => !c.isDeleted())
        classe = notDeleted.length ? notDeleted[0] : classes[0]
      }
      if (classe) {
        if (hasProp(stats, 'nbGroupsUpdated')) stats.nbGroupsUpdated++
        if (classe.isDeleted()) {
          // on vide les élèves qu'elle contient car on veut pas les restaurer
          // (elle ne devrait pas en contenir qui ne seraient pas en corbeille)
          classe.utilisateurs = []
          return classe.restore(next)
        } else {
          return next(null, classe)
        }
      }
      // si on est toujours la classe n'existait pas, faut la créer et l'enregistrer pour avoir un oid
      if (hasProp(stats, 'nbGroupsAdded')) stats.nbGroupsAdded++
      Groupe.create({
        nom,
        niveau: guessNiveau(nom),
        structure: structureOid,
        isClass: true
      }).store(next)
    }).catch(next)
  }

  /**
   * Retourne true si le student fait partie du groupe.
   *
   * @param {Groupe} groupe
   * @param {Utilisateur|string} student objet ou oid
   * @return {boolean}
   * @memberof $groupe
   */
  function hasStudent (groupe, student) {
    if (!groupe) throw new Error('empty group')
    const studentOid = typeof student === 'string' ? student : (student && student.oid)
    if (!studentOid) throw new Error('invalid student')
    if (!Array.isArray(groupe.utilisateurs)) return false
    return groupe.utilisateurs.indexOf(studentOid) !== -1
  }

  /**
   * Indique si le groupe est une classe.
   *
   * @param {Groupe} groupe Le groupe à tester
   * @return {boolean}
   * @memberof $groupe
   */
  function isClass (groupe) {
    if (groupe) return !groupe.owner
    console.error(new Error('groupe falsy'))
    return false
  }

  /**
   * @callback elevesOidsCallback
   * @param {Error} [error]
   * @param {string[]} elevesOids
   */
  /**
   * Enlève des élèves d'un groupe.
   *
   * @param {Groupe} groupe Groupe à traiter
   * @param {string[]} elevesOids Les oids à enlever
   * @param {elevesOidsCallback} callback appelée avec les oids restants
   * @memberof $groupe
   */
  function removeStudents (groupe, elevesOids, callback) {
    const utilisateurs = difference(groupe.utilisateurs, elevesOids)
    groupe.utilisateurs = utilisateurs
    groupe.store(function (error) {
      if (error) return callback(error)
      callback(null, utilisateurs)
    })
  }

  /**
   * Enregistre un groupe en normalisant utilisateurs (si c'est des objets remplace par leur propriété oid
   *
   * @param groupe Groupe
   * @param {groupeCb} Callback
   * @memberof $groupe
   */
  function storeGroup (groupe, callback) {
    // init utilisateurs
    if (!Array.isArray(groupe.utilisateurs)) groupe.utilisateurs = []
    // Attention, si y'a des string vide ou des number ça les conserve…
    // @todo ajouter ce filter pour n'accepter que des string non vides
    // (pas mis tout de suite pour rester identique à l'existant, au cas où il resterait des int)
    // .filter(u => u && typeof u === 'string')
    else groupe.utilisateurs = groupe.utilisateurs.map(u => (u && u.oid) || u)
    // store
    if (groupe.store) return groupe.store(callback)
    if (!groupe.oid) return Groupe.create(groupe).store(callback)

    // Dans les autres cas on récupère l'entity, on la met à jour et on la store
    // C'est nécessaire pour passer dans un beforeStore() correct; après un onLoad sur l'entity existante
    Groupe
      .match('oid').equals(groupe.oid)
      .grabOne(function (err, dbGroupe) {
        if (err) return callback(err)
        if (!dbGroupe) return callback(createError(404, `L'utilisateur avec l'id ${groupe.oid} n'existe pas`))
        Object.assign(dbGroupe, groupe)
        dbGroupe.store(callback)
      })
  }

  /**
   * @callback groupesCallback
   * @param {Erorr} [error]
   * @param {Groupe[]} groupes
   */
  /**
   * Retourne tous les groupes comprenants au moins un des utilisateurs donnés.
   *
   * @param {string[]} usersIds Tableau contenant les identifiants des utilisateurs
   * @param {groupesCallback} callback
   * @memberof $groupe
   */
  function withUsers (usersIds, callback) {
    function grab (skip) {
      Groupe
        .match('utilisateurs').in(usersIds)
        .includeDeleted()
        .grab({ limit: maxResults, skip }, (error, groupes) => {
          if (error) return callback(error)
          allGroupes = allGroupes.concat(groupes)
          if (groupes.length === maxResults) grab(skip + maxResults)
          else callback(null, allGroupes)
        })
    }

    let allGroupes = []
    if (!usersIds.length) return callback(null, allGroupes)
    grab(0)
  }

  return {
    addStudent,
    byOwner,
    findByNom,
    findOrCreateClasse,
    hasStudent,
    isClass,
    removeStudents,
    storeGroup,
    withUsers
  }
})
