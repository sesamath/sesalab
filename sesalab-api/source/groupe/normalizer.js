// on crée ce petit module pour pouvoir être inclus sans rentrer dans l'ordre de gestion des dépendances lassi
// pour le moment seul Groupe l'utilise pour son index nom, mais d'autres pourrait vouloir s'en servir pour
// faire des listes de groupe d'après la même clé d'unicité (l'import par ex)

// à mettre ailleurs quand on y ajoutera d'autres normalizers

/**
 * Normalise le nom de groupe pour déduplication
 * (utilisé par l'index nom)
 * Le trim est déjà fait au beforeStore, donc avant l'appel des normalizer, mais on le rajoute ici
 * au cas où on utilise cette fct hors du workflow d'une entity
 * @param {string} nom
 * @return {string} éventuellement vide si nom n'était pas une string (ou était une chaîne vide)
 */
module.exports = (nom) => (typeof nom === 'string' && nom.trim().toLowerCase().replace(/\s+/g, '_')) || ''
