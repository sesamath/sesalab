/**
 * @module controllers/import
 */
const constants = require('sesalab-commun/constants')
const flow = require('an-flow')
const { logIfError } = require('sesalab-commun/helpers')
const { parseOndeFile, parseSiecleFile } = require('./parsers')

const getCacheKey = (structureOid) => `processing_import_${structureOid}`

let $structure

/**
 * API liées à l'import.
 *
 * @controller Import
 * @see https://wiki.sesamath.net/doku.php?id=labomep:v2:import_eleves
 */
app.controller(function ($cache, $import) {
  const IMPORT = Object.freeze({
    DEFAULT: 'DEFAULT',
    ONDE: 'ONDE',
    SIECLE: 'SIECLE'
  })

  /**
   * Fonction commune d'import de groupes depuis un fichier
   * @private
   * @param {string} importType une des deux constantes IMPORT.ONDE ou IMPORT.SIECLE
   * @param {Context} context
   */
  function importGroupesFromFile (importType, context) {
    if (!$structure) $structure = lassi.service('$structure')
    const { structureOid, file, loginFormat } = context.post
    if (!file) return context.restKo('Aucun fichier fourni.')
    if (!structureOid) return context.restKo('Aucune structure fournie.')
    if (!context.isFormateurInStructure(structureOid)) return context.accessDenied()
    context.timeout = constants.IMPORT_TIMEOUT

    // on a pas l'uai en session et on en aura besoin pour siecle, faudra retourner en db pour vérifier
    let structureUai
    flow().seq(function () {
      ensuresImportIsUniq(context, structureOid, this)
    }).seq(function () {
      if (importType === IMPORT.ONDE) return this()
      $structure.grabByOid(structureOid, this)
    }).seq(function (structure) {
      if (importType === IMPORT.ONDE) return parseOndeFile(file, this)
      structureUai = structure && structure.uai
      if (importType === IMPORT.SIECLE) return parseSiecleFile(file, this)
      this(Error('Type d’import non géré'))
    }).seq(function ({ uai, eleves, degre }) {
      if (uai && structureUai && uai !== structureUai) return this(Error(`Le fichier ${importType} concerne la structure ${uai} mais vous êtes actuellement connecté sur la structure ${structureUai}`))
      $import.importClassesAndEleves(structureOid, degre, eleves, loginFormat, this)
    }).seq(function (stats) {
      flushCache(structureOid)
      context.rest({ stats })
    }).catch(function (error) {
      flushCache(structureOid)
      context.restKo(error)
    })
  }

  /**
   * Importe un ensemble de classes depuis un formulaire
   *
   * @param {Context} context Type de context
   */
  function importGroupesFromForm (context) {
    const { loginFormat, structureOid } = context.post
    if (!structureOid) return context.restKo('Aucune structure fournie')
    if (!context.isFormateurInStructure(structureOid)) return context.accessDenied()
    if (!loginFormat) return context.restKo('Format d’identifiant manquant')
    const { eleves } = context.post
    if (!eleves) return context.restKo('Eleves manquants')
    context.timeout = constants.IMPORT_TIMEOUT

    flow().seq(function () {
      ensuresImportIsUniq(context, structureOid, this)
    }).seq(function () {
      $import.importClassesAndEleves(structureOid, null, eleves, loginFormat, this)
    }).seq(function (stats) {
      flushCache(structureOid)
      context.rest({ stats })
    }).catch(function (error) {
      flushCache(structureOid)
      context.restKo(error)
    })
  }

  /**
   * Vérifie si un import est déjà en cours sinon bloque les imports suivants.
   *
   * @param {Context} context
   * @param {string} structureOid
   * @param {function} callback
   */
  function ensuresImportIsUniq (context, structureOid, callback) {
    const cacheKey = getCacheKey(structureOid)
    $cache.get(cacheKey, (error, isProcessing) => {
      if (error) {
        console.error(error)
        return context.restKo('Erreur interne, impossible de savoir si un import est déjà en cours')
      }
      if (isProcessing) {
        return context.restKo('Un import est déjà en cours, merci de patienter')
      }
      $cache.set(cacheKey, true, 600, callback)
    })
  }

  /**
   * Efface le flag d'import en cours
   * @param {string} structureOid
   */
  function flushCache (structureOid) {
    $cache.delete(getCacheKey(structureOid), logIfError)
  }

  /**
   * Import tableur.
   *
   * @name ImportTableur
   * @route {PUT} /api/import-tableur
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {object[]} eleves Liste des élèves à importer
   */
  this.put('api/import-tableur', importGroupesFromForm)

  /**
   * Importe un fichier CSV issu de ONDE
   * Le fichier posté à été ajouté à req.body par notre middleware qui traite les fichiers dans sesalab-api/source/index.js).
   *
   * @name ImportOnde
   * @route {POST} /api/import-onde
   * @authentication Cette route nécessite un formateur connecté
   * @bodyparam {Structure} structure Une structure
   * @bodyparam {string} loginFormat Format d'import des identifiants
   * @bodyparam {File} file Fichier au format ONDE
   */
  this.post('api/import-onde', importGroupesFromFile.bind(null, IMPORT.ONDE))

  /**
   * Importe un fichier XML issu de siecle
   * Le fichier posté à été ajouté à req.body par notre middleware qui traite les fichiers dans sesalab-api/source/index.js).
   *
   * @name ImportSiecle
   * @route {POST} api/import-siecle
   * @authentication Cette route nécessite un formateur connecté
   * @bodyparam {Structure} structure Une structure
   * @bodyparam {string} loginFormat Format d'import des identifiants
   * @bodyparam {File} file Fichier au format SIECLE
   */
  this.post('api/import-siecle', importGroupesFromFile.bind(null, IMPORT.SIECLE))
})
