/**
 * @typedef {Object} import~ClasseData
 * @property {string} nom
 * @property {number} niveau
 * @property {import~EleveData[]} eleves
 */
/**
 * @typedef {Object} import~EleveData
 * @property {string} classe Le nom de la classe
 * @property {string} nom
 * @property {string} prenom
 * @property {string} mail
 * @property {string} login
 * @property {string} password pass en clair
 * @property {string} nationalId
 */
/**
 * Un objet qui sert à lister des clés (à priori toutes les valeurs sont true quand la clé existe)
 * @typedef TruePropertiesList
 */
/**
 * Objet contenant tous les utilisateurs, accessibles suivant différentes clés
 * @typedef {Object} import~IndexedEleves
 * @property {Utilisateur[]} all Tous
 * @property {Object} byNationalId
 * @property {Object} byNormalizedName
 * @property {Object} byLogin
 * @property {TruePropertiesList} loginHash
 */
