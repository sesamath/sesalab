const _ = require('lodash')
const flow = require('an-flow')
const fs = require('fs')
const iconv = require('iconv-lite')
const jschardet = require('jschardet')
const JSZip = require('jszip')
const xml2js = require('xml2js')
const createError = require('../tools/createError')

const ONDE_CSV_SEPARATOR = ';' // pas forcément celui de constants

/**
 * @typedef {Object} import~Data
 * @property {ClasseData[]} classes
 * @property {EleveData[]} eleves
 */
/**
 * @callback import~dataCallback
 * @param {Error} [error]
 * @param {Data} data
 */
/**
 * Transforme un fichier au format Onde en un tableau de classes/élèves.
 *
 * @param {File} file
 * @param {dataCallback} callback
 */
function parseOndeFile (file, callback) {
  const ext = file.name.match(/\.([a-z]+)$/)[1]
  if (ext !== 'csv') return callback(new Error(`Extension de fichier non gérée : ${ext}`))

  flow().seq(function () {
    fs.readFile(file.path, this)
  }).seq(function (buffer) {
    // Supprime le fichier désormais inutile
    fs.unlink(file.path, () => {})

    // On récupère dynamiquement l'encoding du fichier XML à traiter
    const charsetMatch = jschardet.detect(buffer)
    // Node ne supporte pas nativement l'encoding ISO-8859-15, encoding généralement présent sur le CSV à traiter
    const decodedString = iconv.decode(buffer, charsetMatch.encoding)

    // Construction d'un tableau contenant chaque ligne du CSV
    const rows = decodedString.toString().split('\n')
    const elevesDatas = []
    const headers = rows.shift().split(ONDE_CSV_SEPARATOR).map((cell) => _.trim(cell, [' ', '"']))
    // rows n'a plus la ligne de titre
    rows.forEach(row => {
      const eleveData = {}
      row.split(ONDE_CSV_SEPARATOR).forEach((cell, col) => {
        // on construit nos datas d'après les headers plutôt que la position,
        // histoire de planter si on retrouve pas ce qu'on s'attend à trouver
        // le jour où onde changera de format
        eleveData[headers[col]] = _.trim(cell, [' ', '"'])
      })
      elevesDatas.push(eleveData)
    })

    const eleves = []
    elevesDatas.forEach((eleveData) => {
      const nom = eleveData['Nom d’usage élève'] || eleveData['Nom élève']
      const prenom = eleveData['Prénom élève']
      if (!nom && !prenom) return

      eleves.push({
        nom,
        prenom,
        nationalId: eleveData.INE,
        classeNom: eleveData['Libellé classe'] || null,
        login: null,
        password: null
      })
    })

    callback(null, { eleves, degre: 'primaire' })
  }).catch(callback)
}

/**
 * Transforme un fichier au format SIECLE en un tableau de classes/élèves.
 *
 * @param {File} file
 * @param {function} callback
 */
function parseSiecleFile (file, callback) {
  const ext = file.name.match(/\.([a-z]+)$/)[1]
  if (['xml', 'zip'].includes(ext) === false) return callback(new Error(`Extension de fichier non gérée : ${ext}`))

  flow()
    .seq(function () {
      fs.readFile(file.path, this)
    })
    .seq(function (buffer) {
      // Supprime le fichier désormais inutile
      fs.unlink(file.path, () => {})

      // Nous avons décodé la chaîne base 64 envoyée, nous pouvons maintenant sauvegarder un fichier temporaire tmp.xml ou tmp.zip
      if (ext === 'xml') return this(null, buffer)

      // Le fichier est un zip, on doit utiliser JS ZIP pour le lire et parcourir ses sous-fichiers
      JSZip.loadAsync(buffer)
        .then(zip => {
          const files = Object.keys(zip.files) // On cherche le premier fichier XML dans le zip (normalement il n'y en a qu'un seul)
          const xmlFilename = _.find(files, (f) => f.slice(-3) === 'xml')
          zip.files[xmlFilename].async('nodebuffer')
            .then(fileData => this(null, fileData))
            .catch(this)
        })
        .catch(this)
    })
    .seq(function (buffer) {
      // Node ne supporte pas nativement l'encoding ISO-8859-1,
      // encoding généralement présent sur le fichier XML à traiter
      parseSiecleXml(iconv.decode(buffer, jschardet.detect(buffer).encoding), callback)
    })
    .catch(callback)
}

function parseSiecleXml (xml, next) {
  const parser = new xml2js.Parser()
  parser.parseString(xml, function (error, data) {
    if (error) return next(error)
    try {
      const uai = data.BEE_ELEVES.PARAMETRES[0].UAJ[0]
      const elevesById = {}

      // boucle eleves
      data.BEE_ELEVES.DONNEES[0].ELEVES[0].ELEVE.forEach(function (eleve) {
        const id = eleve.$.ELEVE_ID
        const nom = (eleve.NOM && eleve.NOM[0]) ||
          (eleve.NOM_USAGE && eleve.NOM_USAGE[0]) ||
          (eleve.NOM_DE_FAMILLE && eleve.NOM_DE_FAMILLE[0])
        const prenom = eleve.PRENOM && eleve.PRENOM[0]
        if (nom || prenom) {
          elevesById[id] = {
            nom,
            prenom,
            nationalId: eleve.ID_NATIONAL && eleve.ID_NATIONAL[0]
          }
        } else {
          elevesById[id] = {
            error: `élève d’identifiant SIECLE ${id} sans nom ni prénom`
          }
        }
      })

      // boucle classes
      data.BEE_ELEVES.DONNEES[0].STRUCTURES[0].STRUCTURES_ELEVE.forEach(function (structure) {
        // les attributs du tag donnent l'élève
        const eleveId = structure.$.ELEVE_ID
        if (!elevesById[eleveId]) {
          console.error(Error(`Élève siecle ${eleveId} trouvé dans la boucle classe (BEE_ELEVES.DONNEES[0].STRUCTURES[0].STRUCTURES_ELEVE) mais pas dans les élèves (BEE_ELEVES.DONNEES[0].ELEVES[0].ELEVE)`))
          return
        }
        // boucle sur ses groupes
        structure.STRUCTURE.some(function (s) {
          if (s.TYPE_STRUCTURE[0] === 'D') {
            // c'est une division (sinon, G pour groupe, on ignore pour le moment)
            elevesById[eleveId].classeNom = s.CODE_STRUCTURE[0]
            return true // pour sortir de la boucle some
          }
          return false
        })
      })

      next(null, { uai, eleves: Object.values(elevesById), degre: 'secondaire' })
    } catch (error) {
      console.error(error)
      next(createError(400, 'Le xml reçu n’a pas la structure attendue, êtes-vous sûr qu’il s’agit d’un export siecle ?'))
    }
  })
}

module.exports = {
  parseOndeFile,
  parseSiecleFile
}
