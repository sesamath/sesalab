/**
 * @module services/$import
 */
const flow = require('an-flow')
const _ = require('lodash')
const constants = require('sesalab-commun/constants')
const isEmail = require('sesalab-commun/isEmail')
const { toAscii } = require('sesajstools')
const normalizer = require('../groupe/normalizer')

const cleanStr = (str) => toAscii(str.toLowerCase()) // minuscules sans accents
  .replace(/[^a-z0-9_\-.]/g, '') // sans caractères autres que a-z0-9_-.

const { guessNiveau } = require('sesatheque-client/dist/server/config')

/**
 * @module services/$import
 */

/**
 * @service $import
 */
app.service('$import', function (Groupe, Utilisateur, $structure, $utilisateur, $validators) {
  /**
   * Génère un login unique (d'après le loginFormat, en ajoutant un suffixe numérique si nécessaire)
   * et l'ajoute à loginHash
   * @private
   * @param {Utilisateur} utilisateur
   * @param {string} loginFormat Format de login souhaité
   * @param {object} loginHash liste des logins (key login & value true)
   * @returns {string} Le login généré (ou pris dans utilisateur mais passé en minuscules)
   * @memberof $import
   */
  function generateLogin (utilisateur, loginFormat, loginHash) {
    let login = utilisateur.login
    if (login) {
      login = login.toLowerCase().trim()
    } else {
      // Pas de login fourni, on le crée selon le format fourni
      const nom = cleanStr(utilisateur.nom)
      const prenom = cleanStr(utilisateur.prenom)
      switch (loginFormat) {
        case 'nom.prenom': login = nom + '.' + prenom; break
        case 'prenom.nom': login = prenom + '.' + nom; break
        case 'nomp': login = nom + prenom.substr(0, 1); break
        case 'pnom': login = prenom.substr(0, 1) + nom; break
        case 'nom.p': login = nom + '.' + prenom.substr(0, 1); break
        case 'p.nom': login = prenom.substr(0, 1) + '.' + nom; break
        default: throw Error(`format de login non géré (${loginFormat})`)
      }
    }

    // On ajoute un suffixe si le login existe déjà
    let k = 2
    const baseLogin = login
    while (loginHash[login]) login = baseLogin + k++

    // On ajoute le login créé à la liste pour éviter les doublons
    loginHash[login] = true

    return login
  }

  /**
   * Fonction d'aide permettant la création de tableaux indexés
   * @private
   * @param {Utilisateur[]} utilisateurs
   * @return {IndexedEleves} Un object contenant un ensemble de tableaux indexés
   * @memberof $import
   */
  function createIndexes (utilisateurs) {
    const indexedEleves = {
      all: utilisateurs,
      byNationalId: {},
      byNormalizedName: {}, // un array pour chaque normalizedName
      byLogin: {},
      loginHash: {} // Sera utilisé pour garder des login uniques
    }

    utilisateurs.forEach((user) => {
      // Il ne faut pas qu'un élève importé ait le même login qu'un formateur de la structure
      indexedEleves.loginHash[user.login] = true
      // c'est la seule chose qu'on conserve pour un formateur
      if (user.type === constants.TYPE_FORMATEUR) return

      const normalizedName = $utilisateur.normalizeFullname(user)

      // Il est autorisé d'avoir des logins doublons en corbeille.
      // Si dans un import on cible un login à mettre à jour en particulier, on préférera
      // mettre à jour l'élève qui n'est pas dans la corbeille plutôt que de restaurer un qui est en corbeille.
      if (!indexedEleves.byLogin[user.login] || indexedEleves.byLogin[user.login].isDeleted()) {
        indexedEleves.byLogin[user.login] = user // si user est aussi deleted ça remplace un deleted par un autre
      }

      if (!indexedEleves.byNormalizedName[normalizedName]) indexedEleves.byNormalizedName[normalizedName] = []
      indexedEleves.byNormalizedName[normalizedName].push(user)

      if (user.nationalId) {
        // Comme pour le login on préfére mettre à jour un utilisateur qui n'est pas en corbeille
        // si il y a plusieurs élèves avec le même code INE.
        if (!indexedEleves.byNationalId[user.nationalId] || indexedEleves.byNationalId[user.nationalId].isDeleted()) {
          indexedEleves.byNationalId[user.nationalId] = user
        }
      }
    })

    return indexedEleves
  }

  /**
   * @callback import~ensureClasseCallback
   * @param {Error} error
   * @param {Object} result
   * @param {Groupe} result.classe
   * @param {boolean} result.isUpdated
   * @param {boolean} result.isNew
   */
  /**
   * Recherche une classe dans la collection donnée.
   * - Si la classe existe on la retourne simplement
   * - Si la classe existe mais est en corbeille on la restaure
   * - Sinon on crée une nouvelle classe
   * Contrairement à $groupe.findOrCreateClasse cette fonction recherche dans une collection et non en BDD
   * @private
   * @param {string} nom
   * @param {Groupe[]} classes
   * @param {string} structureOid
   * @param {string} [degre] primaire|secondaire, pour aider guessNiveau
   * @param {ensureClasseCallback} callback
   * @memberof $import
   */
  function ensuresClasseExists (nom, classesByNormalizedName, structureOid, degre, callback) {
    const normalizedName = normalizer(nom)
    const classe = classesByNormalizedName[normalizedName]
    if (classe) {
      if (classe.isDeleted()) {
        // La classe existe en corbeille : on la restaure, mais sans restaurer ses élèves
        classe.utilisateurs = []
        classe.restore((error, classe) => {
          if (error) return callback(error)
          // Mise à jour de la collection pour éviter de multiples restaurations
          classesByNormalizedName[normalizedName] = classe
          callback(null, { classe, isUpdated: true })
        })
      } else {
        callback(null, { classe })
      }
    } else {
      // Création de la classe
      Groupe.create({
        nom,
        niveau: guessNiveau(nom, degre),
        structure: structureOid,
        isClass: true
      }).store((error, classe) => {
        if (error) return callback(error)
        classesByNormalizedName[normalizedName] = classe
        callback(null, { classe, isNew: true })
      })
    }
  }

  /**
   * @typedef import~importEleveCallbackParam
   * @property {Utilisateur} eleve
   * @property {string} message
   * @property {boolean} isEleveUpdated
   * @property {boolean} isEleveCreated
   * @property {boolean} isEleveRestored
   * @property {boolean} aChangeDeClasse
   * @property {string} nouveauPassword
   * @property {string} passwordComment
   */
  /**
   * @callback import~importEleveCallback
   * @param {importEleveCallbackParam}
   */

  /**
   * @typedef EleveDataSource
   * @property {string} nom
   * @property {string} prenom
   * @property {string} [login]
   * @property {string} [mail]
   * @property {string} [password] Le nouveau pass à affecter (en clair)
   * @property {string} [nationalId]
   */
  /**
   * Importe un élève
   * @private
   * @param {EleveDataSource} eleveToImport Eleve a importer
   * @param {Groupe} classe
   * @param {Object} indexedEleves Objet contenant les élèves de la structure indexés sous différents formats
   * @param {string} loginFormat Format de login à adopter
   * @param {importEleveCallback} next
   * @memberof $import
   */
  function importEleve (eleveToImport, classe, indexedEleves, loginFormat, next) {
    if (!classe || !classe.oid) return next(Error('Pas de classe fournie'))
    let isEleveRestored = false
    // le commentaire éventuel pour le rapport
    let passwordComment
    // Le mot de passe qui sera enregistré (restera undefined si on laisse celui d'un utilisateur existant)
    let nouveauPassword

    const existingWithLogin = eleveToImport.login && indexedEleves.byLogin[eleveToImport.login]
    const existingWithNationalId = eleveToImport.nationalId && indexedEleves.byNationalId[eleveToImport.nationalId]

    // Si l'import ne précise pas de mail ou s'il n'a pas le format souhaité, on ne le prend pas en compte
    if (!eleveToImport.mail || !isEmail.validate(eleveToImport.mail)) delete eleveToImport.mail

    // Inutile de continuer pour un élève sans nom ni prénom
    const normalizedFullName = $utilisateur.normalizeFullname(eleveToImport, false)
    if (!normalizedFullName) {
      const error = `L’élève ${eleveToImport.login || eleveToImport.nationalId} n’a pas été importé car celui-ci n’a ni nom ni prénom.`
      return next(null, { error })
    }

    /* REGLES DE GESTION DES CONFLITS LORS DE L'IMPORT */
    // - Si le login est précisé
    //   - s'il existe déjà dans cet établissement, faire un update (cas 1)
    //   - sinon create (sauf si cas 2)
    // - Si l'identifiant national est précisé et qu'il existe déjà dans cet établissement,
    //   lui mettre le login déjà associé à cet identifiant national et faire un update (cas 2)
    // - Si ni le login, ni l'identifiant national ne sont précisés, on regarde les collisions
    //   sur le normalizedFullName (parmi les présents et souhaités)
    //   - si aucun autre normalizedFulname identique, on génère un login, en incrémentant si besoin
    //     pour qu'il soit unique, et on crée
    //   - s'il y a un autre normalizedFullname existant et si le login n'est pas renseigné, on ne sait pas s'il faut
    //     mettre à jour ou créer, on le signale avec un message d'erreur

    let eleveExisting
    const homonymes = _.filter(indexedEleves.byNormalizedName[normalizedFullName] || [], (e) => !e.isDeleted())

    // Cas 1
    if (existingWithLogin) {
      eleveExisting = existingWithLogin
    } else if (existingWithNationalId) {
      // Cas 2
      eleveExisting = existingWithNationalId
    } else if (!eleveToImport.login && homonymes.length && (!eleveToImport.nationalId || _.some(homonymes, (e) => !e.nationalId))) {
      // Cas 3 : risque de conflit avec un homonyme existant : si l'utilisateur courant n'a pas de nationalId
      // et qu'il existe des homonymes, ou inversement si il existe des homonymes sans nationalId
      // (y'a pas de nationalId qui match sinon on serait passé en cas 2)
      // Note: on ignore les homonymes en corbeille
      const error = `Le compte élève ${eleveToImport.prenom} ${eleveToImport.nom} n’a pas été importé car il existe déjà un élève ayant ce nom avec l’identifiant ${homonymes[0].login}. Vous devez ajouter ou mettre à jour cet élève manuellement (ou refaire un import en précisant son identifiant).`
      return next(null, { eleve: eleveToImport, error })
    } else {
      // pas d'existant et pas de pb d'homonyme, on crée un user
      const nouveauPassword = eleveToImport.password || $utilisateur.generateUnPassword()
      const eleveData = {
        type: constants.TYPE_ELEVE,
        nom: eleveToImport.nom,
        prenom: eleveToImport.prenom,
        login: generateLogin(eleveToImport, loginFormat, indexedEleves.loginHash),
        password: nouveauPassword,
        mustResetPassword: true,
        classe: classe.oid,
        structures: [classe.structure]
      }
      const eleve = Utilisateur.create(eleveData)
      $validators.setValid(eleve)

      return $utilisateur.hashPasswordAndStoreUtilisateur(eleve, (error, storedEleve) => {
        if (error) return next(error, eleveToImport)
        next(null, {
          eleve: storedEleve,
          passwordComment: eleveToImport.password ? 'Affecté comme demandé' : 'Généré automatiquement',
          nouveauPassword,
          message: `L’élève ${eleveToImport.prenom} ${eleveToImport.nom} (${classe.nom}) a été importé.`,
          isEleveCreated: true
        })
      })
    }

    // si on est toujours là, y'a un eleveExisting
    if (eleveExisting.isDeleted()) {
      isEleveRestored = true
      eleveExisting.markToRestore()

      // Ici on va restaurer un élève de la corbeille donc il y a un risque de créer un conflit avec
      // un login utilisé par un élève hors corbeille.
      //
      // Il n'y a que deux cas où on trouve un eleveExisting dans la corbeille :
      //    - par login, mais si on a trouvé un élève de la corbeille c'est qu'il n'y en avait pas avec
      //      ce login hors de la corbeille (cf. createIndexes), donc pas de risque de conflit
      //
      //    - par nationalId SANS imposer son login (sinon c'est par login qui prime),
      //      mais dans ce cas ce sera réglé à la génération du login
      if (!eleveToImport.login) {
        // C'est un élève en corbeille récupéré par son nationalId, et le login n'était pas précisé
        // (mais l'élève récupéré en a un)
        // => on ne modifie le login existant qu'en cas de collision
        // car rappeler generateLogin va ajouter un suffixe inutile vu que ce login est déjà dans loginHash)
        const sameLogin = indexedEleves.byLogin[eleveExisting.login]
        if (sameLogin.oid !== eleveExisting.oid && !sameLogin.isDeleted()) {
          eleveExisting.login = generateLogin(eleveToImport, loginFormat, indexedEleves.loginHash)
        }
      }
    }

    // On conserve le password existant si l'import n'en précise pas ET que l'élève n'est pas dans la corbeille
    if (!eleveToImport.password && !isEleveRestored) {
      nouveauPassword = undefined
      passwordComment = 'Ancien conservé'
      // on ne touche pas à mustResetPassword, qui peut être true
    } else if (eleveToImport.password) {
      // l'import impose un mot de passe, on ne lui impose aucune contrainte
      // mais on obligera l'élève à en saisir un nouveau à sa 1re connexion
      nouveauPassword = eleveToImport.password
      eleveExisting.password = nouveauPassword
      passwordComment = 'Affecté comme demandé'
      eleveExisting.mustResetPassword = true
    } else {
      // on en génère un nouveau
      nouveauPassword = $utilisateur.generateUnPassword()
      eleveExisting.password = nouveauPassword
      passwordComment = 'Généré automatiquement'
      eleveExisting.mustResetPassword = true
    }

    let isEleveUpdated = false
    const aChangeDeClasse = eleveExisting.classe !== classe.oid
    if (aChangeDeClasse) eleveExisting.classe = classe.oid
    isEleveUpdated = aChangeDeClasse ||
      isEleveRestored ||
      (eleveToImport.login && eleveExisting.login !== eleveToImport.login)

    for (const p of ['nom', 'prenom']) {
      if (eleveExisting[p] !== eleveToImport[p]) {
        eleveExisting[p] = eleveToImport[p]
        isEleveUpdated = true
      }
    }
    if (eleveToImport.mail && eleveExisting.mail !== eleveToImport.mail) {
      eleveExisting.mail = eleveToImport.mail
      isEleveUpdated = true
    }

    // Message à afficher
    let message
    if (isEleveUpdated) {
      message = `L’élève ${eleveToImport.prenom} ${eleveToImport.nom} (${classe.nom})`
      if (isEleveRestored) message += ' était dans la corbeille et'
      message += ' a été mis à jour'
      if (aChangeDeClasse) message += ' (il a changé de classe)'
      message += '.'
    }

    const surchargedCallback = (error, storedEleve) => {
      if (error) return next(error, eleveToImport)
      next(null, { eleve: storedEleve, message, isEleveUpdated, aChangeDeClasse, isEleveRestored, nouveauPassword, passwordComment, isEleveCreated: false })
    }
    if (nouveauPassword) $utilisateur.hashPasswordAndStoreUtilisateur(eleveExisting, surchargedCallback)
    else eleveExisting.store(surchargedCallback) // on ne re-hash pas le password
  } // importEleve

  /**
   * @typedef import~StatEleve
   * @property {string} oid
   * @property {string} nom
   * @property {string} prenom
   * @property {string} login
   * @property {string} mail
   * @property {string} classe le nom de la classe
   * @property {boolean} isEleveCreated
   * @property {boolean} isEleveUpdated
   * @property {boolean} aChangeDeClasse
   * @property {boolean} isEleveRestored
   * @property {boolean} nouveauPassword
   * @property {boolean} passwordComment
   * @property {string} [initialLogin]
   * @property {string} [error]
   */
  /**
   * @typedef import~Stats
   * @property {StatEleve[]} eleves
   * @property {string[]} errors liste de messages d'erreur
   * @property {number} nbGroupsAdded
   * @property {number} nbGroupsUpdated
   * @property {number} nbUsersAdded
   * @property {number} nbUsersUpdated
   * @property {number} nbUsersIgnored
   */
  /**
   * @callback import~statsCallback
   * @param {Error} error
   * @param {Stats} stats
   */
  /**
   * Importe l'ensemble des élèves données dans la structure
   *
   * @param {string} structureOid
   * @param {string} [degre] primaire|secondaire si on le sait (pour rendre guessNiveau plus pertinent)
   * @param {EleveData[]} elevesImport Collection d'élèves à importer
   * @param {string} loginFormat
   * @param {statsCallback} callback
   * @memberof $import
   */
  function importClassesAndEleves (structureOid, degre, elevesImport, loginFormat, callback) {
    const classesByNormalizedName = {}
    // pour indexer tous les élèves (cf createIndexes, on indexe aussi les login des profs)
    let indexedEleves
    const stats = {
      eleves: [],
      errors: [],
      nbGroupsAdded: 0,
      nbGroupsUpdated: 0,
      nbUsersAdded: 0,
      nbUsersUpdated: 0,
      nbUsersIgnored: 0
    }

    // Inutile de continuer s'il n'y a rien à importer
    if (!elevesImport || !elevesImport.length) return callback(null, stats)

    flow().seq(function () {
      // récup des classes existantes
      $structure.getClasses(structureOid, true, this)
    }).seq(function (classes) {
      // on indexe
      classes.forEach(classe => {
        classesByNormalizedName[normalizer(classe.nom)] = classe
      })
      // récup des élèves existants (+profs pour leurs logins)
      $structure.getAllSpecificUsers(structureOid, { includeDeleted: true }, this)
    }).seq(function (users) {
      indexedEleves = createIndexes(users)
      this(null, elevesImport)
    }).seqEach(function (eleveImport) {
      const nextEleve = this
      // s'il a pas de classe on s'arrête là
      if (!eleveImport.classeNom) {
        stats.nbUsersIgnored++
        stats.eleves.push({ ...eleveImport, error: 'Élève sans classe' })
        return nextEleve()
      }

      let classeNom

      // import de l'élève
      flow().seq(function () {
        // sa classe
        ensuresClasseExists(eleveImport.classeNom, classesByNormalizedName, structureOid, degre, this)
      }).seq(function ({ classe, isNew, isUpdated }) {
        classeNom = classe.nom
        if (isNew) stats.nbGroupsAdded++
        if (isUpdated) stats.nbGroupsUpdated++
        importEleve(eleveImport, classe, indexedEleves, loginFormat, this)
      }).seq(function ({ eleve, passwordComment, nouveauPassword, isEleveCreated, isEleveUpdated, error, message, aChangeDeClasse, isEleveRestored }) {
        // Mise à jour des stats
        if (isEleveCreated) stats.nbUsersAdded++
        if (isEleveUpdated) stats.nbUsersUpdated++
        if (error) stats.nbUsersIgnored++

        // pour le rapport détaillé
        const eleveResult = eleve ? _.pick(eleve, ['oid', 'nom', 'prenom', 'mail', 'login']) : {}
        stats.eleves.push(Object.assign(eleveResult, {
          classeNom,
          error,
          message,
          isEleveCreated,
          isEleveUpdated,
          isEleveRestored,
          aChangeDeClasse,
          nouveauPassword,
          passwordComment,
          initialLogin: eleveImport.login
        }))

        nextEleve(null, eleve)
      }).catch(function (error, data) {
        stats.nbUsersIgnored++
        // on ajoute les infos élève si y'en a
        if (data && data.eleve) {
          const eleveResult = _.pick(data.eleve, ['oid', 'nom', 'prenom', 'mail', 'login'])
          eleveResult.error = error
          eleveResult.classe = eleveImport.classeNom
          stats.eleves.push(eleveResult)
        } else {
          stats.errors.push(error.message)
        }
        // et on continue
        nextEleve()
      })
      // fin du flow de l'import de cet élève
    }).seq(function () {
      // pas la peine de mettre à jour les classes, c'est fait dans l'afterStore utilisateur
      callback(null, stats)
    }).catch(callback)
  }

  return {
    importClassesAndEleves
  }
})
