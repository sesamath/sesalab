const flow = require('an-flow')
const _ = require('lodash')
const { DEFAULT_TIMEZONE, TYPE_ELEVE, TYPE_FORMATEUR } = require('sesalab-commun/constants.js')

const purgeStructure = require('./purgeStructure')

// const isArrayNotEmpty = a => Boolean(Array.isArray(a) && a.length)

/**
 * @Service $structure
 */
app.service('$structure', function (Groupe, Structure, Utilisateur, $session, $settings) {
  const maxResults = $settings.get('application.maxResults', 500)

  /**
   * Compte le nombre de classes pour une structure donnée
   *
   * @param {string}   structureOid L'identifiant interne de la structure
   * @param {Function} callback     Callback
   */
  function countClasses (structureOid, callback) {
    Groupe
      .match('structure').equals(structureOid)
      .match('isClass').equals(true)
      .count(callback)
  }

  /**
   * Récupère le nb d'utilisateurs de la structure (tous types confondus, non validés exclus)
   * @param structureOid
   * @param callback
   */
  function countUsers (structureOid, callback) {
    Utilisateur
      .match('structures').equals(structureOid)
      .match('isValid').equals(true)
      .count(callback)
  }

  /**
   * Récupère tous les utilisateurs souhaités d'une structure (sans limite de nombre,
   * à n'utiliser qu'en interne mais pas pour renvoyer une liste via l'api)
   *
   * @param {string}   structureOid L'identifiant interne de la structure
   * @param {Object}   options      Options pour filtrer les utilisateurs
   * @param {string}   [options.type] une des constantes TYPE_ELEVE ou TYPE_FORMATEUR
   * @param {boolean}  [options.onlyDeleted=false]
   * @param {boolean}  [options.includeDeleted=false]
   * @param {boolean}  [options.includeInvalids=false]
   * @param {Function} callback     Callback
   */
  function getAllSpecificUsers (structureOid, options, callback) {
    function grab (skip) {
      const query = Utilisateur
        .match('structures').equals(structureOid)
      if (!options.includeInvalids) {
        query.match('isValid').equals(true)
      }
      if (options.type === TYPE_ELEVE || options.type === TYPE_FORMATEUR) {
        query.match('type').equals(options.type)
      }
      if (options.onlyDeleted) query.onlyDeleted()
      if (options.includeDeleted) query.includeDeleted()

      query.grab({ limit: maxResults, skip }, (error, users) => {
        if (error) return callback(error)
        allUsers = allUsers.concat(users)
        if (users.length === maxResults) grab(skip + maxResults)
        else callback(null, allUsers)
      })
    }

    let allUsers = []
    grab(0)
  }

  /**
   * @callback groupesCallback
   * @param {Error} [error]
   * @param {Groupe[]} groupes les Entities Groupe (toujours un array, éventuellement vide)
   */
  /**
   * Retourne les classes de la structure
   * @param {string} strutureOid
   * @param {boolean} [includeDeleted=false]
   * @param {groupesCallback} next
   * @return {*}
   */
  function getClasses (strutureOid, includeDeleted, next) {
    if (typeof includeDeleted === 'function') {
      next = includeDeleted
      includeDeleted = false
    }
    const query = Groupe
      .match('isClass').equals(true)
      .match('structure').equals(strutureOid)
    if (includeDeleted) query.includeDeleted()
    query.grab(next)
  }

  /**
   * Retourne la liste des oids des élèves (valides) de la structure
   * @param structureOid
   * @param callback
   *
   * finalement pas utilisé (ni testé) /
  function getElevesOids (structureOid, callback) {
    Utilisateur
      .match('structures').equals(structureOid)
      .match('isValid').equals(true)
      .match('type').equals(TYPE_ELEVE)
      .grab({ fields: ['oid'] }, (error, result) => {
        if (error) return callback(error)
        callback(error, result.map(data => data.oid))
      })
  } /* */

  /**
   * Retourne le label de la structure (nom avec type, ville et département si ça existe)
   * @param structure
   * @return {string}
   */
  function getLabel (structure) {
    const chunks = []
    const { type, nom, ville, departementNumero } = structure
    if (type) chunks.push(type)
    chunks.push(nom)
    if (ville) {
      if (departementNumero) chunks.push(`(${ville} - ${departementNumero})`)
      else chunks.push(`(${ville})`)
    }
    return chunks.join(' ')
  }

  /**
   * Retourne la timezone d'une structure
   *
   * @param {object} structure Une instance de Structure
   * @return {string} La timezone sous forme textuelle
   */
  function getTimezone (structure) {
    return (structure && structure.timezone) || DEFAULT_TIMEZONE
  }

  /**
   * Recherche d'une structure par son id (si on en précise plusieurs ça rentournera un array)
   *
   * @param {string} oid L'id interne de la structure
   * @param {function} callback retour
   */
  function grabByOid (oid, callback) {
    if (Array.isArray(oid)) return Structure.match('oid').in(oid).grab(callback)
    Structure.match('oid').equals(oid).grabOne(callback)
  }

  /**
   * Récupère les structures d'après structuresFilter (qui doit avoir été contrôlé avant)
   *
   * @param {object} structuresFilter
   * @param {function} callback appelée avec (error, structures)
   */
  function _grabStructuresWithFilter (structuresFilter, callback) {
    const addMatch = (matcher) => {
      const nomIndex = (matcher === 'origine') ? 'externalSource' : matcher
      const filterValue = structuresFilter[matcher]
      if (!filterValue) return false
      // fct interne à ce service, structuresFilter a normalement déja été vérifié, on contrôle pas de nouveau
      // init query si 1er appel, + match
      if (query) query.match(nomIndex)
      else query = Structure.match(nomIndex)
      // reste le critère
      if (['string', 'number'].includes(typeof filterValue)) query.equals(filterValue)
      else query.in(filterValue)
      return true
    }

    let query
    // on impose un seul filter géographique, avec cet ordre de priorité
    if (_.some(['academieId', 'departementNumero', 'ville', 'oid'], addMatch)) {
      ['origine', 'category'].forEach(addMatch)
      query.grab(callback)
    } else {
      callback(null, [])
    }
  }

  /**
   * Retourne true si value semble une valeur possible pour un filter
   * (number ou string ou Array non vide de string ou number)
   * @private
   * @param value
   * @return {boolean}
   */
  function looksLikeFilterValue (value) {
    if (!value) return false
    if (['string', 'number'].includes(typeof value)) return true
    return Array.isArray(value) && value.length && value.every(item => ['string', 'number'].includes(typeof item))
  }

  /**
   * Retourne le structuresFilter du user courant (gestionnaire)
   * @param {Context} context
   * @return {Object} Avec une propriété academieId|departementNumero|ville|oid qui vaudra un Array non-vide
   * @throws {Error} Si pas gestionnaire ou pas de filter
   */
  function getGestionStructuresFilter (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    // l'appelant devrait contrôler que c'est bien un gestionnaire… mais on plante si c'est pas le cas !
    if (!currentFormateur.$isAdminGestion) {
      const error = new Error('Accès réservé aux gestionnaires')
      // pour que lassi fasse suivre au user final dans beforeTransport
      error.status = 200
      // on veut pas logguer ça
      error.noLog = true
      throw error
    }
    // le code suivant peut planter si la config n'est pas au format attendu
    try {
      const filter = {}
      const [{ structuresFilter }] = $settings.get('gestion').filter(user => user.login === currentFormateur.login && user.nom.toLowerCase() === currentFormateur.nom.toLowerCase())
      // on prend un seul filter géographique, avec cet ordre de priorité
      const hasGeoMatcher = ['academieId', 'departementNumero', 'ville', 'oid'].some((key) => {
        const value = structuresFilter[key]
        if (looksLikeFilterValue(value)) {
          filter[key] = value
          return true
        }
        return false
      })
      if (hasGeoMatcher) {
        // on regarde alors les autres clés autorisées
        ['origine', 'category'].forEach((p) => {
          if (looksLikeFilterValue(structuresFilter[p])) filter[p] = structuresFilter[p]
        })
        return filter
      }
    } catch (error) {
      console.error('Pb dans config.gestion', error)
    }
    const error = new Error('Aucun filtre de structure pour le gestionnaire courant')
    // pour que lassi fasse suivre au user final dans beforeTransport
    error.status = 200
    // pour que ce soit loggé
    error.properties = { currentFormateur }
    throw error
  }

  /**
   * Retourne une clé pour ce filter, pouvant être utilisée comme clé de cache par ex
   * @param filter
   */
  function getStructuresFilterKey (filter) {
    let matcherKey
    let matcherValueSerialized
    const hasGeoMatcher = ['academieId', 'departementNumero', 'ville', 'oid'].some((key) => {
      const value = filter[key]
      if (looksLikeFilterValue(value)) {
        matcherKey = key
        if (Array.isArray(value)) matcherValueSerialized = value.map(String).sort().join('_')
        else matcherValueSerialized = value // ok pour number seul aussi
        return true
      }
      return false
    })
    if (hasGeoMatcher) {
      return `${matcherKey}_${matcherValueSerialized}`
    }
  }

  /**
   * Renvoie la liste des structures concernées par le gestionnaire courant
   * @param context
   * @param callback
   */
  function getStructuresForGestion (context, callback) {
    try {
      const structureMatcher = getGestionStructuresFilter(context)
      _grabStructuresWithFilter(structureMatcher, callback)
    } catch (error) {
      callback(error)
    }
  }

  /**
   * Récupère l'ensemble des auteurs visibles par une structure.
   *
   * @param {string} structureOid
   * @param {Function} callback
   */
  function getAuthorsForNews (structureOid, callback) {
    const gestion = $settings.get('gestion', [])
    const logins = []
    // @FIXME c'est très coûteux de remonter toutes les structures de tous les gestionnaires pour savoir si la structure est dans le périmètre d’un gestionnaire
    // piste possible, indexer chaque message avec le filter du gestionnaire, en utilisant getStructuresFilterKey
    // puis comparer à la structure courante (si un truc match entre oid|ville|departementNumero|academieId)

    flow(gestion)
      .seqEach(function (gestionnaire) {
        const cb = this
        flow()
          .seq(function () {
            // ce code sera à virer mais en l'état il marche par car login n'est pas un index de Structure
            _grabStructuresWithFilter({ login: gestionnaire.login }, this)
          })
          .seq(function (structures) {
            if (_.find(structures, (s) => s.oid === structureOid)) {
              logins.push(gestionnaire.login)
            }

            cb()
          })
          .catch(this)
      })
      .seq(function () {
        callback(null, logins)
      })
      .catch(callback)
  }

  /**
   * Purge tous les comptes élèves de l'établissement (Utilisateur, Groupe, Seance, Resultat)
   * On ne charge pas les élèves un par un car le Utilisateur.beforeDelete est trop gourmand
   *
   * @param {string}   structureOid L'identifiant de la structure
   * @param {Function} callback     Callback
   */
  function purgeEleves (structureOid, callback) {
    if (!structureOid) throw new Error('oid de structure obligatoire')
    if (typeof structureOid !== 'string') throw new Error('oid de structure invalide')
    if (typeof callback !== 'function') throw new Error('erreur interne')

    const result = {
      message: '',
      elevesDeleted: 0,
      elevesCorbeilleDeleted: 0,
      classesDeleted: 0,
      classesCorbeilleDeleted: 0
    }
    const infos = []
    let nbEleves = 0
    let nbClasses = 0

    flow()
      .seq(function () {
        // Nombre d'élèves (hors softDeleted)
        Utilisateur
          .match('structures').equals(structureOid)
          .match('type').equals(TYPE_ELEVE)
          .count(this)
      })
      .seq(function (nb) {
        nbEleves = nb
        // Nombre de classes (hors softDeleted)
        Groupe
          .match('structure').equals(structureOid)
          .count(this)
      })
      .seq(function (nb) {
        nbClasses = nb
        // Purge des élèves
        Utilisateur
          .match('structures').equals(structureOid)
          .match('type').equals(TYPE_ELEVE)
          .softPurge(this)
      })
      .seq(function (nbElevesDeleted) {
        if (nbElevesDeleted) {
          result.elevesDeleted = nbElevesDeleted
          if (nbElevesDeleted >= nbEleves) {
            result.elevesCorbeilleDeleted = nbElevesDeleted - nbEleves
            infos.push(`${nbElevesDeleted} comptes élèves mis dans la corbeille`)
          } else {
          // Quelqu'un en a supprimé pendant notre flow ? Ça parait quand même louche…
            const warningMessage = `La purge signale ${nbElevesDeleted} élèves supprimés alors qu'on en avait ${nbEleves} au départ`
            console.error(new Error(warningMessage))
            infos.push(warningMessage)
          }
        } else if (nbEleves) {
          const warningMessage = `Il y avait ${nbEleves} élèves mais la purge ne signale aucune suppression (un collègue qui purge en même temps ?)`
          console.error(new Error(warningMessage))
          infos.push(warningMessage)
        } else {
          infos.push('Aucun élève à supprimer')
        }

        // Purge des classes
        Groupe
          .match('structure').equals(structureOid)
          .softPurge(this)
      })
      .seq(function (nbClassesDeleted) {
        if (nbClassesDeleted) {
          result.classesDeleted = nbClassesDeleted
          if (nbClassesDeleted >= nbClasses) {
            infos.push(`${nbClassesDeleted} classes mises dans la corbeille`)
            result.classesCorbeilleDeleted = nbClassesDeleted - nbClasses
          } else {
          // qqun en a supprimé pendant notre flow ? Ça parait quand même louche…
            const warningMessage = `La purge signale ${nbClassesDeleted} classes supprimées alors qu'on en avait ${nbClasses} au départ`
            console.error(new Error(warningMessage))
            infos.push(warningMessage)
          }
        } else if (nbClasses) {
          const warningMessage = `Il y avait ${nbClasses} classes mais la purge signale 0 suppression (un collègue qui purge en même temps ?)`
          console.error(new Error(warningMessage))
          infos.push(warningMessage)
        } else {
          infos.push('Aucune classe à supprimer')
        }
        result.message = infos.join(' ;\n')
        callback(null, result)
      })
      .catch(callback)
  }

  return {
    countClasses,
    countUsers,
    getAllSpecificUsers,
    getAuthorsForNews,
    getClasses,
    // getElevesOids,
    getLabel,
    getGestionStructuresFilter,
    getStructuresFilterKey,
    getStructuresForGestion,
    getTimezone,
    grabByOid,
    purgeStructure,
    purgeEleves
  }
})
