// $structure.purge

const flow = require('an-flow')
const { formatDate } = require('sesajs-date')
const { hasProp } = require('sesajstools')

const { OID_DELETED, TYPE_ELEVE } = require('sesalab-commun/constants')

const { getMailTextPurgePostponed, getMailTextPurgeReport } = require('sesalab-api/source/utilisateur/mailTemplates')

// le nb écoulés de j à partir duquel on ne purge plus car ça aurait dû être fait
// (un nouvel import a probablement déjà été fait et il faut pas le purger)
// si le cron plantait pendant plus de ce nb de jours faudra aviser…
const TOO_OLD = 14

// le nb de jour à décaler dans _postponePurge
const POSTPONED_DAYS = 7

let Groupe, Resultat, Seance, Sequence, Utilisateur, UtilisateurAcces, $mail, $settings, $utilisateur

function _init () {
  Groupe = lassi.service('Groupe')
  Resultat = lassi.service('Resultat')
  Seance = lassi.service('Seance')
  Sequence = lassi.service('Sequence')
  Utilisateur = lassi.service('Utilisateur')
  UtilisateurAcces = lassi.service('UtilisateurAcces')
  $mail = lassi.service('$mail')
  $settings = lassi.service('$settings')
  $utilisateur = lassi.service('$utilisateur')
}

/**
 * Notifie les formateurs de la structure que la date de purge a été décalée
 * @param {Structure} structure
 * @param {string} oldPurge
 * @param {string} newPurge
 * @param callback
 * @private
 */
function _notifPostponeToFormateurs (structure, oldPurge, newPurge, callback) {
  const appName = $settings.get('application.name', 'Sesalab')
  flow().seq(function () {
    $mail.setup(this)
  }).seq(function () {
    $utilisateur.grabTeacherByStructure(structure.oid, this)
  }).seqEach(function (utilisateur) {
    const mailData = {
      to: utilisateur.mail,
      subject: 'Anomalie lors de la purge de l’établissement',
      text: getMailTextPurgePostponed({ appName, utilisateur, structure, oldPurge, newPurge })
    }
    $mail.sendMail(mailData, this)
  }).done(callback)
}

/**
 * Envoie un mail de confirmation de purge aux formateurs de la structure
 * @private
 * @param {Structure} structure Une instance de structure
 * @param {boolean} purgeWanted
 * @param {function} callback  Callback
 */
function _notifPurgeToFormateurs (structure, purgeWanted, callback) {
  const appName = $settings.get('application.name', 'Sesalab')
  flow().seq(function () {
    $mail.setup(this)
  }).seq(function () {
    $utilisateur.grabTeacherByStructure(structure.oid, this)
  }).seqEach(function (utilisateur) {
    const mailData = {
      to: utilisateur.mail,
      subject: 'Purge de l’établissement',
      text: getMailTextPurgeReport({ appName, utilisateur, structure, purgeWanted })
    }
    $mail.sendMail(mailData, this)
  }).done(callback)
}

/**
 * Helper de purgeStructure pour décaler la date de purge dans 7j
 * (+ notifier les formateur et passer une erreur à next)
 * @param structure
 * @param options
 * @param next
 * @private
 */
function _postponePurge (structure, options, next) {
  // on vérifie qu'on est bien dans le cas d'une purge voulue y'à plus de TOO_OLD jours
  const datePurgeMs = structure.datePurge.getTime()
  const dateWantedFr = formatDate({ date: structure.datePurge, fr: true })
  const postponeDate = new Date(Date.now() + POSTPONED_DAYS * 24 * 3600_000)
  const postponeDateFr = formatDate({ date: postponeDate, fr: true })
  if (datePurgeMs > Date.now()) {
    return next(Error(`La date de purge de la structure ${structure.oid} est dans le futur ! ${dateWantedFr}`))
  }

  flow().seq(function () {
    structure.datePurge = postponeDate
    structure.store(this)
  }).seq(function () {
    _notifPostponeToFormateurs(structure, dateWantedFr, postponeDateFr, this)
  }).seq(function () {
    const message = `ABANDON : la structure ${structure.oid} a une date de purge dépassée de plus de ${TOO_OLD}j : ${dateWantedFr}. La purge est reportée au ${postponeDateFr}, les formateurs ont été prévenus.`
    next(Error(message))
  }).catch(next)
}

/**
 * Vire les séquences si elles n'ont plus de proprio, sinon vide les élèves
 * + purge Seance et Resultat attachés à ces séquences (restera seulement les résultats
 * des exos en autonomie à purger par oid d'élève)
 * @private
 * @param structureOid Identifiant de la structure
 * @param stats       Statistiques propres à la purge de la structure
 * @param next        Callback
 */
function _purgeStructureSequencesResultatSeance (structureOid, stats, next) {
  if (!hasProp(stats, 'sequencesFlushed')) stats.sequencesFlushed = 0
  if (!hasProp(stats, 'sequencesRemoved')) stats.sequencesRemoved = 0
  if (!hasProp(stats, 'seancesRemoved')) stats.seancesRemoved = 0
  if (!hasProp(stats, 'resultatsRemoved')) stats.resultatsRemoved = 0

  // le nb de séquences par structure peut largement dépasser le max de grab, on passe par forEachEntity
  const onEach = (sequence, next) => {
    const seqOid = sequence.oid
    if (!seqOid) return next(Error('séquence sans oid'))
    flow().seq(function () {
      if (sequence.owner === OID_DELETED) return this(null, 0)
      // sinon on cherche le owner (le OID_DELETED a été introduit en 2022-10, il reste bcp de séquences avec un owner disparu)
      // car si la structure courante ne devrait plus avoir de formateurs,
      // il peut très bien y avoir des séquences dont le owner existe toujours (dans une autre structure)
      Utilisateur.match('oid').equals(sequence.owner).count(this)
    }).seq(function (nbUtilisateur) {
      if (!nbUtilisateur) {
        stats.sequencesRemoved++
        return sequence.delete(this)
      }
      // sinon le owner existe encore, on vide la séquence
      let isMod = false
      sequence.sousSequences.forEach(ssSeq => {
        if (ssSeq.eleves.length) {
          isMod = true
          ssSeq.eleves = []
        }
      })
      if (isMod) {
        stats.sequencesFlushed++
        sequence.store(this)
      } else {
        this()
      }
    }).seq(function () {
      // purge Seance
      Seance.match('sequence').equals(seqOid).purge(this)
    }).seq(function (nb) {
      stats.seancesRemoved += nb
      // purge Resultat, attention les résultats des exos en autonomie ont une séquence undefined
      Resultat.match('sequence').equals(seqOid).purge(this)
    }).seq(function (nb) {
      stats.resultatsRemoved += nb
      next()
    }).catch(next)
  }

  Sequence
    .match('structure').equals(structureOid)
    .includeDeleted()
    .forEachEntity(onEach, next)
}

/**
 * Supprime les classes de la structure
 * @private
 * @param structureOid Identifiant de la structure
 * @param stats       Statistiques propres à la purge de la structure
 * @param next    Callback
 */
function _purgeStructureClasses (structureOid, stats, next) {
  if (!hasProp(stats, 'classesRemoved')) stats.classesRemoved = 0
  Groupe
    .match('structure').equals(structureOid)
    .match('isClass').equals(true) // à priori toujours vrai si y'a une structure
    .includeDeleted()
    .purge((error, nbDeleted) => {
      if (error) return next(error)
      stats.classesRemoved += nbDeleted
      next()
    })
}

/**
 * Supprime les élèves de la structure (et leurs éventuels résultats réalisés en autonomie)
 * @private
 * @param structureOid Identifiant de la structure
 * @param stats       Statistiques propres à la purge de la structures (retourné avec les props resultatsAutonomieRemoved et elevesRemoved
 * @param next    Callback
 */
function _purgeStructureResAutEleves (structureOid, stats, next) {
  if (!hasProp(stats, 'resultatsAutonomieRemoved')) stats.resultatsAutonomieRemoved = 0

  // on doit récupérer les oid des élèves, pour les bilans des exos en autonomie
  const getElevesQuery = () => Utilisateur
    .match('type').equals(TYPE_ELEVE)
    .match('structures').equals(structureOid)
    .includeDeleted()

  const limit = 50
  let skip = 0
  const purgeResultatsAutonomie = () => {
    let isLastBatch
    flow().seq(function () {
      getElevesQuery().grab({ skip, limit }, this)
    }).seq(function (eleves) {
      isLastBatch = eleves.length < limit
      const oids = eleves.map(e => e.oid)
      if (!oids.length) return this(null, 0)
      Resultat
        .match('autonomie').equals(true)
        .match('eleve').in(oids)
        .purge(this)
    }).seq(function (nb) {
      stats.resultatsAutonomieRemoved += nb
      // on purgera les élèves avec purge à la fin plutôt qu'avec user.delete ici
      // (pour shunter les beforeDelete en cascade)
      if (isLastBatch) return purgeEleves()
      skip += limit
      process.nextTick(purgeResultatsAutonomie)
    }).catch(next)
  }

  const purgeEleves = () => {
    getElevesQuery().purge((error, nb) => {
      if (error) return next(error)
      stats.elevesRemoved = nb
      next(null, stats)
    })
  }

  purgeResultatsAutonomie()
}

/**
 * Purge une structure en supprimant
 * - élèves
 * - groupes
 * - séances
 * - résultats
 * - utilisateurAccess
 * @param {Structure} structure
 * @param {object} [options]  Options éventuelles
 * @param {boolean} [options.noCheckDatePurge=false] passer true pour ne pas s'occuper de datePurge
 * @param {boolean} [options.noStore=false] passer true pour ne pas modifier structure (dernierePurge et datePurge)
 * @param {boolean} [options.noNotification=false] passer true pour ne pas notifier les formateurs de la purge effectuée
 * @param next  appelée avec (error, stats)
 */
function purgeStructure (structure, options, next) {
  if (typeof options === 'function') {
    next = options
    options = {}
  }
  if (!Groupe) _init()

  const structureOid = structure.oid
  const stats = {}

  // check datePurge
  if (!options.noCheckDatePurge) {
    // on vérifie que datePurge est dépassé de 0 à TOO_OLD jours
    const datePurgeMs = structure.datePurge.getTime()
    // faut autoriser la purge si c'est aujourd'hui, même plus tard dans la journée
    // (la date de purge est un jour sans heure)
    const endOfTodayMs = (new Date(formatDate() + 'T23:59:59.999')).getTime() // on précise pas Z à la fin pour prendre la fin de journée dans le fuseau local
    const isOld = datePurgeMs < endOfTodayMs
    const isTooOld = datePurgeMs + TOO_OLD * 24 * 3600_000 < endOfTodayMs
    // on exige vieux mais pas trop
    if (!isOld) {
      // purge demandée dans le futur !
      const dateString = formatDate({ date: structure.datePurge, fr: true })
      const message = `ABANDON : la structure ${structure.oid} a une date de purge dans le futur (${dateString})`
      return next(Error(message))
    }
    if (isTooOld) {
      return _postponePurge(structure, options, next)
    }
  }

  flow().seq(function () {
    _purgeStructureSequencesResultatSeance(structureOid, stats, this)
  }).seq(function () {
    _purgeStructureClasses(structureOid, stats, this)
  }).seq(function () {
    _purgeStructureResAutEleves(structureOid, stats, this)
  }).seq(function () {
    UtilisateurAcces.match('structure').equals(structureOid).purge(this)
  }).seq(function (nb) {
    stats.nbAccessRemoved = nb
    if (options.noStore) return this()
    structure.dernierePurge = new Date()
    // on a pas de fonction addYear, et pour rester précis en utilisant addDays
    // il faudrait regarder si l'année courante est bissextile, y'a plus simple :
    const year = structure.datePurge.getFullYear()
    structure.datePurge.setFullYear(year + 1)
    structure.store(this)
  }).seq(function () {
    if (options.noNotification) return this()
    const purgeWanted = formatDate({ date: structure.datePurge, fr: true })
    _notifPurgeToFormateurs(structure, purgeWanted, this)
  }).seq(function () {
    next(null, stats)
  }).catch(next)
}

module.exports = purgeStructure
