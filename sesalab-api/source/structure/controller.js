const flow = require('an-flow')
const constants = require('sesalab-commun/constants.js')
const { addDays, diffSecs, formatDate } = require('sesajs-date')

/**
 * API liées aux structures.
 * @Controller Structure
 */
app.controller(function (Structure, Message, $crypto, $mail, $utilisateur, $session, $settings, $structure) {
  const MAX_STRUCTURES_AUTOCOMPLETE = 100 // Maximum de structures retournées pour l'autocomplete

  const PURGE_MAX_MONTH_INTERVAL = 18 // Maximum months allowed

  /**
   * Récupère une structure suivant les paramètres fournis.
   *
   * @param context Context
   */
  this.get('api/structure', function (context) {
    let baseQuery = Structure

    // queryString http éventuelles (forcément string)
    const qsNom = context.get._nom
    if (qsNom && qsNom.length < constants.MIN_AUTOCOMPLETE_INPUT) {
      return context.rest({
        message: `Il faut ${constants.MIN_AUTOCOMPLETE_INPUT} caractères minimum pour avoir une réponse`
      })
    }
    // @todo Virer ce like pour un autocomplete plus efficace/plus safe, remplacer par un match().regex() à créer dans lassi
    if (qsNom) baseQuery = baseQuery.match('_nom').like('%' + qsNom.toLowerCase() + '%')
    const qsCode = context.get.code
    if (qsCode) baseQuery = baseQuery.match('code').equals(qsCode.toUpperCase())
    if (!qsNom && !qsCode) return context.rest({ message: 'Critère de recherche manquant' })
    const response = {}

    flow()
      .seq(function () {
        baseQuery.count(this)
      })
      .seq(function (count) {
        if (count > MAX_STRUCTURES_AUTOCOMPLETE) {
          response.message = `Plus de ${MAX_STRUCTURES_AUTOCOMPLETE} structures possibles, affinez vos critères`
          return this()
        }
        baseQuery.grab(this)
      })
      .seq(function (structures) {
        if (structures && !context.get.hideClosedStructures) {
          structures = structures.filter(structure => structure.ajoutFormateursInterdit === false)
        }

        if (structures) response.structures = structures
        context.rest(response)
      })
      .catch(context.next)
  })

  /**
   * Retourne la structure en json
   * ATTENTION, si elle n'existe pas ça répond success: true sans structure associée !
   * @param context
   * @param structureOid
   */
  function getStructureResponse (context, structureOid) {
    flow()
      .seq(function () {
        Structure
          .match('oid').equals(structureOid)
          .grabOne(this)
      })

      .seq(function (structure) {
        context.rest({ structure })
      })
      .catch(context.next)
  }

  /**
   * Enregistre la structure courante en session
   */
  this.post('api/current-structure/:oid', function (context) {
    const structureOid = context.arguments.oid
    if (!structureOid) context.restKo('Aucune structure fournie')
    if (!context.isFormateurInStructure(structureOid)) return context.accessDenied()

    $session.updateCurrentStructureOid(context, structureOid)
    context.rest({})
  })

  /**
   * Récupère les news/derniers messages destinés à cette structure (faut aller les chercher par auteur)
   * @FIXME aller chercher les messages par matcher (ceux pour notre oid, puis ceux pour notre département puis ceux pour notre academie)
   * @param context
   */
  this.get('api/current-structure-news', function (context) {
    const structureOid = $session.getCurrentStructureOid(context)
    if (!structureOid) return context.accessDenied('Il faut être authentifié')

    flow()
      .seq(function () {
        $structure.getAuthorsForNews(structureOid, this)
      })
      .seq(function (logins) {
        if (!logins.length) return this(null, [])
        Message
          .match('author').in(logins)
          .match('startDate').lowerThanOrEquals(new Date())
          .match('endDate').greaterThanOrEquals(new Date())
          .sort('endDate', 'asc')
          .grab(this)
      })
      .seq(function (messages) {
        context.rest({ messages })
      })
      .catch(context.next)
  })

  /**
   * Récupère la structure ayant l'identifiant indiqué.
   * ATTENTION, si elle n'existe pas ça répond `{ success: true }` sans structure associée !
   * @param context Context
   */
  this.get('api/structure/:oid', function (context) {
    const structureOid = context.arguments.oid
    if (!structureOid) context.restKo('Aucune structure fournie')
    getStructureResponse(context, structureOid)
  })

  /**
   * Création d'une nouvelle structure.
   *
   * @param context Context
   */
  // On attend 10min entre chaque création de structure par une même ip
  this.post('api/structure', function (context) {
    let structureDb
    flow().seq(function () {
      context.checkThrottle(this)
    }).seq(function () {
      const structure = context.post
      if (!structure) return context.restKo('Aucune structure fournie')
      if (!structure.nom) return context.fieldError('nom', 'Le nom de la structure ne doit pas être vide')
      Structure.create(structure).store(this)
    }).seq(function (structure) {
      structureDb = structure
      context.setThrottle(600, 'Vous ne pouvez pas créer plus d’une structure par tranche de 10 minutes')
      context.rest({ code: structureDb.code, structure: structureDb })
    }).catch(function (error) {
      console.error(error)
      context.restKo(error.message)
    })
  })

  /**
   * Mise à jour d'une structure existante.
   *
   * @param context Context
   */
  this.put('api/structure', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const structure = context.post
    if (!structure) context.restKo('Aucune structure fournie')
    if (!context.isFormateurInStructure(structure.oid)) return context.accessDenied()
    let oldStructure = null
    const changedKeys = []

    flow().seq(function () {
      Structure.match('oid').equals(structure.oid).grabOne(this)
    }).seq(function (dbStructure) {
      // On recherche les données modifiées, inutile de continuer s'il n'y a pas de changements
      for (const key in structure) {
        if (key in structure && structure[key] === dbStructure[key]) continue
        if (key === 'datePurge' && Math.abs(diffSecs(structure.datePurge, dbStructure.datePurge)) < 1) {
          continue
        }
        changedKeys.push(key)
      }
      if (!changedKeys.length) {
        return context.rest({ structure: dbStructure })
      }

      if (changedKeys.includes('datePurge')) {
        const minDatePurge = addDays(new Date(), 3)
        if (diffSecs(structure.datePurge, minDatePurge) < 0) {
          return context.fieldError('datePurge', `La date de purge doit être au plus tôt le ${formatDate({ date: minDatePurge, fr: true })} (pour laisser à vos collègues le temps de récupérer les bilans ou changer cette date)`)
        }
        const maxDatePurge = addDays(new Date(), PURGE_MAX_MONTH_INTERVAL * 30)
        if (diffSecs(structure.datePurge, maxDatePurge) > 0) {
          let errorMessage = ''
          if (dbStructure.dernierePurge) {
            errorMessage = 'La dernière réinitialisation des comptes élèves a eu lieu le ' + formatDate({ date: dbStructure.dernierePurge, fr: true }) + '.'
          }
          errorMessage += 'Vous ne pouvez pas repousser la date de purge au delà du ' + formatDate({ date: maxDatePurge, fr: true }) + '.'
          return context.fieldError('datePurge', errorMessage)
        }
      }

      oldStructure = Object.assign({}, dbStructure)
      Object.assign(dbStructure, structure)
      dbStructure.store(this)
    }).seq(function (structure) {
      const next = this
      const timezoneUpdated = changedKeys.includes('timezone')
      const oldTimezone = oldStructure.timezone
      const newTimezone = structure.timezone
      const dateUpdated = changedKeys.includes('datePurge')
      $utilisateur.grabTeacherByStructure(structure.oid, (error, teachers) => {
        if (error) return next(error)
        for (const teacher of teachers) {
          if (!teacher.mail || teacher.mail === currentFormateur.mail) {
            continue
          }

          $mail.send(
            teacher.mail,
            'Mise à jour de la structure',
            require('./templates/mail-update.dust'), {
              formateur: currentFormateur,
              oldStructure,
              newStructure: structure,
              dateUpdated,
              timezoneUpdated,
              oldTimezone,
              newTimezone
            }
          )
        }
      })

      this(null, structure)
    }).seq(function (structure) {
      context.rest({ structure })
    }).catch(context.next)
  })

  /**
   * Purge les élèves et les classes de la structure passée en post
   * @param context Context
   */
  this.post('api/purge-eleves', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const structureOid = context.post.structureOid
    const currentStructureOid = $session.getCurrentStructureOid(context)
    if (!structureOid) context.restKo('Aucune structure fournie')
    if (!context.isFormateurInStructure(structureOid)) return context.accessDenied()
    if (currentStructureOid !== structureOid) {
      console.error(new Error(`Demande de purge pour ${structureOid} alors que la structure courante est ${currentStructureOid}`))
      return context.accessDenied('Ce n’est pas la structure courante')
    }

    flow()
      .seq(function () {
        $structure.purgeEleves(structureOid, this)
      })
      .seq(function (response) {
        context.rest(response)
      })
      .catch(context.next)
  })
})
