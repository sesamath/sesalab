const moment = require('moment')
const defaultTimezone = require('sesalab-commun/constants').DEFAULT_TIMEZONE
const { addSecs, isOrdered, formatDate } = require('sesajs-date')
const appLog = require('../tools/appLog')

const addHours = (date, hours) => addSecs(date, hours * 3600)

/**
 * Représente une structure/un établissement.
 *
 * @Entity Structure
 * @class Structure
 * @property {string} oid Identifiant
 * @property {string} code Le code à 5 lettres pour que les users se refilent plus facilement les établissements (généré lors du 1er insert)
 * @property {string} ville (saisie libre à la création ou transmis par la source externe)
 * @property {string} pays (saisie libre à la création ou transmis par la source externe)
 * @property {Date} [datePurge=20/08]
 * @property {Date} [removeDate] date de suppression programmée
 * @property {string} [externalSource] Nom de la source d'authentification qui nous a filé les infos
 * @property {string} [externalId] Identifiant chez la source
 * @property {string} [category] ecole|college|lycee|undefined
 * @property {string} [uai] (fourni éventuellement par le SSO)
 * @property {string} [academieId] (fourni éventuellement par le SSO, utilisé pour les stats)
 * @property {string} [departementNumero] (fourni éventuellement par le SSO, utilisé pour les stats)
 */
app.entity('Structure', function ($crypto, Backup, Journal) {
  const Structure = this
  let $structure

  this.validateJsonSchema(
    {
      // JSON-Schema
      type: 'object',
      properties: {
        oid: { type: 'string' },
        code: { type: 'string' },
        // TODO: on a 4 structures avec une catégorie null, normal?
        category: { enum: ['ecole', 'college', 'lycee', null] },
        nom: { type: 'string' },
        ville: { type: 'string' },
        departementNumero: { type: 'string' },
        departement: { type: 'string' },
        departementId: { type: 'string' },
        academieId: { type: 'string' },
        region: { type: 'string' },
        pays: { type: 'string' },
        type: { type: 'string' },
        externalSource: { type: 'string' },
        externalId: { type: 'string' },
        onlyFromEntId: { type: 'string' },
        uai: { type: 'string' },
        ajoutElevesAuthorises: { type: 'boolean' },
        ajoutFormateursInterdit: { type: 'boolean' },
        accesMultiple: { type: 'boolean' },
        bloquerExerciceAutonomie: { type: 'boolean' },
        timezone: {
          type: ['string'],
          description: 'Timezone de la structure, par exemple "Europe/Paris"'
        },
        // Purge et suppression
        datePurge: { instanceof: 'Date' },
        removeDate: { instanceof: 'Date' },
        dernierePurge: { instanceof: 'Date' }
      },
      required: ['code', 'nom']
    }
  )

  this.defaults(function () {
    this.ajoutElevesAuthorises = true
    this.ajoutFormateursInterdit = false
    // random entre 15 et 22, pour étaler un peu les purges
    const day = 15 + Math.random() * 7
    const datePurge = moment({ day, month: 7 }) // Default: 15~22 août
    if (datePurge <= moment()) datePurge.add(1, 'year')
    this.datePurge = datePurge.toDate()
    this.timezone = defaultTimezone
  })

  /**
   * Avant la suppression on vérifie qu'il n'y a plus d'utilisateur dans la structure et on la backup
   */
  this.beforeDelete(function (cb) {
    // const {oid, nom, code, uai} = this
    if (!$structure) $structure = lassi.service('$structure')
    // on contrôle qu'il n'y a plus d'utilisateur dedans et on refuse la suppression sinon (faut lancer une purge manuellement avant)
    $structure.countUsers(this.oid, (error, nb) => {
      if (error) return cb(error)
      if (nb !== 0) return cb(Error(`Impossible de supprimer la structure car elle contient encore ${nb} utilisateurs`))
      Backup.create({ id: `Structure-${this.oid}`, entity: this }).store(cb)
    })
  })

  /**
   * Après suppression on note dans le journal et on lance un nettoyage,
   * au cas où la suppression des utilisateurs aurait laissé des scories
   */
  this.afterDelete(function (cb) {
    // la suppression a eu lieu, on appelle pas cb avec une erreur si qqchose plante ici
    const journalData = { id: `Structure-${this.oid}`, action: 'delete' }
    if (this.$deleteMessage) journalData.message = this.$deleteMessage
    Journal.create(journalData).store(error => {
      if (error) console.error(error)
      // pour supprimer tout ce qui est lié à la structure on lance quand même ça,
      // mais ça ne devrait pas être utile (car déjà nettoyé à la suppression des utilisateurs)
      // ATTENTION, option noStore indispensable sinon on va recréer la structure à l'identique
      $structure.purgeStructure(this, { noCheckDatePurge: true, noNotification: true, noStore: true }, error => {
        if (error) console.error(error)
        cb()
      })
    })
  })

  // Avant la sauvegarde on vérifie un peu d'intégrité, et s'il n'y a pas encore de code
  // on le génère (basé sur le nombre de structures déjà existantes)
  this.beforeStore(function (cb) {
    const structure = this
    // trim sur departementNumero pour virer les éventuels espaces de début (c'est un char(4) coté Sésamath)
    if (typeof structure.departementNumero === 'string') structure.departementNumero = structure.departementNumero.trim()

    // il semblerait qu'on ait des externalId de type number donc on laisse ça par sécurité en attendant de corriger chez les clients SSO
    if (typeof structure.externalId === 'number') {
      structure.externalId = structure.externalId.toString()
      console.error(new Error(`structure avec externalId ${structure.externalId} en number`))
    }
    // on vérifie que l'on a toujours externalSource avec externalId (ou aucun)
    if (Boolean(this.externalId) !== Boolean(this.externalSource)) return cb(new Error(`externalId et externalSource doivent être tous les deux renseignés (ou aucun), on a ${this.externalSource}/${this.externalId} pour ${this.oid}`))

    // si onlyFromEntId n'est pas vide, alors on ne peut pas ajouter de prof ou d'élève
    if (structure.onlyFromEntId) {
      structure.ajoutElevesAuthorises = false
      structure.ajoutFormateursInterdit = true
      // et la date de purge est forcément fixée entre le 15 et le 18/08
      const now = new Date()
      let year = now.getFullYear()
      let minDate = new Date(`${year}-08-15`)
      if (!isOrdered(now, minDate)) {
        // c'est le 15/08 de l'année prochaine…
        year++
        minDate = new Date(`${year}-08-15`)
      }
      const maxDate = new Date(`${year}-08-19`)
      // on ajoute une marge de 3h pour d'éventuelles histoire de fuseau
      if (!isOrdered(addHours(minDate, -3), structure.datePurge) || !isOrdered(structure.datePurge, addHours(maxDate, 3))) {
        if (structure.oid) {
          appLog.warn(Error(`Date de purge ${formatDate({ date: structure.datePurge, fr: true })} de la structure ${structure.oid} invalide (pas entre 15/08 et 19/08 pour une structure ENT)`))
        } // sinon elle vient d'être créée et on râle pas
        const day = 15 + Math.floor(Math.random() * 3) // entre 15 et 17
        structure.datePurge = new Date(`${year}-08-${day}`)
      }
    }

    if (structure.code) return cb()

    // pas encore de code, on le génère
    Structure.match().sort('code', 'desc').grabOne(function (error, latestStructure) {
      if (error) return cb(error)
      if (!latestStructure) {
        structure.code = $crypto.fromIntToCode(0)
        return cb()
      }
      // On récupère le dernier code en base et on l'incrémente.
      // il reste un petit risque de collision en cas de création simultanée de structures
      // ça plantera à cause de l'index unique
      structure.code = $crypto.incrementCode(latestStructure.code)
      cb()
    })
  })

  // y'a des doublons…
  // this.defineIndex('code', 'string', {unique: true})
  // db.Structure.aggregate([{$group: {_id: '$code', count: {$sum: 1}, oids: {$push: '$_id'}}}, {$match: { count: {$gt: 1} }}]).forEach(res => { print(res._id, res.count, res.oids.join(' ') })
  this.defineIndex('code', 'string')
  this.defineIndex('ville', 'string')
  this.defineIndex('pays', 'string')
  this.defineIndex('datePurge', 'date')
  this.defineIndex('removeDate', 'date')
  // concat code & nom (beforeStore passe avant buildIndexes donc on aura toujours code)
  this.defineIndex('_nom', 'string', function () {
    return this.code.toLowerCase() + '-' + this.nom.toLowerCase()
  })
  // ces index sont spécifiques à certaines sources externes
  this.defineIndex('externalSource', 'string')
  this.defineIndex('externalId', 'string')
  this.defineIndex('category', 'string') // ecole|college|lycee|undefined
  this.defineIndex('onlyFromEntId', 'string')
  this.defineIndex('uai', 'string', { unique: true, sparse: true })
  this.defineIndex('academieId', 'string')
  this.defineIndex('departementNumero', 'string')
})
