const flow = require('an-flow')
const { getMailTextContact } = require('../utilisateur/mailTemplates')
const { pick } = require('../../../sesalab-commun/utils')

app.controller(function (Structure, $mail, $sso, $session, $settings) {
  const confApp = app.settings.application

  this.post('api/contact', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const utilisateur = $session.getCurrentFormateur(context)
    const { mail } = utilisateur
    if (!mail) return context.fieldError('mail', 'Vous n’avez pas renseigné votre adresse mail, envoi de message impossible (vous ne pourriez recevoir de réponse)')
    const currentStructureOid = $session.getCurrentStructureOid(context)
    const { message, ressourceUrl, files } = context.post
    let { subject } = context.post
    if (!message || message.length === 0) return context.fieldError('mail', 'Le champ "message" est obligatoire')
    // le préfixe sera ajouté à l'envoi
    if (!subject) subject = 'Signalement via le formulaire'

    flow()
      .seq(function () {
        Structure
          .match('oid').equals(currentStructureOid)
          .grabOne(this)
      })
      .seq(function (structure) {
        const data = {
          appName: $settings.get('application.name'),
          structure,
          utilisateur,
          message,
          browser: context.post.browser,
          host: context.post.host,
          version: confApp.version,
          ressourceUrl
        }

        const mailData = {
          sender: mail,
          replyTo: mail,
          subject,
          text: getMailTextContact(data),
          to: confApp.contactMail
        }
        // sesalab-formateur/source/client/classes/dialogs/Contact.js:207 nous envoie du [{name, content}, …]
        if (files?.length) {
          // cf https://nodemailer.com/message/attachments/
          mailData.attachments = files.map(({ name, content }) => ({ filename: name, href: content }))
        }
        $mail.sendMail(mailData, this)
      })
      .seq(function () {
        context.rest({})
      })
      .catch(context.next)
  })

  /**
   * Retourne les settings (pour un formateur|élève|gestion|home), avec vérification d'une session authentifiée ou pas (param connected)
   * @route GET /api/settings
   */
  this.get('api/settings', function (context) {
    if (!$session) $session = lassi.service('$session')
    // si le user est sensé être connecté on commence par vérifier ça
    if (context.get.connected && !$session.isAuthenticated(context)) {
      $session.flush(context)
      return context.rest({
        success: true, // faut pas tomber directement dans le catch coté client
        message: 'Session expirée, vous devez vous reconnecter',
        redirectTo: '/',
        settings: {}
      })
    }

    let settings
    // et suivant les cas on liste explicitement ici tous les champs qui seront renvoyés
    // (ne jamais rien mettre de confidentiel car toutes ces urls sont joignable publiquement)
    if (context.get.eleve) {
      settings = pick(confApp, [
        'baseId',
        'baseUrl',
        'maxBilans',
        'name',
        'numeroCNIL',
        'sesatheques',
        'staging',
        'timeoutDefaultView',
        'version'
      ])
      settings.aide = pick(confApp.aide, ['bilans', 'eleves', 'rgpd'])
      settings.profils = { autonomie: confApp.profils.autonomie }
    } else if (context.get.formateur) {
      settings = pick(confApp, [
        'baseId',
        'contactMail',
        'joursDisplayDatePurgeStructure',
        'maxClasses',
        'maxResourcesDropped',
        'name',
        'numeroCNIL',
        'profils',
        'sesatheques',
        'staging',
        'sso',
        'version'
      ])
      // faut ajouter des propriétés "profondes"
      settings.aide = pick(confApp.aide, ['charte',
        'corbeille',
        'homepage',
        'panneau',
        'profil',
        'rgpd',
        'sequences'])
      settings.content = { homeInfo: confApp.content.homeInfo }
      // les sso éventuels ne peuvent modifier les setting que pour les formateurs
      const currentFormateur = $session.getCurrentFormateur(context)
      // Ajout de la config SSO sur les settings pour ce user (pas forcément connecté)
      settings.sso = {}

      if (currentFormateur) {
        // authentifié comme prof
        if (currentFormateur.externalMech) {
          // il a une source d'authentification externe (donc à priori il en vient)
          const ssoProvider = $sso.get(currentFormateur.externalMech)
          // url "mon compte"
          settings.sso.urlAccount = ssoProvider.getUrlAccount(currentFormateur.sso)
          // si true ça empêche l'édition de structure car c'est géré côté sso
          settings.sso.hasStructureManagement = !!ssoProvider.getHasStructureManagement(currentFormateur.sso)
          // + overrides éventuels
          settings = ssoProvider.overrideSettings(currentFormateur.sso, settings)
        }
      } else {
        // si le user n'est pas authentifié et que des providers sso veulent ajouter des infos
        // à mettre sur le form de création de compte
        settings.sso.accountCreationBlocks = $sso.getAll()
          .map((provider) => provider.getAccountCreationInfos())
          .filter(Boolean) // pour virer les blocks null
      }
      // fin du cas formateur
    } else if (context.get.gestion) {
      settings = pick(confApp, ['baseUrl', 'joursGestion'])
      settings.aide = { gestion: confApp.aide.gestion }
    } else if (context.get.home) {
      settings = {
        aide: { comptes: confApp.aide.comptes }
      }
      // on ajoute une propriété redirectTo ici, pour indiquer au js qu'il doit rediriger qq part si c'est le cas
      // ça n'a pas grand chose à voir avec des settings mais cet appel est le 1er
      // vers l'api au chargement de la page, autant le signaler tout de suite.
      if ($session.isAuthenticatedAsFormateur(context)) {
        return context.rest({
          settings,
          redirectTo: '/formateur/',
          message: 'Vous êtes déjà authentifié comme formateur'
        })
      }
      if ($session.isAuthenticatedAsEleve(context)) {
        return context.rest({
          settings,
          redirectTo: '/eleve/',
          message: 'Vous êtes déjà authentifié comme élève'
        })
      }
    } else {
      // par défaut on ne renvoie rien
      settings = {}
    }

    const response = { settings }

    // s'il y a un message en session mis par le login on le retourne aussi
    if (context.session?.postLogin) {
      response.postLogin = context.session.postLogin
      delete context.session.postLogin
    }

    context.rest(response)
  }) // GET /api/settings
})
