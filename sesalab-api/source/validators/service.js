/**
 * @module services/$validators
 */
const appLog = require('sesalab-api/source/tools/appLog')

const { TYPE_FORMATEUR } = require('sesalab-commun/constants')
const createError = require('../tools/createError')

/**
 * @Service $validator
 *
 * @todo Enlever tout ce qui est lié à "newEmail" pour le fusionner au validator "mail"
 */
app.service('$validators', function ($crypto) {
  /**
   * Initialise un validateur.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite initialiser
   * @param {string} validatorName Nom du validateur
   * @param {object} [options] Options
   * @param {boolean} [options.withToken=false] passer true pour créer un token
   * @memberof $validators
   */
  function initValidator (utilisateur, validatorName, { withToken = false } = {}) {
    if (!utilisateur.validators) utilisateur.validators = {}
    if (utilisateur.validators[validatorName]) throw createError(400, `L'utilisateur a déjà un validator ${validatorName}`)

    utilisateur.validators[validatorName] = {
      responseDate: null, // portera la date à laquelle la validation a eu lieu, qu'elle soit positive ou négative
      createdAt: new Date()
    }

    if (withToken) {
      utilisateur.validators[validatorName].token = $crypto.token()
    }
  }

  /**
   * Initialise le validateur d'un compte.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite initialiser
   * @memberof $validators
   */
  function initAccountValidator (utilisateur) {
    initValidator(utilisateur, 'account')
  }

  /**
   * Initialise le validateur d'email d'un compte.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite initialiser
   * @memberof $validators
   */
  function initEmailValidator (utilisateur) {
    initValidator(utilisateur, 'mail', { withToken: true })
  }

  /**
   * Initialise le validateur d'email d'un compte.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite initialiser
   * @memberof $validators
   */
  function initNewEmailValidator (utilisateur) {
    initValidator(utilisateur, 'newMail', { withToken: true })
  }

  /**
   * Validate le compte d'un utilisateur (mais ne fait pas de store !)
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite valider
   * @param {string} validateurOid Identifiant de l'utilisateur validant le compte
   * @throws {Error} throw des createError 400 en cas d'incohérence (ou compte déjà validé)
   * @memberof $validators
   */
  function validateAccount (utilisateur, validateurOid) {
    // ces vérifs ont dû être faites par le contrôleur, on laisse ça ici au cas où il y aurait
    // deux demandes simultanées (le contrôleur n'ayant pas encore reçu la réponse à la première
    // laisserait passer la 2e)
    if (!utilisateur.validators.account) {
      appLog.error(Error(`utilisateur ${utilisateur.oid} passe dans validateAccount sans validators.account`))
      utilisateur.validators.account = {}
    }
    // on ne peut se valider soi-même, ce truc devrait plutôt être dans un contrôleur mais tant pis
    if (utilisateur.oid === validateurOid) {
      appLog.error(Error('tentative d’auto-validation qui arrive jusqu’au service de validation'))
      throw createError(400, 'Vous ne disposez pas des droits nécessaires pour valider ce compte.')
    }
    if (utilisateur.validators.account.responseDate) throw createError(400, 'Ce compte a déjà été traité par un autre formateur.')
    if (utilisateur.validators.account.validatedBy) {
      console.error(Error(`Compte ${utilisateur.oid} déjà validé avec une validation sans done :\n ${JSON.stringify(utilisateur.validators.account)}`))
      throw createError(400, 'Ce compte a déjà été validé.')
    }

    utilisateur.validators.account = Object.assign({}, utilisateur.validators.account, {
      responseDate: new Date(),
      validatedBy: validateurOid,
      accepted: true
    })
    // On s'assure qu'il ne reste pas une date de suppression sur un compte finalement accepté
    // (qui ne l'aurait pas été dans les 10 premiers jours => ajout de ce removeDate)
    if (utilisateur.removeDate) delete utilisateur.removeDate
  }

  /**
   * Valide un validator lié aux adresses emails d'un utilisateur.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite valider
   * @param {string} validatorName Nom du validateur
   * @param {string} token Token de validation
   * @memberof $validators
   */
  function _validateEmail (utilisateur, validatorName, token) {
    if (!utilisateur.validators[validatorName]) throw createError(400, 'Un validateur non initialisé ne peut pas être validé.')
    if (utilisateur.validators[validatorName].token !== token) throw createError(400, 'Ce lien n’est pas valide')
    if (utilisateur.validators[validatorName].responseDate) throw createError(400, 'Ce compte est déjà validé')

    utilisateur.validators[validatorName] = Object.assign({}, utilisateur.validators[validatorName], {
      responseDate: new Date()
    })
  }

  /**
   * Valide l'email d'un utilisateur.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite valider
   * @param {string} token Token de validation
   * @memberof $validators
   */
  function validateEmail (utilisateur, token) {
    _validateEmail(utilisateur, 'mail', token)
  }

  /**
   * Valide la nouvelle adresse email d'un utilisateur.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite valider
   * @param {string} token Token de validation
   * @memberof $validators
   */
  function validateNewMail (utilisateur, token) {
    _validateEmail(utilisateur, 'newMail', token)
  }

  /**
   * Indique si le compteur d'un utilisateur a été accepté.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite vérifier
   * @return {boolean} true pour un compte accepté
   * @memberof $validators
   */
  function isAccountAccepted (utilisateur) {
    return utilisateur.validators.account?.accepted === true
  }

  /**
   * Indique si le compteur d'un utilisateur est validé ou rejeté.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite vérifier
   * @return {boolean} true pour un compte validé
   * @memberof $validators
   */
  function isAccountProcessed (utilisateur) {
    return Boolean(utilisateur.validators.account?.responseDate)
  }

  /**
   * Indique si l'email d'un utilisateur est validé (renvoie false si l'utilisateur n'a pas de mail)
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite vérifier
   * @return {boolean} true pour une adresse validée
   * @memberof $validators
   */
  function hasValidEmail (utilisateur) {
    return Boolean(utilisateur.validators?.mail?.responseDate)
  }

  /**
   * Retourne true si user est accepté (avec un mail valide pour les formateurs), false s'il est refusé, null s'il est toujours en attente de validation.
   *
   * @param {Utilisateur} utilisateur Utilisateur que l'on souhaite vérifier
   * @return {boolean|null}
   * @memberof $validators
   */
  function isValid (utilisateur) {
    // Attention, dans les tests on a des user.validators = {}
    if (!utilisateur.validators?.account) {
      return null
    }
    // pour les prof avec account accepted on vérifie le mail
    if (
      utilisateur.validators.account.accepted === true &&
      utilisateur.type === TYPE_FORMATEUR &&
      // sauf pour les profs venant de l'extérieur (ils ont un mail validé en amont ou pas de mail)
      !utilisateur.externalMech
    ) {
      return hasValidEmail(utilisateur)
    }
    return utilisateur.validators.account.accepted ?? null
  }

  /**
   * Valide d'autorité un utilisateur (pour les users venant d'un SSO ou importés ou créés par le prof)
   * (sans écrire en bd)
   * @param {Utilisateur} utilisateur
   */
  function setValid (utilisateur, by = '') {
    if (!utilisateur.validators) utilisateur.validators = {}
    const now = new Date()
    if (!isAccountAccepted(utilisateur)) {
      if (!utilisateur.validators.account) utilisateur.validators.account = { createdAt: now }
      const a = utilisateur.validators.account
      a.accepted = true
      a.responseDate = now
      if (by) a.validatedBy = by
    }
    // s'il a un mail on le valide
    if (utilisateur.mail && !hasValidEmail(utilisateur)) {
      if (!utilisateur.validators.mail) {
        console.error(`utilisateur ${utilisateur.oid} avec mail mais sans validators.mail`)
        utilisateur.validators.mail = { createdAt: now }
      }
      utilisateur.validators.mail.accepted = true
      utilisateur.validators.mail.responseDate = now
    }
  }

  return {
    initAccountValidator,
    initEmailValidator,
    initNewEmailValidator,
    isAccountAccepted,
    isAccountProcessed,
    hasValidEmail,
    isValid,
    setValid,
    validateAccount,
    validateEmail,
    validateNewMail
  }
})
