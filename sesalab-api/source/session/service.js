/**
 * @module services/$session
 */

// on est requis par sesalab-api/source/index.js pour être dispo pour tous les autres contrôleurs et services
// c'est pour ça qu'il n'y a pas de index.js dans ce dossier pour nous inclure

const { TYPE_ELEVE, TYPE_FORMATEUR } = require('sesalab-commun/constants')
const appLog = require('sesalab-api/source/tools/appLog')

/**
 * Les méthodes pour manipuler la session
 *
 * @service $session
 * @name $session
 * @type Object
 */
app.service('$session', function ($cache, $crypto) {
  /**
   * Purge la session courante (à appeler sur un logout)
   * @param context
   * @param {string[]} [except] Une liste de propriétés à conserver dans la session
   * @memberOf $session
   */
  function flush (context, except = []) {
    appLog.debug(`flush session${except.length ? ' (sauf ' + except.join(' et ') + ')' : ''} qui était`, context.session)
    for (const key of Object.keys(context.session)) {
      // faut pas virer cookie sinon le save de la session plante
      if (key === 'cookie' || except.includes(key)) continue
      delete context.session[key]
    }
  }

  /**
   * Retourne la liste des oids des structures autorisées pour le ou les users connectés
   * @param context
   * @return {string[]}
   * @memberOf $session
   */
  function getAllowedStructures (context) {
    // le filtrage a eu lieu lors de la mise en session (par login)
    // mais on peut être appelé par un GET /api/utilisateur qui a perdu sa session
    return context.session.allowedStructures ?? []
  }
  /**
   * Retourne le ou les élèves connectés
   * @param context
   * @return {Utilisateur[]|undefined}
   * @memberOf $session
   */
  function getCurrentEleves (context) {
    return context.session.eleves?.length ? context.session.eleves : undefined
  }

  /**
   * Retourne le formateur loggué (undefined si y'en a pas)
   * @param context
   * @return {Utilisateur|undefined}
   * @memberOf $session
   */
  function getCurrentFormateur (context) {
    return context.session.utilisateur
  }

  /**
   * Retourne l'oid de la structure courante (ou undefined si on est pas loggé)
   * @param {Context} context
   * @return {string|undefined}
   * @memberOf $session
   */
  function getCurrentStructureOid (context) {
    return context.session.currentStructureOid
  }

  /**
   * Retourne le readOnlyToken du user courant (le génère s'il n'existait pas encore)
   * Ce token sert à récupérer l'oid du ou des user(s) courant(s) pour les appels sans session
   * (api/resultat/last passés depuis l'iframe de l'exercice)
   *
   * Il est généré au login et mis en cache pour y associer l'oid du user courant (ou les en cas d'élèves multiples)
   * Mise en cache pour 4h, au premier appel (login) avec rafraîchissement à chaque appel (dans seance/start par ex)
   * Le token est passé au front lors de l'appel de api/utilisateur, il le garde ensuite dans sa session.
   * @param context
   * @return {string}
   * @memberOf $session
   */
  function getReadOnlyToken (context) {
    // on rafraîchit le cache en tâche de fond
    const formateur = getCurrentFormateur(context)
    if (formateur) {
      return ''
    }
    const eleves = getCurrentEleves(context)
    if (!eleves?.length) {
      appLog.error(Error('getReadonlyToken appelé sans session utilisateur'))
      return ''
    }
    const data = {
      eleves: eleves.map(e => e.oid)
    }
    const readonlyToken = context.session.readonlyToken || $crypto.token()
    const key = `session.readonlyToken.${readonlyToken}`
    // On fixe un délai de 4h, et on regénèrera ça à chaque appel de api/seance/start
    const ttl = 4 * 3600
    $cache.set(key, data, ttl, function (error) {
      if (error) appLog.error(error)
    })
    // on retourne le token en sync
    return readonlyToken
  }

  /**
   * @callback tokenDataCallback
   * @param {Error} [error]
   * @param {object} data peut être null si le cache est expiré
   * @param {string[]} [data.eleves] oids des élèves associés à ce token
   */
  /**
   * Récupère en cache les datas associées à ce token
   * ATTENTION, cette méthode n'utilise context.session, elle est quand même dans ce service $session qui gère le $cache.set pour ce token
   * @param {tokenDataCallback} cb
   */
  function getDataForReadonlyToken (context, token, cb) {
    return $cache.get('session.readonlyToken.' + token, cb)
  }

  /**
   * Retourne true si un utilisateur est loggué (formateur, élève unique ou élèves multiples)
   * @param context
   * @return {boolean}
   * @memberOf $session
   */
  function isAuthenticated (context) {
    return !!(context.session.utilisateur || (context.session.eleves && context.session.eleves.length))
  }

  /**
   * Retourne true si la session courante concerne un admin (gestion, donc formateur)
   * @param context
   * @return {boolean}
   * @memberOf $session
   */
  function isAuthenticatedAsAdmin (context) {
    return !!(context.session.utilisateur && context.session.utilisateur.type === TYPE_FORMATEUR && context.session.utilisateur.$isAdminGestion)
  }

  /**
   * Retourne true si la session courante concerne un formateur
   * @param context
   * @return {boolean}
   * @memberOf $session
   */
  function isAuthenticatedAsFormateur (context) {
    return !!(context.session.utilisateur && context.session.utilisateur.type === TYPE_FORMATEUR)
  }

  /**
   * Retourne true la session courante concerne un ou des élèves
   * @param context
   * @return {boolean}
   * @memberOf $session
   */
  function isAuthenticatedAsEleve (context) {
    return !!(context.session.eleves && context.session.eleves.length)
  }

  /**
   * Met un ou des users en session (après un login réussi ou après une modif du user ou un changement de structure du prof)
   * @param {Context} context
   * @param {Utilisateur|Utilisateur[]} users
   * @param {string} structureOid
   * @param {string[]} [allowedStructures] La liste oids des structures sur lesquelles l'utilisateur doit être restreint
   *                    (pb du GAR qui exige qu'un utilisateur ne puisse pas se connecter
   *                    à une structure sans passer par son ENT par ex)
   * @memberOf $session
   */
  function login (context, users, structureOid, allowedStructures) {
    appLog.debug('login de', users, 'sur', structureOid)
    // On reset la session complètement, avant les vérifs au cas où ça plante
    flush(context)

    // check params
    if (!Array.isArray(users) && users?.type === TYPE_FORMATEUR) users = [users]
    if (!Array.isArray(users) || !users.length) throw Error('Utilisateur manquant')
    if (!structureOid) throw Error('structure manquante')
    if (allowedStructures != null && !Array.isArray(allowedStructures)) throw Error('Liste des structures autorisées invalide')

    if (users.length === 1 && users[0].type === TYPE_FORMATEUR) {
      // un prof
      const user = users[0]
      if (!user.structures.includes(structureOid)) throw Error(`Le formateur ${user.oid} n’est pas dans la structure ${structureOid}`)
      context.session.utilisateur = user
      if (!allowedStructures) allowedStructures = user.structures
    } else if (users.every(u => u.type === TYPE_ELEVE)) {
      // un ou des élèves
      for (const user of users) {
        if (!user.structures.includes(structureOid)) throw Error(`L’élève ${user.oid} n’est pas dans la structure ${structureOid}`)
        if (allowedStructures) {
          // on vire éventuellement celles auxquelles n'appartiendrait pas ce user
          allowedStructures = allowedStructures.filter(oid => user.structures.includes(oid))
        } else {
          allowedStructures = user.structures
        }
      }
      context.session.eleves = users
    } else {
      throw Error('type incohérent')
    }
    // on met aussi ça pour formateur & élèves
    context.session.currentStructureOid = structureOid
    context.session.allowedStructures = allowedStructures
    // ça va générer le token
    getReadOnlyToken(context)
    appLog.debug('login ok avec la session', context.session)
  }

  /**
   * Change la structure courante en session (throw si c'est pas un formateur en session ou s'il n'a pas cette structure)
   * @param {Context} context
   * @param {string} structureOid
   * @memberOf $session
   */
  function updateCurrentStructureOid (context, structureOid) {
    const formateur = getCurrentFormateur(context)
    if (!formateur) {
      throw Error('Seul un formateur peut changer de structure')
    }
    const allowedStructures = context.session.allowedStructures || formateur.structures
    if (!allowedStructures.includes(structureOid)) {
      throw Error('Accès à cette structure interdit (probablement une restriction d’accès via l’ENT)')
    }
    context.session.currentStructureOid = structureOid
  }

  /**
   * Met  à jour la session
   * @param context
   * @param users
   * @param [structureOid]
   * @memberOf $session
   */
  function update (context, users, structureOid = null) {
    // check cohérence de l'appel
    if (!Array.isArray(users)) users = [users]
    if (!users.length || !users[0].oid) throw Error('Aucun utilisateur à mettre à jour en session')
    const formateur = getCurrentFormateur(context)
    if (formateur) {
      if (users[0].oid !== formateur.oid) {
        flush(context)
        throw Error('Données en session incohérentes, vous devez vous reconnecter')
      }
      context.session.utilisateur = users[0]
      if (structureOid) updateCurrentStructureOid(structureOid)
    } else if (isAuthenticatedAsEleve(context) && users.every(u => u.type === TYPE_ELEVE)) {
      // un ou des élèves
      context.session.eleves = users
    } else {
      throw Error('utilisateurs invalides')
    }
  }

  return {
    flush,
    getAllowedStructures,
    getCurrentEleves,
    getCurrentFormateur,
    getCurrentStructureOid,
    getDataForReadonlyToken,
    getReadOnlyToken,
    isAuthenticated,
    isAuthenticatedAsAdmin,
    isAuthenticatedAsFormateur,
    isAuthenticatedAsEleve,
    login,
    update,
    updateCurrentStructureOid
  }
})
