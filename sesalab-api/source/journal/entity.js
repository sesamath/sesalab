app.entity('Journal', function () {
  // JSON-Schema
  this.validateJsonSchema(
    {
      type: 'object',
      properties: {
        oid: { type: 'string' },
        id: { type: 'string' },
        action: { type: 'string' },
        message: { type: 'string' },
        timestamp: { type: 'number' }
      },
      required: ['id', 'action'],
      additionalProperties: false
    }
  )

  // indexes, on ne défini pas de type car ça va permettre d'indexer directement _data,
  // sans créer de propriété supplémentaire à la racine de l'objet.
  /**
   * EntityName-oid de l'entity concernée
   */
  this.defineIndex('id')
  this.defineIndex('action')
  this.defineIndex('timestamp')

  this.beforeStore(function (cb) {
    if (this.oid) return cb(Error('Une entrée de journal ne peut pas être modifiée'))
    // on impose toujours la date du moment
    this.timestamp = Date.now()
    cb()
  })
})
