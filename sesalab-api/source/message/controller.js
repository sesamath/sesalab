/**
 * @module controllers/message
 */
const flow = require('an-flow')
const { diffSecs } = require('sesajs-date/cjs')

/**
 * API liées au message.
 * @Controller Message
 */
app.controller(function (Message, $session) {
  /**
   * Récupère les messages de l'utilisateur donné ainsi que ceux auquel il a accès.
   * (formateurAdmin only, sinon 403).
   *
   * @name Messages
   * @route {GET} api/messages
   * @authentication Cette route nécessite un utilisateur connecté
   */
  this.get('api/messages', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      Message
        .match('author').equals(currentFormateur.login)
        .grab(this)
    }).seq(function (messages) {
      context.rest({ messages })
    }).catch(context.next)
  })

  /**
   * Affiche un message.
   *
   * @name Messages
   * @route {GET} api/messages
   * @authentication Cette route nécessite un utilisateur connecté
   * @routeparam {string} oid Identifiant du message
   */
  this.get('api/messages/:oid', function (context) {
    const messageOid = context.arguments.oid
    if (!messageOid) return context.restKo('Aucun message fourni')
    const currentFormateur = $session.getCurrentFormateur(context)

    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      Message
        .match('oid').equals(messageOid)
        .match('author').equals(currentFormateur.login)
        .grabOne(this)
    }).seq(function (message) {
      if (!message) return context.notFound()
      else context.rest({ message })
    }).catch(context.next)
  })

  /**
   * Supprime le message avec l'identifiant indiqué.
   *
   * @name Message
   * @route {GET} api/messages
   * @authentication Cette route nécessite un utilisateur connecté
   * @routeparam {string} oid Identifiant du message
   */
  this.delete('api/messages/:oid', function (context) {
    const messageOid = context.arguments.oid
    if (!messageOid) context.restKo('Aucun message fourni')
    if (!$session) $session = lassi.service('$session')
    const currentFormateur = $session.getCurrentFormateur(context)

    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      Message
        .match('oid').equals(messageOid)
        .match('author').equals(currentFormateur.login)
        .grabOne(this)
    }).seq(function (message) {
      if (!message) return context.notFound()
      else message.delete(this)
    }).seq(function () {
      context.rest({})
    }).catch(context.next)
  })

  function createOrUpdateMessage (context, messageOid) {
    let login
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      login = $session.getCurrentFormateur(context).login
      // On s'assure que les données sont valides
      if (!context.post.startDate) {
        return context.fieldError('startDate', 'Une date de début est obligatoire')
      }
      if (!context.post.endDate) {
        return context.fieldError('endDate', 'Une date de fin est obligatoire')
      }
      if (diffSecs(context.post.startDate, context.post.endDate) < 0) {
        return context.fieldError('endDate', 'La date de fin ne peut être antérieure à la date de début')
      }
      if (!context.post.title || context.post.title.length < 1) {
        return context.fieldError('title', 'L’article doit posséder un titre')
      }
      if (!context.post.content || context.post.title.content < 1) {
        return context.fieldError('content', 'L’article doit posséder un contenu')
      }
      if (!messageOid) return this()
      Message
        .match('oid').equals(messageOid)
        .match('author').equals(login)
        .grabOne(this)
    }).seq(function (message) {
      if (!message) {
        message = Message.create()
        message.author = login
      } else if (diffSecs(new Date(), message.endDate) < 0) {
        return context.fieldError('content', 'Vous ne pouvez pas modifier un article dont la date de fin est dépassée')
      }
      message.date = new Date()
      message.title = context.post.title
      message.content = context.post.content
      message.startDate = context.post.startDate
      message.endDate = context.post.endDate
      message.store(this)
    }).seq(function (message) {
      context.rest({ message })
    }).catch(context.next)
  }

  /**
   * Met à jour un message.
   *
   * @name MessageUpdate
   * @route {PUT} api/messages/:oid
   * @routeparam {string} oid Identifiant du message
   * @bodyparam {*} Données du message
   */
  this.put('api/messages/:oid', function (context) {
    const messageOid = context.arguments.oid
    if (!messageOid) context.restKo('Aucun message fourni')
    createOrUpdateMessage(context, messageOid)
  })

  /**
   * Création d'un message.
   *
   * @name MessageCreate
   * @route {POST} api/messages
   * @bodyparam {*} Données du message
   */
  this.post('api/messages', function (context) {
    createOrUpdateMessage(context)
  })
})
