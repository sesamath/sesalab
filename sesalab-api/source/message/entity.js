/**
 * Représente un message destiné à plusieurs structures.
 *
 * @Entity Message
 * @class Message
 * @property {string} oid Identifiant
 * @property {string} author Login de l'auteur
 * @property {string} title Titre du message
 * @property {string} content Contenu du message
 * @property {Date} date Date de création du message
 * @property {Date} startDate Date à laquelle le message va apparaître
 * @property {Date} endDate Date à laquelle le message va disparaître
 */
app.entity('Message', function () {
  this.validateJsonSchema(
    {
      // JSON-Schema
      type: 'object',
      properties: {
        oid: { type: 'string' },
        author: { type: 'string', description: 'Login de l’auteur' },
        title: { type: 'string', minLength: 1 },
        content: { type: 'string', minLength: 1 },
        date: { instanceof: 'Date' },
        startDate: { instanceof: 'Date' },
        endDate: { instanceof: 'Date' }
      },
      required: ['author', 'title', 'content', 'startDate', 'endDate']
    }
  )
  this.defineIndex('author', 'string')
  this.defineIndex('content', 'string')
  this.defineIndex('startDate', 'date')
  this.defineIndex('endDate', 'date')
})
