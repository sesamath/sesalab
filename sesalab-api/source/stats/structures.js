const flow = require('an-flow')
const moment = require('moment')
const { formatDate } = require('sesajs-date')

moment.locale('fr')

const { TYPE_ELEVE } = require('sesalab-commun/constants.js')

const { getFromCacheOrBuild, getStructuresWithLabel, sendCsvFile } = require('./common')
// moment.HTML5_FMT.DATE n'existe qu'à partir de 2.2.20
const HTML5_FMT_DATE = 'YYYY-MM-DD'

module.exports = function setup (controller) {
  /**
   * Ajoute les stats de la structure à rows
   * @private
   * @param {string} structure oid
   * @param {Object} stats L'objet qui sera peuplé
   * @param callback
   */
  function buildStatsStructure (structure, stats, callback) {
    const structureOid = structure.oid
    const { rows } = stats
    const { startDate, endDate, horsEntRow } = stats.internals
    // le cumul dans la structure d'oid 0
    // à priori, on fera du rows[structureOid] = { nom: structure.label, … }
    // sauf si c'est des rows hors ENT, dans ce cas on compile dans rows.horsEnt
    // on stock ici les info, à la fin on ajoutera un prepend hors ent ou pas
    const structureStats = { nom: structure.label }
    flow().seq(function () {
      // Nb formateurs
      $utilisateur.countTeacherByStructure(structureOid, this)
    }).seq(function (count) {
      structureStats.nbf = count
      // nb élèves
      $utilisateur.countStudentsByStructure(structureOid, this)
    }).seq(function (count) {
      structureStats.nbe = count

      // nb connexions élèves
      const filters = {
        structure: structureOid,
        type: TYPE_ELEVE,
        after: startDate,
        before: endDate
      }
      $utilisateurAcces.count(filters, this)
    }).seq(function (count) {
      structureStats.nbc = count
      // en 2024 tous les établissements ENT sont onlyFromEntId
      // (si ça devait changer, il faudrait compter pour chaque étab avec/sans ENT, cf code avant 2024-05)
      if (structure.onlyFromEntId) {
        // etab ENT, il a sa ligne
        rows['s' + structureOid] = structureStats
      } else {
        // sinon cumul horsEnt
        horsEntRow.nbf += structureStats.nbf
        horsEntRow.nbe += structureStats.nbe
        horsEntRow.nbc += structureStats.nbc
      }
      callback()
    }).catch(callback)
  }

  function getCsvLinesForStructuresStats (stats) {
    const csvLines = Object.values(stats.rows).map(row => Object.values(row).join(';'))
    csvLines.unshift(stats.headers.join(';'))

    // on Ajoute une description
    const emptyLine = ';'.repeat(stats.headers.length - 1)
    const date = formatDate({ fr: true })
    // attention, on empile au début, faut donc ajouter la première ligne en dernier
    csvLines.unshift(emptyLine)
    csvLines.unshift(`("Professeurs" et "Élèves" comptabilisés le ${date})${emptyLine}`)
    csvLines.unshift(`Statistiques par établissement pour la période du ${stats.infos.startJour} au ${stats.infos.endJour}${emptyLine}`)

    return csvLines
  }

  /**
   * Retourne une fonction qui prend une cb pour lui filer un nouveau build de stats.
   * @private
   */
  function getStatsBuilder (context, startMoment, endMoment) {
    return function statsBuilder (callback) {
      // init
      const stats = {
        headers: ['Etablissement', 'Professeurs', 'Élèves', 'Connexions élève'],
        internals: {
          startDate: startMoment.toDate(),
          endDate: endMoment.toDate(),
          // init de la "structure" particulière avec le pseudo oid "horsEnt"
          horsEntRow: {
            nom: 'Cumul établissements hors ENT',
            nbf: 0,
            nbe: 0,
            nbc: 0
          }
        },
        infos: {
          startJour: startMoment.format('L'),
          endJour: endMoment.format('L')
        },
        rows: {}
      }

      flow().seq(function () {
        $structure.getStructuresForGestion(context, this)
      }).seq(function (structures) {
        if (!structures.length) return context.restKo('Aucune structure dans votre périmètre')
        this(null, getStructuresWithLabel(structures))
      }).seqEach(function (structure) {
        buildStatsStructure(structure, stats, this)
      }).seq(function () {
        // on ajoute à horsEnt si y'a des connexions sur cette structure
        if (stats.internals.horsEntRow.nbc) {
          stats.rows.horsEnt = stats.internals.horsEntRow
        }
        // et une ligne de total
        const acc = {
          nbf: 0,
          nbe: 0,
          nbc: 0
        }
        const reducer = (acc, row) => {
          acc.nbf += row.nbf
          acc.nbe += row.nbe
          acc.nbc += row.nbc
          return acc
        }
        const cumul = Object.values(stats.rows).reduce(reducer, acc)
        stats.rows.total = { nom: 'TOTAL', ...cumul }
        // on vire internals
        delete stats.internals
        callback(null, stats)
      }).catch(callback)
    }
  }

  /**
   * Retourne les stats structures
   * Un objet avec headers et rows. rows est un objet dont propriétés sont les oids des structures et les valeurs un Array (les colonnes)
   * En cas d'erreur gérée, ça appelle directement context.fieldError ou context.restKo.
   *
   * @private
   * @param context
   * @param callback
   */
  function grabStats (context, callback) {
    // on mutualise ici le contrôle des arguments
    let { start, end } = context.arguments
    // On s'assure que les dates semblent valides
    const startMoment = moment(start, HTML5_FMT_DATE, true)
    if (!startMoment.isValid()) return context.fieldError('startDate', 'Une date de début est obligatoire')
    const endMoment = moment(end, HTML5_FMT_DATE, true)
    if (!endMoment.isValid()) return context.fieldError('endDate', 'Une date de fin est obligatoire')
    if (endMoment.isBefore(startMoment)) return context.fieldError('endDate', 'La date de fin ne peut être antérieure à la date de début')
    // on repasse par moment.format car son parser a pu nettoyer les strings initiales
    start = startMoment.format(HTML5_FMT_DATE)
    end = endMoment.format(HTML5_FMT_DATE)
    // on positionne en début et fin de journée
    startMoment.startOf('day')
    endMoment.endOf('day')
    // on peut lancer
    try {
      const structuresFilter = $structure.getGestionStructuresFilter(context)
      const matchersKey = $structure.getStructuresFilterKey(structuresFilter)
      const cacheKey = `statsStructures_${matchersKey}_${start}_${end}`
      const builder = getStatsBuilder(context, startMoment, endMoment)
      getFromCacheOrBuild(cacheKey, builder, callback)
    } catch (error) {
      callback(error)
    }
  }

  // MAIN
  const $structure = lassi.service('$structure')
  const $utilisateur = lassi.service('$utilisateur')
  const $utilisateurAcces = lassi.service('$utilisateurAcces')

  /**
   * Retourne un tableau de stats pour chaque structure liée à l'administrateur.
   *
   * @route {GET} api/stats/structures/:startTime/:endTime
   * @routeparam {Number} startTime Temps pour la date de début de la période
   * @routeparam {Number} endTime Temps pour la date de fin de la période
   * @return {JSON}
   */
  controller.get('api/stats/structures/:start/:end', function (context) {
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (stats) {
      context.rest({ stats })
    }).catch(context.next)
  })

  /**
   * Export CSV pour les stats de chaque structure liée à l'administrateur
   *
   * @name StatsExportStructures
   * @route {GET} api/stats/export/structures/:startTime/:endTime
   * @routeparam {Number} startTime Temps pour la date de début de la période
   * @routeparam {Number} endTime Temps pour la date de fin de la période
   * @return {JSON}
   */
  controller.get('api/stats/export/structures/:start/:end', function (context) {
    // Note: buildStats contrôle les droits d'accès
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (stats) {
      const csvLines = getCsvLinesForStructuresStats(stats)
      // ici on a plus stats.internals, mais moment a déjà validé les arguments de l'url, on les prend tel quel
      const filename = `structuresStats_${context.arguments.start}_${context.arguments.end}.csv`
      sendCsvFile(context, csvLines, filename)
    }).catch(context.next)
  })
}
