const general = require('./general')
const hebdo = require('./hebdo')
const ent = require('./ent')
const structures = require('./structures')

/**
 * @Controller Stats
 */
app.controller(function setupStatsController () {
  general(this)
  hebdo(this)
  ent(this)
  structures(this)
})
