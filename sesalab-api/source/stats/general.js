const flow = require('an-flow')
const moment = require('moment')
const { hasProp } = require('sesajstools')

const constants = require('sesalab-commun/constants.js')
const { getFromCacheOrBuild, getStructuresWithLabel, sendCsvFile } = require('./common')

moment.locale('fr')

module.exports = function generalSetup (controller) {
  /**
   * Init stats pour un département
   * @param stats
   * @param departement
   * @param jour
   */
  function initGeneralStatsDepartement (stats, departement, jour) {
    if (!stats.headers) {
      stats.headers = [
        '',
        'Ecoles',
        'Collèges',
        'Lycées',
        'Indéfini',
        'Nb structures',
        'Total',
        '(unité du total)'
      ]
      stats.tables = {}
    }
    if (!stats.tables[departement]) {
      stats.tables[departement] = {
        formateurs: {
          title: 'Etablissements avec au moins un professeur inscrit',
          ecole: 0,
          college: 0,
          lycee: 0,
          indefini: 0,
          total: 0,
          totalUsers: 0,
          unit: 'formateurs'
        },
        elevesHorsEnt: {
          title: 'Etablissements avec au moins un élève inscrit hors connecteur',
          ecole: 0,
          college: 0,
          lycee: 0,
          indefini: 0,
          total: 0,
          totalUsers: 0,
          unit: 'élèves'
        },
        eleves: {
          title: 'Etablissements avec au moins un élève inscrit via un connecteur',
          ecole: 0,
          college: 0,
          lycee: 0,
          indefini: 0,
          total: 0,
          totalUsers: 0,
          unit: 'élèves'
        },
        activeFormateurs: {
          title: `Connexions professeurs depuis le ${jour}`,
          ecole: 0,
          college: 0,
          lycee: 0,
          indefini: 0,
          total: 0,
          totalUsers: 0,
          unit: 'connexions formateur'
        },
        activeEleves: {
          title: `Connexions élève via un ENT depuis le ${jour}`,
          ecole: 0,
          college: 0,
          lycee: 0,
          indefini: 0,
          total: 0,
          totalUsers: 0,
          unit: 'connexions élève'
        }
      }
    }
  }

  /**
   * Incrémente par type d'établissement
   * @private
   * @param {number} count
   * @param {object} statChunk
   * @param {string} category
   */
  function incGeneralStat (count, statChunk, category) {
    if (count > 0) {
      if (hasProp(statChunk, category)) statChunk[category]++
      else statChunk.indefini++
      statChunk.total++
      statChunk.totalUsers += count
    }
  }

  /**
   * Ajoute les stats de cette structure à stats[departement]
   * @param structure
   * @param stats
   * @param callback
   */
  function buildStatsStructure (structure, stats, callback) {
    const { departement, oid } = structure
    const startMoment = moment().subtract(joursGestion, 'days').startOf('day')
    const jour = startMoment.format('DD/MM/YY')
    const startTs = startMoment.valueOf()
    // Initialisation des stats par département (son nom)
    initGeneralStatsDepartement(stats, departement, jour)
    const rows = stats.tables[departement]
    let totalElevesEnt = 0

    flow().seq(function () {
      // nb de formateurs
      $utilisateur.countTeacherByStructure(oid, this)
    }).seq(function (countTeacher) {
      incGeneralStat(countTeacher, rows.formateurs, structure.category)
      // nb d'élèves via ENT
      $utilisateur.countEntStudentsByStructure(oid, this)
    }).seq(function (countElevesEnt) {
      if (countElevesEnt) {
        totalElevesEnt = countElevesEnt
        incGeneralStat(countElevesEnt, rows.eleves, structure.category)
      }
      // pour le nb d'élève hors ENT, on récupère le total
      $utilisateur.countStudentsByStructure(oid, this)
    }).seq(function (countEleves) {
      // et on soustrait du total précédent
      // si y'avait aussi des élèves hors ENT c'est compté 2 fois
      incGeneralStat(countEleves - totalElevesEnt, rows.elevesHorsEnt, structure.category)
      // accès prof
      const filters = {
        structure: oid,
        type: constants.TYPE_FORMATEUR,
        after: startTs
      }
      $utilisateurAcces.count(filters, this)
    }).seq(function (count) {
      incGeneralStat(count, rows.activeFormateurs, structure.category)
      // accès élève
      const filters = {
        structure: oid,
        type: constants.TYPE_ELEVE,
        after: startTs,
        origine: constants.ENT_EXTERNAL_MECH
      }
      $utilisateurAcces.count(filters, this)
    }).seq(function (count) {
      incGeneralStat(count, rows.activeEleves, structure.category)
      callback()
    }).catch(callback)
  }

  /**
   * construit un tableau de lignes (chacune est une string) pour le csv
   * @private
   * @param {Object} stats
   * @return {String[]}
   */
  function getCsvLines (stats) {
    const csvLines = []
    const nbCols = stats.headers.length
    const emptyCols = ';'.repeat(nbCols - 1)
    for (const [departement, table] of Object.entries(stats.tables)) {
      // ajout d'une ligne vide sauf pour le 1er dpt
      if (csvLines.length) csvLines.push(emptyCols)
      // dpt
      csvLines.push(departement + emptyCols) // deux strings
      csvLines.push(stats.headers.join(';'))
      // table est un objet avec les clés title, ecole, college…
      for (const rowObj of Object.values(table)) {
        csvLines.push(Object.values(rowObj).join(';'))
      }
    }

    return csvLines
  }

  /**
   * Retourne une fonction qui prend une cb pour lui filer generalStats recalculé
   * @private
   * @param context
   * @return {function} generalStatsBuilder (callback)
   */
  function getStatsBuilder (context) {
    return function statsBuilder (callback) {
      const stats = {}
      flow().seq(function () {
        $structure.getStructuresForGestion(context, this)
      }).seq(function (structures) {
        if (!structures.length) return context.restKo('Aucune structure dans votre périmètre')
        this(null, getStructuresWithLabel(structures))
      }).seqEach(function (structure) {
        buildStatsStructure(structure, stats, this)
      }).seq(function () {
        callback(null, stats)
      }).done(callback)
    }
  }

  /**
   * Récupère les stats général (from cache ou build)
   * @private
   * @param context
   * @param callback
   */
  function grabStats (context, callback) {
    try {
      const structuresFilter = $structure.getGestionStructuresFilter(context)
      const matchersKey = $structure.getStructuresFilterKey(structuresFilter)
      const cacheKey = `statsGeneral_${matchersKey}_${moment().format('L')}`
      const builder = getStatsBuilder(context)
      getFromCacheOrBuild(cacheKey, builder, callback)
    } catch (error) {
      callback(error)
    }
  }

  // MAIN general
  const $structure = lassi.service('$structure')
  const $utilisateur = lassi.service('$utilisateur')
  const $utilisateurAcces = lassi.service('$utilisateurAcces')
  const $settings = lassi.service('$settings')

  const joursGestion = $settings.get('application.joursGestion', 60)

  /**
   * Retourne un tableau de stats par département.
   *
   * @name StatsGeneral
   * @route {GET} api/stats/general
   * @memberof module:controllers/stats
   */
  controller.get('api/stats/general', function (context) {
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (stats) {
      context.rest({ stats })
    }).catch(context.next)
  })

  /**
   * Export CSV pour les stats générales de chaque département lié à l'administrateur.
   *
   * @name StatsExportGeneral
   * @route {GET} api/stats/export/general
   * @return {JSON}
   * @memberof module:controllers/stats
   */
  controller.get('api/stats/export/general', function (context) {
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (data) {
      const csvLines = getCsvLines(data)
      const filename = 'generalStats_' + moment().format('YYYY-MM-DD') + '.csv'
      sendCsvFile(context, csvLines, filename)
    }).catch(context.next)
  })
}
