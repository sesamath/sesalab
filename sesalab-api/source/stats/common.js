const flow = require('an-flow')
const moment = require('moment')
moment.locale('fr')

const CACHE_TTL = 6 * 3600 // 6h

let $cache

/**
   * Retourne le label complet, avec département et type
   * @private
   * @param structure
   * @return {string}
   */
function getStructureLabel (structure) {
  const chunks = []
  if (structure.departementNumero) chunks.push(structure.departementNumero)
  if (structure.departement) chunks.push(structure.departement)
  if (structure.ville) chunks.push(structure.ville)
  if (structure.type) chunks.push(`${structure.type} ${structure.nom}`)
  else chunks.push(structure.nom)

  return chunks.join(' - ')
}

module.exports = {
  /**
   * Prend les datas en cache si elles existent, sinon les construits et les mets avant de les retourner.
   *
   * @private
   * @param {string} cacheKey
   * @param {function} computeFn
   * @param callback
   */
  getFromCacheOrBuild: (cacheKey, computeFn, callback) => {
    if (!$cache) $cache = lassi.service('$cache')
    flow().seq(function () {
      $cache.get(cacheKey, this)
    }).seq(function (data) {
      if (data) return callback(null, data)
      computeFn(this)
    }).seq(function (computedData) {
    // on retourne après mise en cache
      $cache.set(cacheKey, computedData, CACHE_TTL, (error) => {
        if (error) console.error(error)
        callback(null, computedData)
      })
    }).catch(callback)
  },

  /**
     * Retourne le moment du dernier 1er sept.
     * @return {moment.Moment}
     */
  getLastFirstSeptMoment: () => {
    const thisYear = moment().format('YYYY')
    let startMoment = moment(`${thisYear}-09-01`)
    if (moment().isBefore(startMoment)) startMoment = startMoment.subtract(1, 'year')
    return startMoment
  },

  /**
   * Ajoute une propriété label à chaque structure et les retourne par ordre alphabétique de label
   * @param {Structure[]} structures
   * @returns {Structure[]}
   */
  getStructuresWithLabel: (structures) => {
    for (const structure of structures) {
      if (!structure.departement) structure.departement = 'Département non renseigné'
      if (!structure.ville) structure.ville = 'Ville non renseignée'
      structure.label = getStructureLabel(structure)
    }
    return structures.sort((a, b) => a.label > b.label ? 1 : -1)
  },

  /**
     * Envoie le csv.
     *
     * @param context
     * @param {String[]} csvLines
     * @param {string} filename
     */
  sendCsvFile: (context, csvLines, filename) => {
    context.contentType = 'application/csv'
    const headers = {
      'Content-Disposition': `attachment; filename="${filename}"`
    }
    context.raw(csvLines.join('\n'), { headers })
  },

  /**
     * Délai de mise en cache (en s)
     */
  CACHE_TTL
}
