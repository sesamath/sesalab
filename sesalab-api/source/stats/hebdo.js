const flow = require('an-flow')
const _ = require('lodash')
const moment = require('moment')
moment.locale('fr')

const constants = require('sesalab-commun/constants.js')

const { getFromCacheOrBuild, getLastFirstSeptMoment, sendCsvFile } = require('./common')

module.exports = function setup (controller) {
  /**
   * Retourne les stats de connexion hebdo
   */
  function buildStats (structuresFilter, cb) {
    // on fonctionne par année scolaire, on démarre au 1er sept dernier
    const startMoment = getLastFirstSeptMoment()
    const now = new Date()
    const stats = {
      headers: ['Mois', 'De', 'à', 'Connexions élève via ENT', 'Connexions élève hors ENT', 'Total'],
      infos: {
        startJour: startMoment.format('L'),
        endJour: moment(now).format('L')
      },
      rows: {}
    }

    // init des date de début pour le flow
    const startMoments = []
    while (startMoment.isBefore(now)) {
      startMoments.push(moment(startMoment)) // faut cloner pour avoir un nouveau moment
      startMoment.add(1, 'week')
    }

    flow(startMoments).seqEach(function (startMoment) {
      const nextWeek = this
      // moment(startMoment) pour pas muter startMoment
      const endMoment = moment(startMoment).add(1, 'week')
      const baseFilter = {
        type: constants.TYPE_ELEVE,
        after: startMoment.valueOf(),
        before: endMoment.valueOf()
      }
      const filters = Object.assign({}, structuresFilter, baseFilter, { origine: constants.ENT_EXTERNAL_MECH })

      // récup des stats pour cette semaine
      let entConnexions
      flow().seq(function () {
        $utilisateurAcces.count(filters, this)
      }).seq(function (count) {
        entConnexions = count
        // on pourrait chercher origin = 'sesalab' pour avoir les autres, mais au cas où un jour on a d'autres origines on les cherche tous
        // (on a pas de notIn dans count)
        filters.origine = undefined
        $utilisateurAcces.count(filters, this)
      }).seq(function (count) {
        // angular aime pas les Array…
        const row = {
          mois: startMoment.format('MMMM'),
          de: startMoment.format('L'),
          a: endMoment.subtract(1, 'day').format('L'), // end est exclus
          cnxEnt: entConnexions,
          cnxHorsEnt: count - entConnexions,
          total: count
        }
        nextWeek(null, row)
      }).catch(cb)
    }).seq(function (rows) {
      // faut filer à angular un objet avec des clés unique et pas un tableau,
      // on en profite pour calculer le total
      let i = 0
      let cnxEntTot = 0
      let cnxHorsEntTot = 0
      let totalCumul = 0
      rows.forEach(row => {
        stats.rows[`row${i++}`] = row
        cnxEntTot += row.cnxEnt
        cnxHorsEntTot += row.cnxHorsEnt
        totalCumul += row.total
      })
      stats.rows.total = {
        mois: 'TOTAL',
        de: stats.infos.startJour,
        a: stats.infos.endJour,
        cnxEnt: cnxEntTot,
        cnxHorsEnt: cnxHorsEntTot,
        total: totalCumul
      }
      // on envoie
      cb(null, stats)
    }).catch(cb)
  }

  function getStatsBuilder (context, structuresFilter) {
    return function statBuilder (callback) {
      buildStats(structuresFilter, callback)
    }
  }

  function grabStats (context, callback) {
    try {
      const structuresFilter = $structure.getGestionStructuresFilter(context)
      const matchersKey = $structure.getStructuresFilterKey(structuresFilter)
      const cacheKey = `statsHebdo_${matchersKey}_${moment().format('YYYY-MM-DD')}`
      const builder = getStatsBuilder(context, structuresFilter)
      getFromCacheOrBuild(cacheKey, builder, callback)
    } catch (error) {
      callback(error)
    }
  }

  // MAIN
  const $structure = lassi.service('$structure')
  const $utilisateurAcces = lassi.service('$utilisateurAcces')

  /**
   * Retourne un tableau de stats propres aux élèves de chaque structure.
   *
   * @name StatsExportHebdo
   * @route {GET} api/stats/eleves
   * @return {JSON}
   * @memberof module:controllers/stats
   */
  controller.get('api/stats/hebdo', function (context) {
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (stats) {
      context.rest({ stats })
    }).catch(context.next)
  })

  /**
   * Export CSV pour les stats propres aux élèves de chaque structure.
   *
   * @name StatsExportHebdo
   * @route {GET} api/stats/export/eleves
   * @return {JSON}
   * @memberof module:controllers/stats
   */
  controller.get('api/stats/export/hebdo', function (context) {
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (stats) {
      const csvLines = _.map(stats.rows, (row) => _.toArray(row).join(';'))
      csvLines.unshift(stats.headers.join(';'))
      const filename = 'statsHebdo_' + moment().format('YYYY-MM-DD') + '.csv'
      sendCsvFile(context, csvLines, filename)
    }).catch(context.next)
  })
}
