const flow = require('an-flow')
const moment = require('moment')
moment.locale('fr')

const { ENT_EXTERNAL_MECH, TYPE_ELEVE } = require('sesalab-commun/constants.js')

const { getFromCacheOrBuild, getLastFirstSeptMoment, getStructuresWithLabel, sendCsvFile } = require('./common')

module.exports = function generalSetup (controller) {
  function buildStatsStructure (structure, stats, callback) {
    flow().seq(function () {
      const filters = {
        structure: structure.oid,
        type: TYPE_ELEVE,
        origine: ENT_EXTERNAL_MECH,
        after: stats.infos.startDate
      }
      $utilisateurAcces.count(filters, this)
    }).seq(function (countConnexions) {
      if (!countConnexions) return callback()
      const { departement, nom, oid, type, ville } = structure
      if (!stats.tables[departement]) stats.tables[departement] = {}
      // sans le préfixe s ça casse l'ordre :-S (oid est bien une string
      stats.tables[departement]['s' + oid] = {
        ville,
        label: `${type} ${nom}`,
        connexions: countConnexions
      }
      callback()
    }).catch(callback)
  }

  function getCsvLines (stats) {
    const emptyLine = ';'.repeat(stats.headers.length - 1)
    const csvLines = []
    csvLines.push('Cumul des connexions élève via ENT, par établissement' + emptyLine)
    csvLines.push(`Période du ${stats.infos.startJour} au ${stats.infos.endJour}${emptyLine}`)
    for (const [departement, statsDep] of Object.entries(stats.tables)) {
      csvLines.push(emptyLine)
      csvLines.push(departement + emptyLine)
      csvLines.push(stats.headers.join(';'))
      for (const row of Object.values(statsDep)) {
        csvLines.push(Object.values(row).join(';'))
      }
    }

    return csvLines
  }

  function getStatsBuilder (context) {
    return function statsBuilder (callback) {
      const startMoment = getLastFirstSeptMoment()
      const stats = {
        headers: ['Ville', 'Établissement', 'Connexions élève via ENT'],
        infos: {
          startDate: startMoment.toDate(),
          startJour: startMoment.format('L'),
          endJour: moment().format('L')
        },
        tables: {}
      }
      flow().seq(function () {
        $structure.getStructuresForGestion(context, this)
      }).seq(function (structures) {
        if (!structures.length) return context.restKo('Aucune structure dans votre périmètre')
        // tri
        this(null, getStructuresWithLabel(structures))
      }).seqEach(function (structure) {
        buildStatsStructure(structure, stats, this)
      }).seq(function () {
        // on ajoute les totaux en fin de chaque tableaux
        const reducer = (cumul, row) => cumul + row.connexions
        for (const statsDep of Object.values(stats.tables)) {
          const total = Object.values(statsDep).reduce(reducer, 0)
          statsDep.total = {
            ville: 'TOTAL',
            label: '',
            connexions: total
          }
        }
        callback(null, stats)
      }).catch(callback)
    }
  }

  function grabStats (context, callback) {
    try {
      const structuresFilter = $structure.getGestionStructuresFilter(context)
      const matchersKey = $structure.getStructuresFilterKey(structuresFilter)
      const cacheKey = `statsEnt_${matchersKey}_${moment().format('L')}`
      const builder = getStatsBuilder(context, structuresFilter)
      getFromCacheOrBuild(cacheKey, builder, callback)
    } catch (error) {
      callback(error)
    }
  }

  // MAIN
  const $structure = lassi.service('$structure')
  const $utilisateurAcces = lassi.service('$utilisateurAcces')

  /**
   * Retourne la liste des établissements ayant un élève connecté via ENT.
   *
   * @name StatsENT
   * @route {GET} api/stats/ent
   * @return {JSON}
   */
  controller.get('api/stats/ent', function (context) {
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (stats) {
      // si y'a des stats on envoie
      if (Object.keys(stats.tables).length) return context.rest({ stats })
      // sinon on explicite
      const debut = moment(stats.infos.startDate).format('L')
      context.restKo(`Aucune connexion via un ENT depuis le ${debut} sur les structures de votre périmètre`)
    }).catch(context.next)
  })

  /**
   * Export CSV pour la liste des établissements ayant un élève connecté via ENT.
   *
   * @name StatsExportENT
   * @route {GET} api/stats/export/ent
   * @return {JSON}
   */
  controller.get('api/stats/export/ent', function (context) {
    flow().seq(function () {
      context.forceFormateurAdmin(this)
    }).seq(function () {
      grabStats(context, this)
    }).seq(function (stats) {
      const csvLines = getCsvLines(stats)
      const filename = 'entStats_' + moment().format('L') + '.csv'
      sendCsvFile(context, csvLines, filename)
    }).catch(context.next)
  })
}
