const appLog = require('../tools/appLog')

/**
 * @module services/$seance
 */
/**
 * Les méthodes pour récupérer ou manipuler des objets Seance.
 *
 * @service $seance
 * @name $seance
 * @type Object
 * @requires Groupe
 * @requires Seance
 * @requires Sequence
 * @requires Utilisateur
 * @requires $resultat
 * @requires $settings
 * @requires $structure
 */
app.service('$seance', function (Groupe, Seance, Sequence, Utilisateur, $resultat, $settings) {
  const maxResults = $settings.get('application.maxResults', 500)

  /**
   * @callback seancesCallback
   * @param {Error} [error]
   * @param {Seance[]} seances
   */

  /**
   * Retourne toutes les séances faites par un des utilisateurs donnés
   * @memberOf $seance
   *
   * @todo mettre ce bypass du hardLimit dans le controleur (qui ne devrait pas faire de bypass mais renvoyer un next)
   * @param {string[]}    usersIds Tableau contenant les identifiants des utilisateurs
   * @param {Object}   options  Tableau d'options pour filtrer les séances
   * @param {seancesCallback} callback
   */
  function withUsers (usersIds, options, callback) {
    function grab (skip) {
      const query = Seance.match('eleve').in(usersIds)
      if (options.includeDeleted) query.includeDeleted()
      query.grab({ limit: maxResults, skip }, (error, seances) => {
        if (error) return callback(error)
        allSeances = allSeances.concat(seances)
        if (seances.length === maxResults) grab(skip + maxResults)
        else callback(null, allSeances)
      })
    }

    let allSeances = []
    if (!usersIds.length) return callback(null, allSeances)
    grab(0)
  }

  /**
   * Construit une séance virtuelle pour une liste de participants, à partir de leur séance respective.
   * On prend :
   * - dernierVisionnage le dernier de toutes les séances
   * et pour chaque ressource
   * - nbEssai minimum (pour ne pas bloquer le groupe si l'un est au max)
   * - pourcentageSucces minimum
   * - fin true si c'est le cas pour tous les élèves.
   *
   * @private
   * @param seancesEleve
   * @param participants
   * @param sequenceOid
   * @return {Seance}
   */
  function buildVirtualSeanceForParticipants (seancesEleve, participants, sequenceOid) {
    if (!seancesEleve?.length) {
      return {
        sequence: sequenceOid,
        ressources: []
      }
    }
    // pas la peine de tripoter la séance dans tous les sens pour la reconstituer à l'identique
    if (participants.length === 1) {
      // un seul participant avec au moins une séquence
      if (seancesEleve.length > 1) return deduplicate(seancesEleve)
      return seancesEleve[0]
    }

    // valeurs par défaut de la séance
    // pour le dernier visionnage il faut comparer les timestamps
    let dernierVisionnage
    let dernierVisionnageTs = 0
    for (const seance of seancesEleve) {
      if (seance.dernierVisionnage == null) continue
      const ts = seance.dernierVisionnage.getTime()
      if (ts > dernierVisionnageTs) {
        dernierVisionnageTs = ts
        dernierVisionnage = seance.dernierVisionnage
      }
    }
    /** @type {Seance} */
    const seanceVirtuelle = {
      sequence: sequenceOid,
      dernierVisionnage,
      ressources: []
    }

    // regroupe l'avancement par ressource, dans un array pour chaque ressource
    const ressourceStateByRid = {}
    for (const seance of seancesEleve) {
      for (const ressource of seance.ressources) {
        if (!ressourceStateByRid[ressource.rid]) ressourceStateByRid[ressource.rid] = []
        ressourceStateByRid[ressource.rid].push(ressource)
      }
    }

    for (const [rid, stateArray] of Object.entries(ressourceStateByRid)) {
      // Ici on a que les vraies séances, donc max une par élève, pour savoir
      // si tous les participants ont au moins un essai sur cette ressource
      // on peut simplement regarder le nb
      const allPupilsHaveAState = stateArray.length === participants.length
      // État par défaut de la ressource si tous les élèves ne l'ont pas traitée
      const ressourceVirtualState = {
        rid,
        nbEssais: 0,
        pourcentageSucces: 0,
        fin: false
      }

      if (allPupilsHaveAState) {
        // Si tous les élèves du groupe connecté ont déjà travaillé sur cette ressource (dans cette séquence)
        // => on analyse et on prend le minimum de l'ensemble de leurs résultats en terme de nombre d'essais et de scores
        let { nbEssais, pourcentageSucces, fin } = stateArray[0]
        for (const state of stateArray.slice(1)) {
          if (state.nbEssais < nbEssais) nbEssais = state.nbEssais
          if (state.pourcentageSucces < pourcentageSucces) pourcentageSucces = state.pourcentageSucces
          if (!state.fin && fin) fin = false
        }
        Object.assign(ressourceVirtualState, { nbEssais, pourcentageSucces, fin })
      }

      seanceVirtuelle.ressources.push(ressourceVirtualState)
    }

    return seanceVirtuelle
  }

  /**
   * Retourne une seance qui cumule les infos de toutes les séances et supprime les autres (en tâche de fond)
   * @param {Seance[]} seances
   * @returns {Seance}
   * @throws {Error} si seances n'était pas un tableau d'au moins deux séances, toutes avec même séquence et élève
   */
  function deduplicate (seances) {
    if (seances.length < 2) {
      throw Error('Impossible de retirer des doublons inexistants')
    }
    const { sequence, eleve } = seances[0]
    if (seances.some(s => s.sequence !== sequence || s.eleve !== eleve)) {
      throw Error('Impossible de dédoublonner une liste incohérente')
    }
    appLog.error(`${seances.length} seances en doublon (séquence ${seances[0].sequence} et élève ${seances[0].eleve})`, ...seances)
    const seance = seances.pop()
    if (!seance.ressources) seance.ressources = []
    for (const s of seances) {
      if (s.dernierVisionnage.getTime() > seance.dernierVisionnage.getTime()) {
        seance.dernierVisionnage = s.dernierVisionnage
      }
      for (const r of s.ressources) {
        const ressource = seance.ressources.find(ress => ress.rid === r.rid)
        if (ressource) {
          // on garde le plus favorable à l'élève
          if (r.nbEssais > ressource.nbEssais) ressource.nbEssais = r.nbEssais
          if (r.pourcentageSucces > ressource.pourcentageSucces) ressource.pourcentageSucces = r.pourcentageSucces
          if (r.fin && !ressource.fin) ressource.fin = r.fin
        } else {
          seance.ressources.push(r)
        }
      }
      // on lance la suppression qui se terminera en tâche de fond
      s.delete((error) => console.error(error))
    }
    return seance
  }

  /**
   * Retourne une séance pour l'élève en la créant si elle n'existe pas dans la liste
   * (ça l'ajoute au tableau).
   * Gère les doublons (et les vire du tableau et de mongo)
   * @param {Seance[]} seancesEleves
   * @param {string} eleveOid
   * @param {string} rid
   * @param {string} sequenceOid nécessaire au cas ou la liste est vide (doit être identique pour toutes les séances de la liste)
   * @return {Seance} la séance pour cet élève, contenant au moins rid
   * @memberof $seance
   */
  function findOrCreateEleveSeanceInArray (seancesEleves, eleveOid, rid, sequenceOid) {
    const seanceTemplate = {
      eleve: eleveOid,
      sequence: sequenceOid,
      ressources: [
        {
          rid,
          nbEssais: 0,
          pourcentageSucces: 0,
          fin: false
        }
      ],
      dernierVisionnage: new Date()
    }

    const seancesEleve = seancesEleves.filter(seance => seance.eleve === eleveOid)
    let seance
    // si y'en a pas on retourne la séance minimaliste
    if (!seancesEleve.length) {
      seance = Seance.create(seanceTemplate)
      seancesEleves.push(seance)
      return seance
    }
    // si y'en a plus d'une y'a un pb
    if (seancesEleve.length > 1) {
      seance = deduplicate(seancesEleve)
      // faut aussi nettoyer seancesEleves
      while (true) {
        const index = seancesEleves.findIndex(s => s.eleve === eleveOid && s.oid !== seance.oid)
        if (index === -1) break
        seancesEleves.splice(index, 1)
      }
    } else {
      seance = seancesEleve[0]
    }
    // Si l'élève n'a pas cette ressource dans sa séance, on l'ajoute
    if (seance.ressources.every(r => r.rid !== rid)) {
      seance.ressources.push(seanceTemplate.ressources[0])
    }
    return seance
  }

  /**
   * @typedef $seance~getSeancesPourSequencesCallback
   * @param {Error|undefined} error
   * @param {Seance[]}
   */
  /**
   * Purge les séances attachées à cette séquence
   * @param {string} sequenceOid
   * @param {$seance~getSeancesPourSequencesCallback} callback
   * @memberof $seance
   */
  function purgeSequence (sequenceOid, callback) {
    Seance.match('sequence').equals(sequenceOid).purge(callback)
  }

  /**
   * Supprime toutes les séances de l'élève (includeDeleted)
   * @param {string} eleveOid
   * @param callback
   */
  function purgeEleve (eleveOid, callback) {
    Seance.match('eleve').equals(eleveOid).includeDeleted().purge(callback)
  }

  return {
    buildVirtualSeanceForParticipants,
    deduplicate,
    findOrCreateEleveSeanceInArray,
    purgeEleve,
    purgeSequence,
    withUsers
  }
})
