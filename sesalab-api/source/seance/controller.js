/**
 * @module controllers/seance
 */
const flow = require('an-flow')

/**
 * @Controller SeanceController
 * @requires Groupe
 * @requires Seance
 * @requires Sequence
 * @requires Utilisateur
 * @requires $seance
 */
app.controller(function (Groupe, Seance, Sequence, Utilisateur, $resultat, $seance, $session) {
  /**
   * Renvoie la dernière séance (sequenceOid passé dans ?sequence=…),
   * Attention, sans la propriété eleve (une seance est pour un seul élève,
   * ici ce qui est retourné est valable pour plusieurs dans le cas d'une connexion multiple)
   * @see Entity
   * @name LastSeance
   * @route {GET} api/last-seance
   * @authentication Cette route nécessite un utilisateur connecté
   */
  this.get('api/last-seance', function (context) {
    const currentEleves = $session.getCurrentEleves(context)
    if (!currentEleves || !currentEleves.length) return context.accessDenied()
    const participantOids = currentEleves.map(e => e.oid)
    const sequenceOid = context.get.sequence
    if (!sequenceOid) return context.restKo('Séquence manquante')

    flow().seq(function () {
      // avec les élèves A & B, ici on remonte les Seances de cette séquence pour A et B
      // (mais y'en a pas pour A + B, elles sont virtuelles dans ce cas)
      Seance
        .match('sequence').equals(sequenceOid)
        .match('eleve').in(participantOids)
        .grab(this)
    }).seq(function (seancesEleve) {
      const virtualLastSeance = $seance.buildVirtualSeanceForParticipants(seancesEleve, participantOids, sequenceOid)
      context.rest(virtualLastSeance)
    }).catch(context.next)
  })

  /**
   * Démarrage d'une séance avec une ressource (retourne les props de cette ressource)
   * @route {POST} api/seance/start
   * @authentication Cette route nécessite un utilisateur connecté
   * @bodyparam {string} sequenceOid Identifiant de la {@link Sequence} que l'on vient de démarrer
   * @bodyparam {string} selectedRessourceRid Identifiant de la ressource que l'on vient de démarrer
   */
  this.post('api/seance/start', function (context) {
    // La séance est créée par un formateur qui teste la vue d'un élève, on ne sauvegarde rien.
    if ($session.isAuthenticatedAsFormateur(context)) return context.rest({})
    const currentEleves = $session.getCurrentEleves(context)
    if (!currentEleves) return context.accessDenied()
    const { sequenceOid, selectedRessourceRid } = context.post
    if (!sequenceOid) return context.restKo('Aucune séquence fournie')
    if (!selectedRessourceRid) return context.restKo('Aucune ressource sélectionnée')
    const participantOids = currentEleves.map(e => e.oid)

    // flow principal
    flow().seq(function () {
      // on vérifie qu'elle existe
      Sequence.match('oid').equals(sequenceOid).grabOne(this)
    }).seq(function (sequence) {
      if (!sequence) return context.restKo('La séquence n’existe pas (ou plus)')

      // on refresh les infos associées au readonlyToken en cache
      $session.getReadOnlyToken(context)

      // et on va chercher la séance
      Seance
        .match('sequence').equals(sequenceOid)
        .match('eleve').in(participantOids)
        .grab(this)
    }).seq(function (seancesEleve) {
      // Pour chaque élève on met à jour leur avancement sur cette séquence (en terme
      // de nombre d'essais pour l'instant)
      flow(participantOids)
        .seqEach(function (eleveOid) {
          const seanceEleve = $seance.findOrCreateEleveSeanceInArray(seancesEleve, eleveOid, selectedRessourceRid, sequenceOid)
          this(null, seanceEleve)
        })
        .done(this)
    }).seq(function (seancesEleve) {
      const virtualSeance = $seance.buildVirtualSeanceForParticipants(seancesEleve, participantOids, sequenceOid)
      const ressState = virtualSeance.ressources.find(r => r.rid === selectedRessourceRid)
      context.rest(ressState)
    }).catch(context.next)
  })
})
