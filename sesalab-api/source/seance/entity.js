const appLog = require('sesalab-api/source/tools/appLog')

/**
 * @typedef Seance~ressourceItem
 * @type {Object}
 * @property {string} rid
 * @property {number} nbEssais
 * @property {number} pourcentageSucces entier entre 0 et 100
 * @property {boolean} fin
 */

/**
 * Une séance donne l'état d'avancement d'une séquence pour un élève donné
 * (dernier visionnage et scores/nombre d'essais sur chaque ressource de la séquence).
 *
 * Elle est donc unique par couple séquence-élève
 *
 * Pour les élèves connectés à plusieurs, il n'y a pas de Seance en bdd mais
 * on en construit une virtuelle à partir de leur séances respectives à chaque fois.
 * (car faut mettre à jour à chaque login multiple avec les séances individuelles qui
 * ont pu évoluer entre temps).
 *
 * Pour cette séquence virtuelle (cf buildVirtualSeanceForParticipants dans seance/service)
 * qui représente l'avancement du groupe, ça donne par exemple :
 *   si élève1 a fini l'exo A mais pas élève2, à eux deux ils n'ont pas fini l'exo
 *   si élève1 a fait 80% sur exo A et élève2 n'a pas fait l'exo, à eux deux ils repartent à 0.
 *   si élève1 a fait 1 essai sur exo A et élève2 2 essais, à eux deux ils repartent à 1 essai
 *      (pour ne pas qu'un élève ayant épuisé son nombre d'essais bloque le groupe).
 *
 * Chaque "avancement" de cette séance à plusieurs sera sauvegardée individuellement
 * dans les séances des élèves, à condition que le résultat soit "meilleur" que leur
 * résultat individuel antérieur.
 *
 * @Entity Seance
 * @class Seance
 * @property {string} oid l'oid de la séance (bijectif sur sequence+eleve, ça pourrait être la concaténation des deux)
 * @property {string} sequence l'oid de la séquence
 * @property {string} eleve l'oid de l'élève
 * @property {Seance~ressourceItem[]} ressources
 * @property {Date} dernierVisionnage
 */
app.entity('Seance', function () {
  // JSON-Schema
  this.validateJsonSchema({
    type: 'object',
    properties: {
      oid: { type: 'string' },
      sequence: { type: 'string' },
      eleve: { type: 'string' },
      dernierVisionnage: { instanceof: 'Date' },
      ressources: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            rid: { type: 'string' },
            nbEssais: { type: 'number' },
            pourcentageSucces: { type: 'number' },
            fin: { type: 'boolean' }
          }
        }
      }
    },
    required: ['sequence', 'eleve']
  })
  this.defineIndex('sequence')
  this.defineIndex('eleve')

  // pas de backup en beforeDelete pour les séances

  // @FIXME hotfix car plein de séances arrivent sans fin, ça devait être contrôlé en amont
  this.beforeStore(function (cb) {
    if (this.ressources?.length) {
      let errorType = 0 // 1 pour null|undefined et 2 pour autre chose
      const trace = JSON.stringify(this.ressources) // faut le mémoriser au cas où
      for (const r of this.ressources) {
        if (typeof r.fin !== 'boolean') {
          if (r.fin === null || r.fin === undefined) {
            if (!errorType) errorType = 1
          } else {
            errorType = 2
          }
          r.fin = false
        }
      }
      if (errorType) {
        if (errorType === 1) appLog.error(`Séance ${this.oid} avec une ressource ayant fin null|undefined (mise à false)`)
        else appLog.error(`Séance ${this.oid} avec une ressource ayant une prop fin ni boolean ni nullish (mise à false)`, trace)
      }
    }
    cb()
  })
})
