/**
 * @module templates/mail
 */

const { diffDays, formatDate } = require('sesajs-date')
/**
 * Retourne le nom de la structure, avec éventuellement le type en préfixe et le code entre parenthèse à la fin
 * @private
 * @param {Structure} structure
 * @return {string}
 */
function getStructureTitre (structure) {
  let titre = ''
  if (structure.type) titre = `${structure.type} `
  titre += structure.nom
  if (structure.code) titre += ` (${structure.code})`
  return titre
}

/**
 * Retourne le corps du mail de bienvenue
 * @param {object} data
 * @param {string} data.link
 * @param {string} data.login
 * @param {string} data.nom
 * @param {string} data.prenom
 * @param {string} [data.appName=Sesalab]
 * @throws s'il manque un paramètre (nom ou prénom peut être vide mais pas les deux)
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextWelcome ({ appName, link, login, nom, prenom }) {
  if (!link || !login || (!nom && !prenom)) throw new Error('paramètre manquant')
  if (!appName) appName = 'Sesalab'
  return `Bienvenue sur ${appName} ${prenom} ${nom},

Vous avez créé un compte d’identifiant ${login}

Cliquez sur ce lien pour valider votre adresse e-mail : ${link}

-- 
L’équipe ${appName}
`
}

/**
 * Retourne le corps du mail demandant la validation de l'adresse email
 *
 * @param {object} data
 * @param {string} data.appName Nom de l'application
 * @param {Utilisateur} data.utilisateur L'utilisateur à valider
 * @param {string} data.link Lien de validation
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextModified ({ appName, utilisateur, link }) {
  return `Bonjour ${utilisateur.prenom} ${utilisateur.nom} (${utilisateur.login}),

Vous avez modifié votre adresse mail.

Merci de valider cette modification en cliquant sur ce lien : ${link}

-- 
L’équipe ${appName}
`
}

/**
 * Retourne le corps du mail pour changer son mot de passe.
 *
 * @param {object} data
 * @param {string} data.appName Nom de l'application
 * @param {string} data.login Identifiant de l'utilisateur
 * @param {string} data.link Lien de validation
 * @param {string} data.nomStructure Nom de la structure
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextReinitializePassword ({ appName, link, login, nomStructure }) {
  return `Bonjour,

Vous avez demandé à réinitialiser le mot de passe associé au compte ${login} dans la structure ${nomStructure}.

Merci de le confirmer en cliquant sur ce lien : ${link}

-- 
L’équipe ${appName}
`
}

/**
 * Retourne le corps du mail de signalement
 *
 * @param {object} data
 * @param {string} data.appname Nom de l'application
 * @param {Structure} data.structure Structure de l'utilisateur
 * @param {Utilisateur} data.utilisateur Utilisateur souhaitant contacter l'administrateur
 * @param {string} data.message Message de l'utilisateur
 * @param {string} data.browser Nom du navigateur
 * @param {string} data.host
 * @param {string} data.version Version de l'application
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextContact ({ appName, structure, utilisateur, message, browser, host, version, ressourceUrl }) {
  const { login, mail, nom, oid, prenom } = utilisateur
  const { departement, pays, type, ville } = structure

  // attention à répercuter toute modif dans l'init de
  // sesalab-formateur/source/client/classes/dialogs/Contact.js
  return `Signalement de ${nom} ${prenom}
Id : ${login} (${oid})
Mél : ${mail}
Etablissement : ${departement || pays || '-'} / ${ville} / ${type} ${structure.nom} (${structure.oid})
Navigateur : ${browser}
Serveur : ${host}
Version : ${version}
${ressourceUrl ? `Ressource : ${ressourceUrl}\n` : ''}
${message}
`
}

/**
 * Retourne le corps du mail de confirmation de suppression de compte.
 *
 * @param {object} data
 * @param {string} data.appname Nom de l'application
 * @param {string} data.link Lien de suppression
 * @param {Utilisateur} data.utilisateur Utilisateur souhaitant supprimer son compte
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextRemoveAccount ({ appName, link, utilisateur }) {
  return `Bonjour ${utilisateur.prenom} ${utilisateur.nom} (${utilisateur.login}),

Vous avez demandé la suppression de votre compte.

Merci de le confirmer en cliquant sur ce lien : ${link}

-- 
L’équipe ${appName}
`
}

/**
 * Retourne le corps du mail notifiant la validation d'un compte.
 *
 * @param {object} data
 * @param {string} data.appname Nom de l'application
 * @param {Utilisateur} data.utilisateur Utilisateur à notifier
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextAccountAccepted ({ appName, utilisateur }) {
  return `Bonjour ${utilisateur.prenom} ${utilisateur.nom},

Votre compte ${utilisateur.login} a été validé par un formateur de la structure.

-- 
L’équipe ${appName}
`
}

/**
 * Retourne le corps du mail notifiant la non validation d'un compte.
 *
 * @param {object} data
 * @param {string} data.appname Nom de l'application
 * @param {Utilisateur} data.utilisateur Utilisateur à notifier
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextAccountRejected ({ appName, utilisateur }) {
  return `Bonjour ${utilisateur.prenom} ${utilisateur.nom},

La création du compte ${utilisateur.login} a été refusée par un formateur de la structure.

-- 
L’équipe ${appName}
`
}

/**
 * Retourne le corps du mail notifiant la validation ou non d'un compte par un formateur de la structure
 * (en le nommant).
 *
 * @param {object} data
 * @param {string} data.appname Nom de l'application
 * @param {Utilisateur} data.utilisateur Utilisateur validé ou rejetté
 * @param {Utilisateur} data.formateur Formateur à notifier
 * @param {Utilisateur} data.validator Le formateur qui a pris la décision
 * @param {boolean}     data.isValidation true si c'est une validation (donc false => rejet)
 * @param {string}      data.structureLabel Le texte décrivant la structure
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextAccountDecision ({ appName, utilisateur, formateur, validator, isValidation, structureLabel }) {
  const actionText = isValidation ? 'acceptée' : 'refusée'
  return `Bonjour ${formateur.prenom} ${formateur.nom},

La création du compte ${utilisateur.login} (${utilisateur.prenom} ${utilisateur.nom}) dans la structure ${structureLabel} a été ${actionText} par ${validator.prenom} ${validator.nom}.

-- 
L’équipe ${appName}
Merci de ne pas répondre à ce mél automatique
`
}

/**
 * Retourne le corps du mail notifiant les utilisateurs que leur compte n'a toujours pas été validé.
 *
 * @param {object} data
 * @param {string} data.appname Nom de l'application
 * @param {Utilisateur} data.utilisateur Utilisateur validé
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextAccountValidationMissing ({ appName, utilisateur }) {
  const nbJours = diffDays(utilisateur.created, new Date())
  return `Bonjour ${utilisateur.prenom} ${utilisateur.nom},

${nbJours}j après votre demande de création de compte, aucun formateur ne l'a validé ni rejeté.

Veuillez les contacter pour valider votre compte.

-- 
L’équipe ${appName}
Merci de ne pas répondre à ce mél automatique
`
}

/**
 * Retourne le corps du mail notifiant les formateurs d'un compte en attente de validation.
 *
 * @param {object} data
 * @param {string} data.appname Nom de l'application
 * @param {Utilisateur} data.utilisateur Utilisateur validé
 * @param {Utilisateur} data.formateur Formateur à notifier
 * @param {Structure} data.structure Structure
 * @param {string} data.expire Date d'expiration
 * @param {string} data.urlDashboard Lien vers le dashboard des formateurs
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextAccountWaitingReview ({ appName, utilisateur, formateur, structure, expire, urlDashboard }) {
  const type = utilisateur.type ? 'formateur' : 'élève'
  return `Bonjour ${formateur.prenom} ${formateur.nom} (${formateur.login}),

${utilisateur.prenom} ${utilisateur.nom} ${utilisateur.mail ? utilisateur.mail : 'pas de mail fourni'}) a créé le compte ${type} "${utilisateur.login}" sur la structure ${getStructureTitre(structure)} le ${formatDate({ date: utilisateur.created, fr: true })}.

Merci de valider ou refuser son accès depuis votre compte : ${urlDashboard}

Son compte sera automatiquement supprimé le ${expire} si aucun formateur ne prend de décision avant cette date.

-- 
L’équipe ${appName}
Merci de ne pas répondre à ce mél automatique
`
}

/**
 * Retourne le texte d'anomalie ayant occasionné le décalage de la purge établissement
 * @param appName
 * @param utilisateur
 * @param structure
 * @param oldPurge
 * @param newPurge
 * @returns {string}
 */
function getMailTextPurgePostponed ({ appName, utilisateur, structure, oldPurge, newPurge }) {
  return `Bonjour ${utilisateur.prenom} ${utilisateur.nom},

La purge annuelle des comptes élèves de l'établissement ${structure.nom} aurait dû être effectuée le ${oldPurge} mais elle n’a pas eu lieu en raison d'une anomalie.

Vu le retard, cette purge a été reportée au ${newPurge}, afin de vous permettre éventuellement :
- de récupérer des résultats (les séquences ne sont pas modifiées par la purge, simplement vidées de leurs élèves)
- d'informer vos élèves
- de décaler encore un peu cette date de purge (via les préférences de l’établissement).

Avec toutes nos excuses pour cette anomalie.

-- 
L’équipe ${appName}
Merci de ne pas répondre à ce mél automatique
`
}

/**
 * Retourne le corps du mail de rapport de la purge annuelle des élèves (envoyé à chaque formateur par cron)
 *
 * @param {object} data
 * @param {string} data.appName
 * @param {Utilisateur} data.utilisateur
 * @param {Structure} data.structure
 * @return {string}
 * @memberof module:templates/mail
 */
function getMailTextPurgeReport ({ appName, utilisateur, structure, purgeWanted }) {
  // utilisé par source/tasks/cron/cronDaily.js
  return `Bonjour ${utilisateur.prenom} ${utilisateur.nom},

La purge annuelle des comptes élèves de l'établissement ${structure.nom} a été effectuée cette nuit comme programmé (la date de purge était ${purgeWanted}).

-- 
L’équipe ${appName}
Merci de ne pas répondre à ce mél automatique
`
}

module.exports = {
  getMailTextAccountAccepted,
  getMailTextAccountDecision,
  getMailTextAccountRejected,
  getMailTextAccountValidationMissing,
  getMailTextAccountWaitingReview,
  getMailTextContact,
  getMailTextModified,
  getMailTextPurgePostponed,
  getMailTextPurgeReport,
  getMailTextReinitializePassword,
  getMailTextRemoveAccount,
  getMailTextWelcome
}
