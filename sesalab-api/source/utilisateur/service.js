/**
 * @module services/$utilisateur
 */
const flow = require('an-flow')
const { toAscii } = require('sesajstools')
const { addDays, formatDate } = require('sesajs-date')

const { ENT_EXTERNAL_MECH, MIN_PASSWORD_LENGTH, OID_DELETED, TYPE_ELEVE, TYPE_FORMATEUR } = require('sesalab-commun/constants.js')
const {
  getMailTextAccountAccepted,
  getMailTextAccountRejected,
  getMailTextAccountDecision,
  getMailTextAccountWaitingReview
} = require('./mailTemplates')

const logIfError = (error) => { if (error) console.error(error) }

/**
 * @service $utilisateur
 */
app.service('$utilisateur', function (Groupe, Journal, Sequence, Utilisateur, UtilisateurAcces, $crypto, $groupe, $mail, $settings, $structure, $validators) {
  const maxResults = $settings.get('application.maxResults', 500)
  const heuresAffichables = $settings.get('application').heuresAffichables

  /**
   * Supprime tous les formateurs qui n'ont pas validé leur compte par mail.
   * (Attention, ça peut en remonter plus de maxResults)
   *
   * @memberof $utilisateur
   * @param {Date}     date     Date limite de création d'utilisateur (supprimera tous les non validés créés avant)
   * @param {function} callback Callback rappelée avec (error, nbDeleted)
   * @throws {Error} Si date est invalide ou trop récente (max j - 24h)
   * @memberof $utilisateur
   */
  function purgeTeachersNotValidated (date, callback) {
    if (!(date instanceof Date)) throw Error('date invalide')
    // garde-fou, on veut au minimum un jour de validité avant suppression
    if (date >= new Date(Date.now() - 3600_000 * 24)) throw Error(`date de purge des formateurs non validés trop récente ${date} (il faut au moins 24h de validité)`)
    Utilisateur
      .match('type').equals(TYPE_FORMATEUR)
      .match('isValid').notEquals(true)
      .match('externalMech').isNull()
      .match('created').before(date)
      .purge(callback)
  }

  /**
   * Purge les comptes élèves non valides et créés avant date
   * @param {Date} date
   * @param callback
   * @throws {Error} Si date est invalide ou trop récente (max j - 24h)
   * @memberof $utilisateur
   */
  function purgeStudentsNotValidated (date, callback) {
    if (!(date instanceof Date)) throw Error('date invalide')
    if (date >= new Date(Date.now() - 3600 * 24)) throw Error(`date trop récente ${date}`)
    Utilisateur
      .match('type').equals(TYPE_ELEVE)
      .match('isValid').notEquals(true) // false et null
      .match('created').before(date)
      .purge(callback)
  }

  /**
   * Récupére l'ensemble des formateurs (valides) d'une structure.
   *
   * @param {string}   structureOid Identifiant interne de la structure
   * @param {function} callback retour
   * @memberof $utilisateur
   */
  function grabTeacherByStructure (structureOid, callback) {
    flow().seq(function () {
      Utilisateur
        .match('type').equals(TYPE_FORMATEUR)
        .match('structures').equals(structureOid)
        // le match précédent devrait garantir un user validé (sinon son index structure est null)
        // mais on le précise quand même
        .match('isValid').equals(true)
        .grab(this)
    }).seq(function (formateurs) {
      // on filtre par sécurité, mais en 2018-01 les inactifs ne sont pas indexés sur la structure
      callback(null, formateurs.filter(formateur => $validators.isValid(formateur)))
    }).catch(callback)
  }

  /**
   * Supprime tout ce qui est relatif à l'élève (mais pas l'élève)
   * - Enlève l'élève de ses classes
   * - Enlève l'élève de ses groupes
   * - Supprime les résultats de l'élève
   * - Supprime les séances de l'élève
   * @param {Utilisateur} eleve Un élève
   * @param {function} cb Callback
   * @memberof $utilisateur
   */
  function purgeEleve (eleve, cb) {
    if (eleve.type !== TYPE_ELEVE) return cb(new Error('L’utilisateur donné n’est pas un élève'))

    const $resultat = lassi.service('$resultat')
    const $seance = lassi.service('$seance')

    flow().seq(function () {
      // On nettoie les groupes/classes de l'élève
      $groupe.withUsers([eleve.oid], this)
    }).seqEach(function (groupe) {
      $groupe.removeStudents(groupe, [eleve.oid], this)
    }).seq(function () {
      $resultat.purgeEleve(eleve.oid, this)
    }).seq(function () {
      // nettoie les séances de l'élève (chaque séance n'a qu'un élève)
      $seance.purgeEleve(eleve.oid, this)
    }).done(cb)
  }

  /**
   * Supprime tout ce qui est relatif au formateur (mais pas le formateur) :
   * - Supprime les groupes du professeur
   * - marque ses séquences avec un owner 'deleted' (compliqué de voir si on peut la supprimer ou pas)
   * - vire la structure si c'était le dernier formateur dedans et qu'il n'y a plus d'élèves
   * - @todo informer les sésatheques que le user est supprimé
   *
   * @param {Utilisateur} formateur Un formateur
   * @param {function} cb Callback
   * @memberof $utilisateur
   */
  function purgeFormateur (formateur, cb) {
    // Pour un prof supprimé, on supprime ses groupes (mais pas les classes)
    flow().seq(function () {
      // à priori, ça peut pas remonter de classe…
      Groupe.match('owner').equals(formateur.oid).grab(this)
    }).seqEach(function (groupe) {
      // …mais on le vérifie quand même
      if (groupe.isClass) {
        console.error(Error(`groupe ${groupe.oid} est une classe avec un owner (${groupe.owner}) !`))
        return this()
      }
      groupe.delete(this)
    }).seq(function () {
      // @todo Il faudrait aussi supprimer ses séquences, mais c'est vraiment compliqué
      // Il faudrait vérifier
      // - si editableAmis alors faut regarder si y'a d'autres profs dans la structure
      // - qu'il n'y a pas de résultats enregistrés avec des élèves qui existent toujours (ou alors qu'ils peuvent continuer d'y accéder si la séquence a disparue)
      // on se contente pour le moment de mettre le owner à "deleted"
      Sequence.match('owner').equals(formateur.oid).grab(this)
    }).seqEach(function (sequence) {
      sequence.owner = OID_DELETED
      sequence.store(this)
    }).seq(function () {
      checkStructureToRemove(formateur.structures, this)
    }).done(cb)
  }

  /**
   * Affecte tous les contenus de oidToDel à oidToKeep puis le supprime
   * @param {Object} options
   * @param {string} options.oidToKeep
   * @param {string} options.oidToDel
   * @param {callback} next callback rappelée avec (error, userMerged)
   */
  function merge ({ oidToKeep, oidToDel }, next) {
    // lassi.entities ne propose pas de moyen de faire de l'updateMany
    // on pourrait récupérer l'objet db du driver mongo et passer des requêtes dessus,
    // mais ce sera plus pénible à maintenir (on aura ici de la syntaxe
    // qui dépend de la version du driver), et ça zappe les beforeStore des entities.
    // pour un truc qui ne sert que très rarement, et qui est très sensible,
    // on choisit la fiabilité => on charge et sauvegarde les entities une par une

    let uKept, uToDel, journalMessage
    flow().seq(function () {
      Utilisateur.match('oid').in([oidToKeep, oidToDel]).grab(this)
    }).seq(function (users) {
      // on vérifie qu'ils ont le même nom/prénom, aux accents et caractères non-lettre près
      const [u0, u1] = users
      if (!haveAlmostSameName(u0, u1)) {
        return next(Error(`Les deux utilisateurs n’ont pas le même nom, impossible de les fusionner : "${u0.prenom} ${u0.nom}" ≠ "${u1.prenom} ${u1.nom}"`))
      }
      if (u0.type !== u1.type) return next(Error('Les deux utilisateurs ne sont pas du même type, impossible de les fusionner'))
      // ok, on y va
      uKept = u0
      uToDel = u1
      if (u0.type === TYPE_FORMATEUR) _mergeFormateurs(uKept, uToDel, this)
      else _mergeEleves(uKept, uToDel, this)
    }).seq(function () {
      UtilisateurAcces.match('utilisateur').equals(oidToDel).grab(this)
    }).seqEach(function (ua) {
      ua.utilisateur = oidToKeep
      ua.store(this)
    }).seq(function () {
      // màj uKept

      // structures
      for (const sid of uToDel.structures) {
        if (!uKept.structures.includes(sid)) uKept.structures.push(sid)
      }

      // mail
      if (!uKept.mail && uToDel.mail) {
        uKept.mail = uToDel.mail
        if (!uKept.validators) uKept.validators = {}
        uKept.validators.mail = uToDel.validators?.mail
      }

      // sequenceFolders (si y'en a du même nom ça fera des doublons, tant pis
      if (!uKept.sequenceFolders) uKept.sequenceFolders = {}
      if (!uKept.sequenceFolders.sequences) uKept.sequenceFolders.sequences = []
      if (!uKept.sequenceFolders.folders) uKept.sequenceFolders.folders = []
      if (uToDel.sequenceFolders?.sequences?.length) {
        for (const seq of uToDel.sequenceFolders.sequences) {
          uKept.sequenceFolders.sequences.push(seq)
        }
      }
      if (uToDel.sequenceFolders?.folders?.length) {
        for (const folder of uToDel.sequenceFolders.folders) {
          uKept.sequenceFolders.folders.push(folder)
        }
      }

      // idem pour ressourcesFolders
      if (!uKept.ressourcesFolders) uKept.ressourcesFolders = {}
      if (!uKept.ressourcesFolders.ressources) uKept.ressourcesFolders.ressources = []
      if (!uKept.ressourcesFolders.folders) uKept.ressourcesFolders.folders = []
      if (uToDel.ressourcesFolders?.ressources?.length) {
        for (const ress of uToDel.ressourcesFolders.ressources) {
          uKept.ressourcesFolders.ressources.push(ress)
        }
      }
      if (uToDel.ressourcesFolders?.folders?.length) {
        for (const folder of uToDel.ressourcesFolders.folders) uKept.ressourcesFolders.folders.push(folder)
      }

      // màj mongo
      uKept.store(this)
    }).seq(function (userMerged) {
      uKept = userMerged
      // on peut virer l'ancien, mais faut générer le message d'abord
      journalMessage = `${uKept.type === TYPE_FORMATEUR ? 'prof' : 'élève'} ${oidToDel} ${uToDel.prenom} ${uToDel.nom}`
      if (uToDel.externalMech) journalMessage += ` (${uToDel.externalMech}/${uToDel.externalId})`
      journalMessage += ` fusionné dans ${oidToKeep} ${uKept.prenom} ${uKept.nom}`
      if (uKept.externalMech) journalMessage += ` (${uKept.externalMech}/${uKept.externalId})`
      uToDel.delete(this)
    }).seq(function () {
      // entrée de journal pour la suppression
      Journal.create({
        entity: 'Utilisateur',
        entityOid: oidToDel,
        action: 'delete',
        message: journalMessage
      }).store(this)
    }).seq(function () {
      // entrée de journal pour le merge
      Journal.create({
        entity: 'Utilisateur',
        entityOid: oidToKeep,
        action: 'merge',
        message: journalMessage
      }).store(this)
    }).seq(function () {
      this(null, uKept)
    }).done(next)
  }

  function _mergeFormateurs (uKept, uToDel, next) {
    flow().seq(function () {
      // les groupes
      Groupe.match('owner').equals(uToDel.oid).grab(this)
    }).seqEach(function (groupe) {
      groupe.owner = uKept.oid
      groupe.store(this)
    }).seq(function () {
      // les séquences
      Sequence.match('owner').equals(uToDel.oid).grab(this)
    }).seqEach(function (sequence) {
      sequence.owner = uKept.oid
      sequence.store(this)
    }).empty()
      .done(next)
  }

  function _mergeEleves (uKept, uToDel, next) {
    next(Error('_mergeEleves n’est pas encore implémenté'))
  }

  /**
   * Retourne true si les deux utilisateurs ont même nom/prénom à la casse/accentuation/ponctuation près
   * (on enlève les accents et vire tout ce qui n'est pas une lettre avant comparaison)
   * @param user1
   * @param user2
   * @return {boolean}
   */
  function haveAlmostSameName (user1, user2) {
    const cleanNom1 = toAscii(user1.prenom + user1.nom).toLowerCase().replace(/[^a-z]/g, '')
    const cleanNom2 = toAscii(user2.prenom + user2.nom).toLowerCase().replace(/[^a-z]/g, '')
    return cleanNom1 === cleanNom2
  }

  /**
   * Compte les utilisateurs d'une structure.
   *
   * @private
   * @param {string} structureOid
   * @param {Object} filters
   * @param {number} [filters.type] passer constants.TYPE_ELEVE ou constants.TYPE_FORMATEUR
   * @param {number} [filters.externalMech] pour limiter à une provenance
   * @param {number} [filters.origine] idem externalMech
   * @param callback
   * @memberof $utilisateur
   */
  function countByStructure (structureOid, filters, callback) {
    if (typeof filters === 'function') {
      callback = filters
      filters = {}
    }
    const query = Utilisateur
      .match('structures').equals(structureOid)
      .match('isValid').equals(true)
    if (filters.type) query.match('type').equals(filters.type)
    if (filters.externalMech) query.match('externalMech').equals(filters.externalMech)
    else if (filters.origine) query.match('externalMech').equals(filters.origine)
    query.count(callback)
  }

  /**
   * Compte l'ensemble des élèves d'une structure.
   *
   * @param {string} structureOid Identifiant interne de la structure
   * @param {function} callback retour
   * @memberof $utilisateur
   */
  function countStudentsByStructure (structureOid, callback) {
    countByStructure(structureOid, { type: TYPE_ELEVE }, callback)
  }

  /**
   * Compte les élèves ENT d'une structure.
   *
   * @param {string} structureOid Identifiant interne de la structure
   * @param {function} callback retour
   * @memberof $utilisateur
   */
  function countEntStudentsByStructure (structureOid, callback) {
    countByStructure(structureOid, { type: TYPE_ELEVE, externalMech: ENT_EXTERNAL_MECH }, callback)
  }

  /**
   * Compte l'ensemble des formateurs d'une structure (sauf les rejetés, mais ça compte ceux qui ne sont pas encore validés)
   * @param {string} structureOid Identifiant interne de la structure
   * @param {function} callback retour
   * @param {Object} options
   * @param {boolean} [options.withPending= false] Passer true pour compter aussi les profs en attente de validation (par un pair ou de leur adresse mail)
   * @memberof $utilisateur
   */
  function countTeacherByStructure (structureOid, callback, { withPending } = {}) {
    const query = Utilisateur
      .match('type').equals(TYPE_FORMATEUR)
      .match('structures').equals(structureOid)
    if (withPending) query.match('isValid').notEquals(false)
    else query.match('isValid').equals(true) // seulement les validés
    query.count(callback)
  }

  /**
   * Notifie les formateurs de la structure qu'un compte utilisateur est en attente de validation.
   *
   * @param {string} structureOid Identifier de la structure
   * @param {object} utilisateur Utilisateur en attente de validation
   * @param {function} callback
   * @memberof $utilisateur
   */
  function notifyAboutPendingAccount (structureOid, utilisateur, callback) {
    const joursAvantExpiration = $settings.get('application.joursAvantExpiration', 7)
    const dateExpiration = addDays(utilisateur.created, joursAvantExpiration)
    const expire = formatDate({ date: dateExpiration, fr: true })

    let structure

    flow()
      .seq(function () {
        $structure.grabByOid(structureOid, this)
      })
      .seq(function (_structure) {
        structure = _structure
        grabTeacherByStructure(structureOid, this)
      })
      .seqEach(function (formateur) {
        const { mail, oid } = formateur
        if (oid === utilisateur.oid) return this()
        if (!mail) {
          // on râle mais on plante pas
          console.error(Error(`Le formateur ${oid} n’a pas d'adresse mail, c'est louche`))
          return this()
        }

        const data = {
          appName: $settings.get('application.name'),
          utilisateur,
          formateur,
          structure,
          urlDashboard: $settings.get('application').baseUrl + 'formateur',
          expire
        }

        $mail.sendMail({
          to: mail,
          subject: 'Validation de compte',
          text: getMailTextAccountWaitingReview(data)
        }, this)
      })
      .done(callback)
  }

  /**
   * Charge un utilisateur par son oid
   * @param {Object|string} uid si object il est passé directement à la callback
   * @param {function} callback
   * @memberof $utilisateur
   */
  function load (uid, callback) {
    if (typeof uid === 'object') return callback(null, uid)
    Utilisateur.match('oid').equals(uid).grabOne(callback)
  }

  /**
   * Récupère l'ensemble des utilisateurs renseignés par leur ids.
   *
   * @param {string[]} utilisateurOids Tableau d'ids d'utilisateurs
   * @param {boolean} deletedOnly Requête sur les utilisateurs soft deleted ou non
   * @param {Function} callback Callback
   * @memberof $utilisateur
   */
  function loadUsers (utilisateurOids, deletedOnly, callback) {
    function grab (skip) {
      let query = Utilisateur.match('oid').in(utilisateurOids)
      if (deletedOnly) query = query.onlyDeleted()
      query.grab({ limit: maxResults, skip }, (error, users) => {
        if (error) return callback(error)
        allUsers = allUsers.concat(users)
        if (users.length === maxResults) grab(skip + maxResults)
        else callback(null, allUsers)
      })
    }

    let allUsers = []
    if (!utilisateurOids || !utilisateurOids.length) return callback(null, allUsers)
    grab(0)
  }

  /**
   * Enregistre un utilisateur :
   * - en le restaurant s'il était softDeleted
   * - si password est fourni on le hash (mute l'objet utilisateur)
   *
   * @param {Utilisateur|object} utilisateur
   * @param {storeUtilisateurCb} callback
   * @memberof $utilisateur
   */
  function hashPasswordAndStoreUtilisateur (utilisateur, callback) {
    if (utilisateur.password) utilisateur.password = $crypto.hash(utilisateur.password)
    if (utilisateur.store) {
      utilisateur.store(callback)
    } else {
      Utilisateur.create(utilisateur).store(callback)
    }
  }

  /**
   * Callback de récupération d'un message
   * @callback messageCallback
   * @param {Error} [error]
   * @param {string} message
   */
  /**
   * Valide ou rejette un compte en attente d'approbation, notifie les autres formateurs,
   * et l'utilisateur concerné s'il a un mail
   * ATTENTION, ce code suppose qu'un utilisateur en attente de validation ne peut avoir qu'une seule structure !
   * @param {Utilisateur} utilisateur
   * @param {string} validator
   * @param {boolean} isValidation
   * @param {messageCallback} next
   * @memberof $utilisateur
   */
  function processPendingAccount (utilisateur, validator, isValidation, next) {
    const isFormateur = utilisateur.type === TYPE_FORMATEUR
    const strType = isFormateur ? 'formateur' : 'élève'
    const structureOid = utilisateur.structures[0]
    if (utilisateur.structures.length !== 1) console.error(Error(`Utilisateur ${utilisateur.oid} à valider avec ${utilisateur.structures.length} structures`), utilisateur.structures)
    const message = `Création de compte ${strType} ${isValidation ? 'acceptée' : 'refusée'}`
    const appName = $settings.get('application.name')

    // on valide ou supprime avant feedback
    flow().seq(function () {
      if (isValidation) {
        $validators.validateAccount(utilisateur, validator.oid)
        utilisateur.store(this)
      } else {
        utilisateur.$deleteMessage = 'Refus de la création du compte par un formateur de la structure'
        utilisateur.delete(this)
      }
    }).seq(function () {
      // feedback
      if (isValidation) {
        next(null, `Validation effectuée, le compte ${strType} de ${utilisateur.prenom} ${utilisateur.nom} est désormais actif, ${utilisateur.mail ? 'il ou elle a été prévenu(e) par mail' : 'vous pouvez l’en informer'}`)
      } else {
        next(null, 'Votre refus a été pris en compte')
      }

      // notifs en tâche de fond

      // l'utilisateur concerné
      if (utilisateur.mail) {
        const mailData = {
          to: utilisateur.mail,
          subject: message,
          text: isValidation ? getMailTextAccountAccepted({ appName, utilisateur }) : getMailTextAccountRejected({ appName, utilisateur })
        }
        $mail.sendMail(mailData, logIfError)
      }

      // notif des autres formateurs en tâche de fond (on est appelé seulement par le contrôleur
      // GET api/utilisateur/validate/:token/:action donc ils avaient été notifiés de la demande,
      // faut les notifier du résultat)
      let othersTeachers
      let structureLabel
      flow().seq(function () {
        grabTeacherByStructure(structureOid, this)
      }).seq(function (formateurs) {
        othersTeachers = formateurs.filter(({ oid }) => oid !== validator.oid && oid !== utilisateur.oid)
        if (othersTeachers.length) $structure.grabByOid(structureOid, this)
        // sinon on ne fait rien et ce flow s'arrête là
      }).seq(function (structure) {
        structureLabel = `${structure.type} ${structure.nom} (${structure.code})`
        this(null, othersTeachers)
      }).seqEach(function (formateur) {
        if (formateur.mail) {
          $mail.sendMail({
            to: formateur.mail,
            subject: message,
            text: getMailTextAccountDecision({ appName, utilisateur, formateur, validator, isValidation, structureLabel })
          }, logIfError)
        } else {
          console.error(Error(`Le formateur ${formateur.oid} n’a pas d'adresse mail`))
        }
      }).catch(logIfError) // fin du flow notif profs

      // fin des notifs
    }).catch(next)
  }

  /**
   * Passe en revue la liste des oid pour leur affecter une removeDate au lendemain
   * si elle ne possède plus de formateur.
   * C'est pour laisser un peu de temps pour recréer éventuellement un autre compte formateur.
   * Le cron.daily qui fera la suppression vérifiera qu'un compte formateur n'a pas été créé entre temps
   * et qu'aucun élève ne s'est connecté récemment (ils doivent continuer à pouvoir consulter leurs bilans)
   *
   * @param {string[]} structureIds Tableau contenant les ids des structures
   * @param {array} options Tableau d'options
   * @param {Function} callback Callback appelée avec (error, oidProcessed)
   * @memberof $utilisateur
   */
  function checkStructureToRemove (structureIds, callback) {
    const oidsProcessed = []
    flow(structureIds).seqEach(function (structureOid) {
      // on veut aussi ceux en attente de validation
      countTeacherByStructure(structureOid, this, { withPending: true })
    }).seqEach(function (count, index) {
      if (count === 0) $structure.grabByOid(structureIds[index], this)
      else this()
    }).seqEach(function (structure) {
      if (structure) {
        oidsProcessed.push(structure.oid)
        structure.removeDate = new Date(Date.now() + 24 * 60 * 60 * 1000)
        structure.store(this)
      } else {
        this()
      }
    }).seq(function () {
      callback(null, oidsProcessed)
    }).catch(callback)
  }

  /**
   * Génère un password (version non chiffrée).
   *
   * @return {string}
   * @memberof $utilisateur
   */
  function generateUnPassword () {
    let pass = ''
    const allChars = 'azertyupqsdfghjkmwxcvbnAZERTYUPQSDFGHJKMWXCVBN'
    for (let i = 0; i < MIN_PASSWORD_LENGTH - 1; i++) {
      const wpos = Math.floor(Math.random() * allChars.length)
      pass += allChars[wpos]
    }

    return pass + Math.round(9 * Math.random())
  }

  /**
   * Retourne un message d'erreur si le mot de passe ne satisfait pas les contraintes
   * (≠ login, longueur min, lettre + chiffre, pas de séparateur csv)
   * @param {string} password
   * @param {string} login
   * @memberof $utilisateur
   */
  function getPasswordError (password, login) {
    if (!password) return 'Le mot de passe ne doit pas être vide'
    if (password === login) return 'Le mot de passe doit être différent de l’identifiant'
    if (password.length < MIN_PASSWORD_LENGTH) return `Le mot de passe doit faire au moins ${MIN_PASSWORD_LENGTH} caractères`
    // un chiffre et une lettre
    if (!/[0-9]/.test(password)) return 'Le mot de passe doit contenir au moins un chiffre'
    if (!/[a-z]/.test(toAscii(password.toLowerCase()))) return 'Le mot de passe doit contenir au moins une lettre'
    // le séparateur csv doit être également accepté (champs entourés de " dans l'export csv)
    // aucune importance vu qu'il n'est jamais exporté (et que les champs texte sont protégés par des " à l'export)
    // if (password.indexOf(constants.CSV_SEPARATOR) !== -1) return `Le symbole ${constants.CSV_SEPARATOR} ne peut être utilisé`
  }

  /**
   * Obfusque une date.
   *
   * @param {Date}    date     Date à obfusquer
   * @param {string}  timeZone Timezone de la structure
   * @param {boolean} readable Formate la date pour un affichage user-friendly (avant xx ou après yy, sinon xx ou yy)
   * @return {string} La date avec l'heure exacte ou approx suivant le créneau
   * @memberof $utilisateur
   */
  function formatDateWithPrivacy (date, timeZone, readable) {
    // le jour au format dd/mm/YYYY (la locale imposée) d'après la date dans son timezone
    let dateFormatted = (new Intl.DateTimeFormat('fr-FR', { timeZone, dateStyle: 'short' })).format(date) + ' '
    const [hour, min] = (new Intl.DateTimeFormat('fr-FR', { timeZone, hour: '2-digit', minute: '2-digit' })).format(date).split(':')
    // pour l'heure on regarde si on peut laisser l'heure exacte ou si on obfusque
    if (hour < heuresAffichables.min) {
      dateFormatted += readable
        ? `avant ${heuresAffichables.min} h`
        : `${String(heuresAffichables.min).padStart(2, '0')}:00`
    } else if (hour >= heuresAffichables.max) {
      dateFormatted += readable
        ? `après ${heuresAffichables.max} h`
        : `${String(heuresAffichables.max).padStart(2, '0')}:00`
    } else {
      dateFormatted += `${hour}:${min}`
    }

    return dateFormatted
  }

  /**
   * Concatène prenom.nom en minuscule en virant les espaces de début et fin,
   * virant les accents et en remplaçant les espaces par des underscores.
   *
   * @param {Utilisateur} utilisateur
   * @param {boolean} [strict=true] passer false pour ne pas throw en cas de nom & prénom vides (renverra alors une chaîne vide)
   * @return {string}
   * @throws {Error} Si l'utilisateur n'a ni nom ni prénom avec strict = true
   * @memberof $utilisateur
   */
  function normalizeFullname (utilisateur, strict = true) {
    // Les espaces (et autres tabulations) sont remplacés par des underscores;
    const prenom = (typeof utilisateur.prenom === 'string' && utilisateur.prenom.trim()) || ''
    const nom = (typeof utilisateur.nom === 'string' && utilisateur.nom.trim()) || ''
    if (!nom && !prenom) {
      if (strict) throw Error('Utilisateur invalide (sans nom ni prénom)')
      else return ''
    }
    return `${toAscii(prenom)}.${toAscii(nom)}`.toLowerCase().replace(/\s+/g, '_')
  }

  return {
    purgeTeachersNotValidated,
    purgeStudentsNotValidated,
    grabTeacherByStructure,
    purgeEleve,
    purgeFormateur,
    countStudentsByStructure,
    countEntStudentsByStructure,
    countTeacherByStructure,
    haveAlmostSameName,
    load,
    loadUsers,
    merge,
    hashPasswordAndStoreUtilisateur,
    checkStructureToRemove,
    generateUnPassword,
    getPasswordError,
    formatDateWithPrivacy,
    normalizeFullname,
    notifyAboutPendingAccount,
    processPendingAccount
  }
})
