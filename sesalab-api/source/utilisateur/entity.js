const flow = require('an-flow')
const _ = require('lodash')
const { TYPE_ELEVE, TYPE_FORMATEUR } = require('sesalab-commun/constants.js')
const appLog = require('sesalab-api/source/tools/appLog')
const createError = require('../tools/createError')
const isEmail = require('sesalab-commun/isEmail')

/**
 * Représente un utilisateur
 * @see Utilisateur~indexes pour les index
 *
 * @Entity Utilisateur
 * @class Utilisateur
 * @property {string} oid Identifiant unique
 * @property {string} nom Nom (requis si prenom est vide) (INDEXED)
 * @property {string} prenom Prénom (requis si nom est vide) (INDEXED)
 * @property {string} mail Email (requis pour les formateurs hors SSO GAR) (INDEXED)
 * @property {string} newMail Email en attente de validation
 * @property {string} login Login (REQUIRED, INDEXED)
 * @property {string} password Mot de passe chiffré (REQUIRED, INDEXED)
 * @property {Date} created Date de création du compte (INDEXED)
 * @property {string[]} structures oids des {@link Structure} de l'utilisateur (REQUIRED, INDEXED)
 * @property {number} type Cf sesalab-commun/constants.js (0 : élève, 1: formateur) (REQUIRED, INDEXED)
 * @property {boolean} cguAccepted mis à true lors de l'acceptation des CGU
 * @property {boolean} bilanWithSymbols défini dans les préférences utilisateur (affichage sous forme de symboles plutôt que de couleurs, pour les bilans qui le gèrent)
 * @property {string} fontSize défini dans les préférences utilisateur (valeurs : …)
 * @property {string} lastConnexionAsText date de dernière connexion, pour l'afficher dans les bilans (formaté par {@link $utilisateur#formatDateWithPrivacy})
 * @property {string} [externalMech] Nom de la source d'authentification qui nous a filé les infos (INDEXED)
 * @property {string} [externalId] Identifiant chez la source (INDEXED)
 * @property {object} validators liste des éléments validés ou à valider
 * @property {object} validators.account validation du compte
 * @property {string} validators.account.token token de validation
 * @property {Date} validators.account.responseDate date de validation / refus
 * @property {boolean} validators.account.accepted true si compte validé, false si refusé, undefined sinon
 * @property {string} validators.account.validatedBy oid du user qui a répondu
 * @property {Date} validators.account.createdAt date de création du validateur
 * @property {object} [validators.mail] validation du mail
 * @property {string} [validators.mail.token] token de validation
 * @property {Date} [validators.mail.responseDate] date de validation
 * @property {Date} [validators.mail.createdAt] date de création du validateur
 * @property {object} [validators.newMail] validation d'un nouveau mail
 * @property {string} [validators.mail.token] token de validation
 * @property {object} [deleteAccountToken]
 * @property {object} [deleteAccountToken.token]
 * @property {number} [deleteAccountToken.date] Attention, c'est un timestamp
 * @property {object} [reinitializeToken]
 * @property {object} [reinitializeToken.token]
 * @property {number} [reinitializeToken.date] Attention, c'est un timestamp
 * @property {object} [sso] Données éventuellement enregistrées par le provider sso
 * @property {string} [nationalId] Identifiant national élève, si fourni à l'import (pour dédoublonner lors d'un import suivant) (INDEXED)
 * @property {string} [classe] oid de la classe d'un élève (null pour les formateurs)
 * @property {object} [profil] attributs du profil formateur (modifié via ses préférences)
 * @property {string} [profil.type] Nom du profil (nom d'une propriété de settings.application.profils)
 * @property {Utilisateur~profilValue[]} [profil.values] Nom du profil (nom d'une propriété de settings.application.profils)
 * @property {object} ressourcesFolders @todo à documenter
 * @property {object} sequenceFolders @todo à documenter
 * @property {object} groupeFolders @todo à documenter
 * @property {object} sequenceColleguesFolders @todo à documenter
 * @property {object} externalRoles Liste de rôles fourni par le SSO (sous la forme {role1: true, role2:true, …}), ne devrait plus exister ici mais dans la propriété sso
 */

/**
 * Un arbre à présenter dans la liste du profil
 * checked:true & ressourceDuProfil:false => arbre qui n'était pas dans le profil par défaut mais a été ajouté par l'utilisateur
 * checked:false, ressourceDuProfil:true => arbre du profil par défaut que l'utilisateur a retiré du sien
 * Les cas true & true ou false & false ne devraient pas être stockés en base
 * @typedef Utilisateur~profilValue
 * @type {object}
 * @property {string} rid
 * @property {boolean} ressourceDuProfil true si cet arbre est dans le profil par défaut (celui des défini en settings)
 * @property {boolean} checked true si coché dans les préférences
 */
// @todo renommer deleteAccountToken.date en deleteAccountToken.timestamp ou bien stocker une date dedans
// Avec lassi on passe les services dont on a besoin directement dans les arguments de la cb de déclaration et c'est lassi qui se débrouille pour l'injection de dépendances
// mais ils doivent avoir été déclarés avant nous, et ne pas nous utiliser…
// Ici, l'ordre des déclarations dépend de ../index.js
//   require('glob').sync(__dirname+'/**/index.js').map(r => require(r))
// donc pas vraiment garanti, on prend pas de risque et on utilise lassi.service à l'exécution (où tous les services auront été déclarés)
// app.entity('Utilisateur', function (Groupe, $groupe, $resultat, $seance) {
app.entity('Utilisateur', function (Backup, Journal) {
  const UtilisateurDefinition = this
  let Groupe, Structure, $structure, $utilisateur
  const $validators = lassi.service('$validators')

  UtilisateurDefinition.validateJsonSchema(
    {
      // JSON-Schema
      type: 'object',
      properties: {
        oid: { type: 'string' },
        nom: { type: 'string' },
        prenom: { type: 'string' },
        mail: { oneOf: [{ type: 'string', pattern: isEmail.regexp.source }, { enum: [''] }] },
        newMail: { type: 'string', pattern: isEmail.regexp.source },
        login: { type: 'string' },
        password: { type: 'string' },
        created: { instanceof: 'Date' },
        removeDate: { instanceof: 'Date' }, // Pour supprimer un compte après un certain laps de temps (cf. cron)
        structures: {
          type: 'array',
          items: { type: 'string' },
          minItems: 1
        },
        type: {
          enum: [TYPE_ELEVE, TYPE_FORMATEUR]
        },
        mustResetPassword: { type: 'boolean' },
        cguAccepted: { type: 'boolean' },
        bilanWithSymbols: { type: 'boolean' },
        fontSize: { type: 'string' },
        lastConnexionAsText: { type: 'string' },
        externalMech: { type: 'string' },
        externalId: { type: 'string' },
        validators: {
          type: 'object',
          properties: {
            account: {
              type: 'object',
              properties: {
                token: { type: 'string' },
                responseDate: {
                  oneof: [
                    { type: 'null' },
                    { instanceof: 'Date' }
                  ]
                },
                accepted: { type: 'boolean' },
                validatedBy: { type: 'string' },
                createdAt: { instanceof: 'Date' }
              }
            },
            mail: {
              type: 'object',
              properties: {
                token: { type: 'string' },
                responseDate: {
                  oneof: [
                    { type: 'null' },
                    { instanceof: 'Date' }
                  ]
                },
                createdAt: { instanceof: 'Date' }
              }
            },
            newMail: {
              type: 'object',
              properties: {
                token: { type: 'string' }
              }
            }
          }
        },
        deleteAccountToken: {
          type: 'object',
          properties: {
            token: { type: 'string' },
            date: { type: 'number' }
          }
        },
        reinitializeToken: {
          type: 'object',
          properties: {
            token: { type: 'string' },
            date: { type: 'number' }
          }
        },
        sso: {
          type: 'object',
          additionalProperties: true,
          description: 'Données éventuellement enregistrées par le provider sso lors de la dernière connexion'
        },

        // Attributs Eleve
        nationalId: { type: 'string' },
        classe: { type: 'string' },

        // Attributs prof
        profil: {
          type: 'object',
          properties: {
            name: {
              type: 'string',
              description: 'nom du profil de ressource séléctionné (collège, lycée, ...)'
            },
            values: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  rid: { type: 'string' },
                  ressourceDuProfil: { type: 'boolean' },
                  checked: { type: 'boolean' }
                }
              },
              description: 'Enregistre les ressource ajoutées qui ne sont pas dans le profil (checked:true, ressourceDuProfil:false) et celle du profil mais enlevées (checked:false, ressourceDuProfil:true)'
            }
          }
        },
        ressourcesFolders: {
          type: 'object',
          additionalProperties: true // TODO: specifier les propriétés
        },
        sequenceFolders: {
          type: 'object',
          additionalProperties: true // TODO: specifier les propriétés
        },
        groupeFolders: {
          type: 'object',
          additionalProperties: true // TODO: specifier les propriétés
        },
        sequenceColleguesFolders: {
          type: 'object',
          additionalProperties: true // TODO: specifier les propriétés
        },
        // @deprecated
        externalRoles: {
          type: 'object',
          additionalProperties: true,
          description: 'liste de rôles fournie par le sso, ne devrait plus exister (déplacé dans la propriété sso)'
        }
      },
      required: ['login', 'password', 'structures', 'type'],
      errorMessage: {
        properties: {
          mail: 'Impossible de sauvegarder un utilisateur avec un mail invalide'
        }
      }
    }
  )

  UtilisateurDefinition.validate(function (cb) {
    // mail obligatoire pour les formateurs, sauf ceux venant d'un provider SSO
    if (this.type === TYPE_FORMATEUR && !this.externalMech && !this.mail) {
      return cb(Error('Impossible de sauvegarder un formateur sans email'))
    }
    // on vérifie que l'on a toujours externalMech avec externalId (ou aucun)
    if (Boolean(this.externalId) !== Boolean(this.externalMech)) return cb(Error(`externalId et externalMech doivent être tous les deux renseignés (ou aucun), on a ${this.externalMech}/${this.externalId} pour ${this.oid || this.login}`))

    cb()
  })

  /**
   * Vérifie l'unicité externalMech/externalId (affecte oid s'il n'existait pas et qu'on trouve une seul candidat)
   * @private
   * @param {Utilisateur} utilisateur
   * @param {simpleCallback} cb
   */
  function checkDuplicateExternalId (utilisateur, cb) {
    const { externalMech, externalId } = utilisateur
    if (!externalMech && !externalId) return cb()
    if (!externalMech || !externalId) return cb(Error(`utilisateur avec des références externes incohérentes ${externalMech}/${externalId}`))
    const query = UtilisateurDefinition
      .match('externalMech').equals(externalMech)
      .match('externalId').equals(externalId)
    if (utilisateur.oid) query.match('oid').notEquals(utilisateur.oid)
    query.grab(function (error, duplicateUsers) {
      if (error) return cb(error)
      if (!duplicateUsers.length) return cb()
      // FIXME à virer, on ajoute ça pour des pbs SSO, mais c'est le SSO qui devrait faire ce taf au login
      // si y'en a qu'un de même login et qu'on avait pas fourni d'oid, c'est le même user, on affecte oid
      if (duplicateUsers.length === 1 && !utilisateur.oid && utilisateur.login === duplicateUsers[0].login) {
        utilisateur.oid = duplicateUsers[0].oid
        return cb()
      }
      // y'a un pb…
      error = Error(`${utilisateur.externalMech}/${utilisateur.externalId} existe déjà (${duplicateUsers.map(u => u.oid).join(', ')})`)
      // c'est affiché à l'utilisateur mais on veut logguer ça
      console.error(error)
      cb(error)
    })
  }

  UtilisateurDefinition.validateOnChange(['externalMech', 'externalId', 'isDeleted'], function (cb) {
    if (this.isDeleted()) return cb()
    if (this.$byPassDuplicate) return cb()
    if (!this.externalMech && !this.externalId) return cb()

    checkDuplicateExternalId(this, cb)
  })

  UtilisateurDefinition.validateOnChange(['nom', 'prenom'], function (cb) {
    if (!this.nom && !this.prenom) return cb(Error('Utilisateur doit avoir un champ "nom" ou "prenom" non vide'))
    cb()
  })

  // valeurs par défaut à l'instanciation d'une entité
  UtilisateurDefinition.defaults(function () {
    this.validators = {}
  })

  /**
   * Index utilisateurs
   * @typedef UtilisateurIndexes
   * @type {object}
   */
  UtilisateurDefinition
    /**
     * @name login
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('login', 'string')
    /**
     * Pour l'unicité des logins prof (unique & sparse), n'existe pas pour les élèves
     * @name globalUniqueLogin
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('globalUniqueLogin', 'string', { unique: true, sparse: true }, function () {
      // Pas de contrainte sur un utilisateur supprimé
      if (this.isDeleted()) return null
      // Un login prof est unique sur toutes les structures
      if (this.type === TYPE_FORMATEUR) return `${this.login}`
      // Mais pour un élève on a seulmement une contrainte par structure
      return null
    })
    // @todo gérer le cas du prof qui arrive avec un nouvel établissement sur lequel son login existe déjà pour un élève, le store va planter, il faudrait lui envoyer le bon message
    /**
     * Pour l'unicité des logins par établissement (unique & sparse)
     * N'existe pas pour les utilisateurs softDeleted (d'où le sparse)
     * @name structureUniqueLogin
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('structureUniqueLogin', 'string', { unique: true, sparse: true }, function () {
      // Pas de contrainte sur un utilisateur supprimé
      if (this.isDeleted()) return null
      // Un login doit être unique par structure, que ce soit un élève ou un prof
      return this.structures.map((structure) => `${structure}-${this.login}`)
    })
    /**
     * Nom toujours lowerCase
     * @name nom
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('nom', 'string', function () {
      return this.nom.toLowerCase().trim()
    })
    /**
     * Prénom toujours lowerCase
     * @name prenom
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('prenom', 'string', function () {
      return this.prenom.toLowerCase().trim()
    })
    /**
     * @name mail
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('mail', 'string')
    /**
     * Date de création de l'utilisateur (pas sa validation)
     * @name created
     * @type {Date}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('created', 'date')
    /**
     * Id fourni éventuellement par le sso (celui indiqué dans externalMech)
     * @name externalId
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('externalId', 'string')
    /**
     * Identifiant de l'éventuelle source externe gérant cet utilisateur
     * (celle de sa dernière connexion)
     * @name externalMech
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('externalMech', 'string')
    /**
     * Pour l'unicité des utilisateurs en provenance de l'extérieur (unique et
     * sparse pour ne pas indexer les softDeleted qui n'ont pas cet index,
     * ni ceux qui n'ont pas de source externe)
     * @name uniqueExternal
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('uniqueExternal', 'string', { unique: true, sparse: true }, function () {
      // Pas de contrainte sur un utilisateur supprimé
      if (this.isDeleted()) return null
      if (!this.externalMech) return null
      return `${this.externalMech}/${this.externalId}`
    })
    /**
     * Pour dédoublonner
     * @name password
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('password', 'string')
    /**
     * Le type d'utilisateur (cf constants.js)
     * @name type
     * @type {number}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('type', 'integer') // idem
    /**
     * @name classe
     * @type {string}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('classe', 'string')
    /**
     * Liste des oid des structures de l'utilisateur (même si l'utilisateur n'est pas valide)
     * @name structures
     * @type {string[]}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('structures', 'string', function () {
      if (Array.isArray(this.structures) && this.structures.length) return this.structures
    })
    /**
     * True si le compte est valide (validé par un pair, avec un mail validé s'il en a un et qu'il est prof)
     * @name isValid
     * @type {boolean|null}
     * @memberOf UtilisateurIndex
     */
    .defineIndex('isValid', function () {
      return $validators.isValid(this)
    })

  // On masque le password par défaut sur toutes les sérialisation json
  UtilisateurDefinition
    .defineMethod('toJSON', function () {
      return _.omit(this, ['password'])
    })

  // init $before au chargement ou après un store (qui renvoie une nouvelle entité)
  UtilisateurDefinition.onLoad(function () {
    /* en attendant que le pid soit généralisé partout (à activer quand en interne on ne modifiera plus external mais bien pid)
    if (this.externalMech && this.externalId && !this.pid) {
      this.pid = `${this.externalMech}/${this.externalId}`
    } */

    const $settings = lassi.service('$settings')
    const gestion = $settings.get('gestion')
    const isFormateur = this.type === TYPE_FORMATEUR
    this.$isAdminGestion = gestion && isFormateur && gestion.some(user => user.login === this.login && user.nom && user.nom.toLowerCase() === this.nom.toLowerCase())

    // @todo virer ces deux lignes après 2025-03-13, il est là pour supprimer la propriété structure (et structuresToBeValidated) qui pourrait rester en db (supprimée sur l'entity le 2023-08-02 pour structure et 2023-09-12 pour structuresToBeValidated)
    if (this.structure) delete this.structure
    if (this.structuresToBeValidated) delete this.structuresToBeValidated
  })

  /**
   * Efface ou met à jour les dépendances du user
   * élève : màj des groupes & séquences où il est, effacement de ses résultats et séances
   * formateur : suppression de ses groupes
   * Attention, ce hook n'est appliqué que sur des utilisateur.delete(), pas sur des query.purge()
   */
  UtilisateurDefinition.beforeDelete(function (cb) {
    const user = this
    Backup.create({ id: `Utilisateur-${user.oid}`, entity: user }).store(error => {
      if (error) return cb(error)
      if (!$utilisateur) $utilisateur = lassi.service('$utilisateur')
      if (user.type === TYPE_ELEVE) return $utilisateur.purgeEleve(user, cb)
      if (user.type === TYPE_FORMATEUR) return $utilisateur.purgeFormateur(user, cb)
      cb(Error(`Type d’utilisateur inconnu : ${user.type}`))
    })
  })

  /**
   * Vire la structure si y'a plus personne dedans
   */
  UtilisateurDefinition.afterDelete(function (cb) {
    const user = this
    if (!Structure) Structure = lassi.service('Structure')
    if (!$structure) $structure = lassi.service('$structure')
    // la suppression a eu lieu, on appelle pas cb avec une erreur si qqchose plante ici
    const journalData = { id: `Utilisateur-${this.oid}`, action: 'delete' }
    if (this.$deleteMessage) journalData.message = this.$deleteMessage
    flow().seq(function () {
      Journal.create(journalData).store(this)
    }).seq(function () {
      if (user.externalMech) {
        // on crée une 2e entrée de journal d'après cet id (car si on cherche sa suppression peu de chances qu'on connaisse son oid)
        journalData.id = `Utilisateur-${user.externalMech}/${user.externalId}`
        Journal.create(journalData).store(this)
      } else {
        this()
      }
    }).seq(function () {
      this(null, user.structures)
    }).seqEach(function (structureOid) {
      const next = this
      $structure.countUsers(structureOid, (error, nb) => {
        if (error) {
          console.error(error)
          return next()
        }
        if (nb === 0) {
          return Structure.match('oid').equals(structureOid).purge(next)
        }
        next()
      })
    }).seq(function () {
      cb()
    }).catch(function (error) {
      console.error(error)
      cb()
    })
  })

  /**
   * Vérifie que le login fourni à la création d'un user n'existe pas déjà
   *
   * Rappel des règles d'unicité pour les logins (cf utilisateur/controller.js) :
   *   - Un formateur est en doublon s'il y a un autre formateur qui a le même login.
   *   - Un utilisateur (élève ou formateur) est en doublon s'il existe un autre utilisateur
   *     de l'établissement possédant le même login.
   */
  UtilisateurDefinition.beforeStore(function (cb) {
    // On vérifie nos critères d'unicité, à la création comme à l'update (bouffe de l'I/O mais plus sécure)
    const utilisateur = this
    // on peut passer ces propriétés sur l'api (POST api/utilisateur) mais ça ne fait pas partie de l'entity (et ça planterait le store)
    if (utilisateur.structureCode) delete utilisateur.structureCode
    if (utilisateur.pid) delete utilisateur.pid

    if (utilisateur.isNew() && !utilisateur.created) {
      utilisateur.created = new Date()
    }

    // on garanti au moins des strings vides sur ces deux champs
    if (typeof utilisateur.nom !== 'string') utilisateur.nom = ''
    if (typeof utilisateur.prenom !== 'string') utilisateur.prenom = ''
    // mais pas vides toutes les deux
    if (!utilisateur.nom && !utilisateur.prenom) return cb(Error('Un utilisateur doit avoir au moins un nom ou un prénom'))
    // login toujours lowerCase sans espace aux 2 bouts
    if (!utilisateur.login || typeof utilisateur.login !== 'string') return cb(Error('Utilisateur sans login (ou login invalide)'))
    utilisateur.login = utilisateur.login.toLowerCase().trim()
    // on vérifie que l'on a toujours externalMech avec externalId (ou aucun)
    if (Boolean(this.externalId) !== Boolean(this.externalMech)) return cb(Error(`externalId et externalMech doivent être tous les deux renseignés (ou aucun), on a ${this.externalMech}/${this.externalId} pour ${this.oid}`))

    // On vérifiait en db qu'il n'y avait pas d'autre user avec le même mail, depuis que le champ est unique c'est plus la peine
    // (l'enregistrement en db va planter en disant qu'il y a un doublon)
    cb()
  })

  // faut regarder si y'a un changement de classe, pour éventuellement mettre à jour
  // l'ancienne et la nouvelle en afterStore (on a besoin de l'oid pour les nouveaux élèves)
  UtilisateurDefinition.trackAttribute('classe')

  // On doit maintenir la cohérence entre classe.utilisateurs[] et eleve.classe,
  // pour déterminer les élèves ayant accès aux séquence (quand on y assigne un groupe/classe)
  // (classe.utilisateurs est encore utilisé, cf les occurences de ".match('utilisateurs')")
  //
  // Idéalement il faudrait qu'une classe n'ait pas d'attribut 'utilisateurs'
  // (car l'élève a déjà un attribut classe)
  // et que seuls les groupes aient un attribut 'utilisateurs'
  // cf https://trello.com/c/MbQrYx24/168-virer-classeutilisateurs
  // Une fois réglé cette question on pourra supprimer ce afterStore
  UtilisateurDefinition.afterStore(function (cb) {
    const utilisateur = this
    if (utilisateur.type !== TYPE_ELEVE) return cb()
    if (!utilisateur.attributeHasChanged('classe')) return cb()

    // la classe a changée, faut mettre à jour l'ancienne et la nouvelle

    // utilisateur.$before n'existe pas en création d'utilisateur
    const oldClasseOid = utilisateur.attributeWas('classe')
    const newClasseOid = utilisateur.classe
    const classeOids = []
    if (oldClasseOid) classeOids.push(oldClasseOid)
    // @todo ajouter la contrainte de classe pour les élèves au validate
    // au 24/08/2018 on a 56 élèves sans classe en base
    if (newClasseOid) classeOids.push(newClasseOid)
    else appLog.error(`L’élève ${utilisateur.oid} n’a pas de classe (Utilisateur::afterStore)`) // inutile de générer une Error car la stackTrace ne remonte pas plus loin que lassi
    if (!classeOids.length) return cb()

    let newClasseFound = false
    flow().seq(function () {
      if (!Groupe) Groupe = lassi.service('Groupe')
      Groupe
        .match('isClass').equals(true)
        .match('oid').in(classeOids)
        .match('structure').in(utilisateur.structures)
        .grab(this)
    }).seqEach(function (classe) {
      if (classe.oid === newClasseOid) {
        newClasseFound = true
        if (classe.utilisateurs.includes(utilisateur.oid)) {
          this()
        } else {
          classe.utilisateurs.push(utilisateur.oid)
          classe.store(this)
        }
      } else {
        // ancienne classe
        if (classe.utilisateurs.includes(utilisateur.oid)) {
          classe.utilisateurs = _.remove(classe.utilisateurs, utilisateur.oid)
          classe.store(this)
        } else {
          this()
        }
      }
    }).seq(function () {
      if (newClasseFound) cb()
      else cb(createError(404, `La classe ${newClasseOid} n'existe pas`))
    }).catch(cb)
  })
})
