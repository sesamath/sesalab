/**
 * @module controllers/utilisateur
 */

const flow = require('an-flow')
const appLog = require('sesalab-api/source/tools/appLog')
const moment = require('moment-timezone')
const querystring = require('querystring')

const createError = require('../tools/createError')
const { CSV_SEPARATOR, TYPE_ELEVE, TYPE_FORMATEUR } = require('sesalab-commun/constants')
const { formatCsvCell } = require('sesalab-commun/formatString')
const isEmail = require('sesalab-commun/isEmail')
const checkMx = require('../tools/checkMx')

// on le require ici pour être sûr qu'il soit déclaré avant nous
require('../session/service')

const {
  getMailTextModified,
  getMailTextReinitializePassword,
  getMailTextRemoveAccount,
  getMailTextWelcome
} = require('./mailTemplates')

const TOKEN_LIFETIME = 24 // In hours
const NEWS_LIMIT = 10
const FOLDERS = ['ressourcesFolders', 'sequenceFolders', 'sequenceColleguesFolders', 'groupeFolders']
const CSV_HEADER = ['Classe', 'Nom', 'Prénom', 'Mail', 'Identifiant', 'Mot de passe']

// lui ne peut pas être réclamé en injection de dépendances (ça plante les tests)
let $sesalabSsoServer

/**
 * API liées à l'utilisateur.
 *
 * @controller Utilisateur
 */
app.controller(function (Groupe, Resultat, Sequence, Structure, Utilisateur, $crypto, $mail, $session,
  $sequence, $settings, $structure, $utilisateur, $utilisateurAcces, $validators) {
  const maxResults = $settings.get('application.maxResults', 500)
  // pbs de cycle, on ira le chercher au 1er usage
  let $sso

  // ***********************************************
  // Fonctions privées, helpers des contrôleurs
  // ***********************************************

  /**
   * Recherche d'utilisateur existant sur le même login/structure (corbeille incluse)
   * Règles d'unicité pour les logins :
   *   - Un formateur est en doublon s'il y a un autre formateur qui a le même login.
   *   - Un utilisateur (élève ou formateur) est en doublon s'il existe un autre utilisateur
   *     de l'établissement possédant le même login.
   *
   * @param {Object}   utilisateur L'utilisateur dont on souhaite vérifier l'unicité
   * @param {string}   structureOid L'identifiant interne de la structure
   * @param {Object}   context Context
   * @param {Function} callback Callback
   */
  function checkExistingUtilisateurWithLogin (utilisateur, structureOid, context, callback) {
    flow().seq(function () {
      if (typeof utilisateur.login !== 'string' || !utilisateur.login.trim()) return context.fieldError('login', 'L’identifiant ne doit pas être vide')
      Utilisateur
        .match('login').equals(utilisateur.login)
        .match('structures').equals(structureOid)
        .match('isValid').equals(true)
        .grabOne(this)
    }).seq(function (existingUtilisateurOnStructure) {
      if (existingUtilisateurOnStructure) return context.fieldError('login', 'Désolé, cet identifiant est déjà pris')
      // Pour un élève on s'arrête là
      if (utilisateur.type === TYPE_ELEVE) return callback()

      // Pour un prof on vérifie que le login est unique globalement
      Utilisateur
        .match('login').equals(utilisateur.login)
        .match('type').equals(TYPE_FORMATEUR)
        .grabOne(this)
    }).seq(function (existingFormateur) {
      if (existingFormateur) return context.fieldError('login', 'Désolé, cet identifiant est déjà pris')
      callback()
    }).catch(callback)
  }

  function checkStructure (structure, utilisateur, context, callback) {
    if (!structure) return context.fieldError('structure', 'Structure inconnue')
    const currentFormateur = $session.getCurrentFormateur(context)

    // Si ajoutElevesAuthorises est false sur la structure demandée, seul un formateur de cette structyure peut y créer des élèves
    if (utilisateur.type === TYPE_ELEVE) {
      if (!structure.ajoutElevesAuthorises && !currentFormateur) {
        return context.fieldError('structure', 'Les élèves ne peuvent s’ajouter eux-même à cette structure')
      }
      if (!structure.ajoutElevesAuthorises && !context.isFormateurInStructure(structure.oid)) {
        return context.accessDenied()
      }
    } else if (utilisateur.type === TYPE_FORMATEUR && structure.ajoutFormateursInterdit) {
      return context.fieldError('structure', 'La structure interdit la création de compte formateur')
    }

    utilisateur.structures = [structure.oid]
    callback()
  }

  /**
   * Vérifie que le password satisfait les contraintes (appelle context.fieldError en cas de pb et callback sinon)
   * Si OK :
   * - remplace utilisateur.password par son hash
   * - delete utilisateur.passwordBis
   * @param {object} utilisateur
   * @param {object} options
   * @param {boolean} [options.mustResetPassword=false] Passer true pour imposer mustResetPassword (et ne pas vérifier les contraintes de pass)
   * @param {boolean} [options.confirmPassword=false] passer true pour vérifier que passwordBis === password
   * @param {Context} context
   * @param {function} callback
   */
  function checkPasswordFields (utilisateur, options, context, callback) {
    if (options.mustResetPassword) {
      // c'est un mot de passe temporaire qui devra être changé à la première connexion,
      // on vérifie pas les contraintes
      utilisateur.mustResetPassword = true
    } else {
      // on vérifie les contraintes sur le mot de passe
      const errorText = $utilisateur.getPasswordError(utilisateur.password, utilisateur.login)
      if (errorText) return context.fieldError('password', errorText)
    }
    if (options.confirmPassword) {
      if (!utilisateur.passwordBis) return context.fieldError('passwordBis', 'Veuillez répéter le mot de passe')
      if (utilisateur.passwordBis !== utilisateur.password) return context.fieldError('passwordBis', 'Vos mots de passe ne concordent pas')
      delete utilisateur.passwordBis
    }
    utilisateur.password = $crypto.hash(utilisateur.password)
    callback()
  }

  /**
   * Valide les champs utilisateur (nom, prenom, mail). Appelle context.fieldError en cas de pb ou callback sans argument
   * (pour le mail on valide un mail plausible et vérifie le MX du domaine)
   * @param {object} utilisateur
   * @param {string} utilisateur.nom
   * @param {string} utilisateur.prenom
   * @param {string} [utilisateur.mail]
   * @param {number} utilisateur.type
   * @param {Context} context
   * @param {function} callback
   */
  function checkUserFields (utilisateur, context, callback) {
    const { nom, prenom, mail, type, externalMech } = utilisateur
    // on veut des string
    if (nom && typeof nom !== 'string') return context.fieldError('nom', 'Contenu invalide')
    if (prenom && typeof prenom !== 'string') return context.fieldError('prenom', 'Contenu invalide')
    if (mail && typeof mail !== 'string') return context.fieldError('mail', 'Contenu invalide')
    if (!nom && !prenom) return context.fieldError('nom', 'Nom et prénom ne peuvent être tous les deux vides')
    // pour les formateurs créés localement, le mail est obligatoire (ceux venant d'un ENT en SSO on un mail facultatif)
    if (type === TYPE_FORMATEUR && !externalMech && !mail) return context.fieldError('mail', 'L’adresse courriel doit être renseignée !')
    if (!mail) return callback()
    // si renseigné, on valide le mail
    if (!isEmail.validate(mail)) return context.fieldError('mail', 'Adresse mail invalide')
    // on vérifie aussi le MX
    checkMx(mail)
      .then(callback)
      .catch((error) => context.fieldError('mail', error.message))
  }

  /**
   * Vérifie tous les champs et crée l'utilisateur en base si ok
   * @private
   * @param {object} options
   * @param {boolean} options.confirmPassword si true vérifie aussi passwordBis
   * @param {boolean} [options.withAutoValidation=false] passer true pour ne pas demander la validation par mail (et valider le compte d'office)
   * @param {boolean} options.
   * @param {boolean} options.
   * @param {Context} context (données de l'utilisateur dans context.post)
   */
  function createUtilisateur (options, context) {
    let message = options.message
    let nouvelUtilisateur
    let structure
    // on récupère le post avec un trim sur toutes les strings
    const utilisateurData = {}
    Object.entries(context.post).forEach(([prop, val]) => {
      utilisateurData[prop] = typeof val === 'string' ? val.trim() : val
    })

    flow().seq(function () {
      checkUserFields(utilisateurData, context, this)
    }).seq(function () {
      getStructureFromUserData(utilisateurData, context, this)
    }).seq(function (_structure) {
      structure = _structure
      checkStructure(structure, utilisateurData, context, this)
    }).seq(function () {
      checkExistingUtilisateurWithLogin(utilisateurData, structure.oid, context, this)
    }).seq(function () {
      checkPasswordFields(utilisateurData, options, context, this)
    }).seq(function () {
      nouvelUtilisateur = Utilisateur.create(utilisateurData)
      // cette étape est donc obligatoire pour un compte formateur et optionnelle pour un compte élève
      if (options.withAutoValidation) {
        if (nouvelUtilisateur.type === TYPE_FORMATEUR) throw Error('Impossible de court-circuiter la validation d’un compte formateur')
        const formateur = $session.getCurrentFormateur(context)
        if (!formateur) throw Error('Seul un formateur authentifié peut créer des comptes élève validés d’office')
        $validators.setValid(nouvelUtilisateur, formateur.oid)
        return nouvelUtilisateur.store(this)
      }

      // Si l'utilisateur renseigne son mail, alors il va devoir le valider,
      if (nouvelUtilisateur.mail) $validators.initEmailValidator(nouvelUtilisateur)
      // sinon faut quand même un validator (pour pas que le compte ne soit valide tout de suite)
      // ici c'est un élève qui s'auto-crée un compte sans préciser de mail
      else $validators.initAccountValidator(nouvelUtilisateur)

      // pour les élèves on s'arrête là
      if (nouvelUtilisateur.type === TYPE_ELEVE) return nouvelUtilisateur.store(this)

      // Conditions générales d'utilisation
      if (context.post.cguAccepted) {
        nouvelUtilisateur.cguAccepted = true
      }

      // Les comptes formateurs sont "pré-validés" si la structure ne possède aucun compte formateur avec un mail validé
      flow().seq(function () {
        $utilisateur.grabTeacherByStructure(structure.oid, this)
      }).seq(function (formateurs) {
        message = 'Votre compte sera actif lorsque vous aurez cliqué sur le lien qui vient de vous être envoyé par courrier électronique'
        if (formateurs.length) {
          message += ', et qu’un des formateurs actuels de l’établissement aura validé l’activation de votre compte'
        } else {
          // on s'auto-valide le compte maintenant, mais pas le mail qui devra l'être pour que le compte
          // soit vraiment valide, c'est pour éviter d'être tributaire d'un autre qui demanderait une
          // validation de compte et validerait son mail avant nous.
          $validators.initAccountValidator(nouvelUtilisateur)
          $validators.validateAccount(nouvelUtilisateur, 'STRUCTURE_CREATOR')
        }
        nouvelUtilisateur.store(this)
        // un formateur qui crée des élèves ne doit pas activer le throttle pour d'autres élèves qui voudraient s'inscrire
        if (!$session.isAuthenticatedAsFormateur(context)) context.setThrottle(60, 'Vous ne pouvez pas créer plus d’un compte par minute')
      }).done(this)
    }).seq(function (_utilisateur) {
      nouvelUtilisateur = _utilisateur
      if (nouvelUtilisateur.type === TYPE_ELEVE) return this()

      // Annule la suppression de la structure en cours si celle-ci existe
      if (structure.removeDate) {
        delete structure.removeDate
        structure.store(this)
      } else {
        this()
      }
    }).seq(function () {
      if (options.withAutoValidation) return this()

      const url = $settings.get('application.baseUrl') + $settings.get('application.validateMailPath')
      const appName = $settings.get('application.name')
      const link = `${url}/${nouvelUtilisateur.oid}/${nouvelUtilisateur.validators.mail && nouvelUtilisateur.validators.mail.token}`
      const { login, mail, nom, prenom } = nouvelUtilisateur
      const mailData = {
        subject: `Bienvenue sur ${appName}`,
        text: getMailTextWelcome({ appName, link, login, nom, prenom }),
        to: mail
      }
      if (nouvelUtilisateur.type === TYPE_FORMATEUR) {
        // prof
        message = `Un mail a été envoyé à ${mail} pour valider cette adresse.`
        $mail.sendMail(mailData, this)
      } else if (mail) {
        // élève avec mail
        message = `Un mail a été envoyé à ${mail} pour valider cette adresse. Le compte sera ensuite actif dès qu’un enseignant l’aura validé, tu recevras aussitôt un mail pour t’en informer.`
        $mail.sendMail(mailData, this)
      } else {
        // élève sans mail
        message = 'Votre compte sera utilisable dès qu’un formateur l’aura validé.'
        $utilisateur.notifyAboutPendingAccount(structure.oid, nouvelUtilisateur, this)
      }
    }).seq(function () {
      context.rest({
        message,
        utilisateur: nouvelUtilisateur
      })
    }).catch(context.next)
  }

  function getCsvRowEleve (eleve, classesByOid) {
    const classe = classesByOid[eleve.classe]
    return [
      classe ? classe.nom : 'Sans classe',
      eleve.nom,
      eleve.prenom,
      '',
      eleve.login,
      ''
    ].map(formatCsvCell).join(CSV_SEPARATOR)
  }

  function getStructureFromUserData (utilisateur, context, callback) {
    const returnError = (msg) => context.fieldError('structure', msg)
    const getLoadCb = msg => (error, structure) => {
      if (error) return callback(error)
      if (structure) return callback(null, structure)
      // sinon ça remonte rien et on répond avec le message d'erreur sans rappeler la cb
      returnError(msg)
    }
    if (utilisateur.structureOid || utilisateur.structures) {
      let oid
      if (utilisateur.structureOid) {
        oid = utilisateur.structureOid
        delete utilisateur.structureOid
      } else {
        oid = utilisateur.structures[0]
      }
      Structure.match('oid').equals(oid).grabOne(getLoadCb(`La structure d’id ${oid} n’existe pas`))
    } else if (utilisateur.structureCode) {
      Structure.match('code').equals(utilisateur.structureCode).grabOne(getLoadCb(`Aucune structure ayant le code ${utilisateur.structureCode}`))
    } else {
      // Sinon on n'appelle pas le callback et renvoie une erreur
      returnError('Vous devez renseigner la structure')
    }
  }

  /**
   * Indique si les changements apportés à un utilisateur nécessitent qu'il saisisse son mot de passe.
   *
   * @param {object} context Instance de context
   * @param {Utilisateur} newUser Nouvel utilisateur
   * @param {Utilisateur} oldUser Ancien utilisateur
   * @return "true" si le mot de passe de l'utilisateur est requis
   */
  function needCurrentPasswordForUpdate (context, newUser, oldUser) {
    // Un élève en sso n'a pas de mot de passe
    if (oldUser.externalMech) return false
    //
    if (oldUser.mustResetPassword) return false
    // Un élève doit valider son mot de passe
    if ($session.isAuthenticatedAsEleve(context)) return true

    // Un formateur doit fournir son password pour mettre à jour certains champs
    if ($session.isAuthenticatedAsFormateur(context) && context.post.type === TYPE_FORMATEUR) {
      if (context.post.password) return true
      return ['nom', 'prenom', 'mail'].some(field => newUser[field] !== oldUser[field])
    }

    // Pour un formateur qui modifie un élève, et toutes les autres mises à jour
    // pas besoin de vérification
    return false
  }

  /**
   * Met à jour un utilisateur
   *
   * @private
   * @param {object} options
   * @param {boolean} options.mustResetPassword
   * @param {boolean} options.confirmPassword
   * @param {Context} context
   */
  function updateUtilisateur (options, context) {
    const oid = context.arguments.oid
    const newUser = context.post
    const currentFormateur = $session.getCurrentFormateur(context)
    const currentEleves = $session.getCurrentEleves(context)
    let oldUser

    // On supprime les champs que l'utilisateur ne peut pas changer directement
    delete newUser.validators

    flow().seq(function () {
      context.forceAuthentifie(this)
    }).seq(function () {
      // on retourne le chercher en db c'est plus sûr (au cas où ça aurait changé depuis la mise en session)
      Utilisateur.match('oid').equals(oid).grabOne(this)
    }).seq(function (_oldUser) {
      const next = this
      if (!_oldUser) return next(createError(404, `L'utilisateur avec l'id ${oid} n'existe pas`))
      oldUser = _oldUser
      // Ignore la vérication du mot de passe pour un utilisateur connecté en SSO
      if (_oldUser.externalMech) options.confirmPassword = false
      if (newUser.login === oldUser.login) return next()
      // on vérifie les collisions sur le login
      flow().seq(function () {
        getStructureFromUserData(newUser, context, this)
      }).seq(function (structure) {
        checkExistingUtilisateurWithLogin(newUser, structure.oid, context, this)
      }).done(next)
    }).seq(function () {
      // ici il faut ajouter externalMech pour que ça ne plante pas sur un mail absent
      // d'un prof qui arriverait du SSO (après validation des CGU et ajout du profil il passe par ici)
      if (oldUser.externalMech) newUser.externalMech = oldUser.externalMech
      checkUserFields(newUser, context, this)
    }).seq(function () {
      if (!newUser.password) {
        delete newUser.password
        delete newUser.passwordBis
        return this()
      }
      // y'a un pass, on vérifie les contraintes et on le chiffre
      checkPasswordFields(newUser, options, context, this)
    }).seq(function () {
      // On vérifie que l'utilisateur connecté a bien le droit de modifier l'élève :
      // si c'est l'élève lui-même, ou un formateur de la même structure.
      const currentEleves = $session.getCurrentEleves(context)
      const currentFormateur = $session.getCurrentFormateur(context)
      let isAllowed = false
      if (newUser.type === TYPE_ELEVE) {
        // Un élève ne peut éditer que son propre compte (accès refusé en groupe)
        if (currentEleves && currentEleves.length === 1 && currentEleves[0].oid === oldUser.oid) isAllowed = true
        // Un formateur peut éditer tous les élèves des structures auxquelles il appartient
        if (currentFormateur && currentFormateur.structures.some(structureOid => oldUser.structures.includes(structureOid))) isAllowed = true
      } else {
        // Un formateur ne peut modifier que son propre compte
        if (currentFormateur && currentFormateur.oid === oid) isAllowed = true
      }
      if (!isAllowed) return context.accessDenied()

      // On vérifie que le mot de passe de verification est bien celui lié à ce compte
      if (needCurrentPasswordForUpdate(context, oldUser, newUser)) {
        if (!newUser.passwordCurrent) {
          return context.fieldError('passwordCurrent', 'Vous devez renseigner votre mot de passe actuel')
        }
        if (oldUser.password !== $crypto.hash(newUser.passwordCurrent)) {
          return context.fieldError('passwordCurrent', 'Le mot de passe ne correspond pas au mot de passe actuel')
        }
      }

      // On vérifie d'abord si les mails ont changé
      if (newUser.mail === oldUser.mail) {
        delete newUser.newMail
        delete oldUser.newMail
        return this()
      }
      // ça a changé, faut envoyer un mail de confirmation ?
      if (!newUser.mail) {
        if (newUser.type === TYPE_FORMATEUR) return context.fieldError('mail', 'L’adresse mail est obligatoire')
        // Un élève peut mettre à jour son profil en supprimant son ancienne adresse mail
        // Dans ce cas, aucun mail de confirmation ne doit être envoyé
        delete oldUser.mail
        return this()
      }
      // on garde les deux en attendant une validation
      newUser.newMail = newUser.mail
      newUser.mail = oldUser.mail

      $validators.initNewEmailValidator(oldUser)

      const baseUrl = $settings.get('application.baseUrl') + $settings.get('application.validateNewMailPath')
      const link = `${baseUrl}/${oldUser.oid}/${oldUser.validators.newMail.token}`
      const appName = $settings.get('application.name')

      $mail.sendMail({
        subject: 'Modification email',
        text: getMailTextModified({ appName, utilisateur: newUser, link }),
        to: newUser.newMail
      }, this)
      // fin mails
    }).seq(function () {
      // Si un formateur change le mot de passe d'un élève on force le reset de son pot de passe
      newUser.mustResetPassword = !!options.mustResetPassword
      Object.assign(oldUser, newUser)

      delete oldUser.passwordBis
      delete oldUser.passwordCurrent
      delete oldUser.utilisateurs
      delete oldUser.supprimerCompte

      // on peut sauvegarder en base
      oldUser.store(this)
    }).seq(function (utilisateur) {
      // On met à jour le formateur en session si présent
      if (currentFormateur && currentFormateur.oid === utilisateur.oid) {
        $session.update(context, [utilisateur])
      } else if (currentEleves && currentEleves.some(e => e.oid === utilisateur.oid)) {
        // On met à jour l'élève en session si présent
        const newEleves = currentEleves.map((eleve) => {
          if (eleve.oid === utilisateur.oid) return utilisateur
          return eleve
        })
        $session.update(context, newEleves)
      }
      context.rest({ utilisateur })
    }).catch(context.next)
  }

  // ********************************************
  // Les controleurs
  // ********************************************

  /**
   * Retourne un contrôleur pour les 2 routes POST /api/(formateur|eleve)/:oid, suivant le type
   * @param type
   */
  const createUtilisateurHandler = (type) => (context) => {
    flow().seq(function () {
      // Si formateur connecté, on ne fixe pas de limite à la création
      if ($session.isAuthenticatedAsFormateur(context)) this()
      else context.checkThrottle(this)
    }).seq(function () {
      if (context.post.type !== type) return context.accessDenied()
      if (context.post.oid) return context.accessDenied() // pas d'oid pour une création

      // Quand un formateur crée un élève, pas de confirmation de nouveau compte, il est validé d'office
      const skipConfirmation = type === TYPE_ELEVE && $session.isAuthenticatedAsFormateur(context)
      const data = {
        confirmPassword: !skipConfirmation,
        withAutoValidation: skipConfirmation
      }
      createUtilisateur(data, context)
    }).catch(context.next)
  }

  /**
   * Retourne un contrôleur pour les 2 routes PUT /api/(formateur|eleve)/:oid, suivant le type
   * @param {number} type
   */
  const updateUtilisateurHandler = (type) => (context) => {
    // On match ça que l'on ne veut pas traiter ici, on passe au suivant pour que son contrôleur le gère
    if (context.arguments.oid === 'restore') return context.next()
    if (!$session.isAuthenticated(context)) return context.accessDenied()
    if (context.post.type !== type) return context.accessDenied()

    // Quand un formateur édite un élève, pas de confirmation de password
    const isEleveUpdatedByFormateur = (type === TYPE_ELEVE && $session.isAuthenticatedAsFormateur(context))
    const updateData = {
      mustResetPassword: isEleveUpdatedByFormateur,
      confirmPassword: !isEleveUpdatedByFormateur
    }
    updateUtilisateur(updateData, context)
  }

  /**
   * Création d'un compte élève.
   *
   * @name EleveCreation
   * @route {POST} api/eleve
   */
  this.post('api/eleve', createUtilisateurHandler(TYPE_ELEVE))

  /**
   * Mise à jour du compte d'un élève.
   *
   * @name EleveUpdate
   * @route {PUT} api/eleve:oid
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string} oid Identifiant de l'élève
   */
  this.put('api/eleve/:oid', updateUtilisateurHandler(TYPE_ELEVE))

  /**
   * Restaure le compte de plusieurs élèves.
   *
   * @name ElevesRestore
   * @route {PUT} api/eleve/:oids/restore
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string[]} oid Tableau d'identifiant d'élèves
   */
  this.put('api/eleve/:oids/restore', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const elevesOids = context.arguments.oids.split(',')

    flow()
      .seq(function () {
        Utilisateur
          .match('oid').in(elevesOids)
          .match('type').equals(TYPE_ELEVE)
          .onlyDeleted()
          .grab(this)
      })
      .seqEach(function (eleve) {
        if (!context.isFormateurInOneOfStructures(eleve.structures)) return this()
        if (!eleve.classe) return this(null, eleve)

        // Supprime la référence eleves <-> classe
        flow()
          .seq(function () {
            Groupe
              .match('oid').equals(eleve.classe)
              .match('isClass').true()
              .includeDeleted()
              .grabOne(this)
          })
          .seq(function (classe) {
            if (!classe.isDeleted()) return this(null, eleve)
            eleve.classe = undefined

            classe.utilisateurs = classe.utilisateurs.filter(oid => oid !== eleve.oid)
            classe.store(this)
          })
          .seq(function () {
            this(null, eleve)
          })
          .done(this)
      })
      .seqEach(function (eleve) {
        if (!eleve) return this()
        eleve.markToRestore().store(this)
      })
      .seq(function (eleves) {
        context.rest({ eleves: eleves.map(e => e) })
      })
      .catch(context.next)
  })

  /**
   * Restreint la session à un élève unique
   * (pour accéder à une séquence mono utilisateur)
   *
   * @name StudentReLogin
   * @route {POST} api/eleve/relogin
   * @queryparam {string} eleve Identifiant de l'utilisateur à re-connecter
   */
  this.post('api/eleve/relogin', function (context) {
    const oidToLogin = context.post.eleve
    const currentEleves = $session.getCurrentEleves(context)
    if (!currentEleves) return context.accessDenied()
    const arrayWithUniqEleveToLogin = currentEleves.filter(e => e.oid === oidToLogin)
    if (!arrayWithUniqEleveToLogin.length) return context.accessDenied(`Pas d’élève ${oidToLogin} en session`)
    $session.update(context, arrayWithUniqEleveToLogin)
    context.rest({})
  })

  /**
   * Retourne la liste des élèves d'une structure au format CSV.
   *
   * @name UtilisateursExport
   * @route {GET} api/eleves/by-structure/export/:structureOid
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string} structureOid Identifiant de la {@link Structure}
   */
  this.get('api/eleves/by-structure/export/:structureOid', function (context) {
    const structureOid = context.arguments.structureOid
    if (!structureOid) return context.next(createError(400, 'Structure invalide (undefined)'))
    if (!context.isFormateurInStructure(structureOid)) return context.accessDenied()

    const csvData = [CSV_HEADER.join(';')]
    let classesByOid

    flow()
      .seq(function () {
        Groupe
          .match('structure').equals(structureOid)
          .match('isClass').true()
          .grab(this)
      })
      .seq(function (classes) {
        classesByOid = {}
        for (const classe of classes) classesByOid[classe.oid] = classe
        $structure.getAllSpecificUsers(structureOid, { type: TYPE_ELEVE }, this)
      })
      .seq(function (eleves) {
        for (const eleve of eleves) {
          const csvRow = getCsvRowEleve(eleve, classesByOid)
          csvData.push(csvRow)
        }

        context.contentType = 'application/csv'
        context.raw(csvData.join('\n'), {
          headers: {
            'Content-Disposition': 'attachment; filename="export_comptes_eleves_utf8.csv"'
          }
        })
      })
      .catch(context.next)
  })

  /**
   * Mise à jour du compte d'un formateur.
   *
   * @name FormateurUpdate
   * @route {PUT} api/formateur:oid
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string} oid Identifiant du formateur
   */
  this.put('api/formateur/:oid', updateUtilisateurHandler(TYPE_FORMATEUR))

  /**
   * Suppression d'un compte formateur (par lui-même)
   *
   * @route {DELETE} api/formateur
   * @authentication Cette route nécessite un formateur connecté
   * @param {Context} context Le context
   */
  this.delete('api/formateur', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()

    const token = $crypto.token()
    let utilisateur
    flow()
      .seq(function () {
        Utilisateur
          .match('oid').equals(currentFormateur.oid)
          .match('type').equals(TYPE_FORMATEUR)
          .grabOne(this)
      })
      .seq(function (_utilisateur) {
        utilisateur = _utilisateur

        const appName = $settings.get('application.name')
        const link = $settings.get('application.baseUrl') + $settings.get('application.removeAccountPath') + '/' + utilisateur.oid + '/' + token
        $mail.sendMail({
          subject: 'Suppression du compte',
          text: getMailTextRemoveAccount({ appName, utilisateur, link }),
          to: utilisateur.mail
        }, this)
      })
      .seq(function () {
        utilisateur.deleteAccountToken = {
          token,
          date: Date.now()
        }

        utilisateur.store(this)
      })
      .seq(function () {
        $session.update(context, utilisateur)
        context.rest({ utilisateur })
      })
      .catch(context.next)
  })

  /**
   * Création d'un compte formateur.
   *
   * @name FormateurCreation
   * @route {POST} api/formateur
   */
  this.post('api/formateur', createUtilisateurHandler(TYPE_FORMATEUR))

  /**
   * Met à jour la structure des arbres d'un formateur.
   *
   * @name Folders
   * @route {PUT} api/formateur/:oid/folders
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string} oid Identifiant du compte {@link Utilisateur}
   * @bodyparam {Object.<string, Object>} Liste des arbres à mettre à jour ainsi que leurs données
   */
  this.put('api/formateur/:oid/folders', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    const currentOid = currentFormateur.oid
    if (currentOid !== context.arguments.oid) return context.accessDenied()
    const postedKeys = Object.keys(context.post)
    const foldersKeysToUpdate = FOLDERS.filter(folder => postedKeys.includes(folder))

    flow()
      .seq(function () {
        Utilisateur
          .match('oid').equals(currentOid)
          .match('type').equals(TYPE_FORMATEUR)
          .grabOne(this)
      })
      .seq(function (utilisateur) {
        for (const folderKey of foldersKeysToUpdate) {
          utilisateur[folderKey] = context.post[folderKey]
        }

        utilisateur.store(this)
      })
      .seq(function (utilisateur) {
        $session.update(context, utilisateur)
        context.rest({ utilisateur })
      })
      .catch(context.next)
  })

  /**
   * Connecte un utilisateur à son compte.
   *
   * @name Login
   * @route {POST} api/login
   * @queryparam {string} login L'identifiant de l'utilisateur
   * @queryparam {string} password Son mot de passe
   */
  this.post('api/login', function (context) {
    /**
     * Variante de context.fieldError avec l'index de l'élève concerné en plus
     *
     * @param {string}  field   Champ du formulaire
     * @param {number} index   Index du champ
     * @param {string}  message Message d'erreur
     */
    function fieldError (field, index, message) {
      const data = {}
      data.field = field
      if (typeof index !== 'undefined') data.index = index
      data.message = message
      context.restKo(data)
    }

    /**
     * Valide le login/pass d'un utilisateur, le récupère en db,
     * vérifie qu'il est valide et le passe à callback
     *
     * @param {Object}   user     L'utilisateur
     * @param {string}   user.login
     * @param {string}   user.password
     * @param {number}  index    L'index de l'utilisateur
     * @param {Function} callback Callback
     */
    function checkUserData ({ login, password }, index, callback) {
      flow()
        .seq(function () {
          if (!login?.length) {
            return fieldError('login', index, 'L’identifiant doit être renseigné')
          }
          if (!password?.length) {
            return fieldError('password', index, 'Le mot de passe ne doit pas être vide')
          }

          // Pour permettre à un admin d'usurper une session,
          // il est possible d'utiliser le super password
          const query = Utilisateur.match('login').equals(login.toLowerCase().trim())
          if (structure?.oid) query.match('structures').equals(structure.oid).match('isValid').equals(true)
          const passHashed = $crypto.hash(password)
          if (superPassword && passHashed === superPassword) {
            isSuperUser = true
          } else {
            query.match('password').equals(passHashed)
          }
          query.grab(this)
        })
        .seq(function (users) {
          if (users.length === 0) {
            return fieldError('login', index, 'L’authentification a échoué (identifiant inconnu ou mot de passe incorrect)')
          }

          if (users.length > 1) {
            if (structureCode) {
              return fieldError('login', index, 'Plusieurs utilisateurs avec le même identifiant existent dans cette structure')
            }
            return fieldError('structure', undefined, 'Veuillez préciser le code ou le nom de votre structure')
          }

          // ok, un seul user avec ce login/pass
          const dbUser = users[0]
          if (!$validators.isValid(dbUser)) {
            // quand on gèrera accepted sur le mail
            // if (dbUser.validators.mail && !dbUser.validators.mail.accepted) {
            //  return fieldError('login', index, `Votre courriel n'est pas encore activé (ou a été désactivé à cause d’un trop grand nombre d’erreurs)`)
            if (dbUser.mail && !$validators.hasValidEmail(dbUser)) {
              return fieldError('login', index, 'Votre courriel n’est pas encore activé')
            }
            if (!$validators.isAccountAccepted(dbUser)) {
              return fieldError('login', index, 'Votre compte n’est pas encore activé par un formateur de la structure')
            }
            return fieldError('login', index, 'Votre compte n’a pas été validé')
          }
          callback(null, dbUser)
        })
        .catch(callback)
    } // checkUserData

    let isSuperUser = false
    const usersPosted = context.post.users
    const structureCode = context.post.structure
    const superPassword = $settings.get('application.superPassword')
    // des variables affectées dans un seq et utilisées dans un autre
    let structure, users, timezone, isFormateur, allowedStructures

    flow()
      .seq(function () {
        // on vérifie qu'on a pas déjà une session en cours (normalement il devrait pas
        // pouvoir avoir la home dans un autre onglet du navigateur, sauf s'il en avait
        // déjà deux avant le 1er login, on bloque ici pour ne pas planter une séquence
        // en cours dans un autre onglet)
        if ($session.isAuthenticated(context)) {
          return fieldError('login', 0, 'Vous avez déjà une session ouverte, il faut la fermer avant de pouvoir vous reconnecter (vous devriez rafraîchir cette page)')
        }
        // Récupération de la structure si le code est donné dans la requête
        if (structureCode && structureCode.length) return Structure.match('code').equals(structureCode).grabOne(this)
        this()
      })
      .seq(function (_structure) {
        if (_structure) {
          // c'est la structure précisée au login
          structure = _structure
          if (structure.onlyFromEntId && !isSuperUser) {
            const label = $structure.getLabel(structure)
            return fieldError('login', 0, `Pour se connecter à la structure ${label} il faut passer par son ENT`)
          }
        }
        this(null, usersPosted)
      })
      .seqEach(function (user, index) {
        checkUserData(user, index, this)
      })
      .seq(function (_users) {
        users = _users
        if (users.length > 1 && users.some(u => u.type !== TYPE_ELEVE)) {
          return fieldError('login', 0, 'Connexion multiple réservée aux élèves')
        }

        if (structure) {
          // connexion élève où ils ont dû préciser la structure, elle est pas onlyFromEntId (sinon ça aurait planté juste au-dessus)
          allowedStructures = [structure.oid]
          return this(null, structure)
        }

        // on regarde les structures déclarés pour ces users
        let structureIds
        for (const user of users) {
          if (!structureIds) {
            structureIds = user.structures
          } else {
            // pas le 1er, on restreint aux structures de ce user
            structureIds = structureIds.filter(s => user.structures.includes(s))
          }
        }
        if (!structureIds.length) {
          // on vérifie quand même qu'ils étaient plusieurs, pour pas raconter n'importe quoi
          if (users.length > 1) return fieldError('login', 0, 'Aucune structure commune à ces comptes')
          return fieldError('login', 0, 'Aucune structure pour ce compte')
        }
        // faut aller les chercher pour vérifier qu'elles ne sont pas restreinte à un ENT
        $structure.grabByOid(structureIds, this)
      })
      .seq(function (structures) {
        // on vire les structures marquées comme accessible seulement de leur ENT
        const structuresOk = isSuperUser ? structures : structures.filter(s => !s.onlyFromEntId)
        if (!structuresOk.length) {
          if (structures.length) {
            const label = $structure.getLabel(structures[0])
            return fieldError('login', 0, `Pour se connecter à la structure ${label} il faut passer par son ENT`)
          }
          return fieldError('login', 0, 'Aucune structure valide pour se connecter')
        }
        allowedStructures = structuresOk.map(s => s.oid)
        structure = structuresOk[0]
        if (users.length > 1 && !structure.accesMultiple) {
          return fieldError('login', 0, `La connexion de plusieurs élèves sur la même session n'est pas
          autorisée pour la structure ${structure.nom}`)
        }
        timezone = $structure.getTimezone(structure)
        isFormateur = (users[0].type === TYPE_FORMATEUR)

        this(null, users)
      })
      .seqEach(function (user) {
        user.lastConnexionAsText = $utilisateur.formatDateWithPrivacy(new Date(), timezone, true)
        user.store(this) // On n'utilise pas $utilisateur.hashPasswordAndStoreUtilisateur pour ne pas re-hasher le password
      })
      .seq(function (users) {
        // on log en tâche de fond
        for (const user of users) {
          $utilisateurAcces.log(user, structure)
        }
        $session.login(context, users, structure.oid, allowedStructures)

        const appUrl = isFormateur ? '/sso/sesatheques' : '/eleve/'
        context.rest({
          appUrl,
          utilisateur: users
        })
      })
      .catch(context.next)
  })

  /**
   * Renvoie l'utilisateur de la session courante (avec contrôle de la cohérence
   * des données en session, en cas d'incohérence ça déconnecte et retourne un
   * message d'erreur)
   *
   * @name Utilisateur
   * @route {GET} api/utilisateur
   * @authentication Cette route nécessite un formateur connecté
   */
  this.get('api/utilisateur', function (context) {
    if (!$sesalabSsoServer) $sesalabSsoServer = lassi.service('$sesalabSsoServer')
    const currentFormateur = $session.getCurrentFormateur(context)
    const currentEleves = $session.getCurrentEleves(context)
    const currentStructureOid = $session.getCurrentStructureOid(context)

    // on retourne l'utilisateur en session (en filtrant ses structures avec allowedStructures)
    // (ce user doit avoir été mis à jour à chaque opération qui l'a modifié,
    // inutile de tout retourner chercher en bdd)
    const allowedStructures = $session.getAllowedStructures(context)
    if (currentFormateur || currentEleves) {
      // check structure
      if (!currentStructureOid || !allowedStructures.includes(currentStructureOid)) {
        const msg = currentStructureOid
          ? `session avec currentStructureOid ${currentStructureOid} qui ne fait pas partie des allowedStructures (${allowedStructures.join(', ')})`
          : 'Session avec un prof sans currentStructureOid'
        console.error(Error(msg))
        $session.flush(context)
        return context.restKo('Session incohérente, veuillez vous reconnecter')
      }
    }

    if (currentFormateur) {
      if (currentEleves) {
        console.error(Error('Session avec un prof et des élèves'))
        $session.flush(context)
        return context.restKo('Session incohérente, veuillez vous reconnecter')
      }
      const structures = currentFormateur.structures.filter(oid => allowedStructures.includes(oid))
      if (!structures.length) return context.restKo('aucune structure possible via cette connexion')
      const result = {
        currentStructureOid,
        utilisateur: { ...currentFormateur, structures }
      }
      // pour l'accès à ses ressources non publiques sur les sesathèques,
      // le client js aura besoin des authBundles
      const authBundles = $sesalabSsoServer.getAuthBundles(context)
      if (authBundles?.length) result.authBundles = authBundles

      return context.rest(result)
    }

    if (currentEleves) {
      const readonlyToken = $session.getReadOnlyToken(context)
      const eleves = []
      for (const eleve of currentEleves) {
        const structures = eleve.structures.filter(oid => allowedStructures.includes(oid))
        if (!structures.length) return context.restKo(`aucune structure possible via cette connexion pour ${eleve.login}`)
        eleves.push({ ...eleve, structures })
      }
      return context.rest({
        eleves,
        currentStructureOid,
        readonlyToken
      })
    }

    // probablement une session expirée, au cas où on flush
    $session.flush(context)
    // ça c'est le form d'inscription… faut pas lui répondre avec une session expirée et une redirection
    if ('check' in context.get) return context.rest({ session: false })
    return context.rest({
      success: false,
      message: 'Session expirée, veuillez vous reconnecter',
      redirectTo: '/'
    })
  })

  /**
   * Suppression d'un groupe d'utilisateurs.
   *
   * @name UtilisateursRemove
   * @route {DEL} api/utilisateur/:oids
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string[]} oid Tableau d'identifiant d'utilisateurs
   */
  this.delete('api/utilisateur/:oids', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const utilisateurs = context.arguments.oids.split(',')
    if (!utilisateurs.length) return context.restKo('Argument oids incorrect')

    flow()
      .seq(function () {
        Utilisateur.match('oid').in(utilisateurs).grab(this)
      })
      .seqEach(function (utilisateurToDelete) {
        if (!utilisateurToDelete) {
          return context.notFound()
        }

        if (utilisateurToDelete.type === TYPE_FORMATEUR) {
          // un formateur ne peut être supprimé que par lui-même
          const formateur = $session.getCurrentFormateur(context)
          if (!formateur || formateur.oid !== utilisateurToDelete.oid) return context.accessDenied()
        } else {
          // un élève ne peut être supprimé que par un formateur appartenant à sa structure
          if (!context.isFormateurInOneOfStructures(utilisateurToDelete.structures)) return context.accessDenied()
        }

        utilisateurToDelete.softDelete(this)
      })
      .seq(function (removedStudents) {
        context.rest({ removed: removedStudents.filter(s => s) })
      })
      .catch(context.next)
  })

  /**
   * Renvoie la liste des actualités d'un utilisateur.
   *
   * @name UtilisateurActualites
   * @route {GET} /api/utilisateur/actualites
   * @authentication Cette route nécessite un formateur connecté
   */
  this.get('/api/utilisateur/actualites', function (context) {
    const currentFormateur = $session.getCurrentFormateur(context)
    if (!currentFormateur) return context.accessDenied()
    if (!currentFormateur.structures.length) return context.restKo('Utilisateur affecté à aucune structure')
    const currentStructureOid = $session.getCurrentStructureOid(context)

    const response = {
      changedSequences: [],
      sequencesWithNewResultats: []
    }
    const dateMoinsDeuxSemaines = moment().subtract(2, 'weeks').toDate()
    // toutes les séquences du user
    const sequencesByOid = {}
    // les oids des séquences avec des résultats récents
    let freshResultatSequenceOids
    const nbFreshResultatBySequenceOid = {}
    flow()
      .seq(function () {
        // les séquences du user modifiées récemment
        Sequence
          .match('owner').equals(currentFormateur.oid)
          .match('structure').equals(currentStructureOid)
          .match('lastChangeTimestamp').isNotNull()
          .sort('lastChangeTimestamp', 'desc')
          .grab(NEWS_LIMIT, this)
      })
      .seq(function (userSequences) {
        response.changedSequences = userSequences
        if (userSequences.length === NEWS_LIMIT) return this(null, [])
        const nbToComleteList = NEWS_LIMIT - userSequences.length

        // celles de ses potes
        Sequence
          .match('structure').equals(currentFormateur.structures)
          .match('publicAmis').true()
          .match('owner').notEquals(currentFormateur.oid)
          .match('lastChangeTimestamp').isNotNull()
          .sort('lastChangeTimestamp', 'desc')
          .grab(nbToComleteList, this)
      })
      .seq(function (coworkersSequences) {
        // on a que des séquences avec lastChange.date non null
        response.changedSequences = response.changedSequences
          .concat(coworkersSequences)
          .sort((seq1, seq2) => seq2.lastChange.date.valueOf() - seq1.lastChange.date.valueOf())
          .map(({ oid, nom, lastChange }) => ({ oid, nom, lastChange }))

        // on passe aux résultats, faut récupérer toutes les séquences du user
        $sequence.getSequencesOf(currentFormateur.oid, { structureOid: currentStructureOid }, this)
      })
      .seq(function (sequences) {
        for (const sequence of sequences) sequencesByOid[sequence.oid] = sequence
        const seqOids = Object.keys(sequencesByOid)

        // si y'a pas de séquences, pas la peine d'aller plus loin
        if (!seqOids.length) return context.rest(response)

        Resultat
          .match('sequence').in(seqOids)
          .match('date').after(dateMoinsDeuxSemaines)
          .countBy('sequence', this)
      })
      .seq(function (_nbFreshResultatBySequenceOid) {
        for (const [oid, nb] of Object.entries(_nbFreshResultatBySequenceOid)) {
          if (nb > 0) nbFreshResultatBySequenceOid[oid] = nb
        }
        freshResultatSequenceOids = Object.keys(nbFreshResultatBySequenceOid)
        // si y'a pas de nouveaux résultats on arrête là
        if (!freshResultatSequenceOids.length) return context.rest(response)
        // on veut le nb total de résultats pour ces séquences (qui en ont des frais)
        Resultat
          .match('sequence').in(freshResultatSequenceOids)
          .countBy('sequence', this)
      })
      .seq(function (nbResultatsBySequenceOid) {
        for (const [oid, nombreResultats] of Object.entries(nbResultatsBySequenceOid)) {
          const nombreNouveauxResultats = nbFreshResultatBySequenceOid[oid]
          response.sequencesWithNewResultats.push({
            oid,
            nom: sequencesByOid[oid].nom,
            nombreResultats,
            nombreNouveauxResultats
          })
        }
        // on veut une liste cohérente d'une vue à l'autre, on trie par nb de résultat frais
        response.sequencesWithNewResultats = response.sequencesWithNewResultats.sort((a, b) => b.nombreNouveauxResultats - a.nombreNouveauxResultats)

        context.rest(response)
      })
      .catch(context.next)
  })

  /**
   * Récupère l'ensemble des utilisateurs d'une structure.
   *
   * @name UtilisateursByStructure
   * @route {GET} api/utilisateur/by-structure/:structureOid
   * @authentication Cette route nécessite un formateur connecté
   * @queryparam {string} structureOid Identifiant de la {@link Structure}
   */
  this.get('api/utilisateur/by-structure/:structureOid', function (context) {
    const structureOid = context.arguments.structureOid
    if (!structureOid || structureOid === 'undefined') return context.next(createError(400, 'structure invalide (undefined)'))
    if (!context.isFormateurInStructure(structureOid)) return context.accessDenied()
    const deletedOnly = context.request.query.deletedOnly
    const type = context.request.query.type

    let from = parseInt(context.request.query.from) || 0
    const response = {}

    let query = Utilisateur
      .match('structures').equals(structureOid)
      .match('isValid').equals(true)
    if (type) query = query.match('type').equals(type)
    if (deletedOnly) query = query.onlyDeleted()
    // @todo, si on fait de la pagination, il vaut mieux donner une clé de tri…

    flow()
      .seq(function () {
        query.count(this)
      })
      .seq(function (count) {
        response.count = count
        const options = { limit: maxResults, offset: from }
        query.grab(options, this)
      })
      .seq(function (utilisateurs) {
        from += maxResults
        if (from < response.count) {
          const nextUrl = 'utilisateur/by-structure/' + structureOid + '?'
          const nextUrlQuery = Object.assign({}, context.request.query, { from })
          response.nextUrl = nextUrl + querystring.stringify(nextUrlQuery)
        }

        response.utilisateurs = utilisateurs
        context.rest(response)
      })
      .catch(context.next)
  })

  /**
   * Renvoie la liste des utilisateurs en attente de validation pour la structure actuelle
   *
   * @name UtilisateursPending
   * @route {GET} api/utilisateur/pending
   * @authentication Cette route nécessite un formateur connecté
   */
  this.get('/api/utilisateur/pending', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    Utilisateur
      .match('structures').equals($session.getCurrentStructureOid(context))
      .match('isValid').equals(null)
      .grab((error, utilisateurs) => {
        if (error) return context.next(error)
        context.json({ utilisateurs })
      })
  })

  /**
   * Demande de réinitialisation du mot de passe de l'utilisateur
   *
   * @name UtilisateurAskPasswordReset
   * @route {GET} api/utilisateur/confirm/reinitialize-password/:oid/:token
   * @queryparam {integer} oid l'identifiant de l'utilisateur
   * @queryparam {String} token le token de validation
   */
  this.get('api/utilisateur/reinitialize-password/:oid/:token', function (context) {
    const oid = context.arguments.oid
    const token = context.arguments.token
    let password = null

    flow()
      .seq(function () {
        Utilisateur
          .match('oid').equals(oid)
          .grabOne(this)
      })
      .seq(function (utilisateur) {
        if (!utilisateur) return this()

        if (!utilisateur.reinitializeToken || utilisateur.reinitializeToken.token !== token) {
          return context.restKo('Ce lien n’est pas valide')
        }
        if (!utilisateur.reinitializeToken.date || moment().diff(utilisateur.reinitializeToken.date, 'hours') > TOKEN_LIFETIME) {
          return context.restKo('Ce lien n’est plus valide')
        }

        password = $utilisateur.generateUnPassword()
        utilisateur.password = $crypto.hash(password)
        delete utilisateur.reinitializeToken
        utilisateur.store(this)
      })
      .seq(function () {
        context.rest({
          password,
          link: $settings.get('application.baseUrl')
        })
      })
      .catch(context.next)
  })

  /**
   * Réinitialise le mot de passe d'un utilisateur.
   *
   * @name UtilisateurPasswordReset
   * @route {PUT} api/utilisateur/reinitialize-password
   */
  this.put('api/utilisateur/reinitialize-password', function (context) {
    if (!context.post.mail) return context.restKo('Données invalides')
    const { mail } = context.post
    let mailData
    let utilisateur
    flow().seq(function () {
      context.checkThrottle(this)
    }).seq(function () {
      Utilisateur
        .match('mail').equals(mail)
        .grabOne(this)
    }).seq(function (user) {
      if (!user) {
        const message = 'L’adresse mail indiquée ne correspond à aucun utilisateur.'
        return context.fieldError('mail', message)
      }
      if (user.externalMech) {
        const message = 'Le compte lié à cette adresse n’est pas un compte géré ici, vous devez vous rendre sur le service où vous vous êtes authentifié pour réinitialiser votre mot de passe.'
        return context.fieldError('mail', message)
      }
      utilisateur = user
      Structure
        .match('oid').equals(utilisateur.structures[0])
        .grabOne(this)
    }).seq(function (structure) {
      const token = $crypto.token()
      // les infos pour le mail (seq suivant)
      const baseUrl = $settings.get('application.baseUrl') + $settings.get('application.resetPasswordPath')
      mailData = {
        appName: $settings.get('application.name'),
        link: baseUrl + `/${utilisateur.oid}/${token}`,
        login: utilisateur.login,
        nomStructure: structure.nom
      }
      // ajout du token en db pour le user
      utilisateur.reinitializeToken = {
        token,
        date: Date.now()
      }
      utilisateur.store(this)
    }).seq(function () {
      // Envoi d'un mail de confirmation de réinitialisation du mot de passe
      const text = getMailTextReinitializePassword(mailData)
      $mail.sendMail({ to: mail, subject: 'Réinitialisation du mot de passe', text }, this)
    }).seq(function () {
      // si y'a une erreur au set en cache on la récupère mais pas de feedback utilisateur
      context.setThrottle(60, 'Vous ne pouvez pas demander plus d’une réinitialisation de mot de passe par minute.')
      context.rest({ message: 'Un mail vient d’être envoyé à ' + mail + ' pour réinitialiser votre mot de passe.' })
    }).catch(context.next)
  })

  /**
   * Déconnexion élève ou prof, redirige vers la déconnexion des clients sesalab-sso éventuels (sésathèques)
   * ATTENTION, c'est une page, pas un endpoint api malgré la route sur /api/ !
   * @route {POST} api/utilisateur/logout
   */
  this.get('api/utilisateur/logout', function (context) {
    let user = $session.getCurrentFormateur(context)
    const currentEleves = $session.getCurrentEleves(context)
    if (!user && currentEleves) user = currentEleves[0]
    appLog('logout du user', user)

    if (!$sso) {
      // on peut avoir des pbs de $sso pas encore déclaré (en test ou en cli ?)
      try {
        $sso = lassi.service('$sso')
      } catch (error) {
        console.error(error)
      }
    }

    let deconnexionUrl
    // en cas de redirection vers une autre page de logout, faut pas purger la session ici
    if ($sso && user?.externalMech) {
      // c'est un SSO externe, on demande au service son adresse de déconnexion
      deconnexionUrl = $sso.get(user.externalMech).getUrlDeconnexion(user.sso) || '/sso/logoutExternal'
    } else if (user?.type === TYPE_FORMATEUR) {
      // formateurs non SSO, faut déconnecter des sésathèques en redirigeant vers notre page html de logout (gérée par le module sesalab-sso)
      deconnexionUrl = '/sso/logout'
    } else {
      // élève non sso, on purge la session et redirige sur la home
      $session.flush(context)
      deconnexionUrl = '/'
    }
    context.redirect(deconnexionUrl)
  })

  /**
   * Suppression d'un compte depuis un mail de confirmation
   * C'est un fetch depuis une page non authentifiée
   * @name RemoveAccount
   * @route {GET} api/utilisateur/removeAccount/:oid/:token
   * @queryparam {string} oid Identifiant du compte {@link Utilisateur}
   * @queryparam {string} token Token de validation
   */
  this.get('api/utilisateur/removeAccount/:oid/:token', function (context) {
    const oid = context.arguments.oid
    const token = context.arguments.token

    let utilisateur

    flow()
      .seq(function () {
        Utilisateur.match('oid').equals(oid).grabOne(this)
      })
      .seq(function (_utilisateur) {
        if (!_utilisateur) return context.restKo('Ce lien n’est pas valide')
        utilisateur = _utilisateur
        if (!utilisateur.deleteAccountToken || utilisateur.deleteAccountToken.token !== token) {
          return context.restKo('Ce lien n’est pas valide')
        }
        if (!utilisateur.deleteAccountToken.date || moment().diff(utilisateur.deleteAccountToken.date, 'hours') > TOKEN_LIFETIME) {
          return context.restKo('Ce lien n’est pas valide')
        }
        utilisateur.delete(this)
      })
      .seq(function () {
        // on est pas sensé être authentifié, mais on purge toute la session quand même
        $session.flush(context)
      })
      .catch(context.next)
  })

  /**
   * Valide le compte d'un utilisateur.
   *
   * @name ValidateAccount
   * @authentication Cette route nécessite un formateur connecté
   * @route GET api/utilisateur/validate/:token/:action
   * @queryparam {string} token Token de validation
   * @queryparam {string} action "accept" pour accepter un compte, "decline" pour le refuser
   */
  this.get('api/utilisateur/validate/:oid/:action', function (context) {
    if (!$session.isAuthenticatedAsFormateur(context)) return context.accessDenied()
    const { oid, action } = context.arguments
    if (!['accept', 'decline'].includes(action)) return context.next(createError(400, `Action ${action} invalide`))
    const validator = $session.getCurrentFormateur(context)
    flow().seq(function () {
      $utilisateur.load(oid, this)
    }).seq(function (utilisateur) {
      // déjà refusé ?
      if (!utilisateur) throw createError(400, 'Cet utilisateur n’existe plus (probablement déjà refusé)')
      if ($validators.isAccountAccepted(utilisateur)) throw createError(400, `Ce compte ${utilisateur.prenom} ${utilisateur.nom} a déjà été validé`)
      // structure ok ? (utilisateur n'est pas validé, il ne peut donc avoir qu'une seule structure
      if (!validator.structures.includes(utilisateur.structures[0])) throw createError(400, 'Vous ne pouvez valider que des utilisateurs de vos structures')
      const isValidation = action === 'accept'
      $utilisateur.processPendingAccount(utilisateur, validator, isValidation, this)
    }).seq(function (message) {
      context.rest({ message })
    }).catch(context.next)
  })

  /**
   * Validation d'une adresse email.
   *
   * @name ValidateNewMail
   * @route {GET} api/utilisateur/validateNewMail/:oid/:token
   * @queryparam {string} oid Identifiant du compte {@link Utilisateur}
   * @queryparam {string} token Token de validation
   */
  this.get('api/utilisateur/validateNewMail/:oid/:token', function (context) {
    const oid = context.arguments.oid
    const token = context.arguments.token

    flow()
      .seq(function () {
        Utilisateur.match('oid').equals(oid).grabOne(this)
      })
      .seq(function (utilisateur) {
        if (!utilisateur) return context.restKo('Ce lien n’est pas valide')
        if (utilisateur?.validators?.newMail?.token !== token) {
          return context.restKo('Ce lien n’est pas valide')
        }
        utilisateur.mail = utilisateur.newMail

        // On n'a plus besoin du validator
        delete utilisateur.validators.newMail
        delete utilisateur.newMail
        utilisateur.store(this)
      })
      .seq(function () {
        context.rest({})
      })
      .catch(context.next)
  })

  /**
   * Validation d'une adresse email.
   *
   * @name ValidateMail
   * @route {GET} api/utilisateur/validateMail/:oid/:token
   * @queryparam {string} oid Identifiant du compte {@link Utilisateur}
   * @queryparam {string} token Token de validation
   */
  this.get('api/utilisateur/validateMail/:oid/:token', function (context) {
    const oid = context.arguments.oid
    const token = context.arguments.token
    let utilisateur

    flow()
      .seq(function () {
        Utilisateur.match('oid').equals(oid).grabOne(this)
      })
      .seq(function (_utilisateur) {
        utilisateur = _utilisateur
        if (!utilisateur) return context.restKo('Ce lien n’est pas valide')

        $validators.validateEmail(utilisateur, token)
        if (!$validators.isAccountAccepted(utilisateur)) {
          $validators.initAccountValidator(utilisateur)
        }

        utilisateur.store(this)
      })
      .seq(function (utilisateur) {
        // le compte a déjà été accepté à la création car y'avait pas d'autres formateurs au moment de la création.
        // À priori il devrait plutôt l'être ici, mais ça permet de régler le cas
        // - A demande un compte sur la structure S qu'il crée
        // - B demande un compte sur la structure S
        // - A valide son mail, il est validé d'office sans avoir besoin de l'aval de B
        if ($validators.isAccountAccepted(utilisateur)) return this()
        $utilisateur.notifyAboutPendingAccount(utilisateur.structures[0], utilisateur, this)
      })
      .seq(function () {
        let validationTarget
        if ($validators.isAccountAccepted(utilisateur)) validationTarget = 'none'
        else validationTarget = (utilisateur.type === TYPE_FORMATEUR ? 'teacher' : 'student')

        return context.rest({ validationTarget })
      })
      .catch(context.next)
  })
})
