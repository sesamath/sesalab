Tests fonctionnels
==================

Il faut implémenter des tests fonctionnels le plus étendus possible avant d'entamer tout upgrade d'angular (en version 1 dans l'interface élève) ou qooxdoo (interface prof)

## Scénarios de tests

On peut les générer relativement rapidement avec https://playwright.dev/docs/codegen

### Anonyme
- contenu de la home
- liens de connexion

### Élève

### Prof
- connexion
- création de ressources
  - type j3p
  - type mathgraph
  - type …
  - alias pour chacun (drag & drop de Ressources vers Mes ressources)
  - tester aussi la modif d'un alias de ressource restreinte
- édition de ressources
  - issues des fixtures
  - créées précédemment
  - modifier un alias doit le transformer en ressource
- pour chaque manip vérifier que les arbres sont mis à jour
- recharger la page et vérifier que tout est identique
- création de séquences
  - une pour chaque cas (ordonnée, bornée, etc)
  - vérifier que l'élève voit la nouvelle séquence ou les modifs
  - tester avec des ressources restreintes et alias de ressources restreintes
- déconnexion

## Setup
Au lancement, on utilisera la config du sesalab de test et ses sésathèques
- ça doit vérifier que les sésathèques répondent et initialiser la db avec les fixtures voulues
- vérifier l'init des sésathèques avec les fixtures voulues aussi
