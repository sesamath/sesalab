// checkBrowser, bugsnag et polyfill seront ajouté avant tout ça par webpackConfigBuilder.js
const angular = require('angular')
require('angular-sanitize')
require('@uirouter/angularjs')

const log = require('sesajstools/utils/log')

require('../styles/app.scss')

// doc angular sur https://docs.angularjs.org/guide
window.app = angular.module('sesalab', ['ui.router', 'ngSanitize'])

app.package = require('../../../package.json')

// on require tous les index.js contenu dans ce dossier
// cf https://webpack.js.org/guides/dependency-management/#require-context
function requireAll (r) {
  r.keys().forEach(r)
}
requireAll(require.context('./', true, /\/index.js$/))
// tout est chargé

app.config(['$locationProvider', function ($locationProvider) {
  $locationProvider.html5Mode(true)
}])

app.run(function ($app, $session, $state, $window) {
  'ngInject'

  $app.ready(function () {
    log('app ready')

    // Ignore les redirections si l'application est déjà dans un état à son lancement
    if ($state.current && $state.current.name) {
      return
    }

    // Redirection vers la home
    if (!$session.isActive()) {
      $window.location.href = app.settings.baseUrl
    } else {
      if ($session.isFormateur()) {
        // Si un formateur essaie d'accéder à l'interface élève sans iframe, on le redirige
        if ($window.top === $window.self) {
          $window.location.href = app.settings.baseUrl + 'formateur/'
        }
        // sinon il est dans une iframe et on le laisse continuer (pour les bilans)
      } else if ($session.mustResetPassword()) {
        $state.go('password')
      } else {
        $state.go('seances')
      }
    }
  })
})
