const _ = require('lodash')
const { notify } = require('sesalab-commun/notify')
const { DEFAULT_MAX_VISIONNAGE, DEFAULT_MINIMUM_REUSSITE, DEFAULT_NON_ZAPPING } = require('sesalab-commun/constants')

// utilisé par $seance
app.service('$exercice', function ($rootScope) {
  'ngInject'

  /**
   * Regroupe les 3 propriétés (nbEssais, pourcentageSucces, fin) d'un exercice
   * qui sont mises à jour à chaque appel de resultat/sauvegarde
   */
  class Exercice {
    constructor (exoData) {
      this.rid = exoData?.rid ?? ''
      /**
       * Nb d'essais de l'exercice
       * @type {number}
       */
      this.nbEssais = Number.isInteger(exoData?.nbEssais) && exoData.nbEssais > -1
        ? exoData.nbEssais
        : 0
      this.nonZapping = Number.isFinite(exoData?.nonZapping) && exoData.nonZapping > 0
        ? exoData.nonZapping
        : DEFAULT_NON_ZAPPING
      this.minimumReussite = Number.isFinite(exoData?.minimumReussite) && exoData.minimumReussite >= 0
        ? exoData.minimumReussite
        : DEFAULT_MINIMUM_REUSSITE
      this.maximumVisionnage = Number.isInteger(exoData?.maximumVisionnage) && exoData.maximumVisionnage > 0
        ? exoData.maximumVisionnage
        : DEFAULT_MAX_VISIONNAGE
      /**
       * Score en %
       * @type {number}
       */
      this.pourcentageSucces = Number.isFinite(exoData?.pourcentageSucces) && exoData.pourcentageSucces >= 0 && exoData.pourcentageSucces <= 100
        ? Math.round(exoData.pourcentageSucces)
        : 0
      /**
       * Le titre de l'exercice
       * @type {string}
       */
      this.titre = exoData?.titre ?? ''
      /**
       * Type d'exercice (nom du plugin sesatheque)
       * @type {string}
       */
      this.type = exoData?.type ?? ''
      /**
       * Exercice terminé
       * @type {boolean}
       */
      this.fin = Boolean(exoData?.fin)
      /**
       * Mis par Serie.add()
       * @type {Serie}
       */
      this.parent = null
      /**
       * Index de l'exercice dans sa série, affecté par Serie.add()
       * @type {number}
       */
      this.index = -1

      // aj des autres propriétés éventuelles
      if (exoData) {
        for (const [prop, value] of Object.entries(exoData)) {
          if (!(prop in this)) {
            // on l'ajoute quand même pour faire "comme avant" 2025-01
            this[prop] = value
            // puis on ajoute getter / setter pour voir si on aurait pu ignorer cette prop
            // si y'a aucune notif après qq semaines on pourra virer tout ça
            Object.defineProperty(this, prop, {
              enumerable: true,
              configurable: true,
              get: () => {
                notify(Error(`Exercice::${prop} demandé alors qu’il n’est pas initialisé dans le constructeur`))
                return value
              },
              set: (value) => {
                notify(Error(`Exercice::${prop} affecté avec ${value} alors qu’il n’est pas initialisé dans le constructeur`))
                this[prop] = value
              }
            })
          }
        }
      }
    }

    getImage () {
      return `/formateur/icons/ressources/${this.type}.gif`
    }

    /**
     * Retourne la série de l'exo (undefined pour les exos en autonomie)
     * @returns {Serie|undefined}
     */
    serie () {
      return this.parent
    }

    loadHistory (derniereSeance) {
      const self = this
      const historiqueRessource = _.find(derniereSeance.ressources, { rid: self.rid })
      if (historiqueRessource) {
        Object.assign(self, {
          nbEssais: historiqueRessource.nbEssais || 0,
          pourcentageSucces: historiqueRessource.pourcentageSucces || 0,
          fin: historiqueRessource.fin
        })
      }
    }

    select () {
      // parent est la serie, sauf si c'est un exo en autonomie
      if (this.parent) {
        this.parent.selectExercice(this)
      }
      $rootScope.$broadcast('exercice::selected', this)
    }

    seance () {
      return this.parent?.parent
    }

    isPreviousSerieCompleted () {
      const previous = this.serie()?.previous
      if (!previous) return true
      return previous.isCompleted()
    }

    isPreviousCompleted () {
      if (!this.previous) return true
      return this.previous.isCompleted()
    }

    getSuccessEnough () {
      return this.minimumReussite <= this.pourcentageSucces
    }

    getNbVisionnages () {
      return this.nbEssais
    }

    getReussite () {
      if (!this.nbEssais) return 'Pas encore essayé'
      if (!this.fin) return 'Pas encore terminé'
      return this.pourcentageSucces + '%'
    }

    getNbVisionnagesRestant () {
      return this.maximumVisionnage - this.nbEssais
    }

    withMaximumVisionnage () {
      return !this.serie()?.isOrdonnee() && this.maximumVisionnage
    }

    withMinimumReussite () {
      return this.serie()?.withMinimumReussite() && this.minimumReussite
    }

    isDisabled () {
      if (this.seance().lineaire && !this.isPreviousSerieCompleted()) return true
      if (this.serie()?.isOrdonnee() && !this.isPreviousCompleted()) return true
      // Le maximum de visionnage n'est pas actif sur les séries ordonnées (pour éviter les blocages)
      if (this.withMaximumVisionnage() && this.getNbVisionnagesRestant() <= 0) return true

      return false
    }

    isCompleted () {
      if (this.serie()?.withMinimumReussite()) {
        return (!this.minimumReussite || this.getSuccessEnough())
      }

      return !!this.fin
    }

    isCurrent () {
      return this.parent && this.parent.isCurrent() && (this.parent.currentExo === this)
    }
  }

  function create (exoData) {
    return new Exercice(exoData)
  }

  return {
    create
  }
})
