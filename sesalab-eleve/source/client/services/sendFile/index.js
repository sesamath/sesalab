/* global Blob */
app.service('$sendFile', function () {
  let _link

  function initialize () {
    _link = document.createElement('a')
    document.body.appendChild(_link)
    _link.style = 'display: none'
  }

  function send (content, fileName, contentType) {
    contentType = contentType || 'octet/stream'
    const blob = new Blob([content], { type: contentType })
    const uri = window.URL.createObjectURL(blob)
    _link.href = uri
    _link.download = fileName
    _link.click()

    setTimeout(function () { // Pour FF sinon il ne déclenche pas le DL
      window.URL.revokeObjectURL(uri)
    })
  }

  initialize()

  return {
    send
  }
})
