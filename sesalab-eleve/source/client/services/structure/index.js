/**
 * Gestion des structures.
 *
 * @service $api
 * @param $api Service API à charger
 */
app.service('$structure', function ($api, $errors, $toaster) {
  'ngInject'

  /**
   * Récupération d'une structure.
   *
   * @param {string} oid Identifiant de la structure
   * @param callback Callback.
   */
  function get (oid, callback) {
    $api
      .get('structure/' + oid)
      .then(response => callback(null, response.structure))
      .catch($errors.handle('Une erreur s’est produite pendant la récupération de la structure', callback))
  }

  /**
   * Récupère une structure suivant son nom ou son code
   *
   * @param {string} name Nom de la structure (ou code)
   * @param {Object} options Options
   * @param {function} callback Callback.
   */
  function getByName (name, options, callback) {
    $api
      .get('structure?_nom=' + name + '&hideClosedStructures=false', options)
      .then((response) => {
        if (response.message) return $toaster.pop(response.message, 'error', 10000)
        if (!response.structures || !response.structures.length) $toaster.pop('Aucun résultat', 'error', 10000)
        callback(null, response.structures)
      })
      .catch($errors.handle('Une erreur s’est produite pendant la récupération de la structure', callback))
  }

  return {
    get,
    getByName
  }
})
