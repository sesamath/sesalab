const { notify } = require('sesalab-commun/notify')

/**
 * Gestion des erreurs.
 *
 * @service $errors
 */
app.service('$errors', function ($toaster, $window) {
  /**
   * Retourne un gestionnaire d'erreur qui affichera un message via toaster en cas d'erreur
   * @param {string} message Le message qui sera affiché en toaster en cas d'erreur
   * @param {function} callback Si fournie, en cas d'erreur sera rappelé avec Error(message) en 1er argument (et pas l'erreur passée), rien sinon
   * @return {Function}
   */
  function handle (message, callback) {
    /**
     * Affiche un message en cas d'erreur (et log l'erreur, + redirect éventuel)
     * @param {Error} [error]
     * @param {object} [error.data]
     * @param {string} [error.data.message] Si ça existe ça remplacera le message passé à $errors.handle
     * @param {string} [error.response.data.message] Si ça existe ça remplacera le message passé à $errors.handle
     * @param {string} [error.data.urlRedirect] Si ça existe ça déclenchera une redirection vers cette url après 2s
     */
    function errorHandler (error) {
      if (error) {
        // error.response mis par nos wrappers de $api
        if (error.response?.data?.message && !error.response.data.internalError) {
          message = error.response.data.message
        }
        if (message) $toaster.pop(message, 'error', 10000)
        // on gère une éventuelle redirection
        if (error.data?.urlRedirect) {
          $toaster.pop('Redirection dans 3s', 'error', 3000)
          setTimeout(function () {
            $window.location.href = error.data.urlRedirect
          }, 3000)
        }
        if (typeof error === 'string') error = Error(error) // pour récupérer la stack
        console.error(error)
        if (callback) {
          callback(message ? Error(message) : error)
        }
      }
    }
    return errorHandler
  }

  /**
   * Notification de l'erreur pour un événement
   * @param {Event} event Objet de type Event
   * @param {Error} error Objet de type Error
   * @param {Object} [metaData] Données supplémentaires éventuelles
   */
  function sendEventError (event, error, metaData) {
    // On ne veut pas logguer l'intégralité de l'event
    const { data, origin, isTrusted, type } = event
    notify(error, Object.assign({ data, origin, isTrusted, type }, metaData))
  }

  return {
    handle,
    notify,
    sendEventError
  }
})
