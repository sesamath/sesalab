const _ = require('lodash')

app.service('$popup', function ($compile) {
  'ngInject'

  const stack = []

  function initialize () {
    jQuery('body').append('<div class="an-curtain"></div>')
  }
  initialize()

  function show (options) {
    const $scope = options.scope.$new(true)
    $scope.processButton = function (button) {
      button.callback(button)
    }
    Object.assign($scope, options)

    let width = options.width || 300
    let height = options.height || 300

    const screenWidth = jQuery(window).width()
    const screenHeight = jQuery(window).height()
    if (typeof width === 'string' && width.indexOf('%') !== -1) {
      width = parseInt(options.width.substr(0, width.length - 1), 10) * screenWidth / 100
    }
    if (typeof height === 'string' && height.indexOf('%') !== -1) {
      height = parseInt(height.substr(0, height.length - 1), 10) * screenHeight / 100
    }
    const left = (screenWidth - width) / 2
    const top = (screenHeight - height) / 2
    $scope.style = _.mapValues({ top, left, height, width }, (value) => `${value}px`)
    $scope.closeable = options.closeable !== false

    $scope.doClosePopup = function () {
      popup.close()
      if (options.onClose) options.onClose()
    }
    let popup
    stack.push(popup = {
      element: $compile(require('./template.html'))($scope)
    })

    popup.close = function () {
      popup.element.removeClass('active')
      popup.element.addClass('inactive')
      setTimeout(function () {
        stack.splice(stack.indexOf(popup), 1)
        processVisibility()
        popup.element.remove()
      }, 250)
    }
    if (options.cssClass) popup.element.addClass(options.cssClass)
    jQuery('body').append(popup.element)
    const content = jQuery('.content', popup.element)
    content.html(options.template)
    $compile(content.contents())(options.scope)

    processVisibility()
    return popup
  }

  function processVisibility () {
    if (stack.length === 0) {
      jQuery('.an-curtain').removeClass('active')
    } else {
      jQuery('.an-curtain').addClass('active')
      for (let i = 0; i < stack.length - 1; i++) {
        stack[i].element.removeClass('active')
      }
      stack[stack.length - 1].element.addClass('active')
    }
  }

  return {
    show
  }
})
