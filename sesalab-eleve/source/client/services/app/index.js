const flow = require('an-flow')
const log = require('sesajstools/utils/log')
const { hasProp } = require('sesajstools')
const { getParameter } = require('sesajstools/http/url')
const getClient = require('sesatheque-client/src').default

/**
 * Initialisation de l'application.
 */
app.service('$app', function ($api, $errors, $session, $toaster) {
  'ngInject'

  const readyCallbacks = []
  let isReady = false

  /** Fonction de démarrage. */
  function ready (cb) {
    if (isReady) return cb()

    readyCallbacks.push(cb)

    // si on est 2e alors que isReady n'est pas encore true c'est que la 1re a pas fini son fetch,
    // on la laisse finir et cb sera rappelée ensuite
    if (readyCallbacks.length !== 1) return

    flow().seq(function () {
      fetchSettings(this)
    }).seq(function (settings) {
      // settings en global
      app.settings = settings

      // on affecte le logLevel en fonction de l'url ou du staging
      let logLevel = getParameter('logLevel')
      if (logLevel && !hasProp(log.levels, logLevel)) {
        log.error(`${logLevel} n’est pas un logLevel connu`)
        logLevel = ''
      }
      if (!logLevel) {
        logLevel = /pre-prod/.test(settings.staging)
          ? log.levels.debug
          : log.levels.warning
      }
      log.setLogLevel(logLevel)

      // et connexion à la session angular
      $session.connect(this)

    // idem sesalab-formateur/source/client/classes/Application.js
    }).seq(function () {
      app.sesatheque = getClient(
        app.settings.sesatheques,
        app.settings.baseId
      )
      // on ajoute aussi 2 raccourcis vers les baseId des deux sésathèques
      app.sesatheque.baseIdGlobal = app.settings.sesatheques[0].baseId
      app.sesatheque.baseIdPrivate = app.settings.sesatheques[1].baseId
      // et l'id de l'arbre des exercices en autonomie
      app.sesatheque.autonomie = app.settings.profils.autonomie

      isReady = true
      // on peut lancer la pile de callback
      for (const cb of readyCallbacks) cb()
    }).catch($errors.handle('Une erreur s’est produite pendant l’initialisation de l’interface'))
  }

  /**
   * Récupération des réglages courants.
   * @param cb Callback.
   */
  function fetchSettings (cb) {
    const defaultErrorMessage = 'Une erreur s’est produite pendant la récupération des paramètres'
    let url = 'settings?eleve=true'
    // faut pas de redirection quand y'a pas de session si on est sur le form d'inscription
    // ou si c'est un formateur qui regarde des bilans en iframe
    if (!/\/inscription/.test(window.location.href) && window.top === window.self) {
      url += '&connected=1'
    }
    $api
      .get(url)
      .then(({ settings, postLogin, redirectTo, message }) => {
        if (redirectTo) {
          // on affiche le message et on redirige, sans rappeler cb
          if (!message) message = defaultErrorMessage
          message += '\nRedirection dans 2s'
          $toaster.pop(message, 'error')
          setTimeout(() => { window.location.href = redirectTo }, 2000)
          return // cb sera jamais appelée et le chargement angular s'arrête là
        }
        if (postLogin) {
          $toaster.pop(postLogin.message, postLogin.type)
        }
        cb(null, settings)
      })
      .catch($errors.handle(defaultErrorMessage, cb))
  }

  return {
    ready
  }
})
