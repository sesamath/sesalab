const { SERIE_STATUT_LIBRE, SERIE_STATUT_ORDONNEE, SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE } = require('sesalab-commun/constants')
const { notify } = require('../../../../../sesalab-commun/notify')

const types = [SERIE_STATUT_LIBRE, SERIE_STATUT_ORDONNEE, SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE]

app.service('$serie', function () {
  /**
   * Constructeur de série, son parent
   * @param options
   * @constructor
   */
  function Serie (options) {
    /**
     * Séance parente, affectée dans Seance::add()
      * @type {Seance}
     */
    this.parent = null
    /**
     * Le type de série (0|1|2) pour libre|ordonnée|minReussite
     * @type {number}
     */
    this.type = types.includes(options?.type) ? options.type : SERIE_STATUT_LIBRE
    /**
     * Les exercices de la série
     * @type {Exercice[]}
     */
    this.exercices = Array.isArray(options?.exercices) ? options.exercices : []
    /**
     * L'exercice courant
     * @type {Exercice}
     */
    this.currentExo = null
    if (options) {
      for (const [prop, value] of Object.entries(options)) {
        if (!(prop in this)) {
          // on l'ajoute quand même pour faire "comme avant" 2025-01
          this[prop] = value
          // puis on ajoute getter / setter pour voir si on aurait pu ignorer cette prop
          // si y'a aucune notif après qq semaines on pourra virer tout ça
          Object.defineProperty(this, prop, {
            enumerable: true,
            configurable: true,
            get: () => {
              notify(Error(`Serie::${prop} demandé alors qu’il n’est pas initialisé dans le constructeur`))
              return value
            },
            set: (value) => {
              notify(Error(`Serie::${prop} affecté avec ${value} alors qu’il n’est pas initialisé dans le constructeur`))
              this[prop] = value
            }
          })
        }
      }
    }
  }

  /**
   * Sélectionne l'exercice passé en param (set currentExo)
   * @param {Exercice} exercice
   */
  Serie.prototype.selectExercice = function (exercice) {
    this.select()
    this.currentExo = exercice
  }

  Serie.prototype.add = function (exercice) {
    exercice.index = this.exercices.length + 1
    exercice.parent = this
    this.exercices.push(exercice)
  }

  Serie.prototype.isOrdonnee = function () {
    return this.type !== SERIE_STATUT_LIBRE
  }

  Serie.prototype.withMinimumReussite = function (resource) {
    return this.type === SERIE_STATUT_ORDONNEE_MINIMUM_REUSSITE
  }

  /**
   * Passe à la série suivante de la séance si la série courante est complétée
   */
  Serie.prototype.nextSerieIfCompleted = function () {
    if (this.isCompleted()) this.parent.checkIfCompleted()
  }

  /**
   * Retourne true si tous les exercices de la série sont completed
   * @return {boolean}
   */
  Serie.prototype.isCompleted = function () {
    if (!this.exercices.length) return false
    return this.exercices.every((exercice) => exercice.isCompleted())
  }

  /**
   * Sélectionne la série et son premier exercice
   */
  Serie.prototype.select = function () {
    this.parent.selectSerie(this)
    this.currentExo = this.exercices[0]
  }

  /**
   * Retourne true si c'est la série courante de la séance
   * @return {boolean}
   */
  Serie.prototype.isCurrent = function () {
    return this.parent.currentSerie === this
  }

  /**
   * Retourne les exercices de la série
   * @return {Exercice[]}
   */
  Serie.prototype.getExercices = function () {
    return this.exercices
  }

  /**
   * Instancie une Serie
   * @param {Object} serieData
   * @param {number} [serieData.type] 0 => libre, 1 => ordonnée, 2 => ordonnée avec minimum de réussite (cf sesalab-commun/constants.js)
   * @param {Exercice[]} exercices
   * @return {Serie}
   */
  function create (serieData) {
    return new Serie(serieData)
  }

  return {
    create
  }
})
