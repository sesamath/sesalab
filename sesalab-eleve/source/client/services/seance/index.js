const _ = require('lodash')
const { SEANCE_STATUT_INACTIF } = require('sesalab-commun/constants')

app.service('$seance', function ($api, $errors, $exercice, $serie, $session, $rootScope, $toaster) {
  'ngInject'

  /**
   * Une Seance reflète l'état de de réalisation d'une séquence (pour chaque exercice
   * le nb d'essais, max de visionnage, etc.)
   * @param options
   * @param sequencesRegistry
   * @constructor
   */
  function Seance (options, sequencesRegistry) {
    Object.assign(this, options)
    this.series = []

    // sequencesRegistry est optionnel, il n'est pas fourni dans le cas du test
    // de la vue élève par un professeur par exemple
    this.sequencesRegistry = sequencesRegistry
  }

  Seance.prototype.loadHistory = function () {
    const self = this
    $api
      .get('last-seance?sequence=' + self.sequenceOid)
      .then((response) => {
        Object.assign(self, {
          dernierVisionnage: response.dernierVisionnage
        })
        _.each(self.getExercices(), (ressource) => ressource.loadHistory(response))

        // Après avoir chargé l'historique on vérifie si la seance est terminée
        this.checkIfCompleted()
      })
      .catch($errors.handle('Une erreur s’est produite pendant le chargement de l’historique de cette séquence'))
  }

  Seance.prototype.is = function (seance) {
    return this.oid === seance.oid
  }

  Seance.prototype.add = function (serie) {
    serie.parent = this
    this.series.push(serie)
    return serie
  }

  Seance.prototype.getExercices = function () {
    let result = []
    _.each(this.series, (serie) => {
      result = result.concat(serie.getExercices())
    })
    return result
  }

  Seance.prototype.isCompleted = function () {
    return _.every(this.series, (s) => s.isCompleted())
  }

  /**
   * Si la séance est complète on appelle sequencesRegistry.complete()
   * pour voir si on peut activer des séquences non prioritaires
   */
  Seance.prototype.checkIfCompleted = function () {
    if (!this.sequencesRegistry) return
    if (this.isCompleted()) this.sequencesRegistry.complete(this)
  }

  Seance.prototype.isDisabled = function () {
    if (!this.sequencesRegistry) return false // sans registry on ne désactive pas les séances

    return this.sequencesRegistry.isDisabled(this)
  }

  Seance.prototype.start = function (exercice) {
    // Note: le dernierVisionnage est aussi mis à jour côté serveur
    this.dernierVisionnage = new Date()
    exercice.select()
  }

  Seance.prototype.selectSerie = function (serie) {
    this.currentSerie = serie
  }

  /**
   * Ajoute les propriétés previous et next à chaque elt du tableau
   * @private
   * @param {Array} a
   */
  function chain (a) {
    for (let i = 0, ii = a.length; i < ii; i++) {
      a[i].previous = a[i - 1]
      a[i].next = a[i + 1]
    }
  }

  /**
   * Vérifie que l'on a les propriétés attendue et ajoute la propriété formateur à chaque séquence
   * @private
   * @param response
   * @return {*}
   */
  function checkSeqResponse (response) {
    // on vérifie que ces propriétés sont présentes
    if (typeof response.formateursByOid !== 'object') throw Error('formateursByOid manquant')
    ;['sequencesActives', 'sequencesInactives', 'sequencesDesaffectees'].forEach(p => {
      // on vérifie que c'est un tableau
      if (!Array.isArray(response[p])) {
        const error = Error(`Propriété ${p} absente de la réponse`)
        // seule cette liste est vraiment obligatoire (le prof qui teste n'a pas les autres)
        if (p === 'sequencesActives') throw error
        response[p] = []
      }
      // on ajoute une propriété formateur à chaque sequence (le reste du code l'attend là)
      response[p].forEach(seq => {
        seq.formateur = response.formateursByOid[seq.owner]
      })
    })

    return response
  }

  /**
   * @typedef AllSequencesObject
   * @property {Object} formateursByOid contient tous les formateur, clé oid et valeur {oid, nom, prenom}
   * @property {Sequence[]} sequencesActives
   * @property {Sequence[]} sequencesInactives
   * @property {Sequence[]} sequencesDesaffectees Les séquences qui ne sont plus affectées à cet élève mais pour lesquelles il a des résultats
   */
  /**
   * @callback getAllSequencesCallback
   * @param {AllSequencesObject} result
   */
  /**
   * Récupère l'ensemble des séquences via GET api/sequences-eleves
   * @param {getAllSequencesCallback} callback
   */
  function getAllSequences (callback) {
    $api
      .get('sequences-eleves')
      .then(checkSeqResponse)
      .then(response => callback(null, response))
      .catch($errors.handle('Impossible de récupérer la liste des exercices pour le moment', callback))
  }

  /**
   * Récupère une séquence (GET /api/sequence-eleve/:sequenceOid/:eleveOid), utilisable seulement par un formateur
   * @param {string} sequenceOid
   * @param {string} eleveOid
   * @param {function} successCb cb appelée avec la séquence (pas en cas d'erreur, gérée par ailleurs)
   */
  function getSequence (sequenceOid, eleveOid, successCb) {
    $api
      .get(`sequence-eleve/${sequenceOid}/${eleveOid}`)
      .then(response => {
        if (!response.sequence || !response.sequence.oid) throw Error('Réponse invalide')
        successCb(response.sequence)
      })
      .catch($errors.handle('Une erreur s’est produite lors de la récupération de la séquence'))
  }

  /**
   * Instancie une Seance
   * @param {Sequence} sequence
   * @param {SequenceRegistry} sequencesRegistry
   * @return {Seance}
   */
  function create (sequence, sequencesRegistry) {
    let dateDebut = null
    if (sequence.fromDate) {
      let tsDebut = (new Date(sequence.fromDate)).getTime()
      if (typeof sequence.fromTime === 'number') tsDebut += sequence.fromTime * 15 * 60000
      else console.error(Error('fromDate présent mais fromTime invalide'))
      dateDebut = new Date(tsDebut)
    }
    let dateFin = null
    if (sequence.toDate) {
      let tsFin = (new Date(sequence.toDate)).getTime()
      if (typeof sequence.toTime === 'number') tsFin += sequence.toTime * 15 * 60000
      else console.error(Error('toDate présent mais toTime invalide'))
      dateFin = new Date(tsFin)
    }
    const seance = new Seance({
      sequenceOid: sequence.oid,
      dateDebut,
      dateFin,
      inactive: sequence.statut === SEANCE_STATUT_INACTIF,
      lineaire: !!sequence.lineaire,
      uniqEleve: !!sequence.uniqEleve,
      message: sequence.message,
      nom: sequence.nom,
      formateur: sequence.formateur,
      prioritaire: sequence.prioritaire
    }, sequencesRegistry)
    sequence.sousSequences.forEach((sousSequence) => {
      const serie = $serie.create({
        type: sousSequence.type
      })
      seance.add(serie)
      for (const ress of sousSequence.serie) {
        // attention, faut produire ici qqchose qui ressemble à un ClientItem car ce sera passé au normalize
        serie.add($exercice.create(ress))
      }
      chain(serie.exercices)
    })
    chain(seance.series)
    if (!$session.isFormateur()) seance.loadHistory()
    return seance
  }

  /**
   * @typedef ApiResultatsForSequenceResponse
   * @property {Object} eleves oid => Utilisateur
   * @property {Resultat[]} resultats
   * @property {Sequence} sequence
   */

  /**
   * @callback ApiResultatsForSequenceResponseCallback
   * @param {Error|null} error
   * @param {ApiResultatsForSequenceResponse} response
   */

  /**
   * Génère éventuellement un message suivant les résultats (si aucun ou trop), sinon trie les résultats par classe/élève/date
   *
   * @param {ApiResultatsForSequenceResponse} response Réponse API contenant les résultats
   * @param {ApiResultatsForSequenceResponseCallback}
   * @return {Object}
   */
  function sortResultats (response, callback) {
    if (!response.resultats || !response.resultats.length) {
      const message = 'Il n’y a pas encore de résultat enregistré.'
      $toaster.pop(message, 'error')
      return callback(new Error(message))
    }
    if (response.resultats.length >= app.settings.maxBilans) {
      const message = `Le bilan demandé contient trop de résultats pour être affiché en totalité.<br/>
      Voici les ${app.settings.maxBilans} derniers bilans de la séquence.<br/>
      Pour obtenir TOUS les résultats, merci de passer par l’export csv.<br/>
      Pour ne pas reproduire ce problème, peut-être devriez-vous mettre moins d’élèves et/ou moins de ressources
      dans la même séquence.`
      $toaster.pop(message, 'error', 15000)
    }

    // on veut trier par nom de classe puis nom élève puis prénom puis date de résultat
    const resultatsTries = response.resultats.sort((resA, resB) => {
      // il faut retourner un négatif pour conserver a avant b, un positif pour b avant a
      // (pour 0 normalement c'est inchangé mais c'est pas garanti)

      // on trie sur le 1er élève, en cas de résultats à plusieurs faudrait faire quoi ?
      const elA = resA.participants[0]
      const elB = resB.participants[0]
      for (const crit of ['className', 'nom', 'prenom']) {
        if (elA[crit] !== elB[crit]) return elA[crit] < elB[crit] ? -1 : 1
      }
      // même élève, on trie par index de sous-seq puis index de ressource puis $date (la date du résultat en string)
      for (const crit of ['$indexSousSequence', '$indexRessource', '$date']) {
        if (resA[crit] !== resB[crit]) return resA[crit] < resB[crit] ? -1 : 1
      }
      return 0 // deux résultats avec exactement la même date, à priori pas possible mais faut retourner une valeur
    })
    response.resultats = resultatsTries

    return callback(null, response)
  }

  /**
   * Récupération des résultats pour une séquence.
   * (ceux de l'élève si c'est un élève en session ou tous si c'est un prof)
   * @param {string} sequenceOid Identifiant de la séquence
   * @param {ApiResultatsForSequenceResponseCallback} callback Callback.
   */
  function resultats (sequenceOid, callback) {
    $api
      .get(`resultat/sequence/${sequenceOid}`)
      .then((response) => sortResultats(response, callback))
      .catch($errors.handle('Une erreur s’est produite durant la récupération des résultats', callback))
  }

  /**
   * Récupération des résultats en autonomie.
   *
   * @param callback Callback.
   */
  function resultatsAutonomie (callback) {
    $api
      .get('resultat/autonomie')
      .then((response) => sortResultats(response, callback))
      .catch($errors.handle('Une erreur s’est produite durant la récupération des résultats', callback))
  }

  return {
    create,
    getAllSequences,
    getSequence,
    resultats,
    resultatsAutonomie
  }
})
