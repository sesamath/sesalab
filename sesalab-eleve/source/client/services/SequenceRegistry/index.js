function notifySeq (sequence) {
  /* global bugsnagClient */
  bugsnagClient.addMetadata('sequence', sequence)
  bugsnagClient.notify(Error('sequence invalide sans formateur.oid (ou sans sequenceOid)'))
}

/**
 * Enregistre les séquences prioritaire de la session par formateur, pour suivre leur état
 * Une fois toutes les séquences prioritaires d'un formateur terminées, on peut activer les autres du même formateur
 * (même s'il reste des séquences prioritaires à faire pour un autre formateur).
 */
class SequenceRegistry {
  /**
   * Initialise les séquences prioritaires par formateur
   * @param {Sequence[]} sequences (AVEC chacune une propriété formateur contenant {oid, nom, prenom})
   */
  constructor (sequences) {
    // une liste [formateurOid]: {[sequenceOid]: boolean}
    // pour savoir s'il reste des prioritaires pour ce formateur
    this.prioritaireByOwner = {}

    sequences.forEach((seq) => {
      if (!seq.prioritaire) return
      if (!seq.formateur || !seq.formateur.oid) return notifySeq(seq)
      const fOid = seq.formateur.oid
      if (!this.prioritaireByOwner[fOid]) this.prioritaireByOwner[fOid] = {}
      this.prioritaireByOwner[fOid][seq.oid] = true
    })
  }

  /**
   * Appelé par seance.checkIfCompleted lorsque tous les exercices d'une séquence sont terminés,
   * pour activer éventuellement les séquences non prioritaires de ce formateur
   * @param seance
   */
  complete (seance) {
    // si y'a pas de séquence prioritaire l'objet prioritaireByOwner reste vide
    if (seance && seance.formateur && seance.formateur.oid && seance.sequenceOid) {
      if (this.prioritaireByOwner[seance.formateur.oid]) {
        delete this.prioritaireByOwner[seance.formateur.oid][seance.sequenceOid]
      }
    } else {
      notifySeq(seance)
    }
  }

  /**
   * Retourne true si la séquence n'est pas prioritaire et qu'il reste
   * des prioritaires inachevées du même formateur
   * @param sequence
   * @return {boolean}
   */
  isDisabled (sequence) {
    if (sequence.prioritaire) return false
    if (!sequence.formateur || !sequence.formateur.oid) return notifySeq(sequence) // renverra undefined
    if (!this.prioritaireByOwner[sequence.formateur.oid]) return false

    return Boolean(Object.keys(this.prioritaireByOwner[sequence.formateur.oid]).length)
  }
}

app.service('SequenceRegistry', function () {
  return SequenceRegistry
})
