const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')

/**
 * Gestion de la session utilisateur.
 *
 * @service $session
 */
app.service('$session', function ($rootScope, $structure, $eleve, $window) {
  'ngInject'

  let _utilisateurs = []
  let _structure = null
  let _readonlyToken

  /**
   * Connection de l'utilisateur si une session
   * existe sur le serveur.
   */
  function connect (cb) {
    $eleve.current((error, data) => {
      if (error) return cb(error)

      _utilisateurs = _.compact(data.utilisateurs)
      _readonlyToken = data.readonlyToken

      /* global bugsnagClient */
      bugsnagClient.addMetadata('user', { eleves: _.map(_utilisateurs, 'oid').join('-') })

      if (_utilisateurs[0] && _utilisateurs[0].structures[0]) {
        $structure.get(_utilisateurs[0].structures[0], (error, structure) => {
          if (error) return cb(error)
          _structure = structure

          $rootScope.$broadcast('session::updated')
          cb()
        })
      } else {
        $rootScope.$broadcast('session::updated')
        cb()
      }
    })
  }

  function disconnect () {
    _utilisateurs = []
    $window.location.href = '/api/utilisateur/logout'
  }

  function isActive () {
    return !!_utilisateurs.length
  }

  function isFormateur () {
    return isActive() && _utilisateurs[0] && _utilisateurs[0].type === constants.TYPE_FORMATEUR
  }

  function mustResetPassword () {
    return _utilisateurs && _utilisateurs.some(u => u.mustResetPassword)
  }

  function utilisateurs () {
    return _utilisateurs
  }

  function isMultiSession () {
    return _utilisateurs && _utilisateurs.length > 1
  }

  function readonlyToken () {
    return _readonlyToken
  }

  function updateUtilisateur (userData) {
    const userToUpdateIndex = _.findIndex(_utilisateurs, { oid: userData.oid })
    if (userToUpdateIndex !== -1) {
      _utilisateurs.splice(userToUpdateIndex, 1, userData)
    }

    return _utilisateurs
  }

  function structure () {
    return _structure
  }

  return {
    connect,
    isActive,
    isFormateur,
    isMultiSession,
    mustResetPassword,
    utilisateurs,
    updateUtilisateur,
    structure,
    disconnect,
    readonlyToken
  }
})
