app.service('$formatter', function () {
  const formatters = require('sesatheque-client/src/resultatFormatters').default

  formatters.default = {
    getHtmlScore: (resultat) => typeof resultat.score === 'number' ? `${Math.round(resultat.score * 100)}%` : 'pas de score',
    getHtmlReponse: (resultat) => resultat.reponse || '?',
    getHtmlFullReponse: (resultat) => false
  }

  // on ajoute les éventuelles méthodes manquantes des formatters importés (qui ne surchargent pas forcément ces deux méthodes)
  Object.keys(formatters).forEach(type => {
    if (type === 'default') return
    ;['getHtmlScore', 'getHtmlReponse', 'getHtmlFullReponse'].forEach(method => {
      if (!formatters[type][method]) formatters[type][method] = formatters.default[method]
    })
  })

  /**
   * Retourne le formatter pour ce type d'exercice
   * @param {string} type
   * @return {{getHtmlScore: function(Resultat): string, getHtmlReponse: function(Resultat, options): string}|*}
   */
  function getFormatter (type) {
    if (formatters[type]) return formatters[type]
    console.error(Error(`type ${type} non géré dans sesatheque-client/src/resultatFormatters`))
    return formatters.default
  }

  return {
    /**
     * Retourne le score formaté en string (en général en %)
     * @param {string} type
     * @param {Resultat} resultat
     * @return {string}
     */
    score: (type, resultat) => getFormatter(type).getHtmlScore(resultat),
    /**
     * Retourne la réponse formatée
     * @param {string} type
     * @param {Resultat} resultat
     * @param {boolean} isDaltonien
     * @return {string}
     */
    response: (type, resultat, isDaltonien) => getFormatter(type).getHtmlReponse(resultat, { isDaltonien }),
    /**
     * Retourne éventuellement les détails de la réponse formatée (false sinon)
     * @param {string} type
     * @param {Resultat} resultat
     * @return {string|boolean} Si false y'a pas de détails à présenter (sinon le code html à mettre dans une iframe pour rien casser de la page courante)
     */
    fullResponse: (type, resultat) => getFormatter(type).getHtmlFullReponse(resultat)
  }
})
