/**
 * Gestion de l'API.
 *
 * @service $api
 * @param $http Service HTTP à charger
 */
app.service('$api', function ($http) {
  'ngInject'

  /**
   * Callback générigue pour formater la réponse
   * @private
   * @param {Object} response L'objet response retourné par le service $http d'angular
   * @return {Promise}
   */
  function responseCallback (response) {
    if (!response.data.success) {
      const error = new Error(response.data.message || 'Une erreur est survenue')
      error.response = response
      return Promise.reject(error)
    }

    /* global bugsnagClient */
    bugsnagClient.addMetadata('user', { lastSessionAlive: new Date() })

    return Promise.resolve(response.data)
  }

  /**
   * Il faut récupérer les rejets de $http pour rejeter avec une vraie Error (il le fait avec un objet !)
   * @param response
   * @return {Promise<never>}
   */
  function errorCallback (response) {
    console.error(Error('Erreur $http'), response)
    let msg
    let isFriendly = false
    if (response?.data?.message) {
      // c'est une réponse json du serveur
      msg = response.data.message
      isFriendly = true
    } else if (response.status === 401) {
      msg = 'Requête non authentifiée (probablement une session expirée, dans ce cas il faut se reconnecter)'
      isFriendly = true
    } else if (response.status === -1) {
      msg = 'La requête n’a pas pu aboutir, vérifier le réseau et retenter'
    } else if (response.data && typeof response.data === 'string' && !response.data.includes('<html>')) {
      // en cas d'erreur 50x ça peut être la page d'erreur en html
      msg = response.data
    } else {
      msg = 'Une erreur est survenue'
    }
    if (response?.data?.urlRedirect) {
      // en cas de perte de session on a un urlRedirect vers /lost-session,
      // on laisse un délai pour prendre une éventuelle capture d'écran
      msg += ' (redirection dans 5s)'
      setTimeout(() => {
        window.location.href = response.data.urlRedirect
      }, 5000)
    }
    const error = Error(msg)
    error.response = response
    error.userFriendly = isFriendly // pas de bugsnag si true
    return Promise.reject(error)
  }

  /**
   * Helper d'accès GET
   *
   * @param {string} service le path (qui suit /api/)
   * @param {object} [options={}] passées au $http d'angular
   * @return {Promise} qui sera résolue avec les datas retournées par l'api, ou rejetée avec une erreur (le message étant celui de l'api)
   */
  function get (service, options = {}) {
    return $http.get('/api/' + service, options).then(responseCallback, errorCallback)
  }

  /**
   * Helper d'accès POST
   *
   * @param {string} service le path (qui suit /api/)
   * @param {object} data Données à envoyer
   * @return {Promise}
   */
  function post (service, data) {
    return $http.post('/api/' + service, data).then(responseCallback, errorCallback)
  }

  /**
   * Helper d'accès PUT
   *
   * @param {string} service le path (qui suit /api/)
   * @param {object} data Données à envoyer
   * @return {Promise}
   */
  function put (service, data) {
    return $http.put('/api/' + service, data).then(responseCallback, errorCallback)
  }

  /**
   * Helper d'accès DELETE
   *
   * @param {string} service le path (qui suit /api/)
   * @return {Promise}
   */
  function del (service) {
    return $http.delete('/api/' + service).then(responseCallback, errorCallback)
  }

  return {
    get,
    post,
    put,
    del
  }
})
