/**
 * Gestion des élèves.
 *
 * @service $api
 * @param $api Service API à charger
 * @param $errors Service de gestion des erreurs à charger
 * @param $toaster Service de notification à charger
 */
app.service('$eleve', function ($api, $errors, $toaster) {
  'ngInject'

  /**
   * Récupération de la classe d'un élève.
   *
   * @param id Identifiant du l'élève
   * @param filters Filtres à envoyer à l'API
   * @param callback Callback.
   */
  function fetchClasse (id, filters, callback) {
    const filtersParam = filters && filters.length ? '?fields=' + filters.join(',') : ''
    $api.get(`eleve/${id}/classe${filtersParam}`)
      .then(response => callback(null, response))
      .catch($errors.handle('Une erreur s’est produite durant la récupération de la classe de l’élève', callback))
  }

  /**
   * Création d'un compte élève.
   *
   * @param utilisateur Un objet contenant les informations de l'utilisateur
   * @param callback Callback.
   */
  function create (utilisateur, callback) {
    $api
      .post('eleve', utilisateur)
      .then(response => callback(null, response))
      .catch($errors.handle('Une erreur s’est produite durant la création du compte', callback))
  }

  /**
   * Récupération de l'utilisateur en cours.
   *
   * @param callback Callback.
   */
  function current (callback) {
    $api
      .get('utilisateur?check') // on passe aussi là avec le form d'inscription, faut pas que ça redirige
      .then((response) => {
        let utilisateurs = []
        if (response.utilisateur) utilisateurs = [response.utilisateur]
        if (response.eleves) utilisateurs = response.eleves
        callback(null, { utilisateurs, readonlyToken: response.readonlyToken })
      })
      .catch($errors.handle('Une erreur s’est produite pendant la récupération de l’utilisateur connecté', callback))
  }

  /**
   * Restreint la session courante à un utilisateur unique
   * (pour accéder à une séquence mono utilisateur)
   *
   * @param utilisateur Utilisateur
   * @param callback Callback.
   */
  function relogin (utilisateur, callback) {
    $api
      .post('eleve/relogin', { eleve: utilisateur.oid })
      .then(response => callback(null, response))
      .catch($errors.handle('Une erreur s’est produite durant la récupération de l’utilisateur', callback))
  }

  /**
   * Récupération des résultats d'un utilisateur.
   *
   * @param utilisateur Utilisateur
   * @param callback Callback.
   */
  function resultats (utilisateurOid, callback) {
    $api
      .get(`resultat/eleve/${utilisateurOid}`)
      .then((response) => {
        if (!response.resultats || !response.resultats.length) {
          $toaster.pop('Il n’y a pas encore de résultat enregistré.', 'error')
          return
        }

        if (response.resultats.length >= app.settings.maxBilans) {
          const message = `Le bilan demandé contient trop de résultats pour être affiché en totalité.<br/>
          Voici les ${app.settings.maxBilans} derniers bilans de l’élève sélectionné.<br/>
          Pour obtenir TOUS les résultats, merci de passer par l’export csv.`
          $toaster.pop(message, 'error', 15000)
        }

        callback(null, response)
      })
      .catch($errors.handle('Une erreur s’est produite durant la récupération des résultats', callback))
  }

  /**
   * Sauvegarde un nouveau résultat (et gère le feedback utilisateur ok|KO)
   * @param {Resultat} resultatForExercice le résultat (cf sesatheque-client pour le format)
   * @param callback Callback
   */
  function sauvegardeResultat (resultatForExercice, callback) {
    async function send () {
      attempts++
      timeout += 2
      let state
      try {
        state = await $api.post('resultat/sauvegarde', resultatForExercice)
      } catch (error) {
        console.error(error)
        if (error?.response?.status === -1) {
          // c'est un pb réseau
          if (attempts < 4) {
            const delay = timeout * 1000
            $toaster.pop(`L'enregistrement du résultat a échoué, nouvelle tentative dans ${timeout} secondes`, 'error', delay)
            // on envoie à 0, +2s, +4s
            return setTimeout(send, delay)
          }
          // On lâche l'affaire en prévenant l'élève
          $toaster.pop(`L'enregistrement du résultat a échoué à ${attempts} reprises, le résultat n’est pas sauvegardé (faire une capture d’écran pour le signaler au professeur)`, 'error', -1)
        } else {
          $toaster.pop(error.message, 'error', 10000)
        }
        return callback(error)
      }
      // hors du try précédent pour ne pas renouveler l'envoi réussi si c'est callback qui plante
      // pas de feedback pour l'init de l'exo
      if (resultatForExercice.duree !== 0) $toaster.pop('Résultat sauvegardé')
      try {
        callback(null, state)
      } catch (error) {
        console.error(error)
        $toaster.pop('Le résultat est bien sauvegardé mais une erreur s’est produite ensuite', 'error')
      }
    } // send

    let attempts = 0
    let timeout = 0
    send()
  }

  /**
   * Récupération de l'utilisateur en cours.
   *
   * @param utilisateur Utilisateur
   * @param callback Callback.
   */
  function update (utilisateur, callback) {
    $api
      .put('eleve/' + utilisateur.oid, utilisateur)
      .then(response => callback(null, response.utilisateur))
      .catch($errors.handle('Une erreur s’est produite lors de la mise à jour du compte de l’élève', callback))
  }

  return {
    current,
    fetchClasse,
    create,
    update,
    relogin,
    resultats,
    sauvegardeResultat
  }
})
