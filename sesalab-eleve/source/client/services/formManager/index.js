app.service('$formManager', function () {
  function Manager (scope) {
    scope.errors = {}

    this.add = function (field, message) {
      if (this.hasError()) return
      scope.errors[field] = message
    }

    this.hasError = function () {
      return !!Object.keys(scope.errors).length
    }
  }

  return {
    manage: function (scope) {
      return new Manager(scope)
    }
  }
})
