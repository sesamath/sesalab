app.directive('rawHtml', function () {
  return {
    restrict: 'A',
    scope: {
      rawHtml: '='
    },
    link: function (scope, element, attributes) {
      const data = '<div>' + scope.rawHtml + '</div>'
      jQuery(data).appendTo(element)
    }
  }
})
