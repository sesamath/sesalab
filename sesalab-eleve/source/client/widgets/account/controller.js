const _ = require('lodash')
const isEmail = require('sesalab-commun/isEmail')

module.exports = function ($scope, $eleve, $session) {
  'ngInject'

  let _error = {}
  let user = {}

  $scope.numeroCNIL = app.settings.numeroCNIL
  $scope.appName = app.settings.name ?? 'Sesalab'
  $scope.version = app.settings.version
  $scope.canAccess = false
  $scope.updated = false
  $scope.userForm = {}

  function initialize () {
    const users = $session.utilisateurs()
    user = _.clone(users[0])
    $scope.canAccess = (users.length <= 1)
    $scope.userForm.prenom = user.prenom
    $scope.userForm.nom = user.nom
    $scope.newMailNotValidated = user.newMail && user.newMail !== user.mail
    $scope.userForm.mail = $scope.newMailNotValidated ? user.newMail : user.mail
    $scope.userForm.bilanWithSymbols = user.bilanWithSymbols

    const structure = $session.structure()
    if (structure) {
      const type = structure.type ? structure.type + ' ' : ''
      $scope.userForm.structure = type + structure.nom
    }

    $eleve.fetchClasse(user.oid, ['nom'], (error, data) => {
      if (error) return // erreur gérée par $eleve.fetchClasse
      $scope.userForm.classe = data.nom
    })
  }

  $scope.errors = function (field, index) {
    if (field === _error.field && index === _error.index) return _error.message
  }

  $scope.resetError = function () {
    _error = {}
  }

  $scope.doUpdate = function (params) {
    if ($scope.userForm.mail) {
      user.mail = $scope.userForm.mail
    } else {
      user.mail = undefined
    }

    user.bilanWithSymbols = $scope.userForm.bilanWithSymbols
    user.password = $scope.userForm.password
    user.passwordBis = $scope.userForm.passwordBis
    user.passwordCurrent = $scope.userForm.passwordCurrent

    $eleve.update(user, (error, utilisateur) => {
      if (error) return // erreur gérée par $eleve.update
      $session.updateUtilisateur(utilisateur)
      $scope.updated = true
      $scope.resetError()
      initialize()
    })
  }

  $scope.checkEmail = function () {
    if (!$scope.userForm.mail) {
      return true
    }

    return isEmail.validate($scope.userForm.mail)
  }

  $scope.checkPassword = function () {
    if (!$scope.userForm.password) {
      return true
    }

    return $scope.userForm.password && $scope.userForm.passwordBis &&
          ($scope.userForm.password === $scope.userForm.passwordBis)
  }

  function isSSOAccount () {
    return user && user.externalMech
  }

  $scope.isFormValid = function () {
    if (isSSOAccount()) {
      return true
    }

    return $scope.userForm.passwordCurrent && this.checkPassword() && this.checkEmail()
  }

  $scope.isSSOAccount = isSSOAccount

  initialize()
}
