app.directive('slAccount', function () {
  return {
    scope: {},
    replace: true,
    template: require('./template.html'),
    controller: require('./controller.js')
  }
})
