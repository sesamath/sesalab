app.directive('slSeance', function () {
  return {
    replace: true,
    scope: {
      seance: '=',
      open: '=',
      onOpen: '&'
    },
    template: require('./template.html'),
    controller: require('./controller'),
    link: function (scope, element, attributes) {
      scope.hasBilan = attributes.bilan !== 'false'
    }
  }
})
