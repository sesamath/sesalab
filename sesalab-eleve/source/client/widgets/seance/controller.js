const log = require('sesajstools/utils/log')

// Useful constants to compute time left
const _second = 1000
const _minute = _second * 60
const _hour = _minute * 60
const _day = _hour * 24

/**
 * Retourne la différence entre deux date sous la forme `${x} j ${y} h ${z} min` (sans les jours si x=0)
 * @private
 * @param {Date} date1 peut être avant ou après date2, le résultat sera le même si on inverse
 * @param {Date} date2
 * @return {string}
 */
function getDelayString (date1, date2) {
  const diff = Math.abs(date2 - date1)

  const days = Math.floor(diff / _day)
  // if (days > 7) return getFormattedDate(date2)
  if (days > 1) return `${days} j`
  const dayStr = days > 0 ? '1j ' : ''
  const hours = Math.floor((diff % _day) / _hour)
  const minutes = Math.round((diff % _hour) / _minute)

  return `${dayStr}${hours} h ${minutes} min`
}

/**
 * Retourne dd/mm/yy
 * @private
 * @param {Date} date
 * @return {string}
 */
function getFormattedDate (date) {
  try {
    return new Intl.DateTimeFormat('fr-FR').format(date)
  } catch (error) {
    // c'est moche mais tant pis, ça devrait jamais arriver
    return date.toString()
  }
}

module.exports = function ($rootScope, $scope, $api, $errors, $session, $toaster) {
  'ngInject'

  $scope.delayMessage = ''
  $scope.deadDelayMessage = ''

  function initialize () {
    // clear non-zapping
    $scope.$on('seance::finished', function () {
      log.debug('seance::finished dans widget seance')
      $scope.running = false
      // Reset non-zapping
      $rootScope.nonZapping = 0
      $rootScope.startedAt = null
    })

    // init seance scope vars
    $scope.isFormateur = $session.isFormateur()
    $scope.running = false

    const debut = $scope.seance.dateDebut
    const fin = $scope.seance.dateFin
    if (debut && fin) {
      const today = new Date()
      if (debut < today && fin > today) {
        if (fin - today > 7 * _day) $scope.delayMessage = `Fin le ${getFormattedDate(fin)}`
        else $scope.delayMessage = `Fin dans ${getDelayString(today, fin)}`
      } else if (debut > today) {
        // pas encore ouvert
        if (debut - today > 7 * _day) $scope.deadDelayMessage = `Pas ouvert avant le ${getFormattedDate(debut)}`
        else $scope.deadDelayMessage = `Pas ouvert avant ${getDelayString(today, debut)}`
      } else {
        // trop tard (fin < today)
        if (today - fin > 7 * _day) $scope.deadDelayMessage = `Terminé depuis le ${getFormattedDate(fin)}`
        else $scope.deadDelayMessage = `Terminé depuis ${getDelayString(today, fin)}`
      }
    } else if (debut || fin) {
      console.error(Error('seance avec début ou fin mais pas les deux'), $scope.seance)
    }
    // log.debug('initialized scope widget seance', $scope)
  }

  $scope.doShowBilan = function (seance) {
    if ($scope.running) {
      $toaster.pop('Le bilan sera accessible une fois l’exercice terminé', 'error')
      return
    }
    $rootScope.$broadcast('seance::show-bilan', seance)
  }

  /**
   * Lance le chargement de la ressource (et start une séance)
   * @param seance
   * @param ressource
   */
  $scope.doStart = function (seance, exercice) {
    if ($session.isFormateur()) {
      seance.start(exercice)
      return
    }

    if (exercice.isDisabled()) return

    if ($rootScope.nonZapping && $rootScope.startedAt) {
      const delay = Math.round((Date.now() - $rootScope.startedAt) / 1000)
      // console.log(`start exercice avec dernier exercice ouvert depuis ${delay}s et un non-zapping de ${$rootScope.nonZapping}s`)
      if (delay < $rootScope.nonZapping) {
        $toaster.pop(`Vous devez attendre encore ${$rootScope.nonZapping - delay}s avant de passer à un autre exercice.`, 'error')
        return
      }
    }

    // ok, on lance
    $scope.running = true
    seance.start(exercice)
  }

  initialize()
}
