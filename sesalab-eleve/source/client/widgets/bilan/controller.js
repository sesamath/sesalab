const _ = require('lodash')

module.exports = function ($exercice, $seance, $scope, $formatter, $popup, $rootScope, $toaster, $session, $errors) {
  'ngInject'

  function initialize () {
    $scope.$watch('seance', (value) => {
      if (value) update()
    })
  }

  function update () {
    // Indique si l'un des utilisateurs est daltonien
    const users = $session.utilisateurs()
    const isDaltonien = _.some(users, 'bilanWithSymbols')

    function callback (error, response) {
      $scope.lignesBilan = []
      if (error || !response || !response.resultats || !response.resultats.length) {
        // Pour les séquences fermées on a pas ça dans notre scope, et elles peuvent ne pas avoir de résultat
        if ($scope.$parent.doShowDefaultView) $scope.$parent.doShowDefaultView()
        return
      }

      // On construit un objet avec comme clé l'oid du meilleur résultat d'une ressource donnée (valeur true)
      const bestResults = {}
      const groupedResultats = _.groupBy(response.resultats, 'rid')
      _.each(groupedResultats, (resultats) => {
        const bestResultat = _.max(resultats, 'score')
        bestResults[bestResultat.oid] = true
      })

      // @TODO faire un revert du commit 441b0580 après le 27/07/2026 (ainsi que du commit suivant qui a ajouté cette ligne)
      // (rustine présente aussi dans sesalab-eleve/source/client/views/formateur/bilan-liste/controller.js)
      // il y a eu un bug dans l'envoi des résultats j3p entre le 24/01/2025 17:17
      // et le 27/01/2025 20:48 (on a un score mais sans savoir sur quel
      // nœud il porte).
      // => ça affiche systématiquement `pas de score pour ce parcours (mis à 100% pour permettre le passage au suivant en cas de minimum de réussite exigé)` pour le score et `Pas d’information enregistrée` pour la réponse
      // on essaie d'améliorer ça lorsque le graphe ne contenait qu'un seul nœud (la propriété score est forcément celle de ce nœud)
      // rustine1
      const onNodeGraphs = new Set()
      // fin rustine1

      // Les résultats renvoyés par l'api doivent être enrichis avec les informations des élèves et des ressources
      _.each(response.resultats, (resultat) => {
        // On va chercher toutes les infos des participants
        const participants = resultat.participants.map((participant) => {
          if (response.eleves[participant]) {
            return response.eleves[participant].nom + ' ' + response.eleves[participant].prenom
          }
          return 'un compte supprimé'
        })

        // rustine2 à virer
        if (resultat.type === 'j3p' && Array.isArray(resultat.contenu?.graphe)) {
          // on a un graphe pour ce résultat, on regarde s'il y a plusieurs nœuds
          const nodes = resultat.contenu.graphe.filter(node => node.length > 2 && !/fin/i.test(node[1]))
          if (nodes.length === 1) {
            onNodeGraphs.add(resultat.rid)
          }
        } // fin rustine2

        // Conversion de la durée
        const time = resultat.duree
        const minutes = Math.floor(time / 60)
        const seconds = time - minutes * 60
        let duree = ''
        if (minutes) duree += minutes + ' min '
        if (seconds) duree += seconds + ' s'

        // à partir de 2025-01-30 on a la propriété essai sur le résultat retourné par l'api
        const essai = resultat.essai
          ? `${resultat.essai}${resultat.essai > 1 ? 'e' : 'er'} essai`
          : ''
        const row = {
          indexRessource: resultat.$indexRessource,
          title: resultat.ressource.titre,
          rid: resultat.ressource.rid,
          $score: resultat.score,
          $type: resultat.ressource.type,
          score: $formatter.score(resultat.ressource.type, resultat),
          reponse: $formatter.response(resultat.ressource.type, resultat, isDaltonien),
          fullResponse: $formatter.fullResponse(resultat.ressource.type, resultat),
          $date: resultat.$date,
          date: resultat.date,
          duree,
          hasMoreThanOneParticipant: participants.length >= 2,
          participants: 'Fait par ' + participants.join(' et '),
          isBest: bestResults && bestResults[resultat.oid],
          essai
        }
        $scope.lignesBilan.push(row)
      })

      // rustine3: on refait une passe pour tenter de corriger ce qui peut l'être
      for (const row of $scope.lignesBilan) {
        if (/^2[4-7]\/01\/2025/.test(row.date) && /pas de score/.test(row.score) && typeof row.$score === 'number') {
          if (onNodeGraphs.has(row.rid)) {
            row.score = String(row.$score)
            if (/Pas d’information enregistrée/.test(row.reponse)) {
              row.reponse = `${Math.round(row.$score * 100)}%`
            }
            row.fullResponse = ''
          } else {
            // on a un score mais on ne sait pas sur quel nœud
            // => on laisse la phrase pas de score… sur le score mais on donne l'info qu'on a dans la réponse
            if (/Pas d’information enregistrée/.test(row.reponse)) {
              row.reponse = `${Math.round(row.$score * 100)}% sur un nœud inconnu`
            }
          }
        }
      } // fin rustine3
    }

    // appelle l'api pour récupérer les résultats et les passer à callback
    if ($scope.seance.sequenceOid) {
      $scope.autonomie = false
      $seance.resultats($scope.seance.sequenceOid, callback)
    } else {
      $scope.autonomie = true
      $seance.resultatsAutonomie(callback)
    }
  }

  $scope.doShowFullResponse = function (row) {
    $scope.currentRow = row
    const popup = $popup.show({
      scope: $scope,
      height: '90%',
      width: '90%',
      template: require('./popup-response.html'),
      buttons: [
        { caption: 'Fermer', callback: function () { popup.close() } }
      ]
    })
  }

  $scope.linkToRessource = function (row) {
    const exercice = $exercice.create({
      type: row.$type,
      titre: row.title,
      rid: row.rid
    })
    $rootScope.$broadcast('exercice::selected', exercice)
  }

  initialize()
}
