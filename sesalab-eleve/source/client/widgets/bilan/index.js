app.directive('slBilan', function () {
  return {
    scope: {
      seance: '=',
      user: '='
    },
    replace: true,
    template: require('./template.html'),
    controller: require('./controller.js')
  }
})
