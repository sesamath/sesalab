app.service('$toaster', function ($rootScope) {
  'ngInject'

  function pop (body, type, time) {
    setTimeout(function () {
      $rootScope.$emit('toaster::newToast', { type, body, time })
    })
  }

  function clear () {
    $rootScope.$emit('toaster::clearToasts')
  }

  return {
    pop,
    clear
  }
})

app.directive('toasterContainer', function ($rootScope, $timeout, $interval) {
  'ngInject'

  return {
    replace: true,
    restrict: 'E',
    scope: true,

    link: function (scope) {
      let id = 0

      function delayToast (toast, time) {
        if (time > 0) {
          toast.timeout = $interval(function () {
            scope.removeToast(toast.id)
          }, time)
        }
        // sinon ça reste à l'écran jusqu'à ce que l'utilisateur le ferme
      }

      scope.configureTimer = function configureTimer (toast) {
        const time = toast.time ? toast.time : 3000
        delayToast(toast, time)
      }

      function addToast (toast) {
        toast.id = ++id
        toast.body = toast.body.replace(/\n/g, '<br>')
        // pourquoi faut lancer ça dans 500ms ???
        $timeout(function () {
          scope.configureTimer(toast)
          scope.toasts.unshift(toast)
        }, 500)
      }

      scope.toasts = []
      $rootScope.$on('toaster::newToast', function (event, toast) {
        addToast(toast)
      })

      $rootScope.$on('toaster::clearToasts', function () {
        scope.toasts.splice(0, scope.toasts.length)
      })
    },

    controller: function ($scope) {
      'ngInject'

      $scope.stopTimer = function (toast) {
        if (toast.timeout) {
          $interval.cancel(toast.timeout)
          toast.timeout = null
        }
      }

      $scope.restartTimer = function (toast) {
        if (!toast.timeout) { $scope.configureTimer(toast) }
      }

      $scope.removeToast = function (id) {
        let i = 0
        for (i; i < $scope.toasts.length; i++) {
          if ($scope.toasts[i].id === id) { break }
        }
        $scope.toasts.splice(i, 1)
      }

      $scope.click = function (toast) {
        $scope.removeToast(toast.id)
      }
    },

    template: require('./template.html')
  }
})
