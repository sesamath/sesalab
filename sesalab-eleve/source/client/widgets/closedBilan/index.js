app.directive('slClosedBilan', function () {
  return {
    scope: {
      // le nom de l'attribut dans le tag <sl-closed-bilan>, camelCase marche pas
      inactives: '='
    },
    replace: true,
    template: require('./template.html')
  }
})
