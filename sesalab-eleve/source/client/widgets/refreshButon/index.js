app.directive('refreshButton', function () {
  return {
    scope: {
      refresh: '='
    },
    replace: true,
    template: require('./template.html'),
    controller: require('./controller.js')
  }
})
