module.exports = function ($scope, $toaster) {
  'ngInject'

  const DELAY = 60000
  let delay = DELAY

  function diminueTimer () {
    if (delay >= 1000) {
      delay -= 1000
      setTimeout(diminueTimer, 1000)
    } else {
      delay = 0
    }
  }

  $scope.onRefresh = function () {
    if (delay <= 0) {
      delay = DELAY
      $scope.refresh()
      setTimeout(diminueTimer, 1000)
    } else {
      $toaster.pop('Prochain rafraîchissement possible dans ' + delay / 1000 + ' secondes.', 'error')
    }
  }

  setTimeout(diminueTimer, 1000)
}
