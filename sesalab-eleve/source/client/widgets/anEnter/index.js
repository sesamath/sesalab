app.directive('anEnter', function ($parse) {
  'ngInject'

  return {
    restrict: 'A',
    scope: false,
    link: function (scope, element, attributes) {
      const callback = $parse(attributes.anEnter)
      function validate () {
        scope.$apply(function () {
          const value = element.val()
          if (!value || typeof value !== 'string') return
          if (value.trim() === '') return
          callback(scope)
        })
      }
      element.on('keydown', function (event) {
        if (event.which === 13) validate()
      })
    }
  }
})
