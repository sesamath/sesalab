app.directive('slExercice', function () {
  return {
    scope: {
      save: '='
    },
    replace: true,
    template: require('./template.html'),
    controller: require('./controller.js')
  }
})
