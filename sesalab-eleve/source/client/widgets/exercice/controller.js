const _ = require('lodash')
const log = require('sesajstools/utils/log')

const { getBaseId } = require('sesatheque-client/dist/server/sesatheques')

const resultChecksum = require('sesalab-commun/resultChecksum')
const { notify } = require('sesalab-commun/notify')

module.exports = function ($api, $eleve, $errors, $scope, $session, $rootScope, $toaster) {
  'ngInject'

  /**
   * Le listener sur le message venant de l'iframe de l'exercice
   * @private
   * @param event
   */
  function resultatListener (event) {
    if (event?.data?.action !== 'sesalab::result') return
    log.debug('résultat reçu en postMessage', event.data)
    const resultat = event.data.result
    if (!resultat) {
      return $errors.sendEventError(event, Error('Résultat absent'))
    }
    if (!resultat.resultatId) {
      return $errors.sendEventError(event, Error('Résultat sans identifiant'), { resultat })
    }
    if (typeof resultat.resultatId !== 'string') {
      notify(Error('resultatId n’est pas une string'), { resultat })
      resultat.resultatId = String(resultat.resultatId)
    }
    const resultatId = resultat.resultatId

    const exercice = exosByResultatId[resultatId]
    if (!exercice?.$displayUrl) {
      const error = Error('Données du résultat corrompues')
      // notif pour être au courant et vérifier que ce n'est pas un bug dans le code
      $errors.sendEventError(event, error, { exercice, exosByResultatId, resultatId, message: 'pas de exosByResultatId[resultatId].$displayUrl' })
      // feedback élève
      $toaster.pop(error.message, 'error')
      return
    }
    // On ne traite que les résultats envoyés par l'iframe de la sésathèque (anti-triche)
    // mais attention, un rid sur une sesathèque peut rediriger sur une autre
    // (en cas d'alias, et on a pas la propriété aliasRid pour le savoir)
    if (exercice.$displayUrl.indexOf(event.origin) !== 0) {
      // on regarde si cette origine est une sesathèque existante
      let baseUrl = event.origin
      if (!baseUrl.endsWith('/')) baseUrl += '/'
      if (getBaseId(baseUrl)) {
        // c'est une sésathèque connue, mais pas celle de départ, on rectifie le rid dans le résultat reçu
        if (resultat.rid !== exercice.rid) {
          log.debug(`resultat.rid (${resultat.rid}) ≠ exercice.rid (${exercice.rid}), probablement un alias… On avait displayUrl = ${exercice.$displayUrl} et le résultat vient de ${event.origin}`)
          resultat.rid = exercice.rid
        }
      } else {
        $errors.sendEventError(event, Error(`invalid origin ${event.origin}`), { exercice })
        $toaster.pop('Résultat reçu invalide', 'error')
        return
      }
    }

    // Nous regardons si cet exercice est lié à une séance ou si c'est un exercice en autonomie
    const sequenceOid = exercice.parent ? exercice.parent.parent.sequenceOid : null
    const oidsParticipants = _.map($session.utilisateurs(), 'oid')
    const checksum = resultChecksum(exercice.rid, oidsParticipants, resultat.score)
    const resultatForExercice = _.assign({}, resultat, {
      checksum,
      sequence: sequenceOid,
      participants: oidsParticipants,
      resultatId,
      ressource: {
        rid: exercice.rid, // déjà dans resultat.rid
        type: exercice.type, // déjà dans resultat.type
        titre: exercice.titre
      }
    })

    $eleve.sauvegardeResultat(resultatForExercice, (error, state) => {
      if (error) {
        // sauvegardeResultat a déjà notifié l'utilisateur, on notify bugsnag, sauf userFriendly (session expirée, déjà affiché à l'élève)
        if (!error.userFriendly) {
          const { parent, rid, titre, type } = exercice
          $errors.sendEventError(event, error, {
            exercice: { nomParent: parent?.nom ?? '', rid, titre, type },
            response: error.response,
            resultat: resultatForExercice
          })
        }
        return
      }
      // màj des infos séance / exo (sauf en autonomie)
      if (sequenceOid) {
        if (exercice.rid === state?.rid) {
          // màj séance
          const { fin, nbEssais, pourcentageSucces } = state
          Object.assign(exercice, { fin, nbEssais, pourcentageSucces })
        } else {
          notify(Error('réponse incohérente de sauvegardeResultat'), { exercice, state })
        }
      }
      // on active éventuellement l'exo suivant
      // (en cas de min de réussite ça vérifie si la réussite de cet exo est suffisante)
      if (resultatForExercice.fin && exercice.parent && exercice.isCompleted()) {
        exercice.parent.nextSerieIfCompleted(exercice)
      }
    })
  }

  /**
   * Normalise exercice avant de le passer à loadExercice
   * @private
   * @param event
   * @param exercice
   */
  function prepareExercice (event, exercice) {
    try {
      if (!exercice) throw Error('Pas de ressource pour cette sélection')

      if (exercice.$displayUrl) {
        loadExercice(exercice)
      } else if (exercice.type === 'error') {
        // on ne fait rien au select dans ce cas, mais ça devrait pas arriver jusqu'à l'élève
        $errors.notify(Error('ressource de type error coté élève'), exercice)
      } else if (exercice.rid) {
        // Note: normalize() ajoute des propriétés à la ressource, notamment $displayUrl
        const normalizedRessource = app.sesatheque.normalize(exercice)
        // On n'écrase pas notre objet exercice car il est "connecté" au scope angular
        exercice.$displayUrl = normalizedRessource.$displayUrl
        loadExercice(exercice)
      } else {
        throw Error('ressource invalide, rien pour trouver son $displayUrl')
      }
    } catch (error) {
      $toaster.pop('Désolé, cette ressource a un problème et ne pourra pas être affichée…')
      $errors.notify(error, { event, exercice })
    }
  }

  /**
   * Affiche une ressource dans l'iframe
   * @private
   * @param exercice
   */
  function loadExercice (exercice) {
    // à chaque chargement on prend un timestamp comme id, qui sera renvoyé avec les résultats
    const resultatId = String(Date.now())
    exosByResultatId[resultatId] = exercice

    // On incrémente nb d'essais pour avoir un visuel côté client, l'incrémentation réelle se fait côté serveur sur api/seance/start
    const sequenceOid = exercice.parent ? exercice.parent.parent.sequenceOid : 0

    let iframeUrl = exercice.$displayUrl +
      (exercice.$displayUrl.includes('?') ? '&' : '?') +
      'verbose=0' +
      '&showTitle=0' +
      '&resultatId=' + resultatId +
      '&loadedMessageAction=sesalab::ressourceLoaded'

    if ($scope.save) {
      // ajout de resultatMessageAction et lastResultUrl
      const lastResultUrl = encodeURIComponent(
        app.settings.baseUrl +
        'api/resultat/last?token=' + $session.readonlyToken() +
        '&sequence=' + sequenceOid +
        '&ressource=' + exercice.rid
      )
      iframeUrl += '&resultatMessageAction=sesalab::result&lastResultUrl=' + lastResultUrl
    }
    // Attention, il faut que la vue courante soit VIEWS.EXERCICE sinon l'iframe est masquée
    // et lancer l'affichage dans un loadListener ne fonctionnerait pas car il ne serait jamais appelé
    // C'est un listener exercice::selected qui fait du $scope.openedView = VIEWS.EXERCICE
    // puis broadcast exercice::load, dont le listener nous appelle
    // Le chargement de cet exercice pourrait provoquer l'envoi d'un résultat au unload du précédent
    // d'où le about:blank mis en src par le listener exercice::selected avant de lancer son event
    const tagIframe = document.getElementById('exercice-frame')

    // on ajoute et retire un listener sur le postMessage ici pour chaque exercice,
    // pour garder une ref à l'exo courant
    const ressourceLoadedListener = (event) => {
      if (!event.data || event.data.action !== 'sesalab::ressourceLoaded') return
      window.removeEventListener('message', ressourceLoadedListener)
      log.debug('message ressourceLoaded', event.data.rid)
      if (sequenceOid) {
        // init timer non-zapping
        $rootScope.nonZapping = exercice.nonZapping // en s
        $rootScope.startedAt = Date.now() // ms

        const data = {
          sequenceOid,
          selectedRessourceRid: exercice.rid
        }
        $api
          .post('seance/start', data)
          .then((resp) => {
            if (typeof resp?.nbEssais !== 'number') {
              console.error(Error('nbEssais invalide dans la réponse à seance/start'), resp)
            }
            // màj du nb d'essais d'après la réponse, pour rester sync avec la db
            exercice.nbEssais = resp?.nbEssais ?? 1
            $rootScope.$broadcast('exercice::started', exercice)
          })
          .catch($errors.handle('Une erreur s’est produite pendant le chargement de la séance'))
      } else {
        // autonomie, pas d'init de séance
        $rootScope.$broadcast('exercice::started', exercice)
      }
    }
    window.addEventListener('message', ressourceLoadedListener)

    // et on peut charger l'iframe
    tagIframe.src = iframeUrl
    // ici on a le tag iframe qui est dans un
    // <div class="sl-exercice ng-isolate-scope ng-hide" ng-show="openedView === VIEWS.EXERCICE" save="true">
  }

  // init
  // on stocke des infos qu'on ajoutera aux résultats reçus, par rid de ressource
  // reste un pb si le même rid apparaît deux fois dans la même séquence
  // ET que les exercices sont lancés l'un après l'autre
  // => dans ce cas le unload du 1er peut déclencher un message qui arriverait après l'init avec les infos du 2e
  const exosByResultatId = {}
  // Chaque lancement d'exercice a un identifiant unique qui permet d'avoir un unique score
  // pour ce lancement (la ressource pouvant envoyer plusieurs résultats au fil de l'exercice)
  window.addEventListener('message', resultatListener)
  $scope.$on('exercice::load', prepareExercice)
}
