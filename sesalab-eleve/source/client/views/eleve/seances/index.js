app.config(function ($stateProvider) {
  'ngInject'

  $stateProvider.state({
    name: 'seances',
    url: '',
    template: require('./template.html'),
    controller: require('./controller')
  })
})

app.directive('recursive', function ($compile) {
  'ngInject'

  return {
    restrict: 'EACM',
    priority: 100000,
    compile: (tElement, tAttr) => {
      const contents = tElement.contents().remove()
      let compiledContents
      return (scope, iElement, iAttr) => {
        if (!compiledContents) {
          compiledContents = $compile(contents)
        }
        iElement.append(compiledContents(scope, (clone) => { return clone }))
      }
    }
  }
})

// définition du composant angular ressource-tree utilisé dans les templates
app.directive('ressourceTree', function ($rootScope, $timeout, $errors) {
  'ngInject'

  return {
    restrict: 'E',
    scope: {
      // root peut prendre 3 valeurs =|&|@
      // @see https://stackoverflow.com/a/28310787
      // = is used in directive isolate scope to enable two way binding
      //   The model in parent scope is linked to the model in the directive's isolated scope.
      //   Changes to one model affects the other, and vice versa.
      // @ does not updates model, only updates Directive scope values
      //   These strings support {{}} expressions for interpolated values.
      //   For example: The interpolated expression is evaluated against directive's parent scope.
      // & binding is for passing a method into your directive's scope
      //   so that it can be called within your directive.
      //   The method is pre-bound to the directive's parent scope, and supports arguments.
      //   For example if the method is hello(name) in parent scope, then in order to execute
      //   the method from inside your directive, you must call $scope.hello({name:'world'})
      // @see https://stackoverflow.com/questions/14908133/what-is-the-difference-between-vs-and-in-angularjs
      root: '='
    },
    template: require('./leaf.html'),
    link: function (scope, element, attribute) {
      scope.root.$enfants = []
      scope.doToggle = function () {
        if (scope.root.type !== 'arbre') {
          $rootScope.$broadcast('exercice::selected', scope.root)
          return
        }
        if (!scope.root.$enfants.length) {
          app.sesatheque.getEnfants(scope.root, (error, response) => {
            if (!error && !response) {
              error = new Error('app.sesatheque.getEnfants response vide')
            }
            if (error) {
              $errors.handle('Une erreur s’est produite lors de la récupération d’une ressource')(error)
              return
            }

            scope.root.$enfants = response
            scope.root.$enfants.forEach((e) => {
              e.$leaf = e.type !== 'arbre'
            })
            $timeout(function () {
              $rootScope.$apply()
            })
          })
        }
        scope.root.$open = !scope.root.$open
      }
    }
  }
})
