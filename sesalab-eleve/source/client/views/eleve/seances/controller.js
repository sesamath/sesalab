const _ = require('lodash')
const flow = require('an-flow')
const log = require('sesajstools/utils/log')

module.exports = function ($app, $eleve, $scope, $session, $seance, $rootScope, $toaster, $document, $popup, $errors, SequenceRegistry) {
  'ngInject'

  function doToggleMenu () {
    $scope.showPanel = !$scope.showPanel
    if ($scope.showPanel) {
      const width = $scope.lastResizerPosition > 100 ? $scope.lastResizerPosition : SIDEBAR_WIDTH
      setSidebarWidth(width)
    } else {
      $scope.lastResizerPosition = $scope.resizerPosition
      setSidebarWidth(0)
    }
  }

  function initialize () {
    log.debug('initialize seances')
    refresh()

    $scope.doShowDefaultView()

    $scope.$on('seance::finished', () => {
      log.debug('seance::finished dans seances')
      $scope.exerciceInSequenceRunning = undefined
      $scope.currentRid = undefined
    })

    // ce controller n'est pas appelé en cas de test par un formateur
    // il faut remettre ce listener dans sesalab-eleve/source/client/views/formateur/test-sequence/controller.js
    $scope.$on('exercice::selected', (event, exercice) => {
      // on ne fait rien si on clique sur l'exercice déjà ouvert (avant ça le redémarrait)
      if (exercice.rid === $scope.currentRid) return
      // on provoque le déchargement de la ressource courante pour qu'elle envoie
      // son dernier résultat au unload
      log.debug('unload forced on exercice::selected')
      $scope.openedView = VIEWS.EXERCICE
      // c'est standard et va provoquer le unload du src précédent si y'en avait un
      // https://www.w3.org/TR/html5/embedded-content-0.html#the-iframe-element
      document.getElementById('exercice-frame').src = 'about:blank'
      // et on charge le suivant
      $scope.$broadcast('exercice::load', exercice)
    })

    $scope.$on('exercice::started', (event, exercice) => {
      log.debug('exercice::started event', event)
      log.debug('exercice::started exercice', exercice)
      // Puisqu'on commence une nouvelle ressource, les bilans vont être modifiés donc on s'assure que le watcher
      // côté bilan va détecter une nouvelle valeur
      $scope.bilan = undefined
      $scope.currentRid = exercice.rid
      // Si la ressource a un parent on est dans une séquence (!= autonomie)
      $scope.exerciceInSequenceRunning = Boolean(exercice.parent)
    })

    $scope.$on('seance::show-bilan', (event, seance) => {
      $scope.bilan = seance
      $scope.openedView = VIEWS.BILAN
    })

    $document.bind('mousemove', mousemove)
    $document.bind('mouseup', () => {
      resizing = false
    })

    $scope.openedSeance = undefined
    // log.debug('initialized scope seances', $scope)
  }

  /**
   * Charge les séquences (les transforme en séances et les met dans le scope)
   * @param {Error} [error]
   * @param {AllSequencesObject} response
   */
  function loadSequences (error, response) {
    if (error) {
      // l'affichage de l'erreur a déjà été fait par $errors.handle
      $scope.seancesState = STATE.ERROR
      return
    }
    // on génère les séances actives, et on les trie par formateur en même temps
    const sequencesRegistry = new SequenceRegistry(response.sequencesActives)
    const formateursByOid = response.formateursByOid
    const activesByFormateur = {}
    // inconnu si l'élève a récupéré une séquence d'un prof qui n'est plus en base
    // ça ne devrait jamais arriver mais on a quand même trouvé des sequences avec owner disparu…
    const unknown = { oid: '', nom: 'inconnu', prenom: '' }
    response.sequencesActives.forEach((sequence) => {
      const foid = sequence.owner
      const seance = $seance.create(sequence, sequencesRegistry)
      $scope.seances.actives.push(seance)
      if (!activesByFormateur[foid]) {
        // la propriété formateur
        activesByFormateur[foid] = formateursByOid[foid] || unknown
        // plus un array de séances
        activesByFormateur[foid].seances = []
      }
      activesByFormateur[foid].seances.push(seance)
    })
    // reste à trier les formateurs (activesByFormateur est un objet mais $scope.formateurs est un array
    $scope.formateurs = _.orderBy(activesByFormateur, formateur => formateur.nom.toLowerCase())
    // et les séances de chaque formateur
    $scope.formateurs.forEach(formateur => {
      formateur.seances = _.orderBy(
        formateur.seances,
        [
          'prioritaire', // - d'abord les prioritaires
          s => !!s.dateFin, // - puis les séquences bornées
          s => s.nom.toLowerCase() // - puis par ordre alphabétique de séquence (seance.nom = sequence.nom)
        ],
        ['desc', 'desc', 'asc']
      )
      formateur.label = formateur.prenom[0].toUpperCase() + '. ' + formateur.nom
    })
    $scope.hasFormateur = !!$scope.formateurs.length

    // et les inactives, pour le moment on mélange inactives et old
    response.sequencesInactives.concat(response.sequencesDesaffectees).forEach((sequence) => {
      if (sequence.structure !== $scope.structure.oid) return
      $scope.seances.inactives.push($seance.create(sequence))
    })
    $scope.hasInactiveSeance = $scope.seances.inactives.length > 0
    $scope.seancesState = STATE.LOADED
  }

  function mousemove (event) {
    if (!resizing) return

    hasResizerMoved = true
    setSidebarWidth(event.pageX)
    $scope.$apply()
    event.preventDefault() // Désactive la selection de texte pendant le drag&drop
  }

  function refresh () {
    if (!$session.isActive()) return
    $scope.applicationName = app.settings.name
    $scope.version = app.settings.version
    $scope.structure = $session.structure()
    const hasAutonomie = !$scope.structure.bloquerExerciceAutonomie
    $scope.utilisateurs = $session.utilisateurs()
    $scope.user = $scope.utilisateurs[0]
    $scope.seances = {
      actives: [],
      inactives: []
    }
    $scope.formateurs = []
    $scope.arbres = []
    $scope.seancesState = STATE.LOADING
    $scope.autonomyState = hasAutonomie ? STATE.LOADING : STATE.LOADED

    // Récupération des séquences "classiques"
    $seance.getAllSequences(loadSequences)

    // Récupération des contenus utilisables en autonomie, si c'est autorisé
    if (!hasAutonomie) return
    flow().seq(function () {
      // on récupère l'arbre des exercices réalisable en autonomie
      const rid = app.sesatheque.autonomie.rid
      // un élève n'a pas de token et ne peut pas récupérer de contenus privés => toujours public
      app.sesatheque.getItem(rid, true, this)
    }).seq(function (arbreAutonomie) {
      app.sesatheque.getEnfants(arbreAutonomie, this)
    }).seq(function (enfants) {
      $scope.arbres = enfants
      $scope.autonomyState = STATE.LOADED
    }).catch((error) => {
      $scope.autonomyState = STATE.ERROR
      $toaster.pop('Impossible de récupérer la liste des exercices en autonomie pour le moment', 'error')
      console.error(error)
    })
  }

  /**
   * Redimensionne le menu de gauche.
   * @param {Number} size Un entier
   */
  function setSidebarWidth (size) {
    $scope.resizerPosition = size
    $scope.seancesWidth = size
    $scope.contentLeft = size + SEPARATOR_WIDTH
  }

  let resizing = false
  let hasResizerMoved = false
  const SIDEBAR_WIDTH = 350
  const SEPARATOR_WIDTH = 10

  const VIEWS = {
    ACCOUNT: 'ACCOUNT',
    BILAN: 'BILAN',
    CLOSEDBILAN: 'CLOSEDBILAN',
    DEFAULT: 'DEFAULT',
    EXERCICE: 'EXERCICE'
  }

  const STATE = {
    LOADING: 'LOADING',
    LOADED: 'LOADED',
    ERROR: 'ERROR'
  }

  $scope.seancesState = STATE.LOADING
  $scope.autonomyState = STATE.LOADING
  $scope.VIEWS = VIEWS
  $scope.STATE = STATE
  setSidebarWidth(SIDEBAR_WIDTH)
  $scope.showPanel = true

  $scope.openSeance = function (seance) {
    if (seance.isDisabled()) {
      $toaster.pop('Cette séquence sera accessible une fois les séquences prioritaires terminées', 'error')
      return
    }

    if ($scope.exerciceInSequenceRunning) {
      // On ne peut pas ouvrir une autre séquence quand un exercice est en cours,
      // sauf si c'est un exercice en autonomie (auquel cas on passe pas dans ce if)
      $toaster.pop('Cette séquence sera accessible une fois l’exercice terminé', 'error')
      return
    }

    if ($scope.openedSeance && $scope.openedSeance.sequenceOid === seance.sequenceOid) {
      // On ferme la séance ouverte
      $scope.openedSeance = undefined
      return
    }

    // On ajoute les informations utiles au debug
    if (seance.sequenceOid) {
      /* global bugsnagClient */
      bugsnagClient.addMetadata('sequence', { sequenceOid: seance.sequenceOid })
    }

    let uniqEleveDialog
    const closeDialog = () => {
      if (uniqEleveDialog) {
        uniqEleveDialog.close()
        uniqEleveDialog = null
      }
    }

    // Dans le cas d'une connexion multiple, il n'est pas possible de réaliser à plusieurs les exercices d'une séquence à élève unique
    if ($scope.utilisateurs.length > 1 && seance.uniqEleve) {
      uniqEleveDialog = $popup.show({
        scope: $scope,
        cssClass: 'uniq-eleve',
        title: 'Séquence mono utilisateur',
        template: require('./popup-uniq-eleve.html'),
        width: 400,
        height: 150,
        buttons: [{
          caption: 'Oui',
          callback: () => {
            $eleve.relogin($scope.utilisateurs[0], () => {
              closeDialog()
              $session.connect(() => {
                refresh()
                $scope.openedSeance = seance
              })
            })
          }
        }, {
          caption: 'Non',
          callback: closeDialog
        }]
      })

      $scope.openedSeance = undefined
      return
    }

    $scope.openedSeance = seance
  }

  $scope.terminerSeance = function () {
    $scope.$broadcast('seance::finished')
    // On ferme la séance ouverte
    $scope.openedSeance = undefined
    // On ferme aussi l'écran de l'exercice, mais avant on change le src pour forcer le unload
    // (sinon l'envoi du résultat de l'exo abandonné se fait à l'ouverture du prochain,
    // et le feedback "résultat sauvegardé" à l'ouverture de l'exo fait bizarre…)
    document.getElementById('exercice-frame').src = 'about:blank'
    $scope.openedView = VIEWS.DEFAULT
  }

  $scope.splitUsers = function (utilisateurs) {
    const users = utilisateurs.map(user => user.prenom + ' ' + user.nom)
    return users.join(' - ')
  }

  $scope.doShowDefaultView = function () {
    // attention, cette fct est appelée en setTimeout, ne pas utiliser son this
    $scope.openedView = VIEWS.DEFAULT
    $scope.currentRid = undefined
  }

  $scope.doShowBilan = function () {
    // On demande le bilan pour les exercices en autonomie, on crée une séance virtuelle
    const seance = {
      id: null,
      sequenceOid: null,
      autonomie: true
    }
    $rootScope.$broadcast('seance::show-bilan', seance)
  }

  $scope.doShowClosedBilan = function () {
    $scope.openedView = VIEWS.CLOSEDBILAN
  }

  $scope.doShowExerciceAutonomie = function (bool) {
    if (!bool) $toaster.pop('Les exercices en autonomie seront accessibles une fois l’exercice terminé', 'error')
  }

  $scope.doDisplayHelp = function () {
    window.open(app.settings.aide.eleves, '_blank')
  }

  $scope.doAccount = function () {
    if ($scope.exerciceInSequenceRunning) return

    $scope.bilan = null
    $scope.openedView = VIEWS.ACCOUNT
  }

  $scope.doLogout = function () {
    $session.disconnect()
  }

  $scope.mousedown = function (event) {
    // au mousedown en enclenche un possible drag
    resizing = true
    // ça passera à true si ça bouge (dans mousemove)
    hasResizerMoved = false
    event.stopPropagation() // faut pas propager du div au a qu'il contient (mais qui déborde)
  }

  $scope.mouseup = function (event) {
    // si ça n'a pas bougé on prend ça pour un click et on toggle
    if (!hasResizerMoved) doToggleMenu()
    resizing = false
    event.stopPropagation() // faut pas propager du div au a qu'il contient (mais qui déborde)
  }

  $app.ready(initialize)
}
