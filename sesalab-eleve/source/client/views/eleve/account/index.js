app.config(function ($stateProvider) {
  'ngInject'

  $stateProvider.state({
    name: 'password',
    url: '/changement-password/',
    template: require('./template.html'),
    controller: require('./controller')
  })
})
