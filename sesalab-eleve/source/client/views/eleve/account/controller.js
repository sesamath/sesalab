module.exports = function ($app, $eleve, $session, $scope, $state, $window, $popup) {
  'ngInject'

  const _error = {}
  $scope.password = null
  $scope.passwordBis = null
  $scope.isAlone = false
  let passwordBisTimer

  $scope.errors = function (field, index) {
    if (field === _error.field && index === _error.index) return _error.message
  }

  $scope.isFormValid = function () {
    return isPasswordValid()
  }

  $scope.isPasswordValid = isPasswordValid

  function isPasswordValid () {
    return $scope.password && $scope.passwordBis && $scope.password === $scope.passwordBis
  }

  $scope.onKeydown = function () {
    $scope.passwordBisUpdated = false

    if (passwordBisTimer) clearTimeout(passwordBisTimer)
    passwordBisTimer = setTimeout(() => {
      $scope.passwordBisUpdated = true
      $scope.$apply()
    }, 500)
  }

  $scope.onSubmit = function () {
    updatePassword()
  }

  /**
   * Mise à jour côté back.
   */
  function updatePassword () {
    const utilisateur = $session.utilisateurs()[0]
    utilisateur.password = $scope.password
    utilisateur.passwordBis = $scope.passwordBis

    $eleve.update(utilisateur, (error) => {
      if (error) return // La gestion des erreurs est présente en amont (apparition d'un toast)
      $window.location.href = '/eleve/'
    })
  }

  /**
   * Ouverture du pop-up confirmant la création du compte.
   */
  function showPasswordResetPopup () {
    $popup.show({
      scope: $scope,
      title: 'Changement de mot de passe',
      template: require('./popup-account-password-reset.html'),
      width: 600,
      height: 250,
      closeable: false,
      onClose: function () {
        $window.location.href = '/eleve/'
      }
    })
  }

  function showUnableToResetPopup () {
    $popup.show({
      scope: $scope,
      title: 'Changement de mot de passe',
      template: require('./popup-account-password-reset.html'),
      width: 600,
      height: 170,
      closeable: true,
      onClose: function () {
        $window.location.href = '/eleve/'
      },
      buttons: [{
        caption: 'Fermer la session et retourner à l’accueil',
        callback: function () {
          $session.disconnect()
        }
      }]
    })
  }

  function initialize () {
    if ($session.utilisateurs().length === 0) {
      $window.location.href = '/eleve/'
      return
    }

    if (!$session.mustResetPassword()) {
      $state.go('seances')
      return
    }

    const isMultiSession = $session.isMultiSession()
    $scope.isAlone = !isMultiSession
    if (isMultiSession) {
      showUnableToResetPopup()
    } else {
      showPasswordResetPopup()
    }
  }

  // On doit attendre que les settings soient récupérés
  $app.ready(initialize)
}
