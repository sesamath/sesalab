app.config(function ($stateProvider) {
  'ngInject'

  $stateProvider.state({
    name: 'inscription',
    url: '/inscription/',
    template: require('./template.html'),
    controller: require('./controller')
  })
})
