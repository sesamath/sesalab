const _ = require('lodash')
const constants = require('sesalab-commun/constants.js')
const { notify } = require('sesalab-commun/notify')

module.exports = function ($api, $app, $eleve, $scope, $popup, $structure, $toaster, $errors, $q) {
  'ngInject'

  let _error = {}
  let newAccountDialog
  let timer
  let cancelFetch // on l'utilise aussi pour savoir si y'a une requête pending ou pas

  /**
   * Création d'un compte utilisateur à partir des données du scope
   */
  function createNewAccount () {
    // Il est impossible de créer un compte sur une structure ne possédant pas encore de classe
    if (!$scope.structureHasClasses) {
      $toaster.pop('Cette structure n’a pas encore de classe, vous ne pouvez pas y créer de compte', 'info')
      return
    }

    const utilisateur = _.clone($scope.user)
    // Permet de remplacer l'attribut classe hérité du scope pour être conforme à la définition de l'index classe
    utilisateur.classe = utilisateur.classe.oid
    utilisateur.structureCode = $scope.structureCode
    utilisateur.type = constants.TYPE_ELEVE
    $eleve.create(utilisateur, (error, response) => {
      if (error) {
        $toaster.pop(error.response.message, 'info', 10000)
        if (error.response.data && error.response.data.field) _error = error.response.data
        return
      }

      $scope.successMessage = response.message
      showAccountCreatedContainer()
    })
  }

  /**
   * Ouverture du pop-up confirmant la création du compte.
   */
  function showAccountCreatedContainer () {
    if (newAccountDialog) newAccountDialog.close()
    $popup.show({
      scope: $scope,
      title: 'Compte crée avec succès',
      template: require('./popup-account-created.html'),
      width: 400,
      height: 175,
      buttons: [
        {
          caption: 'Retourner à l’accueil',
          callback: function () {
            window.location.href = '/'
          }
        }
      ],
      onClose: function () {
        window.location.href = '/'
      }
    })
  }

  /**
   * Ouverture du formulaire d'inscription
   */
  function showCreateAccountForm () {
    // On ferme la potentiel modal existante
    if (newAccountDialog) newAccountDialog.close()

    newAccountDialog = $popup.show({
      scope: $scope,
      cssClass: 'register',
      title: 'Créer un nouveau compte',
      template: require('./popup-register.html'),
      width: 400,
      height: 400,
      buttons: [
        {
          caption: 'Créer',
          callback: createNewAccount
        },
        {
          caption: 'Annuler',
          callback: function () {
            window.location.href = '/'
          }
        }
      ],
      onClose: function () {
        window.location.href = '/'
      }
    })
  } // initialize

  function resetScope () {
    $scope.user = {}
    // on reset pas $scope.structure.label car c'est ça qu'on veut lire ! (fait dans initialize only)
    $scope.ajoutElevesAuthorises = undefined
    $scope.structureHasClasses = undefined
    $scope.structures = []
    $scope.classes = []
  }

  function initialize () {
    resetScope()
    $scope.structure = { label: '' }

    $scope.structureCode = ''
    $scope.successMessage = undefined
    // rgpd
    $scope.isRgpdChecked = false
    $scope.urlRgpd = app.settings.aide.rgpd
    showCreateAccountForm()
  }

  function fetchStructures () {
    resetScope()
    if (cancelFetch) cancelFetch.reject()

    cancelFetch = $q.defer()
    const options = {
      timeout: cancelFetch
    }

    // cancel après 10s
    setTimeout(() => {
      if (cancelFetch) {
        cancelFetch.reject()
        cancelFetch = undefined
        $toaster.pop('Aucune réponse après 10s, impossible de récupérer la liste des structures (changer un caractère pour relancer)', 'error', 10000)
      }
    }, 10000)

    $structure.getByName($scope.structure.label, options, (error, structures) => {
      timer = undefined
      cancelFetch = undefined
      $toaster.clear()
      if (error) $scope.structures = []
      else $scope.structures = structures
      $scope.structureHasClasses = structures.length !== 0
    })
  }

  $scope.onChange = function () {
    // on veut un nombre minimum de caractères
    const sid = $scope.structure.label
    if (sid.length < constants.MIN_AUTOCOMPLETE_INPUT) {
      $scope.structures = []
      $scope.structureHasClasses = false
      $toaster.clear()
      return
    }

    clearTimeout(timer)
    timer = setTimeout(fetchStructures, 500)
  }

  $scope.doSelectStructure = function (structure) {
    if (!structure?.oid) return notify(Error('aucune structure fournie'))
    // on reset ça en attendant que l'api réponde
    $scope.structureCode = ''
    $scope.structure.label = ''
    $api
      .get('groupes/by-structure/' + structure.oid)
      .then((response) => {
        $scope.classes = _.values(response.groupes)
        $scope.structureHasClasses = false
        if ($scope.classes.length) {
          $scope.structureHasClasses = true
          $scope.user.classe = $scope.classes[0]
        }

        $scope.structureCode = structure.code
        let structureLabel = structure.code + ' - ' + structure.nom
        if (structure.ville) structureLabel += ' (' + structure.ville + ')'
        $scope.structure.label = structureLabel
        $scope.ajoutElevesAuthorises = structure.ajoutElevesAuthorises
        $scope.structureNom = structure.nom
        $scope.structures = []
      })
      .catch($errors.handle('Une erreur s’est produite durant le changement de structure'))
  }

  $scope.errors = function (field, index) {
    if (field === _error.field && index === _error.index) return _error.message
  }

  // On doit attendre que les settings soient récupérés
  $app.ready(initialize)
}
