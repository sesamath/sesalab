const filters = {
  // lui sera éliminé par isFilterEnabled pour un bilan élève
  participants: {
    label: 'Participants',
    id: 'participants',
    stores: [
      { id: 1, name: 'Tous' },
      { id: 2, name: 'ayant participé' },
      { id: 3, name: "n'ayant pas participé" },
      { id: 4, name: 'ayant atteint la dernière ressource' },
      { id: 5, name: "n'ayant pas atteint la dernière ressource" },
      { id: 6, name: "ayant participé mais n'ayant pas atteint la dernière ressource" }
    ]
  },
  dates: {
    id: 'dates',
    label: 'Dates',
    stores: [
      { id: 0, name: "N'importe quand" },
      { id: 1, name: "Aujourd'hui" },
      { id: 2, name: 'Hier' },
      { id: 3, name: 'Moins de 3 jours' },
      { id: 4, name: 'Plus de 3 jours' }
    ]
  },
  // typeScore seulement pour la vue table
  typeScore: {
    id: 'typeScore',
    label: 'Affichage du score',
    stores: [
      { name: 'Score en % avec le nombre d’essais', withEssais: true, withPc: true },
      { name: 'Score en % sans le nombre d’essais', withPc: true },
      { name: 'Score numérique (sans le nombre d’essais, séparateur virgule)', num: true, withComma: true },
      { name: 'Score numérique (sans le nombre d’essais, séparateur point)', num: true },
      { name: 'Note sur 20 avec le nombre d’essais', withEssais: true, note20: true },
      { name: 'Note sur 20 sans le nombre d’essais', note20: true },
      { name: 'Note sur 20 numérique (sans /20 ni le nombre d’essais)', num: true, note20: true },
      { name: 'Note sur 20 numérique (au dixième, séparateur virgule)', num: true, note20: true, decimal: true, withComma: true },
      { name: 'Note sur 20 numérique (au dixième, séparateur point)', num: true, note20: true, decimal: true },
      { name: 'Note sur 10 avec le nombre d’essais', withEssais: true, note10: true },
      { name: 'Note sur 10 sans le nombre d’essais', note10: true },
      { name: 'Note sur 10 numérique (sans /10 ni le nombre d’essais)', num: true, note10: true },
      { name: 'Note sur 10 numérique (au dixième, séparateur virgule)', num: true, note10: true, decimal: true, withComma: true },
      { name: 'Note sur 10 numérique (au dixième, séparateur point)', num: true, note10: true, decimal: true }
    ]
  }
}
// ajoute les id automatiquement à typeScore
let i = 1
for (const store of filters.typeScore.stores) {
  store.id = i++
}

/**
 * Retourne la clé du filtre pour localStorage
 * @param {Object} filter
 * @returns {string}
 */
const getCurrentFilterKey = (filter) => `filter-${filter.id}-currentId`

function initFilters () {
  // init de current pour chaque filter
  for (const filter of Object.values(filters)) {
    let current
    try {
      const memo = localStorage.getItem(getCurrentFilterKey(filter))
      if (memo) {
        const id = Number.parseInt(memo, 10)
        current = filter.stores.find((store) => store.id === id)
      }
    } catch (error) {
      console.error(error)
    }
    filter.current = current ?? filter.stores[0]
  }
}

// au changement de la valeur d'un filtre on mémorise dans localStorage
function filterChange (filter) {
  try {
    if (!filter.stores[filter.current.id]) {
      return console.error(Error('filter invalide'), filter)
    }
    localStorage.setItem(getCurrentFilterKey(filter), filter.current.id)
  } catch (error) {
    console.error(error)
  }
}

function days (date) {
  const t0 = new Date().setHours(0, 0, 0, 0)
  const t1 = new Date(date).setHours(0, 0, 0, 0)
  return (t0 - t1) / 86_400_000
}

function eleveFilter ($scope, data) {
  if ($scope.elevesDeLaSequenceOnly?.value && !data.dansLaSequence) return false
  const rows = Array.isArray(data)
    ? data // table
    : data.rows // liste

  if ($scope.elevesFilteredByDate.value && $scope.filters.dates.current.id) {
    return rows.some(row => $scope.rowFilter(row))
  }

  switch ($scope.filters.participants.current.id) {
    case 1: return true
    case 2: return !!rows.length
    case 3: return !rows.length
    case 4: return data.reachLast
    case 5: return !data.reachLast
    case 6: return !data.reachLast && !!rows.length
  }
  return false
}

function rowFilter ($scope, data) {
  switch ($scope.filters.dates.current.id) {
    case 0: return true
    case 1: return days(data.$date) === 0
    case 2: return days(data.$date) === 1
    case 3: return days(data.$date) <= 3
    case 4: return days(data.$date) > 3
  }
  return false
}

module.exports = {
  eleveFilter,
  filterChange,
  filters,
  initFilters,
  rowFilter
}
