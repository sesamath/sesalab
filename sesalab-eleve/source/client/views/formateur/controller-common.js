const filters = {
  // lui sera éliminé par isFilterEnabled pour un bilan élève
  participants: {
    label: 'Participants',
    id: 'participants',
    stores: [
      { id: 1, name: 'Tous' },
      { id: 2, name: 'ayant participé' },
      { id: 3, name: "n'ayant pas participé" },
      { id: 4, name: 'ayant atteint la dernière ressource' },
      { id: 5, name: "n'ayant pas atteint la dernière ressource" },
      { id: 6, name: "ayant participé mais n'ayant pas atteint la dernière ressource" }
    ]
  },
  dates: {
    id: 'dates',
    label: 'Dates',
    stores: [
      { id: 0, name: "N'importe quand" },
      { id: 1, name: "Aujourd'hui" },
      { id: 2, name: 'Hier' },
      { id: 3, name: 'Moins de 3 jours' },
      { id: 4, name: 'Plus de 3 jours' }
    ]
  },
  // typeScore seulement pour la vue table
  typeScore: {
    id: 'typeScore',
    label: 'Affichage du score',
    stores: [
      { name: 'Score en % avec le nombre d’essais', withEssais: true, withPc: true },
      { name: 'Score en % sans le nombre d’essais', withPc: true },
      { name: 'Score numérique (sans le nombre d’essais, séparateur virgule)', num: true, withComma: true },
      { name: 'Score numérique (sans le nombre d’essais, séparateur point)', num: true },
      { name: 'Note sur 20 avec le nombre d’essais', withEssais: true, note20: true },
      { name: 'Note sur 20 sans le nombre d’essais', note20: true },
      { name: 'Note sur 20 numérique (sans /20 ni le nombre d’essais)', num: true, note20: true },
      { name: 'Note sur 20 numérique (au dixième, séparateur virgule)', num: true, note20: true, decimal: true, withComma: true },
      { name: 'Note sur 20 numérique (au dixième, séparateur point)', num: true, note20: true, decimal: true },
      { name: 'Note sur 10 avec le nombre d’essais', withEssais: true, note10: true },
      { name: 'Note sur 10 sans le nombre d’essais', note10: true },
      { name: 'Note sur 10 numérique (sans /10 ni le nombre d’essais)', num: true, note10: true },
      { name: 'Note sur 10 numérique (au dixième, séparateur virgule)', num: true, note10: true, decimal: true, withComma: true },
      { name: 'Note sur 10 numérique (au dixième, séparateur point)', num: true, note10: true, decimal: true }
    ]
  }
}
// ajoute les id automatiquement à typeScore
let i = 1
for (const store of filters.typeScore.stores) {
  store.id = i++
}

// les ckeckboxes, les propriétés id et value seront ajoutées dans initFilters
const checkboxes = {
  elevesDeLaSequenceOnly: {
    label: 'Afficher seulement les élèves encore présent dans la séquence',
  },
  elevesFilteredByDate: {
    label: 'Ne pas afficher les élèves avec aucun résultat vérifiant le filtre "Dates" ci-dessus',
  },
  moyenneWithFinishedOnly: {
    label: 'Ne compter que les résultats des exercices terminés dans la moyenne',
  }
}

/**
 * Retourne la clé du filtre pour localStorage
 * @param {Object} filter
 * @returns {string}
 */
const getCurrentFilterKey = (filter) => `filter-${filter.id}-currentId`

/**
 * Init des valeurs des select / checkboxes d'après localStorage
 * @param $scope
 * @param {Object} [onChanges] des éventuelles callback pour être rappelé au changement d'une valeur
 */
function initFilters ($scope, onChanges = {}) {
  // init de current pour chaque filter (select)
  for (const filter of Object.values(filters)) {
    let current
    try {
      const memo = localStorage.getItem(getCurrentFilterKey(filter))
      if (memo) {
        const id = Number.parseInt(memo, 10)
        current = filter.stores.find((store) => store.id === id)
      }
    } catch (error) {
      console.error(error)
    }
    filter.current = current ?? filter.stores[0]
  }
  // les checkboxes
  for (const [id, cb] of Object.entries(checkboxes)) {
    cb.id = id
    cb.value = localStorage.getItem(getCurrentFilterKey(cb)) === 'on'
    if (onChanges[id]) {
      // faut un getter/setter pour être rappelé quand ça change
      let value = cb.value
      delete cb.value
      Object.defineProperty(cb, 'value', {
        enumerable: true,
        configurable: true,
        get: () => value,
        set: (newValue) => {
          value = Boolean(newValue)
          $scope[cb.id] = value
          onChanges[cb.id](value)
        }
      })
    }
  }
  // et on met tout ça dans le scope
  $scope.filters = filters
  $scope.checkboxes = checkboxes
  $scope.filterChange = filterChange
  $scope.eleveFilter = eleveFilter.bind(null, $scope)
  $scope.rowFilter = rowFilter.bind(null, $scope)
}

// au changement de la valeur d'un filtre on mémorise dans localStorage
function filterChange (filter) {
  try {
    const key = getCurrentFilterKey(filter)
    if (filter.stores) {
      if (!filter.stores[filter.current.id]) {
        return console.error(Error('filter invalide'), filter)
      }
      localStorage.setItem(key, filter.current.id)
    } else {
      // checkbox
      localStorage.setItem(key, filter.value ? 'on' : 'off')
    }
  } catch (error) {
    console.error(error)
  }
}

function days (date) {
  const t0 = new Date().setHours(0, 0, 0, 0)
  const t1 = new Date(date).setHours(0, 0, 0, 0)
  return (t0 - t1) / 86_400_000
}

function eleveFilter ($scope, data) {
  if ($scope.checkboxes.elevesDeLaSequenceOnly.value && !data.dansLaSequence) return false
  const isListView = Array.isArray(data.rows)
  const results = isListView ? data.rows : data
  switch ($scope.filters.participants.current.id) {
    // tous (sauf si on veut exclure ceux sans résultat
    case 1: {
      if (!$scope.checkboxes.elevesFilteredByDate.value) return true
      // faut exclure ceux sans résultat qui match le filtre date
      if ($scope.filters.dates.current.id === 0) {
        return isListView
          ? results.length > 0
          : data.hasResult
      }
      // pour les autres filtres de date on délègue
      return results.some(row => rowFilter($scope, row))
    }
    // ayant participé
    case 2: return isListView
      ? results.length > 0
      : data.hasResult
      // n'ayant pas participé
    case 3: return isListView
      ? results.length === 0
      : !data.hasResult
    case 4: return data.reachLast
    case 5: return !data.reachLast
    case 6: return !data.reachLast && (isListView ? results.length > 0 : data.hasResult)
  }
  console.error(Error('filter invalide'), $scope.filters.participants.current)
  return false
}

// helper pour la vue liste
function rowFilter ($scope, data) {
  // data est un resultat, une ligne pour la vue liste et une cellule pour la vue table
  switch ($scope.filters.dates.current.id) {
    case 0: return true
    case 1: return days(data.$date) === 0
    case 2: return days(data.$date) === 1
    case 3: return days(data.$date) <= 3
    case 4: return days(data.$date) > 3
  }
  return false
}

module.exports = initFilters
