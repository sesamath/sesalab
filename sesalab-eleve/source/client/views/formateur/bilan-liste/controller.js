const flow = require('an-flow')

const initFilters = require('../controller-common')

module.exports = function ($app, $eleve, $scope, $seance, $session, $stateParams,
  $formatter, $state, $popup, $toaster, $errors) {
  'ngInject'

  const eleveOid = $scope.eleve = $stateParams.eleve
  let isDaltonien, lastResponse

  function initialize () {
    const user = $session.utilisateurs()[0]
    isDaltonien = user?.bilanWithSymbols ?? false

    $scope.print = () => window.print()
    initFilters($scope)

    const sequence = $stateParams.sequence

    if (sequence && !eleveOid) {
      $scope.isBilanSequence = true
      $scope.exportUrl = `/api/resultat/export/sequence/${sequence}`
    } else {
      $scope.isBilanSequence = false
      if (!sequence && eleveOid) {
        $scope.exportUrl = `/api/resultat/export/eleve/${eleveOid}`
      }
    }

    if ($stateParams.lastResponse) {
      lastResponse = $stateParams.lastResponse
      $scope.listes = getBilans(lastResponse)
    } else {
      refresh()
    }
  }

  function refresh () {
    function callback (error, response) {
      if (error || !response) return // erreur gérée par $eleve.resultats et $seance.resultats
      lastResponse = response
      // et getBilans retrie par index et date…
      $scope.listes = getBilans(response)
    }

    const sequence = $stateParams.sequence
    if (!sequence && eleveOid) {
      $eleve.resultats(eleveOid, callback)
    } else if (sequence && !eleveOid) {
      $seance.resultats(sequence, callback)
    }
  }
  $scope.refresh = refresh

  function getBilans (response) {
    lastResponse = response
    const bilans = []

    // On construit un objet avec les meillleurs scores par élève pour chaque exercice
    const maxScorePerStudent = {}

    // il y a eu un bug dans l'envoi des résultats j3p entre le 24/01/2025 17:17
    // et le 27/01/2025 20:48 (on a un score mais sans savoir sur quel
    // nœud il porte).
    // => ça affiche systématiquement `pas de score pour ce parcours (mis à 100% pour permettre le passage au suivant en cas de minimum de réussite exigé)` pour le score et `Pas d’information enregistrée` pour la réponse
    // on essaie d'améliorer ça lorsque le graphe ne contenait qu'un seul nœud (la propriété score est forcément celle de ce nœud)
    // cf sesalab-eleve/source/client/widgets/bilan/controller.js
    // @todo virer les 3 rustines ci-dessous après 2026-07
    // rustine1
    const onNodeGraphs = new Set()
    // fin rustine1

    for (const resultat of response.resultats) {
      for (const userId of resultat.participants) {
        maxScorePerStudent[userId] = maxScorePerStudent[userId] || {}
        maxScorePerStudent[userId][resultat.rid] = Math.max(maxScorePerStudent[userId][resultat.rid] || 0, resultat.score)
      }
      // rustine2 à virer
      if (resultat.type === 'j3p' && Array.isArray(resultat.contenu?.graphe)) {
        // on a un graphe pour ce résultat, on regarde s'il y a plusieurs nœuds
        const nodes = resultat.contenu.graphe.filter(node => node.length > 2 && !/fin/i.test(node[1]))
        if (nodes.length === 1) {
          onNodeGraphs.add(resultat.rid)
        }
      } // fin rustine2
    }

    // on veut la liste des élèves par ordre alphabétique (c'est pas le cas en sortie d'api, "machine" se retrouve avant "machin")
    // mais il faut aussi trier par classe avant !
    const eleves = Object.values(response.eleves).sort((a, b) => {
      for (const p of ['classeName', 'nom', 'prenom']) {
        if (a[p] !== b[p]) return a[p] < b[p] ? -1 : 1
      }
      return 0
    })

    for (const eleve of eleves) {
      // Si nous demandons un bilan pour un élève, nous n'affichons que les lignes liées à cet élève
      if (eleveOid && eleve.oid !== eleveOid) continue
      const lignesBilanPourEleve = []
      let reachLast = false
      for (const resultat of response.resultats) {
        if (!resultat.participants.includes(eleve.oid)) continue
        // Les résultats renvoyés par l'api doivent être enrichis avec les informations des élèves et des ressources
        // On va chercher toutes les infos des participants
        const participants = resultat.participants.map((participant) => {
          if (response.eleves[participant]) return response.eleves[participant].nom + ' ' + response.eleves[participant].prenom
          else return 'un compte supprimé'
        })

        if (resultat.$reachLast) {
          reachLast = true
        }

        const time = resultat.duree
        const minutes = Math.floor(time / 60)
        const seconds = time - minutes * 60
        let duree = ''
        if (minutes) duree += minutes + ' min '
        if (seconds) duree += seconds + ' s'

        // à partir de 2025-01-30 on a la propriété essai sur le résultat retourné par l'api
        const essai = resultat.essai
          ? `${resultat.essai}${resultat.essai > 1 ? 'e' : 'er'} essai`
          : ''
        const row = {
          index: resultat.$indexRessource,
          title: resultat.ressource.titre,
          rid: resultat.ressource.rid,
          $score: resultat.score,
          $type: resultat.ressource.type,
          score: $formatter.score(resultat.ressource.type, resultat),
          reponse: $formatter.response(resultat.ressource.type, resultat, isDaltonien),
          fullResponse: $formatter.fullResponse(resultat.ressource.type, resultat),
          $date: resultat.$date,
          date: resultat.date,
          duree,
          hasMoreThanOneParticipant: participants.length >= 2,
          participants: 'Fait par ' + participants.join(' et '),
          isBest: maxScorePerStudent[eleve.oid]?.[resultat.rid] === resultat.score,
          essai
        }

        // rustine3: on tente de corriger ce qui peut l'être
        if (/^2[4-7]\/01\/2025/.test(row.date) && /pas de score/.test(row.score) && typeof row.$score === 'number') {
          if (onNodeGraphs.has(row.rid)) {
            row.score = String(row.$score)
            if (/Pas d’information enregistrée/.test(row.reponse)) {
              row.reponse = `${Math.round(row.$score * 100)}%`
            }
            row.fullResponse = ''
          } else {
            // on a un score mais on ne sait pas sur quel nœud
            // => on laisse la phrase pas de score… sur le score mais on donne l'info qu'on a dans la réponse
            if (/Pas d’information enregistrée/.test(row.reponse)) {
              row.reponse = `${Math.round(row.$score * 100)}% sur un nœud inconnu`
            }
          }
        } // fin rustine3

        lignesBilanPourEleve.push(row)
      }

      // le tri par index de l'exo dans la série n'a de sens que pour les bilans de séquence, pas pour les bilans élèves
      if ($stateParams.sequence) {
        lignesBilanPourEleve.sort((a, b) => {
          if (a.index === b.index) {
            return a.$date < b.$date ? -1 : 1 // De la plus ancienne à la plus récente
          }
          return a.index - b.index // ordre croissant des index
        })
      }

      bilans.push({
        eleve: `${eleve.nom} ${eleve.prenom} (${eleve.classeName})`,
        rows: lignesBilanPourEleve,
        reachLast,
        dansLaSequence: eleve.$dansLaSequence
      })
    } // fin boucle eleve

    return bilans
  }

  $scope.isFilterEnabled = function (filter) {
    switch (filter.id) {
      case 'typeScore': return false
      case 'moyenneWithFinishedOnly': return false
      case 'participants': return $scope.isBilanSequence
      default: return true
    }
  }

  $scope.doSelectTab = function () {
    if (!$stateParams.sequence) {
      $toaster.pop("La vue 'Table' n'est disponible que pour les bilans des séquences", 'error')
      return
    }
    $state.go('formateur-bilan-table', {
      sequence: $stateParams.sequence,
      eleves: $stateParams.eleves,
      lastResponse
    })
  }

  $scope.doShowFullResponse = function (row) {
    $scope.currentRow = row
    const popup = $popup.show({
      scope: $scope,
      height: '90%',
      width: '90%',
      template: require('./popup-response.html'),
      buttons: [
        { caption: 'Fermer', callback: function () { popup.close() } }
      ]
    })
  }

  $scope.linkToRessource = function (row) {
    // On accède à l'application Qooxdoo, parente de l'iframe
    const qooxdooApp = window.parent.window.qx.core.Init.getApplication()

    flow()
    // On récupère la ressource normalisée
      .seq(function () {
        const next = this
        app.sesatheque.getItem(row.rid, function (error, item) {
          if (error) {
            qooxdooApp.toaster.toast(error, 'error')
            return next()
          }
          return next(null, item)
        })
      })
    // On ouvre une iframe dans l'application Qooxdoo pour tester la ressource
      .seq(function (resource) {
        if (!resource) return this()
        qooxdooApp.testResource(resource)
        this()
      })
      .catch(this)
  }

  $scope.doDisplayHelp = function () {
    const qooxdooApp = window.parent.window.qx.core.Init.getApplication()
    qooxdooApp.openIframePage('Aide bilans', 'help', app.settings.aide.bilans, 'help-bilans')
  }

  // On doit attendre que l'application soit configurée pour accéder aux formatters
  // de la sesatheque
  $app.ready(initialize)
}
