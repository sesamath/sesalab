app.config(function ($stateProvider) {
  'ngInject'

  $stateProvider.state({
    name: 'formateur-bilan-liste',
    url: '/formateur/bilan/liste?sequence&eleves&eleve',
    template: require('./template.html'),
    controller: require('./controller'),
    params: {
      eleves: null,
      ressource: 'none',
      lastResponse: null
    }
  })
})
