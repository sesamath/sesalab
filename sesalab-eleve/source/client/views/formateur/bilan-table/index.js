app.config(function ($stateProvider) {
  'ngInject'

  $stateProvider.state({
    name: 'formateur-bilan-table',
    url: '/formateur/bilan/table?sequence&eleves',
    template: require('./template.html'),
    controller: require('./controller'),
    params: {
      eleves: null,
      ressource: 'none',
      lastResponse: null
    }
  })
})
