const constants = require('sesalab-commun/constants.js')
const { formatCsvCell } = require('sesalab-commun/formatString.js')
const helpers = require('sesalab-commun/helpers.js')
const initFilters = require('../controller-common')

module.exports = function ($app, $scope, $seance, $session, $stateParams, $state) {
  'ngInject'

  let user, sequence, lastResponse

  function initialize () {
    user = $session.utilisateurs()[0]
    $scope.withSymbols = user?.bilanWithSymbols ?? false
    $scope.mode = 'liste'
    $scope.limitReussite = 70
    $scope.print = () => window.print()
    initFilters($scope, { moyenneWithFinishedOnly: refreshMoyenne })

    sequence = $stateParams.sequence

    if ($stateParams.lastResponse) {
      lastResponse = $stateParams.lastResponse
      $scope.table = getTable(lastResponse)
      refreshMoyenne()
    } else {
      refresh()
    }
  }

  function refresh () {
    $seance.resultats(sequence, (error, response) => {
      if (error || !response) return // erreur déjà gérée par $seance.resultats
      lastResponse = response
      $scope.table = getTable(response)
      refreshMoyenne()
    })
  }
  $scope.refresh = refresh

  $scope.isFilterEnabled = function (filter) {
    return true
  }

  function getTable (data) {
    const table = {
      header: [{
        titre: 'Classe',
        isLastOfSousSequence: true
      }, {
        titre: 'Nom',
        isLastOfSousSequence: true
      }, {
        titre: 'Moyenne',
        isLastOfSousSequence: true
      }],
      rows: []
    }

    // toutes les ressources de la séquence
    const ressources = []
    // l'index de cellule (dans row) ou chaque ressource apparaît en premier (démarre donc à 2, 0 est la classe, 1 le nom de l'élève)
    const firstCellByRid = {}
    // on remplit ces deux variables avec une boucle sur les séries des sous-séquences
    data.sequence.sousSequences.forEach((sousSequence, sousSequenceIndex) => {
      // pour cette sous-seq, un Set pour les participants
      const participantsSsSeq = new Set(sousSequence.$participantOids)

      sousSequence.serie.forEach((ressource, ressourceIndex) => {
        // on ajoute le set à chaque ressource
        ressource.participantsSsSeq = participantsSsSeq
        // si c'est la dernière ressource de la série, mais pas la dernière sous-séquence de la séquence
        // on lui pose le flag isLastOfSousSequence (pour le séparateur vertical plus gras)
        if (ressourceIndex === sousSequence.serie.length - 1 && sousSequenceIndex < data.sequence.sousSequences.length - 1) {
          ressource.isLastOfSousSequence = true
        }
        ressources.push(ressource)
        // on mémorise où elle apparaît en 1er (index de row, décalé de 3 par rapport à ressources,
        // donc peut pas être 0 ni 1)
        if (!firstCellByRid[ressource.rid]) firstCellByRid[ressource.rid] = ressources.length + 2
      })
    })

    // Les headers du tableau : le titre des exercices (+ le flag)
    for (const { titre, isLastOfSousSequence } of ressources) {
      table.header.push({ titre, isLastOfSousSequence })
    }

    // On récupère seulement les meilleurs résultats, par participantOid et ressourceRid
    const bestResults = {} // {participantOid: {rid: resultat}}
    // idem, mais juste avec des booléens, pour distinguer "aucun résultat" de "Exercice non terminé"
    const hasPartialResults = {}
    // boucle résultats
    for (const resultat of data.resultats) {
      const rid = resultat.ressource.rid
      for (const participantOid of resultat.participants) {
        // si c'est un résultat d'exercice non terminé on le note mais sans le prendre en compte dans bestResults
        if (!resultat.fin) {
          if (!hasPartialResults[participantOid]) hasPartialResults[participantOid] = {}
          hasPartialResults[participantOid][rid] = true
          continue
        }
        if (!bestResults[participantOid]) bestResults[participantOid] = {}
        const lastBestResult = bestResults[participantOid][rid]
        // on prend ce résultat si ces conditions sont remplies
        if (
          // y'en avait pas encore
          !lastBestResult ||
          // celui qu'on a n'a pas de score et le nouveau en a un
          (lastBestResult.score === undefined && resultat.score !== undefined) ||
          // les deux en ont un et le nouveau est meilleur
          (lastBestResult.score !== undefined && resultat.score !== undefined && resultat.score > lastBestResult.score)
        ) {
          bestResults[participantOid][rid] = resultat
        }
      }
    }

    // un helper
    const fillCell = (cell, resultat) => {
      if (Number.isFinite(resultat.score) && resultat.score >= 0 && resultat.score <= 1) {
        // On retrouve le nom des élèves qui ont fait l'exercice
        const participants = resultat.participants.map((oid) => `${data.eleves[oid].prenom} ${data.eleves[oid].nom}`)
        cell.isScore = true
        cell.value = resultat.score
        cell.hasMoreThanOneParticipant = resultat.participants.length > 1
        cell.$date = resultat.$date
        if (cell.hasMoreThanOneParticipant) cell.participants = 'Fait par ' + participants.join(' et ')
        cell.essai = resultat.essai
          ? `${resultat.essai}${resultat.essai > 1 ? 'e' : 'er'} essai`
          : ''
        cell.shouldBeIgnored = false
      } else {
        // un résultat sans score, c'est pas normal
        cell.value = 'Pas de score'
      }
    }

    // élèves triés par classe / nom / prénom
    const eleves = Object.values(data.eleves).sort((elA, elB) => {
      for (const p of ['classeName', 'nom', 'prenom']) {
        if (elA[p] !== elB[p]) return elA[p] < elB[p] ? -1 : 1
      }
      return 0
    })

    // On passe au remplissage, une ligne par élève
    for (const eleve of eleves) {
      // classe puis nom puis moyenne
      const row = [
        {
          value: eleve.classeName,
          isName: true,
          isLastOfSousSequence: true
        }, {
          value: `${eleve.nom} ${eleve.prenom}`,
          isName: true,
          isLastOfSousSequence: true
        }, { // la moyenne
          isName: false,
          isScore: true,
          $date: null,
          value: '',
          isLastOfSousSequence: true
        }
      ]
      // y'a un meilleur résultat (terminé) pour cet élève
      let hasBestResult = false
      // dernière ressource atteinte
      let reachLastResultat = false
      // élève dans la séquence
      let dansLaSequence = false

      // pour mettre de coté les résultats qu'on omet car l'élève n'est pas dans la sous-séquence
      // cela sera ajouté à la fin si ça n'a pas été affiché dans aucune sous-séquence
      const resultatsOmisParRid = {}
      // pour mémoriser les rid déjà traités
      const resultatsTraites = new Set()
      // Pour chaque ligne, une case par exercice
      for (const ressource of ressources) {
        const cell = {
          hasMoreThanOneParticipant: false,
          participants: null,
          value: 'Aucun résultat',
          $date: null,
          isLastOfSousSequence: ressource.isLastOfSousSequence
        }
        if (ressource.participantsSsSeq.has(eleve.oid)) {
          dansLaSequence = true
        } else {
          // si l'élève n'est pas dans la sous-seq on affiche une case vide par défaut
          // (mais si on trouve un résultat plus loin d'un exercice qui n'est pas dans une autre
          // sous-séquence dans laquelle se trouve l'élève on remplira quand même)
          // @todo rectifier quand on aura des id de sous-séquence, pour le cas où il était dans la ssSeq et n'y est plus
          cell.value = ''
          cell.shouldBeIgnored = true
        }

        const bestResult = bestResults[eleve.oid] && bestResults[eleve.oid][ressource.rid]
        if (bestResult) {
          if (ressource.participantsSsSeq.has(eleve.oid)) {
            hasBestResult = true
            if (bestResult.$reachLast && !reachLastResultat) reachLastResultat = true
            fillCell(cell, bestResult)
            resultatsTraites.add(ressource.rid)
            if (resultatsOmisParRid[ressource.rid]) delete resultatsOmisParRid[ressource.rid]
          } else if (!resultatsTraites.has(ressource.rid)) {
            // l'élève est pas dans cette sous-séquence et ce rid n'a pas encore été affiché dans une autre
            resultatsOmisParRid[ressource.rid] = bestResult
          }
        } else if (hasPartialResults[eleve.oid] && hasPartialResults[eleve.oid][ressource.rid]) {
          cell.value = 'Non terminé'
          cell.shouldBeIgnored = false
        }
        row.push(cell)
      } // fin for ressource

      // s'il reste des résultats omis on les ajoute à la 1re cellule où la ressource apparaît
      // tant pis si c'était pas la bonne sous-séquence, on peut pas le deviner
      for (const [rid, resultat] of Object.entries(resultatsOmisParRid)) {
        const cellIndex = firstCellByRid[rid]
        const cell = row[cellIndex]
        fillCell(cell, resultat)
      }

      row.hasBestResult = hasBestResult
      row.hasResult = hasBestResult || Boolean(hasPartialResults[eleve.oid])
      row.reachLast = reachLastResultat
      row.dansLaSequence = dansLaSequence
      table.rows.push(row)
    }

    return table
  } // getTable

  function refreshMoyenne () {
    for (const row of $scope.table.rows) {
      if (row.length < 4) continue
      const scores = row.slice(3)
      const moyenne = row[2]
      let total = 0
      let nbTotal = 0
      for (const cell of scores) {
        if (cell.shouldBeIgnored) continue // l'élève n'avait pas cette ressource assignée
        const score = Number(cell.value)
        if (Number.isFinite(score) || !$scope.checkboxes.moyenneWithFinishedOnly.value) {
          nbTotal++
          total += score || 0
        }
      }
      if (nbTotal) {
        moyenne.value = total / nbTotal
      } else {
        moyenne.value = ''
      }
    }
  }
  /**
   * Génère le contenu du csv, d'après l'option du type d'export voulu
   * @param table
   * @returns {string}
   */
  function generateExport (table) {
    let content = table.header
      .map(({ titre }) => formatCsvCell(titre))
      .join(constants.CSV_SEPARATOR) + '\n'
    for (const row of table.rows) {
      const [classe, nom, ...scores] = row
      const cells = [`"${classe.value}"`, `"${nom.value}"`]
      for (const cell of scores) {
        let value = $scope.getCellValue(cell)
        // faut ajouter les essais si besoin (traités à part pour l'affichage)
        if (cell.essai && $scope.filters.typeScore.current.withEssais) {
          value += ` (${cell.essai})`
        }
        if ($scope.filters.typeScore.current.num) {
          cells.push(value)
        } else {
          cells.push(`"${value}"`)
        }
      }
      content += cells.join(constants.CSV_SEPARATOR) + '\n'
    }

    return content
  }

  function days (date) {
    const t0 = new Date().setHours(0, 0, 0, 0)
    const t1 = new Date(date).setHours(0, 0, 0, 0)
    const d = (t0 - t1) / 86400000
    return d
  }

  $scope.exportToFile = function () {
    const content = generateExport($scope.table)
    helpers.download(content, 'export_table_utf8.csv')
  }

  $scope.isCellSuccess = function (cell) {
    return Boolean(cell.isScore && cell.value >= $scope.limitReussite / 100)
  }

  $scope.isCellFailure = function (cell) {
    return cell.isScore && !$scope.isCellSuccess(cell)
  }

  $scope.getCellValue = function (cell) {
    if (cell.isName || !cell.isScore) {
      return cell.value
    }
    // c'est un score
    /** @type {string} */
    let value
    if (Number.isFinite(cell.value)) {
      const score = cell.value
      // on traite cette valeur numérique, qui finira en string dans tous les cas
      if ($scope.filters.typeScore.current.withPc) {
        // on veut des %
        value = (100 * score).toFixed(0) + '%'
      } else {
        if ($scope.filters.typeScore.current.note20) {
          // note sur 20
          if ($scope.filters.typeScore.current.decimal) {
            value = (20 * score).toFixed(1)
          } else {
            value = String(Math.round(20 * score))
            if (!$scope.filters.typeScore.current.num) value += ' / 20'
          }
        } else if ($scope.filters.typeScore.current.note10) {
          // note sur 10
          if ($scope.filters.typeScore.current.decimal) {
            value = (10 * score).toFixed(1)
          } else {
            value = String(Math.round(10 * score))
            if (!$scope.filters.typeScore.current.num) value += ' / 10'
          }
        } else {
          // on garde la note sur 1, mais en string avec 2 chiffres après la virgule
          value = score.toFixed(2)
        }

        // si y'a du décimal on vire un éventuel .0 inutile
        // (mais on laisse le 0.50 par ex, pour conserver deux chiffres après la virgule dans ce cas)
        if ($scope.filters.typeScore.current.decimal) {
          value = value.replace('.0', '')
        }
        // et si on préfère une virgule
        if ($scope.filters.typeScore.current.withComma) {
          value = value.replace('.', ',')
        }
      }
    } else {
      value = cell.value
    }
    // on regarde le filtrage par date
    if ($scope.filters.dates.current.id === 0) return value

    const cellDays = days(cell.$date)
    if ($scope.filters.dates.current.id === 1 && cellDays === 0) return value
    if ($scope.filters.dates.current.id === 2 && cellDays === 1) return value
    if ($scope.filters.dates.current.id === 3 && cellDays <= 3) return value
    if ($scope.filters.dates.current.id === 4 && cellDays >= 3) return value
  }

  $scope.doSelectTab = function () {
    $state.go('formateur-bilan-liste', {
      sequence: $stateParams.sequence,
      eleves: $stateParams.eleves,
      lastResponse
    })
  }

  $scope.isEssaiDisplayed = (cell) => cell.essai && $scope.filters.typeScore.current.withEssais

  // On doit attendre que l'application soit configurée pour accéder aux formatters
  // de la sesatheque
  $app.ready(initialize)
}
