const { TYPE_FORMATEUR } = require('sesalab-commun/constants.js')

module.exports = function ($app, $scope, $session, $seance, $stateParams, $toaster) {
  'ngInject'

  function initialize () {
    const sequenceOid = $stateParams.sequence
    const eleveOid = $stateParams.eleve
    $seance.getSequence(sequenceOid, eleveOid, loadSequence)
  }

  function loadSequence (sequence) {
    const sequenceOid = $stateParams.sequence
    if (!sequence) return $toaster.pop('Aucune séquence', 'error', 10000)
    const me = $session.utilisateurs()[0]
    if (me.type !== TYPE_FORMATEUR) return $toaster.pop('Accès refusé', 'error')
    const { oid, nom, prenom } = me
    // il ne faut pas tester sequence.owner === oid car on peut charger les séquences des collègues
    if (sequence.oid !== sequenceOid) return $toaster.pop('Séquence invalide', 'error', 10000)
    // faut lui ajouter la propriété formateur
    sequence.formateur = { oid, nom, prenom }
    // et mettre tout ça dans le scope
    $scope.formateurs = [
      // un seul formateur, celui qui teste
      {
        oid,
        nom,
        prenom,
        // une seule séance, celle qu'on teste
        seances: [$seance.create(sequence)]
      }
    ]
  }

  $scope.doLogout = function () {
    $session.disconnect()
  }

  // idem listener élève de sesalab-eleve/source/client/views/eleve/seances/controller.js
  // sauf qu'on a pas besoin de changer la vue
  $scope.$on('exercice::selected', (event, exercice) => {
    $scope.$broadcast('exercice::load', exercice)
  })

  $app.ready(initialize)
}
