app.config(function ($stateProvider) {
  'ngInject'

  $stateProvider.state({
    name: 'formateur-test-sequence',
    url: '/formateur/test/:sequence/:eleve',
    template: require('./template.html'),
    controller: require('./controller')
  })
})
