const path = require('path')

app.controller(function () {
  const publicPath = path.join(__dirname, '/../../public')
  this.serve('eleve', publicPath)
  this.get('eleve/inscription', { fsPath: publicPath })
  this.get('eleve/changement-password', { fsPath: publicPath })
})
