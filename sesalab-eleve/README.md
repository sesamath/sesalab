## Documentations
* [sesalab](../../documentation/index.html)
* [Angular Howto](./documentation/angular_howto.md)

## Flux des échanges pendant une séance
* état client initialisé
* à la sélection d'une ressource
  * broadcast `exercice::selected`
  * le listener impose la vue VIEWS.EXERCICE, décharge l'iframe (ça peut provoquer 
    un postMessage avec un résultat s'il y avait un exo chargé) puis broadcast `exercice::load`
  * le listener `exercice::load` (prepareExercice) formate exercice et appelle loadExercice,
     qui ajoute les listener pour le résultat (message avec action `sesalab::result` qui sera envoyé par l'iframe en postMessage)
     et celui pour ressourceLoaded (message avec action `sesalab::ressourceLoaded`) 
     puis charge l'iframe
  * l'iframe fait un postMessage ressourceLoaded
  * le listener `ressourceLoadedListener` init `$rootScope.nonZapping` et `$rootScope.startedAt` 
    puis post vers `api/seance/start`
  * au retour nbEssais++ coté client (le serveur l'a fait avec l'appel précédent) 
    + broadcast `exercice::started`
  * listener `exercice::started` reset le scope
* lors de l'envoi d'un résultat (message avec action `sesalab::result`)
  * check du contenu puis post sur
  * si resultat.fin, broadcast `ressource::finished`
  * le listener `ressource::finished` (views/eleve/seances), s'il concerne la ressource courante (si c'est la précédente ça fait rien)
    affiche qu'il va reset la vue, et si on est dans une séquence (pas en autonomie) broadcast `seance::finished`
  * le listener `seance::finished` reset le scope
