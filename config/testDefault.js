/**
 * Ce module renvoie les données par défaut pour l'environnement de test
 * (surchargées par un éventuel _private/test.js dans ./index.js)
 */
const uuid = require('an-uuid')

const bibli = require('../docker/sesatheque/_privateConfig/testBibli').application
const commun = require('../docker/sesatheque/_privateConfig/testCommun').application

module.exports = {
  application: {
    name: 'sesalabTest',
    baseUrl: 'http://sesalab.local:3010',
    homeTitle: 'Sesalab Test',
    mail: 'nobody@example.com',
    fromMail: 'nobody@example.com',
    contactMail: 'nobody@example.com',
    sesatheques: [
      { baseId: bibli.baseId, baseUrl: bibli.baseUrl },
      { baseId: commun.baseId, baseUrl: commun.baseUrl }
    ]
  },

  appLog: {
    addBaseIdSuffix: true // on veut du log/app.test.log pour les traces du serveur lancé pour les tests
  },

  $cache: {
    redis: {
      prefix: 'sesalab_test'
    }
  },
  $crypto: {
    salt: '1234' // attention à modifier le test de password si on change ça
  },

  $entities: {
    database: {
      name: 'sesalab-test'
    }
  },

  $server: {
    port: 3010
  },

  $rail: {
    cookie: {
      key: uuid()
    },
    session: {
      secret: uuid()
    }
  }
}
