const { existsSync, mkdirSync, readdirSync, readFileSync } = require('node:fs')
const { join } = require('node:path')
const _ = require('lodash')
const { addSlash } = require('sesajstools/http/url')
const { hasProp } = require('sesajstools')

/**
 * Les éléments de configuration
 * @type {sesalab~Settings}
 */
const configDefault = require('./default')
/**
 * Fichier de conf locale qui pourra surcharger les valeurs générales de conf de l'appli définies ici
 * @private
 * @type {string}
 */
const localConfigDir = join(__dirname, '..', '_private')
const localConfigFile = join(localConfigDir, 'config.js')
const localTestConfigFile = join(localConfigDir, 'test.js')
const exempleConfigDir = join(__dirname, '..', '_private.exemple')
const exempleConfigFile = join(exempleConfigDir, 'config.js')
const exempleConfig = require(exempleConfigFile)

// pour du contenu spécifique à cette instance, cf plus bas
const localConfigContentDir = join(localConfigDir, 'content')

if (typeof window !== 'undefined') {
  // Ce fichier contient des infos sensible qu'on ne veut pas dans le code client
  // Utiliser plutôt un GET /api/settings pour obtenir les informations de config
  throw new Error('config.js should never be included in browser source code!')
}
/**
 * Configuration retournée, qui est la configuration par défaut avec les surcharges locales
 * 1) Cas normal, surcharge avec _private/config.js (qui est docker/sesalab/config/config.js pour docker)
 * 2) NODE_ENV = test => surcharge avec config/test.js, qui appelle _private/test.js
 *    (soit docker/sesalab/config/test.js pour docker)
 * @type {sesalab~Settings}
 */
const config = {}

// préciser ici les clés optionnelles (pour ne pas planter au boot si ça manque en _private)
const optional = [
  'application.maintenance',
  'application.logLevel',
  '$cache.redis.host',
  '$cache.redis.port',
  // on autorise une connexion sans pass, pour ceux qui ont un mongo local et pas envie de gérer ça
  '$entities.database.user',
  '$entities.database.password',
  '$server',
  'gestion',
  'logger'
]

// On surcharge avec le contenu de la conf locale
// _private/config.js ou (./test.js + _private/test.js) suivant le cas
//
// on ne peut pas utiliser _.extend, qui est un alias de assignIn et revient à du Object.assign
// car ça ne permet pas de surcharger une seule sous-propriété (la propriété parente est remplacée
// mais pas enrichie).
// On procède dans l'autre sens avec _.defaultsDeep qui ajoute au fur et à mesure les propriétés
// qui n'existent pas, sans modifier celles déjà définies
if (process.env.NODE_ENV === 'test') {
  // pour le test, on veut surcharger par defaultForTest + _private/test.js
  // si _private/test.js existe on init config avec
  if (existsSync(localTestConfigFile)) _.defaultsDeep(config, require(localTestConfigFile))
  _.defaultsDeep(config, require('./testDefault'))
  // mais il faut prendre le $crypto.salt de la conf "normale" pour le test:browser
  // (il lance un lassi de test mais le browser de test utilise
} else {
  // hors test un _private/config.js est obligatoire
  if (!existsSync(localConfigFile)) throw new Error(`Il faut préciser la configuration spécifique à cette instance dans ${localConfigFile}`)
  _.defaultsDeep(config, require(localConfigFile))
}
// et on complète avec la conf par défaut et la conf d'exemple
// pour tout ce qui n'a pas été précisé localement
_.defaultsDeep(config, configDefault, exempleConfig)

// On cherche aussi du contenu éventuel mis dans des fichiers de _private/content à ajouter à la conf
if (existsSync(localConfigContentDir)) {
  for (const file of readdirSync(localConfigContentDir)) {
    const match = /([\w]+)\.inc\.html/.exec(file)
    const key = match && match[1]
    if (key) {
      if (!config.application.content) config.application.content = {}
      config.application.content[key] = readFileSync(join(localConfigContentDir, file)).toString()
    }
  }
}

// et la version déduite du package.json
config.application.version = require('../package.json').version

// slash de fin ajouté s'il n'y est pas
config.application.baseUrl = addSlash(config.application.baseUrl)
// il faut cette propriété, qui sera affectée à la bonne valeur dans preboot
if (!hasProp(config.application, 'baseId')) config.application.baseId = ''

// on vire le cache en dev
// @todo virer cette ligne, on veut vérifier que les headers de cache fonctionnent en dev
// (si on veut pas de cache faut cocher la case noCache dans les devTools du navigateur
// et si vraiment c'est indispensable de le faire par défaut, modifier le ?version mis sur les
// appels de fichiers statique pour mettre un timestamp
if (config.application.staging === 'dev') config.pathProperties = {}

// on permet de forcer ça en _private/config.js, pour pouvoir tourner en recette avec staging prod
// mais laisser isProd à false (pour les envois de mail par ex, ou les urls de SSO)
if (typeof config.application.isProd !== 'boolean') {
  config.application.isProd = config.application.staging === 'prod'
}

// ////////////////////////////////////////////////////////////
//
// Fin du build de la configuration
// (sauf sesalabSso déduit du reste à la fin, si tout est ok)
//
// On passe à sa vérification
//
// ////////////////////////////////////////////////////////////

const errors = []
let globalBaseId = 'undefined'
if (!Array.isArray(config.application.sesatheques)) {
  errors.push('Configuration application.sesatheques obligatoire (array de 2 éléments minimum)')
} else if (config.application.sesatheques.length < 2) {
  errors.push('Configuration application.sesatheques doit comporter deux sésathèques (globale et privée, qui peuvent être la même)')
} else if (config.application.sesatheques.every((s) => s.baseUrl && s.baseId)) {
  globalBaseId = config.application.sesatheques[0].baseId
} else {
  errors.push('Configuration application.sesatheques doit indiquer un baseId et un baseUrl pour chaque sésathèque')
}

// profils (transforme les oid en rid au passage)
if (typeof config.application.profils !== 'object') {
  errors.push(`Configuration application.profils obligatoire (cf ${exempleConfigFile})`)
  // pour que les tests suivants provoquent des erreurs utilisables
  config.application.profils = {}
}
// propriétés de profils obligatoires
for (const p of ['all', 'default', 'autonomie']) {
  const profil = config.application.profils[p]
  if (!profil) {
    errors.push(`profils.${p} est obligatoire (et doit comporter  une propriété oid ou rid)`)
  } else if (!profil.oid && !profil.rid) {
    errors.push(`profils.${p}.oid (ou rid) est obligatoire`)
  }
}

if (Object.keys(config.application.profils).length < 5) {
  errors.push(`Configuration application.profils incomplet (cf ${exempleConfigFile})`)
}
// construction des rid et check rid|oid & label
for (const p of Object.keys(config.application.profils)) {
  const profil = config.application.profils[p]
  if (!profil.rid && profil.oid) {
    profil.rid = `${globalBaseId}/${profil.oid}`
    delete profil.oid
  }
  if (!profil.rid) errors.push(`profils.${p}.rid (ou oid) manquant`)
  if (!profil.label) errors.push(`profils.${p}.label manquant`)
}

// Et on verifie que la configuration est complète d'après le contenu de _private.exemple/config.js
const check = (exemple, ref, parent, key) => {
  const path = `${parent}${parent ? '.' : ''}${key}`
  if (optional.includes(path)) return
  const addError = () => errors.push(`clé ${path} manquante (ou invalide) en configuration`)
  // la récursion dépend du type
  if (Array.isArray(exemple[key])) {
    if (!Array.isArray(ref[key])) addError()
  } else if (typeof exemple[key] === 'object') {
    if (typeof ref[key] !== 'object') {
      addError()
    } else {
      for (const sk of Object.keys(exemple[key])) {
        check(exemple[key], ref[key], path, sk)
      }
    }
  } else if (typeof exemple[key] === 'string') {
    // un cas particulier pour les profils, où on peut avoir oid ou rid initialement,
    // mais rid seulement au final
    if (key === 'oid') key = 'rid'
    if (typeof ref[key] !== 'string' || !ref[key] || ref[key] === 'xxx') addError()
  } else if (!hasProp(ref, key)) {
    addError()
  }
}
for (const key of Object.keys(exempleConfig)) {
  check(exempleConfig, config, '', key)
}

// Affichage des erreurs éventuelles
if (errors.length) {
  console.error(errors.join('\n'))
  console.error(`Configuration invalide, cf ${exempleConfigFile} à adapter dans ${localConfigFile}`)
  process.exit(1)
}

/**
 * C'est tout bon, on peut passer à la config du component sesalabSso, déduite du reste
 */
if (!config.components) config.components = {}
if (!config.components.sesalabSso) config.components.sesalabSso = {}
if (!config.components.sesalabSso.clients) config.components.sesalabSso.clients = []
const confSso = config.components.sesalabSso
const ssoClients = confSso.clients
for (const sesatheque of config.application.sesatheques) {
  // slash de fin ajouté s'il n'y est pas
  sesatheque.baseUrl = addSlash(sesatheque.baseUrl)
  // si cette base n'est pas enregistrée comme client sso, on l'ajoute
  // (pour autoriser cette sésathèque à nous interroger comme serveur SSO pour s'authentifier là-bas)
  if (!ssoClients.some(client => client.baseUrl === sesatheque.baseUrl)) {
    // on y était pas, on ajoute
    ssoClients.push({ baseUrl: sesatheque.baseUrl })
  }
}
// url de retour ici après propagation sur tous les clients
confSso.afterClientsPage = 'formateur/'

// url de retour ici après un pb sur un client (avec un message explicite dans ?error=)
confSso.errorPage = '/'
// affiche la liste des déconnexions des clients (fait les appels en ajax)
confSso.logoutPage = 'sso/logout'
// @todo affiche le formulaire de login pour un client qui réclame une authentification
// il faudra rediriger vers l'url qu'il demande en retour et le stocker comme client authentifié
confSso.loginPage = 'sso/login'

// on crée le logDir s'il n'existe pas
const logDir = config.logs?.dir
if (typeof logDir === 'string') {
  if (!existsSync(logDir)) {
    mkdirSync(logDir)
  }
} else {
  errors.push('logs.dir absent')
}

module.exports = config
