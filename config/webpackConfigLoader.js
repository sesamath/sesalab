const _ = require('lodash')
const config = require('./index')

/*
 * ici on n'exporte que des variables non-sensibles et qui peuvent être
 * exposées dans le browser
 */

// @todo: on pourra ajouter progressivement d'autres éléments "sûrs" de config plutôt que de les récupérer via /api/settings
const safeConfig = _.pick(config, [
  'application.version',
  'application.staging',
  'bugsnag'
])

// Exports un "loader" webpack
module.exports = function () {
  // Un peu étrange, ici on doit renvoyer le texte du code JS à injecter
  // dans le build webpack
  return `module.exports = ${JSON.stringify(safeConfig)}`
}
