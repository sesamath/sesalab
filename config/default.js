'use strict'
/*
 * @preserve This file is part of "sesalab".
 *    Copyright 2009-2014,
 *    Author :
 *    eMail  :
 *    Site   :
 *
 * "sesalab" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "sesalab" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "sesalab"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

const { join } = require('node:path')

const bibli = require('../docker/sesatheque/_privateConfig/config').application
const commun = require('../docker/sesatheque/_privateConfig/commun').application

const logDir = join(__dirname, '..', 'log')

function setCacheControl (res, path) {
  let cacheControl
  if (/\/$/.test(path)) {
    // pour le html de la home, s'il est en cache on peut le considérer bon pendant 10 min avec màj en tâche de fond
    // sinon faut vérifier etag (mais dans la plupart des cas il sera quand même utilisé depuis le cache)
    cacheControl = 'public, must-revalidate, stale-while-revalidate=600'
  } else if (/\/(app|qooxdoo|style)\.(css|js)/.test(path)) {
    // pour ces js & css, ils ont du ?x.y.z, donc ça peut rester plus longtemps
    cacheControl = 'public, max-age=86400'
  } else {
    // pour les images et le reste, en cas de modif ce sera màj au pire au 2e affichage
    cacheControl = 'public, must-revalidate, stale-while-revalidate=86400'
  }
  // console.log('cache control pour', path, cacheControl)
  res.setHeader('Cache-Control', cacheControl)
}

// sert à rien, lassi met du max-age=3600 quoi qu'il arrive…
const defaultStaticOptions = { setHeaders: setCacheControl, maxAge: true }

/**
 * Configuration retournée, qui est la configuration par défaut avec les surcharges locales
 * 1) Cas normal, surcharge avec _private/config.js (qui est docker/sesalab/config/config.js pour docker)
 * 2) NODE_ENV = test => surcharge avec config/test.js, qui appelle _private/test.js
 *    (soit docker/sesalab/config/test.js pour docker)
 * @typedef {sesalab~Settings}
 * @type {{application: {name: string, baseUrl: string, mail: string, fromMail: string, contactMail: string, validateMailPath: string, validateNewMailPath: string, removeAccountPath: string, resetPasswordPath: string, joursAvantExpiration: number, joursPurgeCorbeille: number, joursGestion: number, joursDisplayDatePurgeStructure: number, maxBilans: number, maxClasses: number, maxResourcesDropped: number, maxResults: number, heuresAffichables: {min: number, max: number}, staging: string, timeoutDefaultView: number, updates: {folder: *, lockFile: *}}, $cache: {redis: {prefix: string}}, $entities: {database: {connectionLimit: number, user: string, password: string, database: string}}, $rail: {cors: {origin: string}, cookie: {key: string}, session: {secret: string, saveUninitialized: boolean, resave: boolean}}, $server: {port: number}, logs: {dir: *, anLog: {'lassi-cli': {renderers: {name: string, logLevel: string}}, $mail: {renderers: {name: string, logLevel: string}}}}, pathProperties: {'/eleve': {maxAge: string}, '/formateur': {maxAge: string}, '/front': {maxAge: string}, '/resource': {maxAge: string}}}}
 */
module.exports = {
  application: {
    name: 'sesalab',
    baseUrl: 'http://sesalab.local:3000/',
    mail: 'xxx',
    fromMail: 'xxx',
    contactMail: 'xxx',
    validateMailPath: 'valider-email',
    validateNewMailPath: 'valider-nouvel-email',
    removeAccountPath: 'supprimer-compte',
    resetPasswordPath: 'reinitialiser-mot-de-passe',
    // délais et limites
    joursAvantExpiration: 10, // Nombre de jours avant qu'un compte en attente de validation expire
    joursPurgeCorbeille: 30,
    joursGestion: 60,
    joursDisplayDatePurgeStructure: 70,
    maxBilans: 3000,
    maxClasses: 200,
    maxResourcesDropped: 10, // Nombre maximum de ressources pouvant être récupérées en 1 drop de dossier
    maxResults: 250,
    heuresAffichables: {
      min: 8,
      max: 18
    },
    sesatheques: [
      { baseId: bibli.baseId, baseUrl: bibli.baseUrl },
      { baseId: commun.baseId, baseUrl: commun.baseUrl }
    ],
    staging: 'prod', // dev|preprod|prod
    timeoutDefaultView: 4000, // Timeout d'affichage de la vue par défaut lorsqu'un élève finit un exercice
    updates: {
      folder: join(__dirname, '/../updates'),
      lockFile: join(__dirname, '/../_private/updates.lock')
    }
  },

  appLog: {
    dir: logDir,
    logLevel: 'warning',
    addBaseIdSuffix: false // le surcharger à true dans _private/test.js par ex
  },

  $cache: {
    redis: {
      prefix: 'sesalab_'
    }
  },

  $entities: {
    database: {
      connectionLimit: 10,
      // ajouter user & password dans _private/config.js
      // si mongo est configuré avec authentification
      database: 'sesalab'
    }
  },

  $rail: {
    cors: {
      origin: '*'
    },
    /* pour activer un log d'accès
    accessLog: {
      logFile: 'log/access.log'
    }, /* */
    cookie: {
      key: 'xxx'
    },
    session: {
      secret: 'xxx',
      saveUninitialized: true,
      resave: true
    }
  },

  $server: {
    port: 3002
  },

  // conf des logs, à surcharger en _private pour les rendre plus verbeux
  logs: {
    dir: logDir,
    anLog: {
      app: {
        renderers: [{
          name: 'file',
          target: join(logDir, 'app.log'),
          logLevel: 'warning'
        }]
      },
      'lassi-cli': {
        renderers: [{
          name: 'ansi',
          logLevel: 'warning'
        }]
      },
      $mail: {
        renderers: [{
          name: 'ansi',
          logLevel: 'warning'
        }]
      }
    }
  },

  // ATTENTION, c'est ignoré en cas de staging dev
  pathProperties: {
    // lister ici les path qui seront passés à controller.serve pour lesquel on veut un max-age
    // le contenu sera passé tel quel à express.static en options, cf https://github.com/expressjs/serve-static
    // pour la liste des options possibles
    // Attention, pour tout ce qui n'est pas listé ici on aura du max-age: 0, donc aucune mise en cache
    // (ni par varnish ni par le navigateur)
    '/eleve': defaultStaticOptions,
    '/formateur': defaultStaticOptions,
    // pour les icones qooxdoo
    '/resource': defaultStaticOptions
  }
}
