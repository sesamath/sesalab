module.exports = function ($app, $state, $scope, $session, $window) {
  'ngInject'

  function initialize () {
    $scope.includes = $state.includes
    $scope.utilisateur = $session.user()
  }

  $scope.doDisplayHelp = function () {
    $window.open(app.settings.aide.gestion, '_blank')
  }

  $scope.doLogout = function () {
    $session.disconnect()
  }

  $app.ready(initialize)
}
