const { endOfDay, startOfDay } = require('sesajs-date')

module.exports = function ($app, $scope, $http, $errors, $notification, $loader, $state, $timeout) {
  'ngInject'
  const isNew = $scope.isNew = $state.params.oid === 'new'
  $scope.oid = $state.params.oid
  $scope.title = ''
  $scope.content = ''
  $scope.date = null
  $scope.startDate = new Date()
  $scope.endDate = new Date()

  function initialize () {
    if (isNew && !$state.params.originalMessage) {
      return
    }

    const isCopy = !!$state.params.originalMessage
    const oid = isCopy ? $state.params.originalMessage : $scope.oid

    $http.get('/api/messages/' + oid)
      .then((response) => {
        $scope.title = response.data.message.title
        $scope.content = response.data.message.content
        $scope.startDate = new Date(response.data.message.startDate)
        $scope.endDate = new Date(response.data.message.endDate)
        if (!isCopy) {
          $scope.date = response.data.message.date
        }
      })
  }

  $scope.isPastDate = function () {
    return startOfDay($scope.endDate) < startOfDay(new Date())
  }

  $scope.save = function () {
    $notification.info('Sauvegarde en cours …')

    const httpVerb = isNew ? 'post' : 'put'
    const url = isNew ? '/api/messages' : '/api/messages/' + $scope.oid

    $http[httpVerb](url, {
      title: $scope.title,
      startDate: startOfDay($scope.startDate),
      endDate: endOfDay($scope.endDate),
      content: $scope.content,
      login: $scope.utilisateur.login
    }).then((response) => {
      if (!response.data.success && response.data.field) {
        $notification.error(response.data.message)
        $loader.hide()
        return
      }

      $loader.hide()
      $state.go('messages')
      $notification.success('Votre message a bien été sauvegardé.')
    }).catch((err) => {
      $errors.handle('Une erreur s’est produite durant la sauvegarde de votre message.')(err)
      $loader.hide()
    })
  }

  $app.ready(initialize)
}
