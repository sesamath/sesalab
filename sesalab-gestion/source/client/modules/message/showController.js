module.exports = function ($app, $scope, $http, $state) {
  'ngInject'

  $scope.oid = $state.params.oid
  $scope.title = ''
  $scope.content = ''
  $scope.date = null
  $scope.startDate = new Date()
  $scope.endDate = new Date()

  function initialize () {
    $http.get('/api/messages/' + $scope.oid)
      .then((response) => {
        $scope.title = response.data.message.title
        $scope.content = response.data.message.content
        $scope.startDate = new Date(response.data.message.startDate)
        $scope.endDate = new Date(response.data.message.endDate)
        $scope.date = response.data.message.date
      })
  }

  $scope.copy = function () {
    $state.go('message-edit', { oid: 'new', originalMessage: $scope.oid })
  }

  $app.ready(initialize)
}
