const _ = require('lodash')
const moment = require('moment-timezone')

module.exports = function ($app, $scope, $http, $errors, $notification, $loader, $state, $timeout) {
  'ngInject'

  $scope.messages = []
  $scope.oldMessages = []

  $scope.removeMessage = function (oid) {
    const message = _.find($scope.messages, { oid })
    // eslint-disable-next-line no-alert
    if (!message || !window.confirm('Supprimer ce message ?')) {
      return
    }

    $http.delete('/api/messages/' + oid)
      .then((response) => {
        if (!response.data.success && response.data.field) {
          $notification.error(response.data.message)
          $loader.hide()
          return
        }

        _.pull($scope.messages, message)
        $notification.success('Votre message a bien été supprimé.')
        $loader.hide()
      })
      .catch((err) => {
        $errors.handle('Une erreur s’est produite durant la suppression de votre message.')(err)
        $loader.hide()
      })
  }

  function initialize () {
    if (!$scope.utilisateur) {
      return
    }

    $http.get('/api/messages')
      .then((response) => {
        $scope.messages = _.remove(response.data.messages, function (o) { return moment(o.endDate).diff(new Date()) > 0 })
        $scope.oldMessages = response.data.messages
      })
  }

  $app.ready(initialize)
}
