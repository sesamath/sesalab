const _ = require('lodash')
const moment = require('moment')
// moment.HTML5_FMT.DATE n'existe qu'à partir de 2.2.20
const HTML5_FMT_DATE = 'YYYY-MM-DD'

module.exports = function ($app, $http, $notification, $loader, $scope, $state, $timeout) {
  'ngInject'
  $scope._ = _

  const error = {}
  $scope.periodForm = {}

  function initialize () {
    $scope.lastDays = app.settings.joursGestion

    // Initialisation des dates pour les stats sur les structures
    const startDate = new Date()
    startDate.setDate(startDate.getDate() - 7)
    $scope.periodForm.startDate = startDate
    $scope.periodForm.endDate = new Date()

    refresh()
  }

  $scope.errors = function (field, index) {
    if (field === error.field && index === error.index) return error.message
  }

  function refresh () {
    $loader.show()
    $scope.isSuccess = false

    const stateName = $state.current.name
    if (stateName === 'stats-hebdo') showHebdo()
    if (stateName === 'stats-ent') showEnt()
    if (stateName === 'stats-general') showGeneral()
    if (stateName === 'stats-structures') showStructures()
  }

  let refreshDelayPromise
  function delayedRefresh () {
    // On rafraichit la page si la date change puis ne change plus pendant au moins une seconde
    // pour éviter d'envoyer une requête par incrément
    $timeout.cancel(refreshDelayPromise)
    refreshDelayPromise = $timeout(refresh, 1000)
  }

  function getApi (url) {
    const exportUrl = url.replace('stats/', 'stats/export/')
    $http.get(url)
      .then((response) => {
        const { data } = response
        if (data.success && data.stats) {
          $scope.stats = data.stats
          $scope.exportUrl = exportUrl
          successCallback()
        } else if (data.message) {
          messageCallback(data.message)
        } else {
          errorCallback()
        }
      })
      .catch(errorCallback)
  }

  function showHebdo () {
    getApi('/api/stats/hebdo')
  }

  function showEnt () {
    getApi('/api/stats/ent')
  }

  function showGeneral () {
    getApi('/api/stats/general')
  }

  function showStructures () {
    // On récupère les jours de début et de fin
    const startDay = moment($scope.periodForm.startDate).format(HTML5_FMT_DATE)
    const endDay = moment($scope.periodForm.endDate).format(HTML5_FMT_DATE)
    getApi(`/api/stats/structures/${startDay}/${endDay}`)
  }

  function successCallback () {
    $scope.isSuccess = true
    $loader.hide()
  }
  function messageCallback (message) {
    $scope.isSuccess = false
    // @todo mettre le message dans la page
    $scope.errorMessage = message
    $loader.hide()
  }

  function errorCallback (error) {
    // en cas d'erreur 403 error est l'objet response
    if (error) console.error(error)
    else console.error(Error('errorCallback sans erreur'))
    $scope.isSuccess = false
    $scope.errorMessage = 'Impossible d’afficher ces statistiques : ' + ((error && error.data) || '')
    $loader.hide()
  }

  $scope.refreshStructure = function () {
    delayedRefresh()
  }

  $scope.errors = function (field, index) {
    if (field === error.field && index === error.index) return error.message
  }

  $scope.isInteger = function (value) {
    return Number.isInteger(value)
  }

  $scope.format = function (value) {
    if (!Number.isInteger(value)) {
      return value
    }

    return Number(value).toLocaleString('fr-FR')
  }

  $app.ready(initialize)
}
