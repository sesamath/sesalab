/**
 * Gestion de l'API.
 *
 * @service $api
 */
app.service('$api', function ($http) {
  'ngInject'

  /** Helper d'accès GET. */
  function get (service, cb) {
    $http.get('/api/' + service)
      .then((response) => {
        if (!response.data.success) return cb(null, { service, message: response.data.message })
        cb(null, response.data)
      })
      .catch(error => {
        console.error(`L’appel de api/${service} a planté`, error)
        cb(Error('Erreur interne'))
      })
  }

  /**
   * Récupération de l'utilisateur en cours.
   */
  function utilisateur (cb) {
    get('utilisateur', function (error, response) {
      if (error) return cb(error)
      let utilisateur = null
      if (response.utilisateur) {
        utilisateur = response.utilisateur
      }
      cb(null, { utilisateur })
    })
  }

  /**
   * Récupération des réglages courants.
   */
  function settings (cb) {
    get('settings?gestion=true', function (error, response) {
      if (error) return cb(error)
      cb(null, {
        settings: response.settings
      })
    })
  }

  return {
    utilisateur,
    settings
  }
})
