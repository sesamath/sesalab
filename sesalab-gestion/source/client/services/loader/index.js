app.service('$loader', function ($api, $session) {
  'ngInject'

  let domElement = null

  function ensureExist () {
    if (!domElement) {
      domElement = document.getElementById('loader')
    }
  }

  function show () {
    ensureExist()
    domElement.classList.add('show')
  }

  function hide () {
    ensureExist()
    domElement.classList.remove('show')
  }

  return {
    show,
    hide
  }
})
