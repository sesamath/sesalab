const constants = require('sesalab-commun/constants.js')

/**
 * Gestion de la session utilisateur.
 *
 * @service $session
 */
app.service('$session', function ($api, $window) {
  'ngInject'

  let _utilisateur = null

  /**
   * Connexion de l'utilisateur si une session
   * existe sur le serveur.
   */
  function connect (cb) {
    $api.utilisateur(function (error, data) {
      if (error) return cb(error)
      if (data.utilisateur) _utilisateur = data.utilisateur
      if (cb) cb()
    })
  }

  function disconnect () {
    _utilisateur = null
    $window.location.href = '/api/utilisateur/logout'
  }

  function isActive () {
    return _utilisateur != null
  }

  function isFormateur () {
    return isActive() && _utilisateur.type === constants.TYPE_FORMATEUR
  }

  function user () {
    return _utilisateur
  }

  return {
    connect,
    disconnect,
    isActive,
    isFormateur,
    user
  }
})
