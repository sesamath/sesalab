const flow = require('an-flow')

/**
 * Initialisation de l'application.
 */
app.service('$app', function ($api, $session) {
  'ngInject'

  let isReady = false

  /** Fonction de démarrage. */
  function ready (cb) {
    if (isReady) { return cb() }
    flow()
      .seq(function () {
        $api.settings(this)
      })
      .seq(function (response) {
        app.settings = response.settings
        $session.connect(this)
      })
      .seq(function () {
        isReady = true
        cb()
      })
      .catch(function (error) {
        console.error(error)
      })
  }

  return {
    ready
  }
})
