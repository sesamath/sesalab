/**
 * Gestion des erreurs.
 *
 * @service $errors
 */
app.service('$errors', function ($notification) {
  function handle (message) {
    return (error) => {
      if (message) $notification.error(message)
      if (error) console.error(error)
    }
  }

  return {
    handle
  }
})
