// checkBrowser & bugsnag seront ajouté avant tout ça par webpackConfigBuilder.js
import routing from '../client/routes.js'
import HomeController from '../client/modules/home/controller.js'
import MessageEditController from '../client/modules/message/editController.js'
import MessageListController from '../client/modules/message/listController.js'
import MessageShowController from '../client/modules/message/showController.js'
import StatsController from '../client/modules/stats/controller.js'
import ExportButton from '../client/widgets/exportButton/directive.js'

import '../styles/app.scss'

const angular = require('angular')
require('angular-sanitize')
require('@uirouter/angularjs')
require('angular-trix')
require('trix/dist/trix')
require('trix/dist/trix.css')
require('ionicons-npm/scss/ionicons.scss')
require('roboto-fontface/css/roboto/roboto-fontface.css')

window.app = angular.module('sesalab-gestion', ['ui.router', 'ngSanitize', 'angularTrix'])
app
  .config(routing)
  .controller('HomeController', HomeController)
  .controller('MessageListController', MessageListController)
  .controller('MessageShowController', MessageShowController)
  .controller('MessageEditController', MessageEditController)
  .controller('StatsController', StatsController)
  .directive('exportButton', ExportButton)

app.package = require('../../../package.json')

function requireAll (r) { r.keys().forEach(r) }
requireAll(require.context('./', true, /\/index.js$/))

app.run(function ($app, $session, $state, $window) {
  'ngInject'

  $app.ready(function () {
    console.info('UI gestion ready')

    if (!$session.isActive()) {
      $window.location.href = '/'
    } else {
      if (!$session.isFormateur()) {
        $window.location.href = app.settings.baseUrl + 'eleve'
      }
    }
  })
})
