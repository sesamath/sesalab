module.exports = ['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/')

    $stateProvider
      .state('home', {
        url: '',
        controller: 'HomeController'
      })
      .state('messages', {
        url: '/messages/',
        controller: 'MessageListController',
        template: require('../client/modules/message/list.html')
      })
      .state('message', {
        url: '/message/{oid}',
        controller: 'MessageShowController',
        template: require('../client/modules/message/show.html')
      })
      .state('message-edit', {
        url: '/message/{oid}/edit',
        controller: 'MessageEditController',
        params: {
          originalMessage: null
        },
        template: require('../client/modules/message/edit.html')
      })
      .state('stats-hebdo', {
        url: '/stats-hebdo',
        controller: 'StatsController',
        template: require('../client/modules/stats/hebdo.html')
      })
      .state('stats-ent', {
        url: '/stats-ent/',
        controller: 'StatsController',
        template: require('../client/modules/stats/ent.html')
      })
      .state('stats-general', {
        url: '/stats-general/',
        controller: 'StatsController',
        template: require('../client/modules/stats/general.html')
      })
      .state('stats-structures', {
        url: '/stats-structures/',
        controller: 'StatsController',
        template: require('../client/modules/stats/structures.html')
      })
  }
]
