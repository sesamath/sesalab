const webpackConfigBuilder = require('../webpackConfigBuilder')
const webpack = require('webpack')

const config = webpackConfigBuilder(__dirname, 'gestion')

// Define global variables to make lodash and jquery easily accessible
config.plugins.push(
  new webpack.ProvidePlugin({
    jQuery: 'jquery',
    _: 'lodash'
  })
)

module.exports = config
