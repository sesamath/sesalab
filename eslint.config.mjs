// Cf https://eslint.org/docs/latest/use/configure/
// et https://eslint.org/docs/latest/use/configure/migration-guide

// pour forcer les imports sur une seule ligne (pour rechercher avec regex tous les fichiers qui utilisent une fct)
// cf https://github.com/SeinopSys/eslint-plugin-import-newlines
import importNewlines from 'eslint-plugin-import-newlines'
import globals from 'globals'
import neostandard from 'neostandard'

export default [
  // les fichiers à ignorer (https://eslint.org/docs/latest/use/configure/ignore#ignoring-files)
  // ATTENTION : `only global ignores patterns can match directories`
  // => ça doit être précisé ici dans le premier objet de la config
  {
    plugins: { importNewlines },
    rules: {
      // https://github.com/SeinopSys/eslint-plugin-import-newlines#readme
      'importNewlines/enforce': [
        'error',
        {
          items: 1000,
          forceSingleLine: true,
          semi: false
        }
      ]
    },
  },

  // la conf standard, cf https://github.com/neostandard/neostandard?tab=readme-ov-file#configuration-options
  ...neostandard({
    globals: {
      ...globals.browser,
      ...globals.jquery,
      app: true,
      lassi: true,
      sesalab: true,
    },
    ignores: [
      '**/node_modules',
      '_private',
      '**/public',
      '**/tmp',
      'sesalab-formateur/qooxdooBuilder',
      'sesalab-formateur/source/client/classes.js',
      'sesalab-formateur/source/client/qooxdoo.d.ts',
      'sesalab-formateur/source/assets/qooxdoo.js',
      'qooxdoo.js',
      'docs',
      '**/qx'
    ]
  }),

  // options communes
  {
    files: ['**/*.js'],
    languageOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module',
    },
    // avec qq ajouts
    rules: {
      // un alert() doit lancer une erreur
      'no-alert': 'error',
      // et un console.log aussi (on autorise console.error, warn, …)
      'no-console': ['error', { allow: ['error', 'warn', 'info', 'debug'] }]
    }
  },

  // règles qooxdoo
  {
    files: ['sesalab-formateur/**/*.js'],
    rules: {
      'new-cap': 'off'
    },
    languageOptions: {
      globals: {
        qx: true
      }
    }
  },

  // fichiers en es5
  {
    files: [
      'sesalab-commun/checkBrowser.js',
      'sesalab-commun/preLoad.js'
    ],
    languageOptions: {
      ecmaVersion: 5,
      sourceType: 'script'
    },

    rules: {
      'no-var': 'off',
      'prefer-const': 'off'
    }
  },

  // la partie node
  {
    files: ['sesalab-api/**/*', 'sesalab-formateur/scripts/**/*.js', 'source/**/*.js', 'docker/**/*.js', 'webpackConfigBuilder.js'],
    languageOptions: {
      globals: {
        ...globals.node
      }
    },
    rules: {
      'no-console': 'off'
    }
  },

  // les tests
  {
    files: ['test/**/*.js'],
    rules: {
      // ça c’est pour les trucs du genre `expect(value).to.be.true`
      'no-unused-expressions': 'off',
      'no-console': 'off'
    },
    languageOptions: {
      globals: {
        $$: true,
        browser: true,
        ...globals.chai,
        ...globals.mocha,
        ...globals.node,
        sinon: true,
      }
    },
  }
]
