'use strict'
const flow = require('an-flow')

module.exports = {
  name: 'Réindexe les séquences (pour ajouter les rids des ressources)',
  description: '',
  isNotBlocking: true,
  run: (done) => {
    // on utilise le cli de lassi
    const $entitiesCli = require('lassi/source/services/entities-cli')
    const reindexAll = $entitiesCli().commands().reindexAll
    flow().seq(function () {
      reindexAll('Sequence', 'continueOnError', this)
    }).done(done)
  }
}
