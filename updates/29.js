const flow = require('an-flow')
// const { stringify } = require('sesajstools')
const appLog = require('sesalab-api/source/tools/appLog')

const firstDate = new Date('2023-08-16')

module.exports = {
  name: 'Valide élève importés',
  description: 'Valide et réindexe les élèves importés et non validés',
  isNotBlocking: true,
  run: (done) => {
    const log = appLog.getLogger('update29')
    log('starting update 29')
    let nbEleves = 0
    let nbElevesMod = 0

    const check = (eleve, next) => {
      nbEleves++
      if (nbEleves % 1000 === 0) log(`traitement élève ${nbEleves}`)
      if (eleve.validators?.account) return next()
      // l'import ne remplissait pas ça
      if (!eleve.validators) eleve.validators = {}
      eleve.validators.account = {
        accepted: true,
        responseDate: eleve.created
      }
      nbElevesMod++
      return eleve.store(next)
    }

    const Utilisateur = lassi.service('Utilisateur')
    // MongoNotConnectedError: Client must be connected before running operations
    // => setTimeout
    setTimeout(() => {
      flow().seq(function () {
        Utilisateur
          .match('created').after(firstDate)
          .match('isValid').notEquals(true)
          .match('type').equals(0)
          .forEachEntity(check, this)
      }).seq(function () {
        log(`${nbElevesMod} modifiés sur ${nbEleves} traités`)
        done()
      }).done(done)
    }, 10_000)
  }
}
