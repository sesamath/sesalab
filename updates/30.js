const flow = require('an-flow')
// const { stringify } = require('sesajstools')
const appLog = require('sesalab-api/source/tools/appLog')

const ressProps = ['titre', 'type', 'rid', 'aliasRid', 'cle']

// le nom du fichier sans extension
const updateNum = __filename.substring(__dirname.length + 1, __filename.length - 3)

module.exports = {
  name: 'Nettoie les ressources des propriétés inutiles dans les séquences',
  description: 'Ne conserve que les propriétés titre/rid/aliasOf',
  isNotBlocking: true,
  run: (done) => {
    const log = appLog.getLogger(`update${updateNum}`, { logLevel: 'info' })
    log(`starting update ${updateNum}`)
    let nbSeqs = 0
    let nbSeqsMod = 0

    // traite une séquence
    const clean = (sequence, next) => {
      nbSeqs++
      if (nbSeqs % 1000 === 0) log(`traitement séquence ${nbSeqs}`)
      let hasChanged = false
      // on a trouvé une séquence avec cette propriété supprimée depuis des années
      if (sequence.path) {
        console.error(`La séquence ${sequence.oid} contenait une propriété path (${sequence.path})`)
        delete sequence.path
        hasChanged = true
      }

      for (const ssSeq of sequence.sousSequences) {
        for (const ressource of ssSeq.serie) {
          for (const key of Object.keys(ressource)) {
            if (!ressProps.includes(key)) {
              hasChanged = true
              delete ressource[key]
            }
          }
        }
      }
      // log(`séquence ${sequence.oid} ${hasChanged ? 'a changé' : 'n’a pas changé'} (${nbSeqs})`)
      if (!hasChanged) return next()
      nbSeqsMod++
      sequence.store((error) => {
        // en cas de pb on log et on continue
        if (error) log.error(error, 'sur la séquence', sequence)
        next()
      })
    }

    const Sequence = lassi.service('Sequence')
    const limit = 100
    let skip = 0

    // le batch qui récupère un paquet de séquences et les traite
    const grab = () => {
      let nb = 0
      flow().seq(function () {
        Sequence.match().includeDeleted().grab({ limit, skip }, this)
      }).seqEach(function (sequence) {
        nb++
        clean(sequence, this)
      }).seq(function () {
        if (nb < limit) {
          log(`Fin update ${updateNum} : ${nbSeqsMod} séquences modifiée (sur ${nbSeqs} sequences analysées)`)
          return done()
        }
        skip += limit
        process.nextTick(grab)
      }).catch(done)
    }

    // et on lance
    grab()
  }
}
