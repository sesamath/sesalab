const path = require('path')
const fs = require('fs')

const autoprefixer = require('autoprefixer')
const glob = require('glob')

const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
// cf https://docs.bugsnag.com/build-integrations/webpack/
const { BugsnagSourceMapUploaderPlugin } = require('webpack-bugsnag-plugins')

const { version, babel: babelConfig } = require('./package.json')
const settings = require('./config')

const isProd = process.env.NODE_ENV === 'production'

function rmAll (dir, parentToo = true) {
  fs.readdirSync(dir).forEach(entry => {
    const absPath = path.resolve(dir, entry)
    if (fs.lstatSync(absPath).isDirectory()) return rmAll(absPath)
    fs.unlinkSync(absPath)
  })
  if (parentToo) fs.rmdirSync(dir)
}

function webpackConfigBuilder (modulePath, publicPath = '/') {
  // ajout du slash de début s'il manque
  if (publicPath[0] !== '/') publicPath = '/' + publicPath
  const buildDir = path.join(modulePath, 'public')
  // home|formateur|…, plus besoin
  // const sesalabApp = path.basename(modulePath).replace('sesalab-', '')

  try {
    if (fs.existsSync(buildDir)) {
      rmAll(buildDir, false)
      console.log(buildDir, 'vidé')
    } else {
      fs.mkdirSync(buildDir)
      console.log(buildDir, 'créé')
    }
  } catch (error) {
    console.error(error)
    console.log('La suppression des anciens fichiers de build a planté mais on continue la compilation')
  }

  const appEntry = path.join(modulePath, './source/client/app.js')
  const checkBrowser = path.join(__dirname, './sesalab-commun/checkBrowser.js')
  const bugsnag = path.join(__dirname, './sesalab-commun/bugsnag.js')
  const polyfill = path.join(__dirname, './sesalab-commun/polyfill.js')

  if (!fs.existsSync(appEntry)) {
    console.error(`${appEntry} n’existe pas => skip webpack build`)
    return {}
  }
  console.log(`building ${appEntry}`)

  const jsRules = [
    {
      loader: 'ng-annotate-loader',
      options: {
        ngAnnotate: 'ng-annotate-patched',
        es6: true,
        explicitOnly: false
      }
    },
    {
      loader: 'babel-loader',
      options: babelConfig
    }
  ]

  const config = {
    mode: isProd ? 'production' : 'development',
    context: modulePath,
    entry: {
      app: [checkBrowser, bugsnag, polyfill, appEntry],
      // @todo virer cette entrée et importer ces css dans les js
      style: glob.sync(modulePath + '/**/app.scss').concat(glob.sync(modulePath + '/**/styles.scss'))
    },
    output: {
      path: buildDir,
      filename: '[name].js',
      publicPath
    },
    resolve: {
      extensions: ['.js'],
      modules: ['node_modules', path.resolve(__dirname), path.resolve(__dirname, 'node_modules', '.registry.npmjs.org')]
    },
    resolveLoader: {
      alias: {
        'config-loader': path.join(__dirname, './config/webpackConfigLoader.js'),
        'throw-loader': path.join(__dirname, './source/webpackThrowLoader.js')
      }
    },
    module: {
      rules: [
        {
          test: /\.s?css$/,
          rules: [
            { loader: MiniCssExtractPlugin.loader },
            { loader: 'css-loader' },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [
                  // il prend sa liste de browsers dans package.json:browserslist
                  autoprefixer()
                ]
              }
            },
            { test: /\.scss$/, loader: 'sass-loader' }
          ]
        },
        {
          test: /\.(jpeg|jpg|woff|woff2|eot|ttf|svg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 10000
              }
            }
          ]
        },
        {
          test: /\.html/,
          loader: 'html-loader'
        },
        {
          // On empêche de require un fichier du répertoire _private
          test: /_private/,
          loader: 'throw-loader',
          exclude: /node_modules/
        },
        {
          test: /node_modules\/sesatheque-client\/src\/.*\.js$/,
          rules: jsRules
        },
        {
          // On empêche de charger directement la config qui contient des données sensibles
          // config/index.js
          // ou _private/config.js
          test: /config\/index\.js/,
          loader: 'config-loader',
          exclude: /node_modules/
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          rules: jsRules
        },
        // faut aussi passer sesajs-date par babel
        {
          test: /\/sesajs-date\/.*\.js/,
          rules: jsRules
        }
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].css'
      }),
      new CopyWebpackPlugin([
        { from: './source/assets' }
      ]),
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/) // cf. http://stackoverflow.com/a/25426019/1878592
    ],
    // toujours source-map, eval rend le code illisible en console ou dans les js de build
    devtool: 'source-map',
    node: {
      fs: 'empty',
      cluster: 'empty',
      // Isemail has dependency on node's dns to use it in the browser
      dns: 'mock'
    },
    stats: {
      // indique l'heure de build, très utile en mode watch
      builtAt: true,
      // Nice colored output
      colors: true
    }
  }

  if (isProd) {
    config.optimization = {
      minimizer: [
        new TerserPlugin({
          // https://webpack.js.org/plugins/terser-webpack-plugin/
          // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
          cache: true,
          parallel: true,
          // par défaut il laisse N fois les commentaires identiques (avec @licence) dans le fichier minifié
          // on ne veut pas extraire les commentaires dans un fichier .LICENSE séparé,
          // seulement n'en garder qu'un, d'où le output.comments ci-dessous
          extractComments: false,
          sourceMap: true, // Must be set to true if using source-maps
          // pour terserOptions cf https://github.com/terser-js/terser#minify-options
          terserOptions: {
            output: {
              // les seuls qu'on garde dans le fichier minifié
              // https://github.com/webpack-contrib/terser-webpack-plugin#preserve-comments
              // Attention, la chaîne passée à la regex démarre après "/*" (ou //)
              comments: /^\**!/
            },
            // https://github.com/webpack-contrib/terser-webpack-plugin#warningsfilter
            // avec 'verbose' c'est vraiment très verbeux, avec plein de warning qui n'en sont pas vraiment
            // (par ex il vire une variable utilisée une seule fois sur la ligne suivante,
            // mais c'est souvent fait exprès et bienvenu pour la lisibilité)
            // warnings: 'verbose'
            warnings: true
          }
          // cf https://github.com/webpack-contrib/terser-webpack-plugin#warningsfilter
          // mais en 4.0.0 the warningsFilter option was removed without replacement :-(
        }),
        new OptimizeCSSAssetsPlugin({})
      ]
    }
  }

  const pathToEntry = './source/assets/index.ejs'
  if (fs.existsSync(path.join(modulePath, pathToEntry))) {
    const getDomain = (url) => {
      const chunks = /https?:\/\/([^/]+)/.exec(url)
      if (chunks && chunks[1]) return chunks[1]
    }
    // pour content security policy
    const otherCspDomains = settings.application.otherCspDomains || []
    // et l'aide
    const heplDomain = getDomain(settings.application.aide.homepage)
    if (!otherCspDomains.includes(heplDomain)) otherCspDomains.push(heplDomain)
    // et les sésathèques
    const cspDomains = settings.application.sesatheques.map(({ baseUrl }) => getDomain(baseUrl)).concat(otherCspDomains).join(' ')
    config.plugins.push(
      new HtmlWebpackPlugin({
        title: settings.application.name,
        version,
        sesathequeBaseUrl: settings.application.sesatheques[0].baseUrl,
        cspDomains,
        template: pathToEntry,
        inject: false
      })
    )
  }

  const navigObsoletePath = './source/assets/navigateurObsolete.ejs'
  if (fs.existsSync(path.join(modulePath, navigObsoletePath))) {
    config.plugins.push(
      new HtmlWebpackPlugin({
        bibliBaseUrl: settings.application.sesatheques[0].baseUrl,
        template: navigObsoletePath,
        filename: 'navigateurObsolete.html',
        inject: false
      })
    )
  }

  // le push des sources.map chez bugsnag si on a une apiKey
  if (settings.bugsnag?.apiKey) {
    // cf https://docs.bugsnag.com/build-integrations/webpack/#source-map-uploader
    const bsPlugin = new BugsnagSourceMapUploaderPlugin({
      apiKey: settings.bugsnag.apiKey,
      appVersion: version,
      ignoredBundleExtensions: ['.css', '.scss', '.woff', '.woff2', '.ttf', '.svg', '.gif', '.ico', '.png', '.jpg', '.jpeg', '.eot', '.ejs', '.html'],
      overwrite: true
    })
    config.plugins.push(bsPlugin)
  }

  return config
}

module.exports = webpackConfigBuilder
