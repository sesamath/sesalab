/**
 * Gestion de l'API.
 *
 * @service $api
 */
app.service('$api', function ($http) {
  'ngInject'

  /** Helper d'accès GET. */
  function get (service, cb) {
    $http.get('/api/' + service)
      .then((response) => {
        if (!response.data.success) {
          return cb(null, { service, message: response.data.message })
        }
        cb(null, response.data)
      })
      .catch(error => {
        console.error(`L’appel de api/${service} a planté`, error)
        cb(Error('Erreur interne'))
      })
  }

  /**
   * Récupération des réglages courants.
   */
  function settings (cb) {
    get('settings?home=true', cb)
  }

  return {
    settings
  }
})
