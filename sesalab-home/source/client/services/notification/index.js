app.service('$notification', function () {
  'ngInject'

  const LIFE_TIME = 50000 // Durée de vie en millisecondes
  let domElement = null
  let timer = null

  function ensureExist () {
    if (!domElement) domElement = document.getElementById('notification')
  }

  function show (message, type) {
    ensureExist()
    domElement.innerHTML = message.replace(/\n/g, '<br>')
    domElement.className = 'notification show ' + type

    // Auto-hide
    if (timer) clearTimeout(timer)
    timer = setTimeout(function () {
      domElement.classList.add('hide')
      domElement.classList.remove('show')
    }, LIFE_TIME)
  }

  function error (message) {
    show(message, 'error')
  }

  function info (message) {
    show(message, 'info')
  }

  function success (message) {
    show(message, 'success')
  }

  return {
    info,
    error,
    success
  }
})
