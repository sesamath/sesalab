const flow = require('an-flow')

/**
 * Initialisation de l'application.
 */
app.service('$app', function ($api, $notification) {
  'ngInject'

  let isReady = false

  function ready (cb) {
    if (isReady) return cb()
    flow().seq(function () {
      $api.settings(this)
    }).seq(function (response) {
      // si on avait déjà une session settings nous indique où aller
      if (response.redirectTo) {
        let message = response.message || ''
        message += '\nRedirection dans 2s'
        $notification.info(message)
        setTimeout(() => { window.location.href = response.redirectTo }, 2000)
        return
      }
      isReady = true
      app.settings = response.settings
      cb()
    }).catch(function (error) {
      console.error(error)
    })
  }

  return {
    ready
  }
})
