module.exports = ['$stateProvider', '$urlRouterProvider', '$interpolateProvider',
  function ($stateProvider, $urlRouterProvider, $interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]')

    $urlRouterProvider.otherwise('/')

    $stateProvider
      .state('home', {
        url: '',
        controller: 'HomeController'
      })
  }
]
