const _ = require('lodash')
const isEmail = require('sesalab-commun/isEmail')
const { notify } = require('sesalab-commun/notify')

module.exports = function ($http, $notification, $scope, $window) {
  'ngInject'

  let _error = {}

  function initialize () {
    $scope.users = []
    $scope.user = {}
    $scope.doAddUser()
    $scope.structures = []
    $scope.structureLabel = ''
    $scope.structureCode = ''
    $scope.showStructure = false
    $scope.reinitializeMail = ''
    $scope.showReinitialize = false
    $scope.newPassword = null
    $scope.link = ''
    $scope.validationTarget = null
  }

  $scope.confirmAccountRemoving = function (userId, token) {
    $http
      .get(`/api/utilisateur/removeAccount/${userId}/${token}`)
      .then(() => {
        $window.location.href = '/'
      })
  }

  $scope.validateEmail = function (userId, token) {
    $http
      .get(`/api/utilisateur/validateMail/${userId}/${token}`)
      .then((response) => {
        if (!response.data.success) {
          $notification.error(response.data.message)
          return
        }

        $scope.validationTarget = response.data.validationTarget
      })
  }

  $scope.validateNewMail = function (userId, token) {
    $http
      .get(`/api/utilisateur/validateNewMail/${userId}/${token}`)
      .then((response) => {
        if (!response.data.success) {
          $notification.error(response.data.message)
          return
        }

        $scope.validationTarget = 'user'
      })
  }

  $scope.confirmPasswordReset = function (userId, token) {
    $http
      .get(`/api/utilisateur/reinitialize-password/${userId}/${token}`)
      .then((response) => {
        if (!response.data.success) {
          $notification.error(response.data.message)
          return
        }

        $scope.link = response.data.link
        $scope.newPassword = response.data.password
      })
  }

  $scope.doDisplayHelp = function () {
    $window.open(app.settings.aide.comptes, '_blank')
  }

  $scope.doConnect = function () {
    _error = {}
    $http.post('/api/login', {
      // le code est le début de la string qui a pu être ajoutée via autocomplétion, sinon le code saisi
      structureCode: $scope.structureLabel.split(/\s/)[0],
      users: $scope.users
    })
      .then((response) => {
        if (!response.data.success) {
          if (response.data.field === 'structure') $scope.showStructure = true
          else $notification.error(response.data.message)
          _error = response.data // cette ligne sert à qqchose ?
          return
        }
        $window.location.href = response.data.appUrl
      })
  }

  $scope.errors = function (field, index) {
    if (field === _error.field && index === _error.index) return _error.message
  }

  $scope.doAddUser = function () {
    if ($scope.users.length <= 2) {
      $scope.users.push({
        login: '',
        password: ''
      })
    } else {
      $notification.error('Attention, vous ne pouvez pas vous connecter à plus de 3 élèves simultanément')
    }
  }

  $scope.doRemoveUser = function (index) {
    $scope.users.splice(index, 1)
  }

  let timer
  $scope.doKeydown = function () {
    if (timer === false) return
    clearTimeout(timer)
    timer = setTimeout(function () {
      timer = false
      // on cherche sur le premier mot saisi
      const firsWord = $scope.structureLabel.split(/\s/)[0]
      $http.get('/api/structure?_nom=' + firsWord)
        .then((response) => {
          $scope.structures = response.data.structures
          timer = undefined
        })
    }, 1000)
  }

  $scope.doSelectStructure = function (structure) {
    if (!structure?.oid) return notify(Error('aucune structure fournie'))
    $http.get('/api/groupes/by-structure/' + structure.oid)
      .then((response) => {
        $scope.classes = _.values(response.data.groupes)
        $scope.structureHasClasses = false
        if ($scope.classes.length) {
          $scope.structureHasClasses = true
          $scope.user.classe = $scope.classes[0]
        }

        $scope.structureCode = structure.code
        let structureLabel = structure.code + ' - ' + structure.nom
        structureLabel += structure.ville ? ' (' + structure.ville + ')' : ''
        $scope.structureLabel = structureLabel
        $scope.ajoutElevesAuthorises = structure.ajoutElevesAuthorises
        $scope.structureNom = structure.nom
        $scope.structures = []
      })
  }

  $scope.doReinitializePassword = function () {
    $scope.showReinitialize = !$scope.showReinitialize

    $scope.doReinitialize = function () {
      if (!$scope.checkEmail()) return

      _error = {}
      $http.put('/api/utilisateur/reinitialize-password', {
        mail: $scope.reinitializeMail
      })
        .then((response) => {
          if (!response.data.success) {
            _error = response.data
          } else {
            $notification.info(response.data.message)
            initialize()
          }
        }, (response) => {
          _error.field = 'mail'
          _error.message = response.data
        })
    }

    $scope.checkEmail = function () {
      if (!$scope.reinitializeMail) {
        return false
      }

      return isEmail.validate($scope.reinitializeMail)
    }
  }

  initialize()
}
