// checkBrowser & bugsnag seront ajouté avant tout ça par webpackConfigBuilder.js

import routing from '../client/routes.js'
import HomeController from '../client/home/controller.js'

import '../styles/app.scss'

const angular = require('angular')
require('angular-sanitize')
require('@uirouter/angularjs')

window.app = angular.module('sesalab-home', ['ui.router', 'ngSanitize'])
app
  .config(routing)
  .controller('HomeController', HomeController)

app.package = require('../../../package.json')
const log = require('an-log')('app')

function requireAll (r) { r.keys().forEach(r) }
requireAll(require.context('./', true, /\/index.js$/))

app.run(function ($app, $state) {
  'ngInject'
  $app.ready(function () {
    log('ready')
  })
})
