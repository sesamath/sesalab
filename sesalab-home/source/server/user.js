/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SesaReactComponent (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de SesaReactComponent, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'
const flow = require('an-flow')

/**
 * Controleur /user/ qui gère les actions propres à l'utilisateur.
 */
app.controller(function (Utilisateur, $settings) {
  const { getBaseData } = require('./viewHelper')($settings)
  // ce qui ne dépend que de la conf
  const baseUrl = $settings.get('application.baseUrl', '/')

  /**
   * Gestion commune du token/user.
   *
   * @private
   * @param {string} title Titre de la page
   * @param {string} blocId Identifiant de bloc, c'est views/user.dust qui inclue views/partials/{blocId}.dust
   * @param {Context} context
   * @param {string} userId oid de l'utilisateur
   * @param {string} token Token unique
   * @param {object} additionalData Données supplémentaires à transmettre au template blocId
   */
  function handleView (title, blocId, context, userId, token, additionalData = {}) {
    const baseData = getBaseData(title, 'user')
    const userLayoutData = {
      userId,
      token,
      [blocId]: true
    }
    const data = Object.assign({}, baseData, additionalData, userLayoutData)

    context.setNoCache()
    context.html(data)
  }

  /**
   * Page de suppression de compte.
   *
   * @route {GET} /supprimer-compte
   * @routeparam {string} token Token de vérification lié à un validator
   */
  const removeAccountPath = $settings.get('application.removeAccountPath', 'supprimer-compte')
  this.get(`${removeAccountPath}/:userId/:token`, function (context) {
    handleView('Supprimer mon compte', 'removeAccount', context, context.arguments.userId, context.arguments.token)
  })

  /**
   * Page de réinitialisation de mot de passe.
   *
   * @route {GET} /reinitialiser-mot-de-passe
   * @routeparam {string} token Token de vérification lié à un validator
   */
  const resetPasswordPath = $settings.get('application.resetPasswordPath', 'reinitialiser-mot-de-passe')
  this.get(`${resetPasswordPath}/:userId/:token`, function (context) {
    handleView('Réinitialiser le mot de passe', 'resetPassword', context, context.arguments.userId, context.arguments.token)
  })

  /**
   * Page de validation d'adresse email.
   *
   * @route {GET} /valider-email
   * @routeparam {string} token Token de vérification lié à un validator
   */
  const validateMailPath = $settings.get('application.validateMailPath', 'valider-email')
  this.get(`${validateMailPath}/:userId/:token`, function (context) {
    const { userId, token } = context.arguments
    flow().seq(function () {
      Utilisateur.match('oid').equals(userId).grabOne(this)
    }).seq(function (utilisateur) {
      const additionalData = {
        baseUrl,
        userExist: Boolean(utilisateur),
        mailValidated: Boolean(utilisateur?.validators?.mail?.responseDate),
        mail: (utilisateur?.mail) || ''
      }

      handleView('Valider mon compte', 'validateMail', context, userId, token, additionalData)
    }).catch(context.next)
  })

  /**
   * Page de validation d'une nouvelle adresse email.
   *
   * @route {GET} /valider-nouvel-email
   * @routeparam {string} userId Identifiant de l'utilisateur
   * @routeparam {string} token Token de vérification lié à un validator
   */
  const validateNewMailPath = $settings.get('application.validateNewMailPath', 'valider-nouvel-email')
  this.get(`${validateNewMailPath}/:userId/:token`, function (context) {
    const { userId, token } = context.arguments
    flow().seq(function () {
      Utilisateur.match('oid').equals(userId).grabOne(this)
    }).seq(function (utilisateur) {
      const additionalData = {
        baseUrl,
        userExist: Boolean(utilisateur)
      }
      handleView('Valider ma nouvelle adresse email', 'validateNewMail', context, userId, token, additionalData)
    }).catch(context.next)
  })
})
