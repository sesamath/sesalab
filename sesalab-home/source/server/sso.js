/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SesaReactComponent (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de SesaReactComponent, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

const appLog = require('sesalab-api/source/tools/appLog')
// const { fetchJson } = require('sesalab-commun/utils')

// on le require ici pour être sûr qu'il soit déclaré avant nous
require('sesalab-api/source/session/service')

/**
 * Controleurs /sso/ qui gère la propagation vers les sésathèques et le logout sur les sésathèques
 * Il utilise le module sesalab-sso (code commun avec les Sésathèques) qui a d'autres controleurs sur /sesalabSso/ pour gérer les échanges de tickets avec ses clients (des sésathèques)
 */
app.controller(function ($session, $settings, $sesalabSsoServer, $staticHtml) {
  const { printError } = require('./viewHelper')($settings)

  const appName = $settings.get('application.name', 'Sesalab')
  const homeLink = $settings.get('application.baseUrl', '/')

  /**
   * Affiche la page de logout (et le fait)
   * @param context
   * @param text
   */
  function printLogoutPage (context, text) {
    context.setNoCache()
    let content = `<div class="bloc"><p>${text}</p>\n`
    $sesalabSsoServer.logoutOnClients(context)
      .then(origins => {
        for (const origin of origins) {
          content += `<p>Déconnexion de ${origin} effectuée</p>\n`
        }
        content += `<p>Retour à <a href="${homeLink}">l’accueil</a></p></div>`
        // on purge tout
        $session.flush(context)
        $staticHtml.displayRaw(context, { content, title: 'Déconnexion' })
      }).catch(error => {
        console.error(error)
        // on purge tout
        $session.flush(context)
        const content = 'Désolé, une erreur est survenue lors de la déconnexion. Si vous êtes toujours authentifié fermez le navigateur pour vous déconnecter.'
        $staticHtml.displayRaw(context, { content, title: 'Erreur à la déconnexion' })
      })
  }

  // on initialise notre page d'erreur en cb
  $sesalabSsoServer.setErrorCallback(function (context, error) {
    context.isSsoError = true
    printError(context, error)
  })

  /**
   * Après un login réussi, le js client qui a posté login/pass récupère cette url et redirige dessus,
   * C'est le point d'entrée pour gérer les redirections successives pour faire du SSO avec les sesatheques.
   *
   * @route {GET} /sso/sesatheques
   */
  this.get('sso/sesatheques', function (context) {
    // pour tester ce fonctionnement en local, se logguer normalement
    // puis aller modifier la conf des sesatheques ($cache.redis.prefix) et les redémarrer
    context.setNoCache()
    const currentFormateur = $session.getCurrentFormateur(context)
    if (currentFormateur) {
      // il faut ajouter la propriété pid si elle n'y est pas déjà
      if (!currentFormateur.pid) {
        if (currentFormateur.externalMech && currentFormateur.externalId) currentFormateur.pid = `${currentFormateur.externalMech}/${currentFormateur.externalId}`
        else currentFormateur.pid = `${app.settings.application.baseId}/${currentFormateur.oid}`
      }
      appLog(`GET /sso/sesatheques avec user ${currentFormateur.login} en session (pid ${currentFormateur.pid}), on lance loginOnClients`)
      $sesalabSsoServer.loginOnClients(context, currentFormateur)
    } else {
      appLog.error(Error('Appel de sso/sesatheques sans formateur en session, redirect vers /'))
      context.redirect('/?error=' + encodeURIComponent('La tentative de propagation d’authentification a échouée, il faut être authentifié ici avant.'))
    }
  })

  /**
   * Affiche le résultat de la déconnexion chez tous les clients sesalab-sso (les sésathèques)
   * On arrive là après /api/utilisateur/logout dans le cas d'un formateur sans externalMech
   * @route {GET} /sso/logout
   */
  this.get('sso/logout', function (context) {
    const text = $session.isAuthenticated(context) ? 'Déconnexion locale effectuée' : 'Vous n’étiez pas ou plus authentifié localement'
    printLogoutPage(context, text)
  })

  /**
   * Affiche le résultat de la déconnexion chez tous les clients sesalab-sso (les sésathèques)
   * On arrive là après /api/utilisateur/logout pour un externalMech sans getUrlDeconnexion
   * @route {GET} /sso/logoutExternal
   */
  this.get('sso/logoutExternal', function (context) {
    printLogoutPage(context, `Vous êtes maintenant déconnecté de ${appName}, vous pouvez vous reconnecter en passant par votre ENT.`)
  })

  /**
   * Affiche une erreur au retour d'un client (c'est le paramètre
   * authServer.errorPage dans la conf des app clientes).
   *
   * @route {GET} /sso/error
   */
  this.get('sso/error', function (context) {
    context.setNoCache()
    context.isSsoError = true
    if (context.get.error) {
      printError(context)
    } else {
      printError(context, new Error('Une erreur est survenue lors de la propagation de l’authentification vers les serveurs de contenus'))
    }
  })
})
