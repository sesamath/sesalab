/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SesaReactComponent (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de SesaReactComponent, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

const path = require('path')
const version = require('../../../package.json').version

module.exports = function ($settings) {
  /**
   * Ajoute un bloc de contenu
   * @param {object} data
   * @param {object|string} content
   */
  function addContentBloc (data, content) {
    if (typeof data !== 'object') throw new Error('Pas de données pour la vue')
    if (!content) throw new Error('Pas de contenu à ajouter')
    if (!data.mainContent) data.mainContent = {}
    if (!data.mainContent.blocs) data.mainContent.blocs = []
    data.mainContent.blocs.push(content)
  }
  /**
   * Ajoute une erreur dans le bloc des erreurs de la page
   * @param {object} data
   * @param {string} errorMessage
   */
  function addError (data, errorMessage) {
    if (typeof data !== 'object') throw new Error('Pas de données pour la vue')
    if (!errorMessage) throw new Error('Pas d’erreur à ajouter')
    if (!data.errors) data.errors = {}
    if (!data.errors.messages) data.errors.messages = []
    data.errors.messages.push(errorMessage)
  }

  /**
   * Retourne un objet data préparé pour layout-page ($metas.css & js sont des tableaux vides)
   * @param {string} title
   * @param {string} layout Layout à utiliser
   * @return {{$metas: {title: string, css: string[], js: string[]}, $layout: string, $views: string, appName: string, title: string, version: string}}
   */
  function getBaseData (title = '', layout = 'layout-page') {
    return {
      // Lassi génère des tags head avec
      $layout: layout,
      // Dossier des vues (pourrait être passé de manière générique dans settings.application.defaultViewsPath)
      $metas: {
        css: [],
        js: [],
        title
      },
      $views: path.join(__dirname, '/../views'),
      // Ensuite, chaque propriété est passée au layout :
      // - si c'est une string, directement
      appName,
      title,
      version
      // - si c'est un objet, il sera rendu avant d'être passé au layout
    }
  }

  /**
   * Affiche une erreur avec le template de base (on sert de callback)
   * @param {Context} context
   * @param {string} [error=Erreur interne]
   */
  function printError (context, error) {
    if (!context || !context.setNoCache) throw new Error('Pas de contexte')
    if (!error && !context.get.error) error = 'Erreur interne'
    context.setNoCache()
    const data = getBaseData('Erreur')
    if (error) {
      console.error(error)
      const strError = typeof error === 'string' ? error : error.toString()
      addError(data, strError)
    }
    // si error est vide c'est dans l'url et traité en js dans la page

    // isSsoError mis par le setErrorCallback dans sso.js
    if (context.isSsoError) addContentBloc(data, 'Faire une nouvelle <a href="/sso/sesatheques">tentative</a>.')
    addContentBloc(data, 'Retour à <a href="/">l’accueil</a>')
    context.html(data)
  }

  const appName = $settings.get('application.name', 'Sesalab')

  return {
    addContentBloc,
    addError,
    getBaseData,
    printError
  }
}
