app.service('$staticHtml', function (Groupe, Sequence, Utilisateur, $settings) {
  const { getBaseData } = require('./viewHelper')($settings)

  // constantes d'après les settings
  const appName = $settings.get('application.name', 'Sésalab')
  const version = $settings.get('application.version', '0.0.0')
  const sesathequeBaseUrl = $settings.get('application.sesatheques')?.[0]?.baseUrl

  return {
    /**
     * Affiche le title dans un cadre orange en haut de page et le html de content dans un bloc
     * (passe par context.html() et utilise donc dust)
     * @param context
     * @param title
     * @param content
     */
    display: function display (context, title, content) {
      const data = getBaseData(title)
      if (sesathequeBaseUrl) data.sesathequeBaseUrl = sesathequeBaseUrl
      // on ajoute notre contenu
      data.mainContent = {
        blocs: [content]
      }
      // et on affiche
      context.html(data)
    },

    /**
     * Affiche le html de base (home générique) avec context.raw() (donc sans context.html() et ses views dust)
     * @param {Context} context
     * @param {Object} options
     * @param {string} options.title
     * @param {string} options.content
     * @param {string[]} [options.cssFiles]
     * @param {string[]} [options.jsFiles]
     * @param {string} [options.jsCode]
     * @returns {string} le html complet
     */
    displayRaw: function getHtml (context, { title, content, cssFiles, jsFiles, jsCode = '' }) {
      // on regarde si on a du error= dans l'url
      let error = ''
      if (context.get.error) {
        error = decodeURIComponent(context.get.error)
        if (!/^err/i.test(error)) error = 'Erreur : ' + error
        // au cas où ce serait une erreur mise par an-flow
        error = error.replace('[forwarded]', '')
      }
      let html = `<!DOCTYPE html>
<html lang="fr">
<head>
  <title>${appName} - ${title}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link type="text/css" rel="stylesheet" media="all" href="/style.css?${version}" />\n`
      if (cssFiles) {
        for (const cssFile of cssFiles) {
          html += `<link type="text/css" rel="stylesheet" media="all" href="${cssFile}" />\n`
        }
      }
      // js, faut mettre par défaut l'url de la 1re sesathèque en global
      if (sesathequeBaseUrl) {
        html += `<script type="application/javascript">
  window.sesathequeBaseUrl = '${sesathequeBaseUrl}';
</script>\n`
      }

      // début du body
      html += `</head>
<body>
  <div class="sl-header">
    <div class="app">
      <a href="/"><img src="/images/logoLabomep.png" title="${appName}" alt="${appName}"></a>
      <span class="version">${version}</span>
      <span class="version"><a href="/mentionsLegales" target="_blank">Mentions légales</a></span>
    </div>
    <div class="info"></div>
    <div class="tools"></div>
  </div>

  <div id="main" role="document">
    <h1>${title}</h1>
    <noscript>
      <div class="bloc error" style="font-weight: bold; text-align: center">Vous devez avoir un navigateur avec javascript activé pour utiliser ce site.</div>
    </noscript>\n`

      // erreur éventuelle
      if (error) html += `<div class="bloc error">${error}</div>\n`

      // divs pour du js éventuel et contenu
      html += `<div id="warning"></div>
    <div id="notification"></div>
    ${content}
  </div>\n`

      // et le js
      if (jsFiles) {
        for (const jsFile of jsFiles) {
          html += `<script type="application/javascript" src="${jsFile}"></script>\n`
        }
      }
      if (jsCode) html += `<script type="application/javascript">${jsCode}</script>\n`

      // terminé
      html += '</body>\n</html>\n'

      const headers = {
        'Content-Type': 'text/html'
      }
      context.raw(html, { headers })
    }
  }
})
