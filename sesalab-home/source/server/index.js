/**
 * This file is part of Sesalab.
 *   Copyright 2014-2015, Association Sésamath
 *
 * Sesalab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation.
 *
 * Sesalab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SesaReactComponent (LICENCE.txt).
 * @see http://www.gnu.org/licenses/agpl.txt
 *
 *
 * Ce fichier fait partie de SesaReactComponent, créée par l'association Sésamath.
 *
 * Sesalab est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
 * les termes de la GNU Affero General Public License version 3 telle que publiée par la
 * Free Software Foundation.
 * Sesalab est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
 * sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.
 * Consultez la GNU Affero General Public License pour plus de détails.
 * Vous devez avoir reçu une copie de la GNU General Public License en même temps que SesaQcm
 * (cf LICENCE.txt et http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
 * pour une explication en français)
 */
'use strict'

const path = require('path')

/* global app */
app.controller(function ($settings, $staticHtml) {
  const title = $settings.get('application.homeTitle', 'Bienvenue sur Sesalab')
  const appName = $settings.get('application.name', 'Sésalab')
  const homePublicContent = $settings.get('application.content.homePublic', 'Sesalab est une application permettant de créer des séquences d’apprentissage')
  const mentionsLegalesContent = $settings.get('application.content.mentionsLegales', 'Aucune autre info disponible, il manque un fichier content/mentionsLegales.inc.html')
  const { getBaseData } = require('./viewHelper')($settings)
  const sesatheques = $settings.get('application.sesatheques')
  const sesathequeBaseUrl = (sesatheques.length && sesatheques[0].baseUrl) || undefined
  if (!sesathequeBaseUrl) console.error(Error('La première sesatheque n’a pas de baseUrl'))

  let Utilisateur

  // Static
  this.serve('/', path.join(__dirname, '/../../public'))

  // Home
  this.get('/', function (context) {
    /* @todo mettre ça dans sesasso-labomep pour le sortir de sesalab,
    // et ajouter un setCookie pour ne rediriger qu'une fois
    // on regarde si on a un cookie pour le SSO Sésamath
    // on verra pour le remettre plus tard
    const referer = context.request.get('Referer')
    if (context.request.cookies.sbd && (!referer || referer.indexOf(baseUrl) !== 0)) {
      return context.redirect('/sesasso/login')
    } */
    const data = getBaseData(title, 'home')
    // aj sesathequeBaseUrl
    if (sesathequeBaseUrl) data.sesathequeBaseUrl = sesathequeBaseUrl

    // @todo c'est au ssoProvider de filer des contenus à ajouter ici
    // il faudrait utiliser $sso.getAll et sur chacun appeler un getHtmlLoginBlock si ça existe
    data.hasLoginSup = true
    data.mainContent = {
      content: homePublicContent
    }
    // on ne gère pas ici context.get.error car le source est mis en cache pour tout le monde !
    // => ce sera traité en js (cf source du layout-page)
    context.html(data)
  })

  this.get('/mentionsLegales', function (context) {
    const data = getBaseData('Mentions légales')
    data.mainContent = {
      $view: 'mentionsLegales',
      appName,
      content: mentionsLegalesContent
    }
    context.html(data)
  })

  /**
   * Répond OK si mongo répond, une erreur mongo sinon (toujours en plain/text)
   * @route {GET} /alive
   */
  this.get('/alive', function (context) {
    if (!Utilisateur) Utilisateur = lassi.service('Utilisateur')
    Utilisateur.match().count((error, nb) => {
      if (!error && nb) return context.plain('OK')
      if (!error) error = new Error('Aucun utilisateur dans mongoDB (mais pas d’erreur en les comptant)')
      context.status = 500
      context.plain(error.stack || error.toString())
    })
  })

  /**
  * Page de destination en cas de perte de session
  * @route {GET} /lost-session
  */
  this.get('/lost-session', function (context) {
    const data = getBaseData(title)
    data.mainContent = {
      blocs: ['Session expirée, veuillez vous reconnecter (sur votre ENT si vous en utilisez un, sinon sur la <a href="/">page d’accueil</a>)']
    }

    context.html(data)
  })
})

// et on ajoute les autres controleurs de pages html
require('./sso')
require('./user')
// plus un service utile pour les pages statiques
require('./staticHtmlService')
