#!/bin/bash

usage() {
  echo "$0 [file]"
  echo 'Ouvre un shell mongo ou exécute un script mongo.'
  echo 'Sans argument ça ouvre un shell en chargeant scripts-mongo/mongorc.js au préalable'
  echo '(qui ajoute plusieurs fonctions utile dans un contexte sesalab, cf source pour le détail)'
  echo
  echo 'Options :'
  echo '  -c <collection> : indique une collection (pour -i ou -x, si le nom du fichier ne l’indique pas)'
  echo '  -d <dossier> : pour traiter tous les fichiers *.export qu’il contient'
  echo '  -e <string> : évalue <string> dans le scope du script (ex `-e "var foo = 42"` pour avoir la variable foo dans le script)'
  echo '  -f <fichier> : fichier à utiliser (script ou import ou export)'
  echo '  -g <fichier> : fichier (path absolu) de conf de l’appli (à prendre à la place de _private/config.js), peut aussi être surchargé avec NODE_ENV'
  echo '  -h : affiche cette aide'
  echo '  -i : lance mongoimport sur le fichier (collection déduite si -c absent) ou tous les fichiers de -d (sauf staging prod), ATTENTION car sans l’option -u ça DROP la collection avant l’import'
  echo '  -o : utiliser stdout plutôt que des fichiers pour l’export (ou stdin pour l’import)'
  echo '  -p : affiche seulement les arguments mongo'
  echo '  -r : run la commande passée et sort (sans donner de shell)'
  echo '  -q : mode --quiet'
  echo '  -Q : query pour filtrer l’export (ignoré sans -x -c), cf https://www.mongodb.com/docs/database-tools/mongoexport/#std-option-mongoexport.--query'
  echo '  -s : ajoute --shell, pour charger un ou des scripts et récupérer un shell ensuite'
  echo '  -u : mode --upsert à la place de --drop pour les imports'
  echo '  -x : avec -c, lance mongoexport vers le fichier (-f), sans exporte toutes les collections vers des fichiers du dossier -d'
  echo
  echo 'exemple d’export vers des fichiers ~/tmp/sesalab.YYYY-MM-JJ/collxxx.export :'
  echo "$0 -x -d ~/tmp/bibliotheque"'.$(date +%F)/'
  echo
  echo 'exemple d’import, avec des fichiers ~/tmp/sesalab.2017-10-30/collxxx.export :'
  echo './scripts/mongoApp -i -d ~/tmp/sesalab.2017-10-30/'
  echo
  echo 'pour boucler sur les collections :'
  echo 'for c in $(./scripts/mongoApp -q -r '\''db.getCollectionNames().join(" ")'\''); do echo "On peut faire qqchose avec la collection $c" ; done'
  exit $1
}

_abort () {
  echo "$* (ABANDON)" >&2
  exit 1
}

rootDir="$(realpath "$(dirname $0)"/..)"
nodeConf="$rootDir/_private/config.js"
[ -f "$rootDir/_private/${NODE_ENV-}.js" ] && nodeConf="$rootDir/_private/$NODE_ENV.js"

jsCode=''
staging=''
collection=''
dir=''
file=''
isImport=No
isExport=No
isQuiet=No
isUpsert=No
isStdIo=No
justPrint=No
forceShell=No
query=''
toRun=''

# check des options
while getopts "c:d:e:f:g:hiopqQ:r:sux" OPTION
do
  case $OPTION in
    c) collection=$OPTARG;;
    d) dir=$OPTARG;;
    e) jsCode="$OPTARG";;
    f) file="$OPTARG";;
    g) nodeConf="$OPTARG";;
    h) usage 0;;
    i) isImport=Yes;;
    o) isStdIo=Yes;;
    p) justPrint=Yes;;
    q) isQuiet=Yes;;
    Q) query="$OPTARG";;
    r) toRun="$OPTARG";;
    s) forceShell=Yes;;
    u) isUpsert=Yes;;
    x) isExport=Yes ;;
    *) echo "Erreur, option $OPTION déclarée mais non gérée" >&2
  esac
done

# pass en dernier car il peut contenir des espaces
# shellcheck disable=SC2046
read -r staging host port db user pass <<< $(node -e "const c = require('$nodeConf'); const d = c.\$entities.database; console.log(\`\${c.application.staging} \${d.host} \${d.port} \${d.name}\ \${d.user} '\${d.password}'\`)")

# check des variables
[ -z "$host" ] && echo "pas trouvé host dans $nodeConf">&2 && usage 1
[ -z "$port" ] && echo "pas trouvé port dans $nodeConf">&2 && usage 1
[ -z "$user" ] && echo "pas trouvé user dans $nodeConf">&2 && usage 1
[ -z "$pass" ] && echo "pas trouvé pass dans $nodeConf">&2 && usage 1
[ -z "$db" ] && echo "pas trouvé db dans $nodeConf">&2 && usage 1
# $pass contient déjà les single quotes
args="--host $host --port $port -u $user -p $pass"

[ "$isQuiet" == 'Yes' ] && args="$args --quiet"

MONGO="$(which mongo)"
[ -z "$MONGO" ] && MONGO="$(which mongosh)"
[ -z "$MONGO" ] && echo "Pas trouvé de binaire mongo ou mongosh">&2 && exit 1

# commande à lancer ?
if [ -n "$toRun" ]; then
  args="$args --eval \"${toRun//\"/\\\"}\""
  eval $MONGO $db $args
  exit $?
fi

# si on précise pas de fichier et que c'est ni import ni export, ce sera un shell,
# on ajoute ici nos fonctions par défaut
if [ "$isImport" == "No" ] && [ "$isExport" == "No" ] && [ -z "$file" ]; then
  file="$rootDir/scripts-mongo/mongorc.js"
  forceShell=Yes
fi

[ "$forceShell" == 'Yes' ] && args="$args --shell"
[ "$justPrint" == 'Yes' ] && echo "mongo(import|export) $args (--db $db) …" && exit 0

if [ "$isImport" == "Yes" ]; then
  [ "$staging" == "prod" -o  "$staging" == "production" ] && echo 'Import en production impossible (mettre config.application.staging=dev ou -pre-prod- pour le forcer)'>&2 && usage 1
  [ "$isUpsert" == "Yes" ] && args="--upsert $args --db $db" || args="--drop $args --db $db"
  if [ -n "$file" ]; then
    # On précise un fichier
    if [[ "$file" =~ \.bson$ ]]; then
      if [ -z "$collection" ]; then
        eval mongorestore $args "$file"
      else
        eval mongorestore $args --collection "$collection" "$file"
      fi
    else
      if [ -z "$collection" ]; then
        eval mongoimport $args --file  $file
      else
        eval mongoimport $args --file  $file --collection $collection
      fi
    fi
  elif [ -n "$dir" ]; then
    # on précise un dossier
    [ -n "$collection" ] && echo 'Si vous précisez une collection il faut préciser le fichier'>&2 && usage 1
    for file in "$dir"/*.export; do
      # on gère le cas aucun fichier .export
      [ -f "$file" ] || continue
      echo "import $file"
      eval mongoimport $args --file $file
    done
    for file in "$dir"/*.bson; do
      # on gère le cas aucun fichier .bson
      [ -f "$file" ] || continue
      echo "import $file"
      eval mongorestore $args $file
    done
  elif [ "$isStdIo" == "Yes" ]; then
    # entrée standard, faut préciser une collection
    if [ -n "$collection" ]; then
      eval mongoimport $args --collection $collection
    else
      echo "Avec l’option -o (stdio, soit stdin pour l’import) il faut indiquer une collection pour importer">&2 && usage 1
    fi
  else
    echo "Il faut indiquer un dossier ou un fichier pour importer">&2 && usage 1
  fi

elif [ "$isExport" == "Yes" ]; then
  [ -z "$dir" ] && [ -z "$file" ] && [ "$isStdIo" != "Yes" ] && echo "Il faut indiquer un dossier ou un fichier ou l’option -o (stdout) pour exporter">&2 && usage 1
  if [ -n "$collection" ]; then
    [ -n "$query" ] && args="$args --query '$query'"
    if [ "$isStdIo" == "Yes" ]; then
      eval mongoexport $args --db $db --collection $collection
    else
      [ -z "$file" ] && file="$dir/$collection.export"
      eval mongoexport $args --db $db --collection $collection > $file
    fi
  elif [ "$isStdIo" == "Yes" ]; then
    echo "Avec l’option -o (stdout) il faut indiquer une collection pour exporter">&2 && usage 1
  else
    # toutes les collections
    [ -z "$dir" ] && echo "Il faut indiquer dossier pour l'export si vous ne précisez pas de collection">&2 && usage 1
    for collection in $(echo 'show collections'|eval $MONGO --quiet $args $db); do
      file="$dir/$collection.export"
      echo "Collection $collection dans $dir/$collection.export"
      eval mongoexport $args --db $db --collection $collection > $file
    done
  fi

elif [ -n "$jsCode" ]; then
  eval $MONGO --eval "\"${jsCode//\"/\\\"}\"" $args $db $file

else
  eval $MONGO $args $db $file
fi
