const flow = require('an-flow')

// Les chemins sont relatifs au répertoire dans lequel ce fichier sera copié dans le container : /sesatheque/app/server/cli/loadRessources.js
// getBibliRessources est inséré au même endroit
const getRessources = require('./getBibliRessources')
const bibliBaseId = require('../../../_private/config').application.baseId

// TODO: on pourrait utiliser le même fonctionnement que test/browser/initBibli.js (création des
// ressources via appel API), ce qui éviterait ces injections de CLI dans le container de la sésathèque
function loadRessources (done) {
  const ressources = getRessources(bibliBaseId)
  const EntityRessource = lassi.service('EntityRessource')

  flow(Object.values(ressources))
    .seqEach(function (ressource) {
      console.log(`-- Loading Ressource ${ressource.titre} (${ressource.rid})`)
      EntityRessource.create(ressource).store(this)
    })
    .done((err) => done(err))
}

loadRessources.help = () => console.log('Charge des ressources pour l’environnement de dev / test sesalab')

module.exports = {
  loadRessources
}
