const uuid = require('an-uuid')
/**
 * Config de commun pour une paire de sesatheques de test (avec docker-compose)
 * Fichier à copier dans _private/
 */
module.exports = {
  application: {
    // utilisé en préfixe des message de log et dans qq message
    name: 'dockerBibliTest',
    // identifiant de cette sésathèque, qui devrait être connu de sesatheque-client
    baseId: 'dockerBibliTest',
    // sesatheque de référence qui groupe les baseId avec lesquels on partage des ressources
    baseIdRegistrar: 'dockerBibliTest',
    // si baseIdRegistrar connait baseId, faut mettre la valeur correspondante ici
    baseUrl: 'http://biblitest.local:3011/',
    mail: 'bibli@example.com',
    staging: 'dev' // prod ou dev
  },
  $entities: {
    database: {
      host: 'mongotest', // container mongo
      port: 27017,
      name: 'testBibli'
    }
  },
  // pour redis, prefix obligatoire
  $cache: {
    redis: {
      host: 'redistest',
      port: 6379,
      prefix: 'testBibli'
    }
  },
  $server: {
    port: 3011
  },
  $rail: {
    cookie: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      key: uuid()
    },
    session: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      secret: uuid()
    }
  },
  /* pour modifier le comportement par défaut on peut préciser ici qq overrides,
  cf app/config.js pour les valeurs par défaut
  par ex pour empêcher un formateur de créer des groupes ou des ressources ici */
  components: {
    personne: {
      roles: {
        formateur: { create: false, createGroupe: false }
      }
    }
  },
  logs: {
    debugExclusions: ['cache'],
    perf: 'perf.log'
  },
  // les modules à précharger avant bootstrap
  extraModules: ['sesalab-sso'],
  extraDependenciesLast: ['sesalab-sso'],
  apiTokens: [
    // ne pas laisser ces exemples en dehors d'un usage de dev ou test local !
    'dockerSesalabTestTokenForCommunTest', // celui de dockerSesalabTest
    'dockerCommunTestTokenForBibliTest' // celui de dockerCommunTest
  ],

  // urls absolues des sésathèques utilisées par nos ressources
  // (pour les alias d'une sesatheque dans une autre, mis par ex par un sesalab)
  // les baseId doivent être les mêmes que ceux mis dans les sesalabs qui nous contactent,
  // et identiques à ceux de sesatheque-client/src/sesatheques.js s'ils y sont
  // si on est baseIdRegistrar on répondra sur /api/baseId/:id pour ces baseId
  // inutile d'ajouter la sesatheque courante (baseId:baseUrl), elle est toujours ajoutée à la liste au boot
  sesatheques: [
    {
      baseId: 'dockerCommunTest',
      baseUrl: 'http://communtest.local:3012/',
      // un token à utiliser pour son api
      apiTokens: 'dockerBibliTestTokenForCommunTest'
    }
  ],
  // les sesalab qui nous causent (et propagent ici une authentification)
  // Attention, toutes les sésathèques qu'ils utilisent doivent être listées dans le module
  // sesatheque-client ou ci-dessus, pour qu'ils puissent créer des alias chez nous pointant
  // sur ces autres sésathèques
  sesalabs: [{
    name: 'dockerSesalabTest',
    baseId: 'sesalabtestlocal3010',
    baseUrl: 'http://sesalabtest.local:3010/'
  }],

  smtp: {
    host: 'mailhogtest',
    port: 1025
  }
}
