const uuid = require('an-uuid')
/**
 * Config de sesathequeGlobal pour une paire de sesatheques global/private (avec docker-compose)
 * Fichier à copier dans _private/
 */
module.exports = {
  application: {
    // utilisé en préfixe des message de log et dans qq message
    name: 'dockerBibli',
    // identifiant de cette sésathèque, qui devrait être connu de sesatheque-client
    baseId: 'biblilocal3001',
    // sesatheque de référence qui groupe les baseId avec lesquels on partage des ressources
    baseIdRegistrar: 'biblilocal3001',
    // si baseIdRegistrar connait baseId, faut mettre la valeur correspondante ici
    baseUrl: 'http://bibliotheque.local:3001/',
    mail: 'bibli@example.com',
    staging: 'dev' // prod ou dev
  },
  $entities: {
    database: {
      host: 'mongo', // container mongo
      port: 27017,
      name: 'bibli'
    }
  },
  // pour redis, prefix obligatoire
  $cache: {
    redis: {
      host: 'redis',
      port: 6379,
      prefix: 'bibli'
    }
  },
  $server: {
    port: 3001
  },
  $rail: {
    cookie: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      key: uuid()
    },
    session: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      secret: uuid()
    }
  },
  /* pour modifier le comportement par défaut on peut préciser ici qq overrides,
  cf app/config.js pour les valeurs par défaut
  par ex pour empêcher un formateur de créer des groupes ou des ressources ici */
  components: {
    personne: {
      roles: {
        formateur: { create: false, createGroupe: false }
      }
    }
  },
  logs: {
    debugExclusions: ['cache'],
    perf: 'perf.log'
  },
  // les modules à précharger avant bootstrap
  extraModules: ['sesalab-sso'],
  extraDependenciesLast: ['sesalab-sso'],
  apiTokens: [
    // ne pas laisser ces exemples en dehors d'un usage de dev ou test local !
    'dockerSesalabTokenForCommun', // celui de dockerSesalab
    'dockerCommunTokenForBibli' // celui de dockerCommun
  ],

  // urls absolues des sésathèques utilisées par nos ressources
  // (pour les alias d'une sesatheque dans une autre, mis par ex par un sesalab)
  // les baseId doivent être les mêmes que ceux mis dans les sesalabs qui nous contactent,
  // et identiques à ceux de sesatheque-client/src/sesatheques.js s'ils y sont
  // si on est baseIdRegistrar on répondra sur /api/baseId/:id pour ces baseId
  // inutile d'ajouter la sesatheque courante (baseId:baseUrl), elle est toujours ajoutée à la liste au boot
  sesatheques: [
    {
      baseId: 'communlocal3002',
      baseUrl: 'http://commun.local:3002/',
      // un token à utiliser pour son api
      apiToken: 'dockerBibliTokenForCommun'
    }
  ],
  // les sesalab qui nous causent (et propagent ici une authentification)
  // Attention, toutes les sésathèques qu'ils utilisent doivent être listées dans le module
  // sesatheque-client ou ci-dessus, pour qu'ils puissent créer des alias chez nous pointant
  // sur ces autres sésathèques
  sesalabs: [{
    name: 'dockerSesalab',
    baseId: 'sesalablocal3000',
    baseUrl: 'http://sesalab.local:3000/'
  }],

  // et le container mailhog pour nodemailer
  smtp: {
    host: 'mailhog',
    port: 1025
  },

  plugins: {
    internal: ['mental', 'serie', 'sequenceModele'],
    external: {
      '@sesatheque-plugins/arbre': 'https://git.sesamath.net/sesamath/sesatheque-plugin-arbre.git',
      '@sesatheque-plugins/iep': 'https://git.sesamath.net/sesamath/sesatheque-plugin-iep.git',
      '@sesatheque-plugins/j3p': 'https://git.sesamath.net/sesamath/sesatheque-plugin-j3p.git',
      '@sesatheque-plugins/mathgraph': 'https://git.sesamath.net/sesamath/sesatheque-plugin-mathgraph.git',
      '@sesatheque-plugins/url': 'https://git.sesamath.net/sesamath/sesatheque-plugin-url.git'
    }
  }
}
