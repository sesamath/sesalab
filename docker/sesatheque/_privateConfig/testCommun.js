const uuid = require('an-uuid')
const bibliConfig = require('./testBibli')
const bibliConfigApp = bibliConfig.application
/**
 * Config de commun pour une paire de sesatheques de test (avec docker-compose)
 * Fichier à copier dans _private/
 */
module.exports = {
  application: {
    // utilisé en préfixe des message de log et dans qq message
    name: 'dockerCommunTest',
    // identifiant de cette sésathèque, qui devrait être connu de sesatheque-client
    baseId: bibliConfig.sesatheques[0].baseId,
    // sesatheque de référence qui groupe les baseId avec lesquels on partage des ressources
    baseIdRegistrar: bibliConfigApp.baseId,
    // si baseIdRegistrar connait baseId, faut mettre la valeur correspondante ici
    baseUrl: bibliConfig.sesatheques[0].baseUrl,
    mail: 'comun@example.com',
    staging: 'dev' // prod ou dev
  },

  $entities: {
    database: {
      host: 'mongotest', // container mongo
      port: 27017,
      name: 'communtest'
    }
  },
  $cache: {
    redis: {
      host: 'redistest',
      port: 6379,
      prefix: 'communtest_'
    }
  },
  $server: {
    port: 3012
  },
  $rail: {
    cookie: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      key: uuid()
    },
    session: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      secret: uuid()
    }
  },
  logs: {
    debugExclusions: ['cache'],
    perf: 'perf.log'
  },
  // les modules à précharger avant bootstrap
  extraModules: ['sesalab-sso'],
  extraDependenciesLast: ['sesalab-sso'],
  apiTokens: [
    // mettre ici d'éventuels tokens utilisables pour poster sur notre api (sans session préalable)
    // ne pas laisser ces exemples en dehors d'un usage de dev ou test local !
    'dockerBibliTestTokenForCommunTest', // celui de dockerBibli
    'dockerSesalabTestTokenForCommunTest' // et celui de dockerSesalab
  ],
  // urls absolues des sésathèques utilisées par nos ressources
  // (pour les alias d'une sesatheque dans une autre, mis par ex par un sesalab)
  // les baseId doivent être les mêmes que ceux mis dans les sesalabs qui nous contactent,
  // et identiques à ceux de sesatheque-client/src/sesatheques.js s'ils y sont
  // si on est baseIdRegistrar on répondra sur /api/baseId/:id pour ces baseId
  // inutile d'ajouter la sesatheque courante (baseId:baseUrl), elle est toujours ajoutée à la liste au boot
  sesatheques: [
    {
      baseId: bibliConfigApp.baseId,
      baseUrl: bibliConfigApp.baseUrl,
      // apiToken: un token à utiliser pour son api (pour lire des ressources privées par ex)
      apiToken: bibliConfig.apiTokens[1]
    }
    // on pourrait en mettre d'autres…
  ],
  // les sesalab qui nous causent (et propagent ici une authentification)
  // Attention, toutes les sésathèques qu'ils utilisent doivent être listées dans le module
  // sesatheque-client ou ci-dessus, pour qu'ils puissent créer des alias chez nous pointant
  // sur ces autres sésathèques
  sesalabs: bibliConfig.sesalabs,

  smtp: {
    host: 'mailhogtest',
    port: 1025
  }
}
