const uuid = require('an-uuid')
/**
 * Config de sesathequePrivate pour une paire de sesatheques global/private (avec docker-compose)
 * baseId et baseUrl sont utilisés dans la conf de sesalab si c'est pas précisé dans son _private/config.js
 */
module.exports = {
  application: {
    // utilisé en préfixe des message de log et dans qq message
    name: 'dockerCommun',
    // identifiant de cette sésathèque, qui devrait être connu de sesatheque-client
    baseId: 'communlocal3002',
    // sesatheque de référence qui groupe les baseId avec lesquels on partage des ressources
    baseIdRegistrar: 'biblilocal3001',
    // si baseIdRegistrar connait baseId, faut mettre la valeur correspondante ici
    baseUrl: 'http://commun.local:3002/',
    mail: 'comun@example.com',
    staging: 'dev' // prod ou dev
  },
  $entities: {
    database: {
      host: 'mongo', // container mongo
      port: 27017,
      name: 'commun'
    }
  },
  $cache: {
    redis: {
      host: 'redis',
      port: 6379,
      prefix: 'commun'
    }
  },
  $server: {
    port: 3002
  },
  $rail: {
    cookie: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      key: uuid()
    },
    session: {
      // c'est pour docker, on peut en mettre un ≠ à chaque boot
      secret: uuid()
    }
  },
  logs: {
    debugExclusions: ['cache'],
    perf: 'perf.log'
  },
  // les modules à précharger avant bootstrap
  extraModules: ['sesalab-sso'],
  extraDependenciesLast: ['sesalab-sso'],
  apiTokens: [
    // mettre ici d'éventuels tokens utilisables pour poster sur notre api (sans session préalable)
    // ne pas laisser ces exemples en dehors d'un usage de dev ou test local !
    'dockerSesalabTokenForCommun', // celui de dockerSesalab
    'dockerBibliTokenForCommun' // celui de dockerBibli
  ],
  // urls absolues des sésathèques utilisées par nos ressources
  // (pour les alias d'une sesatheque dans une autre, mis par ex par un sesalab)
  // les baseId doivent être les mêmes que ceux mis dans les sesalabs qui nous contactent,
  // et identiques à ceux de sesatheque-client/src/sesatheques.js s'ils y sont
  // si on est baseIdRegistrar on répondra sur /api/baseId/:id pour ces baseId
  // inutile d'ajouter la sesatheque courante (baseId:baseUrl), elle est toujours ajoutée à la liste au boot
  sesatheques: [
    {
      baseId: 'biblilocal3001',
      // si baseId existe dans sesatheque-client/src/sesatheques.js, faut mettre le bon baseUrl
      baseUrl: 'http://bibliotheque.local:3001/',
      // apiTokens: un token à utiliser pour son api (pour lire du private par ex)
      apiTokens: 'dockerCommunTokenForBibli'
    }
  ],
  // les sesalab qui nous causent (et propagent ici une authentification)
  // Attention, toutes les sésathèques qu'ils utilisent doivent être listées dans le module
  // sesatheque-client ou ci-dessus, pour qu'ils puissent créer des alias chez nous pointant
  // sur ces autres sésathèques
  sesalabs: [{
    name: 'dockerSesalab',
    baseId: 'sesalablocal3000',
    baseUrl: 'http://sesalab.local:3000/'
  }],

  // et le container mailhog pour nodemailer
  smtp: {
    host: 'mailhog',
    port: 1025
  },

  plugins: {
    internal: ['mental', 'serie', 'sequenceModele'],
    external: {
      '@sesatheque-plugins/arbre': 'https://git.sesamath.net/sesamath/sesatheque-plugin-arbre.git#1.0.6',
      '@sesatheque-plugins/iep': 'https://git.sesamath.net/sesamath/sesatheque-plugin-iep.git#1.0.1',
      '@sesatheque-plugins/j3p': 'https://git.sesamath.net/sesamath/sesatheque-plugin-j3p.git#1.0.9',
      '@sesatheque-plugins/mathgraph': 'https://git.sesamath.net/sesamath/sesatheque-plugin-mathgraph.git#1.0.4',
      '@sesatheque-plugins/url': 'https://git.sesamath.net/sesamath/sesatheque-plugin-url.git#1.0.4'
    }
  }
}
