#!/bin/bash
set -u

# pour la lisibilité
OK=0
KO=1

#####
# Affiche la syntaxe à utiliser pour ce script
# [$1=0] code de sortie éventuel
function usage() {
  echo "$0 lance les tâches docker, lui passer en 1er argument"
  echo "  build : construit les container, à relancer à chaque changement de branche ou après un pull"
  echo "  dev : lance le container sesalab en mode dev (avec mapping sur le code courant mais pas les node_modules)"
  echo "  test : lance les tests"
  # si on nous passe pas de code de sortie en $1 on sort avec 0
  exit "${1-0}"
}

# raccourci pour sortir avec un message d'erreur
# (passer le ou les message(s) en argument)
function abandon() {
  echo "$*, ABANDON">&2
  exit $KO
}

# build tous les containers
function build() {
  initLogDir
  # Build tous les containers nécessaires pour le test (mongo, redis, sesalab, sesatheque)
  printf "\n -- Création/Mise à jour de tous les containers\n\n"
  docker-compose -f "$cnfDev" -f "$cnfTest" build

  # On démarre les containers
  printf "\n -- Démarrage des containers\n\n"
  docker-compose up -d

  # Pour faciliter le dev, charge quelques fixtures dans la base de dev
  printf "\n -- Chargement des fixtures Sésalab\n\n"
  # faut attendre les 2 sesathèques pour que ça râle pas au boot, commun est sensé attendre bibli
  # mais bibli refuse les connexions alors que commun les accepte… on enchaîne les deux wait
  ./scripts/wait-for-it.sh commun.local:3002 -t 30 --strict -- \
    ./scripts/wait-for-it.sh bibliotheque.local:3001 -t 30 --strict -- \
       docker-compose run sesalab.local ./source/cli.js loadFixtures
  # ça change rien, on a
  # bibliotheque.local:3001 is available after 0 seconds
  # La Sésathèque http://bibliotheque.local:3001/ ne répond pas correctement : Error: connect ECONNREFUSED 172.18.0.5:3001
  # :-/

  # Charge quelques ressources dans la bibli (commun peut rester vide)
  printf "\n -- Chargement des ressources Sésathèque\n\n"
  docker-compose run bibliotheque.local ./app/server/cli.js loadRessources

  # Fin du build, on stop les containers
  printf "\n -- Arrêt des containers\n\n"
  docker-compose stop
}

# démarre sesalab en mode dev
function dev() {
  initLogDir
  docker-compose -f "$cnfDev" up
}

# on ajoute nos dossiers de log exportés au cas où ils ne seraient pas là
function initLogDir() {
  [ -d log ] || mkdir log
  [ -d log/bibli ] || mkdir log/bibli
  [ -d log/commun ] || mkdir log/commun
  [ -d log/docker ] || mkdir log/docker
  [ -d log/bibliTest ] || mkdir log/bibliTest
  [ -d log/communTest ] || mkdir log/communTest
  [ -d log/dockerTest ] || mkdir log/dockerTest
}

# appelé dans le docker (sesatheque:docker-compose.avec.sesalab.yml) pour faire l'install + build
function initAndStart() {
  initLogDir
  npm install && npm run build && npm run init:fixtures && npm start
}

# arrête tout ce qui tourne
function stop() {
  docker-compose -f "$cnfDev" stop
  local exitCode=$?
  docker-compose -f "$cnfTest"  stop
  [ $? == $OK ] && [ $exitCode == $OK ] && return $OK
  echo "Il y a eu un problème lors de l'arrêt des containers"
  return $KO
}

# lance les tests
function testRun() {
  printf "\n -- Lancement des tests\n\n"
  initLogDir
  # Du temps où les container avaient les même noms dans docker-compose.test.yml et docker-compose.yml
  # il fallait cette ligne pour recréer le bon "sesalab" pour les tests
  # docker-compose -f "$cnfTest" up --no-start

  # le nom sut est imposé par circleCI, on utilise le même container pour lancer les tests localement
  # l'option --rm vire le container après l'exécution de la commande définie dans le docker-compose.test.yml
  docker-compose -f "$cnfTest" run --rm sut
  # On ressort avec le exit status des tests de sut
  exit $?
}

# racine du projet git
ROOT="$(dirname "$0")/.."
# On vérifie qu'on l'a correctement détecté, pas sûr que notre déduction rudimentaire
# fonctionne dans tous les cas de symlinks
if [ ! -f "$ROOT/package.json" ] || [ "$(grep -c '^[\t ]*"name" *: *"sesalab"' "$ROOT/package.json")" -lt 1 ]; then
  echo "$ROOT ne semble pas avoir été correctement détecté comme la racine de sesalab (pb de symlink ?), ABANDON">&2
  exit $KO
fi

# fichier de config docker-compose
cnfDev="$ROOT/docker-compose.yml"
cnfTest="$ROOT/docker-compose.test.yml"

# commande passée en 1er argument
arg=${1-}
case $arg in
  usage|"-h"|"--help") usage;;
  abandon) usage $KO;; # cette fonction doit rester privée
  "test") testRun;; # celle-là on l'a renommée pour pas écraser la commande interne du shell
  *)
    # si y'a une fct de ce nom là on la lance
    # shellcheck disable=SC2015
    declare -f "$arg" >/dev/null && $arg || usage $KO
  ;;
esac
