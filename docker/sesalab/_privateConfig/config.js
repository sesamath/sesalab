/**
 * _private/config de base pour docker
 * ./Dockerfile nous copie au build dans le _private du conteneur
 * ./docker-compose.yml copie ./_private en ./_privateLocal
 * copié dans l'instance docker dans _privateLocal par le Dockerfile,
 * avec nos sésathèques, ainsi que la surcharge des hosts mongo & redis pour coller aux noms
 * du docker-compose
 * @module
 */
// Les chemins sont relatifs au répertoire dans lequel ce fichier sera copié dans le container
// (/sesalab/_private)
const fs = require('fs')
const path = require('path')

const config = require('../config/default')
// ces fichiers bibliConfig.js et communConfig sont copiés par le Dockerfile
// docker/sesatheque/_privateConfig/config.js => _private/bibliConfig.js
// docker/sesatheque/_privateConfig/commun.js => _private/communConfig.js
const bibliConfig = require('./bibliConfig')
const bibliConfigApp = bibliConfig.application
const communConfig = require('./communConfig')
const communConfigApp = communConfig.application

// le _private à la racine de l'appli exporté en _privateLocal par le docker-compose
// fs.existsSync prend pas ../_privateLocal/config.js, on prend le path absolu
const configFile = path.join(__dirname, '..', '_privateLocal', 'config.js')
if (fs.existsSync(configFile)) {
  const localConfig = require('../_privateLocal/config')
  Object.assign(config, localConfig)
} else {
  console.error(Error('_privateLocal/config.js n’existe pas, vérifier docker-compose.yml'))
}

try {
  // on surcharge les sésathèques
  config.application.sesatheques = [
    // les tokens sont pas obligatoires en fonctionnement normal,
    // mais ils peuvent servir pour le chargement de fixtures via l'api de la sesatheque
    { baseId: bibliConfigApp.baseId, baseUrl: bibliConfigApp.baseUrl, apiToken: bibliConfig.apiTokens[0] },
    { baseId: communConfigApp.baseId, baseUrl: communConfigApp.baseUrl, apiToken: communConfig.apiTokens[0] }
  ]

  // notre baseUrl doit être celui que la 1re sesathèque connait dans sa config
  config.application.baseUrl = bibliConfig.sesalabs[0].baseUrl

  // on doit avoir le bon port
  if (!config.$server) config.$server = {}
  config.$server.port = 3000

  // faut ce salt pour que le user chargé avec les fixtures puisse se connecter
  if (!config.$crypto) config.$crypto = {}
  config.$crypto.salt = '1234'

  // les noms des hosts imposés par le docker-compose
  // c'est pas obligatoire de déclarer ça en _private, on vérifie
  // redis
  if (!config.$cache) config.$cache = {}
  if (!config.$cache.redis) config.$cache.redis = {}
  config.$cache.redis.host = 'redis'
  config.$cache.redis.port = 6379
  // mongo
  if (!config.$entities) config.$entities = {}
  if (!config.$entities.database) config.$entities.database = {}
  config.$entities.database.host = 'mongo'
  config.$entities.database.port = 27017

  // le smtp docker
  config.smtp = {
    host: 'mailhog',
    port: 1025
  }
} catch (error) {
  error.message = `_private/config.js incomplet (${error.message})`
  throw error
}

module.exports = config
