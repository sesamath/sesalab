// En test avec docker on ignore _privateLocal, ça doit fonctionner avec cette conf
// et peu importe qu'elle diffère de la conf de test hors docker
const bibliConfig = require('./testBibli')
const bibliConfigApp = bibliConfig.application
const communConfigApp = require('./testCommun').application

const configTest = {
  application: {
    // Cf docker-compose.test.yml
    baseUrl: 'http://sesalabtest.local:3010/',
    sesatheques: [
      { baseId: bibliConfigApp.baseId, baseUrl: bibliConfigApp.baseUrl, apiToken: bibliConfig.apiTokens[0] },
      { baseId: communConfigApp.baseId, baseUrl: communConfigApp.baseUrl }
    ],
    profils: {
      all: {
        label: 'Tout',
        oid: 'sesalabAll'
      },
      default: {
        label: 'Liste par défaut',
        oid: 'sesalabAll'
      },
      autonomie: {
        label: 'Exercices utilisables en autonomie',
        oid: 'sesalabAll'
      },
      ecole: {
        label: 'École',
        oid: 'sesalabEcole'
      },
      college: {
        label: 'Collège',
        oid: 'sesalabCollege'
      },
      lycee: {
        label: 'Lycee',
        oid: 'sesalabCollege'
      }
    }
  },
  $cache: {
    redis: {
      host: 'redistest',
      port: 6379,
      prefix: 'sesalab_test'
    }
  },
  $entities: {
    database: {
      host: 'mongotest',
      port: 27017,
      name: 'sesalab-test'
    }
  },
  smtp: {
    host: 'mailhogtest',
    port: 1025
  }
}

module.exports = configTest
